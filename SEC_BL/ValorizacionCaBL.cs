﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class ValorizacionCaBL
    {
        ValorizacionCaDA objValCaDA = new ValorizacionCaDA();
        public List<ValorizacionCaBE> ObtenerValorizacionPartida(ValorizacionCaBE objValorizacionCa)
        {
            List<ValorizacionCaBE> lstValorizacionCaBE = new List<ValorizacionCaBE>();

            lstValorizacionCaBE = objValCaDA.ObtenerValorizacionPartida(objValorizacionCa);

            return lstValorizacionCaBE;
        }
    }
}
