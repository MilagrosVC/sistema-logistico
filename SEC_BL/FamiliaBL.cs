﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class FamiliaBL
    {
        FamiliaDA ObjFamiliaDA = new FamiliaDA();

        public List<FamiliaBE> ListarFamilia(FamiliaBE ObjFamilia)
        {
            List<FamiliaBE> lstFamilia = new List<FamiliaBE>();
            lstFamilia = ObjFamiliaDA.ListarFamilia(ObjFamilia);
            return lstFamilia;
        }
    }
}
