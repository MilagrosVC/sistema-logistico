﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class ProyectoBL
    {
        ProyectoDA objProyectoDA = new ProyectoDA();
        public DataTable ListarProyecto(ProyectoBE objProyecto)
        {
            DataTable lstProyecto = new DataTable();
            lstProyecto = objProyectoDA.ListarProyectos(objProyecto);
            return lstProyecto;
        }

        public List<ProyectoBE> ListarProyectos(int codEmpresa)
        {
            List<ProyectoBE> lstProyecto = new List<ProyectoBE>();
            lstProyecto = objProyectoDA.ListarProyecto(codEmpresa);
            return lstProyecto;
        }

        public ProyectoBE DatosProyecto(ProyectoBE objProyectoBE)
        {
            objProyectoBE = objProyectoDA.DatosProyecto(objProyectoBE);
            return objProyectoBE;
        }


        public List<ProyectoBE> ListarProyectoCombo(ProyectoBE objProyecto)
        {
            List<ProyectoBE> lstProyecto = new List<ProyectoBE>();
            lstProyecto = objProyectoDA.ListarProyectoCombo(objProyecto);
            return lstProyecto;
        }
    }
}
