﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_DA;
using SEC_BE;

namespace SEC_BL
{
    public class SolicitudProveedorBL
    {
        SolicitudProveedorDA objSolicitudPrvDA = new SolicitudProveedorDA();
        public int InsertarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            int id = 0;
            id = objSolicitudPrvDA.InsertarProveedorSolicitud(ObjSolicitudPrv);
            return id;
        }

        public List<SolicitudProveedorBE> ListarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            List<SolicitudProveedorBE> lstSolicitudPrv = new List<SolicitudProveedorBE>();
            lstSolicitudPrv = objSolicitudPrvDA.ListarProveedorSolicitud(ObjSolicitudPrv);
            return lstSolicitudPrv;
        }

        public int EliminarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            int id = 0;
            id = objSolicitudPrvDA.EliminarProveedorSolicitud(ObjSolicitudPrv);
            return id;
        }
    }
}
