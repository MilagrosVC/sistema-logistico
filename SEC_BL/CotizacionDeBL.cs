﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class CotizacionDeBL
    {
        CotizacionDeDA objCotizacionDeDA = new CotizacionDeDA();
        public List<CotizacionDeBE> ListarCotizacionDetalle(CotizacionDeBE ObjCotizacionDe)
        {
            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            lstCotizacionDe = objCotizacionDeDA.ListarCotizacionDetalle(ObjCotizacionDe);
            return lstCotizacionDe;
        }

        public void ActualizarDetalleCotizacion(CotizacionDeBE ObjCotizacionDe)
        {
            objCotizacionDeDA.ActualizarDetalleCotizacion(ObjCotizacionDe);
        }


        public List<CotizacionDeBE> GenerarVistaCotizacionDetalle(CotizacionDeBE ObjCotizacionDe)
        {
            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            lstCotizacionDe = objCotizacionDeDA.GenerarVistaCotizacionDetalle(ObjCotizacionDe);
            return lstCotizacionDe;
        }
    }
}
