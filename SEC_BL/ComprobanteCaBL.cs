﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class ComprobanteCaBL
    {
        ComprobanteCaDA ObjComprobanteCaDA = new ComprobanteCaDA();
        public int CrearOrdenCompraCabecera(ComprobanteCaBE ObjComprobanteCa)
        {
            int id = 0;
            id = ObjComprobanteCaDA.CrearComprobanteCabecera(ObjComprobanteCa);
            return id;
        }

        public ComprobanteCaBE ExisteFactura(ComprobanteCaBE ObjComprobanteCa)
        {
            ObjComprobanteCa = ObjComprobanteCaDA.ExisteFactura(ObjComprobanteCa);
            return ObjComprobanteCa;
        }

        public List<ComprobanteCaBE> ListarComprobanteCa(ComprobanteCaBE ObjComprobanteCaBE)
        {
            List<ComprobanteCaBE> lstComprobanteCaBE = new List<ComprobanteCaBE>();
            lstComprobanteCaBE = ObjComprobanteCaDA.ListarComprobanteCa(ObjComprobanteCaBE);
            return lstComprobanteCaBE;
        }

        public ComprobanteCaBE GenerarVistaComprobante(ComprobanteCaBE ObjComprobanteCa)
        {
            ObjComprobanteCa = ObjComprobanteCaDA.GenerarVistaComprobante(ObjComprobanteCa);
            return ObjComprobanteCa;
        }
    }
}
