﻿using System;
using System.Collections.Generic;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class PerfilBL
    {

        PerfilDA objPerfilDA = new PerfilDA();

        public int CrearPerfil(PerfilBE ObjPerfilBE)
        {

            int id = 0;

            id = objPerfilDA.CrearPerfil(ObjPerfilBE);

            return id;

        }

        public List<PerfilBE> ListarPerfil(PerfilBE ObjPerfilBE)
        {
            List<PerfilBE> lstPerfilBE = new List<PerfilBE>();
            lstPerfilBE = objPerfilDA.ListarPerfiles(ObjPerfilBE);
            return lstPerfilBE;

        }
        public void ActualizarPerfil(PerfilBE ObjPerfilBE)
        {
            objPerfilDA.ActualizarPerfil(ObjPerfilBE);
        }

        public void EliminarPerfil(PerfilBE ObjPerfilBE)
        {
            objPerfilDA.EliminarPerfil(ObjPerfilBE);
        }
    }
}
