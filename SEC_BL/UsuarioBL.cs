﻿using System;
using System.Collections.Generic;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
  public class UsuarioBL
    {
        UsuarioDA objUsuarioDA = new UsuarioDA();
        public int CrearUsuario(UsuarioBE objUsuarioBE)
        {
            int id = 0;
            id = objUsuarioDA.CrearUsuario(objUsuarioBE);
            return id;
        }

        public List<UsuarioBE> ListarUsuario(UsuarioBE objUsuarioBE)
        {
            List<UsuarioBE> lstUsuarioBE= new List<UsuarioBE>();
            lstUsuarioBE = objUsuarioDA.ListarUsuario(objUsuarioBE);
            return lstUsuarioBE;
        }
        
         public void ActualizarUsuario(UsuarioBE objUsuarioBE)
        {
            objUsuarioDA.ActualizarUsuario(objUsuarioBE);
        }

        public void EliminarUsuario(UsuarioBE objUsuarioBE)
        {
            objUsuarioDA.EliminarUsuario(objUsuarioBE);
        }

        public List<UsuarioBE> ValidarUsuarioLista(UsuarioBE ObjUsuario)
        {
            List<UsuarioBE> lstUsuarioBE = new List<UsuarioBE>();
            lstUsuarioBE = objUsuarioDA.ValidarUsuarioLista(ObjUsuario);
            return lstUsuarioBE;
        }

        public int ValidarUsuario(UsuarioBE ObjUsuario)
        {
            int id = 0;
            id = objUsuarioDA.ValidarUsuario(ObjUsuario);
            return id;
        }


    }


    }

