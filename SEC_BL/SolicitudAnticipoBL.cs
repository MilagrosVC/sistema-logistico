﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class SolicitudAnticipoBL
    {

        SolicitudAnticipoDA objSolicitudAnticipoDA = new SolicitudAnticipoDA();

        //LISTAR SOLICITUD DE ANTICIPO
        public List<SolicitudAnticipoBE> ListarSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            List<SolicitudAnticipoBE> lstSolicitudAnticipo = new List<SolicitudAnticipoBE>();
            lstSolicitudAnticipo = objSolicitudAnticipoDA.ListarSolicitudAnticipo(objSolicitudAnticipo);
            return lstSolicitudAnticipo;
        }

        public int CrearSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo) {
            int i = 0;
            i = objSolicitudAnticipoDA.CrearSolicitudAnticipo(objSolicitudAnticipo);
            return i;
        }

        //ModificarEstadoAnticipo - 01 Septiembre
        public void ModificarEstadoAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            objSolicitudAnticipoDA.ModificarEstadoAnticipo(objSolicitudAnticipo);
        }

        //ModificarEstadoAnticipo - 08 Septiembre PV
        public void EliminarRAP(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            objSolicitudAnticipoDA.EliminarRAP(objSolicitudAnticipo);
        }

        public SolicitudAnticipoBE cargarRAP(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            SolicitudAnticipoBE BE = new SolicitudAnticipoBE();
            BE = objSolicitudAnticipoDA.cargarRAP(objSolicitudAnticipo);
            return BE;
        }
    }
}
