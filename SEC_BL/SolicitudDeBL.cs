﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class SolicitudDeBL
    {
        SolicitudDeDA objSolicitudDeDA = new SolicitudDeDA();
        public int CrearSolicitudDetalle(SolicitudDeBE ObjSolicitudDe)
        {
            int id = 0;
            id = objSolicitudDeDA.CrearSolicitudDetalle(ObjSolicitudDe);
            return id;
        }

        public List<SolicitudDeBE> ListarSolicitudDetalle(SolicitudDeBE ObjSolicitudDe)
        {
            List<SolicitudDeBE> lstSolicitudDe = new List<SolicitudDeBE>();
            lstSolicitudDe = objSolicitudDeDA.ListarSolicitudDetalle(ObjSolicitudDe);
            return lstSolicitudDe;
        }

        public void EditarSolicitudDetalle(SolicitudDeBE ObjSolicitudDe)
        {
            objSolicitudDeDA.EditarSolicitudDetalle(ObjSolicitudDe);
        }
    }
}
