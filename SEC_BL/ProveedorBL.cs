﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class ProveedorBL
    {
        ProveedorDA objProveedorDA = new ProveedorDA();
        //Crear Proveedor
        public int CrearProveedor(ProveedorBE objproveedor)
        {
            int id = 0;
            id = objProveedorDA.CrearProveedor(objproveedor);
            return id;
        }

       
        public DataTable ListarProveedor(ProveedorBE objproveedor)
        {
            DataTable lstProveedor = new DataTable();
            lstProveedor = objProveedorDA.ListarProveedor(objproveedor);
            return lstProveedor;
        }
        //Listar Proveedor en la Tabla
        public List<ProveedorBE> ListarProveedores(ProveedorBE objProveedor) {
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorDA.ListarProveedores(objProveedor);
            return lstProveedor;
        }

        //Modificar Proveedor
        public void ModificarProveedor(ProveedorBE objProveedor) {
            objProveedorDA.ModificarProveedor(objProveedor);
        }
        //Eliminar a Proveedor

        public void EliminarProveedor(ProveedorBE objProveedor) {
            objProveedorDA.EliminarProveedor(objProveedor);
        }
        public List<ProveedorBE> ListarComboProveedores(ProveedorBE objProveedor)
        {
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorDA.ListarComboProveedores(objProveedor);
            return lstProveedor;
        }

    }
}
