﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class ElementoBL
    {
        ElementoDA objElementoDA = new ElementoDA();
        public List<ElementoBE> ListarElemento(ElementoBE ObjElemento)
        {
            List<ElementoBE> lstElemento= new List<ElementoBE>();
            lstElemento= objElementoDA.ListarElemento(ObjElemento);
            return lstElemento;
        }
    }
}
