﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class MarcaBL
    {
        MarcaDA ObjMarcaDA = new MarcaDA();
        
        public List<MarcaBE> ListarMarca(MarcaBE ObjMarca)
        {
            List<MarcaBE> lstMarca = new List<MarcaBE>();
            lstMarca = ObjMarcaDA.ListarMarca(ObjMarca);
            return lstMarca;
        }
    }
}
