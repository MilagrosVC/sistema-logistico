﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class PartidaBL
    {
        PartidaDA objPartidaDA = new PartidaDA();
        public List<PartidaBE> ListarPartida(PartidaBE ObjPartida)
        {
            List<PartidaBE> lstPartida = new List<PartidaBE>();
            lstPartida = objPartidaDA.ListarPartida(ObjPartida);
            return lstPartida;
        }
    }
}
