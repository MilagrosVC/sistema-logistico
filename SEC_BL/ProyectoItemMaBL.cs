﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
   public class ProyectoItemMaBL
    {
       ProyectoItemMaDA ObjProyectoItemDA = new ProyectoItemMaDA();

       //*---Items Disponibles---*//
       public List<Item_MaBE> ListarItemMaDisponible(ProyectoItemMaBE objProyectoItemMa) 
       {
           return ObjProyectoItemDA.ListarItemMaDisponible(objProyectoItemMa);
       }
        //*----Items Asignados----*//
       //public List<Item_MaBE> ListarItemMaAsignado(ProyectoItemMaBE objProyectoItemMa)
       //{
       //    return ObjProyectoItemDA.ListarItemMaAsignado(objProyectoItemMa);
       //}
       public List<ProyectoItemMaBE> ListarItemMaAsignado(ProyectoItemMaBE objProyectoItemMa)
       {
           return ObjProyectoItemDA.ListarItemMaAsignado(objProyectoItemMa);
       }

       //Asignar Asignar - Designar ItemMA
       public void AsignarDesignarItemMa(List<ProyectoItemMaBE> ObjProyectoI)
       {
           foreach (var item in ObjProyectoI)
           {
               ObjProyectoItemDA.AsignarDesignarItemMa(item);
           }
       }
       //Asignar Asignar - Designar ItemMA TODOS
       public void AsignarDesignarItemMaTodos(List<ProyectoItemMaBE> ObjProyectoI)
       {
           foreach (var item in ObjProyectoI)
           {
               ObjProyectoItemDA.AsignarDesignarItemMaTodos(item);
           }
       }
       
       //public void AsignarDesignarItemMa_AD(ProyectoItemMaBE ObjProyectoI)
       //{
       //    //foreach (var item in ObjProyectoI)
       //    //{
       //    //    ObjProyectoItemDA.AsignarDesignarItemMa(item);
       //    //}
       //    string var = ObjProyectoI.IMa_Id;

       //}

       public void ModificarProyectoItem(ProyectoItemMaBE objProyectoItemMa)
       {
           ObjProyectoItemDA.ModificarProyectoItem(objProyectoItemMa);
       }
    }
}
