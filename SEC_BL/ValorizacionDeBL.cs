﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;


namespace SEC_BL
{
    public class ValorizacionDeBL
    {
        ValorizacionDeDA objValDeDA = new ValorizacionDeDA();
        public List<ValorizacionDeBE> ListarItemsValorizacion(int idValorizacionCa)
        {
            List<ValorizacionDeBE> lstItemsValorizacion = new List<ValorizacionDeBE>();
            lstItemsValorizacion = objValDeDA.ListarItemsValorizacion(idValorizacionCa);
            return lstItemsValorizacion;
        }
    }
}
