﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;
namespace SEC_BL
{
    public class TipoMedioPagoBL
    {
        TipoMedioPagoDA objTipoMedioPagoDA = new TipoMedioPagoDA();

        public List<TipoMedioPagoBE> ListarTipoMedioPago(TipoMedioPagoBE ObjTipoMedioPago)
        {
            List<TipoMedioPagoBE> lstTipoMedioPago = new List<TipoMedioPagoBE>();
            lstTipoMedioPago = objTipoMedioPagoDA.ListarTipoMedioPago(ObjTipoMedioPago);
            return lstTipoMedioPago;
        }
    }
}
