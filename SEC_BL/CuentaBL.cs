﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class CuentaBL
    {
        CuentaDA objCuentaDA= new CuentaDA();

        //LISTAR CUENTA

        public List<CuentaBE> ListarCuenta(CuentaBE objCuenta)
        {
            List<CuentaBE> lstCuenta = new List<CuentaBE>();
            lstCuenta = objCuentaDA.ListarCuenta(objCuenta);
            return lstCuenta;
        }
        //Crea Cuenta
        //public int CrearCuenta(CuentaBE ObjCuenta) {
        public CuentaBE CrearCuenta(CuentaBE ObjCuenta)
        {
            //int id = 0;
            ObjCuenta = objCuentaDA.CrearCuenta(ObjCuenta);
            return ObjCuenta;
        }
        //Crear Cuenta Tranfiere
        public int CrearCuentaTransfiere(CuentaBE ObjCuenta)
        {
            int id = 0;
            id = objCuentaDA.CrearCuentaTransfiere(ObjCuenta);
            return id;
        }

        //Verificar Numeros Codigo
        public int VerificarCodigoCuenta(CuentaBE ObjCuenta) {
            int numerocodigo=0;
            numerocodigo = objCuentaDA.VerificarCodigoCuenta(ObjCuenta);
            return numerocodigo;
        }
    }
}
