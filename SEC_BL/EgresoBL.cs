﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;


namespace SEC_BL
{
    public class EgresoBL
    {
        EgresoDA ObjEgresoDA = new EgresoDA();

        public int CrearEgreso(EgresoBE ObjEgreso) {
            int id = 0;
            id = ObjEgresoDA.CrearEgreso(ObjEgreso);
            return id;
        }

        public List<EgresoBE> ListarEgreso(EgresoBE ObjEgreso)
        {
            List<EgresoBE> lstEgreso = new List<EgresoBE>();
            lstEgreso = ObjEgresoDA.ListarEgreso(ObjEgreso);
            return lstEgreso;
        }

        public void ModificarEgreso(EgresoBE ObjEgreso) {
            ObjEgresoDA.ModificarEgreso(ObjEgreso);
        }
    }
}
