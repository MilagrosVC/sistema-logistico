﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;


namespace SEC_BL
{
    public class ParametroBL
    {
        ParametroDA objParametroDA = new ParametroDA();
        //Listar Parametro
        //public List<ParametroBE> ListarParametro(){
        //List<ParametroBE> lstParametro = new List<ParametroBE>();
        //lstParametro = objParametroDA.ListarParametro();
        //return lstParametro;
        //}


        public List<ParametroBE> ListarParametros(ParametroBE ObjetoParametroBE) {

            List<ParametroBE> lstParametros = new List<ParametroBE>();
            lstParametros = objParametroDA.ListarParametros(ObjetoParametroBE);
            return lstParametros;
        }
    }
}
