﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class UbigeoBL
    {
        UbigeoDA objUbigeoDA= new UbigeoDA();

        /*public DataTable ListarUbigeo(UbigeoBE objUbigeo) {
            DataTable LstUbigeo = new DataTable();
            LstUbigeo = objUbigeoDA.ListarUbigeo(objUbigeo);
            return LstUbigeo;
        }*/

        public List<UbigeoBE> ListarUbigeo(UbigeoBE objUbigeo)
        {
            List<UbigeoBE> LstUbigeo = new List<UbigeoBE>();
            LstUbigeo = objUbigeoDA.ListarUbigeo(objUbigeo);
            return LstUbigeo;
        }

        public int ObtenerCodigoUbigeo(UbigeoBE objUbigeo)
        {
            return objUbigeoDA.ObtenerCodigoUbigeo(objUbigeo);
        }
    }
}
