﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class ComprobanteDeBL
    {
        ComprobanteDeDA ObjComprobanteDeDA = new ComprobanteDeDA();
        public List<ComprobanteDeBE> GenerarVistaComprobanteDetalle(ComprobanteDeBE ObjComprobanteDe)
        {
            List<ComprobanteDeBE> lstComprobanteDe = new List<ComprobanteDeBE>();
            lstComprobanteDe = ObjComprobanteDeDA.GenerarVistaComprobanteDetalle(ObjComprobanteDe);
            return lstComprobanteDe;
        }
    }
}
