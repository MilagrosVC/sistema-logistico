﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class CotizacionCaBL
    {
        CotizacionCaDA objCotizacionDA = new CotizacionCaDA();

        public int CrearCotizacionCayDe(CotizacionCaBE objCotizacionCa)
        {
            int id = 0;
            id = objCotizacionDA.CrearCotizacionCayDe(objCotizacionCa);
            return id;
        }

        public List<CotizacionCaBE> ListarCotizaciones(CotizacionCaBE objCotizacionCa)
        {
            List<CotizacionCaBE> lstCotizacionCa = new List<CotizacionCaBE>();
            lstCotizacionCa = objCotizacionDA.ListarCotizaciones(objCotizacionCa);
            return lstCotizacionCa;
        }

        public List<CotizacionCaBE> ListarCotizacion(CotizacionCaBE ObjCotizacion)
        {
            List<CotizacionCaBE> lstCotizacion = new List<CotizacionCaBE>();
            lstCotizacion = objCotizacionDA.ListarCotizacion(ObjCotizacion);
            return lstCotizacion;
        }

        public void CambiarEstadoCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {
            objCotizacionDA.CambiarEstadoCotizacionCabecera(ObjCotizacionCa);
        }

        public void SeleccionCotizacionGanadora(CotizacionCaBE ObjCotizacionCa)
        {
            objCotizacionDA.SeleccionCotizacionGanadora(ObjCotizacionCa);
        }

        public void ActualizarCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {
            objCotizacionDA.ActualizarCotizacionCabecera(ObjCotizacionCa);
        }


        public CotizacionCaBE GenerarVistaCotizacionCA(CotizacionCaBE ObjCotizacionCa)
        {
            ObjCotizacionCa = objCotizacionDA.GenerarVistaCotizacionCA(ObjCotizacionCa);
            return ObjCotizacionCa;

        }

    }
}
