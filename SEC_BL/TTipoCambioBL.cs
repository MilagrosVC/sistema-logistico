﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
   public class TTipoCambioBL
    {
       TTipoCambioDA ObjTTipoCambioDA = new TTipoCambioDA();

       public List<TTipoCambioBE> ListarTTipoCambio(TTipoCambioBE ObjTTipoCambio){
           List<TTipoCambioBE> lstTTipoCambio = new List<TTipoCambioBE>();
           lstTTipoCambio = ObjTTipoCambioDA.ListarTTipoCambio(ObjTTipoCambio);
           return lstTTipoCambio;
       }
    }
}
