﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class PlanDeCuentaBL
    {
        PlanDeCuentaDA objPlanDeCuentaDA = new PlanDeCuentaDA();

        /*Listar Plan de Cuenta*/
        public List<PlanDeCuentaBE> ListarPlanDeCuenta(PlanDeCuentaBE objPlanDeCuenta) {
            List<PlanDeCuentaBE> lstPlanDeCuenta = new List<PlanDeCuentaBE>();
            lstPlanDeCuenta = objPlanDeCuentaDA.ListarPlanDeCuenta(objPlanDeCuenta);
            return lstPlanDeCuenta;
        
        
        }
    }
}
