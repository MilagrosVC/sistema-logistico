﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class OrdenCompraDeBL
    {
        OrdenCompraDeDA objOrdenCompraDeDA = new OrdenCompraDeDA();
        public List<OrdenCompraDeBE> ListarOrdenCompraDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = objOrdenCompraDeDA.ListarOrdenCompraDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;
        }

        public List<OrdenCompraDeBE> GenerarVistaOrdenCompraDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = objOrdenCompraDeDA.GenerarVistaOrdenCompraDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;
        }
        //CREADA MV 07 Agosto
        public List<OrdenCompraDeBE> GenerarVistaOrdenCDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = objOrdenCompraDeDA.GenerarVistaOrdenCDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;
        }
    }
}
