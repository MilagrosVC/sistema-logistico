﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class MonedaBL
    {
        MonedaDA objMonedaDA = new MonedaDA();
        public List<MonedaBE> ListarMoneda(MonedaBE ObjMoneda)
        {
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            lstMoneda = objMonedaDA.ListarMoneda(ObjMoneda);
            return lstMoneda;
        }
    }
}
