﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;
using System.Data;

namespace SEC_BL
{
    public class EmpresaBL
    {
        EmpresaDA objEmpresaDA = new EmpresaDA();
        public DataTable ListarEmpresa(EmpresaBE objEmpresa)
        {
            DataTable lstEmpresa = new DataTable();
            lstEmpresa = objEmpresaDA.ListarEmpresa(objEmpresa);
            return lstEmpresa;
        }

        public List<EmpresaBE> ListarComboEmpresa(EmpresaBE ObjEmpresa)
        {
            List<EmpresaBE> lstEmpresa = new List<EmpresaBE>();
            lstEmpresa = objEmpresaDA.ListarComboEmpresa(ObjEmpresa);
            return lstEmpresa;
        }
    }
}
