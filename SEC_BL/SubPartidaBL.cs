﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class SubPartidaBL
    {
        SubPartidaDA objSubPartidaDA = new SubPartidaDA();
        public List<SubPartidaBE> ListarSubPartida(SubPartidaBE ObjSubPartida)
        {
            List<SubPartidaBE> lstSubPartida = new List<SubPartidaBE>();
            lstSubPartida = objSubPartidaDA.ListarSubPartida(ObjSubPartida);
            return lstSubPartida;
        }
    }
}
