﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class UndMedidaBL
    {
        UndMedidaDA objUndMedidaDA = new UndMedidaDA();
        public List<UndMedidaBE> ListarUnidadMedida(UndMedidaBE ObjUndMedida)
        {
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            lstUndMedida = objUndMedidaDA.ListarUnidadMedida(ObjUndMedida);
            return lstUndMedida;
        }
    }
}
