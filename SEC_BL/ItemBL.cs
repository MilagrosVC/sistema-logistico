﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class ItemBL
    {
        ItemDA objItemDA = new ItemDA();
        public List<ItemBE> ListarItem(ItemBE ObjItem)
        {
            List<ItemBE> lstItem = new List<ItemBE>();
            lstItem = objItemDA.ListarItem(ObjItem);
            return lstItem;
        }

        /*public List<ItemBE> ListarItemSubItem(ItemBE ObjItem)
        {
            List<ItemBE> lstItem = new List<ItemBE>();
            lstItem = objItemDA.ListarItemSubItem(ObjItem);
            return lstItem;
        }*/

        /*public DataTable ListarItemSubItem(ItemBE ObjItem)
        {
            DataTable lstItem = new DataTable();
            lstItem = objItemDA.ListarItemSubItem(ObjItem);
            return lstItem;
        }*/

        public List<ItemBE> ListarItemSubItem(ItemBE ObjItem)
        {
            List<ItemBE> lstItem = new List<ItemBE>();
            lstItem = objItemDA.ListarItemSubItem(ObjItem);
            return lstItem;
        }
    }
}
