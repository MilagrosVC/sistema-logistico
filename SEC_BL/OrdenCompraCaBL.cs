﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class OrdenCompraCaBL
    {
        OrdenCompraCaDA objOrdenCompraCaDA = new OrdenCompraCaDA();
        public int CrearOrdenCompraCabecera(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            int id = 0;
            id = objOrdenCompraCaDA.CrearOrdenCompraCabecera(ObjOrdenCompraCa);
            return id;
        }

        public List<OrdenCompraCaBE> ListarOrdenCompraCabecera(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            List<OrdenCompraCaBE> lstOrdenCompraCa = new List<OrdenCompraCaBE>();
            lstOrdenCompraCa = objOrdenCompraCaDA.ListarOrdenCompraCabecera(ObjOrdenCompraCa);
            return lstOrdenCompraCa;
        }

        public OrdenCompraCaBE GenerarVistaOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            ObjOrdenCompraCa = objOrdenCompraCaDA.GenerarVistaOrdenCompraCa(ObjOrdenCompraCa);
            return ObjOrdenCompraCa;
        }

        public OrdenCompraCaBE ObtenerOrdenCompra(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            ObjOrdenCompraCa = objOrdenCompraCaDA.ObtenerOrdenCompra(ObjOrdenCompraCa);
            return ObjOrdenCompraCa;
        }

        public int ActualizarOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            int id = 0;
            id = objOrdenCompraCaDA.ActualizarOrdenCompraCa(ObjOrdenCompraCa);
            return id;
        }
        public OrdenCompraCaBE GenerarVistaOrdenCCA(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            ObjOrdenCompraCa = objOrdenCompraCaDA.GenerarVistaOrdenCCA(ObjOrdenCompraCa);
            return ObjOrdenCompraCa;

        }
    }
}
