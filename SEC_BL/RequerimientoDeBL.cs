﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class RequerimientoDeBL
    {
        RequerimientoDeDA objRequerimientoDeDA = new RequerimientoDeDA();
        public int CrearRequerimientoDetalle(RequerimientoDeBE ObjRequerimientoDe)
        {
            int id = 0;
            id = objRequerimientoDeDA.CrearRequerimientoDetalle(ObjRequerimientoDe);
            return id;
        }

        public List<RequerimientoDeBE> ListarItemsxRequerimiento(RequerimientoDeBE ObjRequerimientoDe)
        {
            List<RequerimientoDeBE> lstRequerimientoDe = new List<RequerimientoDeBE>();
            lstRequerimientoDe = objRequerimientoDeDA.ListarItemsxRequerimiento(ObjRequerimientoDe);
            return lstRequerimientoDe;
        }

        public void EliminarRqDetalle(RequerimientoDeBE ObjRequerimientoDe)
        {
            objRequerimientoDeDA.EliminarRqDetalle(ObjRequerimientoDe);
        }

        public int ActualizarRequerimientoDe(RequerimientoDeBE ObjRequerimientoDe)
        {
            int id = 0;
            id = objRequerimientoDeDA.ActualizarRequerimientoDe(ObjRequerimientoDe);
            return id;
        }
    }
}
