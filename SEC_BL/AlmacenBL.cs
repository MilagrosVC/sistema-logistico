﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;



namespace SEC_BL
{
    public class AlmacenBL
    {
        AlmacenDA objAlmacenDA = new AlmacenDA();

        public List<AlmacenBE> ListarAlmacen(AlmacenBE objAlmacen)
        {
            List<AlmacenBE> lstAlmacen = new List<AlmacenBE>();
            lstAlmacen = objAlmacenDA.ListarAlmacen(objAlmacen);
            return lstAlmacen;
        }


        //Crear Almacen
        public int CrearAlmacen(AlmacenBE objAlmacen)
        {
            int id = 0;
            id = objAlmacenDA.CrearAlmacen(objAlmacen);
            return id;
        }

        //Eliminar Almacen
        public void EliminarAlmacen(AlmacenBE objAlmacen)
        {
            objAlmacenDA.EliminarAlmacen(objAlmacen);
        }


        //Modificar Almacen
        public void ModificarAlmacen(AlmacenBE objAlmacen)
        {
            objAlmacenDA.ModificarAlmacen(objAlmacen);
        }
    }
}
