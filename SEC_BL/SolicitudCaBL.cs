﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class SolicitudCaBL
    {
        SolicitudCaDA objSolicitudCaDA = new SolicitudCaDA();
        public int CrearSolicitudCabecera(SolicitudCaBE ObjSolicitudCa)
        {
            int id = 0;
            id = objSolicitudCaDA.CrearSolicitudCabecera(ObjSolicitudCa);
            return id;
        }

        public int CrearSolicitudCayDe(SolicitudCaBE ObjSolicitudCa)
        {
            int id = 0;
            id = objSolicitudCaDA.CrearSolicitudCayDe(ObjSolicitudCa);
            return id;
        }

        public List<SolicitudCaBE> ListarSolicitudes(SolicitudCaBE ObjSolicitudCa)
        {
            List<SolicitudCaBE> lstSolicitudCa = new List<SolicitudCaBE>();
            lstSolicitudCa = objSolicitudCaDA.ListarSolicitudes(ObjSolicitudCa);
            return lstSolicitudCa;
        }
    }
}
