﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class EstadoDeBL
    {
        EstadoDeDA objEstadoDeDA = new EstadoDeDA();
        public List<EstadoDeBE> ListarEstados(EstadoDeBE ObjEstadoDe)
        {
            List<EstadoDeBE> lstEstadosBE = new List<EstadoDeBE>();
            lstEstadosBE = objEstadoDeDA.ListarEstados(ObjEstadoDe);
            return lstEstadosBE;
        }

        public List<EstadoDeBE> ListarEstadosxCA(string EstadoCa)
        {
            List<EstadoDeBE> lstEstadosBE = new List<EstadoDeBE>();
            lstEstadosBE = objEstadoDeDA.ListarEstados(EstadoCa);
            return lstEstadosBE;
        }
    }
}
