﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class Item_MaBL
    {
        Item_MaDA ObjItem_MaDA = new Item_MaDA();

        public List<Item_MaBE> ListarItem_Ma(Item_MaBE ObjItem_Ma)
        {
            List<Item_MaBE> lstItem_Ma = new List<Item_MaBE>();
            lstItem_Ma = ObjItem_MaDA.ListarItem_Ma(ObjItem_Ma);
            return lstItem_Ma;
        }

        //Crear ItemMA
        public int CrearItemMA(Item_MaBE ObjItem_Ma)
        {
            int id = 0;
            id = ObjItem_MaDA.CrearItemMA(ObjItem_Ma);
            return id;
        }

        //Eliminar 
        public void EliminarItemMA(Item_MaBE ObjItem_Ma)
        {
            ObjItem_MaDA.EliminarItemMA(ObjItem_Ma);
        }

        //Modificar 
        public void ModificarItemMA(Item_MaBE ObjItem_Ma)
        {
            ObjItem_MaDA.ModificarItemMA(ObjItem_Ma);
        }
    }
}
