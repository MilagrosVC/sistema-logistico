﻿using System;
using System.Collections.Generic;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class PerfilDetalleBL
    {
        PerfilDetalleDA objPerfilDetalleDA = new PerfilDetalleDA();

        public List<PerfilDetalleBE> ListarPerfilDetalle(UsuarioBE ObjUsuario)
        {

            List<PerfilDetalleBE> lstPerfilDetalleBE = new List<PerfilDetalleBE>();

            lstPerfilDetalleBE = objPerfilDetalleDA.ListarPerfilDetalleBE(ObjUsuario);

            return lstPerfilDetalleBE;

        }
        private int obtenerMaxNivel(List<PerfilDetalleBE> lstPerfilDetalleBE) {
            var varMaxNivel = 0; var i = 0;
            for (i = 0; i < lstPerfilDetalleBE.Count; i++)
            {
                if (lstPerfilDetalleBE[i].Nav_Nivel_Nu > varMaxNivel) { varMaxNivel = lstPerfilDetalleBE[i].Nav_Nivel_Nu; }
            }
            return varMaxNivel;
            }
        public List<MenuBE> establecerHijos(List<PerfilDetalleBE> lstPerfilDetalleBE) {
            List<MenuBE> lstMenu = new List<MenuBE>();

            var i = 0; 

            for (i = 0; i < lstPerfilDetalleBE.Count; i++) {
                var varNhijos = 0;
                MenuBE Be = new MenuBE();
                lstMenu.Add(Be);
                lstMenu[i].Nav_Navegacion_Id = lstPerfilDetalleBE[i].Nav_Navegacion_Id;
                lstMenu[i].Nav_Navegacion_Tx = lstPerfilDetalleBE[i].Nav_Navegacion_Tx;
                lstMenu[i].Nav_Padre_Id = lstPerfilDetalleBE[i].Nav_Padre_Id;
                lstMenu[i].Nav_Link = lstPerfilDetalleBE[i].Nav_Link;
                lstMenu[i].Nav_Nivel_Nu = lstPerfilDetalleBE[i].Nav_Nivel_Nu;
                //lstMenu[i].Nav_RutaImagen_Tx = lstPerfilDetalleBE[i].Nav_RutaImagen_Tx;
                //lstMenu[i].Nav_Orden_Nu = lstPerfilDetalleBE[i].Nav_Orden_Nu;
                //lstMenu[i].Nav_Habilitado_Fl = lstPerfilDetalleBE[i].Nav_Habilitado_Fl;  
                varNhijos = buscarHijo(lstPerfilDetalleBE, lstMenu[i].Nav_Navegacion_Id);
                if (varNhijos > 0) { lstMenu[i].Hijo_Fl = true; } else { lstMenu[i].Hijo_Fl = false; }
                lstMenu[i].N_Hijos = varNhijos;
            }
            return lstMenu;
        }
        public int buscarHijo(List<PerfilDetalleBE> lstMenu, int pId)
        {
       
            var varNhij= 0;
            for (var k = 0; k < lstMenu.Count; k++)
            {
                if (lstMenu[k].Nav_Padre_Id == pId) { varNhij++;}//Tiene Hijo
       
            }
            return varNhij;
        }
        public string generarMenu(UsuarioBE ObjUsuario)
        {   var varNMax=0;var Nmenu=0;
            
            var strMenu = "<ul class='nav navbar-nav'><li><a href='../../Home/Form/Home.aspx'><b><i>INICIO</i></b></a></li>";
            PerfilDetalleBE objPerfilDetalleBE = new PerfilDetalleBE();
            List<PerfilDetalleBE> lstPerfilDetalleBE = ListarPerfilDetalle(ObjUsuario);
            varNMax = obtenerMaxNivel(lstPerfilDetalleBE);
            List<MenuBE> lstMenu = new List<MenuBE>();
            lstMenu = establecerHijos(lstPerfilDetalleBE);
            List<MenuBE> lstRama = new List<MenuBE>();
           Nmenu=lstMenu.Count;
            var i = 0; 
            for (i = 0; i < lstMenu.Count; i++)
            {
                //if (i == 18) {
                //    var z = 0;
                
                //}
                
                
                if (lstMenu[i].Nav_Nivel_Nu == 1)
                {

                    if (lstMenu[i].Hijo_Fl == true)
                    {
                        strMenu += string.Format("<li class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' href='#'><b><i>{0}</i></b><span class='caret'></span></a>", lstMenu[i].Nav_Navegacion_Tx);
                        strMenu += string.Format("<ul class='dropdown-menu'>");

                    }
                    else { 
                        strMenu += string.Format("<li><a href='{0}'>{1}</a></li>", lstMenu[i].Nav_Link ,lstMenu[i].Nav_Navegacion_Tx); 

                        
                    }
                    lstRama.Clear(); 
                    lstRama.Add(lstMenu[i]);
                }

                if (lstMenu[i].Nav_Nivel_Nu == 2)
                {
                    if (lstMenu[i].Hijo_Fl == true)
                    {
                        strMenu += string.Format("<li><a class='trigger right-caret'>{0}</a><ul class='dropdown-menu sub-menu'>", lstMenu[i].Nav_Navegacion_Tx);
                    }
                    else {
                        strMenu += string.Format("<li><a href='{0}'>{1}</a></li>", lstMenu[i].Nav_Link, lstMenu[i].Nav_Navegacion_Tx);
                        var Nrama = lstRama.Count; var varExisteRama = false;

                        for (var j = i+1; j < Nmenu; j++)
                        {
                            if (lstRama[Nrama - 1].Nav_Navegacion_Id == lstMenu[j].Nav_Padre_Id)
                            {
                                varExisteRama = true; break;
                            }
                        }

                        if (varExisteRama == false) { strMenu += "</ul></li>"; };
                    
                    }
                    var varNavId =lstMenu[i].Nav_Navegacion_Id;
                    if (lstRama.Count >= 2) { lstRama[1] = lstMenu[i]; }
                    else { lstRama.Add(lstMenu[i]); }
                    
                }
                if (lstMenu[i].Nav_Nivel_Nu == 3)
                {
                    strMenu += string.Format("<li><a href='{0}'>{1}</a></li>", lstMenu[i].Nav_Link, lstMenu[i].Nav_Navegacion_Tx);
                    if (i < lstMenu.Count - 1)
                    {
                        if (lstMenu[i + 1].Nav_Padre_Id != lstMenu[i].Nav_Padre_Id)
                        {
                            strMenu += "</ul>";
                           
                            var Nrama = lstRama.Count-1;var varExisteRama =false;

                            for (var j = i; j < Nmenu; j++) {
                                if (lstRama[Nrama].Nav_Padre_Id == lstMenu[j].Nav_Padre_Id)
                                {
                                    varExisteRama = true; break;
                                } 
                            }

                            if (varExisteRama == false) { strMenu += "</ul></li>"; };
                        }
                    }
                }
            }
            strMenu += "</ul>";
           return strMenu;
          }
        //public string GeneraMenu(ref string defaultPage)
        //{
        //    PerfilDetalleBE objPerfilDetalleBE= new PerfilDetalleBE();
        //    return GenerarMenu();
        //}




        //List<PerfilDetalleBE> GetPadres(List<PerfilDetalleBE> menuList)
        //{
        //    List<PerfilDetalleBE> newList = new List<PerfilDetalleBE>();

        //    foreach (PerfilDetalleBE n in menuList)
        //    {
        //        if (n.Nav_Padre_Id == 0)
        //            newList.Add(n);
        //    }

        //    return newList;
        //}

        //List<PerfilDetalleBE> GetHijos(List<PerfilDetalleBE> menuList, int PadreId)
        //{
        //    List<PerfilDetalleBE> newList = new List<PerfilDetalleBE>();

        //    foreach (PerfilDetalleBE n in menuList)
        //    {
        //        if (n.Nav_Padre_Id == PadreId)
        //            newList.Add(n);
        //    }

        //    return newList;
        //}

    }
}
