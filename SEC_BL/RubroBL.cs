﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_DA;
using SEC_BE;



namespace SEC_BL
{
   public class RubroBL
    {
       RubroDA ObjRubroDA = new RubroDA();
       
       //Listar Rubros Disponibles para asingar a Proveedor
       public List<RubroBE> ListarRubro() {
           List<RubroBE> lstRubro = new List<RubroBE>();
           lstRubro = ObjRubroDA.ListarRubro();
           return lstRubro;
        }

       //Listar Rubros Asignados del Proveedor
       public List<RubroBE> ListarRubroAsignado(int IdProveedor)
       {
           List<RubroBE> lstRubroAsignado = new List<RubroBE>();
           lstRubroAsignado = ObjRubroDA.ListarRubroAsignado(IdProveedor);
           return lstRubroAsignado;
       }
    }
}
