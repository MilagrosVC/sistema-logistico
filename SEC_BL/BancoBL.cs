﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class BancoBL
    {
        BancoDA objBancoDA = new BancoDA();
        public List<BancoBE> ListarBanco(BancoBE objBanco) {
            List<BancoBE> lstBanco = new List<BancoBE>();
            lstBanco = objBancoDA.ListarBanco(objBanco);
            return lstBanco;
        }
    }
}
