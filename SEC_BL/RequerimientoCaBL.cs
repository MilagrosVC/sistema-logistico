﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class RequerimientoCaBL
    {
        RequerimientoCaDA objRequerimientoCaDA = new RequerimientoCaDA();
        public int CrearRequerimientoCabecera(RequerimientoCaBE ObjRequerimientoCa)
        {
            int id = 0;
            id = objRequerimientoCaDA.CrearRequerimientoCabecera(ObjRequerimientoCa);
            return id;
        }

        public List<RequerimientoCaBE> CargarBandejaRQ(RequerimientoCaBE ObjRequerimientoCa)
        {
            List<RequerimientoCaBE> lstRequerimientoCaBe = new List<RequerimientoCaBE>();
            lstRequerimientoCaBe = objRequerimientoCaDA.CargarBandejaRQ(ObjRequerimientoCa);
            return lstRequerimientoCaBe;
        }

        public int CambiarEstadoRequerimiento(RequerimientoCaBE ObjRequerimientoCa)
        {
            int id = 0;
            id = objRequerimientoCaDA.CambiarEstadoRequerimiento(ObjRequerimientoCa);
            return id;
        }

        public List<RequerimientoCaBE> ListarRequerimientoCa(RequerimientoCaBE objRequerimientoCaBE)
        {
            List<RequerimientoCaBE> lstRequerimientoCaBe = new List<RequerimientoCaBE>();
            lstRequerimientoCaBe = objRequerimientoCaDA.ListarRequerimientoCa(objRequerimientoCaBE);
            return lstRequerimientoCaBe;
        }

        public RequerimientoCaBE GenerarVistaRequerimiento(RequerimientoCaBE objRequerimientoCaBE)
        {
            objRequerimientoCaBE = objRequerimientoCaDA.GenerarVistaRequerimiento(objRequerimientoCaBE);
            return objRequerimientoCaBE;
        }

        public RequerimientoCaBE ObtenerDatosRequerimiento(RequerimientoCaBE ObjRequerimientoCaBE)
        {
            ObjRequerimientoCaBE = objRequerimientoCaDA.ObtenerDatosRequerimiento(ObjRequerimientoCaBE);
            return ObjRequerimientoCaBE;
        }
        
        public int ActualizarRequerimientoCa(RequerimientoCaBE ObjRequerimientoCa)
        {
            int id = 0;
            id = objRequerimientoCaDA.ActualizarRequerimientoCa(ObjRequerimientoCa);
            return id;
        }
    }

}
