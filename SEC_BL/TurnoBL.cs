﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class TurnoBL
    {
        TurnoDA objTurnoDA = new TurnoDA();
        public List<TurnoBE> ListarTurno(TurnoBE ObjTurno)
        {
            List<TurnoBE> lstTurno = new List<TurnoBE>();
            lstTurno = objTurnoDA.ListarTurno(ObjTurno);
            return lstTurno;
        }
    }
}
