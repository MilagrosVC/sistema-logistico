﻿using SEC_BE;
using SEC_DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BL
{
    public class PresupuestoBL
    {
        PresupuestoDA objPresupuestoDA = new PresupuestoDA();
        public DataTable ListarPresupuesto(PresupuestoBE Objpresupuesto)
        {
            DataTable lstPresupuesto = new DataTable();
            lstPresupuesto = objPresupuestoDA.ListarPresupuesto(Objpresupuesto);
            return lstPresupuesto;
        }
    }
}
