﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using SEC_DA;

namespace SEC_BL
{
    public class ProveedorRubroBL
    {

        ProveedorRubroDA ObjProveedorRubroDA = new ProveedorRubroDA();

        //*---Rubros Disponibles---*//
        public List<RubroBE> ListarRubroDisponible(ProveedorRubroBE objProveedorRubro) 
            {
                return ObjProveedorRubroDA.ListarRubroDisponible(objProveedorRubro);
             }
       

        //*----Rubros Asignados----*//
            public List<RubroBE> ListarRubroAsignado(ProveedorRubroBE objProveedorRubro) {

                return ObjProveedorRubroDA.ListarRubroAsignado(objProveedorRubro);            
            //public List<RubroBE> ListarRubroAsignado(int IdProveedor)
            //{
            //    return ObjProveedorRubroDA.ListarRubroAsignado(IdProveedor);
            //}
            }

        //*---Asignar Designar Rubros a Proveedor---*//

            //public void AsignarDesignarRubro(ProveedorRubroBE ObjProveedorR) {
            public void AsignarDesignarRubro(List<ProveedorRubroBE> ObjProveedorR)
            {
                foreach (var item in ObjProveedorR) {
                    ObjProveedorRubroDA.AsignarDesignarRubros(item);
                }     
            }
        //*----Listar Proveedor Rubro----*//



    }
}
