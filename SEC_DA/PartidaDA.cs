﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class PartidaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<PartidaBE> ListarPartida(PartidaBE ObjPartida)
        {
            try
            {
                //PartidaBE ObjPartida = new PartidaBE();
                List<PartidaBE> lstPartida = new List<PartidaBE>();

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_PARTIDA", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                cmdsq.Parameters.Add(new SqlParameter("@Partida_Id", ObjPartida.Partida_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Presupuesto_Id", ObjPartida.Presupuesto_Id));
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    ObjPartida = new PartidaBE();

                    ObjPartida.Partida_Id = Int32.Parse(rdr["Partida_Id"].ToString());
                    ObjPartida.Prt_Nombre_Tx = rdr["Prt_Nombre_Tx"].ToString();
                    ObjPartida.Presupuesto_Id = Int32.Parse(rdr["Presupuesto_Id"].ToString());
                    //ObjParametro.Par_Descripcion_Tx=rdr["Par_Descripcion_Tx"].ToString();
                    //ObjParametro.Par_Valor_Nu = Int32.Parse(rdr["Par_Valor_Nu"].ToString());

                    lstPartida.Add(ObjPartida);
                }
                conn.Close();
                return lstPartida;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
