﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class ComprobanteDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<ComprobanteDeBE> GenerarVistaComprobanteDetalle(ComprobanteDeBE ObjComprobanteDe)
        {
            List<ComprobanteDeBE> lstComprobanteDe = new List<ComprobanteDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VISTA_COMPROBANTE_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Comprobante_Id", ObjComprobanteDe.Comprobante_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjComprobanteDe = new ComprobanteDeBE();
                    lstComprobanteDe.Add(new EntityMapper<ComprobanteDeBE>(rdr, ObjComprobanteDe).FillEntity());
                }

                conn.Close();
                return lstComprobanteDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
