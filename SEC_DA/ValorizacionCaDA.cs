﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SEC_BE;


namespace SEC_DA
{
    public class ValorizacionCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
        public List<ValorizacionCaBE> ObtenerValorizacionPartida(ValorizacionCaBE objValorizacionCA)
        {
            List<ValorizacionCaBE> lstValCaBE = new List<ValorizacionCaBE>();

            try
            {
                SqlConnection cnn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VALORIZACION_CA", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@PartidaId", objValorizacionCA.Partida_Id));

                cnn.Open();


                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objValorizacionCA = new ValorizacionCaBE();

                    objValorizacionCA.Valorizacion_Id = Int32.Parse(rdr["Valorizacion_Id"].ToString());
                    objValorizacionCA.Val_Obra_Tx = rdr["Val_Obra_Tx"].ToString();
                    objValorizacionCA.NomPartida = rdr["NomPartida"].ToString();
                    objValorizacionCA.NomCliente = rdr["NomCliente"].ToString();
                    objValorizacionCA.NomProveedor = rdr["NomProveedor"].ToString();
                    objValorizacionCA.Val_Descripcion_Tx = rdr["Val_Descripcion_Tx"].ToString();
                    objValorizacionCA.Val_Fe = DateTime.Parse(rdr["Val_Fe"].ToString());

                    lstValCaBE.Add(objValorizacionCA);
                }

                cnn.Close();

                return lstValCaBE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
