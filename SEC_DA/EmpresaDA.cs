﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class EmpresaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public DataTable ListarEmpresa(EmpresaBE objEmpresa)
        {
            try
            {
               //Database database = DatabaseFactory.CreateDatabase("SEC_DB");
               
               // DbCommand cmd = database.GetStoredProcCommand("SEC_PRC_S_EMPRESA");
               //// database.AddInParameter(cmd, "Area_Id", DbType.Int32, area.AreaId);
               //// database.AddInParameter(cmd, "Are_Est_Activo_Fl", DbType.Boolean, area.Estado);
               //// database.AddInParameter(cmd, "Are_Nombre_Tx", DbType.String, area.Nombre);
               //// database.AddInParameter(cmd, "Are_Descripcion_Tx", DbType.String, area.Descripcion);

               // List<EmpresaBE> result = new List<EmpresaBE>();
               // using (IDataReader rdr = database.ExecuteReader(cmd))
               // {
               //     while (rdr.Read())
               //     {
               //         objEmpresa = new EmpresaBE();
               //         result.Add(objEmpresa);
               //     }
               //     rdr.Close();
               // }
               // return result;
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_EMPRESA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objEmpresa.Prv_Nombres_tx));
                //cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx", objEmpresa.Prv_APaterno_tx));

                //Abrir conexion
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EmpresaBE> ListarComboEmpresa(EmpresaBE ObjEmpresa)
        {
            List<EmpresaBE> lstEmpresa = new List<EmpresaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_EMPRESA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjEmpresa = new EmpresaBE();
                    lstEmpresa.Add(new EntityMapper<EmpresaBE>(rdr, ObjEmpresa).FillEntity());
                }
                conn.Close();
                return lstEmpresa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
