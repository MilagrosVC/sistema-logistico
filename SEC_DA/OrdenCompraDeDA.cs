﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class OrdenCompraDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<OrdenCompraDeBE> ListarOrdenCompraDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ORDEN_COMPRA_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraDe.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Detalle_Id", ObjOrdenCompraDe.Orden_Compra_Detalle_Id));
                      //cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjCotizacion.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraDe = new OrdenCompraDeBE();
                    lstOrdenCompraDe.Add(new EntityMapper<OrdenCompraDeBE>(rdr, ObjOrdenCompraDe).FillEntity());
                }

                conn.Close();
                return lstOrdenCompraDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        public List<OrdenCompraDeBE> GenerarVistaOrdenCompraDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VISTA_ORDEN_COMPRA_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraDe.Orden_Compra_Id));
                //cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Detalle_Id", ObjOrdenCompraDe.Orden_Compra_Detalle_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraDe = new OrdenCompraDeBE();
                    lstOrdenCompraDe.Add(new EntityMapper<OrdenCompraDeBE>(rdr, ObjOrdenCompraDe).FillEntity());
                }

                conn.Close();
                return lstOrdenCompraDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //CREADA MV 07 Agosto
        public List<OrdenCompraDeBE> GenerarVistaOrdenCDetalle(OrdenCompraDeBE ObjOrdenCompraDe) {
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ORDEN_COMPRA_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraDe.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Detalle_Id", ObjOrdenCompraDe.Orden_Compra_Detalle_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjOrdenCompraDe = new OrdenCompraDeBE();
                    lstOrdenCompraDe.Add(new EntityMapper<OrdenCompraDeBE>(rdr, ObjOrdenCompraDe).FillEntity());
                }

                conn.Close();
                return lstOrdenCompraDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        
        
        }
    }
}
