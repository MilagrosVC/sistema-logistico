﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SEC_BE;

namespace SEC_DA
{
    
    public class ProveedorDA
    {
        /* Cadena de conexion */
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        /*--- Version de conexion con libreria EnterpriseLibrary(Falta configurar) ---*/
        /*public int CrearProveedor(ProveedorBE objProveedor)
        {
            int id = 0;
            Database dtb = DatabaseFactory.CreateDatabase("SEC");
            try
            {
                DbCommand command = dtb.GetStoredProcCommand("SEC_PRC_I_PROVEEDOR");
                    
                dtb.AddInParameter(command,"@Rubro_Id",DbType.Int32,objProveedor.Rubro_Id);
                dtb.AddInParameter(command,"@Tipopersona_Id",DbType.Int32,objProveedor.Tipopersona_Id);
                dtb.AddInParameter(command,"@Prv_Homologado_Fl",DbType.Boolean,objProveedor.Prv_Homologado_Fl);
                dtb.AddInParameter(command,"@Prv_Estado_Homologado_Id",DbType.Int32,objProveedor.Prv_Estado_Homologado_Id);
                dtb.AddInParameter(command,"@Prv_Homologadora_Id",DbType.Int32,objProveedor.Prv_Homologadora_Id);
                dtb.AddInParameter(command,"@Prv_Estado_Id",DbType.Boolean,objProveedor.Prv_Estado_Id);
                dtb.AddInParameter(command,"@Prv_APaterno_tx",DbType.String,objProveedor.Prv_APaterno_tx);
                dtb.AddInParameter(command,"@Prv_AMaterno_tx",DbType.String,objProveedor.Prv_AMaterno_tx);
                dtb.AddInParameter(command,"@Prv_Nombres_tx",DbType.String,objProveedor.Prv_Nombres_tx);
                dtb.AddInParameter(command,"@Prv_RazonSocial_Tx",DbType.String,objProveedor.Prv_RazonSocial_Tx);
                dtb.AddInParameter(command,"@Prv_Personacontacto_Tx",DbType.String,objProveedor.Prv_Personacontacto_Tx);
                dtb.AddInParameter(command,"@TipDocIdent_Id",DbType.Int32,objProveedor.TipDocIdent_Id);
                dtb.AddInParameter(command,"@Prv_DocIdent_nu",DbType.String,objProveedor.Prv_DocIdent_nu);
                dtb.AddInParameter(command,"@Prv_Nacimiento_Fe",DbType.DateTime,objProveedor.Prv_Nacimiento_Fe);
                dtb.AddInParameter(command,"@Prv_Direccion_tx",DbType.String,objProveedor.Prv_Direccion_tx);
                dtb.AddInParameter(command,"@Ubigeo_Id",DbType.Int32,objProveedor.Ubigeo_Id);
                dtb.AddInParameter(command,"@Prv_Telefono1_nu",DbType.String,objProveedor.Prv_Telefono1_nu);
                dtb.AddInParameter(command,"@Prv_Telefono2_nu",DbType.String,objProveedor.Prv_Telefono2_nu);
                dtb.AddInParameter(command,"@Prv_Email_tx",DbType.String,objProveedor.Prv_Email_tx);
                dtb.AddInParameter(command,"@Prv_Cla_Id",DbType.Int32,objProveedor.Prv_Cla_Id);
                dtb.AddInParameter(command,"@Aud_UsuarioCreacion_Id",DbType.String,objProveedor.Aud_UsuarioCreacion_Id);
                dtb.AddInParameter(command,"@Aud_Creacion_Fe",DbType.DateTime,objProveedor.Aud_Creacion_Fe);
                dtb.AddInParameter(command,"@Aud_Estado_Fl", DbType.Boolean, objProveedor.Aud_Estado_Fl);
                dtb.AddOutParameter(command, "@ID", DbType.Int32,32);
                dtb.ExecuteNonQuery(command);
                id = int.Parse(dtb.GetParameterValue(command, "@ID").ToString());
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        //Crear Proveedor
        public int CrearProveedor(ProveedorBE objProveedor)
        {
            int id = 0;
             //Database dtb = DatabaseFactory.CreateDatabase("SEC");
            
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_PROVEEDOR",conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Rubro_Id", objProveedor.Rubro_Id));
                cmd.Parameters.Add(new SqlParameter("@Tipopersona_Id", objProveedor.Tipopersona_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Homologado_Fl", objProveedor.Prv_Homologado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Prv_Estado_Homologado_Id", objProveedor.Prv_Estado_Homologado_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Homologadora_Id", objProveedor.Prv_Homologadora_Id));
                //cmd.Parameters.Add(new SqlParameter("@Prv_Estado_Id", objProveedor.Prv_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx", objProveedor.Prv_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_AMaterno_tx", objProveedor.Prv_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objProveedor.Prv_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_RazonSocial_Tx", objProveedor.Prv_RazonSocial_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Personacontacto_Tx", objProveedor.Prv_Personacontacto_Tx));
                cmd.Parameters.Add(new SqlParameter("@TipDocIdent_Id", objProveedor.TipDocIdent_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_DocIdent_nu", objProveedor.Prv_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nacimiento_Fe", objProveedor.Prv_Nacimiento_Fe));
                cmd.Parameters.Add(new SqlParameter("@Prv_Direccion_tx", objProveedor.Prv_Direccion_tx));
                cmd.Parameters.Add(new SqlParameter("@Ubigeo_Id", objProveedor.Ubigeo_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Telefono1_nu", objProveedor.Prv_Telefono1_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Telefono2_nu", objProveedor.Prv_Telefono2_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Email_tx", objProveedor.Prv_Email_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Cla_Id", objProveedor.Prv_Cla_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objProveedor.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int,32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Listar Proveedor en Tabla
        public DataTable ListarProveedor(ProveedorBE objProveedor)
        {
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            //Database dtb = DatabaseFactory.CreateDatabase("SEC");

            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objProveedor.Prv_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx", objProveedor.Prv_APaterno_tx));

                //Abrir conexion
                conn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
                /*SqlDataReader rdr = cmd.ExecuteReader();
                conn.Close();

                while (rdr.Read())
                {
                    objProveedor = new ProveedorBE();

                    objProveedor.Prv_Nombres_tx = rdr["Prv_Nombres_tx"].ToString();
                    objProveedor.Prv_APaterno_tx = rdr["Prv_APaterno_tx"].ToString();
                    objProveedor.Prv_AMaterno_tx = rdr["Prv_AMaterno_tx"].ToString();
                    objProveedor.Prv_Direccion_tx = rdr["Prv_Direccion_tx"].ToString();
                    objProveedor.Prv_Email_tx = rdr["Prv_Email_tx"].ToString();

                    lstProveedor.Add(objProveedor);
                }

                return lstProveedor;*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Modificar Proveedor
        public void ModificarProveedor(ProveedorBE objProveedor) {

            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedor.Prv_Id));
                cmd.Parameters.Add(new SqlParameter("@Rubro_Id", objProveedor.Rubro_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Homologado_Fl", objProveedor.Prv_Homologado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Prv_Estado_Homologado_Id",objProveedor.Prv_Estado_Homologado_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Homologadora_Id", objProveedor.Prv_Homologadora_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx",objProveedor.Prv_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_AMaterno_tx", objProveedor.Prv_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objProveedor.Prv_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_RazonSocial_Tx", objProveedor.Prv_RazonSocial_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Personacontacto_Tx", objProveedor.Prv_Personacontacto_Tx));
                cmd.Parameters.Add(new SqlParameter("@TipDocIdent_Id", objProveedor.TipDocIdent_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_DocIdent_nu", objProveedor.Prv_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nacimiento_Fe", objProveedor.Prv_Nacimiento_Fe));
                cmd.Parameters.Add(new SqlParameter("@Prv_Direccion_tx", objProveedor.Prv_Direccion_tx));
                cmd.Parameters.Add(new SqlParameter("@Ubigeo_Id", objProveedor.Ubigeo_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Telefono1_nu", objProveedor.Prv_Telefono1_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Telefono2_nu", objProveedor.Prv_Telefono2_nu));
                cmd.Parameters.Add(new SqlParameter("@Prv_Email_tx", objProveedor.Prv_Email_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Cla_Id", objProveedor.Prv_Cla_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objProveedor.Aud_UsuarioActualizacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex) 
                {
                    throw ex; 
                }
        
        
        }

        //ListarProveedores
        public List<ProveedorBE> ListarProveedores(ProveedorBE objProveedor) {
            

            List<ProveedorBE> listarProveedor = new List<ProveedorBE>();
            try {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedor.Prv_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objProveedor.Prv_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx", objProveedor.Prv_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_AMaterno_tx", objProveedor.Prv_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_RazonSocial_Tx", objProveedor.Prv_RazonSocial_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Personacontacto_Tx", objProveedor.Prv_Personacontacto_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_DocIdent_nu", objProveedor.Prv_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@lstProveedores", objProveedor.Lst_Proveedor));
                cmd.Parameters.Add(new SqlParameter("@isCombo", 0));

                //Abrir conexion
                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objProveedor = new ProveedorBE();

                    
                    /*objProveedor.Prv_RazonSocial_Tx = rdr["Prv_RazonSocial_Tx"].ToString();
                    objProveedor.Prv_Nombres_tx = rdr["Prv_Nombres_tx"].ToString();
                    objProveedor.Prv_APaterno_tx = rdr["Prv_APaterno_tx"].ToString();
                    objProveedor.Prv_AMaterno_tx = rdr["Prv_AMaterno_tx"].ToString();
                    objProveedor.Prv_Direccion_tx = rdr["Prv_Direccion_tx"].ToString();
                    objProveedor.Prv_Email_tx = rdr["Prv_Email_tx"].ToString();
                    objProveedor.Prv_DocIdent_nu = rdr["Prv_DocIdent_nu"].ToString();
                    objProveedor.Tipopersona_Id = Int32.Parse(rdr["Tipopersona_Id"].ToString());
                    objProveedor.Prv_Personacontacto_Tx = rdr["Prv_Personacontacto_Tx"].ToString();
                    objProveedor.TipDocIdent_Id = Int32.Parse(rdr["TipDocIdent_Id"].ToString());
                    //objProveedor.Prv_Nacimiento_Fe = DateTime.TryParse(rdr["Prv_Nacimiento_Fe"].ToString(),);
                    DateTime tempDate;
                    if(! DateTime.TryParse(rdr["Prv_Nacimiento_Fe"].ToString(),out tempDate))
                    {
                        objProveedor.Prv_Nacimiento_Fe = null;
                    }
                    else { objProveedor.Prv_Nacimiento_Fe = tempDate; }
                    objProveedor.Departamento_Co = Int32.Parse(rdr["Departamento_Co"].ToString());
                    objProveedor.Provincia_co = Int32.Parse(rdr["Provincia_co"].ToString());
                    objProveedor.Distrito_co = Int32.Parse(rdr["Distrito_co"].ToString());

                    objProveedor.Prv_Telefono1_nu = rdr["Prv_Telefono1_nu"].ToString();
                    objProveedor.Prv_Telefono2_nu = rdr["Prv_Telefono2_nu"].ToString();
                    objProveedor.Prv_Cla_Id = Int32.Parse(rdr["Prv_Cla_Id"].ToString());
                    objProveedor.Prv_Homologado_Fl = bool.Parse(rdr["Prv_Homologado_Fl"].ToString());
                    objProveedor.Prv_Estado_Homologado_Id =Int32.Parse(rdr["Prv_Estado_Homologado_Id"].ToString());
                    objProveedor.Prv_Homologadora_Id =Int32.Parse(rdr["Prv_Homologadora_Id"].ToString());
                    objProveedor.Prv_Estado_Id = Int32.Parse(rdr["Prv_Estado_Id"].ToString());
                    objProveedor.Prv_Id = Int32.Parse(rdr["Prv_Id"].ToString());*/

                    //listarProveedor.Add(objProveedor);
                    listarProveedor.Add(new EntityMapper<ProveedorBE>(rdr, objProveedor).FillEntity());
                }

                conn.Close();
                return listarProveedor;
            }

            catch (Exception ex) { throw ex; }
        }

        //Eliminar Proveedor
        public void EliminarProveedor(ProveedorBE objProveedor) {

            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedor.Prv_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objProveedor.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objProveedor.Aud_UsuarioEliminacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex) { throw ex; }
        
        }
        public List<ProveedorBE> ListarComboProveedores(ProveedorBE objProveedor)
        {


            List<ProveedorBE> listarProveedor = new List<ProveedorBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedor.Prv_Id));
                cmd.Parameters.Add(new SqlParameter("@Prv_Nombres_tx", objProveedor.Prv_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_APaterno_tx", objProveedor.Prv_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_AMaterno_tx", objProveedor.Prv_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_RazonSocial_Tx", objProveedor.Prv_RazonSocial_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_Personacontacto_Tx", objProveedor.Prv_Personacontacto_Tx));
                cmd.Parameters.Add(new SqlParameter("@Prv_DocIdent_nu", objProveedor.Prv_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@lstProveedores", objProveedor.Lst_Proveedor));
                cmd.Parameters.Add(new SqlParameter("@isCombo", 1));

                //Abrir conexion
                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objProveedor = new ProveedorBE();
                    listarProveedor.Add(new EntityMapper<ProveedorBE>(rdr, objProveedor).FillEntity());
                }

                conn.Close();
                return listarProveedor;
            }

            catch (Exception ex) { throw ex; }
        }
    }
}
