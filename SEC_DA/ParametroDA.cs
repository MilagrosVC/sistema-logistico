﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class ParametroDA
        
    {
      String cnx=System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
    
        //Parametro Tipo de Documento
        //string PCa_Nombre_Tx = "DOCUMENTO_IDENTIDAD_TIPO";
        //Parametro Tipo de Documento
      //  string PCa_NombreTipoCl_Tx="PROVEEDOR_CLASIFICACION";

      //ListarParametro(TipoDocumento)
      //public List<ParametroBE> ListarParametro() 
      //{
      //    List<ParametroBE> lstParametro = new List<ParametroBE>();
      //    ParametroBE ObjParametro = new ParametroBE();

      //    try
      //    {
      //        SqlConnection conn = new SqlConnection(cnx);
      //        SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_SEC_PARAMETRO", conn);
      //        cmdsq.CommandType = CommandType.StoredProcedure;
      //        cmdsq.Parameters.Add(new SqlParameter("@PCa_Nombre_Tx", PCa_Nombre_Tx));
      //        conn.Open();

      //        SqlDataReader rdr = cmdsq.ExecuteReader();
      //        while (rdr.Read()) {
      //            ObjParametro = new ParametroBE();

      //            ObjParametro.Par_Id = Int32.Parse(rdr["Par_Id"].ToString());
      //            ObjParametro.Par_Padre_Id = Int32.Parse(rdr["Par_Padre_Id"].ToString());
      //            ObjParametro.Par_Nombre_Tx = rdr["Par_Nombre_Tx"].ToString();

      //            lstParametro.Add(ObjParametro);
      //        }
      //        conn.Close();
      //        return lstParametro;
          
      //    }
      //    catch (Exception ex)
      //    {
      //        throw ex;
      //    }

      //}


      public List<ParametroBE> ListarParametros(ParametroBE ObjetoParametroBE)
      {

          List<ParametroBE> listadeParametro = new List<ParametroBE>();
          try {

              SqlConnection conn = new SqlConnection(cnx);
              SqlCommand cmd = new SqlCommand("SEC_PRC_S_PARAMETRO", conn);
              cmd.CommandType = CommandType.StoredProcedure;
              cmd.Parameters.Add(new SqlParameter("@Par_Id",ObjetoParametroBE.Par_Id));
              cmd.Parameters.Add(new SqlParameter("@Par_Padre_Id", ObjetoParametroBE.Par_Padre_Id));
              cmd.Parameters.Add(new SqlParameter("@Par_Nombre_Tx", ObjetoParametroBE.Par_Nombre_Tx));
              cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", ObjetoParametroBE.Aud_Estado_Fl));

              conn.Open();

              SqlDataReader rdr = cmd.ExecuteReader();
              while (rdr.Read()) {
                  ObjetoParametroBE = new ParametroBE();
                  ObjetoParametroBE.Par_Id = Int32.Parse(rdr["Par_Id"].ToString());
                  ObjetoParametroBE.Par_Padre_Id = Int32.Parse(rdr["Par_Padre_Id"].ToString());
                  ObjetoParametroBE.Par_Nombre_Tx = rdr["Par_Nombre_Tx"].ToString();
                  ObjetoParametroBE.Aud_Estado_Fl = bool.Parse(rdr["Aud_Estado_Fl"].ToString());

                  listadeParametro.Add(ObjetoParametroBE);
                  
              }
              conn.Close();
             return listadeParametro;
          }

          catch (Exception e) { throw e; }


         
      }
    }
}
