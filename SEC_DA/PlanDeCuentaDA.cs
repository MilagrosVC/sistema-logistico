﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class PlanDeCuentaDA
    {
        /* Cadena de conexion */
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        /* Listar Plan de Cuenta */
        public List<PlanDeCuentaBE> ListarPlanDeCuenta(PlanDeCuentaBE objPlanDeCuenta) {
            List<PlanDeCuentaBE> listarPlanDeCuenta = new List<PlanDeCuentaBE>();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PLAN_DE_CUENTA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de los parametros
                cmd.Parameters.Add(new SqlParameter("@PlanCuenta_Id", objPlanDeCuenta.PlanCuenta_Id));
                //Actualmente la busqueda se realiza por estos tres parametros
                cmd.Parameters.Add(new SqlParameter("@Empresa_Id", objPlanDeCuenta.Empresa_Id));
                cmd.Parameters.Add(new SqlParameter("@Pla_Nombre_Tx",objPlanDeCuenta.Pla_Nombre_Tx));
                
                conn.Open(); //Abrir la conexion
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read()) {
                    objPlanDeCuenta = new PlanDeCuentaBE();
                    listarPlanDeCuenta.Add(new EntityMapper<PlanDeCuentaBE>(rdr, objPlanDeCuenta).FillEntity());
                }
                conn.Close(); //Cerrar la conexion
                return listarPlanDeCuenta; // Retorna la lista
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
    }
}
