﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class SolicitudDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearSolicitudDetalle(SolicitudDeBE objSolicitudDe)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_SOLICITUD_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudDe.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Item_Id", objSolicitudDe.Item_Id));
                cmd.Parameters.Add(new SqlParameter("@SDe_Cantidad_Nu", objSolicitudDe.SDe_Cantidad_Nu));
                cmd.Parameters.Add(new SqlParameter("@SDe_Descripcion_Tx", objSolicitudDe.SDe_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Unidad_Medida_Id", objSolicitudDe.Unidad_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudDe.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SolicitudDeBE> ListarSolicitudDetalle(SolicitudDeBE objSolicitudDeBE)
        {
            List<SolicitudDeBE> lstSolicitudDe = new List<SolicitudDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_SOLICITUD_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudDeBE.Solicitud_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objSolicitudDeBE = new SolicitudDeBE();
                    lstSolicitudDe.Add(new EntityMapper<SolicitudDeBE>(rdr, objSolicitudDeBE).FillEntity());
                }

                conn.Close();
                return lstSolicitudDe;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditarSolicitudDetalle(SolicitudDeBE objSolicitudDe)
        {
            //int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_SOLICITUD_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudDe.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Detalle_Id", objSolicitudDe.Solicitud_Detalle_Id));
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Detalle_Id", objSolicitudDe.Requerimiento_Detalle_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudDe.Aud_UsuarioCreacion_Id));

                /*cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;*/

                conn.Open();
                cmd.ExecuteNonQuery();

                //id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                //return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
