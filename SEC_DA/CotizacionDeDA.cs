﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class CotizacionDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<CotizacionDeBE> ListarCotizacionDetalle(CotizacionDeBE ObjCotizacionDe)
        {
            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COTIZACION_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionDe.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Detalle_Id", ObjCotizacionDe.Cotizacion_Detalle_Id));
                //cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjCotizacion.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjCotizacionDe = new CotizacionDeBE();
                    lstCotizacionDe.Add(new EntityMapper<CotizacionDeBE>(rdr, ObjCotizacionDe).FillEntity());
                }

                conn.Close();
                return lstCotizacionDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDetalleCotizacion(CotizacionDeBE ObjCotizacionDe)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_COTIZACION_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionDe.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Detalle_Id", ObjCotizacionDe.Cotizacion_Detalle_Id));
                cmd.Parameters.Add(new SqlParameter("@CDe_Descripcion_Tx", ObjCotizacionDe.CDe_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@CDe_Cantidad_Nu", ObjCotizacionDe.CDe_Cantidad_Nu));
                cmd.Parameters.Add(new SqlParameter("@CDe_Und_Medida_Id", ObjCotizacionDe.CDe_Und_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@CDe_Precio_Unitario_Nu", ObjCotizacionDe.CDe_Precio_Unitario_Nu));
                cmd.Parameters.Add(new SqlParameter("@CDe_Sub_Total_Nu", ObjCotizacionDe.CDe_Sub_Total_Nu));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjCotizacionDe.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@CDe_Estado_Itm_Id", ObjCotizacionDe.CDe_Estado_Itm_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<CotizacionDeBE> GenerarVistaCotizacionDetalle(CotizacionDeBE ObjCotizacionDe){

            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COTIZACION_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionDe.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Detalle_Id", ObjCotizacionDe.Cotizacion_Detalle_Id));
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjCotizacionDe = new CotizacionDeBE();
                    lstCotizacionDe.Add(new EntityMapper<CotizacionDeBE>(rdr, ObjCotizacionDe).FillEntity());
                }

                conn.Close();
                return lstCotizacionDe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
