﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;

namespace SEC_DA
{
    public class EgresoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;


        public int CrearEgreso(EgresoBE ObjEgreso) {
            int id = 0;

            try {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_I_EGRESO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", ObjEgreso.Solicitud_Anticipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Ege_NumeroCheque", ObjEgreso.Ege_NumeroCheque));
                cmd.Parameters.Add(new SqlParameter("@Ege_NumeroOperacion", ObjEgreso.Ege_NumeroOperacion));
                cmd.Parameters.Add(new SqlParameter("@Ege_TipoCambio", ObjEgreso.Ege_TipoCambio));
                cmd.Parameters.Add(new SqlParameter("@BancoOrigen_Id", ObjEgreso.BancoOrigen_Id));
                cmd.Parameters.Add(new SqlParameter("@BancoDestino_Id", ObjEgreso.BancoDestino_Id));
                cmd.Parameters.Add(new SqlParameter("@Ege_CuentaOrigen", ObjEgreso.Ege_CuentaOrigen));
                cmd.Parameters.Add(new SqlParameter("@Ege_CuentaDestino", ObjEgreso.Ege_CuentaDestino));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjEgreso.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;
                conn.Open();
                cmd.ExecuteNonQuery();
                
                id = (int)cmd.Parameters["@ID"].Value;
                conn.Close();
                return id;
            }

            catch (Exception ex) 
                { 
                    throw ex; 
                }
        
        }


        public List<EgresoBE> ListarEgreso(EgresoBE ObjEgreso)
        {

            List<EgresoBE> lstEgreso = new List<EgresoBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_EGRESO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Egreso_Id", ObjEgreso.Egreso_Id));
                //BANCOS
                cmd.Parameters.Add(new SqlParameter("@BancoOrigen_Id", ObjEgreso.BancoOrigen_Id));
                cmd.Parameters.Add(new SqlParameter("@BancoDestino_Id", ObjEgreso.BancoDestino_Id));
                
                ///PERTENECEN A OTRAS TABLAS
                //Luego se buscara por numero de cotizacion y numero de solicitud de Anticipo
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjEgreso.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", ObjEgreso.Solicitud_Anticipo_Id));

                //Actualmente la busqueda se realiza por estos tres parametros
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", ObjEgreso.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@FormaPago_Id", ObjEgreso.FormaPago_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Tipo_Anticipo_Id", ObjEgreso.Sol_Tipo_Anticipo_Id));
                
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjEgreso = new EgresoBE();
                    lstEgreso.Add(new EntityMapper<EgresoBE>(rdr, ObjEgreso).FillEntity());
                }
                conn.Close();
                return lstEgreso;
            }

            catch (Exception ex) { throw ex; }


        }

        public void ModificarEgreso(EgresoBE ObjEgreso) {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_EGRESO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Egreso_Id", ObjEgreso.Egreso_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", ObjEgreso.Solicitud_Anticipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Ege_NumeroCheque", ObjEgreso.Ege_NumeroCheque));
                cmd.Parameters.Add(new SqlParameter("@Ege_NumeroOperacion", ObjEgreso.Ege_NumeroOperacion));
                cmd.Parameters.Add(new SqlParameter("@Ege_TipoCambio", ObjEgreso.Ege_TipoCambio));
                cmd.Parameters.Add(new SqlParameter("@BancoOrigen_Id", ObjEgreso.BancoOrigen_Id));
                cmd.Parameters.Add(new SqlParameter("@BancoDestino_Id", ObjEgreso.BancoDestino_Id));
                cmd.Parameters.Add(new SqlParameter("@Ege_CuentaOrigen", ObjEgreso.Ege_CuentaOrigen));
                cmd.Parameters.Add(new SqlParameter("@Ege_CuentaDestino", ObjEgreso.Ege_CuentaDestino));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjEgreso.Aud_UsuarioActualizacion_Id));
                //cmd.Parameters.Add(new SqlParameter("", ObjEgreso.));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
        }
    }
}
