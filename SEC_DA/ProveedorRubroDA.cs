﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class ProveedorRubroDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;


        public List<RubroBE> ListarRubroDisponible(ProveedorRubroBE objProveedorRubro)
        {
            List<RubroBE> ListaRubroDisponible = new List<RubroBE>();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDORRUBRO_DISPONIBLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedorRubro.Prv_Id));
                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read()) {
                    RubroBE ObjRubro = new RubroBE();
                    ObjRubro.Rubro_Id = Int32.Parse(rdr["Rubro_Id"].ToString());
                    ObjRubro.Rub_Nombre_Tx = rdr["Rub_Nombre_Tx"].ToString();
                    ListaRubroDisponible.Add(ObjRubro);
                
                }
                conn.Close();
                return ListaRubroDisponible;
            }
            catch(Exception ex) {
                throw (ex);
            }
        
        }

        public List<RubroBE> ListarRubroAsignado(ProveedorRubroBE ObjProveedorRubro) {
            List<RubroBE> ListaRubroAsignado = new List<RubroBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDORRUBRO_ASIGNADO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Prv_Id", ObjProveedorRubro.Prv_Id));
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    RubroBE ObjRubro = new RubroBE();
                    ObjRubro.Rubro_Id = Int32.Parse(rdr["Rubro_Id"].ToString());
                    ObjRubro.Rub_Nombre_Tx = rdr["Rub_Nombre_Tx"].ToString();

                    ListaRubroAsignado.Add(ObjRubro);

                }
                conn.Close();
                return ListaRubroAsignado;
            }
            catch(Exception ex) {
                throw (ex);

            }

        }


        public void AsignarDesignarRubros(ProveedorRubroBE ObjProveedorRubroBE) 
        {
            try {
                SqlConnection cnn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_RUBROSASIGNACION", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Prv_Id",ObjProveedorRubroBE.Prv_Id));
                cmd.Parameters.Add(new SqlParameter("@Rubro_Id", ObjProveedorRubroBE.Rubro_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", ObjProveedorRubroBE.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjProveedorRubroBE.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjProveedorRubroBE.Aud_UsuarioCreacion_Id));
                cnn.Open();
                cmd.ExecuteNonQuery();
                cnn.Close();
            }

            catch(Exception ex) {
                throw ex;
            }
        }


        //public List<ProveedorRubroBE> ListarProveedorRubro(ProveedorRubroBE objProveedorRubroBE)
        //{
        //    List<ProveedorRubroBE> objProveedorRubro = new List<ProveedorRubroBE>();
        //    try{
        //        SqlConnection conn = new SqlConnection(cnx);
        //        SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDOR_RUBROS", conn);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add(new SqlParameter("@Prv_Id", objProveedorRubroBE.Prv_Id));
        //        conn.Open();
        //        return;
        //    }
        //    catch
        //    {

        //    }
        //}
    }
}
