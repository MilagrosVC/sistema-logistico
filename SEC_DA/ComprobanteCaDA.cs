﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class ComprobanteCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearComprobanteCabecera(ComprobanteCaBE ObjComprobanteCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_COMPROBANTE_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjComprobanteCa.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjComprobanteCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", ObjComprobanteCa.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@Com_IGV_Nu", ObjComprobanteCa.Com_IGV_Nu));
                cmd.Parameters.Add(new SqlParameter("@Com_SubTotal_Nu", ObjComprobanteCa.Com_SubTotal_Nu));
                cmd.Parameters.Add(new SqlParameter("@Com_Total_Nu", ObjComprobanteCa.Com_Total_Nu));
                cmd.Parameters.Add(new SqlParameter("@Comprobante_Co", ObjComprobanteCa.Comprobante_Co));
                cmd.Parameters.Add(new SqlParameter("@Com_Guia_Remision_Nu", ObjComprobanteCa.Com_Guia_Remision_Nu));
                cmd.Parameters.Add(new SqlParameter("@Com_Emision_Fe", ObjComprobanteCa.Com_Emision_Fe));

                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjComprobanteCa.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ComprobanteCaBE ExisteFactura(ComprobanteCaBE ObjComprobanteCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_EXISTE_FACTURA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Comprobante_Co", ObjComprobanteCa.Comprobante_Co));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjComprobanteCa.Proveedor_Id));
                

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjComprobanteCa = new ComprobanteCaBE();
                    new EntityMapper<ComprobanteCaBE>(rdr, ObjComprobanteCa).FillEntity();
                }

                conn.Close();
                return ObjComprobanteCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ComprobanteCaBE> ListarComprobanteCa(ComprobanteCaBE ObjComprobanteCa)
        {
            List<ComprobanteCaBE> lstComprobanteCa = new List<ComprobanteCaBE>();

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COMPROBANTE_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Comprobante_Co", ObjComprobanteCa.Comprobante_Co));
                cmd.Parameters.Add(new SqlParameter("@fec_desde", ObjComprobanteCa.FechaDesde));
                cmd.Parameters.Add(new SqlParameter("@fec_hasta", ObjComprobanteCa.FechaHasta));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjComprobanteCa = new ComprobanteCaBE();
                    lstComprobanteCa.Add(new EntityMapper<ComprobanteCaBE>(rdr, ObjComprobanteCa).FillEntity());
                }

                conn.Close();
                return lstComprobanteCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ComprobanteCaBE GenerarVistaComprobante(ComprobanteCaBE ObjComprobanteCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VISTA_COMPROBANTE_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Comprobante_Id", ObjComprobanteCa.Comprobante_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjComprobanteCa = new ComprobanteCaBE();
                    new EntityMapper<ComprobanteCaBE>(rdr, ObjComprobanteCa).FillEntity();
                }

                conn.Close();
                return ObjComprobanteCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
