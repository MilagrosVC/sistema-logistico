﻿using System;
using SEC_BE;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class ElementoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;


        public List<ElementoBE> ListarElemento(ElementoBE ObjElemento) {
            List<ElementoBE> lstElemento = new List<ElementoBE>();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ELEMENTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                 conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read()) {
                    ObjElemento = new ElementoBE();
                    lstElemento.Add(new EntityMapper<ElementoBE>(rdr, ObjElemento).FillEntity());
                
                }
                conn.Close();
                return lstElemento;
            }
            catch (Exception ex){
                throw ex;            
            }

        
        }
    }
}
