﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class EstadoDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
        public List<EstadoDeBE> ListarEstados(EstadoDeBE ObjEstadoDe)
        {
            List<EstadoDeBE> lstEstadosDE = new List<EstadoDeBE>();
            try
            {

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ESTADO_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Estado_CA_Id", ObjEstadoDe.Estado_CA_Id));

                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjEstadoDe = new EstadoDeBE();
                    lstEstadosDE.Add(new EntityMapper<EstadoDeBE>(rdr, ObjEstadoDe).FillEntity());
                }
                conn.Close();
                return lstEstadosDE;
            }

            catch (Exception e) { throw e; }
        }

        public List<EstadoDeBE> ListarEstados(string EstadoCa)
        {
            EstadoDeBE objEstadoDeBE = new EstadoDeBE();
            List<EstadoDeBE> lstEstadosDE = new List<EstadoDeBE>();
            try
            {

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ESTADO_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ECa_Nombre_Tx", EstadoCa));

                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    objEstadoDeBE = new EstadoDeBE();
                    lstEstadosDE.Add(new EntityMapper<EstadoDeBE>(rdr, objEstadoDeBE).FillEntity());
                }
                conn.Close();
                return lstEstadosDE;
            }

            catch (Exception e) { throw e; }
        }
    }
}
