﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class SolicitudProveedorDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
        public int InsertarProveedorSolicitud(SolicitudProveedorBE objSolicitudPrv)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_SOLICITUD_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudPrv.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", objSolicitudPrv.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudPrv.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SolicitudProveedorBE> ListarProveedorSolicitud(SolicitudProveedorBE objSolicitudPrv)
        {
            List<SolicitudProveedorBE> lstSolicitudPrv = new List<SolicitudProveedorBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_SOLICITUD_PROVEEDOR", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudPrv.Solicitud_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objSolicitudPrv = new SolicitudProveedorBE();
                    lstSolicitudPrv.Add(new EntityMapper<SolicitudProveedorBE>(rdr, objSolicitudPrv).FillEntity());
                }

                conn.Close();
                return lstSolicitudPrv;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int EliminarProveedorSolicitud(SolicitudProveedorBE objSolicitudPrv)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_SOLICITUD_PROVEEDOR  ", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Prov_Id", objSolicitudPrv.Solicitud_Prov_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objSolicitudPrv.Aud_UsuarioEliminacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();

                id = 1;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
