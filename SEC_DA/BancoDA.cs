﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;

namespace SEC_DA
{
    public class BancoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
        //Listar Banco en Combo
        public List<BancoBE> ListarBanco(BancoBE objBanco) {
            List<BancoBE> lstBanco = new List<BancoBE>();
           
            
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_BANCO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while(rdr.Read()){
                    objBanco = new BancoBE();
                    lstBanco.Add(new EntityMapper<BancoBE>(rdr, objBanco).FillEntity());
                }
                conn.Close();
                return lstBanco;
            }
            catch (Exception ex) {
                throw ex;
            }
        
        }
    }
}
