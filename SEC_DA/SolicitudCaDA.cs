﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class SolicitudCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearSolicitudCabecera(SolicitudCaBE objSolicitudCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_SOLICITUD_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitante_Id", objSolicitudCa.Solicitante_Id));
                cmd.Parameters.Add(new SqlParameter("@Centro_Costo_Id", objSolicitudCa.Centro_Costo_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Entrega_Fe", objSolicitudCa.Sol_Entrega_Fe));
                cmd.Parameters.Add(new SqlParameter("@Sol_Estado_Id", objSolicitudCa.Sol_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Tipo_Id", objSolicitudCa.Sol_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudCa.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CrearSolicitudCayDe(SolicitudCaBE objSolicitudCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_SOLICITUD_CA_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objSolicitudCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Entrega_Fe", objSolicitudCa.Sol_Entrega_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudCa.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@ItemsSol", objSolicitudCa.ItemsxSol));
                cmd.Parameters.Add(new SqlParameter("@ObsSol", objSolicitudCa.Sol_Observacion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Lst_Proveedor", objSolicitudCa.Lst_Proveedor));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SolicitudCaBE> ListarSolicitudes(SolicitudCaBE objSolicitudCaBE)
        {
            List<SolicitudCaBE> lstSolicitudCa = new List<SolicitudCaBE>();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_SOLICITUD_CA",conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objSolicitudCaBE.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objSolicitudCaBE.Solicitud_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objSolicitudCaBE = new SolicitudCaBE();
                    lstSolicitudCa.Add(new EntityMapper<SolicitudCaBE>(rdr, objSolicitudCaBE).FillEntity());
                }

                conn.Close();
                return lstSolicitudCa;
 
            }
            catch (Exception ex) {
                throw ex;
            }
        }
        
    }
}
