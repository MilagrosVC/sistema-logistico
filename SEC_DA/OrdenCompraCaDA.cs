﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class OrdenCompraCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearOrdenCompraCabecera(OrdenCompraCaBE objOrdenCompraCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_ORDEN_COMPRA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objOrdenCompraCa.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", objOrdenCompraCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Gestionador_Tx", objOrdenCompraCa.OCC_Gestionador_Tx));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Id", objOrdenCompraCa.OCC_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Estado_Id", objOrdenCompraCa.OCC_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Plazo_Entrega_Id", objOrdenCompraCa.OCC_Plazo_Entrega_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Forma_Pago_Id", objOrdenCompraCa.OCC_Forma_Pago_Id));
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", objOrdenCompraCa.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Documento_Entrega", objOrdenCompraCa.OCC_Tipo_Documento_Entrega));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Documento_Cancelacion", objOrdenCompraCa.OCC_Tipo_Documento_Cancelacion));
                cmd.Parameters.Add(new SqlParameter("@OCC_Penalidad_Fl", objOrdenCompraCa.OCC_Penalidad_Fl));
                cmd.Parameters.Add(new SqlParameter("@OCC_Total_Nu", objOrdenCompraCa.OCC_Total_Nu));
                cmd.Parameters.Add(new SqlParameter("@OCC_Emision_Fe", objOrdenCompraCa.OCC_Emision_Fe));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Cambio", objOrdenCompraCa.OCC_Tipo_Cambio));
                cmd.Parameters.Add(new SqlParameter("@Centro_Costo_Id", objOrdenCompraCa.Centro_Costo_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Penalidad_Nu", objOrdenCompraCa.OCC_Penalidad_Nu));
                cmd.Parameters.Add(new SqlParameter("@OCC_IGV_Nu", objOrdenCompraCa.OCC_IGV_Nu));
                cmd.Parameters.Add(new SqlParameter("@OCC_SubTotal_Nu", objOrdenCompraCa.OCC_SubTotal_Nu));

                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objOrdenCompraCa.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<OrdenCompraCaBE> ListarOrdenCompraCabecera(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            List<OrdenCompraCaBE> lstOrdenCompraCa = new List<OrdenCompraCaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ORDEN_COMPRA_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraCa.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Co", ObjOrdenCompraCa.Orden_Compra_Co));
                cmd.Parameters.Add(new SqlParameter("@OCC_Estado_Id", ObjOrdenCompraCa.OCC_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Id", ObjOrdenCompraCa.OCC_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjOrdenCompraCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@fec_desde", ObjOrdenCompraCa.FechaDesde));
                cmd.Parameters.Add(new SqlParameter("@fec_hasta", ObjOrdenCompraCa.FechaHasta));


                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraCa = new OrdenCompraCaBE();
                    lstOrdenCompraCa.Add(new EntityMapper<OrdenCompraCaBE>(rdr, ObjOrdenCompraCa).FillEntity());
                }

                conn.Close();
                return lstOrdenCompraCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public OrdenCompraCaBE GenerarVistaOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VISTA_ORDEN_COMPRA_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@OrdenCompra_Id", ObjOrdenCompraCa.Orden_Compra_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraCa = new OrdenCompraCaBE();
                    new EntityMapper<OrdenCompraCaBE>(rdr, ObjOrdenCompraCa).FillEntity();
                }

                conn.Close();
                return ObjOrdenCompraCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public OrdenCompraCaBE ObtenerOrdenCompra(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ORDEN_COMPRA_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraCa.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Co", ObjOrdenCompraCa.Orden_Compra_Co));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraCa = new OrdenCompraCaBE();
                    new EntityMapper<OrdenCompraCaBE>(rdr, ObjOrdenCompraCa).FillEntity();
                }

                conn.Close();
                return ObjOrdenCompraCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ActualizarOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_ORDEN_COMPRA_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraCa.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjOrdenCompraCa.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Anticipo_Fl", ObjOrdenCompraCa.Anticipo_Fl));


                conn.Open();
                cmd.ExecuteNonQuery();

                id = 1;//(int)cmd.Parameters["@ID"].Value;

                conn.Close();
            }
            catch (Exception ex)
            {
                id = 0;
                throw ex;
            }
            return id;
        }

        //CREADA MV 07 Agosto
        public OrdenCompraCaBE GenerarVistaOrdenCCA(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ORDEN_COMPRA_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Id", ObjOrdenCompraCa.Orden_Compra_Id));
                cmd.Parameters.Add(new SqlParameter("@Orden_Compra_Co", ObjOrdenCompraCa.Orden_Compra_Co));
                cmd.Parameters.Add(new SqlParameter("@OCC_Estado_Id", ObjOrdenCompraCa.OCC_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@OCC_Tipo_Id", ObjOrdenCompraCa.OCC_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjOrdenCompraCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@fec_desde", ObjOrdenCompraCa.FechaDesde));
                cmd.Parameters.Add(new SqlParameter("@fec_hasta", ObjOrdenCompraCa.FechaHasta));
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjOrdenCompraCa = new OrdenCompraCaBE();
                    new EntityMapper<OrdenCompraCaBE>(rdr, ObjOrdenCompraCa).FillEntity();
                }

                conn.Close();
                return ObjOrdenCompraCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        
    }
}
