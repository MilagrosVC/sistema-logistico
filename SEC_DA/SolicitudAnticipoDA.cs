﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class SolicitudAnticipoDA
    {
        /* Cadena de conexion */
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        /* Listar Solicitudes de Anticipo */
        public List<SolicitudAnticipoBE> ListarSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            List<SolicitudAnticipoBE> listarSolicitudAnticipo = new List<SolicitudAnticipoBE>();

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_SOLICITUD_ANTICIPO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de los parametros
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", objSolicitudAnticipo.Solicitud_Anticipo_Id));

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objSolicitudAnticipo.Cotizacion_Id));
                //Actualmente la busqueda se realiza por estos tres parametros
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", objSolicitudAnticipo.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@FormaPago_Id", objSolicitudAnticipo.FormaPago_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Tipo_Anticipo_Id", objSolicitudAnticipo.Sol_Tipo_Anticipo_Id));
                //NUEVO PARAMETRO 01 SEPTIEMBRE  
                cmd.Parameters.Add(new SqlParameter("@Per_Id", objSolicitudAnticipo.Per_Id));

                //Abrir la conexion
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    objSolicitudAnticipo = new SolicitudAnticipoBE();
                    listarSolicitudAnticipo.Add(new EntityMapper<SolicitudAnticipoBE>(rdr, objSolicitudAnticipo).FillEntity());
                }
                conn.Close();
                return listarSolicitudAnticipo;
            }
            catch (Exception ex) { throw ex; }
        }

        public int CrearSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_SOLICITUD_ANTICIPO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objSolicitudAnticipo.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Tipo_Anticipo_Id", objSolicitudAnticipo.Sol_Tipo_Anticipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Monto_Solicitado_nu", objSolicitudAnticipo.Sol_Monto_Solicitado_nu));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objSolicitudAnticipo.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ModificarEstadoAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_SOLICITUD_ANTICIPO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", objSolicitudAnticipo.Solicitud_Anticipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Estado_Solicitud_Id", objSolicitudAnticipo.Estado_Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Sol_Observacion_tx", objSolicitudAnticipo.Sol_Observacion_tx));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objSolicitudAnticipo.Aud_UsuarioActualizacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex) { throw ex; }

        }

        public void EliminarRAP(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_SEC_SOLICITUD_ANTICIPO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", objSolicitudAnticipo.Solicitud_Anticipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objSolicitudAnticipo.Aud_UsuarioEliminacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex) { throw ex; }

        }

        public SolicitudAnticipoBE cargarRAP(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            SolicitudAnticipoBE BE = new SolicitudAnticipoBE();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_SOLICITUD_ANTICIPO_2", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de los parametros

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objSolicitudAnticipo.Cotizacion_Id));
                //cmd.Parameters.Add(new SqlParameter("@Solicitud_Anticipo_Id", objSolicitudAnticipo.Solicitud_Anticipo_Id));
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    objSolicitudAnticipo = new SolicitudAnticipoBE();
                    //new EntityMapper<SolicitudAnticipoBE>(rdr, objSolicitudAnticipo).FillEntity();
                    BE.Solicitud_Anticipo_Id = Convert.ToInt32(rdr["Solicitud_Anticipo_Id"]);
                    BE.Sol_Monto_Solicitado_nu = Convert.ToDecimal(rdr["Sol_Monto_Solicitado_nu"]);
                    BE.Sol_Tipo_Anticipo_Id = Convert.ToInt32(rdr["Sol_Tipo_Anticipo_Id"]);
                }
                conn.Close();
                return BE;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
