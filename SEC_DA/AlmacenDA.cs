﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SEC_BE;


namespace SEC_DA
{
    public class AlmacenDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;



        //Listar 
        public List<AlmacenBE> ListarAlmacen(AlmacenBE objAlmacen)
        {
            List<AlmacenBE> listarAlmacen = new List<AlmacenBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ALMACEN", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Almacen_Id", objAlmacen.Almacen_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Nombre_Tx", objAlmacen.Alm_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Tipo_Id", objAlmacen.Alm_Tipo_Id));
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    objAlmacen = new AlmacenBE();
                    listarAlmacen.Add(new EntityMapper<AlmacenBE>(rdr, objAlmacen).FillEntity());
                }
                conn.Close();
                return listarAlmacen;
            }

            catch (Exception ex) 
            { 
                throw ex; 
            }
        }




        //Crear Almacen
        public int CrearAlmacen(AlmacenBE objAlmacen)
        {
            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_I_ALMACEN", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Alm_Nombre_Tx", objAlmacen.Alm_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Tipo_Id", objAlmacen.Alm_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Abreviado_Tx", objAlmacen.Alm_Abreviado_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Descripcion_Tx", objAlmacen.Alm_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Telefono_Tx", objAlmacen.Alm_Telefono_Tx));
                cmd.Parameters.Add(new SqlParameter("@Ubigeo_Id", objAlmacen.Ubigeo_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objAlmacen.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objAlmacen.Proyecto_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Direccion_Tx", objAlmacen.Alm_Direccion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Responsable_Tx", objAlmacen.Alm_Responsable_Tx));


                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Eliminar Almacen
        public void EliminarAlmacen(AlmacenBE objAlmacen)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_ALMACEN", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Almacen_Id", objAlmacen.Almacen_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objAlmacen.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objAlmacen.Aud_UsuarioEliminacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex) { throw ex; }

        }

        //Modificar Almacen

        //Modificar Proveedor
        public void ModificarAlmacen(AlmacenBE objAlmacen)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_ALMACEN", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Almacen_Id", objAlmacen.Almacen_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Nombre_Tx", objAlmacen.Alm_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Tipo_Id", objAlmacen.Alm_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Abreviado_Tx", objAlmacen.Alm_Abreviado_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Descripcion_Tx", objAlmacen.Alm_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Telefono_Tx", objAlmacen.Alm_Telefono_Tx));
                cmd.Parameters.Add(new SqlParameter("@Ubigeo_Id", objAlmacen.Ubigeo_Id));
                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objAlmacen.Proyecto_Id));
                cmd.Parameters.Add(new SqlParameter("@Alm_Direccion_Tx", objAlmacen.Alm_Direccion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Alm_Responsable_Tx", objAlmacen.Alm_Responsable_Tx));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objAlmacen.Aud_UsuarioActualizacion_Id));
                
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
    }
}
