﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using SEC_BE;


namespace SEC_DA
{
    public class ValorizacionDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<ValorizacionDeBE> ListarItemsValorizacion(int idValorizacionCa)
        {
            List<ValorizacionDeBE> lstItemsValorizacion = new List<ValorizacionDeBE>();
            ValorizacionDeBE objValDeBe = new ValorizacionDeBE();

            try {

                SqlConnection cnn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VALORIZACION_DE", cnn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Valorizacion_Id", idValorizacionCa));

                cnn.Open();
                
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objValDeBe = new ValorizacionDeBE();

                    objValDeBe.Itm_Co = rdr["Itm_Co"].ToString(); ;
                    objValDeBe.VDe_Descripcion_Tx = rdr["VDe_Descripcion_Tx"].ToString(); ;
                    objValDeBe.UMe_Simbolo_Tx = rdr["UMe_Simbolo_Tx"].ToString(); ;
                    objValDeBe.VDe_Metrado_Nu = Decimal.Parse(rdr["VDe_Metrado_Nu"].ToString());//Convert.ToDouble(rdr["VDe_Metrado_Nu"]); ;
                    objValDeBe.Itm_Precio_Unitario_Nu = Decimal.Parse(rdr["Itm_Precio_Unitario_Nu"].ToString());//Convert.ToDouble(rdr["Itm_Precio_Unitario_Nu"]); ;
                    objValDeBe.VDe_Monto_Nu = Decimal.Parse(rdr["VDe_Monto_Nu"].ToString());//Convert.ToDouble(rdr["VDe_Monto_Nu"]); ;

                    lstItemsValorizacion.Add(objValDeBe);
                }

                cnn.Close();

                return lstItemsValorizacion;
            }
            catch (Exception ex) { 
                throw ex; 
            }
        }
    }
}
