﻿using System;
using System.Collections.Generic;
using SEC_BE;
using System.Data;
using System.Data.SqlClient;
namespace SEC_DA
{
   public class PerfilDetalleDA
    {
        
String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
public List<PerfilDetalleBE> ListarPerfilDetalleBE(UsuarioBE ObjUsuario)
        {UsuarioBE Obj=new UsuarioBE();
            Obj=ObjUsuario;
            List<PerfilDetalleBE> lstPerfilDetalleBE = new List<PerfilDetalleBE>();
            try {
               
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_MENU", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Usuario_Id", ObjUsuario.Usu_Id));            
                 
				conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    PerfilDetalleBE objPerfilDetalleBE= new PerfilDetalleBE();
                    lstPerfilDetalleBE.Add(new EntityMapper<PerfilDetalleBE>(rdr, objPerfilDetalleBE).FillEntity());
                }

                conn.Close();
                return lstPerfilDetalleBE;
 
            }
            catch (Exception ex) {
                throw ex;
            }
        }





    }
}
