﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class ProyectoDA
    {
        
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public DataTable ListarProyectos(ProyectoBE objProyecto)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROYECTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Empresa_Id", objProyecto.Empresa_Id));
                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyecto.Proyecto_Id));

                //Abrir conexion
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProyectoBE> ListarProyecto(int codEmpresa)
        {
                try
                {
                    ProyectoBE ObjProyecto = new ProyectoBE();
                    List<ProyectoBE> lstProyecto = new List<ProyectoBE>();

                    SqlConnection conn = new SqlConnection(cnx);
                    SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_PROYECTO", conn);
                    cmdsq.CommandType = CommandType.StoredProcedure;
                    cmdsq.Parameters.Add(new SqlParameter("@Empresa_Id", codEmpresa));
                    
                    conn.Open();

                    SqlDataReader rdr = cmdsq.ExecuteReader();
                    while (rdr.Read())
                    {
                        ObjProyecto = new ProyectoBE();

                        ObjProyecto.Proyecto_Id = Int32.Parse(rdr["Proyecto_Id"].ToString());
                        ObjProyecto.Empresa_Id = Int32.Parse(rdr["Empresa_Id"].ToString());
                        ObjProyecto.Emp_RazonSocial_Tx = rdr["Emp_RazonSocial_Tx"].ToString();
                        ObjProyecto.Pry_Nombre_Tx = rdr["Pry_Nombre_Tx"].ToString();
                        ObjProyecto.Pry_Direccion_Tx = rdr["Pry_Direccion_Tx"].ToString();
                        //ObjParametro.Par_Descripcion_Tx=rdr["Par_Descripcion_Tx"].ToString();
                        //ObjParametro.Par_Valor_Nu = Int32.Parse(rdr["Par_Valor_Nu"].ToString());

                        lstProyecto.Add(ObjProyecto);
                    }
                    conn.Close();
                    return lstProyecto;

                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }


        public ProyectoBE DatosProyecto(ProyectoBE objProyectoBE)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_PROYECTO", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                cmdsq.Parameters.Add(new SqlParameter("@Empresa_Id", objProyectoBE.Empresa_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyectoBE.Proyecto_Id));

                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    objProyectoBE = new ProyectoBE();
                    new EntityMapper<ProyectoBE>(rdr, objProyectoBE).FillEntity();
                }
                conn.Close();
                return objProyectoBE;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        //Listar en Combo Proyectos
        public List<ProyectoBE> ListarProyectoCombo(ProyectoBE objProyecto)
        {
            List<ProyectoBE> lstProyecto = new List<ProyectoBE>();

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROYECTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Empresa_Id", objProyecto.Empresa_Id));
                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyecto.Proyecto_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objProyecto = new ProyectoBE();
                    lstProyecto.Add(new EntityMapper<ProyectoBE>(rdr, objProyecto).FillEntity());
                }
                conn.Close();
                return lstProyecto;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
