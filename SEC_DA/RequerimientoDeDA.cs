﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class RequerimientoDeDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearRequerimientoDetalle(RequerimientoDeBE objRequerimientoDe)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_REQUERIMIENTO_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objRequerimientoDe.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Item_Id", objRequerimientoDe.Item_Id));
                cmd.Parameters.Add(new SqlParameter("@RDe_Cantidad_Nu", objRequerimientoDe.RDe_Cantidad_Nu));
                cmd.Parameters.Add(new SqlParameter("@RDe_Descripcion_Tx", objRequerimientoDe.RDe_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@RDe_Observacion_Tx", objRequerimientoDe.RDe_Observacion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Unidad_Medida_Id", objRequerimientoDe.Unidad_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@IMa_Id", objRequerimientoDe.IMa_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objRequerimientoDe.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<RequerimientoDeBE> ListarItemsxRequerimiento(RequerimientoDeBE objRequerimientoDe)
        {
            List<RequerimientoDeBE> lstRequerimientoDeBe = new List<RequerimientoDeBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ITEMS_REQUERIMIENTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objRequerimientoDe.Requerimiento_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objRequerimientoDe = new RequerimientoDeBE();
                    lstRequerimientoDeBe.Add(new EntityMapper<RequerimientoDeBE>(rdr, objRequerimientoDe).FillEntity());
                }

                conn.Close();
                return lstRequerimientoDeBe;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void EliminarRqDetalle(RequerimientoDeBE ObjRequerimientoDe)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_REQUERIMIENTO_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Detalle_Id", ObjRequerimientoDe.Requerimiento_Detalle_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", ObjRequerimientoDe.Aud_UsuarioEliminacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ActualizarRequerimientoDe(RequerimientoDeBE objRequerimientoDe)
        {
            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_REQUERIMIENTO_DETALLE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Detalle_Id", objRequerimientoDe.Requerimiento_Detalle_Id));
                cmd.Parameters.Add(new SqlParameter("@Item_Id", objRequerimientoDe.Item_Id));
                cmd.Parameters.Add(new SqlParameter("@RDe_Cantidad_Nu", objRequerimientoDe.RDe_Cantidad_Nu));
                cmd.Parameters.Add(new SqlParameter("@RDe_Descripcion_Tx", objRequerimientoDe.RDe_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@RDe_Observacion_Tx", objRequerimientoDe.RDe_Observacion_Tx));                
                cmd.Parameters.Add(new SqlParameter("@Unidad_Medida_Id", objRequerimientoDe.Unidad_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@IMa_Id", objRequerimientoDe.IMa_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objRequerimientoDe.Aud_UsuarioActualizacion_Id));

                //cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                //cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = 1;//(int)cmd.Parameters["@ID"].Value;

                conn.Close();
            }
            catch (Exception ex)
            {
                id = 0;
                throw ex;
            }
            return id;
        }
    }
}
