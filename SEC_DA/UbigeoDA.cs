﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Data.SqlClient;


namespace SEC_DA
{
    public class UbigeoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
       
        //Listar Ubigeo
        public List<UbigeoBE> ListarUbigeo(UbigeoBE objUbigeoBE)
        {
            List<UbigeoBE> lstUbigeo = new List<UbigeoBE>();
 
            try{
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_UBIGEO", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;

                cmdsq.Parameters.Add(new SqlParameter("@UbigeoId", objUbigeoBE.Ubigeo_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Nombre", objUbigeoBE.Ubi_Nombre_Tx));
                cmdsq.Parameters.Add(new SqlParameter("@CodigoUbigeo", objUbigeoBE.Ubigeo_Co));
                cmdsq.Parameters.Add(new SqlParameter("@EstadoActivo", objUbigeoBE.Aud_Estado_Fl));
                cmdsq.Parameters.Add(new SqlParameter("@CodigoDepartamento", objUbigeoBE.Departamento_Co));
                cmdsq.Parameters.Add(new SqlParameter("@CodigoProvicia", objUbigeoBE.Provincia_co));
                cmdsq.Parameters.Add(new SqlParameter("@CodigoDistrito", objUbigeoBE.Distrito_co));
            
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                
            
                while (rdr.Read())
                {
                    objUbigeoBE = new UbigeoBE();

                    objUbigeoBE.Ubigeo_Id = Int32.Parse(rdr["Ubigeo_Id"].ToString());
                    objUbigeoBE.Nombre = rdr["Nombre"].ToString();
                    objUbigeoBE.Departamento_Co = Int32.Parse(rdr["Departamento_Co"].ToString());
                    objUbigeoBE.Provincia_co = Int32.Parse(rdr["Provincia_co"].ToString());
                    objUbigeoBE.Distrito_co = Int32.Parse(rdr["Distrito_co"].ToString());
                    objUbigeoBE.Ubigeo_Co = rdr["Ubigeo_Co"].ToString();

                    lstUbigeo.Add(objUbigeoBE);
                }
                conn.Close();
                return lstUbigeo;

             }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //Obtener Codigo de Ubigeo
        
        public int ObtenerCodigoUbigeo(UbigeoBE objUbigeoBE)
        {
            //List<UbigeoBE> lstUbigeo = new List<UbigeoBE>();
            int id;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_CODIGOUBIGEO", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;

                cmdsq.Parameters.Add(new SqlParameter("@Departamento_Co", objUbigeoBE.Departamento_Co));
                cmdsq.Parameters.Add(new SqlParameter("@Provincia_co", objUbigeoBE.Provincia_co));
                cmdsq.Parameters.Add(new SqlParameter("@Distrito_co", objUbigeoBE.Distrito_co));

                cmdsq.Parameters.Add(new SqlParameter("@IdUbigeo", SqlDbType.Int, 32));
                cmdsq.Parameters["@IdUbigeo"].Direction = ParameterDirection.Output;

                conn.Open();

                cmdsq.ExecuteNonQuery();

                id = (int)cmdsq.Parameters["@IdUbigeo"].Value;

                conn.Close();

                return id;

                /*SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    objUbigeoBE = new UbigeoBE();

                    objUbigeoBE.Departamento_Co = Int32.Parse(rdr["Departamento_Co"].ToString());
                    objUbigeoBE.Provincia_co = Int32.Parse(rdr["Provincia_co"].ToString());
                    objUbigeoBE.Distrito_co = Int32.Parse(rdr["Distrito_co"].ToString());

                    lstUbigeo.Add(objUbigeoBE);
                    conn.Close();
                }
                return lstUbigeo;*/
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
