﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class TurnoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<TurnoBE> ListarTurno(TurnoBE ObjTurno)
        {
            try
            {
                List<TurnoBE> lstTurno = new List<TurnoBE>();

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_TURNO", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                //cmdsq.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjSubPartida.Sub_Partida_Id));
                //cmdsq.Parameters.Add(new SqlParameter("@Partida_Id", ObjSubPartida.Partida_Id));
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    ObjTurno = new TurnoBE();

                    ObjTurno.Turno_Id = Int32.Parse(rdr["Turno_Id"].ToString());
                    ObjTurno.Tur_Nombre_Tx = rdr["Tur_Nombre_Tx"].ToString();
                    ObjTurno.Tur_Descripcion_Tx = rdr["Tur_Descripcion_Tx"].ToString();
                    ObjTurno.Tur_Hora_Ini_Ti = TimeSpan.Parse(rdr["Tur_Hora_Ini_Ti"].ToString());
                    ObjTurno.Tur_Hora_Fin_Ti = TimeSpan.Parse(rdr["Tur_Hora_Fin_Ti"].ToString());
                    ObjTurno.Tur_Horas_Nu = Int32.Parse(rdr["Tur_Horas_Nu"].ToString());

                    lstTurno.Add(ObjTurno);
                }
                conn.Close();
                return lstTurno;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
