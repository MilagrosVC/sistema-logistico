﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class UsuarioDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<UsuarioBE> ListarUsuario(UsuarioBE objUsuarioBE)
        {
            List<UsuarioBE> lstUsuarioBE = new List<UsuarioBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_USUARIO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id_Codigo", objUsuarioBE.Usu_Id_Codigo));
                cmd.Parameters.Add(new SqlParameter("@Usu_Id", objUsuarioBE.Usu_Id));

                //cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", objUsuarioBE.Usu_Password_Tx));//
                cmd.Parameters.Add(new SqlParameter("@Usu_APaterno_tx", objUsuarioBE.Usu_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_AMaterno_tx", objUsuarioBE.Usu_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Nombres_tx", objUsuarioBE.Usu_Nombres_tx));
                //cmd.Parameters.Add(new SqlParameter("@Usu_Email_tx", objUsuarioBE.Usu_Email_tx)); //
                //cmd.Parameters.Add(new SqlParameter("@Usu_TipDocIdent_Id", objUsuarioBE.Usu_TipDocIdent_Id));//
                //cmd.Parameters.Add(new SqlParameter("@Usu_DocIdent_nu", objUsuarioBE.Usu_DocIdent_nu));//
                cmd.Parameters.Add(new SqlParameter("@Usu_Telefono_nu", objUsuarioBE.Usu_Telefono_nu));
                //cmd.Parameters.Add(new SqlParameter("@Usu_Direccion_tx", objUsuarioBE.Usu_Direccion_tx));//
                //cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabDes_nu", objUsuarioBE.Usu_HoraLabDes_nu));//
                //cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabHas_nu", objUsuarioBE.Usu_HoraLabHas_nu));//
                cmd.Parameters.Add(new SqlParameter("@Area_Id", objUsuarioBE.Area_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objUsuarioBE.Aud_UsuarioCreacion_Id));
                //cmd.Parameters.Add(new SqlParameter("@Aud_Creacion_Fe", objUsuarioBE.Aud_Creacion_Fe));//
                //cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objUsuarioBE.Aud_UsuarioActualizacion_Id));//
                //cmd.Parameters.Add(new SqlParameter("@Aud_Actualizacion_Fe", objUsuarioBE.Aud_Actualizacion_Fe));//
                //cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objUsuarioBE.Aud_UsuarioEliminacion_Id));//
                //cmd.Parameters.Add(new SqlParameter("@Aud_Eliminacion_Fe", objUsuarioBE.Aud_Eliminacion_Fe));//
                //cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objUsuarioBE.Aud_Estado_Fl));//
                cmd.Parameters.Add(new SqlParameter("@Per_Id", objUsuarioBE.Per_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objUsuarioBE = new UsuarioBE();
                    lstUsuarioBE.Add(new EntityMapper<UsuarioBE>(rdr, objUsuarioBE).FillEntity());
                }

                conn.Close();
                return lstUsuarioBE;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CrearUsuario(UsuarioBE objUsuarioBE)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_I_USUARIO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id_Codigo", objUsuarioBE.Usu_Id_Codigo));
                cmd.Parameters.Add(new SqlParameter("@Usu_Id", objUsuarioBE.Usu_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", objUsuarioBE.Usu_Password_Tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_APaterno_tx", objUsuarioBE.Usu_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_AMaterno_tx", objUsuarioBE.Usu_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Nombres_tx", objUsuarioBE.Usu_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Email_tx", objUsuarioBE.Usu_Email_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_TipDocIdent_Id", objUsuarioBE.Usu_TipDocIdent_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_DocIdent_nu", objUsuarioBE.Usu_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Telefono_nu", objUsuarioBE.Usu_Telefono_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Direccion_tx", objUsuarioBE.Usu_Direccion_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabDes_nu", objUsuarioBE.Usu_HoraLabDes_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabHas_nu", objUsuarioBE.Usu_HoraLabHas_nu));
                cmd.Parameters.Add(new SqlParameter("@Area_Id", objUsuarioBE.Area_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objUsuarioBE.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Creacion_Fe", objUsuarioBE.Aud_Creacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objUsuarioBE.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Actualizacion_Fe", objUsuarioBE.Aud_Actualizacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objUsuarioBE.Aud_UsuarioEliminacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Eliminacion_Fe", objUsuarioBE.Aud_Eliminacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objUsuarioBE.Aud_Estado_Fl));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void ActualizarUsuario(UsuarioBE objUsuarioBE)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_U_SEC_USUARIO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id_Codigo", objUsuarioBE.Usu_Id_Codigo));
                cmd.Parameters.Add(new SqlParameter("@Usu_Id", objUsuarioBE.Usu_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", objUsuarioBE.Usu_Password_Tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_APaterno_tx", objUsuarioBE.Usu_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_AMaterno_tx", objUsuarioBE.Usu_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Nombres_tx", objUsuarioBE.Usu_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Email_tx", objUsuarioBE.Usu_Email_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_TipDocIdent_Id", objUsuarioBE.Usu_TipDocIdent_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_DocIdent_nu", objUsuarioBE.Usu_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Telefono_nu", objUsuarioBE.Usu_Telefono_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Direccion_tx", objUsuarioBE.Usu_Direccion_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabDes_nu", objUsuarioBE.Usu_HoraLabDes_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabHas_nu", objUsuarioBE.Usu_HoraLabHas_nu));
                cmd.Parameters.Add(new SqlParameter("@Area_Id", objUsuarioBE.Area_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objUsuarioBE.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Creacion_Fe", objUsuarioBE.Aud_Creacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objUsuarioBE.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Actualizacion_Fe", objUsuarioBE.Aud_Actualizacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objUsuarioBE.Aud_UsuarioEliminacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Eliminacion_Fe", objUsuarioBE.Aud_Eliminacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objUsuarioBE.Aud_Estado_Fl));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public void EliminarUsuario(UsuarioBE objUsuarioBE)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_D_SEC_USUARIO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id_Codigo", objUsuarioBE.Usu_Id_Codigo));
                cmd.Parameters.Add(new SqlParameter("@Usu_Id", objUsuarioBE.Usu_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", objUsuarioBE.Usu_Password_Tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_APaterno_tx", objUsuarioBE.Usu_APaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_AMaterno_tx", objUsuarioBE.Usu_AMaterno_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Nombres_tx", objUsuarioBE.Usu_Nombres_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_Email_tx", objUsuarioBE.Usu_Email_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_TipDocIdent_Id", objUsuarioBE.Usu_TipDocIdent_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_DocIdent_nu", objUsuarioBE.Usu_DocIdent_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Telefono_nu", objUsuarioBE.Usu_Telefono_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_Direccion_tx", objUsuarioBE.Usu_Direccion_tx));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabDes_nu", objUsuarioBE.Usu_HoraLabDes_nu));
                cmd.Parameters.Add(new SqlParameter("@Usu_HoraLabHas_nu", objUsuarioBE.Usu_HoraLabHas_nu));
                cmd.Parameters.Add(new SqlParameter("@Area_Id", objUsuarioBE.Area_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objUsuarioBE.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Creacion_Fe", objUsuarioBE.Aud_Creacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objUsuarioBE.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Actualizacion_Fe", objUsuarioBE.Aud_Actualizacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objUsuarioBE.Aud_UsuarioEliminacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Eliminacion_Fe", objUsuarioBE.Aud_Eliminacion_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objUsuarioBE.Aud_Estado_Fl));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UsuarioBE> ValidarUsuarioLista(UsuarioBE ObjUsuario)
        {
            List<UsuarioBE> lstUsuario = new List<UsuarioBE>();

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VALIDAR_USUARIO_LISTA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id", ObjUsuario.Usu_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", ObjUsuario.Usu_Password_Tx));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjUsuario = new UsuarioBE();
                    lstUsuario.Add(new EntityMapper<UsuarioBE>(rdr, ObjUsuario).FillEntity());
                }
                conn.Close();
                return lstUsuario;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int ValidarUsuario(UsuarioBE ObjUsuario)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VALIDAR_USUARIO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Usu_Id", ObjUsuario.Usu_Id));
                cmd.Parameters.Add(new SqlParameter("@Usu_Password_Tx", ObjUsuario.Usu_Password_Tx));

                cmd.Parameters.Add(new SqlParameter("@Existe", SqlDbType.Int,32));
                cmd.Parameters["@Existe"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@Existe"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
