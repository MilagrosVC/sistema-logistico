﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SEC_BE;
using System.Data;
using System.Data.SqlClient;

namespace SEC_DA
{
   public class TTipoCambioDA
    {
       String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

       public List<TTipoCambioBE> ListarTTipoCambio(TTipoCambioBE ObjTTipoCambio) {
           List<TTipoCambioBE> lstTTipoCambio = new List<TTipoCambioBE>();
       try {
           SqlConnection conn = new SqlConnection(cnx);
           SqlCommand cmd = new SqlCommand("SEC_PRC_S_T_TIPO_CAMBIO", conn);
           cmd.CommandType = CommandType.StoredProcedure;

           conn.Open();
           SqlDataReader rdr = cmd.ExecuteReader();

           while (rdr.Read())
           {
               ObjTTipoCambio = new TTipoCambioBE();
               lstTTipoCambio.Add(new EntityMapper<TTipoCambioBE>(rdr, ObjTTipoCambio).FillEntity());
           }

           conn.Close();
           return lstTTipoCambio;
       }
       catch (Exception ex) { throw ex; }
       
       
       
       }
    }
}
