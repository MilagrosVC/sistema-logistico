﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SEC_DA
{
    public class RubroDA 
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        //Listar Rubro COMENTAR

        public List<RubroBE> ListarRubro() 
        {
            List<RubroBE> ListaRubro = new List<RubroBE>();
            RubroBE ObjRubro = new RubroBE();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_RUBRO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //LEER LOS PARAMETROS
                //cmd.Parameters.Add(new SqlParameter("@Rubro_Id",ObjRubro.Rubro_Id ));
                //cmd.Parameters.Add(new SqlParameter("@Rub_Nombre_Tx", ObjRubro.Rub_Nombre_Tx));

                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read()) 
                {
                    ObjRubro = new RubroBE();

                    ObjRubro.Rubro_Id = Int32.Parse(rdr["Rubro_Id"].ToString());
                    ObjRubro.Rub_Nombre_Tx = rdr["Rub_Nombre_Tx"].ToString();

                    ListaRubro.Add(ObjRubro);
                }

                conn.Close();
                return ListaRubro;
               }

            catch(Exception ex) {

                throw (ex);
                   }

        }




        public List<RubroBE> ListarRubroAsignado(int IdProveedor) {
            List<RubroBE> ListaRubroAsignado = new List<RubroBE>();
            //RubroBE ObjRubro = new RubroBE();
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROVEEDORRUBRO_ASIGNADO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Prv_Id", IdProveedor));

                 conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    RubroBE ObjRubro = new RubroBE();
                    ObjRubro.Rubro_Id = Int32.Parse(rdr["Rubro_Id"].ToString());
                    ObjRubro.Rub_Nombre_Tx = rdr["Rub_Nombre_Tx"].ToString();

                    ListaRubroAsignado.Add(ObjRubro);
                                    
                }
                conn.Close();
                return ListaRubroAsignado;
            }

            catch(Exception ex) {
                throw (ex);
            }
        
        }
    }
}
