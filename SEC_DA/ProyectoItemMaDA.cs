﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SEC_BE;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace SEC_DA
{
   public class ProyectoItemMaDA
    {
       String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;



       public List<Item_MaBE> ListarItemMaDisponible(ProyectoItemMaBE objProyectoItemMa)
       {
           List<Item_MaBE> ListaItemMaDisponible = new List<Item_MaBE>();
           try
           {
               SqlConnection conn = new SqlConnection(cnx);
               SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROYECTO_ITEM_MA_DISPONIBLE", conn);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyectoItemMa.Proyecto_Id));
               cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objProyectoItemMa.Aud_Estado_Fl));

               cmd.Parameters.Add(new SqlParameter("@IMa_Co", objProyectoItemMa.IMa_Co));
               cmd.Parameters.Add(new SqlParameter("@IMa_Nombre_Tx", objProyectoItemMa.IMa_Nombre_Tx));
               cmd.Parameters.Add(new SqlParameter("@Familia_Id", objProyectoItemMa.Familia_Id));
               cmd.Parameters.Add(new SqlParameter("@Marca_Id", objProyectoItemMa.Marca_Id));
               conn.Open();

               SqlDataReader rdr = cmd.ExecuteReader();

               while (rdr.Read())
               {
                   
                   Item_MaBE ObjItemMa = new Item_MaBE();
                   //ObjItemMa.IMa_Id = Int32.Parse(rdr["IMa_Id"].ToString());
                   //ObjItemMa.IMa_Nombre_Tx = rdr["IMa_Nombre_Tx"].ToString();
                   //ObjItemMa.IMa_Descripcion_Tx = rdr["IMa_Descripcion_Tx"].ToString();

                   
                   ListaItemMaDisponible.Add(new EntityMapper<Item_MaBE>(rdr, ObjItemMa).FillEntity());
                  
                   //lstItem_Ma.Add(new EntityMapper<Item_MaBE>(rdr, ObjItem_Ma).FillEntity());


                   //ListaItemMaDisponible.Add(ObjItemMa);

               }
               conn.Close();
               return ListaItemMaDisponible;
           }
           catch (Exception ex)
           {
               throw (ex);
           }

       }

      // public List<Item_MaBE> ListarItemMaAsignado(ProyectoItemMaBE objProyectoItemMa)
       public List<ProyectoItemMaBE> ListarItemMaAsignado(ProyectoItemMaBE objProyectoItemMa)
       {
          // List<Item_MaBE> ListaItemMaAsignado = new List<Item_MaBE>();
           List<ProyectoItemMaBE> ListaProyectoItemMaAsignado = new List<ProyectoItemMaBE>();
           try
           {
               SqlConnection conn = new SqlConnection(cnx);
               SqlCommand cmd = new SqlCommand("SEC_PRC_S_PROYECTO_ITEM_MA_ASIGNADO", conn);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyectoItemMa.Proyecto_Id));
               cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objProyectoItemMa.Aud_Estado_Fl));

               cmd.Parameters.Add(new SqlParameter("@IMa_Co", objProyectoItemMa.IMa_Co));
               cmd.Parameters.Add(new SqlParameter("@IMa_Nombre_Tx", objProyectoItemMa.IMa_Nombre_Tx));
               cmd.Parameters.Add(new SqlParameter("@Familia_Id", objProyectoItemMa.Familia_Id));
               cmd.Parameters.Add(new SqlParameter("@Marca_Id", objProyectoItemMa.Marca_Id));
               
               
               conn.Open();
               SqlDataReader rdr = cmd.ExecuteReader();
               while (rdr.Read())
               {
                   //Item_MaBE ObjItemMa = new Item_MaBE();
                   ProyectoItemMaBE ObjItemProyectoItem = new ProyectoItemMaBE();
                  // ListaItemMaAsignado.Add(new EntityMapper<Item_MaBE>(rdr, ObjItemMa).FillEntity());
                   ListaProyectoItemMaAsignado.Add(new EntityMapper<ProyectoItemMaBE>(rdr, ObjItemProyectoItem).FillEntity());
               }
               conn.Close();
              // return ListaItemMaAsignado;
               return ListaProyectoItemMaAsignado;
           }
           catch (Exception ex)
           {
               throw (ex);

           }

       }


       public void AsignarDesignarItemMa(ProyectoItemMaBE objProyectoItemMa) {
           try
           {
               SqlConnection cnn = new SqlConnection(cnx);
               SqlCommand cmd = new SqlCommand("SEC_PRC_U_ITEMMAASIGNACION", cnn);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyectoItemMa.Proyecto_Id));
               cmd.Parameters.Add(new SqlParameter("@IMa_Id", objProyectoItemMa.IMa_Id));

               cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objProyectoItemMa.Aud_Estado_Fl));
               cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objProyectoItemMa.Aud_UsuarioActualizacion_Id));
               cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objProyectoItemMa.Aud_UsuarioCreacion_Id));
               cnn.Open();
               cmd.ExecuteNonQuery();
               cnn.Close();

           }

           catch(Exception ex) {
               throw ex;
           }
       }




       public void AsignarDesignarItemMaTodos(ProyectoItemMaBE objProyectoItemMa)
       {
           try
           {
               SqlConnection cnn = new SqlConnection(cnx);
               SqlCommand cmd = new SqlCommand("SEC_PRC_U_ITEMMAASIGNACION_TODOS", cnn);
               cmd.CommandType = CommandType.StoredProcedure;
               cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objProyectoItemMa.Proyecto_Id));
               //cmd.Parameters.Add(new SqlParameter("@IMa_Id", objProyectoItemMa.IMa_Id));

               cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objProyectoItemMa.Aud_Estado_Fl));
               cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objProyectoItemMa.Aud_UsuarioActualizacion_Id));
               cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objProyectoItemMa.Aud_UsuarioCreacion_Id));
               cnn.Open();
               cmd.ExecuteNonQuery();
               cnn.Close();

           }

           catch (Exception ex)
           {
               throw ex;
           }
       }


       public void ModificarProyectoItem(ProyectoItemMaBE objProyectoItemMa) {
           try {
               SqlConnection conn = new SqlConnection(cnx);
               SqlCommand cmd = new SqlCommand("SEC_PRC_U_PROYECTO_ITEM_MA", conn);
               cmd.CommandType = CommandType.StoredProcedure;

               cmd.Parameters.Add(new SqlParameter("@Ite_Pro_Stock_Min", objProyectoItemMa.Ite_Pro_Stock_Min));
               cmd.Parameters.Add(new SqlParameter("@Ite_Pro_Id", objProyectoItemMa.Ite_Pro_Id));
               cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objProyectoItemMa.Aud_UsuarioActualizacion_Id));

               conn.Open();
               cmd.ExecuteNonQuery();
               conn.Close();
           }
           catch (Exception ex)
           {
               throw ex; 
           }
       
       }
   }
}
