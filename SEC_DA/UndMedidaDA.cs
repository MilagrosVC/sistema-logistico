﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class UndMedidaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<UndMedidaBE> ListarUnidadMedida(UndMedidaBE ObjUndMedida)
        {
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_UNIDAD_DE_MEDIDA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjCotizacion.Proveedor_Id));
                //cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjCotizacion.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjUndMedida = new UndMedidaBE();
                    lstUndMedida.Add(new EntityMapper<UndMedidaBE>(rdr, ObjUndMedida).FillEntity());
                }
                conn.Close();
                return lstUndMedida;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
