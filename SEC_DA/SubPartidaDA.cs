﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class SubPartidaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<SubPartidaBE> ListarSubPartida(SubPartidaBE ObjSubPartida)
        {
            try
            {
                //PartidaBE ObjPartida = new PartidaBE();
                List<SubPartidaBE> lstSubPartida = new List<SubPartidaBE>();

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_SUB_PARTIDA", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                cmdsq.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjSubPartida.Sub_Partida_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Partida_Id", ObjSubPartida.Partida_Id));
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    ObjSubPartida = new SubPartidaBE();

                    ObjSubPartida.Sub_Partida_Id = Int32.Parse(rdr["Sub_Partida_Id"].ToString());
                    ObjSubPartida.SPa_Nombre_Tx = rdr["SPa_Nombre_Tx"].ToString();
                    ObjSubPartida.SPa_Co = rdr["SPa_Co"].ToString();
                    ObjSubPartida.SPa_Descripcion_Tx = rdr["SPa_Descripcion_Tx"].ToString();
                    ObjSubPartida.Partida_Id = Int32.Parse(rdr["Partida_Id"].ToString());
                    
                    //ObjParametro.Par_Descripcion_Tx=rdr["Par_Descripcion_Tx"].ToString();
                    //ObjParametro.Par_Valor_Nu = Int32.Parse(rdr["Par_Valor_Nu"].ToString());

                    lstSubPartida.Add(ObjSubPartida);
                }
                conn.Close();
                return lstSubPartida;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
