﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class RequerimientoCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;
        

        public int CrearRequerimientoCabecera(RequerimientoCaBE objRequerimientoCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_REQUERIMIENTO_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitante_Id", objRequerimientoCa.Solicitante_Id));
                cmd.Parameters.Add(new SqlParameter("@Centro_Costo_Id", objRequerimientoCa.Centro_Costo_Id));
                cmd.Parameters.Add(new SqlParameter("@Req_Entrega_Fe", objRequerimientoCa.Req_Entrega_Fe));
                cmd.Parameters.Add(new SqlParameter("@Req_Tipo_Id", objRequerimientoCa.Req_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objRequerimientoCa.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objRequerimientoCa.Proyecto_Id));
                cmd.Parameters.Add(new SqlParameter("@Turno_Id", objRequerimientoCa.Turno_Id));
                cmd.Parameters.Add(new SqlParameter("@Partida_Id", objRequerimientoCa.Partida_Id));
                
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RequerimientoCaBE> CargarBandejaRQ(RequerimientoCaBE ObjRequerimientoCa)
        {
            List<RequerimientoCaBE> lstRequerimientoCaBe = new List<RequerimientoCaBE>();
            try{
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_BANDEJA_RQ", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", ObjRequerimientoCa.Proyecto_Id));
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Co", ObjRequerimientoCa.Requerimiento_Co));
                cmd.Parameters.Add(new SqlParameter("@Req_Solicitante", ObjRequerimientoCa.Req_Solicitante));
                cmd.Parameters.Add(new SqlParameter("@FechaDesde", ObjRequerimientoCa.FechaDesde));
                cmd.Parameters.Add(new SqlParameter("@FechaHasta", ObjRequerimientoCa.FechaHasta));
                
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjRequerimientoCa = new RequerimientoCaBE();
                    lstRequerimientoCaBe.Add(new EntityMapper<RequerimientoCaBE>(rdr, ObjRequerimientoCa).FillEntity());
                }

                conn.Close();
                return lstRequerimientoCaBe;
            }
            catch (Exception ex) {
                throw ex; 
            }
        }

        public int CambiarEstadoRequerimiento(RequerimientoCaBE objRequerimientoCa)
        {
            int id = 0;
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_ESTADO_REQUERIMIENTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objRequerimientoCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Req_Estado_Id", objRequerimientoCa.Req_Estado_Id));
                
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex) { 
                throw ex; 
            }
        }

        public List<RequerimientoCaBE> ListarRequerimientoCa(RequerimientoCaBE ObjRequerimientoCa)
        {
            List<RequerimientoCaBE> lstRequerimientoCa = new List<RequerimientoCaBE>();

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_REQUERIMIENTO_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", ObjRequerimientoCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitante_Id", ObjRequerimientoCa.Solicitante_Id));
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Co", ObjRequerimientoCa.Requerimiento_Co));
                cmd.Parameters.Add(new SqlParameter("@Req_Estado_Id", ObjRequerimientoCa.Req_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@fec_desde", ObjRequerimientoCa.FechaDesde));
                cmd.Parameters.Add(new SqlParameter("@fec_hasta", ObjRequerimientoCa.FechaHasta));


                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjRequerimientoCa = new RequerimientoCaBE();
                    lstRequerimientoCa.Add(new EntityMapper<RequerimientoCaBE>(rdr, ObjRequerimientoCa).FillEntity());
                }

                conn.Close();
                return lstRequerimientoCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RequerimientoCaBE GenerarVistaRequerimiento(RequerimientoCaBE objRequerimientoCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_VISTA_REQUERIMIENTO_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objRequerimientoCa.Requerimiento_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objRequerimientoCa = new RequerimientoCaBE();
                    new EntityMapper<RequerimientoCaBE>(rdr, objRequerimientoCa).FillEntity();
                }

                conn.Close();
                return objRequerimientoCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public RequerimientoCaBE ObtenerDatosRequerimiento(RequerimientoCaBE ObjRequerimientoCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_REQUERIMIENTO_CA_X_ID", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", ObjRequerimientoCa.Requerimiento_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjRequerimientoCa = new RequerimientoCaBE();
                    new EntityMapper<RequerimientoCaBE>(rdr, ObjRequerimientoCa).FillEntity();
                }

                conn.Close();
                return ObjRequerimientoCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ActualizarRequerimientoCa(RequerimientoCaBE objRequerimientoCa)
        {
            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_REQUERIMIENTO_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objRequerimientoCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitante_Id", objRequerimientoCa.Solicitante_Id));
                cmd.Parameters.Add(new SqlParameter("@Req_Estado_Id", objRequerimientoCa.Req_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Req_Tipo_Id", objRequerimientoCa.Req_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Req_Entrega_Fe", objRequerimientoCa.Req_Entrega_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objRequerimientoCa.Aud_UsuarioActualizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Turno_Id", objRequerimientoCa.Turno_Id));
                

                //cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                //cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = 1;//(int)cmd.Parameters["@ID"].Value;

                conn.Close();                
            }
            catch (Exception ex)
            {
                id = 0;
                throw ex;
            }
            return id;
        }
    }
}
