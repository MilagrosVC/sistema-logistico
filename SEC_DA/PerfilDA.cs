﻿using System;
using System.Collections.Generic;
using SEC_BE;
using System.Data;
using System.Data.SqlClient;
namespace SEC_DA
{
    public class PerfilDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<PerfilBE> ListarPerfiles(PerfilBE objPerfilBE)
        {

            List<PerfilBE> lstPerfilBE = new List<PerfilBE>();

            try
            {

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PERFIL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();

                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objPerfilBE = new PerfilBE();
                    lstPerfilBE.Add(new EntityMapper<PerfilBE>(rdr, objPerfilBE).FillEntity());
                }

                conn.Close();
                return lstPerfilBE;

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CrearPerfil(PerfilBE objPerfilBE)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_I_PERFIL", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //cmd.Parameters.Add(new SqlParameter("@Per_Id", objPerfilBE.Per_Id));
                cmd.Parameters.Add(new SqlParameter("@Per_Nombre_Tx", objPerfilBE.Per_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Per_Descripcion_Tx", objPerfilBE.Per_Descripcion_Tx));
                //cmd.Parameters.Add(new SqlParameter("@Per_Jefe_Id", objPerfilBE.Per_Jefe_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objPerfilBE.Aud_UsuarioCreacion_Id));
                //cmd.Parameters.Add(new SqlParameter("@Aud_Creacion_Fe", objPerfilBE.Aud_Creacion_Fe));
                //cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objPerfilBE.Aud_UsuarioActualizacion_Id));
                //cmd.Parameters.Add(new SqlParameter("@Aud_Actualizacion_Fe", objPerfilBE.Aud_Actualizacion_Fe));
                //cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objPerfilBE.Aud_UsuarioEliminacion_Id));
                //cmd.Parameters.Add(new SqlParameter("@Aud_Eliminacion_Fe", objPerfilBE.Aud_Eliminacion_Fe));
                //cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objPerfilBE.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();

                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarPerfil(PerfilBE objPerfilBE)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_U_PERFIL", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Per_Id", objPerfilBE.Per_Id));
                cmd.Parameters.Add(new SqlParameter("@Per_Nombre_Tx", objPerfilBE.Per_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Per_Descripcion_Tx", objPerfilBE.Per_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", objPerfilBE.Aud_UsuarioActualizacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarPerfil(PerfilBE objPerfilBE)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_D_PERFIL", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Per_Id", objPerfilBE.Per_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", objPerfilBE.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", objPerfilBE.Aud_UsuarioEliminacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
