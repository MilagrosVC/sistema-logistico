﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class CotizacionCaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public int CrearCotizacionCayDe(CotizacionCaBE objCotizacionCa)
        {
            int id = 0;

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_COTIZACION_CA_DE", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objCotizacionCa.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", objCotizacionCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Gestionador_Tx", objCotizacionCa.Cot_Gestionador_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_Tipo_Id", objCotizacionCa.Cot_Tipo_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Plazo_Entrega_Id", objCotizacionCa.Cot_Plazo_Entrega_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Forma_Pago_Id", objCotizacionCa.Cot_Forma_Pago_Id));
                cmd.Parameters.Add(new SqlParameter("@CDe_Total_Nu", objCotizacionCa.CDe_Total_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_NumProforma_Nu", objCotizacionCa.Cot_NumProforma_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_NomVendedor_Tx", objCotizacionCa.Cot_Prv_NomVendedor_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_TelVendedor_Nu", objCotizacionCa.Cot_Prv_TelVendedor_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_MailVendedor_Tx", objCotizacionCa.Cot_Prv_MailVendedor_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_Moneda_Id", objCotizacionCa.Cot_Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Monto_Nu", objCotizacionCa.Cot_Monto_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_IGV_Nu", objCotizacionCa.Cot_IGV_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_MontoTotal_Nu", objCotizacionCa.Cot_MontoTotal_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Detraccion_Nu", objCotizacionCa.Cot_Detraccion_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Observacion_Tx", objCotizacionCa.Cot_Observacion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_FechaEmision_Fe", objCotizacionCa.Cot_FechaEmision_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", objCotizacionCa.Aud_UsuarioCreacion_Id));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();

                id = (int)cmd.Parameters["@ID"].Value;

                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CotizacionCaBE> ListarCotizaciones(CotizacionCaBE objCotizacionCa)
        {
            List<CotizacionCaBE> lstCotizacionCa = new List<CotizacionCaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COTIZACION_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objCotizacionCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objCotizacionCa.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objCotizacionCa.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", objCotizacionCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Estado_Id", objCotizacionCa.Cot_Estado_Id));
                ///
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Co", objCotizacionCa.Requerimiento_Co)); //MV
                cmd.Parameters.Add(new SqlParameter("@Cot_NumProforma_Nu", objCotizacionCa.Cot_NumProforma_Nu)); //MV
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objCotizacionCa = new CotizacionCaBE();
                    lstCotizacionCa.Add(new EntityMapper<CotizacionCaBE>(rdr, objCotizacionCa).FillEntity());
                }

                conn.Close();
                return lstCotizacionCa;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CotizacionCaBE> ListarCotizacion(CotizacionCaBE ObjCotizacion)
        {
            List<CotizacionCaBE> lstCotizacion = new List<CotizacionCaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COTIZACION", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", ObjCotizacion.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjCotizacion.Proveedor_Id));
                //cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjCotizacion.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjCotizacion = new CotizacionCaBE();
                    lstCotizacion.Add(new EntityMapper<CotizacionCaBE>(rdr, ObjCotizacion).FillEntity());
                }

                conn.Close();
                return lstCotizacion;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void CambiarEstadoCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_COTIZACION_CA_ESTADO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", ObjCotizacionCa.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionCa.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Estado_Id", ObjCotizacionCa.Cot_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjCotizacionCa.Aud_UsuarioActualizacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SeleccionCotizacionGanadora(CotizacionCaBE ObjCotizacionCa)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_COTIZACION_CA_GANADORA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionCa.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Estado_Id", ObjCotizacionCa.Cot_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjCotizacionCa.Aud_UsuarioActualizacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarCotizacionCabecera(CotizacionCaBE ObjCotizacionCa) {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_COTIZACION_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", ObjCotizacionCa.Cotizacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjCotizacionCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Forma_Pago_Id", ObjCotizacionCa.Cot_Forma_Pago_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_NumProforma_Nu", ObjCotizacionCa.Cot_NumProforma_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_NomVendedor_Tx", ObjCotizacionCa.Cot_Prv_NomVendedor_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_TelVendedor_Nu", ObjCotizacionCa.Cot_Prv_TelVendedor_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Prv_MailVendedor_Tx", ObjCotizacionCa.Cot_Prv_MailVendedor_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_Moneda_Id", ObjCotizacionCa.Cot_Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Monto_Nu", ObjCotizacionCa.Cot_Monto_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_IGV_Nu", ObjCotizacionCa.Cot_IGV_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_MontoTotal_Nu", ObjCotizacionCa.Cot_MontoTotal_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Detraccion_Nu", ObjCotizacionCa.Cot_Detraccion_Nu));
                cmd.Parameters.Add(new SqlParameter("@Cot_Observacion_Tx", ObjCotizacionCa.Cot_Observacion_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cot_FechaEmision_Fe", ObjCotizacionCa.Cot_FechaEmision_Fe));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjCotizacionCa.Aud_UsuarioActualizacion_Id));

                conn.Open();
                cmd.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public CotizacionCaBE GenerarVistaCotizacionCA(CotizacionCaBE objCotizacionCa) {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_COTIZACION_CA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Id", objCotizacionCa.Requerimiento_Id));
                cmd.Parameters.Add(new SqlParameter("@Solicitud_Id", objCotizacionCa.Solicitud_Id));
                cmd.Parameters.Add(new SqlParameter("@Cotizacion_Id", objCotizacionCa.Cotizacion_Id));  //USADO PARA LA VISTA
                cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", objCotizacionCa.Proveedor_Id));
                cmd.Parameters.Add(new SqlParameter("@Cot_Estado_Id", objCotizacionCa.Cot_Estado_Id));
                cmd.Parameters.Add(new SqlParameter("@Requerimiento_Co", objCotizacionCa.Requerimiento_Co)); //MV
                cmd.Parameters.Add(new SqlParameter("@Cot_NumProforma_Nu", objCotizacionCa.Cot_NumProforma_Nu)); //MV
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    objCotizacionCa = new CotizacionCaBE();
                    new EntityMapper<CotizacionCaBE>(rdr, objCotizacionCa).FillEntity();
                }

                conn.Close();
                return objCotizacionCa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
