﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SEC_DA
{
    public class EntityMapper<T>
    {
        #region Variables Globales --------------------------------------

        IDataReader dr;
        T obj;

        #endregion
        #region Constructores -------------------------------------------

        public EntityMapper(IDataReader dr, T obj)
        {
            this.dr = dr;
            this.obj = obj;
        }

        #endregion
        #region Métodos -------------------------------------------------

        public T FillEntity()
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                try
                {
                    if (!dr.IsDBNull(i))
                    {
                        obj.GetType().GetProperty(dr.GetName(i), BindingFlags.Instance | BindingFlags.Public).SetValue(obj, dr.GetValue(i), null);
                    }
                }
                catch (NullReferenceException)
                {
                    continue;
                }
            }
            return obj;
        }

        #endregion
    }
}
