﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class ItemDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<ItemBE> ListarItem(ItemBE ObjItem)
        {
            try
            {
                //PartidaBE ObjPartida = new PartidaBE();
                List<ItemBE> lstItem = new List<ItemBE>();

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_ITEM", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                cmdsq.Parameters.Add(new SqlParameter("@Item_Id", ObjItem.Item_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjItem.Sub_Partida_Id));
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    ObjItem = new ItemBE();

                    ObjItem.Item_Id = Int32.Parse(rdr["Item_Id"].ToString());
                    ObjItem.Itm_Co = rdr["Itm_Co"].ToString();
                    ObjItem.Sub_Partida_Id = Int32.Parse(rdr["Sub_Partida_Id"].ToString());
                    ObjItem.Itm_Nombre_Tx = rdr["Itm_Nombre_Tx"].ToString();

                    lstItem.Add(ObjItem);
                }
                conn.Close();
                return lstItem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /*public List<ItemBE> ListarItemSubItem(ItemBE ObjItem)
        {
            try
            {
                //PartidaBE ObjPartida = new PartidaBE();
                List<ItemBE> lstItem = new List<ItemBE>();

                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmdsq = new SqlCommand("SEC_PRC_S_PRESUPUESTO_SUB_PARTIDA", conn);
                cmdsq.CommandType = CommandType.StoredProcedure;
                cmdsq.Parameters.Add(new SqlParameter("@Partida_Id", ObjItem.Item_Id));
                cmdsq.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjItem.Sub_Partida_Id));
                //cmdsq.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjItem.Sub_Partida_Id));
                conn.Open();

                SqlDataReader rdr = cmdsq.ExecuteReader();
                while (rdr.Read())
                {
                    ObjItem = new ItemBE();

                    ObjItem.Codigo = rdr["Codigo"].ToString();
                    ObjItem.Descripcion = rdr["Descripcion"].ToString();
                    ObjItem.Und = Int32.Parse(rdr["Und"].ToString());
                    ObjItem.Metrado = Double.Parse(rdr["Metrado"].ToString());
                    ObjItem.Precio = Double.Parse(rdr["Precio"].ToString());
                    ObjItem.Parcial = Double.Parse(rdr["Parcial"].ToString());
                    ObjItem.Item_Id = Int32.Parse(rdr["Item_Id"].ToString());

                    lstItem.Add(ObjItem);
                }
                conn.Close();
                return lstItem;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }*/

        /*public DataTable ListarItemSubItem(ItemBE ObjItem)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PRESUPUESTO_SUB_PARTIDA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Partida_Id", ObjItem.Item_Id));
                cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjItem.Sub_Partida_Id));

                //Abrir conexion
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }*/


        public List<ItemBE> ListarItemSubItem(ItemBE ObjItem)
        {
            List<ItemBE> lstItemSubItem = new List<ItemBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PRESUPUESTO_SUB_PARTIDA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Partida_Id", ObjItem.Item_Id));
                cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjItem.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjItem = new ItemBE();
                    lstItemSubItem.Add(new EntityMapper<ItemBE>(rdr, ObjItem).FillEntity());
                }

                conn.Close();
                return lstItemSubItem;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
