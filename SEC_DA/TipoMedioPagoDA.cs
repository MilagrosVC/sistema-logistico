﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class TipoMedioPagoDA

    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<TipoMedioPagoBE> ListarTipoMedioPago(TipoMedioPagoBE ObjTipoMedioPago) {
            List<TipoMedioPagoBE> lstTipoMedioPago = new List<TipoMedioPagoBE>();

            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_TIPO_MEDIO_PAGO", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjTipoMedioPago = new TipoMedioPagoBE();
                    lstTipoMedioPago.Add(new EntityMapper<TipoMedioPagoBE>(rdr, ObjTipoMedioPago).FillEntity());
                }
                conn.Close();
                return lstTipoMedioPago;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
