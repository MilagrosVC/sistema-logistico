﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class MonedaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<MonedaBE> ListarMoneda(MonedaBE ObjMoneda)
        {
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_S_MONEDA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //cmd.Parameters.Add(new SqlParameter("@Proveedor_Id", ObjCotizacion.Proveedor_Id));
                //cmd.Parameters.Add(new SqlParameter("@Sub_Partida_Id", ObjCotizacion.Sub_Partida_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    ObjMoneda = new MonedaBE();
                    lstMoneda.Add(new EntityMapper<MonedaBE>(rdr, ObjMoneda).FillEntity());
                }
                conn.Close();
                return lstMoneda;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
