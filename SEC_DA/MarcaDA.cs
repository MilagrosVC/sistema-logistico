﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class MarcaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;


        public List<MarcaBE> ListarMarca(MarcaBE ObjMarca)
        {

            List<MarcaBE> lstMarca = new List<MarcaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_MARCA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Marca_Id", ObjMarca.Marca_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjMarca = new MarcaBE();
                    lstMarca.Add(new EntityMapper<MarcaBE>(rdr, ObjMarca).FillEntity());

                }
                conn.Close();
                return lstMarca;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
