﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class FamiliaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;


        public List<FamiliaBE> ListarFamilia(FamiliaBE ObjFamilia)
        {

            List<FamiliaBE> lstFamilia = new List<FamiliaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_FAMILIA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Familia_Id", ObjFamilia.Familia_Id));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjFamilia = new FamiliaBE();
                    lstFamilia.Add(new EntityMapper<FamiliaBE>(rdr, ObjFamilia).FillEntity());

                }
                conn.Close();
                return lstFamilia;
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
