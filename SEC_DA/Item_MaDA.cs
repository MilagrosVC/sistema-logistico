﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class Item_MaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        //Listar
        public List<Item_MaBE> ListarItem_Ma(Item_MaBE ObjItem_Ma)
        {

            List<Item_MaBE> lstItem_Ma = new List<Item_MaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_ITEM_MA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.Add(new SqlParameter("@IMa_Nombre_Tx", ObjItem_Ma.IMa_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@Familia_Id", ObjItem_Ma.Familia_Id));
                cmd.Parameters.Add(new SqlParameter("@Marca_Id", ObjItem_Ma.Marca_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", ObjItem_Ma.Aud_Estado_Fl));
                //cmd.Parameters.Add(new SqlParameter("@Mantenimiento_Item", ObjItem_Ma.Mantenimiento_Item));

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    ObjItem_Ma = new Item_MaBE();
                    lstItem_Ma.Add(new EntityMapper<Item_MaBE>(rdr, ObjItem_Ma).FillEntity());

                }
                conn.Close();
                return lstItem_Ma;
            }
            catch (Exception ex) { throw ex; }
        }

        //Crear Item_MA

        public int CrearItemMA(Item_MaBE ObjItem_Ma)
        {
            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_I_ITEM_MA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@IMa_Nombre_Tx", ObjItem_Ma.IMa_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@IMa_Descripcion_Tx", ObjItem_Ma.IMa_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@IMa_Metrado_Nu", ObjItem_Ma.IMa_Metrado_Nu));
                cmd.Parameters.Add(new SqlParameter("@Unidad_Medida_Id", ObjItem_Ma.Unidad_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjItem_Ma.Aud_UsuarioCreacion_Id));
                cmd.Parameters.Add(new SqlParameter("@Familia_Id", ObjItem_Ma.Familia_Id));
                cmd.Parameters.Add(new SqlParameter("@Marca_Id", ObjItem_Ma.Marca_Id));
                cmd.Parameters.Add(new SqlParameter("@IMa_Co", ObjItem_Ma.IMa_Co));

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();
                id = (int)cmd.Parameters["@ID"].Value;
                conn.Close();
                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Eliminar ItemMA
        public void EliminarItemMA(Item_MaBE ObjItem_Ma)
        {

            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_D_ITEM_MA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@IMa_Id", ObjItem_Ma.IMa_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_Estado_Fl", ObjItem_Ma.Aud_Estado_Fl));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioEliminacion_Id", ObjItem_Ma.Aud_UsuarioEliminacion_Id));
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex) { throw ex; }

        }

        //Modificar
        public void ModificarItemMA(Item_MaBE ObjItem_Ma)
        {   
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_U_ITEM_MA", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@IMa_Id", ObjItem_Ma.IMa_Id));
                cmd.Parameters.Add(new SqlParameter("@IMa_Nombre_Tx", ObjItem_Ma.IMa_Nombre_Tx));
                cmd.Parameters.Add(new SqlParameter("@IMa_Descripcion_Tx", ObjItem_Ma.IMa_Descripcion_Tx));
                cmd.Parameters.Add(new SqlParameter("@IMa_Metrado_Nu", ObjItem_Ma.IMa_Metrado_Nu));
                cmd.Parameters.Add(new SqlParameter("@Unidad_Medida_Id", ObjItem_Ma.Unidad_Medida_Id));
                cmd.Parameters.Add(new SqlParameter("@Familia_Id", ObjItem_Ma.Familia_Id));
                cmd.Parameters.Add(new SqlParameter("@Marca_Id", ObjItem_Ma.Marca_Id));
                cmd.Parameters.Add(new SqlParameter("@IMa_Co", ObjItem_Ma.IMa_Co));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioActualizacion_Id", ObjItem_Ma.Aud_UsuarioActualizacion_Id));


                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

    }
}
