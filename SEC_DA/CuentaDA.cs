﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using SEC_BE;

namespace SEC_DA
{
    public class CuentaDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public List<CuentaBE> ListarCuenta(CuentaBE objCuenta)
        {

            List<CuentaBE> lstCuenta = new List<CuentaBE>();
            try
            {
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_CUENTA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PlanCuenta_Id", objCuenta.PlanCuenta_Id)); //Variable que se enviara desde la otra pagina
                cmd.Parameters.Add(new SqlParameter("@Cue_Codigo", objCuenta.Cue_Codigo));
                cmd.Parameters.Add(new SqlParameter("@Cue_Nombre_tx", objCuenta.Cue_Nombre_tx));
                cmd.Parameters.Add(new SqlParameter("@Elemento_Id", objCuenta.Elemento_Id));
                cmd.Parameters.Add(new SqlParameter("@Cue_Padre_Id", objCuenta.Cue_Padre_Id));
                cmd.Parameters.Add(new SqlParameter("@Cuenta_Id", objCuenta.Cuenta_Id));
                
                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read()) {
                    objCuenta = new CuentaBE();
                    lstCuenta.Add(new EntityMapper<CuentaBE>(rdr, objCuenta).FillEntity());
                
                }
                conn.Close();
                return lstCuenta;
            }
            catch (Exception ex) { throw ex; }
        }

        //CrearCuenta
        /*public int CrearCuenta(CuentaBE ObjCuenta) {
            int id = 0;
            try {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_CUENTA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de Parametros
               // cmd.Parameters.Add(new SqlParameter("@Cue_Codigo", ObjCuenta)); // ES AUTOGENERADO
                cmd.Parameters.Add(new SqlParameter("@Cue_Nombre_tx", ObjCuenta.Cue_Nombre_tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Cc_Fl", ObjCuenta.Cue_Cc_Fl));
                cmd.Parameters.Add(new SqlParameter("@Cue_Ta_Tx", ObjCuenta.Cue_Ta_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Transfiere_Tx", ObjCuenta.Cue_Transfiere_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_TipoDestino_FL", ObjCuenta.Cue_TipoDestino_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenDebe_nu", ObjCuenta.Cue_PorcenDebe_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenHaber_nu", ObjCuenta.Cue_PorcenHaber_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_Padre_Id", ObjCuenta.Cue_Padre_Id)); // ID del Padre
                cmd.Parameters.Add(new SqlParameter("@Elemento_Id", ObjCuenta.Elemento_Id)); //El mismo elemento del padre
                cmd.Parameters.Add(new SqlParameter("@PlanCuenta_Id", ObjCuenta.PlanCuenta_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjCuenta.Aud_UsuarioCreacion_Id));
                /*--Leer Parametros Adicionales 17 Agosto--> Se modifica segun lo modificado en PRC_I_CUENTA*/
                /*cmd.Parameters.Add(new SqlParameter("@Cue_CancTranf_FL", ObjCuenta.Cue_CancTranf_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_CuentaOficial_FL", ObjCuenta.Cue_CuentaOficial_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_NumeroCuenta", ObjCuenta.Cue_NumeroCuenta));
                cmd.Parameters.Add(new SqlParameter("@Banco_Id", ObjCuenta.Banco_Id));
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", ObjCuenta.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@TTipoCambio_Id", ObjCuenta.TTipoCambio_Id));
                ////////////////////////////////////////////////////////////////////////////////////////////////

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;
           
                conn.Open();
                cmd.ExecuteNonQuery();
                id = (int)cmd.Parameters["@ID"].Value;
                conn.Close();
                return id;
            }
            catch (Exception ex) { throw ex; }
        }*/

        public int CrearCuentaTransfiere(CuentaBE ObjCuenta) {

            int id = 0;
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_CUENTA_TRANSFIERE", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de Parametros
                cmd.Parameters.Add(new SqlParameter("@Cue_Codigo", ObjCuenta.Cue_Codigo)); 
                cmd.Parameters.Add(new SqlParameter("@Cue_Nombre_tx", ObjCuenta.Cue_Nombre_tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Cc_Fl", ObjCuenta.Cue_Cc_Fl));
                cmd.Parameters.Add(new SqlParameter("@Cue_Ta_Tx", ObjCuenta.Cue_Ta_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Transfiere_Tx", ObjCuenta.Cue_Transfiere_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_TipoDestino_FL", ObjCuenta.Cue_TipoDestino_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenDebe_nu", ObjCuenta.Cue_PorcenDebe_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenHaber_nu", ObjCuenta.Cue_PorcenHaber_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_Padre_Id", ObjCuenta.Cue_Padre_Id)); // ID del Padre
                cmd.Parameters.Add(new SqlParameter("@Elemento_Id", ObjCuenta.Elemento_Id)); //El mismo elemento del padre
                cmd.Parameters.Add(new SqlParameter("@PlanCuenta_Id", ObjCuenta.PlanCuenta_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjCuenta.Aud_UsuarioCreacion_Id));
                /*--Leer Parametros Adicionales 17 Agosto--> Se modifica segun lo modificado en PRC_I_CUENTA*/
                cmd.Parameters.Add(new SqlParameter("@Cue_CancTranf_FL", ObjCuenta.Cue_CancTranf_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_CuentaOficial_FL", ObjCuenta.Cue_CuentaOficial_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_NumeroCuenta", ObjCuenta.Cue_NumeroCuenta));
                cmd.Parameters.Add(new SqlParameter("@Banco_Id", ObjCuenta.Banco_Id));
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", ObjCuenta.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@TTipoCambio_Id", ObjCuenta.TTipoCambio_Id));
                ////////////////////////////////////////////////////////////////////////////////////////////////
                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                cmd.ExecuteNonQuery();
                id = (int)cmd.Parameters["@ID"].Value;
                conn.Close();
                return id;
            }
            catch (Exception ex) { throw ex; }
        
        }

        public int VerificarCodigoCuenta(CuentaBE ObjCuenta) { 
         int numerocodigo = 0;
         try
         {
             SqlConnection conn = new SqlConnection(cnx);
             SqlCommand cmd = new SqlCommand("SEC_PRC_V_NUMEROCODIGO", conn);
             cmd.CommandType = CommandType.StoredProcedure;
             //Lectura de Parametros
             cmd.Parameters.Add(new SqlParameter("@CUE_CODIGO", ObjCuenta.Cue_Codigo));

             cmd.Parameters.Add(new SqlParameter("@NUMERO_CODIGO", SqlDbType.Int, 32));
             cmd.Parameters["@NUMERO_CODIGO"].Direction = ParameterDirection.Output;

             conn.Open();
             cmd.ExecuteNonQuery();
             numerocodigo = (int)cmd.Parameters["@NUMERO_CODIGO"].Value;
             conn.Close();
             return numerocodigo;
         }
         catch (Exception ex)
         {
             throw ex;
         }
        
        
        }

        //Creacion de Cuenta 21 Agosto
        public CuentaBE CrearCuenta(CuentaBE ObjCuenta)
        {
            try
            {
                SqlConnection conn = new SqlConnection(cnx);
                SqlCommand cmd = new SqlCommand("SEC_PRC_I_CUENTA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                //Lectura de Parametros
                // cmd.Parameters.Add(new SqlParameter("@Cue_Codigo", ObjCuenta)); // ES AUTOGENERADO
                cmd.Parameters.Add(new SqlParameter("@Cue_Nombre_tx", ObjCuenta.Cue_Nombre_tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Cc_Fl", ObjCuenta.Cue_Cc_Fl));
                cmd.Parameters.Add(new SqlParameter("@Cue_Ta_Tx", ObjCuenta.Cue_Ta_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_Transfiere_Tx", ObjCuenta.Cue_Transfiere_Tx));
                cmd.Parameters.Add(new SqlParameter("@Cue_TipoDestino_FL", ObjCuenta.Cue_TipoDestino_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenDebe_nu", ObjCuenta.Cue_PorcenDebe_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_PorcenHaber_nu", ObjCuenta.Cue_PorcenHaber_nu));
                cmd.Parameters.Add(new SqlParameter("@Cue_Padre_Id", ObjCuenta.Cue_Padre_Id)); // ID del Padre
                cmd.Parameters.Add(new SqlParameter("@Elemento_Id", ObjCuenta.Elemento_Id)); //El mismo elemento del padre
                cmd.Parameters.Add(new SqlParameter("@PlanCuenta_Id", ObjCuenta.PlanCuenta_Id));
                cmd.Parameters.Add(new SqlParameter("@Aud_UsuarioCreacion_Id", ObjCuenta.Aud_UsuarioCreacion_Id));
                /*--Leer Parametros Adicionales 17 Agosto--> Se modifica segun lo modificado en PRC_I_CUENTA*/
                cmd.Parameters.Add(new SqlParameter("@Cue_CancTranf_FL", ObjCuenta.Cue_CancTranf_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_CuentaOficial_FL", ObjCuenta.Cue_CuentaOficial_FL));
                cmd.Parameters.Add(new SqlParameter("@Cue_NumeroCuenta", ObjCuenta.Cue_NumeroCuenta));
                cmd.Parameters.Add(new SqlParameter("@Banco_Id", ObjCuenta.Banco_Id));
                cmd.Parameters.Add(new SqlParameter("@Moneda_Id", ObjCuenta.Moneda_Id));
                cmd.Parameters.Add(new SqlParameter("@TTipoCambio_Id", ObjCuenta.TTipoCambio_Id));
                ////////////////////////////////////////////////////////////////////////////////////////////////

                cmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int, 32));
                cmd.Parameters["@ID"].Direction = ParameterDirection.Output;

                conn.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    new EntityMapper<CuentaBE>(rdr, ObjCuenta).FillEntity();

                }
                
                
                conn.Close();
                return ObjCuenta;
                
            }
            catch (Exception ex) { throw ex; }
        }

    }
}
