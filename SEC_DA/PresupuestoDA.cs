﻿using SEC_BE;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_DA
{
    public class PresupuestoDA
    {
        String cnx = System.Configuration.ConfigurationManager.ConnectionStrings["SEC"].ConnectionString;

        public DataTable ListarPresupuesto(PresupuestoBE objPresupuesto)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlConnection conn = new SqlConnection(cnx);

                SqlCommand cmd = new SqlCommand("SEC_PRC_S_PRESUPUESTO", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(new SqlParameter("@Proyecto_Id", objPresupuesto.Proyecto_Id));
                cmd.Parameters.Add(new SqlParameter("@Presupuesto_Id", objPresupuesto.Presupuesto_Id));

                //Abrir conexion
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
