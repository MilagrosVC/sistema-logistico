﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_Utilitarios.Enums
{
   public enum eCotizacionCa
    {
        creada = 36,
        ganadora = 37,
        Desaprobada = 38,
        OCGenerada = 39,
        Enviada_OC = 50,
        Enviada_SA = 51,
        DenegadaA = 63,
        Desaprobada_excede_presupuesto = 64
    }

   public enum eTipoPersona { 
   PersonaNatural=17,
   PersonaJuridica=16
   }
   public enum eProyecto_Almacen 
   {
       Proyecto=1,
           //Parametros
       Interno=34,
       InternoAireLibre=35,
       Externo=36,
       Arrendado=37

   }

   public enum ESTADO_COTIZACION_DETALLE
   { 
        CREADO,
        INGRESADO
   }

   public enum ESTADO_SOLICITUD
   {
       CREADA,ENVIADA
   }

}
