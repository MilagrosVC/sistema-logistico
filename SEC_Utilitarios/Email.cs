﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace SEC_Utilitarios
{
    public class Email
    {
        public static int Enviar(
            string sDe,
            //string sPara,
            List<string> sPara,
            string sAsunto,
            string sContenido,
            List<string> sParaCC = null,
            bool sContenidoEsHTML = true,
            List<string> sArchivos = null)
        {
            try
            {
                //MailMessage oMsg = new MailMessage();
                //MailMessage mailObj = new MailMessage(sDe, sPara, sAsunto, sContenido);
                MailMessage mailObj = new MailMessage(); 
                mailObj.From = new MailAddress(sDe);
                //mailObj.To.Add(sPara);
                //if (sDe != "") { mailObj.From = new System.Net.Mail.MailAddress(sDe); }

                foreach (string itemPara in sPara)
                {
                    mailObj.To.Add(itemPara);
                }

                if (sParaCC != null)
                {
                    foreach (string itemParaCC in sParaCC)
                    {
                        mailObj.CC.Add(itemParaCC);
                    }
                }
                
                mailObj.Subject = sAsunto;

                mailObj.Body = sContenido;
                //mailObj.IsBodyHtml = sContenidoEsHTML;

                if (sArchivos != null)
                {
                    foreach (string itemArchivo in sArchivos)
                    {
                        mailObj.Attachments.Add(new Attachment(itemArchivo));
                    }
                }
                                
                mailObj.IsBodyHtml = true;

                SmtpClient smtp = new SmtpClient("mail.dinetech.pe");
                smtp.Port = 25;
                //smtp.Timeout = 5000;
                //smtp.EnableSsl = false;
                smtp.Send(mailObj);

                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
