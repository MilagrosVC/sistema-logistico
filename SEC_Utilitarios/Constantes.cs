﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_Utilitarios
{
    public class Constantes
    {
        public class SessionVariables
        {
            public const string Usuario = "_Usuario";
            public const string Proyecto_Id = "_IdProyecto";
            public const string Menu = "_Menu";
            public const string Presupuesto = "_IdPresupuesto";
            public const string PerfilId = "_PerfilId";
            
        }
    }
}
