﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="RequerimientoUsuario.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.RequerimientoUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #MainSEC {
        padding: 0 10px 0 10px;
        color: #444;

        font-size:14px;
        }
        #tbBusqueda td {
            padding:3px 5px;
        }
        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
    </style>
    <link href="../../Scripts/Styles_SEC/SEC_BandRQ_ImprRQ.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3><b>Bandeja de Requerimiento</b></h3>
    <div style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">Filtros de Busqueda</b>
        </div>
        <div style="margin:10px 0;">
            <table id="tbBusqueda">
                <tr>
                    <td><label>Numero de Rq:</label></td>
                    <td style="padding-left:15px;"><input type="text" id="txtNumeroRq" name="txtNumeroRq" placeholder="N° RQ" class="inputs" /></td>
                    <td><label>Estado :</label></td>
                    <td>
                        <select id="ddlEstadoRQ" name="ddlEstadoRQ" class="inputs">
                        </select>
                    </td>
                    <td><label>Desde :</label></td>
                    <td style="padding-left:15px;"><input type="text" id="txtDesde" class="inputs" name="txtDesde" readonly="readonly" placeholder="dd/mm/aaaa" style="width:110px;" /></td>
                    <td style="padding-left:15px;"><label>Hasta :</label></td>
                    <td style="padding-left:15px;"><input type="text" id="txtHasta" class="inputs"name="txtHasta" readonly="readonly" placeholder="dd/mm/aaaa" style="width:110px;" /></td>
               </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" id="btnBuscar" name="btnBuscar" value="Buscar" class="boton"/>
                        <input type="button" id="btnLimpiar" name="btnLimpiar" value="Limpiar" class="boton"/>
                    </td>
                </tr>                                  
            </table>
        </div>
    </div>
    <br />

    <div id="divRequerimientoUsuario" style="max-width:100%;"></div>
    <br />
    <input type="button" id="btnGenerar" name="btnGenerar" value="Crear RQ" class="boton"/>

    <div id="dvVerRequerimiento" style="display:none">
        <div id="divImprimirRQ">
        <h4 style="text-align:center;"><label id="lblrq-Titulo">REQUERIMIENTO</label></h4>
        <div id="divLogoImpresion">
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png" id="LogoId" style="position:relative;top:-35px"/>
        </div>
        <label id="lblRq-CodRequerimiento" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerRq" style="font-size:10px;">
            <tr>
                <td>REQUERIDO POR :</td><td class="td-lbl"><label id="lblRq-Solicitante"></label></td>
            </tr>
            <tr>
                <td>FECHA DE REQUERIMIENTO :</td><td class="td-lbl"><label id="lblRq-FechaCreacion"></label></td>
            </tr>
            <tr>
                <td>FECHA DE ENTREGA :</td><td class="td-lbl"><label id="lblRq-FechaEntrega"></label></td>
            </tr>  
            <tr>
                <td>PROYECTO :</td><td class="td-lbl"><label id="lblRq-NomProyecto"></label></td>
            </tr> 
            <tr>
                <td>DIRECCION :</td><td class="td-lbl"><label id="lblRq-Direccion"></label></td>
            </tr>
            <tr>
                <td>ESPECIALIDAD :</td><td class="td-lbl"><label id="lblRq-Partida"></label></td>
            </tr>
            <tr>
                <td>PARTIDA :</td><td class="td-lbl" style="text-align:right;">TURNO :<label id="lblRq-Turno" style="margin:0 -10px 0 10px"></label></td>   
            </tr>
            <tr>
                <td colspan="2">
                    <ul id="lstRq-Subpartidas">
                        
                    </ul>
                </td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="2">
                    <table id="tbRq-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">ITEM</th>
                            <th style="width:300px">DESCRIPCION</th>
                            <th style="width:70px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:196px">OBSERVACION</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnImprimirRQ" name="name" value="Imprimir" class="boton" />
            <input type="button" id="btnCerrarVerRQ" name="name" value="Cerrar" class="boton" />   
            
        </div>
    </div>  
    
    <div id="divExceldialog" style="display:none;" >
        <div id="divExcel">
        <table id="tbExcelRq" style="font-size:10px;">
            <caption><h4 style="text-align:center;"><label id="lblExcelrq-Titulo">REQUERIMIENTO</label></h4><label id="lblExcelRq-CodRequerimiento" style="float:right;"></label></caption>             
            <tr>
                <td>REQUERIDO POR :</td><td class="td-lbl"><label id="lblExcelRq-Solicitante"></label></td>
            </tr>
            <tr>
                <td>FECHA DE REQUERIMIENTO :</td><td class="td-lbl"><label id="lblExcelRq-FechaCreacion"></label></td>
            </tr>
            <tr>
                <td>FECHA DE ENTREGA :</td><td class="td-lbl"><label id="lblExcelRq-FechaEntrega"></label></td>
            </tr>  
            <tr>
                <td>PROYECTO :</td><td class="td-lbl"><label id="lblExcelRq-NomProyecto"></label></td>
            </tr> 
            <tr>
                <td>DIRECCION :</td><td class="td-lbl"><label id="lblExcelRq-Direccion"></label></td>
            </tr>
            <tr>
                <td>ESPECIALIDAD :</td><td class="td-lbl"><label id="lblExcelRq-Partida"></label></td>
            </tr>
            <tr>
                <td>PARTIDA :</td><td class="td-lbl" style="text-align:right;">TURNO :<label id="lblExcelRq-Turno" style="margin:0 -10px 0 10px"></label></td>   
            </tr>
            <tr>
                <td colspan="2">
                    <ul id="lstExcelRq-Subpartidas">
                        
                    </ul>
                </td>
            </tr>
            <tr>
                <td id="tdExcelDetalle" colspan="2">
                    <table id="tbExcelRq-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">ITEM</th>
                            <th style="width:300px">DESCRIPCION</th>
                            <th style="width:70px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:196px">OBSERVACION</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
        <br />
    <div style="text-align:center;">
        <input type="button" id="btnExport" value="Generar Excel" style="width:120px;" class="boton" />
        <input type="button" id="btnCerrarExcel" value="Cerrar" style="width:120px;" class="boton"/>
    </div>
</div>
    
    <asp:HiddenField ID="hdf_Proyecto_Id" runat="server" />
    <asp:HiddenField ID="hdf_Usuario" runat="server" />
    <script src="../../Scripts/JS_SEC/Logistica/Compras/RequerimientoUsuario.js"></script>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
