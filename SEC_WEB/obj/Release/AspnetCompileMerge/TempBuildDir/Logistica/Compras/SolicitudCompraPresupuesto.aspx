﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="SolicitudCompraPresupuesto.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.SolicitudCompraPresupuesto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Solicitud de Compra: Presupuestos</h2>
        <table id="jQGridDemo"></table>
        <div id="jqGridPager"></div>
    <asp:HiddenField ID="hdf_Proyecto_Id" runat="server" />
    <script src="../../Scripts/JS_SEC/Logistica/Compras/SolicitudCompraPresupuesto.js"></script>
</asp:Content>
