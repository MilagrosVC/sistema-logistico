﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="SolicitudCompra.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.SolicitudCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body{
            /*background-color:#222;*/
            background-color:#fff;
        }
        #MainSEC {
            padding: 0 10px 0 10px;

            color: #444;/*#9d9d9d;*/
            font-size:14px;
        }
        .ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
            
        }
        .boton {
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;/*#9d9d9d;*/
            font-weight:bold;
        }
        #frmEnvioMail label{
            line-height: 35px;
        }
        .txtMail {
            height:30px;
            width: 400px;
            margin: 0 0 10px 10px;
        }
        .dvDatosRQ-td {
            padding:0 10px;
        }
        .ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled {
            opacity: .8;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!--<h2>Solicitud de Compra</h2>
        <table id="jQGridDemo"></table>
        <div id="jqGridPager"></div>
    <asp:HiddenField ID="hdfCodigoProyecto" runat="server" />
    <script src="../../Scripts/JS_SEC/Logistica/Compras/SolicitudCompras.js"></script>-->

    <!------ Hiddens ------>
    <asp:HiddenField ID="hdUsuario" runat="server" />
    <asp:HiddenField ID="hdMailUsu" runat="server" />
    <asp:HiddenField ID="hdProyecto" runat="server" />
    <asp:HiddenField ID="hdRequerimiento" runat="server" />
    <input type="hidden" id="hdNumSolicitud" />
    <!--------------------->
     
    <h3><b>Solicitud de Requerimientos</b></h3>
    <div id="dvDatosRQ" style="border:solid 2px;">
        <div style="background-color:#333;border-bottom:solid 2px;">
            <b style="color:#fff;font-size:12px; padding-left:5px;">Información de Requerimiento</b>
        </div>
        <div>
        <table style="margin:5px">
            <tr>
                <td>Proyecto:</td><td class="dvDatosRQ-td"><label id="lblProyecto" style="margin-bottom:0px;"></label></td>
                <td><span style="padding:40px;"></span></td>
                <td>Cantidad de Items:</td><td class="dvDatosRQ-td"><label id="lblNumItems" style="margin-bottom:0px;"></label></td>
            </tr>
            <tr>
                <td>Requerimiento:</td><td class="dvDatosRQ-td"><label id="lblRequerimiento" style="margin-bottom:0px;"></label></td>
            </tr>
            <tr>
                <td>Solicitante:</td><td class="dvDatosRQ-td"><label id="lblSolicitante" style="margin-bottom:0px;"></label></td>
            </tr>
            <tr>
                <td>Fecha Requerimiento:</td><td class="dvDatosRQ-td"><label id="lblFRequerimiento" style="margin-bottom:0px;"></label></td>
            </tr>
            <!--<tr>
                <td style="padding:10px 0">
                    <input type="button" id="btnNuevo" name="btnNuevo" value="Nueva Solicitud" class="boton" />
                </td>
                <td>
                    <input type="button" class="boton" id="btnRegresar" name="name" value="Regresar" style="/*margin: 0 0 10px 0;float:right*/" />
                </td>  
            </tr>-->
        </table>
        </div>
    </div>
    <br />
    <input type="button" id="btnNuevo" name="btnNuevo" value="Nueva Solicitud" class="boton" />
    <input type="button" class="boton" id="btnRegresar" name="name" value="Regresar" style="/*margin: 0 0 10px 0;float:right*/" />
    <br /><br />
    <div id="SEC_Solicitudes">

    </div>
    <!--Dialog para registro de items a solicitud-->
    <div id="dlg-NuevaSol" style="display:none;">
        <!--Grid para seleccion de Items-->
        <h4>Seleccione los items de la solicitud</h4>
        <div id="SEC_ItemsRQ"></div>
        <!--Seleccion de Proveedores
        <div id ="SEC_ProvsSolRQ-Proveedor" style="display:none;">
            <h4>Seleccione los proveedores asociados a la solicitud </h4>
            <label>Proveedor:</label>
            <select style="margin:0 10px; width:200px; display:inline-block" class="form-control" id="ddlProveedores">
                <option value="0">SELECCIONE</option>
            </select>
            <input type="button" class="boton" id="btnAgregarProv" name="btnAgregarProv" value="Agregar Proveedor" /><br /><br />   
            <div id="SEC_ProvsSolRQ"></div>
        </div>-->
        <p>---------------------------------------------------------------------------------------------------------------</p>
        <div id ="SEC_ProvsSolRQ-Proveedor">
            <h4>Seleccione los proveedores</h4>
            <label>Proveedor:</label>
            <select style="margin:0 10px; width:200px; display:inline-block" class="form-control" id="ddlProveedores">
                <option value="0">SELECCIONE</option>
            </select>
            <input type="button" class="boton" id="btnAgregarProv" name="btnAgregarProv" value="Agregar Proveedor" /><br /><br />   
            <div id="SEC_ProvsSolRQ"></div>
        </div>
        <p>---------------------------------------------------------------------------------------------------------------</p>
        <h4>Ingresar Observacion:</h4>
        <textarea class="form-control" id="txtArSolObs" rows="4"></textarea>
    </div>

    <!--Dialog para edicion de una solicitud-->
    <div id="dlg-EditSol" style="display:none;">
        <!--Grid para seleccion de Items-->
        <h4>Seleccione los items de la solicitud</h4>
        <div id="EditSol-Items"></div>
        <!--Seleccion de Proveedores-->
        <div id ="EditSol-ProvSolRQ">
            <h4>Listado de proveedores</h4>
            <label>Proveedor:</label>
            <select style="margin:0 10px; width:200px; display:inline-block" class="form-control" id="EditSol-ddlProveedores">
                <option value="0">SELECCIONE</option>
            </select>
            <input type="button" class="boton" style="width:200px" id="EditSol-btnAgregarProv" name="btnAgregarProv" value="Agregar Proveedor" /><br /><br />   
            <div id="EditSol-ProvsSolRQ"></div>
        </div>
        <p>---------------------------------------------------------------------------------------------------------------</p>
        <h4>Observacion:</h4>
        <textarea class="form-control" id="EditSol-Obs" rows="4"></textarea>
    </div>

    <div id="dlg-EnviarSol" style="display:none;">
        <div id="frmEnvioMail" style="padding:0 5px;">
            <table cellspacing="5">
                <tr>
                    <td style="width:40px">De: </td>
                    <td style="width:400px"><label id="DeMsj" class="txtMail"></label></td>
                </tr>
                <tr>
                    <td>Para: </td>
                    <td><input id="ParaMsj" type="text" name="name" value="" class="txtMail"/></td>
                </tr>
                <tr>
                    <td>CC: </td>
                    <td><input id="CCMsj" type="text" name="name" value="" class="txtMail"/></td>
                </tr>
                <tr>
                    <td>Asunto: </td>
                    <td><input id="AsuntoMsj" type="text" name="name" value="" class="txtMail"/></td>
                </tr>
            </table>
            <br />
            <textarea id="ContenidoMsj" name="content"></textarea>
        </div>
        

    </div>
    <!--<div id="dvFiltros">
        <h3>Criterios de Busqueda</h3>
        <div>
            <table id="tbBusqueda">
                <tr>
                    <td>Solicitante:</td>
                    <td><input type="text" id="txtSolicitante" name="" value="" placeholder="Ingrese Nombre"/></td>
                    <td>Desde:</td>
                    <td><input type="text" id="txtFiltroDesde" name="" value="" placeholder="aaaa/mm/dd"/></td>
                    <td>Hasta:</td>
                    <td><input type="text" id="txtFiltroHasta" name="" value="" placeholder="aaaa/mm/dd"/></td>
                </tr>
                        
            </table>
            <br />
            <input type="button" id="btnBuscar" value="Buscar" />
            <input type="button" id="btnLimpiar" value="Limpiar" />  
        </div>
    </div>
    <br />-->
    <div id="divExceldialog" style="display:none;" >
        <div id="divExcel">
            <table id="tbExcelRq" style="font-size:14px;color:#222">
                <caption><h2 style="text-align:center;"><label id="lblSolExcel-Titulo">SOLICITUD DE MATERIALES</label></h2></caption>             
                <tr>
                    <td></td><td style="font-weight:bold;">SOLICITADO POR:</td><td colspan="3"><label id="lblSolExcel-Solicitante"></label></td><!--<td></td><td></td>-->
                </tr>
                <tr>
                    <td></td><td style="font-weight:bold;">FECHA DE SOLICITUD:</td><td colspan="3" style="text-align:left;"><label style="text-align:left;" id="lblSolExcel-FechaCreacion"></label></td><!--<td></td><td></td>-->
                </tr>
                <tr>
                    <td></td><td style="font-weight:bold;">FECHA DE ENTREGA:</td><td colspan="3" style="text-align:left;"><label style="text-align:left;" id="lblSolExcel-FechaEntrega"></label></td><!--<td></td><td></td>-->
                </tr>  
                <tr>
                    <td></td><td style="font-weight:bold;">PROYECTO:</td><td colspan="3"><label id="lblSolExcel-NomProyecto"></label></td><!--<td></td><td></td>-->
                </tr> 
                <tr>
                    <td></td><td style="font-weight:bold;">DIRECCION:</td><td colspan="3"><label id="lblSolExcel-Direccion"></label></td><!--<td></td><td></td>-->
                </tr>
                <tr>
                    <td></td><td style="font-weight:bold;">OBSERVACION:</td><td colspan="3"></td><!--<td></td><td></td>-->
                </tr>
                <tr>
                    <td></td><td></td><td colspan="3"><label id="lblSolExcel-Observacion" style="font-size:12px;"></label></td><!--<td></td><td></td>-->
                </tr>
                <tr>
                    <td></td><td></td><td colspan="3"></td><!--<td></td><td></td>-->
                </tr>
            </table>
            <table id="tbExcelRq-Detalle" style="font-size:10px;">
                <tr>
                    <th></th>
                    <th style="width:50px;font-weight:bold;border:1px groove;background-color:#496B99;color:#fff">ITEM</th>
                    <th style="width:300px;font-weight:bold;border:1px groove;background-color:#496B99;color:#fff">DESCRIPCION</th>
                    <th style="width:30px;font-weight:bold;border:1px groove;background-color:#496B99;color:#fff">CANTIDAD</th>
                    <th style="width:60px;font-weight:bold;border:1px groove;background-color:#496B99;color:#fff">UND</th>
                </tr>
            </table>
        </div>
    </div>
    <script src="../../Scripts/JS_SEC/Logistica/Compras/SolicitudCompras.js"></script>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
