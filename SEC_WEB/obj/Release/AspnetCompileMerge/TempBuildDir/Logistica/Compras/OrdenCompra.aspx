﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="OrdenCompra.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.OrdenCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"> 
    <style>
    #MainSEC {
        padding: 0 10px 0 10px;
        color: #444;

        font-size:14px;
    }
    #tbBusqueda td {
        padding:3px 5px;
    }
    #tbOrdenCompra td{
        padding:0 10px;
    }
    .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }

    .inputs {
            border: 1px solid;
            border-radius:4px;
            border-color: #9d9d9d;
            height:28px;
            padding-left:10px;
        }
        .auto-style1 {
            width: 90px;
        }
    </style>
    <link href="../../Scripts/Styles_SEC/SEC_Orden_Compra.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3><b>Generar Ordenes de Compra</b></h3>
    <div style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">Filtros de Busqueda</b>
        </div>
        <div style="margin:10px 0;">
            <table id="tbBusqueda" style="width:100%">
                <tr>
                    <td class="auto-style1"><label>Proveedor :</label></td>
                    <td style="padding-left:15px;">
                        <select id="ddlProveedorOC" name="ddlProveedorOC" class="inputs" style="height:30px; width:250px;">
                        </select>                    
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="button" name="btnBuscar" id="btnBuscar" value="Buscar" class="boton"/>
                        <input type="button" name="btnLimpiar" id="btnLimpiar" value="Limpiar" class="boton"/>
                    </td>
                    <td style="float:right;margin-right:20px;">
                        <input type="button" name="btnVolver" id="btnVolver" value="Ir a bandeja Orden Compra" class="boton" style="width:220px"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br />
    <div id="divCotizacion" style="width:100%;">
    </div>
    <div id="dialog_OrdenCompra" title="GENERAR ORDEN DE COMPRA" style="display:none;">
        <table id="tbOrdenCompra">
            <tr>
                <td><label>Fecha :</label></td> 
                <td><input type="text" name="txtFecha" id="txtFecha" class="inputs" readonly="readonly" placeholder="dd/mm/aaaa" style="width:110px;"/></td>                
                <td><label>Tipo de Orden :</label></td>
                <td>
                    <select id="ddlTipoOrdenOC" name="ddlTipoOrdenOC" class="inputs" style="height:30px; width:200px;">
                    </select>
                </td>
                <td><label>N° :</label></td>
                <td><label id="lblNumeroOrden">Por Generar</label></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label>Razón Social :</label></td>
                <td colspan="3"><label id="lblRazonSocial"></label></td>
                <td><label>Penalidad :</label></td>
                <td><input type="checkbox" id="cbPenalidad" name="cbPenalidad" checked="checked" /></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label>Domicilio :</label></td>
                <td colspan="4"><label id="lblDomicilio"></label></td>
                <td><input type="text" name="txtPenalidad" id="txtPenalidad" class="inputs" maxlength="5" style="width:100px;" onkeypress="return validarnumeros(event)" /></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label>Cuidad :</label></td>
                <td><label id="lblCuidad"></label></td>
                <td><label>Moneda :</label></td>
                <td>
                    <label id="lblMoneda"></label></td>
                <td><label>Tipo Cambio :</label></td>
                <td><input type="text" name="txtTipoCambio" id="txtTipoCambio" class="inputs allownumericwithdecimal" maxlength="5" style="width:100px;" /></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label>Telefono :</label></td>
                <td>
                    <label id="lblTelefono"></label>
                    <!--<select id="ddlMonedaOC" name="ddlMonedaOC" class="form-control" style="height:30px; width:200px;">
                    </select>-->
                </td>
                <td><label>SubTotal :</label></td>
                <td>
                    <label id="lblSubTotal"></label></td>
                <td><label>IGV :</label></td>
                <td>                    
                    <label id="lblIGV"></label>                    
                </td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label>Forma de Pago :</label></td>
                <td colspan="3">
                    <label id="lblFormaPago"></label>
                    <!--<select id="ddlFormaPagoOC" name="ddlFormaPagoOC" class="form-control" style="height:30px; width:200px;">                        
                    </select>-->
                </td>
                <td style="padding-left:15px;"><label>Total :</label></td>
                <td>
                    <label id="lblTotal"></label>                    
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td></td>
            </tr>
        </table>
        <br />
        <div id="divCotizacionDe" style="width:100%;"></div>
    </div>

    <!--<div id="dialog_OrdenCompraInfo" title="Generar Excel Orden de Compra" style="display:none;">
        <table>
            <tr>
                <td><label>Fecha :</label></td>
                <td><label id="lblFechaInfo"></label></td>                
                <td style="padding-left:15px;"><label>Tipo de Orden :</label></td>
                <td>
                    <select id="ddlTipoOrdenInfoOC" name="ddlTipoOrdenInfoOC" class="form-control" style="height:30px; width:200px;">
                    </select>
                </td>
                <td style="padding-left:15px;"><label>N° :</label></td>
                <td><label id="lblNumeroOrdenInfo"></label></td>
            </tr>
            <tr>
                <td><label>Razón Social :</label></td>
                <td colspan="3"><label id="lblRazonSocialInfo"></label></td>
                <td style="padding-left:15px;"><label>Penalidad :</label></td>
                <td><input type="checkbox" id="cbPenalidadInfo" name="cbPenalidadInfo" disabled="disabled" /></td>
            </tr>
            <tr>
                <td><label>Domicilio :</label></td>
                <td colspan="4"><label id="lblDomicilioInfo"></label></td>
                <td><label id="lblPenalidad"></label></td>
            </tr>
            <tr>
                <td><label>Cuidad :</label></td>
                <td><label id="lblCuidadInfo"></label></td>
                <td><label style="padding-left:15px;">Telefono :</label></td>
                <td><label id="lblTelefonoInfo"></label></td>
                <td><label style="padding-left:15px;">Forma de Pago :</label></td>
                <td>
                    <select id="ddlFormaPagoInfoOC" name="ddlFormaPagoInfoOC" class="form-control" style="height:30px; width:200px;">
                    </select>
                </td>
            </tr>
            <tr>
                <td><label>Tipo de Moneda :</label></td>
                <td>
                    <select id="ddlMonedaInfoOC" name="ddlMonedaInfoOC" class="form-control" style="height:30px; width:200px;">
                    </select>
                </td>
                <td style="padding-left:15px;"><label>Tipo Cambio :</label></td>
                <td><label id="lblTipoCambioInfo"></label></td>
                <td style="padding-left:15px;"><label>IGV :</label></td>
                <td><label id="lblIGVInfo"></label>
                </td>
                
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="padding-left:15px;"><label>SubTotal :</label></td>
                <td><label id="lblSubTotalInfo"></label></td>
                <td style="padding-left:15px;"><label>Total :</label></td>
                <td><label id="lblTotalInfo"></label></td>
            </tr>
        </table>
        <br />
        <div id="divOrdenCompraDe" style="margin-left:75px;"></div>
        <br />
        <div id="divAnticipo" style="margin-left:75px;"></div>
        </div>-->

        <div id="dvVerOrdenCompra" style="display:none">
        <div id="divImprimirOC">
        <h4 style="text-align:center;"><label id="lblrq-Titulo">ORDEN DE COMPRA</label></h4>
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>
        <label id="lblOC-CodOrdenCompra" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerOC" style="font-size:10px;">
            <tr>
                <td>FECHA :</td><td class="td-lbl"><label id="lblOC-Fecha"></label></td>
            </tr>
            <tr>
                <td>RAZON SOCIAL :</td><td class="td-lbl"><label id="lblOC-RazonSocial"></label></td>
            </tr>
            <tr>
                <td>DOMICILIO :</td><td class="td-lbl"><label id="lblOC-Domicilio"></label></td>
            </tr>  
            <tr>
                <td>CUIDAD :</td><td class="td-lbl"><label id="lblOC-Ciudad"></label></td>
            </tr> 
            <tr>
                <td>TELEFONO :</td><td class="td-lbl"><label id="lblOC-Telefono"></label></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="2">
                    <table id="tbOC-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:300px">ARTICULO</th>
                            <th style="width:60px">PRECIO UND</th>
                            <th style="width:100px">TOTAL</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table id="tbOC-Anticipo" style="font-size:10px;">
            <!--<caption style="font-size:12px;font-weight:bold;color:#000;">ANTICIPOS</caption>
            <tr>
                <th style='width:50px;border: 1px solid #000;'>ITEM</th>
                <th style='width:150px;border: 1px solid #000;'>MONEDA</th>
                <th style='width:150px;border: 1px solid #000;'>MONTO SOLICITADO</th>
                <th colspan="2" style='width:300px;border: 1px solid #000;'>MEDIO PAGO</th>
                <th style='width:150px;border: 1px solid #000;'>TIPO ANTICIPO</th>
            </tr>
            <tr><td style="border: 1px solid #000;">&nbsp</td><td style="border: 1px solid #000;"></td><td style="border: 1px solid #000;"></td><td colspan="2" style="border: 1px solid #000;"></td><td style="border: 1px solid #000;"></td></tr>
        --></table>
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnImprimirOC" name="name" value="Imprimir" class="boton"/>
            <input type="button" id="btnCerrarVerOC" name="name" value="Cerrar" class="boton"/>   
            <input type="button" id="btnExport" value="Generar Excel" class="boton" />
        </div>
    </div>  

    

    <asp:HiddenField ID="hdf_Proyecto_Id" runat="server" />
    <asp:HiddenField ID="hdf_Usuario" runat="server" />
    <asp:HiddenField ID="hdf_OrdenCompraCabecera_Id" runat="server" />
    <asp:HiddenField ID="hdf_Cotizacion_Id" runat="server" />
    <asp:HiddenField ID="hdf_Moneda_Id" runat="server" />
    <asp:HiddenField ID="hdf_Forma_Pago_Id" runat="server" />

    <script src="../../Scripts/JS_SEC/Logistica/Compras/OrdenCompra.js"></script>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
