﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Proveedores.aspx.cs" Inherits="SEC_WEB.Logistica.Maestros.Proveedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/Styles_SEC/SEC_Proveedor.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/Proveedor/SEC_Proveedor.js"></script>
    <asp:HiddenField ID="hdUsuario" runat="server" />

     <asp:HiddenField ID="hdfPersonaNatural" runat="server" />
     <asp:HiddenField ID="hdfPersonaJuridica" runat="server" />

    <!--<div id="Content-Proveedor">-->
    <input type="hidden" id="hdfCodigoUbigeo"  />
    <input type="hidden" id="txtEstadoID" />
 <%--   <input type="hidden" id="hddCodigoProveedor" />--%>

    
     <div id="divUbigeo" style="display:none"></div>

    <div id="PantallaPrincipal" style="margin-left:25px">
     <h3>Proveedor</h3>
    <!-------Filtros de Busqueda ----------->
    <div id="Filtrobusqueda" style="width:80%;">
        <label for="txtTipoProveedor">Seleccione el Proveedor a Buscar:&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <input type="radio" name="RdbtnTipoProveedor"  value="RdbtnNatural" id="RdbtnNatural"/> Natural &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="radio" name="RdbtnTipoProveedor" value="RdbtnJuridico" id="RdbtnJuridico"/> Juridico
        <br/><br/>
    <table id="TbFiltroBusq">

        <tr>
            <td id="tdLApellidoPaterno"> 
                <label for="txtbusqApellidoPaterno"> Apellido Paterno:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIApellidoPaterno"> 
                <input id="txtbusqApellidoPaterno" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqApellidoPaterno" value="" />
            </td>
            <td id="TdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdLApellidoMaterno"> 
                <label for="txtbusqApellidoMaterno"> Apellido Materno:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIApellidoMaterno"> 
                <input id="txtbusqApellidoMaterno" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqApellidoMaterno" value="" />
            </td>
            <td id="TdEsp2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdLRazonSocial"> 
                <label for="txtbusqRazSocial"> Razon Social:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIRazonSocial"> 
                <input id="txtbusqRazSocial" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqRazSocial" value="" />
            </td>
            <td id="TdEsp3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdLPersonaContacto"> 
                <label for="txtbusqPersContacto"> Persona Contacto:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIPersonaContacto"> 
                <input id="txtbusqPersContacto" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqPersContacto" value="" />
            </td>
            <td id="TdEsp4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td id="tdLNroDocumento"> 
                <label for="txtbusqNroDocumento"> Nro Documento:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdINroDocumento"> 
                <input id="txtbusqNroDocumento" type="text"  onkeypress="return validarnumeros(event)" class="form-control" name="txtbusqNroDocumento" value="" />
            </td>
        </tr>
        <tr><td colspan="11">&nbsp;&nbsp;</td></tr> 
<%--        <tr id="TrBtnBusqueda"> 
            <td colspan="11" style="text-align:right" >
                 
            </td>
        </tr>--%>
    </table>
       <%-- <br/>--%>
        <%--<table>
            <tr>
                <td style="width:2000px;">--%>
        <input type="button" style="width:140px" id="btnAVRegistroPN" class="boton" name="name" value="Proveedor Natural" />
        <input type="button" style="width:140px" id="btnAVRegistroPJ" class="boton" name="name" value="Proveedor Juridico" />
        <input type="button" id="btnRealizarBusqueda" class="boton" name="name" value="Buscar" />
        <input type="button" id="btnLimpiarBusqueda" class="boton" name="name" value="Limpiar" /> 
             <%--   </td>
            </tr>
        </table>--%>
    <br/><br/>

        </div>


    <!-------Ventana para Registrar Proveedor ----------->
    <div id="Proveedor-Reg" style='display:none;'>
    <table class="table">
        <tr id="trDatosPrincipalesNatural">
        
       <%-- <tr id="trAPaterno">--%>
            <td>
                <label for="txtApePaterno">Apellido Paterno:  </label>
            </td>
            <td>
                <input id="txtApePaterno" type="text" onkeypress="return validarletras(event)" class="form-control" name="txtApePaterno" value="" required="required"/>
            </td>
       <%-- </tr>--%>

       <%-- <tr id="trAMaterno">--%>
            <td>
                <label for="txtApeMaterno">Apellido Materno:  </label>
            </td>
            <td>
                <input id="txtApeMaterno" type="text" onkeypress="return validarletras(event)" class="form-control" name="txtApeMaterno" value="" required="required"/>
            </td>
        <%--</tr>--%>

        <%--<tr id="trNombres">--%>
            <td>
                <label for="txtNombres">Nombres:</label>
            </td>
            <td>
                <input id="txtNombres" type="text" onkeypress="return validarletras(event)"  class="form-control" name="txtNombres" value="" required="required"/>
            </td>
       <%-- </tr>--%>

        </tr>

        <tr id="tdDatosPrincipalesJuridico">
           <td id="tdLNumeroRuc">
                <label for="txtNumeroRUC">Número de RUC:  </label>
            </td>
            <td id="tdINumeroRuc">
               <%-- maxlength="11" --%>
                <input id="txtNumeroRUC" type="text" maxlength="11" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroRUC" value="" required="required"/>
            </td>

        <%--<tr id="trRazonSocial">--%>
            <td>
                <label for="txtRazonSocial">Razón Social:  </label>
            </td>
            <td>
                <input id="txtRazonSocial" type="text" class="form-control" name="txtRazonSocial" value="" required="required"/>
            </td>
        <%--</tr>--%>
      

           
             <%--<tr id="trPContacto">--%>
            <td>
                <label for="txtPersonaContacto">Persona Contacto:  </label>
            </td>
            <td>
                <input id="txtPersonaContacto" type="text" onkeypress="return validarletras(event)" class="form-control" name="txtPersonaContacto" value="" required="required" />
            </td>
         <%--</tr>--%> 
        </tr>

        <tr>
       <%-- <tr>--%>
            <td id="tdLTipoDocumento">
                <label for="ddlTipoDocumento">Tipo Documento:  </label>
            </td>
            <td id="dlSTipoDocumento">
                <select id="ddlTipoDocumento" class="form-control">
                    <option value="0"> SELECCIONE </option>
                </select>

            </td>
        <%-- </tr>--%>
        <%--<tr>--%>
            <td id="tdLtxtNumeroDoc">
                <label for="txtNumeroDocumento">Número Documento:  </label>
            </td>
            <td id="tdItxtNumeroDoc">
                <input id="txtNumeroDocumento" onkeypress="return validarnumeros(event)" type="text" class="form-control" name="txtNumeroDocumento" value="" required="required"/>
            </td>
         <%--</tr>--%>

        <%--<tr id="trFNacimiento">--%>
            <td id="tdLFNacimineto">
                <label for="txtFechaNacimiento">Fecha Nacimiento:  </label>
            </td>
            <td id="tdIFNaciemiento">
                <!--<input id="txtFechaNacimiento" type="date" class="form-control" name="txtFechaNacimiento" max="1997-12-31"  value="" required="required"/>-->
            </td>
        <%--</tr>--%>

        </tr>

        <tr>
            <!--<td>
                <label for="txtUbigeoID">Ubigeo ID:  </label>
            </td>
            <td>
                <input id="txtUbigeoID" type="text" class="form-control" name="txtUbigeoID" value="" required="required"/>
            </td>-->
            <td><label for="ddlDepartamento">Departamento: </label></td>
            <td>
                <select id="ddlDepartamento" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlProvincia">Provincia: </label></td>
            <td>
                <select id="ddlProvincia" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlDistrito">Distrito: </label></td>
            <td>
                <select id="ddlDistrito" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>  
        </tr>

        <tr>
            <td>
                <label for="txtDireccion">Direccion:  </label>
            </td>
            <td colspan="3">
                <input id="txtDireccion" type="text" class="form-control" name="txtDireccion" value="" required="required" />
            </td>

           <td>
                <label for="txtemail">Email:  </label>
            </td>
            <td>
                <input id="txtemail" type="email" class="form-control" name="txtemail" value="" required="required"/>
            </td>
        </tr>

        <tr>
       <%-- <tr>--%>
            <td>
                <label for="txttelefono1">Télefono 1:  </label>
            </td>
            <td>
                <input id="txttelefono1" type="text" onkeypress="return validartelefono(event)" class="form-control" name="txttelefono1" value="" required="required"/>
            </td>
        <%--</tr>--%>
        <%--<tr>--%>
            <td>
                <label for="txttelefono2">Télefono 2:  </label>
            </td>
            <td>
                <input id="txttelefono2" type="text"  onkeypress="return validartelefono(event)" class="form-control" name="txttelefono2" value="" required="required"/>
            </td>
        <%--</tr>--%>
       <%-- <tr>--%>
            

             <td>
               <%-- <label for="txtProvClaID">Proveedor Clasificación-ID:</label>--%>
                  <label for="txtProvClaID">Clasificación:</label>
            </td>
            <td>
                
               <%-- <input id="txtProvClaID" type="text" class="form-control" name="txtProvClaID" value="" required="required"/>--%>
                <select id="ddlProvedorClasificacion" class="form-control">
                    <option value="0"> SELECCIONE </option>
                </select>
            </td>
        <%--</tr>--%>
        </tr>

        <tr>

<%--            <td>
                <label for="txtRubroID">Rubro ID:</label>
            </td>
            <td>
                <input id="txtRubroID" type="text" class="form-control" name="txtRubroID" value="" required="required"/>
                <input type="button" id="btnAgregarRubro" class="btn btn-default btn-sm" name="name" value="Agregar Rubro" />
            </td>--%>

      <%--  <tr>--%>
            
        <%--</tr>--%>
        </tr>          
        <!--<tr>
            <td>
                <label for="txtTipoPersonaID">Tipo Persona ID:</label>
            </td>
            <td>
                <input id="txtTipoPersonaID" type="text" class="form-control" name="txtTipoPersonaID" value="" required="required"/>
            </td>
        </tr>-->

        <tr>
     <%--   <tr>--%>
            <td>
                <label for="chkProveedorHomologadoFl">Proveedor Homologado:</label>
            </td>
            <!--<td>
                <input id="txtProveedorHomologadoFL" type="text" class="form-control" name="txtProveedorHomologadoFL" value="" required="required"/>
            </td>-->
           <td>
               <input type="checkbox" id="chkProveedorHomologadoFl" value=" "/>
           </td>   
      <%--  </tr>--%>
       <%-- <tr>--%>
            <td>
              <%--  <label for="txtPrvEstadoHomologadoID">Estado Homologado ID:</label>--%>
                <label for="txtPrvEstadoHomologadoID">Estado Homologado:</label>
            </td>
            <td>
                <input id="txtPrvEstadoHomologadoID" type="text" readonly="true" class="form-control" onkeypress="return validarnumeros(event)" name="txtPrvEstadoHomologadoID" value="" required="required"/>
            </td>
        <%--</tr>--%><!---->
        <%--<tr>--%>
            <td>
               <%-- <label for="txtProvHomologadoraID">Proveedor Homologador ID:</label>--%>
                <label for="txtProvHomologadoraID">Proveedor Homologador:</label>
            </td>
            <td>
                <input id="txtProvHomologadoraID" type="text" readonly="true" class="form-control" onkeypress="return validarnumeros(event)"  name="txtProvHomologadoraID" value="" required="required" />
            </td>
        <%--</tr>--%><!---->
        </tr>
<%--          <tr>
            <td>
                <label for="txtEstadoID">Estado ID:</label>
            </td>
            <td>
                <input id="txtEstadoID" type="text" class="form-control" name="txtEstadoID" value="" required="required"/>
            </td>
        </tr>--%>


        <tr id="trbtnProveedorNatural">
            <td colspan="6" style="text-align:right" <%--align="right" class="btn btn-default btn-sm"--%>>
               <input type="button" style="width:185px" id="btnRegistrarProveedorNatural" class="boton" name="name" value="Registrar Proveedor Natural"  />
                <input type="button" style="width:150px" id="btnCancelarProveedorNatural" class="boton" name="name" value="Cancelar Registro"  />
            </td>
        </tr>
        <tr id="trbtnProveedorJuridico">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnRegistrarProveedorJuridico" class="boton" name="name" value="Registrar Proveedor Juridico"  />
                <input type="button" style="width:150px" id="btnCancelarProveedorJuridico" class="boton" name="name" value="Cancelar Registro"<%-- onclick="this.close();"--%> />
            </td>
        </tr>

        <tr id="trbtnModificarProveedorNatural">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnModificaraProveedorNatural" class="boton" name="name" value="Editar Proveedor Natural"  />    
                <input type="button" style="width:150px" id="btnCancelarModificarProveedorNatural" class="boton" name="name" value="Cancelar Edición"  />
            </td>
        </tr>

                <tr id="trbtnModificarProveedorJuridico">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnModificaraProveedorJuridico" class="boton" name="name" value="Editar Proveedor Juridico"  />    
                <input type="button" style="width:150px" id="btnCancelarModificarProveedorJuridico" class="boton" name="name" value="Cancelar Edición"  />
            </td>
        </tr>
            <tr id="trbtnDetalleProveedor">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:200px" id="btnDetalleRubros" class="boton" name="name" value="Visualizar Rubros del Proveedor"  />    
                <input type="button" style="width:195px" id="btnSalirDetalle" class="boton" name="name" value="Salir de Detalle de Proveedor"  />
            </td>
        </tr>

    </table>

        
    <div id="dvAsignarRubros">
      <%--  <br/>--%>
        <h5>Proceda a seleccionar los rubros que le correspondan:</h5>
        <table class="table">
            <tr>
        <%--<tr>--%>
            <%--<td>
                <label for="btnAgregarRubro">Rubro ID:</label>
            </td>--%>
            <td>           
           <%-- <label for="txtRubroID">Rubro ID:</label>
            </td>
            <td>
                <input id="txtRubroID" type="text" class="btn btn-default btn-sm" name="txtRubroID" value="" required="required"/>--%>

                <input type="button" id="btnAgregarRubro" style="width:190px"  class="boton" name="name" value="Agregar Rubro a Proveedor" />
                 <input type="button" id="btnSalirdelRegistro" style="width:190px"  class="boton" name="name" value="Salir sin seleccionar Rubro" />
             </td>
           </tr>
        </table>
    </div>
        
    </div>
    <!-------------------------------------------------------->
    <!------------------------ JqGrid ------------------------->
    <div id="ProveedorGrid">
    </div>
    <!-------------------------------------------------------->
    </div>
    <!-------Ventana para Seleccionar el/los Rubro(s) del Proveedor ----------->
    <div id="ProveedorRubro" style="display:none">
        <%--<h5>Seleccione los Rubros del Proveedor</h5>--%>
        <input type="hidden" id="hdnIdProveedor" name="hdnIdProveedor" />
       <%-- <br/>--%>
        <table>
        <tr>
            <td style="text-align:center"><h5>Rubros Disponibles</h5></td>
            <td></td>
            <td style="text-align:center"><h5>Rubros Asignados</h5></td>
        </tr>
        <tr>
            <td>
                <div id="divRubro">
                <select multiple="multiple" class="form-control" id="ddlRubro" name="ddlRubro" style="width: 300px;height: 300px;"> </select>
                </div>
            </td>
       
            <td style="text-align: center; padding-left:20px; padding-right:20px;" id="tdbtnAsigRubro">
                
                <input type="button" id="btnAgregarSeleccionados" class="boton" value=">" style="width:35px;"/> <br/><br/>
                <input type="button" id="btnAgregarTodos" class="boton" value=">>" style="width:35px;"/> <br/><br/>
                <input type="button" id="btnQuitarTodos" class="boton" value="<<" style="width:35px;"/>  <br/><br/>
                <input type="button" id="btnQuitarSeleccionados" class="boton" value="<" style="width:35px;"/> <br/><br/>
            </td>
            <td style="text-align:center">
                <div id="divRubroProveedor">
                <select multiple="multiple" class="form-control" id="ddlRubroProveedor" name="ddlRubroProveedor" style="width: 300px;height: 300px;"> </select>
                </div>
            </td>
        </tr>   
        
            <tr><td colspan="3">&nbsp;&nbsp;</td></tr> 
            <tr id="trGuardarRubrosdeProveedor">
                <td colspan="3" style="text-align:right">
                    <input type="button" id="btnGuardarRubrosdeProveedor" style="width: 200px" class="boton" name="name" value="Guardar Rubros Asignados" />
                </td>
            </tr>
            <tr id="trSalirVentanaRubrosProveedor">
                <td colspan="3" style="text-align:right">
                    <input type="button" id="btnSalirVentanaRubrosProveedor" style="width: 200px" class="boton" name="name" value="Salir de Pantalla Actual" />
                </td>
            </tr>
        </table>
        <br/>
<%--        <table style="text-align:right">
            <tr>
             <td style="text-align:right">
              <input type="button" id="btnGuardarRubrosdeProveedor" class="btn btn-default btn-sm" name="name" value="Guardar Rubros Asignados" />
             </td>
            </tr>
        </table>--%>
    </div>
    <!------------------------------------------------------------------------>



    <!--</div>-->
</asp:Content>

 
