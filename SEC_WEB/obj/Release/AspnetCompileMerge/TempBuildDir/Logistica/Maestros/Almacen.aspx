﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Almacen.aspx.cs" Inherits="SEC_WEB.Logistica.Maestros.Almacen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/Almacen/SEC_Almacen.js"></script>
    
    <div id="PantallaPrincipal" style="margin-left: 25px">

        <asp:HiddenField ID="hdUsuario" runat="server" />
     <asp:HiddenField ID="hdfProyecto" runat="server" />
    <!--//Segun Tipo de Almacen --->
    <asp:HiddenField ID="hdfInterno" runat="server" />
    <asp:HiddenField ID="hdfInternoAireLibre" runat="server" />
    <asp:HiddenField ID="hdfExterno" runat="server" />
    <asp:HiddenField ID="hdfArrendado" runat="server" />

    <input type="hidden" id="hdfCodigoUbigeo"  />


        <h3>Alamcen</h3>
        <div id="Filtrobusqueda" style="width: 80%; margin: 10px 0;">
            <table id="TbFiltroBusq">
                <tr>
                    <td id="tdLNombre">
                        <label for="txtbusqNombre">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </td>
                    <td id="tdINombre">
                        <%--<input id="txtbusqNombre" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqNombre" value="" />--%>
                        <input id="txtbusqNombre" type="text" class="inputs" name="txtbusqNombre" value="" placeholder="Ingrese Nombre" />
                    </td>
                    <%--<td id="TdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        <td id="tdLResponsable"> 
                        <label for="txtbusqResponsable"> Responsable:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        </td>
                        <td id="tdIResponsable"> 
                   
                              <input id="txtbusqResponsable" type="text" onkeypress="validarletras()" name="txtbusqResponsable" class="inputs" value="" placeholder="Ingrese Responsable"  />
                        </td>--%>
                </tr>

            </table>
            <br />
            <input type="button" id="btnBuscar" value="Buscar" class="boton" style="margin-left: 5px;" />
            <input type="button" id="btnLimpiar" value="Limpiar" class="boton" style="width:120px;"/>
            <input type="button" id="btnNuevoAlmacen" value="Nuevo Almacen" class="boton" style="margin-left: 5px;" />
        </div>

        <br />
        <!--  grid-->
        <div id="AlmacenGrid">
        </div>
        <!-------------------------------->

        <!---Registro nuevo de almacen--->
        <div id="Almacen-Reg" style='display: none;'>
            <table class="table">
                <tr>
                    <td>
                        <label for="ddlProyecto">Proyecto:  </label>
                    </td>
                    <td>
                 <select id="ddlProyecto" class="form-control"><option value="0"> SELECCIONE </option></select>
                    </td>

                    <td>
                        <label for="txtNombre">Nombre:  </label>
                    </td>
                    <td>
                        <input id="txtNombre" type="text" onkeypress="return validarletras(event)" class="form-control" name="txtNombre" value="" required="required" />
                    </td>
                     <td>
                        <label for="txtAbreviatura">Abreviatura:</label>
                    </td>
                    <td>
                        <input id="txtAbreviatura" type="text" class="form-control" name="txtAbreviatura" value="" required="required" />
                    </td>
                   
                   
                </tr>

                <tr>
                      <td>
                        <label for="txtResponsable">Responsable:  </label>
                    </td>
                    <td>
                        <input id="txtResponsable" type="text" class="form-control" name="txtResponsable" value="" required="required" />
                    </td>
                   
                    <td>
                        <label for="txtTelefono">Telefono:  </label>
                    </td>
                    <td>
                        <input id="txtTelefono" type="text" class="form-control" name="txtTelefono" value="" required="required" />
                    </td>

                     <td>
                        <label for="ddlTipoAlmacen">Tipo:  </label>
                    </td>
                    <td>
                 <select id="ddlTipoAlmacen" class="form-control"><option value="0"> SELECCIONE </option></select>
                    </td>
                </tr>
                <tr>
                    
                   <td>
                        <label for="txtDescripcion">Descripcion:  </label>
                    </td>
                    <td>
                        <input id="txtDescripcion" type="text" class="form-control" name="txtDescripcion" value="" required="required" />
                    </td>
                     <td>
                        <label for="txtDireccion">Direccion:  </label>
                    </td>
                    <td colspan="3">
                        <input id="txtDireccion" type="text" class="form-control" name="txtDireccion" value="" required="required" />
                    </td>
                </tr>
                <tr>
                   <td><label for="ddlDepartamentoA">Departamento: </label></td>
            <td>
                <select id="ddlDepartamentoA" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlProvinciaA">Provincia: </label></td>
            <td>
                <select id="ddlProvinciaA" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlDistritoA">Distrito: </label></td>
            <td>
                <select id="ddlDistritoA" class="form-control">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>  
                </tr>


                <tr id="" style="height: 80px; margin-top:30px;">
            <td colspan="6" style="text-align:right" <%--align="right" class="btn btn-default btn-sm"--%>>
               <input type="button" style="width:185px" id="btnRegistrar" class="boton" name="name" value="Registrar Almacen"  />
                 <input type="button" style="width:185px" id="btnLimpiarRegistro" class="boton" name="name" value="Limpiar"  />
                <input type="button" style="width:150px" id="btnCancelarRegistro" class="boton" name="name" value="Cancelar Registro"  />
            </td>
        </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>

</asp:Content>
