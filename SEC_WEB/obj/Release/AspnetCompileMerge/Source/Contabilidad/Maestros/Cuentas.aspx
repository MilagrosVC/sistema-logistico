﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Cuentas.aspx.cs" Inherits="SEC_WEB.Contabilidad.Maestros.Cuentas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/Styles_SEC/SEC_Cuentas.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 8px;
        }
    </style>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdUsuario" runat="server" />
    <%--  Hidden que sirven para almacenar las variables que se reciben del Plan de Cuenta--%>
    <asp:HiddenField ID="hdf_PlanCuentaID" runat="server" />
    <asp:HiddenField ID="hdf_PlanCuentaNombre" runat="server" />
    <asp:HiddenField ID="hdf_Emp_RazonSocial" runat="server" />
    <asp:HiddenField ID="hdf_Emp_RUC" runat="server" />
    <asp:HiddenField ID="hdf_FechaCreacionPC" runat="server" />
    <asp:HiddenField ID="hdf_HoraCreacionPC" runat="server" />

    <%--Datos Para la creacion de nueva cuenta - EL HIDDEN USUARIO CREACION SE ENCUENTRA EN LA PARTE SUPERIOR--%>
    <input type="hidden" id="hdfPlanCuenta_Id" />
    <input type="hidden" id="hdfIDCuentaPadre" />
    <input type="hidden" id="hdfIDElementoPadre" />
    <input type="hidden" id="hdfCodigoCuenta" />


    <input type="hidden" id="hdfCantidadNumeroCuenta" />

    <%-- 
        
          <label id="IdPlanCuenta"></label>--%>
    <div id="DVPrincipal" style="margin-left: 25px; margin-right: 30px">
        <%-- <h3> PLAN DE CUENTAS</h3>--%>
        <h4 style="text-align:center">
            <label for="PlanCuentaNombre" id="PlanCuentaNombre"></label>
        </h4>
        <!--------------------------------------------------------->
        <div class="bs-example">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <%--  <h1 class="panel-title"><strong>Detalle de Plan de Cuenta</strong></h1>--%>
                    <%--  <hr style="width:30% ; align-content:"left"/>--%>
                   <%-- <br />--%>
                    <table id="tbDetallePC">
                        <tr>
                            <td style="height:6px"></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <label for="LBRUC">RUC :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBRUC" id="LBRUC"></label>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <strong>
                                    <label for="LBEmpresa">Empresa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBEmpresa" id="LBEmpresa"></label>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                            <td>
                                <strong>
                                    <label for="LBFechaCreacion">Fecha Creacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBFechaCreacion" id="LBFechaCreacion"></label>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <strong>
                                    <label for="LBHoraCreacion">Hora Creacion:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBHoraCreacion" id="LBHoraCreacion"></label>
                            </td>
                        </tr>
                    </table>



                </div>
                <div class="panel-body">
 
                    <table id="tbFiltrosBusqueda">
                        <tr>
                            <td>
                                <label for="ICueCodigo">Codigo:&nbsp;</label>
                            </td>
                            <td>
                                <input id="ICueCodigo" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="ICueCodigo" value="" style="width: 110px;" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="ICueNombre">Nombre:&nbsp;</label>
                            </td>
                           <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                            <td colspan="6">
                                <input id="ICueNombre" type="text" onkeypress="validarletras(e)" class="form-control" name="ICueNombre" value="" style="width: 450px;" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="ddlElementoID">Elemento:&nbsp;</label>
                            </td>
                            <td>
                                <select id="ddlElementoID" name="ddlElementoID" class="form-control" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                           <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                            <%--<td>

                                <input type="button" style="text-align: center; width: 75px" id="btnBuscarCuentas" class="btn btn-sm" name="name" value="Buscar" />
                                <input type="button" style="text-align: center; width: 75px" id="btnLimpiarBusqueda" class="btn btn-sm" name="name" value="Limpiar" />
                            </td>--%>
                           <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                            </tr>
                             <tr>
                            <td colspan="12" style="height:15px">
                                </td>
                        </tr>
                        <tr>
                          
                            <%--<td>
                                <input type="button" style="text-align: center; width: 120px" id="btnCrearCuentas" class="btn btn-sm" name="name" value="Crear SubCuenta" />
                                <input type="button" style="text-align: center; width: 120px" id="btnCrearTransferenciaDestino" class="btn btn-sm" name="name" value="Crear Destino" />
                            </td>--%>
                        </tr>
                       
                    
                       
                        <tr>
                           
                           <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                              <td >
                                <label for="txtCodCuentaPadreID">Cuenta:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                             <strong><input id="txtCodCuentaPadreID" type="text" class="form-control" name="txtCodCuentaPadreID" value="" style="width: 110px;" /></strong>   
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;</td>
                            
                            <td style="text-align:right">
                               <input type="button" style="text-align: center; width: 150px" id="btnCrearCuentas" class="boton" name="name" value="Crear SubCuenta" />
                            </td>
                            <td>
                                <input type="button" style="text-align: center; width: 150px" id="btnCrearTransferenciaDestino" class="boton" name="name" value="Crear Destino" />
                            </td>

                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                             <td><%--style="text-align:right"--%>
                                 <input type="button" style="text-align: center" id="btnBuscarCuentas" class="boton" name="name" value="Buscar" />
                                </td>
                            <td>
                                <input type="button" style="text-align: center" id="btnLimpiarBusqueda" class="boton" name="name" value="Limpiar" />
                            </td>
                        </tr>
                 </table>
                </div>
            </div>
        </div>
        <!--------------------------------------------------------->

        <!------------------------ JqGrid ------------------------->
        <div id="CuentaGrid">
        </div>
        <!--------------------------------------------------------->

        <!--------------------------------------------------------->
        <div id="DVCrearCuenta" style='display: none;'>

            <%-- <h5>Datos Nueva Sub-Cuenta</h5>--%>
            <div class="panel panel-default" style="background-color: rgba(245, 245, 245, 0);">
                <div class="panel-heading">
                    <table>
                        <tr id="trCrearSubCuenta">
                            <td>
                                <strong>
                                    <label for="txtCuentaCodigoPadre">Cuenta Principal:&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="txtCuentaCodigoPadre" id="txtCuentaCodigoPadre" style="width: 50px"></label>
                            </td>
                            <td>&nbsp;- &nbsp;</td>
                            <td>
                                <label for="txtNombreCuentaPadre" id="txtNombreCuentaPadre" style="width: 270px"></label>
                            </td>
                            <%--<td>&nbsp;&nbsp;&nbsp;</td>--%>
                        </tr>
                        <tr id="trCrearDestino">
                            <td>
                                <strong>
                                    <label for="txtCuentaCodigoCuenta">Codigo de la Cuenta:&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                   <label for="txtCuentaCodigoCuenta" id="txtCuentaCodigoCuenta" style="width: 50px"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <label for="txtElementoCodigoPadre">Elemento:&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="txtElementoCodigoPadre" id="txtElementoCodigoPadre" style="width: 10px"></label>
                            </td>
                            <td>&nbsp;- &nbsp;</td>
                            <td>
                                <label for="txtElementoNombrePadre" id="txtElementoNombrePadre" style="width: 400px"></label>
                            </td>
                        </tr>

                        </table>

                </div>
                <div class="panel-body">

                    <table>
                        <tr>
                            <td>
                                <label for="txtNombreCuenta">Nombre:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="5">
                                <input id="txtNombreCuenta" type="text" class="form-control" name="txtNombreCuenta" value="" /><%--style="width:420px"--%>
                            </td>
                            <%--  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                        </tr>
                        <tr>
                            <td>
                                <label for="flatCentroCosto">Centro Costo:&nbsp;&nbsp;&nbsp;</label>

                            </td>
                            <td>
                                <%--<input type="checkbox" id="flatCentroCosto" value=" "/>--%>
                                <input type="radio" name="flatCentroCosto" value="flatAsociado" id="flatAsociado" />
                                Asociado &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="flatCentroCosto" value="flatSINAsociar" id="flatSINAsociar" />
                                No Asociado
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="txtTA">TA:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="2">
                                <input id="txtTA" type="text" class="form-control" name="txtTA" value="" /><%--style="width:135px"--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="flatTipoDestino">TipoDestino:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                <%--<input type="checkbox" id="flatTipoDestino" value=" "/>--%>
                                <input type="radio" name="flatTipoDestino" value="flatTipoDebe" id="flatTipoDebe" />
                                Debe &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="flatTipoDestino" value="flatTipoHaber" id="flatTipoHaber" />
                                Haber &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="flatTipoDestino" value="flatTipoNinguno" id="flatTipoNinguno" />
                                Ninguno
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td style="width: inherit">
                                <label for="txtTransfiere">Transfiere:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td class="form-horizontal">
                                <input id="txtTransfiere" class="form-control" type="text" name="txtTransfiere" value="" style="width: 80px" />
                                <%--class="form-control"--%>
                            </td>
                            <td>
                                <input type="button" id="btnSeleccionarTransfiere" class="boton" name="name" value="Seleccionar" style="width: 95px" />
                                <%-- class="btn btn-sm"--%>
                                 
                            </td>


                        </tr>
                        <tr>
                            <td>
                                <label for="txtPorcentajeDebe">Porcentaje Debe:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                <input id="txtPorcentajeDebe" type="text" class="form-control" name="txtPorcentajeDebe" value="" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="txtPorcentajeHaber">Porcentaje Haber:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="2">
                                <input id="txtPorcentajeHaber" type="text" class="form-control" name="txtPorcentajeHaber" value="" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label for="flatCancelarTransf">Cancelar Transf:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                 <input type="radio" name="flatCancelarTransf" value="flatCancelarTransfSI" id="flatCancelarTransfSI" />
                                Si &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="flatCancelarTransf" value="flatCancelarTransfNO" id="flatCancelarTransfNO" />
                                No
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                 <label for="ddlEntidadFinancieraID">Entidad Financiera:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="2">
                             <select id="ddlEntidadFinancieraID" name="ddlEntidadFinancieraID" class="form-control" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label for="txtNumCuenta">N° de Cuenta:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                 <input id="txtNumCuenta" type="text" class="form-control" name="txtNumCuenta" value="" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                 <label for="ddlTipoMonedaCuenta">Tipo Moneda:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="2">
                             <select id="ddlTipoMonedaCuenta" name="ddlTipoMonedaCuenta" class="form-control" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label for="flatCuentaOficial">Cuenta Oficial:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                 <input type="radio" name="flatCuentaOficial" value="flatCuentaOficialSI" id="flatCuentaOficialSI" />
                                Si &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="flatCuentaOficial" value="flatCuentaOficialNO" id="flatCuentaOficialNO" />
                                No
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                 <label for="ddlTTipoCambio">Tipo Cambio:&nbsp;&nbsp;&nbsp;</label>
                            </td>
                            <td colspan="2">
                             <select id="ddlTTipoCambio" name="ddlTTipoCambio" class="form-control" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: right">&nbsp;
                                </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align: right">
                                <input type="button" id="btnGrabarCuenta" class="boton"  style="text-align: center; width: 190px" name="name" value="Registrar Nueva Cuenta" />
                                <input type="button" id="btnGrabarCuentaTransferencia" class="boton" name="name" value="Registrar Cuenta Destino" style="text-align: center; width: 190px"/>
                                <%--<input type="button" id="btnLimpiarCampos" class="btn btn-sm" name="name" value="Limpiar" />--%>
                                <input type="button" id="btnLimpiarCamposNuevaCuenta" class="boton" name="name" value="Limpiar" />
                                <input type="button" id="btnLimpiarCamposDestino" class="boton" name="name" value="Limpiar Datos" />
                                <input type="button" id="btnSalir" class="boton" name="name" value="Salir" />
                            </td>
                        </tr>

                    </table>


                    <%--<table>
                        <tr>
                            <td colspan="4" style="text-align: right">
                                <input type="button" id="btnGrabarCuenta" class="btn btn-sm" name="name" value="Registrar Cuenta" />
                                <input type="button" id="btnLimpiarCampos" class="btn btn-sm" name="name" value="Limpiar" />
                                <input type="button" id="btnSalir" class="btn btn-sm" name="name" value="Salir" />
                            </td>
                        </tr>
                    </table>--%>
                </div>
            </div>
            <%--             <input id="padreCodigo" type="text" class="form-control" name="padreCodigo" value="" required="required"/>--%>
        </div>
        <!--------------------------------------------------------->
        <div id="DVSeleccionarCuentaTrasf" style="display: none">
            <div class="panel panel-default" style="background-color: rgba(245, 245, 245, 0);">
                <div class="panel-heading">
                    <%--panel-heading--%><%--panel-body--%>
                    <table id="tbFiltrosBusquedaCuentaTransf">
                        <tr>
                            <td>
                                <label for="ICueCodigoTransf">Codigo:&nbsp;</label>
                            </td>
                            <td>
                                <input id="ICueCodigoTransf" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="ICueCodigoTransf" value="" style="width: 70px;" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="ICueNombreTransf">Nombre:&nbsp;</label>
                            </td>
                            <td>
                                <input id="ICueNombreTransf" type="text" onkeypress="validarletras(e)" class="form-control" name="ICueNombreTransf" value="" style="width: 200px;" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <label for="ddlElementoIDTransf">Elemento:&nbsp;</label>
                            </td>
                            <td>
                                <select id="ddlElementoIDTransf" name="ddlElementoIDTransf" class="form-control" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>

                                <input type="button" style="text-align: center; width: 75px" id="btnBuscarCuentaTransf" class="boton" name="name" value="Buscar" />
                                <input type="button" style="text-align: center; width: 75px" id="btnLimpiarFiltrosCuentaTransf" class="boton" name="name" value="Limpiar" />
                                <input type="button" style="text-align: center; width: 75px" id="btnSalirSeleccionCuentaTransf" class="boton" name="name" value="Salir" />
                            </td>
                            <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
               <td>
                     <label for="txtCodCuentaPadreID"> Cuenta:&nbsp;</label>
               </td>
               <td>
                     <input id="txtCodCuentaPadreID" type="text" class="form-control" name="txtCodCuentaPadreID" value="" style="width:80px;"/>
               </td>
               <td>&nbsp;&nbsp;&nbsp;</td> 
               <td>
                    <input type="button" style="text-align:center; width:120px" id="btnCrearCuentas" class="btn btn-sm" name="name" value="Crear SubCuenta" />
               </td>--%>
                        </tr>

                    </table>
                    <br />
                    <table>
                        <tr>
                            <td>
                                <label for="txtCueCodigoTransf">El codigo de la cuenta seleccionada es:&nbsp;&nbsp;</label>
                            </td>
                            <td>
                                <input id="txtCueCodigoTransf" type="text" class="form-control" name="txtCueCodigoTransf" value="" style="width: 155px;" />
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <input type="button" id="btnSeleccionCuentaTransf" class="boton" name="name" value="Agregar Cuenta" />
                            </td>
                        </tr>
                    </table>
                    <%--  <hr style="border: 1px solid #D8D8D8;" />--%>
                </div>
                <div class="panel-body">
                    <!------------------------ JqGrid ------------------------->
                    <div id="SeleccionCuentaGrid">
                    </div>
                    <!--------------------------------------------------------->
                </div>
            </div>

        </div>
        <div id="DVMensajeRegistroCuenta" style="display: none">
            <table>
                <tr>
                    <td>
                       <span class="ui-icon ui-icon-check" style="float: left; font-size: 8px; margin: 0 7px 50px 0;">
            </span><label>Se registro exitosamente la Cuenta Codigo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><label id="CodigoCuentaCreado"></label>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right">
                        <input type="button" style="text-align: center; width: 120px" id="btnCrearCuentaDestinoMens" class="boton" name="name" value="Crear Destino" />
                      <%--  <input type="button" style="text-align: center; width: 90px" id="btnAceptarMensaje" class="btn btn-sm" name="name" value="Aceptar" />--%>
                        <input type="button" style="text-align: center; width: 90px" id="btnSalirMensaje" class="boton" name="name" value="Cerrar" />
                    </td>
                </tr>
            </table>
            <br/><br/>
        </div>
    </div>
    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
    <script type="text/javascript" src="../../Scripts/JS_SEC/Contabilidad/Maestros/PlanDeCuenta/SEC_Cuentas.js"></script>
</asp:Content>
