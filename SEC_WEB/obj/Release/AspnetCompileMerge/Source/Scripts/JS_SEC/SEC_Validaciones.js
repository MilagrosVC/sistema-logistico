﻿//*VALIDACION PARA QUE CAMPO SOLO PERMITA NUMEROS*//
function validarnumeros(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla == 8) return true; //Tecla de retroceso (para poder borrar) 
    patron = /\d/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

//*VALIDACION PARA QUE CAMPO SOLO PERMITA LETRAS SIN TILDES*//
function validarletrasintildes() {
    if ((event.keyCode != 32) && (event.keyCode < 65) ||
        (event.keyCode > 90) && (event.keyCode < 97) ||
        (event.keyCode > 122))
        event.returnValue = false;
}

//*VALIDACION PARA QUE CAMPO SOLO PERMITA LETRAS CON TILDES*//
function validarletras(e) {
    tecla = (document.all) ? e.keyCode : e.which; // 2
    if (tecla == 8) return true; // 3
    if (tecla == 9) return true; // 3
    if (tecla == 11) return true; // 3
    patron = /[A-Za-zñÑ'áéíóúÁÉÍÓÚàèìòùÀÈÌÒÙâêîôûÂÊÎÔÛÑñäëïöüÄËÏÖÜ\s\t]/; // 4
    te = String.fromCharCode(tecla); // 5
    return patron.test(te); // 6

}

//*VALIDACION PARA QUE CAMPO ACEPTE MAYORES DE EDAD*//
function MayoresdeEdad() {
    var date = new Date(); //Declarar variable Date
    var year = date.getFullYear();     //Obtenemos el año actual
    var limite = date.setYear(year - 18); //Obtener 18 años antes al actual, la cual será el limite
    var fecha = new Date(limite);     //Enviamos la fecha limite como parametro
    var dia, mes, anio;     //declaramos variables 
    if ((fecha.getMonth() + 1) < 10) { mes = "0" + (fecha.getMonth() + 1); } else { mes = (fecha.getMonth() + 1); }     //Condicionales
    if (fecha.getDate() < 10) { dia = "0" + fecha.getDate(); } else { dia = fecha.getDate(); }      //Condicionales
    var Final = fecha.getFullYear() + '-' + mes + '-' + dia;     //Se concatena la fecha
    return Final;
}

function ConvertFecha(Fecha) {
    var date = new Date(parseInt(Fecha.substr(6))); //Declarar variable Date
    var dia = date.getDate();
    var mes = (date.getMonth() + 1);     //declaramos variables
    if (mes < 10) { mes = "0" + mes; }     //Condicionales
    if (dia < 10) { dia = "0" + dia; }      //Condicionales
    var Final = date.getFullYear() + "/" + mes + "/" + dia;     //Se concatena la fecha
    return Final;
}

function ConvertFechaFormatoNormal(Fecha) {
    var date = new Date(parseInt(Fecha.substr(6))); //Declarar variable Date
    var dia = date.getDate();
    var mes = (date.getMonth() + 1);     //declaramos variables
    if (mes < 10) { mes = "0" + mes; }     //Condicionales
    if (dia < 10) { dia = "0" + dia; }      //Condicionales
    var Final = dia + "-" + mes + "-" + date.getFullYear();     //Se concatena la fecha
    return Final;
}

//VALIDACION PARA NUMEROS Y LETRAS SIN ALFANUMERICOS*//
function LetrasyNumeros(e) {
    k = (document.all) ? e.keyCode : e.which;
    if (k == 8 || k == 0) return true;
    patron = /\w/;
    n = String.fromCharCode(k);
    return patron.test(n);
}

//VALIDACION LETRAS Y ALFANUMERICOS *//
function LetrasyAlfanumericos(e) {
    k = (document.all) ? e.keyCode : e.which;
    if (k == 8 || k == 0) return true;
    patron = /\D/;
    n = String.fromCharCode(k);
    return patron.test(n);
}

//VALIDACION SOLO ALFANUMERICOS*//
function Alfanumericos(e) {
    k = (document.all) ? e.keyCode : e.which;
    if (k == 8 || k == 0) return true;
    patron = /\W/;
    n = String.fromCharCode(k);
    return patron.test(n);
}

//VALIDACION PERMITE * # y Numeros USADO PARA TELEFONOS//
function validartelefono(e) { 
    tecla = (document.all) ? e.keyCode : e.which; 
    if (tecla == 8) return true; 
    if (tecla == 9) return true;
    if (tecla == 11) return true;
    patron = /[^a-z\s\t\^.;,+=?${}()!/¡¿"|°&"%¨¨?-]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

//Montos
//function acceptNum(evt) {
//    var key = nav4 ? evt.which : evt.keyCode;
//    return (key = 188 || (key >= 48 && key <= 57));
//}

//VALIDAR MONTOS13

//function ValidarMontos(e) {
//    tecla = (document.all) ? e.keyCode : e.which;
//    if (tecla == 8) return true; //Tecla de retroceso (para poder borrar) 
//    patron = /[0-9\.]/g;
//    te = String.fromCharCode(tecla);
//    return patron.test(te);
//}

function ValidarMonto(e, field) {
    key = e.keyCode ? e.keyCode : e.which
    if (key == 8) return true
    if (key > 47 && key < 58) {
        if (field.value == "") return true
        regexp = /.[0-9]{5}$/
        return !(regexp.test(field.value))
    }
    if (key == 46) {
        if (field.value == "") return false
        regexp = /^[0-9]+$/
        return regexp.test(field.value)
    }
    return false
}

function EspacioBlancoInicioCadena(control) {
    $('#' + control).keypress(function (event) {
        var element = this;
        var text = String.fromCharCode(event.which);
        var patron = /\s/;
        if ($('#' + control).val() != "") { }
        else {
            if (text.search(patron) == 0) {
                event.preventDefault();
            }
        }
    });
    $('#' + control).on('paste', function () {
        var element = this;
        setTimeout(function () {
            var text = $(element).val();
            var patron = /\s/;
            if (text.search(patron) == 0) {
                MensajeFocus("Aviso", "No se acepta espacio en blanco delante o campos llenos de ellos " + '"' + text + '"', control);
                $(element).val('');
            }
        }, 100);
    });
};



function sinEspaciosEnBlanco(control) {
    $('#' + control).keypress(function (event) {
        var element = this;
        var text = String.fromCharCode(event.which);
        var patron = /\s/;
        if (text.search(patron) != -1) {
            event.preventDefault();
        }
    });
    $('#' + control).on('paste', function () {
        var element = this;
        setTimeout(function () {
            var text = $(element).val();
            var patron = /( )/g;
            $(element).val(text.replace(patron, ""));
        }, 100);
    });
}


//Funciones para Mensaje --> En SEC MASTER se encuentra el Dialog

//Mensaje
function Mensaje(Titulo, Mensaje, vfunction) {
    //$("#dialog:ui-dialog").dialog("destroy");
    $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        maxWidth: 500,
        minWidth: 400,
        closeOnEscape: false,
        dialogClass: 'hide-close',
        title: Titulo,//title: "Mensaje",
        buttons: {
            "Aceptar": function () {
                try { vfunction(); } catch (e) { }

                $(this).dialog("close");                
            }
        },
        open: function (event, ui) {
            //$('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('btn btn-sm');
            //addClass('btn btn-sm');
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
    $("#divMensaje").html(Mensaje);
}

 //Confirmacion de Mensaje
function ConfirmacionMensaje(Titulo, Mensaje, vfunction1, vfunction2) {
    $("#divMensaje").html(Mensaje);
    $("#dialog:ui-dialog").dialog("destroy");


    $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        title: Titulo,
        maxWidth: 500,
        minWidth: 400,
        closeOnEscape: false,
        dialogClass: 'hide-close',
        buttons: {
            "Aceptar": function () {
                try {
                    vfunction1();
                } catch (e) {
                }
                $(this).dialog("close");
            },
            "Cancelar": function () {
                try {
                    vfunction2();
                } catch (e) {
                }
                $(this).dialog("close");
            }
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            //$('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('btn btn-primary btn-sm');
            //addClass('btn btn-sm');
        }
    });
}

//Mensaje Focus
function MensajeFocus(Titulo, Mensaje, control, vfunction) {
    $("#divMensaje").html(Mensaje);
    $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        maxWidth: 500,
        minWidth: 400,
        // minHeight: 500,        
        title: Titulo,
        buttons: {
            "Aceptar": function () {
                try { vfunction(); } catch (e) { }

                $(this).dialog("close");
                $('#' + control).focus();

            }
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
}


function MensajeLogin(Titulo, Mensaje,Estado) {
    //$("#dialog:ui-dialog").dialog("destroy");
    $("#dialog-message").dialog({
        modal: true,
        resizable: false,
        draggable: false,
        maxWidth: 500,
        minWidth: 400,
        closeOnEscape: true,
        dialogClass: 'hide-close',
        title: Titulo,//title: "Mensaje",
        buttons: {
            /*Aceptar: function () {
                try { vfunction(); } catch (e) { }

                $(this).dialog("close");


            }*/
        },
        open: function (event, ui) {
            //$('.ui-dialog-buttonpane').find('button:contains("Crear")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
    if (Estado == 1) {
        $("#divMensaje").html(Mensaje);
        $("#divMensaje").append("<i class='fa fa-check-circle' style='float: left; font-size: 18px; margin: 0 7px 50px 0;'></i>");
    }
    else {
        $("#divMensaje").html(Mensaje);
        $("#divMensaje").append("<i class='fa fa-user-times' style='float: left; font-size: 18px; margin: 0 7px 50px 0;'></i>");
    }    
}



//function LimitarDigitos(control, cantidadmaxima) {
//var 



//}