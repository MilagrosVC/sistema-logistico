﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="BandejaOrdenCompra.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.BandejaOrdenCompra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
    #MainSEC {
        padding: 0 10px 0 10px;
        color: #444;

        font-size:14px;
    }
    #tbBusqueda td {
        padding:3px 5px;
    }
    .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
    </style>
    <link href="../../Scripts/Styles_SEC/SEC_Orden_Compra.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3><b>Bandeja de Orden de Compra</b></h3>
    <div style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">Filtros de Busqueda</b>
        </div>
            <div style="margin:10px 0;">
                <table id="tbBusqueda">
                    <tr>
                        <td><label>Orden Compra N°:</label></td>
                        <td><input type="text" id="txtCodOrdenCompra" name="txtCodOrdenCompra" maxlength="12" placeholder="Ingrese N°" class="inputs" style="width:160px;"/></td>
                        <td><label>Desde :</label></td> <%--style="padding-left:20px;"--%>
                        <td><input type="text" id="txtDesde" name="txtDesde" readonly="readonly" placeholder="dd/mm/aaaa" style="width:160px;"class="inputs"/></td>
                        <td><label>Hasta :</label></td>
                        <td><input type="text" id="txtHasta" name="txtHasta" placeholder="dd/mm/aaaa" class="inputs" style="width:160px;"/></td>
                        <td><label>Proveedor :</label></td>
                        <td>
                            <select id="ddlProveedorBanOC" name="ddlProveedorBanOC" class="inputs" style="width:250px;">
                            </select>
                        </td>
                    </tr>
                    <tr>    
                        <td><label>Estado :</label></td>
                        <td>
                            <select id="ddlEstadoBanOC" name="ddlEstadoBanOC" class="inputs" style="width:160px;">
                            </select>
                        </td>
                        <td><label>Tipo :</label></td>
                        <td>
                            <select id="ddlTipoBanOC" name="ddlTipoBanOC" class="inputs" style="width:160px;">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <input type="button" id="btnBuscar" name="btnBuscar" value="Buscar" class="boton" />
                            <input type="button" id="btnLimpiar" name="btnLimpiar" value="Limpiar" class="boton"/>
                                <input type="button" id="btnCrear" name="btnCrear" value="Crear Orden Compra" class="boton" style="width:200px" />

                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <br />
    <div id="divOrdenCompra" style="width:100%;"></div>
    <%--<br />--%>

    <div id="dvVerOrdenCompra" style="display:none"> <%--style="display:none"--%>
        <div id="divImprimirOC">
        <h4 style="text-align:center;"><label id="lblrq-Titulo">ORDEN DE COMPRA</label></h4>
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>
        <label id="lblOC-CodOrdenCompra" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerOC" style="font-size:10px;">
            <tr>
                <td>FECHA :</td><td class="td-lbl"><label id="lblOC-Fecha"></label></td>
            </tr>
            <tr>
                <td>RAZON SOCIAL :</td><td class="td-lbl"><label id="lblOC-RazonSocial"></label></td>
            </tr>
            <tr>
                <td>DOMICILIO :</td><td class="td-lbl"><label id="lblOC-Domicilio"></label></td>
            </tr>  
            <tr>
                <td>CUIDAD :</td><td class="td-lbl"><label id="lblOC-Ciudad"></label></td>
            </tr> 
            <tr>
                <td>TELEFONO :</td><td class="td-lbl"><label id="lblOC-Telefono"></label></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="2">
                    <table id="tbOC-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:300px">ARTICULO</th>
                            <th style="width:60px">PRECIO UND</th>
                            <th style="width:100px">TOTAL</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <table id="tbOC-Anticipo" style="font-size:10px;">
            <!--<caption style="font-size:12px;font-weight:bold;color:#000;">ANTICIPOS</caption>
            <tr>
                <th style='width:50px;border: 1px solid #000;'>ITEM</th>
                <th style='width:150px;border: 1px solid #000;'>MONEDA</th>
                <th style='width:150px;border: 1px solid #000;'>MONTO SOLICITADO</th>
                <th colspan="2" style='width:300px;border: 1px solid #000;'>MEDIO PAGO</th>
                <th style='width:150px;border: 1px solid #000;'>TIPO ANTICIPO</th>
            </tr>
            <tr><td style="border: 1px solid #000;">&nbsp</td><td style="border: 1px solid #000;"></td><td style="border: 1px solid #000;"></td><td colspan="2" style="border: 1px solid #000;"></td><td style="border: 1px solid #000;"></td></tr>
        --></table>
        </div>

        <br /><br />
        <div style="text-align:center;">
            <input type="button" id="btnImprimirOC" name="name" value="Imprimir" class="boton"/>
            <input type="button" id="btnCerrarVerOC" name="name" value="Cerrar" class="boton"/>   
            <input type="button" id="btnExport" value="Generar Excel" class="boton" />
        </div>
    </div>  


        <div id="SEC_TotalesOC">
        <table>
            <tr>
                <td><b>TOTAL:</b></td>
                <td style="border-right:2px solid"><label id="OCTotal"></label></td>

                <td style="padding-left:18px;">Requiere Anticipo:</td>
                <td style="border-right:2px solid"><label id="OCReqAnticipo"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
                
                <td style="padding-left:18px;">Aprobada: </td>
                <td style="border-right:2px solid"><label id="OCAprobados"></label><span style="margin:0 5px;padding-left:18px;background-color:#C6F296;border:1px solid;border-color:#222;"></span></td>
                
                <td style="padding-left:18px;">Denegada:</td>
               <td style="border-right:2px solid"><label id="OCDenegada"></label><span style="margin:0 5px;padding-left:18px;background-color:#D56A6A;border:1px solid;border-color:#222;"></span></td>
               
                 <td style="padding-left:18px;">Cerrada:</td>
                <td><label id="OCCerrada"></label><span style="margin-left:5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td> <%--C6B6C1--%>
            
            </tr>
        </table>
    </div>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
    <script src="../../Scripts/JS_SEC/Logistica/Compras/BandejaOrdenCompra.js"></script>
</asp:Content>
