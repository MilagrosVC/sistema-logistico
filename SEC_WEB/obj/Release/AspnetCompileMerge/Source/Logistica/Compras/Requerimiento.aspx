﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Requerimiento.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.Solicitud" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        select {
            height:20px;
        }

        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            border-radius:4px;
            border-color: #9d9d9d;
            height:28px;
            padding-left:10px;
        }

        #btnVolver{
            float:right;
            width:100px;
            margin-top:20px;
        }
        #MainSEC {
        padding: 0 10px 0 10px;
        color: #444;
        font-size:13px;
        }

        input[type="button"]{
            width:120px;
        }
        input[type="text"]{
            width:120px;
        }
        #tbBusqueda td {
            padding:3px 5px;
        }
        #SEC_TotalesBandRQ td {
            padding:0 5px;
        }
        #SEC_TotalesBandRQ label {
            margin-top:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3 style="display:inline-block;"><b>REQUERIMIENTO</b></h3><input class="boton" type="button" id="btnVolver" name="btnVolver" value="Volver" />    
    <div id="tbIngreso" style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">DATOS DE REQUERIMIENTO</b>
        </div>
        <br />
        <table id="tbBusqueda">
        <tr>
            <td><label>Fecha Requerimiento :</label></td>
            <td style="padding-left:15px;"><label id="lblFechaRequerimiento"></label> <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png" /></td>
            <td style="padding-left:30px;"><label>Fecha de Entrega :</label></td>
            <td style="padding-left:15px;"><input type="text" name="txtFechaEntrega" id="txtFechaEntrega" class="inputs" /></td>
            <td><label>Turno :</label></td>
            <td style="padding-left:15px;">
                <select id="ddlTurno" name="ddlTurno" class="form-control" style="width:200px;">
                </select>
            </td>
            <td style="padding-left:15px;"> 
                <label>Tipo Servicio :</label></td>
            <td style="padding-left:15px;">
                
                <select id="ddlTipoServicioRQ" name="ddlTipoServicioRQ" class="form-control" style="height:30px;width:200px;">
                </select></td>
            <td style="padding-left:15px;" id="tdIMG"></td>
        </tr>
        <tr style="height:5px;">
            <td></td>
        </tr>
        <tr>
            <td><label>Proyecto :</label></td>
            <td style="padding-left:15px;" colspan="3">
                <label id="lblProyecto"></label>
            </td>
            <td>
                <label>Especialidad :</label></td>
            <td style="padding-left:15px;">
                
                <select name="ddlPartida" id="ddlPartida" class="form-control" style="height:30px; width:200px;">
                </select></td>
            <td style="padding-left:15px;">
                <label>Partida :</label></td>
            <td id="tdSubPartida" style="padding-left:15px;">
                <select name="ddlSubPartida" id="ddlSubPartida" class="form-control" style="display:inline-block;height:30px;width:200px;">
                </select><!--<i id="ImgMostarItems" title="Mostrar Items" class='fa fa-eye' style="cursor:pointer;padding-left:10px;" ></i>--></td>
            <td style="padding-left:15px;">
                </td>
        </tr>
        <tr style="height:5px;">
            <td></td>
        </tr>
        <tr>
            <td><label>Direccion :</label></td>
            <td style="padding-left:15px;" colspan="5">
                <label id="lblDireccion"></label>
                </td>
            <td id="td_Codigo" colspan="3" style="padding-left:15px;">
                <!--<label style="padding-right:16%;">Codigo :</label>
                <input type="text" name="txtCodigo" class="inputs" id="txtCodigo" style="height:30px;width:150px;display:inline-block;" />
                <img id="ImgAgregar" src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/agregar.png" alt="Agregar" title="Agrega Item" />-->
            </td>
        </tr>
        </table>    
    <br />
    <!--<div id="GrillaItemsReq" style="max-width:90%;padding-left:5px;padding-bottom:1%;" >
        <br />
        <div id="divReqDetalle"></div>    
    <br />
        <!--<input class="boton" type="button" id="btnFinalizar" name="btnFinalizar" value="Cerrar Rq" />
    </div>-->

    </div>
    <br />
    <div id="dialog_Items" style="display:none;" title="BANDEJA DE SUB PARTIDAS" >
        <!--<i id="ImgOcultar" title="Ocultar" class="fa fa-minus-square" style="cursor:pointer;float:right;margin-right:20%;" ></i>-->
        <div id="GrillaItems" style="max-width:100%;">
        </div>
        
    </div>

    <br />
    <div id="dialogRequerimiento" style="display:none;" title="REGISTRO ITEM">
    <table id="tbAgregarItem" style="margin:auto;width:100%;">
        <tr>
            <td><label id="lblItemCodigo"></label></td>
            <td colspan="3" style="padding-left:15px;"><label id="lblItemDescripcion"></label></td>
        </tr>
        <tr style="height:5px;">
            <td>

            </td>
        </tr>
        <tr>
            <td><label>Seleccione :</label></td>
            <td colspan="3" style="padding-left:15px;">
                <p id="btnMostrarItemsMa" style="margin: 0 0 0 0;display:inline-block;cursor:pointer;"><label>Items </label> <i class="fa fa-search"></i></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" id="td_ItemsMa">
                <div>
                    <table style="width:100%">
                        <tr>
                            <td><label>Familia :</label></td>
                            <td style="padding-left:25px;" >
                                <label id="lblFamilia"></label>
                            </td>
                            <td style="padding-left:15px;"><label>Marca :</label></td>
                            <td style="padding-left:15px;">
                                <label id="lblMarca"></label>
                            </td>
                        </tr>
                        <tr style="height:5px;">
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td><label>Descripcion :</label></td>
                            <td colspan="3" style="padding-left:25px;">
                                <label id="lblDescripcionItemMa"></label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>            
        </tr>        
        <tr style="height:5px;">
            <td>

            </td>
        </tr>
        <tr>
            <td><label for="txtDescripcion">Descrip Prov:</label></td>
            <td colspan="3" style="padding-left:15px;">
                <input type="text" name="txtDescripcion" class="form-control" id="txtDescripcion" style="height:30px;width:100%" placeholder="Ingrese una descripcion" required="required" maxlength="300"/>
            </td>
        </tr>
        <tr style="height:5px;">
            <td>

            </td>
        </tr>
        <tr>
            <td><label for="txtCantidad">Cantidad :</label></td>
            <td style="padding-left:15px;"><input type="text" name="txtCantidad" id="txtCantidad" class="form-control" style="height:30px;width:100px;" onkeypress="return validarnumeros(event)" placeholder="Ingrese cantidad" maxlength="9"/></td>
            <td><label for="txtUnidad">UND</label></td>
            <td style="padding-left:15px;">
                <select name="ddlUndMedidaRQ" id="ddlUndMedidaRQ" class="form-control" style="height:30px;width:100px;">
                </select>
            </td>
        </tr>
        <tr style="height:5px;">
            <td>

            </td>
        </tr>
        <tr>
            <td style="width:100px;"><label for="txtObservacion">Observaciones:</label></td>
            <td colspan="3" style="padding-left:15px;">
                <textarea id="txtObservacion" name="txtObservacion" rows="5" cols="45" maxlength="300"></textarea>
            </td>
        </tr>
    </table>
    </div>

    <div id="dialog_ItemsMa" style="display:none;max-width:100%" title="BANDEJA DE ITEMS">
        <table style="width:80%;margin: 15px 0;">
            <tr>
                <td><label>Familia :</label></td>
                <td>
                    <select id="ddlFamilia" class="inputs" style="height:30px;width:200px;">                    
                    </select>
                </td>
                <td><label>Marca :</label></td>
                <td>
                    <select id="ddlMarca" class="inputs" style="height:30px;width:200px;">
                    </select>
                </td>
                <td>
                    <input type="button" id="btnFiltrar" value="Buscar" />
                </td>
                <td>
                    <input type="button" id="btnLimpiarItemMA" value="Limpiar" />
                </td>
            </tr>
        </table>
        <div id="divItemsMa" style="max-width:100%;">
        </div>
    </div>
    
    
    <!--<div id="GrillaItemsReq" style="border:solid 2px;">
        <br />
    <div id="divReqDetalle" ></div>    
    <br />
        <input type="button" id="btnFinalizar" name="btnFinalizar" value="Cerrar Requerimiento" />
    </div>-->
    

    <asp:HiddenField ID="hdf_Item_Id" runat="server" />
    <asp:HiddenField ID="hdf_ItemMa_Id" runat="server" />
    <asp:HiddenField ID="hdf_RequerimientoCabecera_Id" runat="server" />
    <asp:HiddenField ID="hdf_Usuario" runat="server" />
    <asp:HiddenField ID="hdf_Proyecto_Id" runat="server" />
    <asp:HiddenField ID="hdf_Presupuesto_Id" runat="server" />
    <asp:HiddenField ID="hdf_Requerimiento_Detalle_Id" runat="server" />
    <script src="../../Scripts/JS_SEC/Logistica/Compras/Requerimiento.js"></script>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
