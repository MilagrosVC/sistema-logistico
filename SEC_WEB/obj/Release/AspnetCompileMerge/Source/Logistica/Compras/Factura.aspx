﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Factura.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.Factura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    <style>
        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            border-radius:4px;
            border-color: #9d9d9d;
            height:28px;
            padding-left:10px;
        }
    </style>
    <link href="../../Scripts/Styles_SEC/SEC_Factura.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--  Hidden que sirven para almacenar las variables que se reciben de BANDEJA OC--%>

    <asp:HiddenField ID="hdf_Orden_Compra_Co" runat="server" />
    
    <h3><b>Registro de Facturas</b></h3>
    <div style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">Filtros de Busqueda</b>
        </div>
        <br />
            <div style="margin:10px 0;">
                <table id="tbBusqueda" style="display:inline-block">
                    <tr>
                        <td><label>Orden de Compra N°:</label></td>
                    
                        <td>
                            <input type="text" id="txtCodOrdenCompra" name="txtCodOrdenCompra" maxlength="12" class="inputs" />
                        </td>
                        <%--</tr>
                        <tr>--%>
                        <td>
                            <input type="button" id="btnBuscar" name="btnBuscar" value="Buscar" class="boton"/>                            
                        </td>
                        <td>
                            <input type="button" id="btnLimpiar" name="btnLimpiar" value="Limpiar" class="boton" />
                        </td>
                    </tr>
                </table>
                <input type="button" id="btnBandejaFac" name="btnBandejaFac" value="Ir a Bandeja de Factura" style="float:right;margin-right:20px; width:200px;" class="boton" />
                <input type="button" id="btnBandejaOC" name="btnBandejaOC" value="Ir a Bandeja Orden Compra" style="float:right;margin-right:20px;width:200px;" class="boton" />
                <div style="clear:both;"></div>
            </div>
        <br />
    </div>
    <br />

    <div id="divOrdenCompra" style="width:100%;"></div>

    <div id="divGenerarFactura" style="display:none">
        <div id="divImprimirCom">
        <h4 style="text-align:center;"><label>Registrar Factura</label></h4>
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>
        <div style="float:right;">
            <label>N°:</label>
            <input type="text" name="txtNumero" id="txtNumero" maxlength="12" class="inputs" />
        </div>
        <br style="clear:both;"/>
        <table id="tbVerCom" style="font-size:10px;">
            <tr>
                <td class="td-lbl1">SEÑOR(ES):</td><td class="td-lbl"><label id="lblCom-RazonSocial"></label></td>
                <td>FECHA:</td><td class="td-lbl"><input type="text" id="txtFechaFactura" name="txtFechaFactura" readonly="readonly" placeholder="aaaa/mm/dd"/></td>
            </tr>
            <tr>
                <td>DIRECCIÓN:</td><td colspan="3" class="td-lbl"><label id="lblCom-Direccion"></label></td>
            </tr>  
            <tr>
                <td>RUC :</td><td class="td-lbl"><label id="lblCom-RUC"></label></td>
                <td class="td-lbl1">GUíA DE REMISIÓN N°:</td>
                <td class="td-lbl">
                    <input type="text" id="txtGuiaRemision" name="txtGuiaRemision" class="inputs" />
                </td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="4">
                    <table id="tbCom-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:300px">ARTICULO</th>
                            <th style="width:60px">PRECIO UND</th>
                            <th style="width:100px">TOTAL</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--<table id="tbOC-Anticipo" style="font-size:10px;">
        </table>-->
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnRegistrar" name="name" value="Registrar" class="boton" />
            <input type="button" id="btnCancelar" name="name" value="Cancelar" class="boton" />   
            <!--<input type="button" id="btnExport" value="Generar Excel" />-->
        </div>
        <br />
    </div>
    <div id="dialogFactura" style="display:none;">
        <div id="divImprimirRQ">
        <h4 style="text-align:center;"><label id="lblrq-Titulo">FACTURA</label></h4>
        <div id="divLogoImpresion">
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png" id="LogoId" style="position:relative;top:-35px"/>
        </div>
        <label id="lblRq-CodRequerimiento" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerRq" style="font-size:10px;">
            <tr>
                <td>REQUERIDO POR :</td><td class="td-lbl"><label id="lblRq-Solicitante"></label></td>
            </tr>
            <tr>
                <td>FECHA DE REQUERIMIENTO :</td><td class="td-lbl"><label id="lblRq-FechaCreacion"></label></td>
            </tr>
            <tr>
                <td>FECHA DE ENTREGA :</td><td class="td-lbl"><label id="lblRq-FechaEntrega"></label></td>
            </tr>  
            <tr>
                <td>PROYECTO :</td><td class="td-lbl"><label id="lblRq-NomProyecto"></label></td>
            </tr> 
            <tr>
                <td>DIRECCION :</td><td class="td-lbl"><label id="lblRq-Direccion"></label></td>
            </tr>
            <tr>
                <td>ESPECIALIDAD :</td><td class="td-lbl"><label id="lblRq-Partida"></label></td>
            </tr>
            <tr>
                <td>PARTIDA :</td><td class="td-lbl" style="text-align:right;">TURNO :<label id="lblRq-Turno" style="margin:0 -10px 0 10px"></label></td>   
            </tr>
            <tr>
                <td colspan="2">
                    <ul id="lstRq-Subpartidas">
                        
                    </ul>
                </td>
            </tr>
            <tr>
                <td id="tdDetalle1" colspan="2">
                    <table id="tbRq-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">ITEM</th>
                            <th style="width:300px">DESCRIPCION</th>
                            <th style="width:70px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:196px">OBSERVACION</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnImprimirRQ" name="name" value="Imprimir" class="boton"/>
            <input type="button" id="btnCerrarVerRQ" name="name" value="Cerrar" class="boton"/>   
            
        </div>
    </div> 
    <asp:HiddenField id="hdf_Orden_Compra_Id" runat="server"/>
    <asp:HiddenField id="hdf_Usuario" runat="server"/>
    <asp:HiddenField id="hdf_Proveedor_Id" runat="server" />
    <asp:HiddenField id="hdf_Moneda_Id" runat="server" />
    <asp:HiddenField id="hdf_Com_IGV_Nu" runat="server" />
    <asp:HiddenField id="hdf_Com_SubTotal_Nu" runat="server" />
    <asp:HiddenField id="hdf_Com_Total_Nu" runat="server" />
    <script src="../../Scripts/JS_SEC/Logistica/Compras/Factura.js"></script>
</asp:Content>
