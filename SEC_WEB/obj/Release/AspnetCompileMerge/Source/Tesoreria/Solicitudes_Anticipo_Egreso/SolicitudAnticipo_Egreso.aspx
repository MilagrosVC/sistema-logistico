﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="SolicitudAnticipo_Egreso.aspx.cs" Inherits="SEC_WEB.Tesoreria.Solicitudes_Anticipo_Egreso.SolicitudAnticipo_Egreso1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../../Scripts/Styles_SEC/SEC_BandSolicitudAnticipo.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <asp:HiddenField ID="hdUsuario" runat="server" />
     <asp:HiddenField ID="hdIdPerfil" runat="server" />
    <input type="hidden" id="hdnIdEgreso" name="hdnIdEgreso" />
<%--    <input type="hidden" id="hdnIdPerfil" name="hdnIdPerfil" />--%>
    <div id="DVPrincipal"  style="margin-left:25px">
        <h3> Tesoreria - Bandeja de Solicitudes de Anticipo</h3>
        <div id="FiltrosBusqueda">
          <table id="TbFiltroBusq">
              <tr>
<%--            <td> 
                <label for="txtNumeroCotizacion"> Numero Cotizacion:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td> 
                <input id="txtNumeroCotizacion" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroCotizacion" value="" />
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
            <td> 
                <label for="ddlMoneda"> Moneda:&nbsp;&nbsp;</label>
            </td>
            <td> 
           <%--  <input id="txtMonedaId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtMonedaId" value="" />--%>
                 <select id="ddlMoneda" name="ddlMoneda" class="form-control" style="height:30px; width:150px;">
                    </select>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td> 
                <label for="ddlFormaPago"> Forma de Pago:&nbsp;&nbsp;</label>
            </td>
            <td> 
          <%--      <input id="txtFormaPagoId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtFormaPagoId" value="" />--%>
                <select id="ddlFormaPago" name="ddlFormaPago" class="form-control" style="height:30px; width:200px;">
                    </select>
            </td>
             <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td> 
                <label for="ddlTipoAnticipo"> Tipo Anticipo:&nbsp;&nbsp;</label>
            </td>
            <td> 
                <%--<input id="txtTipoAnticipoId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtTipoAnticipoId" value="" />--%>
            <select id="ddlTipoAnticipo" name="ddlTipoAnticipo" class="form-control" style="height:30px; width:138px;">
                    </select>
            </td>
                  </tr>
                  </table>
            <br />
                  <table><tr>
           <%-- <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                  <td>
        <input type="button" id="btnRealizarBusqueda" class="boton" name="name" value="Buscar" />
        <input type="button" id="btnLimpiarBusqueda" class="boton" name="name" value="Limpiar" /> 
                  </td>
           <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  <td>
 <input type="button" style="width:250px" id="btnVerEgresos" class="boton" name="name" value="Visualizar Solicitudes Atendidas" /> 
       
                  </td>
              </tr>
          </table>

            <br/>
        

        </div>

        <br/>
     <!------------------------ JqGrid ------------------------->
        <div id="SolicitudAnticipoGrid">
        </div>
    <!--------------------------------------------------------> 
        <br/>
        <div id="DvAtencionSolicitudAnticipo" style='display:none;'>
<%--         <h5>Solicitud de Anticipo</h5>--%>

            <div class="panel panel-default" style="background-color: rgba(245, 245, 245, 0);">
                <div class="panel-heading">
                    <table>
                 <tr>
                    <th colspan="8" style="text-align:center"> <h3><label>Detalle de Anticipo &nbsp;&nbsp;&nbsp;&nbsp; </label> </h3> </th>
                </tr>
                <tr>
                    <td>
                        <label for="txtCodigoSolicitud"> Codigo de Solicitud : </label>                
                    </td>
                    <td>
                        <input id="txtCodigoSolicitud" type="text" class="form-control" name="txtCodigoSolicitud" value="" required="required"/>
                    </td>
                    
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><label for="txtTipoPersona"> Tipo Persona : </label> </td>
                    <td>
                        <input id="txtTipoPersona" type="text" class="form-control" name="txtTipoPersona" value="" required="required"/>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td><label for="txtProveedor"> Proveedor : </label> </td>
                    <td>
                        <input id="txtProveedor" type="text" class="form-control" name="txtProveedor" value="" required="required"/>
                    </td>
                </tr>
          
                <tr>
                    <td>
                        <label for="txtCotizacionId"> Numero de Cotización : </label>  
                    </td>
                    <td>
                        <input id="txtCotizacionId" type="text" class="form-control" name="txtCotizacionId" value="" required="required"/>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <label for="ddlMonedaId"> Tipo de Moneda : </label>  
                    </td>
                    <td>
                        <%--<input id="txtMonedaId" type="text" class="form-control" name="txtMonedaId" value="" required="required"/>--%>
                        <select id="ddlMonedaId" name="ddlMonedaId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <label for="txtMonto"> Monto Solicitado : </label>  
                    </td>
                     <td>
                        <input id="txtMontoSolicitado" type="text" class="form-control" name="txtMontoSolicitado" value="" required="required"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="ddlTipoPagoId"> Tipo de Pago : </label> 
                    </td>
                    <td>
                    <select id="ddlTipoPagoId" name="ddlTipoPagoId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                    <td>
                    <label for="ddlTipoAnticipoId"> Tipo Anticipo : </label> 
                    </td>
                    <td>
                    <select id="ddlTipoAnticipoId" name="ddlTipoAnticipoId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>
                    <%--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                    <td>
                    <label for="ddlEstadoSolicitudId"> Estado de Solicitud : </label> 
                    </td>
                    <td>
                    <select id="ddlEstadoSolicitudId" name="ddlEstadoSolicitudId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>--%>

                </tr>
            </table>
                </div>
                 <div class="panel-body">
                     <div  id="DVDatosAtencionTesoreria">
                     <table id="TbDatosSolititados" >
                <tr>
                    <th colspan="8" style="text-align:center"> <h4><label>Datos Solicitados&nbsp;&nbsp;&nbsp;&nbsp; </label> </h4> </th>
                </tr>
                <tr>
                    <td id="tdNumChequeL">
                         <label for="txtNumeroCheque"> Numero de Cheque :&nbsp;&nbsp;&nbsp;&nbsp; </label> 
                    </td>
                    <td id="tdNumChequeI">
                       <input id="txtNumeroCheque" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroCheque" value=""/>
                    </td>
                     <td id="tdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                    <td id="tdNumOpeL">
                         <label for="txtNumeroOperacion">Numero de Operacion:&nbsp;</label> 
                    </td>
                    <td id="tdNumOpeI">
                       <input id="txtNumeroOperacion" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroOperacion" value=""/>
                    </td>
                    <td id="tdEsp2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td id="tdTipoCambioL">
                         <label for="txtTipoCambio">Tipo Cambio:&nbsp;</label> 
                    </td>
                    <td id="tdTipoCambioI">
                       <input id="txtTipoCambio" type="text" onkeypress="return ValidarMonto(event, this);" class="form-control" name="txtTipoCambio" value=""/>
                    </td>
                </tr>
                <tr>
                    <td id="tdBancoOriL">
                         <label for="dllBancoOrigenId">Banco Origen:&nbsp;</label> 
                    </td>
                    <td id="tdBancoOriI">
                         <select id="dllBancoOrigenId" name="dllBancoOrigenId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>
                    <td id="tdEsp3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td id="tdBancoDesL">
                         <label for="dllBancoDestinoId">Banco Destino:&nbsp;</label> 
                    </td>
                    <td id="tdBancoDesI">
                         <select id="dllBancoDestinoId" name="dllBancoDestinoId" class="form-control" style="height:30px; width:200px;">
                    </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td id="tdNumCuentaOriL">
                         <label for="txtNumeroCuentaOrigen">Numero Cuenta Origen:&nbsp;</label> 
                    </td>
                    <td id="tdNumCuentaOriI">
                        <input id="txtNumeroCuentaOrigen" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroCuentaOrigen" value=""/>
                    </td>
                    <td id="tdEsp4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td id="tdNumCuentaDesL">
                         <label for="txtNumeroCuentaDestino">Numero Cuenta Destino:&nbsp;</label> 
                    </td>
                    <td id="tdNumCuentaDesI">
                        <input id="txtNumeroCuentaDestino" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroCuentaDestino" value=""/>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="8" style="text-align:right" >
                        <input type="button" id="btnGrabarPagoSolicitado" class="boton" name="name" value="Registrar Pago" />
                        <input type="button" id="btnModificarEgreso" class="boton" name="name" value="Modificar" />
                        <input type="button" id="btnLimpiarCampos" class="boton" name="name" value="Limpiar" />
                        <input type="button" id="btnSalirPantallaAtencion" class="boton" name="name" value="Salir" />
                    </td>
                </tr>
            </table>
                     </div>
                     <div id="DVDatosAtencionGerente">
                         <table id="TbDatosAprobacion">
                             <tr>
                                 <td> <label for="">Determinar Resultado:&nbsp;</label> </td>
                                 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 <td>
                                     <input type="radio" name="EstadoAnticipo" value="EstadoAprobada" id="EstadoAprobada" />
                                Aprobada &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="EstadoAnticipo" value="EstadoDenegada" id="EstadoDenegada" />
                                Denegada
                                 </td>
                              <%--   </tr>
                             <tr>--%>
                                  <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td> <label for="">Ingresar Observación:&nbsp;</label> </td>
                                 <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 <%-- <td><input id="txtObservacion" type="text" class="form-control" name="txtObservacion" value=""/></td>--%>
                                 <td>
                                  <textarea cols="52" rows="2" id="txtObservacion" class="form-control" name="txtObservacion"  style="resize:none"></textarea></td>
                             </tr>
                             <tr>
                                 <td colspan="4">
                                     <input type="button" id="btnGrabarAprobacion" class="boton" name="name" value="Grabar" />
                                     <input type="button" id="btnLimpiarPantallaGerente" class="boton" name="name" value="Limpiar" />
                                     <input type="button" id="btnSalirPantallaGerente" class="boton" name="name" value="Salir" />
                                 </td>
                             </tr>
                         </table>
                     </div>
                 </div>
            </div>
            
            <!--<br/>-->
            
        </div>
 <!--------------------------------------------------------> 
        <br/>
        <div id="DVVisualizarEgreso" style='display:none;'>
       <%--     <h3> Egresos Realizados</h3>--%>
            <br/>
 
            <table id="Filtros de Busqueda">
                <tr>
                    <td> <label for="ddlMonedaEgreso"> Moneda:&nbsp;</label></td>
                    <td> <select id="ddlMonedaEgreso" name="ddlMonedaEgreso" class="form-control" style="height:30px; width:130px;">
                    </select> </td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td> <label for="ddlFormaPagoEgreso"> Forma Pago:&nbsp;</label></td>
                    <td> <select id="ddlFormaPagoEgreso" name="ddlFormaPagoEgreso" class="form-control" style="height:30px; width:200px;">
                    </select> </td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td> <label for="ddlTipoAnticipoEgreso"> Tipo Anticipo:&nbsp;</label></td>
                    <td> <select id="ddlTipoAnticipoEgreso" name="ddlTipoAnticipoEgreso" class="form-control" style="height:30px; width:130px;">
                    </select> </td>
             <%--       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                     <td> <label for="ddlBancoOrigenEgreso"> Banco Origen:&nbsp;</label></td>
                    <td> <select id="ddlBancoOrigenEgreso" name="ddlBancoOrigenEgreso" class="form-control" style="height:30px; width:160px;">
                    </select> </td>
                     <td>&nbsp;&nbsp;&nbsp;</td>
                     <td> <label for="ddlBancoDestinoEgreso"> Banco Destino:&nbsp;</label></td>
                  <td> <select id="ddlBancoDestinoEgreso" name="ddlBancoDestinoEgreso" class="form-control" style="height:30px; width:160px;">
                    </select> </td>
                    </tr>
                <tr>
                    <td colspan="14">&nbsp;&nbsp;&nbsp;</td>
                </tr>
                </table>
            <table>
                <tr>
                    <td colspan="14"> <%-- style="text-align:right"--%>
                        <input type="button" id="btnRealizarBusquedaEgreso" class="boton" name="name" value="Buscar" />
                        <input type="button" id="btnLimpiarBusquedaEgreso" class="boton" name="name" value="Limpiar" /> 
                        <input type="button" id="btnSalirPantallaEgreso" class="boton" name="name" value="Salir" /> 
                    </td>
                </tr>
                </table>
       
            <br />
            <div id="EgresoGrid">
            </div>

        </div>

        </div>
     <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
    <script type="text/javascript" src="../../Scripts/JS_SEC/Tesoreria/SolicitudAnticipo_Egreso/SEC_SolicitudAnticipo.js"></script>
</asp:Content>
