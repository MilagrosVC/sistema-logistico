﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Acceso
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        
        {
            
        }

        [WebMethod]
        public static List<UsuarioBE> ValidarUsuarioLista(UsuarioBE ObjUsuario)
        {
            UsuarioBL ObjUsuarioBL = new UsuarioBL();
            List<UsuarioBE> lstUsuario = new List<UsuarioBE>();
            lstUsuario = ObjUsuarioBL.ValidarUsuarioLista(ObjUsuario);
            //Session.Add(Constantes.SessionVariables.Usuario, lstUsuario[0].Usu_Id);
            if (lstUsuario.Count == 1)
            {
                HttpContext.Current.Session.Add(Constantes.SessionVariables.Usuario, lstUsuario[0].Usu_Id);
                HttpContext.Current.Session.Add(Constantes.SessionVariables.Proyecto_Id, 1);
                HttpContext.Current.Session.Add(Constantes.SessionVariables.Presupuesto, 3);
                HttpContext.Current.Session.Add(Constantes.SessionVariables.PerfilId, lstUsuario[0].Per_Id);
            }
            // = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            return lstUsuario;
        }

        [WebMethod]
        public static int ValidarUsuario(UsuarioBE ObjUsuario)
        {
            UsuarioBL ObjUsuarioBL = new UsuarioBL();
            int id = 0;
            id = ObjUsuarioBL.ValidarUsuario(ObjUsuario);
            return id;
        }
        [WebMethod]
        public static string generarMenu(UsuarioBE ObjUsuario)
        {
            var strMenu = "";
            PerfilDetalleBL BL = new PerfilDetalleBL();
            //string defaultPage = "../Home/Form/Home.aspx";
            strMenu = BL.generarMenu(ObjUsuario);
            //strMenu = BL.GenerarMenu(ref defaultPage);
            HttpContext.Current.Session.Add(Constantes.SessionVariables.Menu, strMenu);
            //Session.Add(SAR.Util.Constantes.SessionVars.PaginaPerfil, defaultPage);
            return strMenu;
        }
    }
}