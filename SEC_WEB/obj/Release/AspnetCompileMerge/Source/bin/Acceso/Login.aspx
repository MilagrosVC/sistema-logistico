﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SEC_WEB.Acceso.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Acceso SEC</title>
    <link href="../Scripts/JQuery-ui-1.11.4/jquery-ui.css" rel="stylesheet" />
    <link href="../Scripts/JQuery-ui-1.11.4/font-awesome.min.css" rel="stylesheet" />
    <link href="../Scripts/Styles_SEC/Login.css" rel="stylesheet" /> 
    <style>
        .ui-widget-header {
            border: 1px solid #9d9d9d;
            background:-webkit-gradient(linear, left top, left bottom, from(#9d9d9d), to(#222));
            /*background:-ms-linear-gradient(left top, left bottom, from(#9d9d9d), to(#222));*/
            background: -ms-linear-gradient(top, #9d9d9d 2%,#222 100%);
            background:-moz-gradient(linear, left top, left bottom, from(#9d9d9d), to(#222));
            line-height:30px;
            height:30px;
        }
        .ui-dialog-title {
            /*font-size:8px;*/
        }
        .ui-button-icon-only .ui-button-text, .ui-button-icons-only .ui-button-text {
            padding:0px;
        }

    </style>   
</head>
<body>
    <form id="form1" runat="server">
    <div class="cabecera">
        <h1 style="padding:1% 2% 0 2%;">SEC</h1>
        <p style="font-size:12px;">Sistema ERP de Construcción</p>
    </div>
    <div id="Login">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;text-align:center;height:40px;">
            <b style="color:#fff;font-size:16px;display:block;padding-top:3%;">Inicio de Sesión <i class="fa fa-sign-in"></i></b>
        </div>
        <br />
        <table>
            <tr>
                <td><label for="txtUsuario">Usuario :</label></td>                
            </tr>
            <tr>
                <td>
                    <input type="text" name="txtUsuario" id="txtUsuario" />
                    <i class="fa fa-user" title="Ingrese Usuario"></i>
                    <!--<asp:TextBox ID="txtUsuario" runat="server" /> -->
                </td>                 
            </tr>   
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td><label for="txtPassword">Password :</label></td>
            </tr>
            <tr>
                <td>
                    <input type="password" name="txtPassword" id="txtPassword" />
                    <i class="fa fa-key" title="Ingrese Password"></i>
                    <!--<asp:TextBox ID="txtPassword" runat="server" />-->
                    
                </td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;">
                    <input type="button" class="button" name="btnLogin" id="btnLogin" value="Entrar" />
                    <input type="button" class="button" name="btnLimpiar" id="btnLimpiar" value="Limpiar" />
                </td>
            </tr>
        </table>
        <br />
    </div>
    <div id="dialog-message" style="display: none">
        <br />
    <div id="divMensaje">
    </div>
    </div>
    <asp:HiddenField ID="hdf_Usuario" runat="server" />
    </form>
    <script type="text/javascript">
        function CloseWindow() {
            window.close();
        }
    </script>
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JQuery-1.11.3/jquery-1.11.3.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JQuery-ui-1.11.4/jquery-ui.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveUrl("../Scripts/JS_SEC/SEC_Validaciones.js") %>'></script>    
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JS_SEC/Acceso/Login.js") %>'></script>      
</body>
</html>
