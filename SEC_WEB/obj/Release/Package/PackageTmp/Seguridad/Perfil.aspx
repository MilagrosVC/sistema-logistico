﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="SEC_WEB.Seguridad.Perfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div style=" width:100%; padding-left:15px; padding-right:15px;">--%>

    <div style="background:#000;padding:8px;width:100%;" id="divTitle">
        <span style="color:#fff;font-size:12px;font-weight:bold;">MANTENIMIENTO DE PERFILES</span>
    </div>
    <br />
    <h3 class="ui-widget-header ui-corner-top ui-helper-reset" onclick="OcultarDiv('divCliente');OcultarDiv('divIngresodetalle');">
        <span style="display: block; margin: 4px;">Filtros de Búsqueda</span>
    </h3>
    <div class="ui-accordion ui-widget ui-helper-reset ui-accordion-icons" style="width: 100%" id="divFiltro">
        <div id="divIngresodetalle" class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active">
            <input type="button" id="btnCrearPerfil" value="Crear Perfil" class="btn btn-primary btn-sm" />&nbsp;&nbsp;&nbsp;
            <input type="button" id="btnBuscarPerfil" value="Buscar" class="btn btn-primary btn-sm" />
        </div>
    </div>
    <br />
    <div id="pnlPerfil" style="width: 100%">
    </div>

    <div id="dialog-form" title="Registro de Perfil" style="width: 500px; display: none;">

        <p class="validateTips">
        </p>
        <fieldset>
            <table style="width: 100%;">
                <tr>
                    <td>&nbsp;<label for="txtNombrePerfil">Nombre del Perfil</label></td>
                    <td>&nbsp;<input type="text" name="txtNombre" id="txtNombre" class="text ui-widget-content ui-corner-all" style="width: 300px" /></td>
                </tr>
                <tr>
                    <td>&nbsp;<label for="txtDescripcion">Descripción</label></td>
                    <td>&nbsp;<textarea name="txtAreaDescripcion" id="txtAreaDescripcion" class="text ui-widget-content ui-corner-all" style="width: 300px; height: 100px;" maxlength="300"></textarea></td>
                </tr>

            </table>
            <input type="hidden" id="hdnId" name="Id" value="" datatype="int" />
        </fieldset>
    </div>

    <script src="../Scripts/JS_SEC/Seguridad/Perfil.js" type="text/javascript"></script>

</asp:Content>

