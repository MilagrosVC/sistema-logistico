﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="PlanDeCuenta.aspx.cs" Inherits="SEC_WEB.Contabilidad.Maestros.PlanDeCuenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


        <asp:HiddenField ID="hdUsuario" runat="server" />

        <div id="DVPrincipal" style="margin-left:25px">
        <h3> CONTABILIDAD - <strong>Plan de Cuenta</strong></h3>
            <%--<hr style="width:400px; margin-left: initial; box-shadow: 0 0 10px 1px black; border: 0; height: 0;" />--%>
             <%-- <hr style="width:400px; margin-left: initial; box-shadow: 0 0 10px 1px black; " />--%>
            <%--<hr style="width:400px; margin-left: initial;  box-shadow: inset 0 12px 12px -12px rgba(0, 0, 0, 0.5); height: 8px;
                border: 0;" />--%>
            <%--<hr style="width:400px; margin-left: initial; border: 0; height: 1px;
                background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));" />--%>
              <hr style="width:400px; margin-left: initial; border: 0; height: 1px; background: #333;
                 background-image: linear-gradient(to right, #ccc, #333, #ccc);" />
             

        <div id="DVFiltrosBusqueda">
            <table id="TbFiltroBusq">
                <tr>
                    <td> <label for="ddlRazSocialEmpresaID"> Razon Social:&nbsp;&nbsp;</label> </td> <%--IdEmpresa--%>
                    <td>   <select id="ddlRazSocialEmpresaID" name="ddlRazSocialEmpresaID" class="form-control" style="height:30px; width:150px;">
                           </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td> <label for="txtPla_Nombre_Tx"> Nombre del Plan:&nbsp;&nbsp;</label> </td>
                    <td> <input id="txtPla_Nombre_Tx" type="text" onkeypress="validarletras()" class="form-control" name="txtPla_Nombre_Tx" value="" /></td>
                    <%--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                    </tr>
                </table>
            <br/>
            <table>
                <tr>
                    <td>  <input type="button" id="btnBuscarPlanCuenta" class="boton" name="name" value="Buscar" /> 
                         <input type="button" id="btnLimpiarFiltrosBusq" class="boton" name="name" value="Limpiar" />
                    </td>
                </tr>
            </table>
            <br/>
           <%--        --%>

          <!------------------------ JqGrid ------------------------->
            <div id="PlanCuentaGrid">
            </div>
          <!--------------------------------------------------------->
        </div>

    </div>
    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
    <script type="text/javascript" src="../../Scripts/JS_SEC/Contabilidad/Maestros/PlanDeCuenta/SEC_PlanDeCuenta.js"></script>
</asp:Content>
