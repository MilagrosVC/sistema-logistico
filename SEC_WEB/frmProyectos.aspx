﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="frmProyectos.aspx.cs" Inherits="SEC_WEB.frmProyectos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
        <AlternatingRowStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:BoundField DataField="Empresa_Id" HeaderText="Codigo" />
            <asp:BoundField DataField="Emp_RazonSocial_Tx" HeaderText="Razon Social" />
            <asp:BoundField DataField="Emp_Personacontacto_Tx" HeaderText="Persona Contacto" />
            <asp:BoundField DataField="Emp_Nombre_Responsable_Tx" HeaderText="Persona Responsable" />
            <asp:BoundField DataField="Emp_DocIdent_Responsable_Nu" HeaderText="Nro. Doc" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>
</asp:Content>
