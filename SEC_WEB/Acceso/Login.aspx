﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SEC_WEB.Acceso.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Acceso SEC</title>
    <!--
      <link href="../Scripts/JQuery-ui-1.11.4/jquery-ui.css" rel="stylesheet" />
    <link href="../Scripts/JQuery-ui-1.11.4/font-awesome.min.css" rel="stylesheet" />
    <link href="../Scripts/Styles_SEC/Login.css" rel="stylesheet" />
    <link href="../Scripts/Styles_SEC/Login.css" rel="stylesheet" /> 
        -->

    <meta name="viewport" content="width=device-width, user-scalable=no">

    <link href="../Scripts/css/reset_styles.css" rel="stylesheet" />
    <link href="../Scripts/css/estilos.css" rel="stylesheet" />

     
</head>
<body>
    <form id="form1" runat="server">
    <div class="cabecera">
        <h1>SOLUCIONES DINETECH</h1>
        <p>Sistema Logístico de Construcción</p>
    </div>
    <section id="Login" class="login-cont">
        <article>
            <h2>Inicio de Sesión</h2>
        </article>
         <div>
       <label for="txtUsuario">Usuario :</label>
       <input type="text" name="txtUsuario" id="txtUsuario" />
        </div>
        <div>
                    <label for="txtPassword">Password :</label>
                    <input type="password" name="txtPassword" id="txtPassword" />
             </div>
            <div>
                    <input type="button" class="button" name="btnLogin" id="btnLogin" value="Entrar" />
          <input type="button" class="button" name="btnLimpiar" id="btnLimpiar" value="Limpiar" />
                 </div>

    </section>
    <div id="dialog-message" style="display: none">
        <br />
    <div id="divMensaje">
    </div>
    </div>
    <asp:HiddenField ID="hdf_Usuario" runat="server" />
    </form>
    <script type="text/javascript">
        function CloseWindow() {
            window.close();
        }
    </script>
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JQuery-1.11.3/jquery-1.11.3.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JQuery-ui-1.11.4/jquery-ui.js") %>'></script>
    <script type="text/javascript" src='<%= ResolveUrl("../Scripts/JS_SEC/SEC_Validaciones.js") %>'></script>    
    <script type="text/javascript" src='<%= ResolveUrl("~/Scripts/JS_SEC/Acceso/Login.js") %>'></script> 
    <footer class="pie_pagina" >
       Dinetech.pe © 2011 Todos los dererechos reservados Desarrollado por Milagros Beatriz Vallejos Chacón
    </footer>     
</body>
</html>
