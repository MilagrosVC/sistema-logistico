﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using SEC_BE;
using SEC_BL;
using Newtonsoft.Json;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace SEC_WEB.Seguridad
{
    public partial class Perfil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //PerfilBE objPerfilBE= new PerfilBE() ;
            //objPerfilBE.Per_Id = 1;
            //ListarPerfil(objPerfilBE);
        }

        [WebMethod]
        public static List<PerfilBE> ListarPerfil(PerfilBE objPerfilBE)
        {
            PerfilBL objPerfilBL = new PerfilBL();
            List<PerfilBE> lstPerfilBE = new List<PerfilBE>();
            lstPerfilBE = objPerfilBL.ListarPerfil(objPerfilBE);
            return lstPerfilBE;
        }

        [WebMethod]

        public static int CrearPerfil(PerfilBE objPerfilBE)
        {

            PerfilBL objPerfilBL = new PerfilBL();
            int id = 0;
            id = objPerfilBL.CrearPerfil(objPerfilBE);
            return id;

        }

        [WebMethod]
        public static void ActualizarPerfil(PerfilBE objPerfilBE)
        {

            PerfilBL objPerfilBL = new PerfilBL();
            objPerfilBL.ActualizarPerfil(objPerfilBE);

        }

        [WebMethod]
        public static void EliminarPerfil(PerfilBE objPerfilBE)
        {

            PerfilBL objPerfilBL = new PerfilBL();
            objPerfilBL.EliminarPerfil(objPerfilBE);

        }

    }
}