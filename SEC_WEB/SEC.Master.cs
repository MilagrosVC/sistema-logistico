﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB
{
    public partial class SEC : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.lblUsuario.Text = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            lblMenu.Text = HttpContext.Current.Session[Constantes.SessionVariables.Menu].ToString();
        }
    }
}