﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Almacen
{
    public partial class StockMinimo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            this.hdfProyecto.Value = HttpContext.Current.Session[Constantes.SessionVariables.Proyecto_Id].ToString();
        }

        [WebMethod]
        public static ProyectoBE CargarDatosProyecto(ProyectoBE ObjProyecto)
        {
            ProyectoBE objProyectoBE = new ProyectoBE();
            ProyectoBL objProyectoBL = new ProyectoBL();
            objProyectoBE = objProyectoBL.DatosProyecto(ObjProyecto);
            return objProyectoBE;
        }

        [WebMethod]
        public static List<FamiliaBE> ListarFamiliaCombo(FamiliaBE ObjFamilia)
        {
            FamiliaBL ObjFamiliaBL = new FamiliaBL();
            List<FamiliaBE> lstFamilia = new List<FamiliaBE>();
            lstFamilia = ObjFamiliaBL.ListarFamilia(ObjFamilia);
            return lstFamilia;
        }

        [WebMethod]
        public static List<MarcaBE> ListarMarcaCombo(MarcaBE ObjMarca)
        {
            MarcaBL ObjMarcaBL = new MarcaBL();
            List<MarcaBE> lstMarca = new List<MarcaBE>();
            lstMarca = ObjMarcaBL.ListarMarca(ObjMarca);
            return lstMarca;
        }

        [WebMethod]
        public static List<Item_MaBE> ListarItemsMa(Item_MaBE ObjItem_Ma)
        {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            List<Item_MaBE> lstItemMa = new List<Item_MaBE>();
            lstItemMa = ObjItemMaBL.ListarItem_Ma(ObjItem_Ma);
            return lstItemMa;
        }

        [WebMethod]
        public static List<Item_MaBE> ListarItemMaDisponible(ProyectoItemMaBE ProyectoItemMa)
        {
            ProyectoItemMaBL obj = new ProyectoItemMaBL();
            List<Item_MaBE> lstDisponibles = new List<Item_MaBE>();
            lstDisponibles = obj.ListarItemMaDisponible(ProyectoItemMa);
            return lstDisponibles;
        }

        [WebMethod]
        public static List<UndMedidaBE> ListarUnidadMedidaCombo(UndMedidaBE ObjUndMedida)
        {
            UndMedidaBL objUndMedidaBL = new UndMedidaBL();
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            lstUndMedida = objUndMedidaBL.ListarUnidadMedida(ObjUndMedida);
            return lstUndMedida;
        }

        //[WebMethod]
        //public static List<Item_MaBE> ListarItemMaAsignado(ProyectoItemMaBE ProyectoItemMa)
        //{
        //    ProyectoItemMaBL obj = new ProyectoItemMaBL();
        //    List<Item_MaBE> lstAsignados = new List<Item_MaBE>();
        //    lstAsignados = obj.ListarItemMaAsignado(ProyectoItemMa);
        //    return lstAsignados;
        //}
        [WebMethod]
        public static List<ProyectoItemMaBE> ListarItemMaAsignado(ProyectoItemMaBE ProyectoItemMa)
        {
            ProyectoItemMaBL obj = new ProyectoItemMaBL();
            List<ProyectoItemMaBE> lstAsignados = new List<ProyectoItemMaBE>();
            lstAsignados = obj.ListarItemMaAsignado(ProyectoItemMa);
            return lstAsignados;
        }
        [WebMethod]
        public static void AsignarDesignarItemMa(string lstItemMa, string Proyecto_Id, string usuario, string estado)
        {
            List<String> lstItemMaId = lstItemMa.Split(',').ToList();
            List<ProyectoItemMaBE> lstAsignaciones = new List<ProyectoItemMaBE>();
            ProyectoItemMaBL objPrBL = new ProyectoItemMaBL();
            foreach (string IMa_Id in lstItemMaId)
            {
                ProyectoItemMaBE objPrBE = new ProyectoItemMaBE();
                objPrBE.IMa_Id = Convert.ToInt32(IMa_Id);
                objPrBE.Proyecto_Id = Convert.ToInt32(Proyecto_Id);

                objPrBE.Aud_UsuarioActualizacion_Id = usuario;
                objPrBE.Aud_UsuarioCreacion_Id = usuario;
                objPrBE.Aud_Estado_Fl = Convert.ToBoolean(estado);
                lstAsignaciones.Add(objPrBE);
            }
            objPrBL.AsignarDesignarItemMa(lstAsignaciones);
        }

        [WebMethod]
        public static void AsignarDesignarItemMaTodos(string Proyecto_Id, string usuario, string estado)
        {
           // List<String> lstItemMaId = lstItemMa.Split(',').ToList();
            List<ProyectoItemMaBE> lstAsignaciones = new List<ProyectoItemMaBE>();
            ProyectoItemMaBL objPrBL = new ProyectoItemMaBL();
            //foreach (string IMa_Id in lstItemMaId)
            //{
            ProyectoItemMaBE objPrBE = new ProyectoItemMaBE();
            //objPrBE.IMa_Id = Convert.ToInt32(IMa_Id);
            objPrBE.Proyecto_Id = Convert.ToInt32(Proyecto_Id);
            objPrBE.Aud_UsuarioActualizacion_Id = usuario;
            objPrBE.Aud_UsuarioCreacion_Id = usuario;
            objPrBE.Aud_Estado_Fl = Convert.ToBoolean(estado);
            lstAsignaciones.Add(objPrBE);
            //}
            objPrBL.AsignarDesignarItemMaTodos(lstAsignaciones);
        }

        [WebMethod]
        public static void ModificarProyectoItem(ProyectoItemMaBE objProyectoItemMa) {

            ProyectoItemMaBL objProyectoItemMaBL = new ProyectoItemMaBL();
            objProyectoItemMaBL.ModificarProyectoItem(objProyectoItemMa);
         }
    }
}