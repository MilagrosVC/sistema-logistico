﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="StockMinimo.aspx.cs" Inherits="SEC_WEB.Logistica.Almacen.StockMinimo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style>
        body{
            /*background-color:#222;*/
            background-color:#fff;
        }
        #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;

            font-size:14px;
        }
        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
        #tbBusqueda td {
            padding:3px 5px;
        }
        #SEC_TotalesBandRQ td {
            padding:0 5px;
        }
        #SEC_TotalesBandRQ label {
            margin-top:5px;
        }
        .ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
            
        }
        
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Almacen/StockMinimo.js"></script>
     <asp:HiddenField ID="hdUsuario" runat="server" />
     <asp:HiddenField ID="hdfProyecto" runat="server" />
     <%--<h3>Configuracion de stock</h3>--%>
     <div id="DVPrincipal" style="margin-left: 25px; margin-right: 30px">
         <h4 style="text-align:center">
            <%--<label for="PlanCuentaNombre" id="PlanCuentaNombre"></label>--%>
            <strong> Configuracion de Stock Minimo</strong>
        </h4>



         <!--------------------------------------------------------->
        <div class="bs-example">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <%--  <h1 class="panel-title"><strong>Detalle de Plan de Cuenta</strong></h1>--%>
                    <%--  <hr style="width:30% ; align-content:"left"/>--%>
                   <%-- <br />--%>
                    <table id="tbDetallePR">
                        <tr>
                            <td style="height:6px"></td>
                        </tr>
                        <tr>
                            <td>
                                <strong>
                                    <label for="LBRUC">RUC :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBRUC" id="LBRUC" style="width:300px"></label>
                            </td>
                            <td style="width:250px"></td> 
                            <td>
                                <strong>
                                    <label for="LBEmpresa">Empresa:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBEmpresa" id="LBEmpresa"  style="width:300px"></label>
                            </td>
                            <td style="width:250px"></td> 
                            
                            <td>
                                <strong>
                                    <label for="LBProyecto">Proyecto:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                </strong>
                            </td>
                            <td>
                                <label for="LBProyecto" id="LBProyecto" style="width:300px"></label>
                            </td>
                         
                        </tr>
                    </table>



                </div>
                <div class="panel-body">
 
                    <table id="tbFiltrosBusqueda">
                        <tr>
                            <td>
                                <label for="ICodigo">Codigo:&nbsp;</label>
                            </td>
                            <td>
                                <input id="ICodigo" type="text" placeholder="Ingrese Codigo" class="inputs" name="ICodigo" value="" style="width: 110px;" />
                            </td>
                            <td style="width:300px"></td>
                            <td>
                                <label for="INombre">Nombre:&nbsp;</label>
                            </td>
               
                            <td colspan="6">
                                <input id="INombre" type="text"  class="inputs" placeholder="Ingrese Nombre de Item" name="INombre" value="" style="width: 350px;" />
                            </td>
                            <td style="width:200px"></td>
                            <td>
                                <label for="ddlFamiliaID">Familia:&nbsp;</label>
                            </td>
                            <td>
                                <select id="ddlFamiliaID" name="ddlFamiliaID" class="inputs" style="height: 30px; width: 230px;">
                                </select>
                            </td>

                            <td style="width:200px"></td>
                            <td>
                                <label for="ddlMarcaID">Marca:&nbsp;</label>
                            </td>
                            <td>
                                <select id="ddlMarcaID" name="ddlMarcaID" class="inputs" style="height: 30px; width: 230px;">
                                </select>
                            </td>
                           
                            </tr>
                             <tr>
                            <td colspan="12" style="height:15px">
                                </td>
                        </tr>
                             </table>
                              
                             <%--style="text-align:right"--%>
                                 <input type="button" style="text-align: center" id="btnBuscarItemMaAsignados" class="boton" name="name" value="Buscar" />
                                <input type="button" style="text-align: center" id="btnLimpiarBusqueda" class="boton" name="name" value="Limpiar" />

                       <input type="button" style="text-align: center; width: 150px" id="btnCargarItems" class="boton" name="name" value="Cargar Items" />
                
                    <input type="button" style="text-align: center; width: 200px" id="btnQuitarSeleccionados" class="boton" name="name" value="Quitar Items Seleccionados" /> 
                    <input type="button" style="text-align: center; width: 200px" id="btnQuitarTodos" class="boton" name="name" value="Quitar todos los Items" />
                </div>
                
            </div>
        </div>
        <!--------------------------------------------------------->

          <div id="divItemsMaAsignados" style="max-width:100%;">
        </div>





         <div id="dialog_ItemsMa" style="display:none"> <!-- style="display:none" ---->
        <table style="margin: 15px 0;">
            <tr>

                <td>
                                <label for="txtCodigo_Seleccion">Codigo:&nbsp;</label>
                            </td>
                            <td>
                                <input id="txtCodigo_Seleccion" type="text" placeholder="Ingrese Codigo" class="inputs" name="txtCodigo_Seleccion" value="" style="width: 110px;" />
                            </td>
                            <td style="width:25px"></td>
                            <td>
                                <label for="txtNombre_Seleccion">Nombre:&nbsp;</label>
                            </td>
               
                            <td>
                                <input id="txtNombre_Seleccion" type="text"  class="inputs" placeholder="Ingrese Nombre de Item" name="txtNombre_Seleccion" value="" style="width: 350px;" />
                            </td>
                            <td style="width:25px"></td>



                <td><label for="ddlFamilia_Seleccion" >Familia :&nbsp;</label></td>
                <td>
                    <select id="ddlFamilia_Seleccion" class="inputs" style="height:30px;width:200px;">                    
                    </select>
                </td>
                <td style="width:25px"></td>
                <td><label for="ddlMarca_Seleccion" style="width:60px">Marca :&nbsp;</label></td>
                <td>
                    <select id="ddlMarca_Seleccion" class="inputs" style="height:30px;width:200px;">
                    </select>
                </td>
                

                </tr> </table>
               <%-- <tr>
                    <td style="width:60px"></td>
                <td>--%>

             <%--   </td>
                <td>--%><input type="button" style="text-align: center; width: 200px" id="btnAgregarSeleccionados" class="boton" name="name" value="Agregar Items Seleccionados" />
             <input type="button" style="text-align: center; width: 200px" id="btnAgregarTodos" class="boton" name="name" value="Agregar todos los Items" />
                                 <input type="button" class="boton" id="btnBuscar_Disponibles" value="Buscar" />      
              
                    <input type="button" class="boton" id="btnLimpiar_Seleccion" value="Limpiar" />
                <%--</td>
                <td>--%>
                    <input type="button" class="boton" id="btnSalir_Seleccion" value="Salir" />   <br />
              <%--  </td>
            </tr>--%>
    
       <%-- <div id="divItemsMa" style="max-width:100%;">
        </div>--%> <br />
              <div id="divItemsMaDisponibles"> 
        </div>
    </div>






         <!--------------------------------------------------------->










         <div id="ItemMa-Reg" style='display: none;' >
        <table class="table" >
            <tr>
                <td>
                  <%--  <label for="ddlProyecto">Proyecto:  </label>--%>
                      <label for="txtCodigo">Codigo:  </label>
                </td>
                <td>
                    <%-- <select id="ddlProyecto" class="form-control"><option value="0"> SELECCIONE </option></select>--%>
                    <input id="txtCodigo" type="text" class="inputs" name="txtCodigo" value="" required="required" />
                </td>

                <td>
                    <label for="txtNombre">Nombre:  </label>
                </td>
                <td>
                    <input id="txtNombre" type="text" class="inputs" name="txtNombre" value="" required="required" />
                </td>
                </tr>
            
                <tr>

                
                <td>
                    <label for="txtDescripcion">Descripcion:</label>
                </td>
           
                    <td colspan="3">
                        <input id="txtDescripcion" type="text" class="inputsColspan"  name="txtDescripcion" value="" required="required" />
                    </td>
            </tr>

            <tr>
                <td>
                    <label for="txtMetrado">Metrado:</label>
                </td>
                <td>
                    <input id="txtMetrado" type="text" class="inputs" name="txtMetrado" onkeypress="return ValidarMonto(event, this);" value="" required="required" />
                </td>

                <td>
                    <label for="ddlUnidadMedida_Detalle">U. Medida:  </label>
                </td>
                <td>
                    <select id="ddlUnidadMedida_Detalle" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>
                </tr>
            <tr>
                <td>
                    <label for="ddlFamilia_Detalle">Familia:  </label>
                </td>
                <td>
                    <select id="ddlFamilia_Detalle" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>

                <td>
                    <label for="ddlMarca_Detalle">Marca:  </label>
                </td>
                <td>
                    <select id="ddlMarca_Detalle" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>
            </tr>
            
            <tr id="" style="margin-top: 30px;">
                <td colspan="4" style="text-align: right" <%--align="right" class="btn btn-default btn-sm"--%>>
                 
                    <input type="button" style="width: 195px" id="btnSalirDetalle" class="boton" name="name" value="Salir de Detalle" />
                </td>
            </tr>
        </table>
    </div>







     </div>
        <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
