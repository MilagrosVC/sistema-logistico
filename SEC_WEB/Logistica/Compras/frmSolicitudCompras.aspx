﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="frmSolicitudCompras.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.frmSolicitudCompras" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td><b>Proyecto :</b></td>
            <td><asp:TextBox ID="txtProyecto" runat="server" /></td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="gvProyectos" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" DataKeyNames="Proyecto_Id" OnRowCommand="gvProyectos_RowCommand">
        <Columns>
            <asp:BoundField DataField="Proyecto_Id" HeaderText="Codigo" InsertVisible="False" ReadOnly="True" SortExpression="Proyecto_Id"  />
            <asp:BoundField DataField="Empresa_Id" HeaderText="Empresa_Id" SortExpression="Empresa_Id" Visible="False" />
            <asp:BoundField DataField="Emp_RazonSocial_Tx" HeaderText="Razon Social" SortExpression="Emp_RazonSocial_Tx" />
            <asp:BoundField DataField="Pry_Nombre_Tx" HeaderText="Proyecto" SortExpression="Pry_Nombre_Tx" />
            <asp:BoundField DataField="Pry_Descripcion_Tx" HeaderText="Descripcion" SortExpression="Pry_Descripcion_Tx" />
            <asp:BoundField DataField="Pry_Desde_Fe" HeaderText="Desde" SortExpression="Pry_Desde_Fe" />
            <asp:BoundField DataField="Pry_Hasta_Fe" HeaderText="Hasta" SortExpression="Pry_Hasta_Fe" />
            <asp:BoundField DataField="Moneda_Id" HeaderText="Moneda_Id" SortExpression="Moneda_Id" Visible="False" />
            <asp:BoundField DataField="Pry_Monto_Nu" HeaderText="Monto" SortExpression="Pry_Monto_Nu" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:ImageButton ID="btnProyecto" runat="server" CommandName="Seleccionar" AlternateText="Seleccionar"/>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SEC_DB %>" SelectCommand="SEC_PRC_S_PROYECTO" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
</asp:Content>
