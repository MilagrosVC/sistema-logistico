﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Compras
{
    public partial class RequerimientoUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Proyecto_Id"] = 1;
            //Session["Usuario_Id"] = "MPonceG";
            //this.hdf_Proyecto_Id.Value = Session["Proyecto_Id"].ToString();
            //this.hdf_Usuario.Value = Session["Usuario_Id"].ToString();
            this.hdf_Usuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }


        [WebMethod]
        public static List<RequerimientoCaBE> ListarRequerimiento(RequerimientoCaBE ObjRequerimientoCa)
        {
            RequerimientoCaBL ObjRequerimientoCaBL = new RequerimientoCaBL();
            List<RequerimientoCaBE> lstRequerimientoCa = new List<RequerimientoCaBE>();
            lstRequerimientoCa = ObjRequerimientoCaBL.ListarRequerimientoCa(ObjRequerimientoCa);
            return lstRequerimientoCa;
        }


        [WebMethod]
        public static RequerimientoCaBE GenerarVistaRequerimiento(RequerimientoCaBE objRequerimientoCa)
        {
            RequerimientoCaBL objRQCaBL = new RequerimientoCaBL();
            return objRQCaBL.GenerarVistaRequerimiento(objRequerimientoCa);
        }

        /*protected void btnExportar_Click(object sender, EventArgs e)
        {
            Response.AppendHeader("content-disposition", "attachment; filename = ExportedHtml.xls");
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "application / vnd.ms-excel";
            this.EnableViewState = false;
            Response.Write(divExcel.InnerText.ToString());
            Response.End();
        }*/

        [WebMethod]
        public static List<EstadoDeBE> ListarComboEstado(EstadoDeBE ObjEstadoDe)
        {
            EstadoDeBL objEstadoDeBL = new EstadoDeBL();
            List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();
            lstEstadoDe = objEstadoDeBL.ListarEstados(ObjEstadoDe);
            return lstEstadoDe;
        }

    }
}