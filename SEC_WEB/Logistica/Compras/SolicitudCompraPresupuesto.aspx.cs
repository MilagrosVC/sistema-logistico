﻿using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SEC_WEB.Logistica.Compras
{
    public partial class SolicitudCompraPresupuesto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                CargarDatos();
            }
            
        }

        private void CargarDatos()
        {
            //Convert.ToInt32(this.hdf_Sub_Motivo_Id.Value = Request["Sub_Motivo_Id"]);
            this.hdf_Proyecto_Id.Value = Request["Proyecto_Id"];
            //Sub_Motivo_Id = Convert.ToInt32(this.hdf_Sub_Motivo_Id.Value);
        }

        [WebMethod]
        public static string GetJewellerAssets(int jewellerId)
        {
            return null;
        }


       /* [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string ListarPresupuesto2(string customer)
        {
            PresupuestoBL objPreBL = new PresupuestoBL();
            PresupuestoBE objPresupuesto = new PresupuestoBE()
            {
                Proyecto_Id = Convert.ToInt32(customer)
            };

            var orders = objPreBL.ListarPresupuesto(objPresupuesto);

            var grid = new
            {
                page = 1,
                records = orders.Count(),
                total = orders.Count(),

                rows = from item in orders
                       //let orderdate = item.OrderDate.HasValue ? item.OrderDate.Value.ToShortDateString() : ""
                       //let requireddate = item.RequiredDate.HasValue ? item.RequiredDate.Value.ToShortDateString() : ""

                       select new
                       {
                           id = item.OrderID,
                           cell = new string[]{
                       orderdate,
                       requireddate,
                       item.ShipAddress,
                       item.ShipCity,
                       item.ShipCountry,
                       item.Customers.CompanyName
 
                   }
                       }

            };

            return JsonConvert.SerializeObject(grid);
        }

        */

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string ListarPresupuestos(string IdProyecto)
        {
            PresupuestoBL objPreBL = new PresupuestoBL();
            
            DataTable dtPresupuesto = new DataTable();
            PresupuestoBE objPresupuesto = new PresupuestoBE()
            {
                Proyecto_Id = Convert.ToInt32(IdProyecto)
            };

            dtPresupuesto = objPreBL.ListarPresupuesto(objPresupuesto);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows1 = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtPresupuesto.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtPresupuesto.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows1.Add(row);
            }

            var grid = new
            {
                page = 1,
                records = rows1.Count(),
                total = rows1.Count(),

                rows = rows1
            };

            return JsonConvert.SerializeObject(grid);
        }

        /*public void ProcessRequest(HttpContext context)
        {
            PresupuestoBL objPreBL = new PresupuestoBL();

            DataTable dtPresupuesto = new DataTable();
            PresupuestoBE objPresupuesto = new PresupuestoBE()
            {
                Proyecto_Id = 1
            };

            var coleccion = objPreBL.ListarPresupuesto(objPresupuesto);

            var jsonSerializer = new JavaScriptSerializer();
            context.Response.Write(jsonSerializer.Serialize(coleccion.AsQueryable<PresupuestoBE>().))

        }*/
        

    }
}