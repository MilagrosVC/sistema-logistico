﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="BandejaFactura.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.BandejaFactura" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
    #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;

            font-size:14px;
        }
    #tbBusqueda td {
        padding:3px 5px;
    }
    .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
    .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
    
#divImprimirCom { 
    margin: 30px auto; 
    padding: 20px 60px;
    border: 1px solid #D2D2D2;
    font-family: Arial; 
    font-size: 11px; 
    line-height: 1.6; 
    color: #444;
            
    width:800px;
    min-height:750px;  
}
.td-lbl{
    width:523px;
}
.td-lbl1{
    width:270px;
}
.td-lbl label{
    line-height: 0px;
}
#tbVerComprobante td{
    padding:10px 10px 0 0;
}
#tbVerComprobante #tdDetalle{
    padding:0px 0px 0 0;
}
#tbCom-Detalle th {
    text-align:center;
}
#tbCom-Detalle td,#tbCom-Detalle th {
    border: 1px solid black;padding:0px;
}
/*
#tbExcelOC td{
    padding:10px 10px 0 0;
}
#tbExcelOC #tdExcelDetalle{
    padding:0px 0px 0 0;
}
#tbExcelOC-Detalle th {
    text-align:center;
}
#tbExcelOC-Detalle td,#tbExcelOC-Detalle th {
    border: 1px solid black;padding:0px;
    padding:0px 0px 0 0;
}
*/


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h3><b>Bandeja de Facturas</b></h3>
    <div style="border:solid 2px;">
        <div style="background-color:#6E6E6E;border-bottom:solid 2px;padding-left:5px;">
            <b style="color:#fff;font-size:12px;">Filtros de Busqueda</b>
        </div>
            <div style="margin:10px 0;">
                <table id="tbBusqueda">
                    <tr>
                        <td><label>Factura N°:</label></td>
                        <td><input type="text" id="txtCodFactura" name="txtCodFactura" maxlength="12" placeholder="Ingrese N°" class="inputs"/></td>
                        <td style="padding-left:20px;"><label>Desde :</label></td>
                        <td><input type="text" id="txtDesde" name="txtDesde" readonly="readonly" placeholder="dd/mm/aaaa" style="width:110px;" class="inputs"/></td>
                        <td><label>Hasta :</label></td>
                        <td><input type="text" id="txtHasta" name="txtHasta" placeholder="dd/mm/aaaa" class="inputs" style="width:110px;"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="button" id="btnBuscar" name="btnBuscar" value="Buscar" class="boton" />
                            <input type="button" id="btnLimpiar" name="btnLimpiar" value="Limpiar" class="boton"/>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    <br />
    <div id="divFacturas" style="width:100%;"></div>
    <br />
    <input type="button" id="btnCrear" name="btnCrear" value="Crear Factura" class="boton" />

    <div id="dvVerComprobante" style="display:none;">
        <div id="divImprimirCom">
        <h4 style="text-align:center;"><label id="lblfa-Titulo">FACTURA</label></h4>
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>
        <label id="lblCom-CodComprobante" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerComprobante" style="font-size:10px;">
            <tr>
                <td class="td-lbl1">SEÑOR(ES):</td><td class="td-lbl"><label id="lblCom-RazonSocial"></label></td>
                <td>FECHA:</td><td class="td-lbl"><label id="lblCom-Fecha"></label></td>
            </tr>
            <tr>
                <td>DIRECCIÓN:</td><td colspan="3" class="td-lbl"><label id="lblCom-Direccion"></label></td>
            </tr>  
            <tr>
                <td>RUC :</td><td class="td-lbl"><label id="lblCom-RUC"></label></td>
                <td class="td-lbl1">GUíA DE REMISIÓN N°:</td>
                <td class="td-lbl"><label id="lblCom-Guia"></label></td>
            </tr>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="4">
                    <table id="tbCom-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:300px">ARTICULO</th>
                            <th style="width:60px">PRECIO UND</th>
                            <th style="width:100px">TOTAL</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnImprimirCom" name="name" value="Imprimir" class="boton" />
            <input type="button" id="btnCerrarVerCom" name="name" value="Cerrar" class="boton" />   
            <input type="button" id="btnExport" value="Generar Excel" class="boton" />
        </div>
    </div>

    <script src="../../Scripts/JS_SEC/Logistica/Compras/BandejaFactura.js"></script>
</asp:Content>
