﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="BandejaRequerimiento.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.BandejaRequerimiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body{
            /*background-color:#222;*/
            background-color:#fff;
        }
        #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;

            font-size:14px;
        }
        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
        #tbBusqueda td {
            padding:3px 5px;
        }
        #SEC_TotalesBandRQ td {
            padding:0 5px;
        }
        #SEC_TotalesBandRQ label {
            margin-top:5px;
        }
        .ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
            
        }
        /*Estilo para la grilla
        .ui-jqgrid .ui-jqgrid-titlebar {
            height: 26px;
            line-height:15px;
        }
        .ui-widget-header {
            border: 1px solid #9d9d9d;
            background:-webkit-gradient(linear, left top, left bottom, from(#9d9d9d), to(#222));
            line-height:10px;
        }*/

        /*Estilos para el DatePicker
        .ui-datepicker-title,.ui-datepicker table {
            font-size:.7em;
        }
        .ui-datepicker .ui-datepicker-prev{
            left: 2px;
            width: 16px;
            height: 20px;
        }
        .ui-datepicker .ui-datepicker-next{
            right: 2px;
            width: 16px;
            height: 20px;
        }
        img.ui-datepicker-trigger {
            position: relative;
            left: -18px;
            top: -1px;
        }*/
        
        /*Estilos para el input
        input {
            color:#000000;
            border-radius:3px;
            padding:0 5px;
        }
        input.placeholder {
            text-align: center;
        }*/
        /* Visualizacion de Requerimiento*/
        /*#dvVerRequerimiento {
            background-color:#222;
            width:
        }*/
        /*#divImprimirRQ { 
            margin: 30px auto; 
            padding: 20px 60px;
            border: 1px solid #D2D2D2;
            font-family: Arial; 
            font-size: 11px; 
            line-height: 1.6; 
            color: #444;
            
            width:800px;
            min-height:750px;  
        }
        .td-lbl{
            width:523px;
        }
        .td-lbl label{
            line-height: 0px;
        }
        #tbVerRq td{
            padding:10px 10px 0 0;
        }
        #tbVerRq #tdDetalle{
            padding:0px 0px 0 0;
        }
        #tbRq-Detalle th {
            text-align:center;
        }
        #tbRq-Detalle td,#tbRq-Detalle th {
            border: 1px solid black;padding:0px;
        }*/
    </style>
    <link href="../../Scripts/Styles_SEC/SEC_BandRQ_ImprRQ.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!------ Hiddens ------>
    <asp:HiddenField ID="hdUsuario" runat="server" />
    <asp:HiddenField ID="hdProyecto" runat="server" />
    <!--------------------->
    <h3><b>Gestión de Requerimientos</b></h3>
    <div id="dvFiltros"style="border:solid 2px;">
        <div style="background-color:#333;border-bottom:solid 2px;">
            <b style="color:#fff;font-size:12px; padding-left:5px">Filtros de Busqueda</b>
        </div>
        <div style="margin:10px 0;">
            <table id="tbBusqueda" style="margin-bottom:8px;">
                <tr>
                    <td>Cod. Requerimiento:</td>
                    <td><input type="text" id="txtCodRequerimiento" class="inputs" name="" value="" placeholder="Ingrese el codigo"/></td>
                    <td>Solicitante:</td>
                    <td><input type="text" id="txtSolicitante" class="inputs" name="" value="" placeholder="Ingrese Nombre"/></td>
                    <td>Desde:</td>
                    <td><input type="text" id="txtFiltroDesde" class="inputs" name="" value="" placeholder="dd/mm/aaaa"/></td>
                    <td>Hasta:</td>
                    <td><input type="text" id="txtFiltroHasta" class="inputs" name="" value="" placeholder="dd/mm/aaaa"/></td>
                </tr>
            </table>
            <input type="button" id="btnBuscar" value="Buscar" class="boton" style="margin-left:5px;"/>
            <input type="button" id="btnLimpiar" value="Limpiar" class="boton"/> 
        </div>
    </div>
    
    <br />

    <div id="SEC_GrdBandRQ" style="width:100%;"></div>
    <br />
    <div id="SEC_TotalesBandRQ">
        <table>
            <tr>
                <td><b>TOTAL:</b></td>
                <td style="border-right:2px solid"><label id="RQsTotal"></label></td>
                <td>Nuevos:</td>
                <td style="border-right:2px solid"><label id="RQsNuevos"></label><span style="margin:0 5px;padding-left:18px;background-color:#C6F296;border:1px solid;border-color:#222;"></span></td>
                <td>Aprobados:</td>
                <td style="border-right:2px solid"><label id="RQsAprobados"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
                <td>Desaprobados:</td>
                <td><label id="RQsDesaprobados"></label><span style="margin-left:5px;padding-left:18px;background-color:#FDB5B5;border:1px solid;border-color:#222;"></span></td>
            </tr>
        </table>
    </div>
    
    <!-- Visualizacion de Requerimiento -->

    <div id="dvVerRequerimiento" style="display:none">
        <div id="divImprimirRQ">
        <h4 style="text-align:center;"><label id="lblrq-Titulo">REQUERIMIENTO DE MATERIALES</label></h4>
        <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>
        <label id="lblRq-CodRequerimiento" style="float:right;"></label>
        <br style="clear:both;"/>
        <table id="tbVerRq" style="font-size:10px;">
            <tr>
                <td>REQUERIDO POR :</td><td class="td-lbl"><label id="lblRq-Solicitante"></label></td>
            </tr>
            <tr>
                <td>FECHA DE REQUERIMIENTO :</td><td class="td-lbl"><label id="lblRq-FechaCreacion"></label></td>
            </tr>
            <tr>
                <td>FECHA DE ENTREGA :</td><td class="td-lbl"><label id="lblRq-FechaEntrega"></label></td>
            </tr>  
            <tr>
                <td>PROYECTO :</td><td class="td-lbl"><label id="lblRq-NomProyecto"></label></td>
            </tr> 
            <tr>
                <td>DIRECCION :</td><td class="td-lbl"><label id="lblRq-Direccion"></label></td>
            </tr>
            <tr>
                <td>ESPECIALIDAD :</td><td class="td-lbl"><label id="lblRq-Partida"></label></td>
            </tr>
            <tr>
                <td>PARTIDA :</td><td class="td-lbl" style="text-align:right;">TURNO :<label id="lblRq-Turno" style="margin:0 -10px 0 10px"></label></td>   
            </tr>
            <tr>
                <td colspan="2">
                    <ul id="lstRq-Subpartidas">
                        
                    </ul>
                </td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="2">
                    <table id="tbRq-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">ITEM</th>
                            <th style="width:300px">DESCRIPCION</th>
                            <th style="width:70px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:196px">OBSERVACION</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        </div>
        <div style="text-align:center;">
            <input type="button" id="btnImprimirRQ" name="name" value="Imprimir" class="boton" style="background-color:#eee"/>
            <input type="button" id="btnCerrarVerRQ" name="name" value="Cerrar" class="boton" style="background-color:#eee"/>
        </div>
    </div>

    <script src="../../Scripts/JS_SEC/Logistica/Compras/BandejaRequerimiento.js"></script>
</asp:Content>
