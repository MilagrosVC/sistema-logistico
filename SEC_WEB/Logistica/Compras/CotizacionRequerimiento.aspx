﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="CotizacionRequerimiento.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.CotizacionRequerimiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body{
            /*background-color:#222;*/
            background-color:#fff;
        }
        #MainSEC {
            padding: 0 10px 0 10px;

            color: #444;/*#9d9d9d;*/
            font-size:14px;
        }
        .ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
        }
        .boton {
            border:1px solid;
            border-radius:0px;
            border-color:#9d9d9d;
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;/*#9d9d9d;*/
            font-weight:bold;
        }
        .boton:hover {
            border:1px solid;
            border-color:#444;
            border-radius:0px;
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#9d9d9d;
            color: #fff;/*#9d9d9d;*/
            font-weight:bold;
        }
        #frmEnvioMail label{
            line-height: 35px;
        }
        .dvDatosRQ-td {
            padding: 5px 10px 5px 0;
        }
        .inputs {
            border: 1px solid;
            border-radius:4px;
            border-color: #9d9d9d;
            height:28px;
            padding:3px;
        }
        .ui-state-disabled, .ui-widget-content .ui-state-disabled, .ui-widget-header .ui-state-disabled {
            opacity: .8;
        }
        
        /*.ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
            
        }*/
        /*img.ui-datepicker-trigger {
            position: relative;
            left: 152px;
            top: -27px;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hdUsuario" runat="server" />
    <asp:HiddenField ID="hdProyecto" runat="server"/>
    <asp:HiddenField ID="hdRequerimiento" runat="server"/>
    <asp:HiddenField ID="hdSolicitud" runat="server"/>

    <asp:HiddenField ID="hdf_Cotizacion_Id" runat="server" />
      <asp:HiddenField ID="hdf_EstCotGanadora" runat="server" />
     <asp:HiddenField ID="hdf_EstCotCreada" runat="server" />
        <asp:HiddenField ID="hdf_EstCotOCGenerada" runat="server" />
    <asp:HiddenField ID="hdf_EstCotEnviada_OC" runat="server" />
    <asp:HiddenField ID="hdf_EstCotEnviada_SA" runat="server" />


<%--     <asp:HiddenField ID="hdf_e_EstCotDet" runat="server" />--%>
    <asp:HiddenField ID="hdf_EstCotDetCreadaId" runat="server" />
    <asp:HiddenField ID="hdf_EstCotDetIngresadaId" runat="server" />
    <input type="hidden" id="hdCotizacionId" />
     <input type="hidden" id="hdRAP" />
   
    <!---->
    <h3>Cotizaciones para la solicitud N° <span id="NumSol"></span></h3>
    <div id="dvDatosSol" style="border:solid 2px;">
        <div style="background-color:#333;border-bottom:solid 2px;">
            <b style="color:#fff;font-size:12px; padding-left:5px;">Filtro de búsqueda</b>
        </div>
        <div style="margin: 10px 0px">
        <table style="margin:5px">
            <tr>
                <td class="dvDatosRQ-td">Estado:</td><td class="dvDatosRQ-td"><select id="ddlEstadoCot" class="inputs" style="margin-bottom:0px;"><option value="0">SELECCIONE</option></select></td>

                <!--<td class="dvDatosRQ-td">Requerimiento:</td><td class="dvDatosRQ-td"><select id="ddlRequerimiento" class="inputs" style="margin-bottom:0px;"><option value="0">SELECCIONE</option></select></td>
                
                <td class="dvDatosRQ-td">Solicitud:</td><td class="dvDatosRQ-td"><select id="ddlSolicitud" class="inputs" style="margin-bottom:0px;"><option value="0">SELECCIONE</option></select></td>-->
            </tr>
            <!--<tr>
                <td><span style="padding:40px;"></span></td>
                <td>Cantidad de Items:</td><td class="dvDatosRQ-td"><label id="lblNumItems" style="margin-bottom:0px;"></label></td>
            </tr>
            <tr>
                <td>Solicitante:</td><td class="dvDatosRQ-td"><label id="lblSolicitante" style="margin-bottom:0px;"></label></td>
            </tr>
            <tr>
                <td>Fecha Requerimiento:</td><td class="dvDatosRQ-td"><label id="lblFRequerimiento" style="margin-bottom:0px;"></label></td>
            </tr>-->
            <!--<tr>
                <td style="padding:10px 0">
                    <input type="button" id="btnNuevo" name="btnNuevo" value="Nueva Solicitud" class="boton" />
                </td>
                <td>
                    <input type="button" class="boton" id="btnRegresar" name="name" value="Regresar" style="/*margin: 0 0 10px 0;float:right*/" />
                </td>  
            </tr>-->
        </table>
        </div>
    </div>
    <br/>
    <input type="button" id="btnIngCotizacion" name="IngCotizacion" value="Nueva Cotizacion" class="boton" style="width:200px" />
    <input type="button" class="boton" id="btnListarCot" name="ListarCot" value="Listar Todas las Cotizaciones" style="width:250px" />
    <input type="button" class="boton" id="btnRegresar" name="Regresar" value="Ir a Solicitud de Requerimiento" style="width:250px" />
    <input type="button" class="boton" id="btnBandejaCotizaciones" name="BandejaCotizaciones" value="Ir a Bandeja de Cotizaciones" style="width:250px" />
    <br />
    <br />
    <div id="SEC_Cotizaciones">
    </div>

    <!-- Dialog para el registro de cotizacion -->
    <div id="dlg-NuevaCot" style="display:none;">
        <h4 style="display:inline-block">Informacion de Proveedor</h4>
        <input type="button" id="btnEditarCotCa" name="EditarCotCa" value="Editar Cotizacion" class="boton" style="display:inline-block;float:right;display:none;" />
        <div style="clear:both;"></div>
        <table id="tbCot-InfProv" style="font-size:11px">
            <tr>
                <td class="dvDatosRQ-td">Proveedor:</td>
                <td class="dvDatosRQ-td"><select id="ddlProveedor" class="inputs" style="width:300px"><option value='0'>SELECCIONE</option></select></td>
                <td class="dvDatosRQ-td">N° Proforma:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtProforma" onkeypress="validarnumeros()" class="inputs"  name="" value="" /></td>
                <!--<td class="dvDatosRQ-td"><span style="padding-right:8px;"></span></td>-->
                <td class="dvDatosRQ-td">Fecha:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtFechaProf" class="inputs" name="" value="" placeholder="dd/mm/aaaa"/></td>
            </tr>
            <tr>
                <td class="dvDatosRQ-td">Vendedor:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtVendedor" class="inputs" style="width:300px" name="Vendedor" value="" /></td>
                <!--<td class="dvDatosRQ-td"><span style="padding-right:8px;"></span></td>-->
                <td class="dvDatosRQ-td">Telefono:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtTelefonoVend" onkeypress="validartelefono()" class="inputs" style="width:100px" name="TelefonoVend" value="" /></td>
                <!--<td class="dvDatosRQ-td"><span style="padding-right:8px;"></span></td>-->
                <td class="dvDatosRQ-td">Mail:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtMailVend" class="inputs" style="width:180px" name="MailVend" value="" /></td>
            </tr>
        </table>
        <h4>Informacion de Pago</h4>
        <table id="tbCot-InfCost" style="font-size:11px">
            <tr>
                <td class="dvDatosRQ-td">Tipo de Pago:</td>
                <td class="dvDatosRQ-td" colspan="5"><select id="ddlTipoPago" class="inputs" style="width:350px"><option value='0'>SELECCIONE</option></select></td>
                <td class="dvDatosRQ-td">Moneda:</td>
                <td class="dvDatosRQ-td"><select id="ddlMoneda" class="inputs" style="width:140px"><option value='0'>SELECCIONE</option></select></td>
            </tr>
            <tr id="TRCotMontos">
                <td class="dvDatosRQ-td" style="text-align:right">Monto:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtMonto" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="SubTotal" value="" /></td>
                <td class="dvDatosRQ-td">IGV 18%:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtIGV" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="IGV" value="" /></td>
                <td class="dvDatosRQ-td">Total:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtTotal" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="Total" value="" /></td>
                <td class="dvDatosRQ-td">Detraccion:</td>
                <td class="dvDatosRQ-td"><input type="text" id="txtDetraccion" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="Detraccion" value="" /></td>
           
           
                <td class="dvDatosRQ-td"><label>Requiere Solicitud Anticipo</label></td>
                <td class="dvDatosRQ-td"><input type="checkbox" id="chkbSolAnticipo" name="name" value="" disabled="disabled"  /></td>
        <td class="dvDatosRQ-td" ><i id="iVisualizarRAP"    title="Click para visualizar el regitro" onclick="visualizarRAP()" style="cursor:pointer;display:none" class="fa fa-eye"></i></td>
                 </tr>
            <tr>
                <td class="dvDatosRQ-td" style="text-align:right;vertical-align:top;">Observacion:</td>   
                <td class="dvDatosRQ-td"  colspan="7">
                    <textarea id="txtarObs" style="border: 1px solid;border-radius:4px;border-color: #9d9d9d;width:680px;height: 80px;padding:5px"></textarea>
                </td>
            </tr>
        </table>
        <input type="button" id="btnGuardarEdicion" name="GuardarEdit" value="Guardar Cambios" class="boton" style="float:right;display:none;" />
        <input type="button" id="btnCancelarEdicion" name="CancelarEdit" value="Cancelar" class="boton" style="float:right;display:none;" />
        <div style="clear:both;"></div>
        <div id="dvDetalleCot" style="display:none">
            <h4>Detalle de la Cotizacion</h4>
            <div id="dvDetNuevaCotizacion">
            </div>
        </div>
    </div>
    <!------------------------------------------------------------------------------------------------------------------------->
    <div id="dlg-DetalleNuevaCot" style="display:none;">
        <br />
        <table id="tbCotDetalle">
            <tr>
                <td class="dvDatosRQ-td">Descripcion: </td>
                <td class="dvDatosRQ-td" colspan="3"><input type="text" id="txtDetCot-Desc" class="inputs" style="width:400px" name="Descripcion" value="" /></td>
            </tr>
            <tr id="trMontos">
                <td class="dvDatosRQ-td">Cantidad: </td>
                <td class="dvDatosRQ-td"><input type="text" id="txtDetCot-Cant" class="inputs" onkeypress="validarnumeros()" style="width:120px" name="Cantidad" value="" /></td>
                <td class="dvDatosRQ-td">Unidad Medida: </td>
                <td class="dvDatosRQ-td"><select id="ddlDetCot-Und" class="inputs" style="width:100px"><option value="0">SELECCIONE</option></select></td>
            </tr>
            <tr id="trMontos2">
                <td class="dvDatosRQ-td">P. Unitario: </td>
                <td class="dvDatosRQ-td"><input type="text" id="txtDetCot-PUnit" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="PUnitario" value="" /></td>
                <td class="dvDatosRQ-td">P. Total Cotizado: </td>
                <td class="dvDatosRQ-td"><input type="text" id="txtDetCot-PTot" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="PTotal" value="" /></td>
            </tr>
        <%--    <tr>
                <td class="dvDatosRQ-td"></td>
                <td class="dvDatosRQ-td"></td>
                <td class="dvDatosRQ-td">P. Total Calculado: </td>
                <td class="dvDatosRQ-td"><input type="text" id="txtDetCot-PTotCal" class="inputs" onkeypress="return ValidarMonto(event, this);" style="width:80px" name="PTotal" value="" /></td>
            </tr>--%>
        </table>
        <br />
    </div>
    <!------------------------------------------------------------------------------------------------------------------------->
    <div id="dvRAP" style="display:none">
           <table style="margin:5px">

            <%--<tr class="trSolAnticipo" style="display:none;">--%>
               <tr class="trSolAnticipo" >
                <td class="dvDatosRQ-td"><label>Tipo Solicitud:</label></td>
                <td class="dvDatosRQ-td"><select id="ddlTipoSolicitud" class="inputs" style="width:157px"><option value="0">SELECCIONE</option></select></td>
            </tr>
            <tr class="trSolAnticipo" >
                <td class="dvDatosRQ-td"><label>Monto Total Cotizado:</label></td>
                <td class="dvDatosRQ-td"><input type="text" id="txtMontoTotal" name="name" value="" class="inputs"/></td>
            </tr>
            <tr class="trSolAnticipo" >
                <td class="dvDatosRQ-td"><label>Monto a Solicitar:</label></td>
                <td class="dvDatosRQ-td"><input type="text" id="txtMontoSolAnt" name="name" value="" class="inputs"/></td>
            </tr>
        </table>
    </div>
    <!------------------------------------------------------------------------------------------------------------------------->

    <!------------------------------------------------------------------------------------------------------------------------->
    <div id="dvEnviarCotizacion" style="display:none">
        <table style="margin:5px">

        </table>
    </div>
    <!------------------------------------------------------------------------------------------------------------------------->
    <script src="../../Scripts/JS_SEC/Logistica/Compras/CotizacionRequerimiento.js"></script>
    <script src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
