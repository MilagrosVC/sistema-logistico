﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_Utilitarios;
using SEC_BE;
using SEC_BL;

namespace SEC_WEB.Logistica.Compras
{
    public partial class BandejaRequerimiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString(); 
            this.hdProyecto.Value = HttpContext.Current.Session[Constantes.SessionVariables.Proyecto_Id].ToString(); ;
        }

        [WebMethod]
        //public static List<RequerimientoCaBE> CargarBandejaRQ(string objRequerimientoCa)
        public static List<RequerimientoCaBE> CargarBandejaRQ(RequerimientoCaBE objRequerimientoCa)
        {
            RequerimientoCaBL objRQCaBL = new RequerimientoCaBL();
            List<RequerimientoCaBE> lstRQCaBE = new List<RequerimientoCaBE>();

            //lstRQCaBE = objRQCaBL.CargarBandejaRQ(JsonConvert.DeserializeObject<RequerimientoCaBE>(objRequerimientoCa));
            lstRQCaBE = objRQCaBL.CargarBandejaRQ(objRequerimientoCa);
            return lstRQCaBE;

        }
        
        [WebMethod]
        public static int CambiarEstadoRequerimiento(RequerimientoCaBE objRequerimientoCa)
        {
            RequerimientoCaBL objRQCaBL = new RequerimientoCaBL();
            int id = 0;
            id = objRQCaBL.CambiarEstadoRequerimiento(objRequerimientoCa);
            return id;
        }

        [WebMethod]
        public static RequerimientoCaBE GenerarVistaRequerimiento(RequerimientoCaBE objRequerimientoCa)
        {
            RequerimientoCaBL objRQCaBL = new RequerimientoCaBL();
            return objRQCaBL.GenerarVistaRequerimiento(objRequerimientoCa);
        }

        [WebMethod]
        public static List<RequerimientoDeBE> ListarItemsxRequerimiento(RequerimientoDeBE objRequerimientoDe)
        {
            RequerimientoDeBL objRQDeBL = new RequerimientoDeBL();
            List<RequerimientoDeBE> lstRQDeBE = new List<RequerimientoDeBE>();

            //lstRQCaBE = objRQCaBL.CargarBandejaRQ(JsonConvert.DeserializeObject<RequerimientoCaBE>(objRequerimientoCa));
            lstRQDeBE = objRQDeBL.ListarItemsxRequerimiento(objRequerimientoDe);
            return lstRQDeBE;

        }
    }
}