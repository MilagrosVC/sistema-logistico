﻿using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;
using System.Net.Mail;

namespace SEC_WEB.Logistica.Compras
{
    public partial class SolicitudCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.hdRequerimiento.Value = Request["Requerimiento_Id"];
                this.hdProyecto.Value = Request["Proyecto_Id"];
                this.hdMailUsu.Value = "andres.caycho@dinetech.pe";
                this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
                /*Usuario user = HttpContext.Current.Session[Constantes.SessionVars.Usuario] as Usuario;
                if (user == null)
                {
                    this.hdf_Usuario_Smo.Value = "0";
                }
                else
                {
                    this.hdf_Usuario_Smo.Value = user.Codigo;
                }*/
            }
        }

        /*[WebMethod]
        public static List<Dictionary<string, object>> ListarProyectos()
        {
            ProyectoBL objProvBL = new ProyectoBL();

            DataTable dtProveedor = new DataTable();
            ProyectoBE objProveedor = new ProyectoBE();
            dtProveedor = objProvBL.ListarProyecto(objProveedor);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtProveedor.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtProveedor.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }*/

        [WebMethod]
        public static List<ProveedorBE> ListarComboProveedor(ProveedorBE objProveedor)
        {
            ProveedorBL objProvBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();

            lstProveedor = objProvBL.ListarComboProveedores(objProveedor);

            return lstProveedor;
        }
        [WebMethod]
        public static List<ProveedorBE> ListarProveedoresSeleccionados(ProveedorBE objProveedor)
        {
            ProveedorBL objProvBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            if (objProveedor.Lst_Proveedor.Length >= 0) {
                lstProveedor = objProvBL.ListarComboProveedores(objProveedor);
            }
            return lstProveedor;
        }
        [WebMethod]
        public static List<RequerimientoDeBE> ListarItemsxRequerimiento(RequerimientoDeBE objRequerimientoDe)
        {
            RequerimientoDeBL objRQDeBL = new RequerimientoDeBL();
            List<RequerimientoDeBE> lstRQDeBE = new List<RequerimientoDeBE>();

            //lstRQCaBE = objRQCaBL.CargarBandejaRQ(JsonConvert.DeserializeObject<RequerimientoCaBE>(objRequerimientoCa));
            lstRQDeBE = objRQDeBL.ListarItemsxRequerimiento(objRequerimientoDe);
            return lstRQDeBE;

        }
        
        [WebMethod]
        public static RequerimientoCaBE ListarRequerimientoCa(RequerimientoCaBE objRequerimientoCaBE)
        {
            RequerimientoCaBL objRequerimientoCaBL = new RequerimientoCaBL();
            objRequerimientoCaBE = objRequerimientoCaBL.ListarRequerimientoCa(objRequerimientoCaBE)[0];
            return objRequerimientoCaBE;
        }



        [WebMethod]
        public static ProyectoBE DatosProyecto(ProyectoBE objProyectoBE)
        {
            ProyectoBL objProyectoBL = new ProyectoBL();
            objProyectoBE = objProyectoBL.DatosProyecto(objProyectoBE);
            return objProyectoBE;

        }

        [WebMethod]
        public static int CrearSolicitudCayDe(SolicitudCaBE ObjSolicitudCa)
        {
            SolicitudCaBL objSolicitudCaBL = new SolicitudCaBL();
            int id = 0;
            id = objSolicitudCaBL.CrearSolicitudCayDe(ObjSolicitudCa);
            return id;
        }

        [WebMethod]
        public static List<SolicitudCaBE> ListarSolicitudes(SolicitudCaBE ObjSolicitudCa)
        {
            SolicitudCaBL objSolicitudCaBL = new SolicitudCaBL();
            List<SolicitudCaBE> lstSolicitudCa = new List<SolicitudCaBE>();
            lstSolicitudCa = objSolicitudCaBL.ListarSolicitudes(ObjSolicitudCa);
            return lstSolicitudCa;
        }

        [WebMethod]
        public static List<SolicitudDeBE> ListarSolicitudDetalle(SolicitudDeBE ObjSolicitudDe)
        {
            SolicitudDeBL objSolicitudDeBL = new SolicitudDeBL();
            List<SolicitudDeBE> lstSolicitudDe = new List<SolicitudDeBE>();
            lstSolicitudDe = objSolicitudDeBL.ListarSolicitudDetalle(ObjSolicitudDe);
            return lstSolicitudDe;
        }

        [WebMethod]
        public static void EditarSolicitudDetalle(SolicitudDeBE ObjSolicitudDe)
        {
            SolicitudDeBL objSolicitudDeBL = new SolicitudDeBL();
            objSolicitudDeBL.EditarSolicitudDetalle(ObjSolicitudDe);
        }

        [WebMethod]
        public static int EnviarMailSolicitud(string sDe, string sPara, string sCCPara, string sAsunto, string sContenido)
        {
            List<string> olPara = new List<string>();
            olPara.Add(sPara);
            
            List<string> olCCPara = new List<string>();
            if(sCCPara != ""){
                olCCPara.Add(sCCPara);
            }

            int res = SEC_Utilitarios.Email.Enviar(sDe, olPara, sAsunto, sContenido, olCCPara, true);
            
            return res;
        }


        [WebMethod]
        public static int InsertarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            SolicitudProveedorBL objSolicitudPrvBL = new SolicitudProveedorBL();
            int id = 0;
            id = objSolicitudPrvBL.InsertarProveedorSolicitud(ObjSolicitudPrv);
            return id;
        }

        [WebMethod]
        public static List<SolicitudProveedorBE> ListarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            SolicitudProveedorBL objSolicitudPrvBL = new SolicitudProveedorBL();
            List<SolicitudProveedorBE> lstSolicitudPrv = new List<SolicitudProveedorBE>();
            lstSolicitudPrv = objSolicitudPrvBL.ListarProveedorSolicitud(ObjSolicitudPrv);
            return lstSolicitudPrv;
        }

        [WebMethod]
        public static int EliminarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            SolicitudProveedorBL objSolicitudPrvBL = new SolicitudProveedorBL();
            int id = 0;
            id = objSolicitudPrvBL.EliminarProveedorSolicitud(ObjSolicitudPrv);
            return id;
        }
    }
}