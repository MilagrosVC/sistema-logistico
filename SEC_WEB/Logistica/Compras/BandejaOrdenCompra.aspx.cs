﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SEC_WEB.Logistica.Compras
{
    public partial class BandejaOrdenCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<OrdenCompraCaBE> ListarOrdenCompra(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            List<OrdenCompraCaBE> lstOrdenCompraCaBE = new List<OrdenCompraCaBE>();
            lstOrdenCompraCaBE = ObjOrdenCompraCaBL.ListarOrdenCompraCabecera(ObjOrdenCompraCa);
            return lstOrdenCompraCaBE;
        }

        [WebMethod]
        public static List<EstadoDeBE> ListarComboEstado(EstadoDeBE ObjEstadoDe)
        {
            EstadoDeBL objEstadoDeBL = new EstadoDeBL();
            List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();
            lstEstadoDe = objEstadoDeBL.ListarEstados(ObjEstadoDe);
            return lstEstadoDe;
        }

        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }

        [WebMethod]
        public static List<ProveedorBE> ListarComboProveedor(ProveedorBE objProveedor)
        {
            ProveedorBL objProveedorBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorBL.ListarProveedores(objProveedor);
            return lstProveedor;
        }


        //MV 07 Agosto
        [WebMethod]
        public static OrdenCompraCaBE GenerarVistaOrdenCCA(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            return ObjOrdenCompraCaBL.GenerarVistaOrdenCCA(ObjOrdenCompraCa);
        }

        //MV 07 Agosto
        [WebMethod]
        public static List<OrdenCompraDeBE> GenerarVistaOrdenCDetalle(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            OrdenCompraDeBL ObjOrdenCompraDeBL = new OrdenCompraDeBL();
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = ObjOrdenCompraDeBL.GenerarVistaOrdenCDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;

        }
        //MV 07 Agosto
        [WebMethod]
        public static List<EgresoBE> ListarEgreso(EgresoBE ObjEgreso)
        {
            EgresoBL ObjEgresoBL = new EgresoBL();
            List<EgresoBE> lstEgreso = new List<EgresoBE>();
            lstEgreso = ObjEgresoBL.ListarEgreso(ObjEgreso);
            return lstEgreso;
        }
    }
}