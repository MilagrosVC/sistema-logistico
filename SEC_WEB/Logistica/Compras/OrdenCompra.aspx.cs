﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;
using System.Diagnostics;


namespace SEC_WEB.Logistica.Compras
{
    public partial class OrdenCompra : System.Web.UI.Page
    {
        //String targetPath = "C:\\Users\\PC02\\Documents\\ExcelGenerados";        
        protected void Page_Load(object sender, EventArgs e)
        {
            /*if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }*/
            //Session["Proyecto_Id"] = 1;
            //Session["Usuario_Id"] = "MPonceG";
            //this.hdf_Proyecto_Id.Value = Session["Proyecto_Id"].ToString();
            this.hdf_Proyecto_Id.Value = HttpContext.Current.Session[Constantes.SessionVariables.Proyecto_Id].ToString();
            this.hdf_Usuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            //this.hdf_Usuario.Value = Session["Usuario_Id"].ToString();
        }

        [WebMethod]
        public static List<CotizacionCaBE> ListarCotizacion(CotizacionCaBE objCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            List<CotizacionCaBE> lstCotizacionCaBE = new List<CotizacionCaBE>();
            lstCotizacionCaBE = objCotizacionCaBL.ListarCotizaciones(objCotizacionCa);
            return lstCotizacionCaBE;
        }
        
        [WebMethod]
        public static List<ProveedorBE> ListarComboProveedor(ProveedorBE objProveedor)
        {
            ProveedorBL objProveedorBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorBL.ListarProveedores(objProveedor);
            return lstProveedor;
        }
        
        [WebMethod]
        public static List<ProveedorBE> CargaDatosProveedor(ProveedorBE ObjProveedor)
        {
            ProveedorBL objProveedorBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorBL.ListarProveedores(ObjProveedor);
            return lstProveedor;
        }
        
        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }
        
        [WebMethod]
        public static List<MonedaBE> ListarComboMoneda(MonedaBE ObjMoneda)
        {
            MonedaBL objMonedaBL = new MonedaBL();
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            lstMoneda = objMonedaBL.ListarMoneda(ObjMoneda);
            return lstMoneda;
        }
        
        [WebMethod]
        public static List<TipoMedioPagoBE> ListarComboTipoMedioPago(TipoMedioPagoBE ObjTipoMedioPago)
        {
            TipoMedioPagoBL objTipoMedioPagoBL = new TipoMedioPagoBL();
            List<TipoMedioPagoBE> lstTipoMedioPago = new List<TipoMedioPagoBE>();
            lstTipoMedioPago = objTipoMedioPagoBL.ListarTipoMedioPago(ObjTipoMedioPago);
            return lstTipoMedioPago;
        }

        [WebMethod]
        public static List<CotizacionDeBE> ListarCotizacionDetalle(CotizacionDeBE objCotizacionDe)
        {
            CotizacionDeBL objCotizacionDeBL = new CotizacionDeBL();
            List<CotizacionDeBE> lstCotizacionDeBE = new List<CotizacionDeBE>();
            lstCotizacionDeBE = objCotizacionDeBL.ListarCotizacionDetalle(objCotizacionDe);
            return lstCotizacionDeBE;
        }
        
        [WebMethod]
        public static int CrearOrdenCompraCabecera(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            int id = 0;
            id = ObjOrdenCompraCaBL.CrearOrdenCompraCabecera(ObjOrdenCompraCa);
            return id;
        }

        [WebMethod]
        public static void CambiarEstadoCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            objCotizacionCaBL.CambiarEstadoCotizacionCabecera(ObjCotizacionCa);    
        }        
        
        [WebMethod]
        public static List<EgresoBE> ListarEgreso(EgresoBE ObjEgreso)
        {
            EgresoBL ObjEgresoBL = new EgresoBL();
            List<EgresoBE> lstEgreso = new List<EgresoBE>();
            lstEgreso = ObjEgresoBL.ListarEgreso(ObjEgreso);
            return lstEgreso;
        }
        
        [WebMethod]
        public static OrdenCompraCaBE GenerarVistaOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            return ObjOrdenCompraCaBL.GenerarVistaOrdenCompraCa(ObjOrdenCompraCa);
        }

        [WebMethod]
        public static List<OrdenCompraDeBE> GenerarVistaOrdenCompraDe(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            OrdenCompraDeBL ObjOrdenCompraDeBL = new OrdenCompraDeBL();
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = ObjOrdenCompraDeBL.GenerarVistaOrdenCompraDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;
        }

        [WebMethod]
        public static int ActualizarOrdenCompra(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            int id = 0;
            id = ObjOrdenCompraCaBL.ActualizarOrdenCompraCa(ObjOrdenCompraCa);
            return id;
        }  
                
    }
}