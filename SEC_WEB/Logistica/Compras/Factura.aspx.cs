﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Compras
{
    public partial class Factura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdf_Usuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();

            this.hdf_Orden_Compra_Co.Value = Request["Orden_Compra_Co"];
            
        }

        [WebMethod]
        public static OrdenCompraCaBE ObtenerOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            return ObjOrdenCompraCaBL.ObtenerOrdenCompra(ObjOrdenCompraCa);
        }

        [WebMethod]
        public static OrdenCompraCaBE GenerarVistaOrdenCompraCa(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            return ObjOrdenCompraCaBL.GenerarVistaOrdenCompraCa(ObjOrdenCompraCa);
        }

        [WebMethod]
        public static List<OrdenCompraDeBE> GenerarVistaOrdenCompraDe(OrdenCompraDeBE ObjOrdenCompraDe)
        {
            OrdenCompraDeBL ObjOrdenCompraDeBL = new OrdenCompraDeBL();
            List<OrdenCompraDeBE> lstOrdenCompraDe = new List<OrdenCompraDeBE>();
            lstOrdenCompraDe = ObjOrdenCompraDeBL.GenerarVistaOrdenCompraDetalle(ObjOrdenCompraDe);
            return lstOrdenCompraDe;
        }

        [WebMethod]
        public static int CrearComprobanteCabecera(ComprobanteCaBE ObjComprobanteCa)
        {
            ComprobanteCaBL ObjComprobanteCaBL = new ComprobanteCaBL();
            int id = 0;
            id = ObjComprobanteCaBL.CrearOrdenCompraCabecera(ObjComprobanteCa);
            return id;
        }


        [WebMethod]
        public static ComprobanteCaBE ExisteFactura(ComprobanteCaBE ObjComprobanteCa)
        {
            ComprobanteCaBL ObjComprobanteCaBL = new ComprobanteCaBL();
            return ObjComprobanteCaBL.ExisteFactura(ObjComprobanteCa);
        }

        [WebMethod]
        public static List<OrdenCompraCaBE> ListarOrdenCompra(OrdenCompraCaBE ObjOrdenCompraCa)
        {
            OrdenCompraCaBL ObjOrdenCompraCaBL = new OrdenCompraCaBL();
            List<OrdenCompraCaBE> lstOrdenCompraCaBE = new List<OrdenCompraCaBE>();
            lstOrdenCompraCaBE = ObjOrdenCompraCaBL.ListarOrdenCompraCabecera(ObjOrdenCompraCa);
            return lstOrdenCompraCaBE;
        }
    }
}