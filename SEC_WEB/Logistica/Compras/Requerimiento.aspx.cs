﻿using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Compras
{
    public partial class Solicitud : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Proyecto_Id"] = 1;
            //Session["Usuario_Id"] = "MPonceG";
            //Session["Presupuesto_Id"] = 3;
            //this.hdf_Proyecto_Id.Value = Session["Proyecto_Id"].ToString();
            //this.hdf_Presupuesto_Id.Value = Session["Presupuesto_Id"].ToString();
            this.hdf_RequerimientoCabecera_Id.Value = Request["Requerimiento_Id"];
            this.hdf_Proyecto_Id.Value = HttpContext.Current.Session[Constantes.SessionVariables.Proyecto_Id].ToString();
            this.hdf_Usuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            this.hdf_Presupuesto_Id.Value = HttpContext.Current.Session[Constantes.SessionVariables.Presupuesto].ToString();
            //this.hdf_Usuario.Value = Session["Usuario_Id"].ToString();
        }

        [WebMethod]
        public static RequerimientoCaBE ObtenerDatosRequerimiento(RequerimientoCaBE ObjRequerimientoCa)
        {
            RequerimientoCaBL ObjRequerimientoCaBL = new RequerimientoCaBL();
            return ObjRequerimientoCaBL.ObtenerDatosRequerimiento(ObjRequerimientoCa);
        } 



        [WebMethod]
        public static List<ProyectoBE> ListarProyectoCombo(ProyectoBE ObjProyecto)
        {
            ProyectoBL objProyectoBL = new ProyectoBL();
            List<ProyectoBE> lstProyecto = new List<ProyectoBE>();
            int codigo = ObjProyecto.Empresa_Id;
            //lstaParametro = objParametroBL.ListarParametros(JsonConvert.DeserializeObject<ParametroBE>(ObjetoParametroBE));
            lstProyecto = objProyectoBL.ListarProyectos(codigo);
            return lstProyecto;
        }


        [WebMethod]
        public static List<PartidaBE> ListarPartidaCombo(PartidaBE ObjPartida)
        {
            PartidaBL objPartidaBL = new PartidaBL();
            List<PartidaBE> lstPartida = new List<PartidaBE>();
            //lstaParametro = objParametroBL.ListarParametros(JsonConvert.DeserializeObject<ParametroBE>(ObjetoParametroBE));
            lstPartida = objPartidaBL.ListarPartida(ObjPartida);
            return lstPartida;
        }

        [WebMethod]
        public static List<SubPartidaBE> ListarSubPartidaCombo(SubPartidaBE ObjSubPartida)
        {
            SubPartidaBL objSubPartidaBL = new SubPartidaBL();
            List<SubPartidaBE> lstSubPartida = new List<SubPartidaBE>();
            //lstaParametro = objParametroBL.ListarParametros(JsonConvert.DeserializeObject<ParametroBE>(ObjetoParametroBE));
            lstSubPartida = objSubPartidaBL.ListarSubPartida(ObjSubPartida);
            return lstSubPartida;
        }

        [WebMethod]
        public static List<ItemBE> ListarItemCombo(ItemBE ObjItem)
        {
            ItemBL objItemBL = new ItemBL();
            List<ItemBE> lstItem = new List<ItemBE>();
            //lstaParametro = objParametroBL.ListarParametros(JsonConvert.DeserializeObject<ParametroBE>(ObjetoParametroBE));
            lstItem = objItemBL.ListarItem(ObjItem);
            return lstItem;
        }

        
        [WebMethod]
        public static List<TurnoBE> ListarTurnoCombo(TurnoBE ObjTurno)
        {
            TurnoBL objTurnoBL = new TurnoBL();
            List<TurnoBE> lstTurno = new List<TurnoBE>();
            lstTurno = objTurnoBL.ListarTurno(ObjTurno);
            return lstTurno;
        }


        [WebMethod]
        public static List<ItemBE> ListarItemsSubItems(ItemBE objItem)
        {
            ItemBL objItemBL = new ItemBL();
            List<ItemBE> lstItemBE = new List<ItemBE>();
            lstItemBE = objItemBL.ListarItemSubItem(objItem);
            return lstItemBE;
        }


        [WebMethod]
        public static int CrearRequerimientoCabecera(RequerimientoCaBE ObjRequerimientoCa)
        {
            /*SolicitudCaBL objSolicitudCaBL = new SolicitudCaBL();
            int id = 0;
            id = objSolicitudCaBL.CrearRequerimientoCabecera(ObjSolicitudCa);
            return id;*/

            RequerimientoDeBE ObjRequerimientoDe = new RequerimientoDeBE()
            {
                Requerimiento_Id = ObjRequerimientoCa.Requerimiento_Id,
                Item_Id = ObjRequerimientoCa.Item_Id,
                RDe_Cantidad_Nu = ObjRequerimientoCa.RDe_Cantidad_Nu,
                RDe_Descripcion_Tx = ObjRequerimientoCa.RDe_Descripcion_Tx,
                RDe_Observacion_Tx = ObjRequerimientoCa.RDe_Observacion_Tx,
                Unidad_Medida_Id = ObjRequerimientoCa.Unidad_Medida_Id,
                IMa_Id = ObjRequerimientoCa.IMa_Id,
                Aud_UsuarioCreacion_Id = ObjRequerimientoCa.Aud_UsuarioCreacion_Id
            };

            RequerimientoCaBL objRequerimientoCaBL = new RequerimientoCaBL();
            int id = 0;

            if (ObjRequerimientoCa.Req_Ingreso.Equals("nuevo"))
            {
                id = objRequerimientoCaBL.CrearRequerimientoCabecera(ObjRequerimientoCa);                
            }

            if (ObjRequerimientoDe.Requerimiento_Id == 0)
            {
                ObjRequerimientoDe.Requerimiento_Id = id;
            }
            //return id;

            RequerimientoDeBL objRequerimientoDeBL = new RequerimientoDeBL();
            int id2 = 0;
            id2 = objRequerimientoDeBL.CrearRequerimientoDetalle(ObjRequerimientoDe);
            return id;


        }


        [WebMethod]
        public static List<RequerimientoDeBE> ListarRequerimientoDetalle(RequerimientoDeBE ObjRequerimientoDe)
        {
            RequerimientoDeBL objRequerimientoDeBL = new RequerimientoDeBL();
            List<RequerimientoDeBE> lstRequerimientoDe = new List<RequerimientoDeBE>();
            lstRequerimientoDe = objRequerimientoDeBL.ListarItemsxRequerimiento(ObjRequerimientoDe);
            return lstRequerimientoDe;
        }


        [WebMethod]
        public static List<UndMedidaBE> ListarUnidadMedidaCombo(UndMedidaBE ObjUndMedida)
        {
            UndMedidaBL objUndMedidaBL = new UndMedidaBL();
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            lstUndMedida = objUndMedidaBL.ListarUnidadMedida(ObjUndMedida);
            return lstUndMedida;
        }

        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }

        [WebMethod]
        public static void EliminarRqDetalle(RequerimientoDeBE ObjRequerimientoDe)
        {
            RequerimientoDeBL objRequerimientoDeBL = new RequerimientoDeBL();
            objRequerimientoDeBL.EliminarRqDetalle(ObjRequerimientoDe);
        }


        [WebMethod]
        public static int ActualizarRequerimientoCa(RequerimientoCaBE ObjRequerimientoCa)
        {
            RequerimientoCaBL objRequerimientoCaBL = new RequerimientoCaBL();
            int id = 0;
            id = objRequerimientoCaBL.ActualizarRequerimientoCa(ObjRequerimientoCa);
            return id;
        }

        [WebMethod]
        public static int ActualizarRequerimientoDe(RequerimientoDeBE ObjRequerimientoDe)
        {
            RequerimientoDeBL objRequerimientoDeBL = new RequerimientoDeBL();
            int id = 0;
            id = objRequerimientoDeBL.ActualizarRequerimientoDe(ObjRequerimientoDe);
            return id;
        }

        [WebMethod]
        public static List<FamiliaBE> ListarFamiliaCombo(FamiliaBE ObjFamilia)
        {
            FamiliaBL ObjFamiliaBL = new FamiliaBL();
            List<FamiliaBE> lstFamilia = new List<FamiliaBE>();
            lstFamilia = ObjFamiliaBL.ListarFamilia(ObjFamilia);
            return lstFamilia;
        }

        [WebMethod]
        public static List<MarcaBE> ListarMarcaCombo(MarcaBE ObjMarca)
        {
            MarcaBL ObjMarcaBL = new MarcaBL();
            List<MarcaBE> lstMarca = new List<MarcaBE>();
            lstMarca = ObjMarcaBL.ListarMarca(ObjMarca);
            return lstMarca;
        }

        [WebMethod]
        public static List<Item_MaBE> ListarItemsMa(Item_MaBE ObjItem_Ma)
            {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            List<Item_MaBE> lstItemMa = new List<Item_MaBE>();
            lstItemMa = ObjItemMaBL.ListarItem_Ma(ObjItem_Ma);
            return lstItemMa;
        }
    }
}