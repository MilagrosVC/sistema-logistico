﻿using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;
using SEC_Utilitarios.Enums;
using System.Net.Mail;

namespace SEC_WEB.Logistica.Compras
{
    public partial class CotizacionRequerimiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdRequerimiento.Value = Request["Requerimiento_Id"];
            this.hdProyecto.Value = Request["Proyecto_Id"];
            this.hdSolicitud.Value = Request["Solicitud_Id"];
            this.hdf_Cotizacion_Id.Value = Request["Cotizacion_Id"];
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
            var varEstCotGanadora = Convert.ToInt32(eCotizacionCa.ganadora);
            this.hdf_EstCotGanadora.Value = varEstCotGanadora.ToString();
            var varEstCotCreada = Convert.ToInt32(eCotizacionCa.creada);
            this.hdf_EstCotCreada.Value = varEstCotCreada.ToString();

            var varEstCotOCGenerada = Convert.ToInt32(eCotizacionCa.OCGenerada);
            var varEstCotEnviada_OC = Convert.ToInt32(eCotizacionCa.Enviada_OC);
            var varEstCotEnviada_SA = Convert.ToInt32(eCotizacionCa.Enviada_SA);
            this.hdf_EstCotOCGenerada.Value = varEstCotOCGenerada.ToString();
            this.hdf_EstCotEnviada_OC.Value = varEstCotEnviada_OC.ToString();
            this.hdf_EstCotEnviada_SA.Value = varEstCotEnviada_SA.ToString();


            ListarEstadoxCA();
            
        }

            #region Carga de Combos

        [WebMethod]
        public static List<ProveedorBE> ListarComboProveedor(ProveedorBE objProveedor)
        {
            ProveedorBL objProvBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();

            lstProveedor = objProvBL.ListarComboProveedores(objProveedor);

            return lstProveedor;
        }
        [WebMethod]
        public static List<MonedaBE> ListarComboMoneda(MonedaBE ObjMoneda)
        {
            MonedaBL objMonedaBL = new MonedaBL();
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            lstMoneda = objMonedaBL.ListarMoneda(ObjMoneda);
            return lstMoneda;
        }
        [WebMethod]
        public static List<TipoMedioPagoBE> ListarComboTipoMedioPago(TipoMedioPagoBE ObjTipoMedioPago)
        {
            TipoMedioPagoBL objTipoMedioPagoBL = new TipoMedioPagoBL();
            List<TipoMedioPagoBE> lstTipoMedioPago = new List<TipoMedioPagoBE>();
            lstTipoMedioPago = objTipoMedioPagoBL.ListarTipoMedioPago(ObjTipoMedioPago);
            return lstTipoMedioPago;
        }
        [WebMethod]
        public static List<UndMedidaBE> ListarUnidadMedidaCombo(UndMedidaBE ObjUndMedida)
        {
            UndMedidaBL objUndMedidaBL = new UndMedidaBL();
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            lstUndMedida = objUndMedidaBL.ListarUnidadMedida(ObjUndMedida);
            return lstUndMedida;
        }
        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }
        [WebMethod]
        public static List<EstadoDeBE> ListarComboEstado(EstadoDeBE ObjEstadoDe)
        {
            EstadoDeBL objEstadoDeBL = new EstadoDeBL();
            List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();

            var varEstCotDet = Enum.GetName(typeof(ESTADO_COTIZACION_DETALLE), 4);  

            lstEstadoDe = objEstadoDeBL.ListarEstados(ObjEstadoDe);
            return lstEstadoDe;
        }
        #endregion

        private void ListarEstadoxCA()
        {

            var varEstCotDet = typeof(ESTADO_COTIZACION_DETALLE).Name;
            //
            EstadoDeBL objEstadoDeBL = new EstadoDeBL();
            List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();
            lstEstadoDe = objEstadoDeBL.ListarEstadosxCA(varEstCotDet);
            var varEstCotDetCreadaId = lstEstadoDe.Where(x => x.Est_Nombre_Tx == ESTADO_COTIZACION_DETALLE.CREADO.ToString()).FirstOrDefault().Estado_Id;
            this.hdf_EstCotDetCreadaId.Value = varEstCotDetCreadaId.ToString();
            var varEstCotDetIngresadaId = lstEstadoDe.Where(x => x.Est_Nombre_Tx == ESTADO_COTIZACION_DETALLE.INGRESADO.ToString()).FirstOrDefault().Estado_Id;
            this.hdf_EstCotDetIngresadaId.Value = varEstCotDetIngresadaId.ToString();
            //hdf_e_EstCotDet.Value = varEstCotDetCreada;  
            //return lstEstadoDe;


        }

        //[WebMethod]
        //public static void ListarEstadoxCA()
        //{

        //    var varEstCotDet = typeof(ESTADO_COTIZACION_DETALLE).Name;
        //    //
        //    EstadoDeBL objEstadoDeBL = new EstadoDeBL();
        //    List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();
        //    lstEstadoDe = objEstadoDeBL.ListarEstadosxCA(varEstCotDet);
        //    var varEstCotDetCreada = lstEstadoDe.Where(x => x.Est_Nombre_Tx == ESTADO_COTIZACION_DETALLE.CREADO.ToString()).FirstOrDefault().Estado_Id;
        //    //hdf_e_EstCotDet.Value = varEstCotDetCreada;  
        //    //return lstEstadoDe;
        //}


        [WebMethod]
        public static int CrearCotizacionCayDe(CotizacionCaBE objCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            int id = 0;
            id = objCotizacionCaBL.CrearCotizacionCayDe(objCotizacionCa);
            return id;
        }

        [WebMethod]
        public static List<CotizacionCaBE> ListarCotizaciones(CotizacionCaBE objCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            List<CotizacionCaBE> lstCotizacionCa = new List<CotizacionCaBE>();
            lstCotizacionCa = objCotizacionCaBL.ListarCotizaciones(objCotizacionCa);
            return lstCotizacionCa;
        }

        [WebMethod]
        public static List<CotizacionDeBE> ListarCotizacionDetalle(CotizacionDeBE objCotizacionDe)
        {
            CotizacionDeBL objCotizacionDeBL = new CotizacionDeBL();
            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            lstCotizacionDe = objCotizacionDeBL.ListarCotizacionDetalle(objCotizacionDe);
            return lstCotizacionDe;
        }

        [WebMethod]
        public static void ActualizarDetalleCotizacion(CotizacionDeBE objCotizacionDe)
        {
            CotizacionDeBL objCotizacionDeBL = new CotizacionDeBL();
            objCotizacionDeBL.ActualizarDetalleCotizacion(objCotizacionDe);
        }

        [WebMethod]
        public static void SeleccionCotizacionGanadora(CotizacionCaBE ObjCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            objCotizacionCaBL.SeleccionCotizacionGanadora(ObjCotizacionCa);
        }

        [WebMethod]
        public static void ActualizarCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            objCotizacionCaBL.ActualizarCotizacionCabecera(ObjCotizacionCa);
        }

        [WebMethod]
        public static List<SolicitudProveedorBE> ListarProveedorSolicitud(SolicitudProveedorBE ObjSolicitudPrv)
        {
            SolicitudProveedorBL objSolicitudPrvBL = new SolicitudProveedorBL();
            List<SolicitudProveedorBE> lstSolicitudPrv = new List<SolicitudProveedorBE>();
            lstSolicitudPrv = objSolicitudPrvBL.ListarProveedorSolicitud(ObjSolicitudPrv);
            return lstSolicitudPrv;
        }


        [WebMethod]
        public static int CrearSolicitudAnticipo(SolicitudAnticipoBE ObjSolicitudAnticipo)
        {
            SolicitudAnticipoBL objSolAnticipoBL = new SolicitudAnticipoBL();
            int i = 0;
            i = objSolAnticipoBL.CrearSolicitudAnticipo(ObjSolicitudAnticipo);
            return i;
        }

        [WebMethod]
        public static void CambiarEstadoCotizacionCabecera(CotizacionCaBE ObjCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            objCotizacionCaBL.CambiarEstadoCotizacionCabecera(ObjCotizacionCa);
        }

         //ModificarEstadoAnticipo - 08 Septiembre PV

        [WebMethod]
        public static void EliminarRAP(SolicitudAnticipoBE ObjSolicitudAnticipo)
        {
            SolicitudAnticipoBL objSolAnticipoBL = new SolicitudAnticipoBL();

            objSolAnticipoBL.EliminarRAP(ObjSolicitudAnticipo);
            
        }

        [WebMethod]
        public static SolicitudAnticipoBE CargarRAP(SolicitudAnticipoBE ObjSolicitudAnticipo)
        {
            SolicitudAnticipoBE BE = new SolicitudAnticipoBE();
            SolicitudAnticipoBL objSolAnticipoBL = new SolicitudAnticipoBL();

            BE = objSolAnticipoBL.cargarRAP(ObjSolicitudAnticipo);
            return BE;
        }

    }
}