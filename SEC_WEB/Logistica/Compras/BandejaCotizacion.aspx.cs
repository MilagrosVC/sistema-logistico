﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Compras
{
    public partial class BandejaCotizacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();



            //Capturar estados
            int creada = (int)SEC_Utilitarios.Enums.eCotizacionCa.creada;
            hdfCreada.Value = Convert.ToString(creada);

            int ganadora = (int)SEC_Utilitarios.Enums.eCotizacionCa.ganadora;
            hdfGanadora.Value = Convert.ToString(ganadora);

            int Desaprobada = (int)SEC_Utilitarios.Enums.eCotizacionCa.Desaprobada;
            hdfDesaprobada.Value = Convert.ToString(Desaprobada);

            int OCGenerada = (int)SEC_Utilitarios.Enums.eCotizacionCa.OCGenerada;
            hdfOCGenerada.Value = Convert.ToString(OCGenerada);

            int Enviada_OC = (int)SEC_Utilitarios.Enums.eCotizacionCa.Enviada_OC;
            hdfEnviada_OC.Value = Convert.ToString(Enviada_OC);

            int Enviada_SA = (int)SEC_Utilitarios.Enums.eCotizacionCa.Enviada_SA;
            hdfEnviada_SA.Value = Convert.ToString(Enviada_SA);

            int DenegadaA = (int)SEC_Utilitarios.Enums.eCotizacionCa.DenegadaA;
            hdfDenegadaA.Value = Convert.ToString(DenegadaA);

            int Desaprobada_excede_presupuesto = (int)SEC_Utilitarios.Enums.eCotizacionCa.Desaprobada_excede_presupuesto;
            hdfDesaprobada_excede_presupuesto.Value = Convert.ToString(Desaprobada_excede_presupuesto);
            
        }
        [WebMethod]
        public static List<CotizacionCaBE> ListarCotizaciones(CotizacionCaBE objCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            List<CotizacionCaBE> lstCotizacionCa = new List<CotizacionCaBE>();
            lstCotizacionCa=objCotizacionCaBL.ListarCotizaciones(objCotizacionCa);
            return lstCotizacionCa;
        }


        [WebMethod]
        public static CotizacionCaBE GenerarVistaCotizacionCA(CotizacionCaBE objCotizacionCa)
        {
            CotizacionCaBL objCotizacionCaBL = new CotizacionCaBL();
            return objCotizacionCaBL.GenerarVistaCotizacionCA(objCotizacionCa);
        }


        [WebMethod]
        public static List<CotizacionDeBE> GenerarVistaCotizacionDetalle(CotizacionDeBE ObjCotizacionDe)
        {
            CotizacionDeBL ObjCotizacionDeBL = new CotizacionDeBL();
            List<CotizacionDeBE> lstCotizacionDe = new List<CotizacionDeBE>();
            lstCotizacionDe = ObjCotizacionDeBL.GenerarVistaCotizacionDetalle(ObjCotizacionDe);
            return lstCotizacionDe;
        }

        [WebMethod]
        public static List<ProveedorBE> ListarComboProveedor(ProveedorBE objProveedor)
        {
            ProveedorBL objProveedorBL = new ProveedorBL();
            List<ProveedorBE> lstProveedor = new List<ProveedorBE>();
            lstProveedor = objProveedorBL.ListarProveedores(objProveedor);
            return lstProveedor;
        }
        [WebMethod]
        public static List<EstadoDeBE> ListarComboEstado(EstadoDeBE ObjEstadoDe)
        {
            EstadoDeBL objEstadoDeBL = new EstadoDeBL();
            List<EstadoDeBE> lstEstadoDe = new List<EstadoDeBE>();
            lstEstadoDe = objEstadoDeBL.ListarEstados(ObjEstadoDe);
            return lstEstadoDe;
        }

    }
}