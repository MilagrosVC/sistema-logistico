﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="BandejaCotizacion.aspx.cs" Inherits="SEC_WEB.Logistica.Compras.BandejaCotizacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
    .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
    #tbBusqueda td {
            padding:3px 5px;
        }

</style>
    <link href="../../Scripts/Styles_SEC/SEC_BandCotizacion.css" rel="stylesheet" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div style="margin-left:18px">
     <asp:HiddenField ID="hdUsuario" runat="server" />

        <asp:HiddenField ID="hdfCreada" runat="server" />
        <asp:HiddenField ID="hdfGanadora" runat="server" />
        <asp:HiddenField ID="hdfDesaprobada" runat="server" />
        <asp:HiddenField ID="hdfOCGenerada" runat="server" />
        <asp:HiddenField ID="hdfEnviada_OC" runat="server" />
        <asp:HiddenField ID="hdfEnviada_SA" runat="server" />
        <asp:HiddenField ID="hdfDenegadaA" runat="server" />
        <asp:HiddenField ID="hdfDesaprobada_excede_presupuesto" runat="server" />

      <h3><b>Bandeja de Cotizaciones</b></h3>
    <div id="dvFiltros"style="border:solid 2px;">
        <div style="background-color:#333;border-bottom:solid 2px;">
            <b style="color:#fff;font-size:12px; padding-left:5px">Filtros de Busqueda</b>
        </div>
          <div style="margin:10px 0;">
               <table id="tbBusqueda" style="margin-bottom:8px;">
                <tr>
                    <td>Cod. Requerimiento:</td>
                    <td><input type="text" id="txtCodRequerimiento" class="inputs" name="" value="" placeholder="Ingrese Codigo"/></td>
                    <td>Cod. Proforma:</td>
                    <td><input type="text" id="txtCodProforma" class="inputs" name="" value="" placeholder="Ingrese Codigo"/></td>
                    <td>Proveedor:</td>
                    <td style="padding-left:15px;">
                        <select id="ddlProveedorCo" name="ddlProveedorCo" class="inputs" style="height:30px; width:250px;">
                        </select>                    
                    </td>
                    <td>Estado:</td>
                    <td style="padding-left:15px;">
                        <select id="ddlEstadoCo" name="ddlEstadoCo" class="inputs" style="height:30px; width:250px;">
                        </select>                    
                    </td>
                </tr>
            </table>


            <input type="button" id="btnBuscar" value="Buscar" class="boton" style="margin-left:5px;"/>
            <input type="button" id="btnLimpiar" value="Limpiar" class="boton"/> 
        </div>


    </div>
    <br />
    <!--  grid-->
    <div id="CotizacionesGrid">

    </div>
    <!-- ------------------------------>


        <div id="SEC_TotalesCo">
        <table>
            <tr>
                <td><b>TOTAL:</b></td>
                <td style="border-right:2px solid"><label id="CoTotal"></label></td>

                <td style="padding-left:18px;">Creada:</td>
                <td style="border-right:2px solid; text-align:right;"><label id="CoCreada"></label><span style="margin:0 5px;padding-left:18px;background-color:#C6F296;border:1px solid;border-color:#222;"></span></td>
                
                <td style="padding-left:18px;">Ganadora: </td>
                <td style="border-right:2px solid; text-align:right;"><label id="CoGanadora"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
                
                <td style="padding-left:18px;">Desaprobada:</td>
               <td style="border-right:2px solid; text-align:right;"><label id="CoDesaprobada"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
               
                <td style="padding-left:18px;">O/C Generada:</td>
               <td style="border-right:2px solid; text-align:right;"><label id="CoGenerada"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
               
                <td style="padding-left:18px;">Enviada a Orden Compra:</td>
               <td style="border-right:2px solid; text-align:right;"><label id="CoEnviadaOC"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
               
                <td style="padding-left:18px;">Requiere Anticipo:</td>
               <td style="border-right:2px solid; text-align:right;"><label id="CoReqAnticipo"></label><span style="margin:0 5px;padding-left:18px;background-color:#fff;border:1px solid;border-color:#222;"></span></td>
               </tr>
                <tr><td style="height:4px"></td></tr>
                <tr>
                <td style="padding-left:18px;"></td>
                <td style="border-right:2px solid; text-align:right;"><label id=""></label></td>
            
                <td style="padding-left:18px;">Anticipo Denegado:</td>
               <td style="border-right:2px solid; text-align:right;"><label id="CoAnticipoDenegado"></label><span style="margin:0 5px;padding-left:18px;background-color:#D56A6A;border:1px solid;border-color:#222;"></span></td>
           
                 <td style="padding-left:18px;">Excede Presupuesto:</td>
                 <td style="border-right:2px solid; text-align:right;"><label id="CoExcedePre"></label><span style="margin:0 5px;padding-left:18px;background-color:#D56A6A;border:1px solid;border-color:#222;"></span></td>
                    
<%--                    <td style="border-right:2px solid; text-align:right;"><label id="CoExcedePre"></label><span style="margin-left:5px;padding-left:18px;background-color:#D56A6A;border:1px solid;border-color:#222;"></span></td>--%> <%--C6B6C1--%>
            
            </tr>
        </table>
    </div>




        </div>
    <div id="dvVerCotizacion" style="display:none;" > <%--style="display:none;"--%>
        <div id="divImprimirCo">
        
       <%-- <img src="../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/logoDinetech.png"  style="position:relative;top:-35px"/>--%>
        
        <label id="lblCo-CodCotizacion" style="float:right;"></label><br style="clear:both;"/>
        <label id="lblCo-Fecha" style="float:right;"></label>
            <h4 style="text-align:left;"><label id="lblCo-Empresa_RZ"></label></h4>
        <%-- <h5 style="text-align:center;"><label id="lblCo-Empresa_RZ"></label></h5>--%>
      <%--  <br style="clear:both;"/>--%>

              <table id="tbVerCo" style="font-size:10px;">
                  <tr>
                      <td>
                          <table id="DatosdeReceptor" style="border:black 1px solid; width:700px">
                                <tr>
                                    <td class="td-lbl" style="width:20%">DIRIGIDO A :</td>
                                    <td class="td-lbl"><label id="lblCo-ReceptorNombre"></label></td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="td-lbl" style="width:13%">RUC :</td>
                                    <td class="td-lbl"><label id="lblCo-ReceptorRUC"></label></td>
                                </tr>
                                <tr>
                                    <td class="td-lbl">DIRECCION :</td>
                                    <td class="td-lbl"><label id="lblCo-ReceptorDireccion"></label></td>
                                    <td></td><td></td><td></td>
                                </tr>
                          </table>
                      </td>
                  </tr>
                 <%-- <tr style="height:1px;">
                    <td></td>
                  </tr>--%>
                  <tr>
                      <td>
                        <table id="DatosdeEmisor" style="border:black 1px solid; width:700px">
                            <tr>
                                    <td class="td-lbl" style="width:20%">RUC:</td><td class="td-lbl" style="width:200px"><label id="lblCo-EmisorRUC"></label></td>
                                    <%--<td class="td-lbles"></td>--%>
                                   <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="td-lbl" style="width:13%">VENDEDOR :</td><td class="td-lbl"><label id="lblCo-EmisorVendedorNom"></label></td>
                                </tr>    
                                <tr>
                                    <td class="td-lbl">MONEDA:</td><td class="td-lbl"><label id="lblCo-EmisorMoneda"></label></td>
                                <%--<td class="td-lbles"></td>--%>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 <td class="td-lbl">CELULAR :</td><td class="td-lbl"><label id="lblCo-EmisorVendedorCel"></label></td>
                                </tr>
                                <tr>
                                    <td class="td-lbl" style="vertical-align:middle">CONDICION DE PAGO :</td><td class="td-lbl"  colspan="2"><label id="lblCo-EmisorCondPago"></label></td>
                                  <%--  <td class="td-lbles"></td>--%>
                                    <%--<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                                    <td class="td-lbl" style="vertical-align:middle">MAIL :</td><td class="td-lbl" style="vertical-align:middle"><label id="lblCo-EmisorVendedorMail"></label></td>
                                </tr>
                                <%--<tr>
                                    <td>VENDEDOR :</td><td class="td-lbl"><label id="lblCo-EmisorVendedorNom"></label></td>
                                </tr>--%>
                                <%--<tr>
                                    <td>CELULAR :</td><td class="td-lbl"><label id="lblCo-EmisorVendedorCel"></label></td>
                                </tr>--%>
                                <%--<tr>
                                    <td>MAIL :</td><td class="td-lbl"><label id="lblCo-EmisorVendedorMail"></label></td>
                                </tr>--%>
                          </table>
                        </td>
                  </tr>
            
            <%--<tr>
                <td>RAZON SOCIAL :</td><td class="td-lbl"><label id="lblCo-RazonSocial"></label></td>
            </tr>
            <tr>
                <td>DOMICILIO :</td><td class="td-lbl"><label id="lblCo-Domicilio"></label></td>
            </tr>  --%>
            <tr style="height:5px;">
                <td></td>
            </tr>
            <tr>
                <td id="tdDetalle" colspan="2">
                    <table id="tbCo-Detalle" style="font-size:10px;">
                        <tr>
                            <th style="width:50px">CANTIDAD</th>
                            <th style="width:60px">UND</th>
                            <th style="width:300px">ARTICULO</th>
                            <th style="width:60px">PRECIO UND</th>
                            <th style="width:100px">TOTAL</th>
                        </tr>  
                        <tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />

        </div>

        <div style="text-align:center;">
            <input type="button" id="btnImprimirCo" name="name" value="Imprimir" class="boton"/>
            <input type="button" id="btnCerrarVerCo" name="name" value="Cerrar" class="boton"/>   
            <input type="button" id="btnExportCo" value="Generar Excel" class="boton" />
        </div>
    </div> 
    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
    <script src="../../Scripts/JS_SEC/Logistica/Compras/BandejaCotizacion.js"></script>
</asp:Content>
