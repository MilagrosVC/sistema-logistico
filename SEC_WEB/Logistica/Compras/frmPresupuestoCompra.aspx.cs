﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data;
using SEC_BE;
using SEC_BL;

namespace SEC_WEB.Logistica.Compras
{
    public partial class frmPresupuestoCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<Dictionary<string, object>> ListarProveedor()
        {
            EmpresaBL objProvBL = new EmpresaBL();

            DataTable dtProveedor = new DataTable();
            EmpresaBE objProveedor = new EmpresaBE();
            dtProveedor = objProvBL.ListarEmpresa(objProveedor);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtProveedor.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtProveedor.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }
    }
}