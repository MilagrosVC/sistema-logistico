﻿using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SEC_WEB.Logistica.Compras
{
    public partial class BandejaFactura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<ComprobanteCaBE> ListarFacturas(ComprobanteCaBE ObjComprobanteCa)
        {
            ComprobanteCaBL ObjComprobanteCaBL = new ComprobanteCaBL();
            List<ComprobanteCaBE> lstComprobanteCa = new List<ComprobanteCaBE>();
            lstComprobanteCa = ObjComprobanteCaBL.ListarComprobanteCa(ObjComprobanteCa);
            return lstComprobanteCa;
        }

        [WebMethod]
        public static ComprobanteCaBE GenerarVistaComprobanteCa(ComprobanteCaBE ObjComprobanteCa)
        {
            ComprobanteCaBL ObjComprobanteCaBL = new ComprobanteCaBL();
            return ObjComprobanteCaBL.GenerarVistaComprobante(ObjComprobanteCa);
        }

        [WebMethod]
        public static List<ComprobanteDeBE> GenerarVistaComprobanteDe(ComprobanteDeBE ObjComprobanteDe)
        {
            ComprobanteDeBL ObjComprobanteDeBL = new ComprobanteDeBL();
            List<ComprobanteDeBE> lstComprobanteDe = new List<ComprobanteDeBE>();
            lstComprobanteDe = ObjComprobanteDeBL.GenerarVistaComprobanteDetalle(ObjComprobanteDe);
            return lstComprobanteDe;
        }
    }
}