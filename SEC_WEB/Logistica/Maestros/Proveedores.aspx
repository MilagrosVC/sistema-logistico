﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Proveedores.aspx.cs" Inherits="SEC_WEB.Logistica.Maestros.Proveedores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Scripts/Styles_SEC/SEC_Proveedor.css" rel="stylesheet" />
     <style>
        body {
            /*background-color:#222;*/
            background-color: #fff;
        }
         #TbFiltroBusq td {
            padding: 4px 6px;
        }
        #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;
            font-size: 14px;
        }

        .boton {
            margin: 0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color: #fff;
            color: #444;
            font-weight: bold;
        }

        .inputs {
            border: 1px solid;
            height: 28px;
            border-radius: 4px;
            padding: 3px;
            border-color: #9d9d9d;
        }
        .inputsColspan {
            border: 1px solid #9d9d9d;
            height: 28px;
            border-radius: 4px;
            /*padding: 3px;*/
            display: block;
  width: 97%;
  /*height: 34px;*/
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  /*background-color: #fff;*/
  background-image: none;
  /*border: 1px solid #ccc;*/
  /*border-radius: 4px;*/
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        #tbBusqueda td {
            padding: 3px 5px;
        }

        #SEC_TotalesBandRQ td {
            padding: 0 5px;
        }

        #SEC_TotalesBandRQ label {
            margin-top: 5px;
        }

        .ui-state-highlight {
            border: 1px solid #9d9d9d;
            background-color: #222;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/Proveedor/SEC_Proveedor.js"></script>
    <asp:HiddenField ID="hdUsuario" runat="server" />

     <asp:HiddenField ID="hdfPersonaNatural" runat="server" />
     <asp:HiddenField ID="hdfPersonaJuridica" runat="server" />

    <!--<div id="Content-Proveedor">-->
    <input type="hidden" id="hdfCodigoUbigeo"  />
    <input type="hidden" id="txtEstadoID" />
 <%--   <input type="hidden" id="hddCodigoProveedor" />--%>

    
     <div id="divUbigeo" style="display:none"></div>
     <%--style="margin-left: 25px"--%>
    <div id="PantallaPrincipal" >
     <h3>Proveedor</h3>
    <!-------Filtros de Busqueda ------ Filtrobusqueda----->
    <%--<div id="Filtrobusqueda" style="width:80%;">--%>
        <div id="Filtrobusqueda" style="border: solid 2px">
        <div style="background-color: #333; border-bottom: solid 2px;">
            <b style="color: #fff; font-size: 12px; padding-left: 5px">Filtros de Busqueda</b>
        </div>
            <div style="margin: 10px 0;">
                <label for="txtTipoProveedor">&nbsp;&nbsp;Seleccione el Proveedor a Buscar:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                <input type="radio" name="RdbtnTipoProveedor"  value="RdbtnNatural" id="RdbtnNatural"/> Natural &nbsp;&nbsp;&nbsp;&nbsp;
                <input type="radio" name="RdbtnTipoProveedor" value="RdbtnJuridico" id="RdbtnJuridico"/> Juridico
                <br/>

            <table id="TbFiltroBusq" style="margin-bottom: 8px;">

        <tr>
            <td id="tdLApellidoPaterno"> 
                <label for="txtbusqApellidoPaterno"> Apellido Paterno:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIApellidoPaterno"> 
                <input id="txtbusqApellidoPaterno" type="text" onkeypress="validarletras()" placeholder="Ingrese Apellido P." class="inputs" name="txtbusqApellidoPaterno" value="" />
            </td>
            <%--<td id="TdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
            <td id="tdLApellidoMaterno"> 
                <label for="txtbusqApellidoMaterno"> Apellido Materno:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIApellidoMaterno"> 
                <input id="txtbusqApellidoMaterno" type="text" onkeypress="validarletras()" class="inputs" placeholder="Ingrese Apellido M." name="txtbusqApellidoMaterno" value="" />
            </td>
            <%--<td id="TdEsp2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
            <td id="tdLRazonSocial"> 
                <label for="txtbusqRazSocial"> Razon Social:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIRazonSocial"> 
                <input id="txtbusqRazSocial" type="text" onkeypress="validarletras()" class="inputs"placeholder="Ingrese Razon S."  name="txtbusqRazSocial" value="" />
            </td>
           <%-- <td id="TdEsp3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
            <td id="tdLPersonaContacto"> 
                <label for="txtbusqPersContacto"> Persona Contacto:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdIPersonaContacto"> 
                <input id="txtbusqPersContacto" type="text" onkeypress="validarletras()" class="inputs" placeholder="Ingrese Persona C." name="txtbusqPersContacto" value="" />
            </td>
            <%--<td id="TdEsp4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
            <td id="tdLNroDocumento"> 
                <label for="txtbusqNroDocumento"> Nro Documento:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td id="tdINroDocumento"> 
                <input id="txtbusqNroDocumento" type="text"  onkeypress="return validarnumeros(event)"  placeholder="Ingrese N° Doc." class="inputs" name="txtbusqNroDocumento" value="" />
            </td>
        </tr>
        <%--<tr><td colspan="11">&nbsp;&nbsp;</td></tr> --%>
<%--        <tr id="TrBtnBusqueda"> 
            <td colspan="11" style="text-align:right" >
                 
            </td>
        </tr>--%>
    </table>
       <%-- <br/>--%>
        <%--<table>
            <tr>
                <td style="width:2000px;">--%>
        &nbsp;&nbsp;<input type="button" style="width:140px" id="btnAVRegistroPN" class="boton" name="name" value="Proveedor Natural" />
        <input type="button" style="width:140px" id="btnAVRegistroPJ" class="boton" name="name" value="Proveedor Juridico" />
        <input type="button" id="btnRealizarBusqueda" class="boton" name="name" value="Buscar" />
        <input type="button" id="btnLimpiarBusqueda" class="boton" name="name" value="Limpiar" /> 
             <%--   </td>
            </tr>
        </table>--%>
    <br/>
</div>
        </div>


    <!-------Ventana para Registrar Proveedor ----------->
    <div id="Proveedor-Reg" style='display:none;'>
    <table class="table">
        <tr id="trDatosPrincipalesNatural">
        
       <%-- <tr id="trAPaterno">--%>
            <td>
                <label for="txtApePaterno">Apellido Paterno:  </label>
            </td>
            <td>
                <input id="txtApePaterno" style="width:158px" type="text" onkeypress="return validarletras(event)" class="inputs" name="txtApePaterno" value="" required="required"/>
            </td>
       <%-- </tr>--%>

       <%-- <tr id="trAMaterno">--%>
            <td>
                <label for="txtApeMaterno">Apellido Materno:  </label>
            </td>
            <td>
                <input id="txtApeMaterno" style="width:158px" type="text" onkeypress="return validarletras(event)" class="inputs" name="txtApeMaterno" value="" required="required"/>
            </td>
        <%--</tr>--%>

        <%--<tr id="trNombres">--%>
            <td>
                <label for="txtNombres">Nombres:</label>
            </td>
            <td>
                <input id="txtNombres" type="text" onkeypress="return validarletras(event)" style="width:158px" class="inputs" name="txtNombres" value="" required="required"/>
            </td>
       <%-- </tr>--%>

        </tr>

        <tr id="tdDatosPrincipalesJuridico">
           <td id="tdLNumeroRuc">
                <label for="txtNumeroRUC">Número de RUC:  </label>
            </td>
            <td id="tdINumeroRuc">
               <%-- maxlength="11" --%>
                <input id="txtNumeroRUC" type="text" maxlength="11" onkeypress="return validarnumeros(event)" style="width:158px" class="inputs" name="txtNumeroRUC" value="" required="required"/>
            </td>

        <%--<tr id="trRazonSocial">--%>
            <td>
                <label for="txtRazonSocial">Razón Social:  </label>
            </td>
            <td>
                <input id="txtRazonSocial" type="text" class="inputs" name="txtRazonSocial" value="" style="width:158px" required="required"/>
            </td>
        <%--</tr>--%>
      

           
             <%--<tr id="trPContacto">--%>
            <td>
                <label for="txtPersonaContacto">Persona Contacto:  </label>
            </td>
            <td>
                <input id="txtPersonaContacto" type="text" onkeypress="return validarletras(event)" class="inputs" style="width:158px" name="txtPersonaContacto" value="" required="required" />
            </td>
         <%--</tr>--%> 
        </tr>

        <tr>
       <%-- <tr>--%>
            <td id="tdLTipoDocumento">
                <label for="ddlTipoDocumento">Tipo Documento:  </label>
            </td>
            <td id="dlSTipoDocumento">
                <select id="ddlTipoDocumento" class="inputs" style="width:158px">
                    <option value="0"> SELECCIONE </option>
                </select>

            </td>
        <%-- </tr>--%>
        <%--<tr>--%>
            <td id="tdLtxtNumeroDoc">
                <label for="txtNumeroDocumento">Número Documento:  </label>
            </td>
            <td id="tdItxtNumeroDoc">
                <input id="txtNumeroDocumento" onkeypress="return validarnumeros(event)" type="text" class="inputs" style="width:158px" name="txtNumeroDocumento" value="" required="required"/>
            </td>
         <%--</tr>--%>

        <%--<tr id="trFNacimiento">--%>
            <td id="tdLFNacimineto">
                <label for="txtFechaNacimiento">Fecha Nacimiento:  </label>
            </td>
            <td id="tdIFNaciemiento">
                <!--<input id="txtFechaNacimiento" type="date" class="form-control" name="txtFechaNacimiento" max="1997-12-31"  value="" required="required"/>-->
            </td>
        <%--</tr>--%>

        </tr>

        <tr>
            <!--<td>
                <label for="txtUbigeoID">Ubigeo ID:  </label>
            </td>
            <td>
                <input id="txtUbigeoID" type="text" class="form-control" name="txtUbigeoID" value="" required="required"/>
            </td>-->
            <td><label for="ddlDepartamento">Departamento: </label></td>
            <td>
                <select id="ddlDepartamento" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlProvincia">Provincia: </label></td>
            <td>
                <select id="ddlProvincia" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlDistrito">Distrito: </label></td>
            <td>
                <select id="ddlDistrito" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>  
        </tr>

        <tr>
            <td>
                <label for="txtDireccion">Direccion:  </label>
            </td>
            <td colspan="3">
                <input id="txtDireccion" type="text" class="inputsColspan" name="txtDireccion" style="width:468px" value="" required="required" />
            </td>

           <td>
                <label for="txtemail">Email:  </label>
            </td>
            <td>
                <input id="txtemail" type="email" class="inputs" name="txtemail" value="" style="width:158px" required="required"/>
            </td>
        </tr>

        <tr>
       <%-- <tr>--%>
            <td>
                <label for="txttelefono1">Télefono 1:  </label>
            </td>
            <td>
                <input id="txttelefono1" type="text" onkeypress="return validartelefono(event)" class="inputs" style="width:158px" name="txttelefono1" value="" required="required"/>
            </td>
        <%--</tr>--%>
        <%--<tr>--%>
            <td>
                <label for="txttelefono2">Télefono 2:  </label>
            </td>
            <td>
                <input id="txttelefono2" type="text"  onkeypress="return validartelefono(event)" class="inputs" style="width:158px" name="txttelefono2" value="" required="required"/>
            </td>
        <%--</tr>--%>
       <%-- <tr>--%>
            

             <td>
               <%-- <label for="txtProvClaID">Proveedor Clasificación-ID:</label>--%>
                  <label for="txtProvClaID">Clasificación:</label>
            </td>
            <td>
                
               <%-- <input id="txtProvClaID" type="text" class="form-control" name="txtProvClaID" value="" required="required"/>--%>
                <select id="ddlProvedorClasificacion" class="inputs" style="width:158px">
                    <option value="0"> SELECCIONE </option>
                </select>
            </td>
        <%--</tr>--%>
        </tr>

        <tr>

<%--            <td>
                <label for="txtRubroID">Rubro ID:</label>
            </td>
            <td>
                <input id="txtRubroID" type="text" class="form-control" name="txtRubroID" value="" required="required"/>
                <input type="button" id="btnAgregarRubro" class="btn btn-default btn-sm" name="name" value="Agregar Rubro" />
            </td>--%>

      <%--  <tr>--%>
            
        <%--</tr>--%>
        </tr>          
        <!--<tr>
            <td>
                <label for="txtTipoPersonaID">Tipo Persona ID:</label>
            </td>
            <td>
                <input id="txtTipoPersonaID" type="text" class="form-control" name="txtTipoPersonaID" value="" required="required"/>
            </td>
        </tr>-->

        <tr>
     <%--   <tr>--%>
            <td>
                <label for="chkProveedorHomologadoFl">Proveedor Homologado:</label>
            </td>
            <!--<td>
                <input id="txtProveedorHomologadoFL" type="text" class="form-control" name="txtProveedorHomologadoFL" value="" required="required"/>
            </td>-->
           <td>
               <input type="checkbox" id="chkProveedorHomologadoFl" class="inputs" value=" "/>
           </td>   
      <%--  </tr>--%>
       <%-- <tr>--%>
            <td>
              <%--  <label for="txtPrvEstadoHomologadoID">Estado Homologado ID:</label>--%>
                <label for="txtPrvEstadoHomologadoID">Estado Homologado:</label>
            </td>
            <td>
                <input id="txtPrvEstadoHomologadoID" type="text" readonly="true" class="inputs" style="width:158px" onkeypress="return validarnumeros(event)" name="txtPrvEstadoHomologadoID" value="" required="required"/>
            </td>
        <%--</tr>--%><!---->
        <%--<tr>--%>
            <td>
               <%-- <label for="txtProvHomologadoraID">Proveedor Homologador ID:</label>--%>
                <label for="txtProvHomologadoraID">Proveedor Homologador:</label>
            </td>
            <td>
                <input id="txtProvHomologadoraID" type="text" readonly="true" class="inputs" style="width:158px" onkeypress="return validarnumeros(event)"  name="txtProvHomologadoraID" value="" required="required" />
            </td>
        <%--</tr>--%><!---->
        </tr>
<%--          <tr>
            <td>
                <label for="txtEstadoID">Estado ID:</label>
            </td>
            <td>
                <input id="txtEstadoID" type="text" class="form-control" name="txtEstadoID" value="" required="required"/>
            </td>
        </tr>--%>


        <tr id="trbtnProveedorNatural">
            <td colspan="6" style="text-align:right" <%--align="right" class="btn btn-default btn-sm"--%>>
               <input type="button" style="width:185px" id="btnRegistrarProveedorNatural" class="boton" name="name" value="Registrar Proveedor Natural"  />
                <input type="button" style="width:150px" id="btnCancelarProveedorNatural" class="boton" name="name" value="Cancelar Registro"  />
            </td>
        </tr>
        <tr id="trbtnProveedorJuridico">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnRegistrarProveedorJuridico" class="boton" name="name" value="Registrar Proveedor Juridico"  />
                <input type="button" style="width:150px" id="btnCancelarProveedorJuridico" class="boton" name="name" value="Cancelar Registro"<%-- onclick="this.close();"--%> />
            </td>
        </tr>

        <tr id="trbtnModificarProveedorNatural">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnModificaraProveedorNatural" class="boton" name="name" value="Editar Proveedor Natural"  />    
                <input type="button" style="width:150px" id="btnCancelarModificarProveedorNatural" class="boton" name="name" value="Cancelar Edición"  />
            </td>
        </tr>

                <tr id="trbtnModificarProveedorJuridico">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:185px" id="btnModificaraProveedorJuridico" class="boton" name="name" value="Editar Proveedor Juridico"  />    
                <input type="button" style="width:150px" id="btnCancelarModificarProveedorJuridico" class="boton" name="name" value="Cancelar Edición"  />
            </td>
        </tr>
            <tr id="trbtnDetalleProveedor">
            <td colspan="6" style="text-align:right" <%--align="right"--%>>
               <input type="button" style="width:200px" id="btnDetalleRubros" class="boton" name="name" value="Visualizar Rubros del Proveedor"  />    
                <input type="button" style="width:195px" id="btnSalirDetalle" class="boton" name="name" value="Salir de Detalle de Proveedor"  />
            </td>
        </tr>

    </table>

        
    <div id="dvAsignarRubros">
      <%--  <br/>--%>
        <h5>Proceda a seleccionar los rubros que le correspondan:</h5>
        <table class="table">
            <tr>
        <%--<tr>--%>
            <%--<td>
                <label for="btnAgregarRubro">Rubro ID:</label>
            </td>--%>
            <td>           
           <%-- <label for="txtRubroID">Rubro ID:</label>
            </td>
            <td>
                <input id="txtRubroID" type="text" class="btn btn-default btn-sm" name="txtRubroID" value="" required="required"/>--%>

                <input type="button" id="btnAgregarRubro" style="width:190px"  class="boton" name="name" value="Agregar Rubro a Proveedor" />
                 <input type="button" id="btnSalirdelRegistro" style="width:190px"  class="boton" name="name" value="Salir sin seleccionar Rubro" />
             </td>
           </tr>
        </table>
    </div>
        
    </div>
    <!-------------------------------------------------------->
    <!------------------------ JqGrid ------------------------->
        <br />
    <div id="ProveedorGrid">
    </div>
    <!-------------------------------------------------------->
    </div>
    <!-------Ventana para Seleccionar el/los Rubro(s) del Proveedor ----------->
    <div id="ProveedorRubro" style="display:none">
        <%--<h5>Seleccione los Rubros del Proveedor</h5>--%>
        <input type="hidden" id="hdnIdProveedor" name="hdnIdProveedor" />
       <%-- <br/>--%>
        <table>
        <tr>
            <td style="text-align:center"><h5>Rubros Disponibles</h5></td>
            <td></td>
            <td style="text-align:center"><h5>Rubros Asignados</h5></td>
        </tr>
        <tr>
            <td>
                <div id="divRubro">
                <select multiple="multiple" class="form-control" id="ddlRubro" name="ddlRubro" style="width: 300px;height: 300px;"> </select>
                </div>
            </td>
       
            <td style="text-align: center; padding-left:20px; padding-right:20px;" id="tdbtnAsigRubro">
                
                <input type="button" id="btnAgregarSeleccionados" class="boton" value=">" style="width:35px;"/> <br/><br/>
                <input type="button" id="btnAgregarTodos" class="boton" value=">>" style="width:35px;"/> <br/><br/>
                <input type="button" id="btnQuitarTodos" class="boton" value="<<" style="width:35px;"/>  <br/><br/>
                <input type="button" id="btnQuitarSeleccionados" class="boton" value="<" style="width:35px;"/> <br/><br/>
            </td>
            <td style="text-align:center">
                <div id="divRubroProveedor">
                <select multiple="multiple" class="form-control" id="ddlRubroProveedor" name="ddlRubroProveedor" style="width: 300px;height: 300px;"> </select>
                </div>
            </td>
        </tr>   
        
            <tr><td colspan="3">&nbsp;&nbsp;</td></tr> 
            <tr id="trGuardarRubrosdeProveedor">
                <td colspan="3" style="text-align:right">
                    <input type="button" id="btnGuardarRubrosdeProveedor" style="width: 200px" class="boton" name="name" value="Guardar Rubros Asignados" />
                </td>
            </tr>
            <tr id="trSalirVentanaRubrosProveedor">
                <td colspan="3" style="text-align:right">
                    <input type="button" id="btnSalirVentanaRubrosProveedor" style="width: 200px" class="boton" name="name" value="Salir de Pantalla Actual" />
                </td>
            </tr>
        </table>
        <br/>
<%--        <table style="text-align:right">
            <tr>
             <td style="text-align:right">
              <input type="button" id="btnGuardarRubrosdeProveedor" class="btn btn-default btn-sm" name="name" value="Guardar Rubros Asignados" />
             </td>
            </tr>
        </table>--%>
    </div>
    <!------------------------------------------------------------------------>



    <!--</div>-->
</asp:Content>

 
