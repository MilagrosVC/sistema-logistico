﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Maestros
{
    public partial class Proveedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           //this.hdUsuario.Value = "System";
           this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();

           //Capturar Tipo de Persona
           int PersonaNatural = (int)SEC_Utilitarios.Enums.eTipoPersona.PersonaNatural;
           hdfPersonaNatural.Value = Convert.ToString(PersonaNatural);

           int PersonaJuridica = (int)SEC_Utilitarios.Enums.eTipoPersona.PersonaJuridica;
           hdfPersonaJuridica.Value = Convert.ToString(PersonaJuridica);
        }


        [WebMethod]
        public static int CrearProveedor(ProveedorBE proveedor)
        {
            ProveedorBL objProvBL = new ProveedorBL();
            int id = 0;
            id = objProvBL.CrearProveedor(proveedor);
            return id;
        }

        [WebMethod]
        public static List<Dictionary<string, object>> ListarProveedor()
        {
            ProveedorBL objProvBL = new ProveedorBL();

            DataTable dtProveedor = new DataTable();
            ProveedorBE objProveedor = new ProveedorBE();
            dtProveedor = objProvBL.ListarProveedor(objProveedor);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtProveedor.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtProveedor.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }

        [WebMethod]
        //public static List<ProveedorBE> ListarProveedores(string objProveedor)
        public static List<ProveedorBE> ListarProveedores(ProveedorBE objProveedor)
        {
            ProveedorBL objProveedorBL = new ProveedorBL();
            List<ProveedorBE> lstProveedorBE = new List<ProveedorBE>();

            //lstProveedorBE = objProveedorBL.ListarProveedores(JsonConvert.DeserializeObject<ProveedorBE>(objProveedor));
            lstProveedorBE = objProveedorBL.ListarProveedores(objProveedor);
            return lstProveedorBE;

        }

        /*[WebMethod]
        public static List<Dictionary<string, object>> ListarUbigeo()
        {
            UbigeoBL objUbigeoBL = new UbigeoBL();
            DataTable dtUbigeo = new DataTable();
            UbigeoBE objUbigeo = new UbigeoBE();

            dtUbigeo = objUbigeoBL.ListarUbigeo(objUbigeo);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dtUbigeo.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dtUbigeo.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return rows;
        }*/

        [WebMethod]
        public static List<UbigeoBE> ListarUbigeoCombo(string ubigeo)
        {
            UbigeoBL objUbigeoBL = new UbigeoBL();
            List<UbigeoBE> lstubigeoRtrn = new List<UbigeoBE>();
            
            lstubigeoRtrn = objUbigeoBL.ListarUbigeo(JsonConvert.DeserializeObject<UbigeoBE>(ubigeo));

            return lstubigeoRtrn;
            
        }

        [WebMethod]
        public static int ObtenerCodigoUbigeo(string ubigeo)
        {
            UbigeoBL objUbigeoBL = new UbigeoBL();
            int id;
            id = objUbigeoBL.ObtenerCodigoUbigeo(JsonConvert.DeserializeObject<UbigeoBE>(ubigeo));
            return id;
        }

        //PARA SOLO UN COMBO
        //[WebMethod] 
        //public static List<ParametroBE> ListarParametroCombo() {
        //    ParametroBL objParametroBL = new ParametroBL();
        //    List<ParametroBE> lstaparametro = new List<ParametroBE>();
        //    lstaparametro = objParametroBL.ListarParametro();
        //    return lstaparametro;
        
        //}

        [WebMethod]
        public static List<ParametroBE> ListarParametrosCombos(ParametroBE ObjetoParametroBE)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> listaParametro = new List<ParametroBE>();
            listaParametro = objParametroBL.ListarParametros(ObjetoParametroBE);
            return listaParametro;
        }

        [WebMethod]
        public static List<RubroBE> ListarRubroProveedor() {
            RubroBL ObjRubroBL = new RubroBL();
            List<RubroBE> lstRubro = new List<RubroBE>();
            lstRubro = ObjRubroBL.ListarRubro();
            return lstRubro;
        }

        [WebMethod]
        //public static List<RubroBE> ListarRubroProveedorDisponible(string IdProveedor)
        public static List<RubroBE> ListarRubroProveedorDisponible(ProveedorRubroBE ProveedorRubro)
        {
            ProveedorRubroBL obj = new ProveedorRubroBL();
            List<RubroBE> lstDisponibles = new List<RubroBE>();
            //lstDisponibles = obj.ListarRubroDisponible(JsonConvert.DeserializeObject<ProveedorRubroBE>(IdProveedor));
            lstDisponibles = obj.ListarRubroDisponible(ProveedorRubro);
            return lstDisponibles;
        }

        [WebMethod]
        public static List<RubroBE> ListarRubroAsignadoProveedor(ProveedorRubroBE ProveedorRubro) {
            ProveedorRubroBL obj = new ProveedorRubroBL();
            List<RubroBE> lstAsignados = new List<RubroBE>();
            lstAsignados = obj.ListarRubroAsignado(ProveedorRubro);
            return lstAsignados;
        }

        [WebMethod]
        public static void ModificarRubroaProveedor(string lstRubro, string Prov_Id, string usuario, string estado)
        {
            List<String> lstRubId = lstRubro.Split(',').ToList();
            List<ProveedorRubroBE> lstAsignaciones = new List<ProveedorRubroBE>();
            ProveedorRubroBL objPrBL = new ProveedorRubroBL();
            foreach (string RubroId in lstRubId)
            {              
                ProveedorRubroBE objPrBE = new ProveedorRubroBE();
                objPrBE.Rubro_Id=Convert.ToInt32(RubroId);
                objPrBE.Prv_Id = Convert.ToInt32(Prov_Id);
                objPrBE.Aud_UsuarioActualizacion_Id=usuario;
                objPrBE.Aud_UsuarioCreacion_Id=usuario;
                objPrBE.Aud_Estado_Fl = Convert.ToBoolean(estado);
                lstAsignaciones.Add(objPrBE);
            }
            objPrBL.AsignarDesignarRubro(lstAsignaciones);
        }

        //MODIFICAR PROVEEDOR
        [WebMethod]
        public static void ModificarProveedor(ProveedorBE ObjProveedor) {
           
            ProveedorBL objProvBL = new ProveedorBL();
             objProvBL.ModificarProveedor(ObjProveedor);    
        }
        //ELIMINAR PROVEEDOR
        [WebMethod]
        public static void EliminarProveedor(ProveedorBE objProveedor) {
            ProveedorBL objProvBL = new ProveedorBL();
            objProvBL.EliminarProveedor(objProveedor);
        }
    }
}