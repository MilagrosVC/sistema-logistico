﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Maestros
{
    public partial class Almacen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();

            this.hdfProyecto.Value = HttpContext.Current.Session[Constantes.SessionVariables.Proyecto_Id].ToString();
            //Capturar Proyecto
            //int Proyecto = (int)SEC_Utilitarios.Enums.eProyecto_Almacen.Proyecto;
            //hdfProyecto.Value = Convert.ToString(Proyecto);
            //TipoAlmacen
            int Interno = (int)SEC_Utilitarios.Enums.eProyecto_Almacen.Interno;
            hdfInterno.Value = Convert.ToString(Interno);

            int InternoAireLibre = (int)SEC_Utilitarios.Enums.eProyecto_Almacen.InternoAireLibre;
            hdfInternoAireLibre.Value = Convert.ToString(InternoAireLibre);

            int Externo = (int)SEC_Utilitarios.Enums.eProyecto_Almacen.Externo;
            hdfExterno.Value = Convert.ToString(Externo);

            int Arrendado = (int)SEC_Utilitarios.Enums.eProyecto_Almacen.Arrendado;
            hdfArrendado.Value = Convert.ToString(Arrendado);
        }

        [WebMethod]
        public static List<AlmacenBE> ListarAlmacen(AlmacenBE objAlmacen)
        {
            AlmacenBL objAlmacenBL = new AlmacenBL();
            List<AlmacenBE> lstAlmacen = new List<AlmacenBE>();
            lstAlmacen = objAlmacenBL.ListarAlmacen(objAlmacen);
            return lstAlmacen;
        }

        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }

        [WebMethod]
        public static List<ProyectoBE> ListarProyectoCombo(ProyectoBE objProyecto)
        {
            ProyectoBL objProyectoBL = new ProyectoBL();
            List<ProyectoBE> lstProyecto = new List<ProyectoBE>();
            lstProyecto = objProyectoBL.ListarProyectoCombo(objProyecto);
            return lstProyecto;
        }


        //UBIGEO
        [WebMethod]
        public static List<UbigeoBE> ListarUbigeoCombo(string ubigeo)
        {
            UbigeoBL objUbigeoBL = new UbigeoBL();
            List<UbigeoBE> lstubigeoRtrn = new List<UbigeoBE>();

            lstubigeoRtrn = objUbigeoBL.ListarUbigeo(JsonConvert.DeserializeObject<UbigeoBE>(ubigeo));

            return lstubigeoRtrn;

        }

        [WebMethod]
        public static int ObtenerCodigoUbigeo(string ubigeo)
        {
            UbigeoBL objUbigeoBL = new UbigeoBL();
            int id;
            id = objUbigeoBL.ObtenerCodigoUbigeo(JsonConvert.DeserializeObject<UbigeoBE>(ubigeo));
            return id;
        }

        [WebMethod]
        public static List<UsuarioBE> ListarComboUsuario(UsuarioBE objUsuarioBE)
        {
            UsuarioBL objUsuarioBL = new UsuarioBL();
            List<UsuarioBE> lstUsuario = new List<UsuarioBE>();
            lstUsuario = objUsuarioBL.ListarUsuario(objUsuarioBE);
            return lstUsuario;

        }


        [WebMethod]
        public static ProyectoBE CargarDatosProyecto(ProyectoBE ObjProyecto)
        {
            ProyectoBE objProyectoBE = new ProyectoBE();
            ProyectoBL objProyectoBL = new ProyectoBL();
            objProyectoBE = objProyectoBL.DatosProyecto(ObjProyecto);
            return objProyectoBE;
        }

        [WebMethod]
        public static int CrearAlmacen(AlmacenBE objAlmacen)
        {
            AlmacenBL objAlmacenBL = new AlmacenBL();
            int id = 0;
            id = objAlmacenBL.CrearAlmacen(objAlmacen);
            return id;
        }


        //Eliminar Almacen
        [WebMethod]
        public static void EliminarAlmacen(AlmacenBE objAlmacen)
        {
            AlmacenBL objAlmacenBL = new AlmacenBL();
            objAlmacenBL.EliminarAlmacen(objAlmacen);
        }

        //MODIFICAR Almacen
        [WebMethod]
        public static void ModificarAlmacen(AlmacenBE objAlmacen)
        {
            AlmacenBL objAlmacenBL = new AlmacenBL();
            objAlmacenBL.ModificarAlmacen(objAlmacen);
        }
    }
}