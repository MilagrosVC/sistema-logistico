﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="ItemMa.aspx.cs" Inherits="SEC_WEB.Logistica.Maestros.ItemMa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        body {
            /*background-color:#222;*/
            background-color: #fff;
        }

        #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;
            font-size: 14px;
        }

        .boton {
            margin: 0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color: #fff;
            color: #444;
            font-weight: bold;
        }

        .inputs {
            border: 1px solid;
            height: 28px;
            border-radius: 4px;
            padding: 3px;
            border-color: #9d9d9d;
        }
        .inputsColspan {
            border: 1px solid #9d9d9d;
            height: 28px;
            border-radius: 4px;
            /*padding: 3px;*/
            display: block;
  width: 97%;
  /*height: 34px;*/
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  /*background-color: #fff;*/
  background-image: none;
  /*border: 1px solid #ccc;*/
  /*border-radius: 4px;*/
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
          box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
       -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
          transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }

        #tbBusqueda td {
            padding: 3px 5px;
        }

        #SEC_TotalesBandRQ td {
            padding: 0 5px;
        }

        #SEC_TotalesBandRQ label {
            margin-top: 5px;
        }

        .ui-state-highlight {
            border: 1px solid #9d9d9d;
            background-color: #222;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/ItemMa/SEC_ItemMa.js"></script>
    <asp:HiddenField ID="hdUsuario" runat="server" />
      <input type="hidden" id="hdnIdItemMa" name="hdnIdItemMa" />
    <h3>Item</h3>
    <div id="dvFiltros" style="border: solid 2px;">
        <div style="background-color: #333; border-bottom: solid 2px;">
            <b style="color: #fff; font-size: 12px; padding-left: 5px">Filtros de Busqueda</b>
        </div>
        <div style="margin: 10px 0;">
            <table id="tbBusqueda" style="margin-bottom: 8px;">
                <tr>
                    <td>Nombre:</td>
                    <td>
                        <input type="text" id="txtNombreItemMa_Busq" class="inputs" name="" value="" placeholder="Ingrese el Nombre" /></td>
                    <td>Familia:</td>
                    <td>
                        <select id="ddlFamilia_Busq" class="inputs">
                            <option value="0">SELECCIONE </option>
                        </select></td>
                    <td>Marca:</td>
                    <td>
                        <select id="ddlMarca_Busq" class="inputs">
                            <option value="0">SELECCIONE </option>
                        </select></td>

                </tr>
            </table>
            <input type="button" id="btnNuevoItemMA" value="Nuevo Item" class="boton" style="margin-left: 5px;" />
            <input type="button" id="btnBuscar" value="Buscar" class="boton" style="margin-left: 5px;" />
            <input type="button" id="btnLimpiar" value="Limpiar" class="boton" />
        </div>
    </div>

    <br />
    <!--  grid-->
    <div id="divItemsMa">
    </div>
    <!-------------------------------->


    <!---Registro nuevo de item_ma--->
    <div id="ItemMa-Reg" style='display: none;' >
        <table class="table" >
            <tr>
                <td>
                  <%--  <label for="ddlProyecto">Proyecto:  </label>--%>
                      <label for="txtCodigo">Codigo:  </label>
                </td>
                <td>
                    <%-- <select id="ddlProyecto" class="form-control"><option value="0"> SELECCIONE </option></select>--%>
                    <input id="txtCodigo" type="text" class="inputs" name="txtCodigo" value="" required="required" />
                </td>

                <td>
                    <label for="txtNombre">Nombre:  </label>
                </td>
                <td>
                    <input id="txtNombre" type="text" class="inputs" name="txtNombre" value="" required="required" />
                </td>
                </tr>
            
                <tr>

                
                <td>
                    <label for="txtDescripcion">Descripcion:</label>
                </td>
           
                    <td colspan="3">
                        <input id="txtDescripcion" type="text" class="inputsColspan"  name="txtDescripcion" value="" required="required" />
                    </td>
            </tr>

            <tr>
                <td>
                    <label for="txtMetrado">Metrado:</label>
                </td>
                <td>
                    <input id="txtMetrado" type="text" class="inputs" name="txtMetrado" onkeypress="return ValidarMonto(event, this);" value="" required="required" />
                </td>

                <td>
                    <label for="ddlUnidadMedida">U. Medida:  </label>
                </td>
                <td>
                    <select id="ddlUnidadMedida" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>
                </tr>
            <tr>
                <td>
                    <label for="ddlFamilia">Familia:  </label>
                </td>
                <td>
                    <select id="ddlFamilia" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>

                <td>
                    <label for="ddlMarca">Marca:  </label>
                </td>
                <td>
                    <select id="ddlMarca" class="inputs" style="width:158px">
                        <option value="0">SELECCIONE </option>
                    </select>
                </td>
            </tr>
            
            <tr id="" style="margin-top: 30px;">
                <td colspan="4" style="text-align: right" <%--align="right" class="btn btn-default btn-sm"--%>>
                    <input type="button" style="width: 140px" id="btnRegistrar" class="boton" name="name" value="Registrar Item" />
                    <input type="button" style="width: 140px" id="btnModificar" class="boton" name="name" value="Modificar Item" />
                    <input type="button" style="width: 130px" id="btnLimpiarRegistro" class="boton" name="name" value="Limpiar" />
                    <input type="button" style="width: 140px" id="btnCancelarRegistro" class="boton" name="name" value="Cancelar Registro" />
                    <input type="button" style="width: 140px" id="btnCancelarModificar" class="boton" name="name" value="Cancelar Edición" />
                    <input type="button" style="width: 195px" id="btnSalirDetalle" class="boton" name="name" value="Salir de Detalle" />
                </td>
            </tr>
        </table>
    </div>

    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>
</asp:Content>
