﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Almacen.aspx.cs" Inherits="SEC_WEB.Logistica.Maestros.Almacen" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style>
        body{
            /*background-color:#222;*/
            background-color:#fff;
        }
        #MainSEC {
            padding: 0 10px 0 10px;
            color: #444;

            font-size:14px;
        }
        .boton{
            margin:0 10px 0 0;
            padding: 3px 5px;
            /*background-color:#222;*/
            background-color:#fff;
            color: #444;
            font-weight:bold;
        }
        .inputs {
            border: 1px solid;
            height:28px;
            border-radius:4px;
            padding:3px;
            border-color: #9d9d9d;
        }
        #tbBusqueda td {
            padding:3px 5px;
        }
        #SEC_TotalesBandRQ td {
            padding:0 5px;
        }
        #SEC_TotalesBandRQ label {
            margin-top:5px;
        }
        .ui-state-highlight{
            border: 1px solid #9d9d9d;
            background-color:#222;
            
        }
        /*Estilo para la grilla
        .ui-jqgrid .ui-jqgrid-titlebar {
            height: 26px;
            line-height:15px;
        }
        .ui-widget-header {
            border: 1px solid #9d9d9d;
            background:-webkit-gradient(linear, left top, left bottom, from(#9d9d9d), to(#222));
            line-height:10px;
        }*/

        /*Estilos para el DatePicker
        .ui-datepicker-title,.ui-datepicker table {
            font-size:.7em;
        }
        .ui-datepicker .ui-datepicker-prev{
            left: 2px;
            width: 16px;
            height: 20px;
        }
        .ui-datepicker .ui-datepicker-next{
            right: 2px;
            width: 16px;
            height: 20px;
        }
        img.ui-datepicker-trigger {
            position: relative;
            left: -18px;
            top: -1px;
        }*/
        
        /*Estilos para el input
        input {
            color:#000000;
            border-radius:3px;
            padding:0 5px;
        }
        input.placeholder {
            text-align: center;
        }*/
        /* Visualizacion de Requerimiento*/
        /*#dvVerRequerimiento {
            background-color:#222;
            width:
        }*/
        /*#divImprimirRQ { 
            margin: 30px auto; 
            padding: 20px 60px;
            border: 1px solid #D2D2D2;
            font-family: Arial; 
            font-size: 11px; 
            line-height: 1.6; 
            color: #444;
            
            width:800px;
            min-height:750px;  
        }
        .td-lbl{
            width:523px;
        }
        .td-lbl label{
            line-height: 0px;
        }
        #tbVerRq td{
            padding:10px 10px 0 0;
        }
        #tbVerRq #tdDetalle{
            padding:0px 0px 0 0;
        }
        #tbRq-Detalle th {
            text-align:center;
        }
        #tbRq-Detalle td,#tbRq-Detalle th {
            border: 1px solid black;padding:0px;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/Almacen/SEC_Almacen.js"></script>
    <%--style="margin-left: 25px"--%>
    <div id="PantallaPrincipal" >

        <asp:HiddenField ID="hdUsuario" runat="server" />
     <asp:HiddenField ID="hdfProyecto" runat="server" />
    <!--//Segun Tipo de Almacen --->
    <asp:HiddenField ID="hdfInterno" runat="server" />
    <asp:HiddenField ID="hdfInternoAireLibre" runat="server" />
    <asp:HiddenField ID="hdfExterno" runat="server" />
    <asp:HiddenField ID="hdfArrendado" runat="server" />

    <input type="hidden" id="hdfCodigoUbigeo"  />
    <input type="hidden" id="hdnIdAlmacen" name="hdnIdAlmacen" />

        <h3>Alamcen</h3>

            <div id="Filtrobusqueda"style="border:solid 2px;">
        <div style="background-color:#333;border-bottom:solid 2px;">
            <b style="color:#fff;font-size:12px; padding-left:5px">Filtros de Busqueda</b>
        </div>
                 <div style="margin:10px 0;">
      <%--  <div id="Filtrobusqueda" style="width: 80%; margin: 10px 0;">--%>
            <table id="tbBusqueda" style="margin-bottom:8px;">
                <tr>
                    <td id="tdLNombre">
                        <label for="txtbusqNombre">Nombre:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </td>
                    <td id="tdINombre">
                        <%--<input id="txtbusqNombre" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqNombre" value="" />--%>
                        <input id="txtbusqNombre" type="text" class="inputs" name="txtbusqNombre" value="" placeholder="Ingrese Nombre" />
                    </td>
                  <%--  <td id="TdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>--%>
                    <td id="tdLTipoAlmacen">
                        <label for="ddlTipoAlmacenBusq">Tipo Almacen:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    </td>
                    <td id="tdITipoAlmacen">
                        <%--<input id="txtbusqNombre" type="text" onkeypress="validarletras()" class="form-control" name="txtbusqNombre" value="" />--%>
                        <select id="ddlTipoAlmacenBusq" class="inputs"><option value="0"> SELECCIONE </option></select>
                    </td>
                  
                    <%--<td id="TdEsp1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        <td id="tdLResponsable"> 
                        <label for="txtbusqResponsable"> Responsable:&nbsp;&nbsp;&nbsp;&nbsp;</label>
                        </td>
                        <td id="tdIResponsable"> 
                   
                              <input id="txtbusqResponsable" type="text" onkeypress="validarletras()" name="txtbusqResponsable" class="inputs" value="" placeholder="Ingrese Responsable"  />
                        </td>--%>
                </tr>

            </table>
          <%--  <br />--%>
                      <input type="button" id="btnNuevoAlmacen" value="Nuevo Almacen" class="boton" style="margin-left: 5px;" />
            <input type="button" id="btnBuscar" value="Buscar" class="boton" style="margin-left: 5px;" />
            <input type="button" id="btnLimpiar" value="Limpiar" class="boton" style="width:120px;"/>
           
                     </div>
        </div>

        <br />
        <!--  grid-->
        <div id="AlmacenGrid">
        </div>
        <!-------------------------------->

        <!---Registro nuevo de almacen--->
        <div id="Almacen-Reg" style='display: none;'>
            <table class="table">
                <tr>
                    <td>
                        <label for="ddlProyecto">Proyecto:  </label>
                    </td>
                    <td>
                 <select id="ddlProyecto" class="inputs" style="width:158px"><option value="0"> SELECCIONE </option></select>
                    </td>

                    <td>
                        <label for="txtNombre">Nombre:  </label>
                    </td>
                    <td>
                        <input id="txtNombre" type="text" onkeypress="return validarletras(event)" class="inputs" name="txtNombre" value="" required="required" />
                    </td>
                     <td>
                        <label for="txtAbreviatura">Abreviatura:</label>
                    </td>
                    <td>
                        <input id="txtAbreviatura" type="text" class="inputs" name="txtAbreviatura" value="" required="required" />
                    </td>
                   
                   
                </tr>

                <tr>
                      <td>
                        <label for="ddlUsuarioResponsable">Responsable:  </label>
                    </td>
                    <%--<td>
                        <input id="txtResponsable" type="text" class="form-control" name="txtResponsable" value="" required="required" />

                    </td>--%>
                    <td>
                    <select id="ddlUsuarioResponsable" class="inputs" style="width:158px"><option value="0"> SELECCIONE </option></select>
                    </td>
                    <td>
                        <label for="txtTelefono">Telefono:  </label>
                    </td>
                    <td>
                        <input id="txtTelefono" type="text" class="inputs" name="txtTelefono" value="" required="required" />
                    </td>

                     <td>
                        <label for="ddlTipoAlmacen">Tipo:  </label>
                    </td>
                    <td>
                 <select id="ddlTipoAlmacen" class="inputs" style="width:158px"><option value="0"> SELECCIONE </option></select>
                    </td>
                </tr>
                <tr>
                    
                   <td>
                        <label for="txtDescripcion">Descripcion:  </label>
                    </td>
                    <td>
                        <input id="txtDescripcion" type="text" class="inputs" name="txtDescripcion" value="" required="required" />
                    </td>
                     <td>
                        <label for="txtDireccion">Direccion:  </label>
                    </td>
                    <td colspan="3">
                        <input id="txtDireccion" type="text" class="inputsColspan" name="txtDireccion" value="" required="required" style="width:450px"/>
                    </td>
                </tr>
                <tr>
                   <td><label for="ddlDepartamentoA">Departamento: </label></td>
            <td>
                <select id="ddlDepartamentoA" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlProvinciaA">Provincia: </label></td>
            <td>
                <select id="ddlProvinciaA" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>
            <td><label for="ddlDistritoA">Distrito: </label></td>
            <td>
                <select id="ddlDistritoA" class="inputs" style="width:158px">
                    <option value="0">SELECCIONE</option>
                </select>
            </td>  
                </tr>


                <tr id="" style=" margin-top:30px;">
            <td colspan="6" style="text-align:right" <%--align="right" class="btn btn-default btn-sm"--%>>
               <input type="button" style="width:185px" id="btnRegistrar" class="boton" name="name" value="Registrar Almacen"  />
                <input type="button" style="width:185px" id="btnModificar" class="boton" name="name" value="Modificar Almacen"  />
                 <input type="button" style="width:185px" id="btnLimpiarRegistro" class="boton" name="name" value="Limpiar"  />
                <input type="button" style="width:150px" id="btnCancelarRegistro" class="boton" name="name" value="Cancelar Registro"  />
                <input type="button" style="width:150px" id="btnCancelarModificar" class="boton" name="name" value="Cancelar Edición"  />
                <input type="button" style="width:195px" id="btnSalirDetalle" class="boton" name="name" value="Salir de Detalle"  />
            </td>
        </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>

</asp:Content>
