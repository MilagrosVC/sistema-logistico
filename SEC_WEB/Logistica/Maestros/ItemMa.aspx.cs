﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Logistica.Maestros
{
    public partial class ItemMa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }



        [WebMethod]
        public static List<FamiliaBE> ListarFamiliaCombo(FamiliaBE ObjFamilia)
        {
            FamiliaBL ObjFamiliaBL = new FamiliaBL();
            List<FamiliaBE> lstFamilia = new List<FamiliaBE>();
            lstFamilia = ObjFamiliaBL.ListarFamilia(ObjFamilia);
            return lstFamilia;
        }

        [WebMethod]
        public static List<MarcaBE> ListarMarcaCombo(MarcaBE ObjMarca)
        {
            MarcaBL ObjMarcaBL = new MarcaBL();
            List<MarcaBE> lstMarca = new List<MarcaBE>();
            lstMarca = ObjMarcaBL.ListarMarca(ObjMarca);
            return lstMarca;
        }

        [WebMethod]
        public static List<Item_MaBE> ListarItemsMa(Item_MaBE ObjItem_Ma)
        {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            List<Item_MaBE> lstItemMa = new List<Item_MaBE>();
            lstItemMa = ObjItemMaBL.ListarItem_Ma(ObjItem_Ma);
            return lstItemMa;
        }

        [WebMethod]
        public static List<UndMedidaBE> ListarUnidadMedidaCombo(UndMedidaBE ObjUndMedida)
        {
            UndMedidaBL objUndMedidaBL = new UndMedidaBL();
            List<UndMedidaBE> lstUndMedida = new List<UndMedidaBE>();
            lstUndMedida = objUndMedidaBL.ListarUnidadMedida(ObjUndMedida);
            return lstUndMedida;
        }

        //Crear Item_MA
        [WebMethod]
        public static int CrearItemMA(Item_MaBE ObjItem_Ma)
        {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            int id = 0;
            id = ObjItemMaBL.CrearItemMA(ObjItem_Ma);
            return id;
        }

        //Eliminar 
        [WebMethod]
        public static void EliminarItemMA(Item_MaBE ObjItem_Ma)
        {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            ObjItemMaBL.EliminarItemMA(ObjItem_Ma);
        }

        //MODIFICAR
        [WebMethod]
        public static void ModificarItemMA(Item_MaBE ObjItem_Ma)
        {
            Item_MaBL ObjItemMaBL = new Item_MaBL();
            ObjItemMaBL.ModificarItemMA(ObjItem_Ma);
        }

    }
}