﻿<%@ Page Title="" 
    Language="C#" MasterPageFile="~/SEC.Master" 
    AutoEventWireup="true" 
    CodeBehind="SolicitudAnticipo_Egreso2.aspx.cs"
    Inherits="SEC_WEB.Tesoreria.Solicitudes_Anticipo_Egreso.SolicitudAnticipoEgreso" %>

<%--<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="Proveedores.aspx.cs" 
    Inherits="SEC_WEB.Logistica.Maestros.Proveedores" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<%--    <link href="../../Scripts/Styles_SEC/SEC_Proveedor.css" rel="stylesheet" />--%>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript" src="../../Scripts/JS_SEC/Tesoreria/SolicitudAnticipo_Egreso/SEC_SolicitudAnticipo.js"></script>
 
     <div id="DVPrincipal"  style="margin-left:25px">
        <h3> Tesoreria - Bandeja de Solicitudes de Anticipo</h3>
        <div id="FiltrosBusqueda">
          <table id="TbFiltroBusq">
              <tr>
            <td> 
                <label for="txtNumeroCotizacion"> Numero Cotizacion:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td> 
                <input id="txtNumeroCotizacion" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtNumeroCotizacion" value="" />
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td> 
                <label for="txtMonedaId"> Moneda:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td> 
                <input id="txtMonedaId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtMonedaId" value="" />
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td> 
                <label for="txtFormaPagoId"> Forma de Pago:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td> 
                <input id="txtFormaPagoId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtFormaPagoId" value="" />
            </td>
             <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td> 
                <label for="txtTipoAnticipoId"> Forma de Pago:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </td>
            <td> 
                <input id="txtTipoAnticipoId" type="text" onkeypress="return validarnumeros(event)" class="form-control" name="txtTipoAnticipoId" value="" />
            </td>
              </tr>
          </table>
        </div>

<br/><br/><br/><br/>
     <!------------------------ JqGrid ------------------------->
        <div id="SolicitudAnticipoGrid">
        </div>
    <!--------------------------------------------------------> 


    </div>
   
</asp:Content>