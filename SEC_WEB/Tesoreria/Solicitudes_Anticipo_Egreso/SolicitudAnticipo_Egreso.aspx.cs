﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Tesoreria.Solicitudes_Anticipo_Egreso
{
    public partial class SolicitudAnticipo_Egreso1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           //this.hdUsuario.Value = "System";
           this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
           this.hdIdPerfil.Value = HttpContext.Current.Session[Constantes.SessionVariables.PerfilId].ToString();
        }
        /// ListarSolicitudAnticipo --> Lista Solicitudes de Anticipo en Bandeja
        [WebMethod]
        public static List<SolicitudAnticipoBE> ListarSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            SolicitudAnticipoBL objSolicitudAnticipoBL = new SolicitudAnticipoBL();
            List<SolicitudAnticipoBE> lstSolicitudAnticipoBE = new List<SolicitudAnticipoBE>();
            lstSolicitudAnticipoBE = objSolicitudAnticipoBL.ListarSolicitudAnticipo(objSolicitudAnticipo);
            return lstSolicitudAnticipoBE;
        }

        [WebMethod]
        public static List<ParametroBE> ListarComboParametro(ParametroBE ObjParametro)
        {
            ParametroBL objParametroBL = new ParametroBL();
            List<ParametroBE> lstParametro = new List<ParametroBE>();
            lstParametro = objParametroBL.ListarParametros(ObjParametro);
            return lstParametro;
        }

        [WebMethod]
        public static List<MonedaBE> ListarComboMoneda(MonedaBE ObjMoneda)
        {
            MonedaBL objMonedaBL = new MonedaBL();
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            lstMoneda = objMonedaBL.ListarMoneda(ObjMoneda);
            return lstMoneda;
        }

        [WebMethod]
        public static List<TipoMedioPagoBE> ListarComboTipoMedioPago(TipoMedioPagoBE ObjTipoMedioPago)
        {
            TipoMedioPagoBL objTipoMedioPagoBL = new TipoMedioPagoBL();
            List<TipoMedioPagoBE> lstTipoMedioPago = new List<TipoMedioPagoBE>();
            lstTipoMedioPago = objTipoMedioPagoBL.ListarTipoMedioPago(ObjTipoMedioPago);
            return lstTipoMedioPago;
        }

        [WebMethod]
        public static List<BancoBE> ListarComboBanco(BancoBE objBanco) {
            BancoBL objBancoBL = new BancoBL();
            List<BancoBE> lstBanco = new List<BancoBE>();
            lstBanco = objBancoBL.ListarBanco(objBanco);
            return lstBanco;
        }

        [WebMethod]
        public static int CrearEgreso(EgresoBE ObjEgreso) {
            EgresoBL ObjEgresoBL = new EgresoBL();
            int id = 0;
            id = ObjEgresoBL.CrearEgreso(ObjEgreso);
            return id;        
        }
        [WebMethod]
        public static List<EgresoBE> ListarEgreso(EgresoBE ObjEgreso) {
            EgresoBL ObjEgresoBL = new EgresoBL();
            List<EgresoBE> lstEgreso = new List<EgresoBE>();
            lstEgreso = ObjEgresoBL.ListarEgreso(ObjEgreso);
            return lstEgreso;  
        }

        [WebMethod]
        public static void ModificarEgreso(EgresoBE ObjEgreso)
        {
            EgresoBL ObjEgresoBL = new EgresoBL();
            ObjEgresoBL.ModificarEgreso(ObjEgreso);
        }

        [WebMethod]
        public static void ModificarEstadoAnticipo(SolicitudAnticipoBE objSolicitudAnticipo)
        {
            SolicitudAnticipoBL objSolicitudAnticipoBL = new SolicitudAnticipoBL();
            objSolicitudAnticipoBL.ModificarEstadoAnticipo(objSolicitudAnticipo);
        
        }
    }

}