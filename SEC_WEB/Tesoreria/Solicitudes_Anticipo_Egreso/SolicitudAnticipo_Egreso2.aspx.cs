﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;

namespace SEC_WEB.Tesoreria.Solicitudes_Anticipo_Egreso
{
    public partial class SolicitudAnticipo_Egreso : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // this.hdUsuario.Value = "System";
        }
        /// <summary>
        /// ListarSolicitudAnticipo --> Lista Solicitudes de Anticipo en Bandeja
        /// </summary>
        /// <param name="objSolicitudAnticipo"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<SolicitudAnticipoBE> ListarSolicitudAnticipo(SolicitudAnticipoBE objSolicitudAnticipo) {
            SolicitudAnticipoBL objSolicitudAnticipoBL = new SolicitudAnticipoBL();
            List<SolicitudAnticipoBE> lstSolicitudAnticipoBE = new List<SolicitudAnticipoBE>();
            lstSolicitudAnticipoBE = objSolicitudAnticipoBL.ListarSolicitudAnticipo(objSolicitudAnticipo);
            return lstSolicitudAnticipoBE;
                    
        }
    }
}