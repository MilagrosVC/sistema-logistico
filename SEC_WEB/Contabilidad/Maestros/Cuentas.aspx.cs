﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SEC_Utilitarios;
using System.Web.Services;
using Newtonsoft.Json;

using SEC_BE;
using SEC_BL;
namespace SEC_WEB.Contabilidad.Maestros
{
    public partial class Cuentas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //this.hdUsuario.Value = "System";
            this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();

            this.hdf_PlanCuentaID.Value = Request["PlanCuenta_Id"];
            this.hdf_PlanCuentaNombre.Value = Request["Pla_Nombre_Tx"];
            this.hdf_Emp_RazonSocial.Value = Request["Emp_RazonSocial_Tx"];
            this.hdf_Emp_RUC.Value = Request["Emp_DocIdent_RUC_nu"];
            this.hdf_FechaCreacionPC.Value = Request["PC_FechaCreacion"];
            this.hdf_HoraCreacionPC.Value = Request["Aud_Creacion_Fe"];
           // this.LNC.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }

        [WebMethod]
        public static List<ElementoBE> ListarComboElemento(ElementoBE ObjElemento)
        {
            ElementoBL objElementoBL= new ElementoBL();
            List<ElementoBE> lstElemento= new List<ElementoBE>();
            lstElemento = objElementoBL.ListarElemento(ObjElemento);
            return lstElemento;
        }

        [WebMethod]
        public static List<CuentaBE> ListarCuenta(CuentaBE objCuenta)
        {
            CuentaBL objCuentaBL = new CuentaBL();
            List<CuentaBE> lstCuenta = new List<CuentaBE>();
            lstCuenta = objCuentaBL.ListarCuenta(objCuenta);
            return lstCuenta;
        }

        [WebMethod]
        //public static int CrearCuenta(CuentaBE ObjCuenta) { 
        public static CuentaBE CrearCuenta(CuentaBE ObjCuenta)
        { 
            CuentaBL ObjCuentaBL=new CuentaBL();
            //int id =0;
            ObjCuenta = ObjCuentaBL.CrearCuenta(ObjCuenta);
            return ObjCuenta;
        }

        [WebMethod]
        public static int CrearCuentaTransfiere(CuentaBE ObjCuenta)
        {
            CuentaBL ObjCuentaBL = new CuentaBL();
            int id = 0;
            id = ObjCuentaBL.CrearCuentaTransfiere(ObjCuenta);
            return id;
        }

        [WebMethod]
        public static int VerificarCodigoCuenta(CuentaBE ObjCuenta) {
            CuentaBL ObjCuentaBL = new CuentaBL();
            int numerocodigo = 0;
            numerocodigo = ObjCuentaBL.VerificarCodigoCuenta(ObjCuenta);
            return numerocodigo;
        
        }
        [WebMethod]
        public static List<BancoBE> ListarComboBanco(BancoBE objBanco)
        {
            BancoBL objBancoBL = new BancoBL();
            List<BancoBE> lstBanco = new List<BancoBE>();
            lstBanco = objBancoBL.ListarBanco(objBanco);
            return lstBanco;
        }

        [WebMethod]
        public static List<MonedaBE> ListarComboMoneda(MonedaBE ObjMoneda)
        {
            MonedaBL objMonedaBL = new MonedaBL();
            List<MonedaBE> lstMoneda = new List<MonedaBE>();
            lstMoneda = objMonedaBL.ListarMoneda(ObjMoneda);
            return lstMoneda;
        }

        [WebMethod]
        public static List<TTipoCambioBE> ListarTTipoCambio(TTipoCambioBE ObjTTipoCambio) {
            TTipoCambioBL ObjTTipoCambioBL = new TTipoCambioBL();
            List<TTipoCambioBE> lstTTipoCambio = new List<TTipoCambioBE>();
            lstTTipoCambio = ObjTTipoCambioBL.ListarTTipoCambio(ObjTTipoCambio);
            return lstTTipoCambio;
        }
    }
}