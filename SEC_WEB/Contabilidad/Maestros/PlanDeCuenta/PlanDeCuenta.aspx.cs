﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;


namespace SEC_WEB.Contabilidad.Maestros.PlanDeCuenta
{
    public partial class PlanDeCuenta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }
        [WebMethod]
        public static List<PlanDeCuentaBE> ListarPlanDeCuenta(PlanDeCuentaBE objPlanDeCuenta)
        {
            PlanDeCuentaBL objPlanDeCuentaBL = new PlanDeCuentaBL();
            List<PlanDeCuentaBE> lstPlanDeCuentaBE = new List<PlanDeCuentaBE>();
            lstPlanDeCuentaBE = objPlanDeCuentaBL.ListarPlanDeCuenta(objPlanDeCuenta);
            return lstPlanDeCuentaBE;
        }
    }
}