﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="PlanDeCuenta.aspx.cs" Inherits="SEC_WEB.Contabilidad.Maestros.PlanDeCuenta.PlanDeCuenta" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <h1>Bienvendio</h1>--%>
<%--<script type="text/javascript" src="../Scripts/JS_SEC/Contabilidad/Maestros/PlanDeCuenta/SEC_PlanDeCuenta.js"></script>--%>
    <script type="text/javascript" src="../../../Scripts/JS_SEC/Contabilidad/Maestros/PlanDeCuenta/SEC_PlanDeCuenta.js"></script>
<%--
<script type="text/javascript" src="../../Scripts/JS_SEC/Logistica/Maestros/Proveedor/SEC_Proveedor.js"></script>--%>
    <asp:HiddenField ID="hdUsuario" runat="server" />

    <div id="DVPrincipal" style="margin-left:25px">
        <h3> CONTABILIDAD - <strong>Plan de Cuenta</strong></h3>
        <div id="DVFiltrosBusqueda">
            <table id="TbFiltroBusq">
                <tr>
                    <td> <label for="ddlRazSocial"> Razon Social:&nbsp;&nbsp;</label> </td> <%--IdEmpresa--%>
                    <td>   <select id="ddlRazSocial" name="ddlRazSocial" class="form-control" style="height:30px; width:150px;">
                           </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td> <label for="txtPla_Nombre_Tx"> Nombre del Plan:&nbsp;&nbsp;</label> </td>
                    <td> <input id="txtPla_Nombre_Tx" type="text" class="form-control" name="txtPla_Nombre_Tx" value="" /></td>
                </tr>
            </table>
            <br/>
                    <input type="button" id="btnPrueba" class="btn btn-sm" name="name" value="Prueba" />

          <!------------------------ JqGrid ------------------------->
            <div id="PlanCuentaGrid">
            </div>
          <!--------------------------------------------------------->
        </div>

    </div>
<%--       <script type="text/javascript" src="../../Scripts/JS_SEC/General/Funciones.js"></script>--%>


</asp:Content>
