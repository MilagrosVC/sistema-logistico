﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using Newtonsoft.Json;
using SEC_BE;
using SEC_BL;
using SEC_Utilitarios;

namespace SEC_WEB.Contabilidad.Maestros
{
    public partial class PlanDeCuenta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           //this.hdUsuario.Value = "System";
           this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();

            // this.hdUsuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }
        [WebMethod]
        public static List<EmpresaBE> ListarComboEmpresa(EmpresaBE ObjEmpresa)
        {
            EmpresaBL objEmpresaBL = new EmpresaBL();
            List<EmpresaBE> lstEmpresa = new List<EmpresaBE>();
            lstEmpresa = objEmpresaBL.ListarComboEmpresa(ObjEmpresa);
            return lstEmpresa;
        }

        [WebMethod]
        public static List<PlanDeCuentaBE> ListarPlanDeCuenta(PlanDeCuentaBE objPlanDeCuenta)
        {
            PlanDeCuentaBL objPlanDeCuentaBL = new PlanDeCuentaBL();
            List<PlanDeCuentaBE> lstPlanDeCuentaBE = new List<PlanDeCuentaBE>();
            lstPlanDeCuentaBE = objPlanDeCuentaBL.ListarPlanDeCuenta(objPlanDeCuenta);
            return lstPlanDeCuentaBE;
        }

        
    }
}