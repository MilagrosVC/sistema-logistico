﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using SEC_DA;
using SEC_BL;
using SEC_BE;

namespace SEC_WEB
{
    public partial class frmProyectos : System.Web.UI.Page
    {
        EmpresaBL empreBL = new EmpresaBL();
        EmpresaDA empreDA = new EmpresaDA();
        EmpresaBE objEmpresa = new EmpresaBE();

        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = empreBL.ListarEmpresa(objEmpresa);
            GridView1.DataBind();
        }


        //[WebMethod]
        //public static string CargarDatos(EmpresaBE objEmpresa)
        //{
        //    EmpresaBL empreBL = new EmpresaBL();
        //    EmpresaDA empreDA = new EmpresaDA();
        //    List<EmpresaBE> objListaEmpresa = new List<EmpresaBE>();


        //    objListaEmpresa = empreBL.ListarEmpresa(objEmpresa);

        //    return objListaEmpresa.ToString();
        //}
    }
}