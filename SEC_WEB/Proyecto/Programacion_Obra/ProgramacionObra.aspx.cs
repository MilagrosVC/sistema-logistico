﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using SEC_DA;
using SEC_BE;
using SEC_BL;

namespace SEC_WEB.Proyecto.Programacion_Obra
{
    public partial class ProgramacionObra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static List<ValorizacionCaBE> ObtenerValorizacionPartida(string partida)
        {
            ValorizacionCaBL objValCaBL = new ValorizacionCaBL();
            List<ValorizacionCaBE> lstValCaBE = new List<ValorizacionCaBE>();

            lstValCaBE = objValCaBL.ObtenerValorizacionPartida(JsonConvert.DeserializeObject<ValorizacionCaBE>(partida));

            return lstValCaBE;
        }

        [WebMethod]
        public static List<ValorizacionDeBE> ListarItemsValorizacion(string idValorizacionCa)
        {
            ValorizacionDeBL objValDeBL = new ValorizacionDeBL();
            List<ValorizacionDeBE> lstValDeBE = new List<ValorizacionDeBE>();

            lstValDeBE = objValDeBL.ListarItemsValorizacion(Convert.ToInt32(idValorizacionCa));

            return lstValDeBE;
        }
    }

}