﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SEC.Master" AutoEventWireup="true" CodeBehind="ProgramacionObra.aspx.cs" Inherits="SEC_WEB.Proyecto.Programacion_Obra.ProgramacionObra" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #ContProgObra {
            /*width:900px;*/
            padding:0 10px 0 10px;
            margin:auto;
        }
        #dvValorizacionCa {
            display:block;
            width:40%;
            font-size:11px;
        }
        #tbValorizacion{
            border-bottom:1px groove;
            border-right:1px groove;
            border-color:#fefdfd;
            display:inline-block;
            width:57.7%;
            height:400px;
            font-size:10px;
            /*overflow-y: scroll;*/
            overflow: hidden;
            /*overflow-x: scroll;*/
            /*max-width:*/

            display:none;
        }
        #tbProgramacion{
            /*border-bottom:1px groove;
            border-right:1px groove;*/
            border:1px groove;
            border-color:#fefdfd;
            /*display:inline-block;*/
            width:41.8%;
            overflow-x: scroll;
            height:400px;
            font-size:10px;
            float:right;

            display:none;
        }
        /*---------------------------*/
        #tbDinamica1,#tbDinamica1 th,#tbDinamica1 tr{
            border: 1px groove black;
            margin: 10px;
        }
        #tbDinamica1 th {
            font-size: 10px;
            background-color:#b9afaf;
            color:black;
            text-align: center;
        }
        #tbDinamica1 td {
            font-size: 10px;
            color:black;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../Scripts/JS_SEC/Proyecto/Programacion_Obra/ProgramacionObra.js"></script>
    <div id="ContProgObra">
        <div id="dvValorizacionCa">
            <h3>CALENDARIO VALORIZADO</h3>
            <table style=""> 
                <tr>
                    <td style ="width:70px;"><label>OBRA:</label></td>
                    <td><label id="lblObra"></label></td>
                </tr>
                <tr>
                    <td><label>CLIENTE:</label></td>
                    <td><label id="lblCliente"></label></td>
                </tr>
                <tr>
                    <td><label>EJECUTA:</label></td>
                    <td><label id="lblEjecuta"></label></td>
                </tr>
                <tr>
                    <td><label>PLAZO:</label></td>
                    <td><label id="lblPlazo"></label></td>
                </tr>
                <tr>
                    <td><label>FECHA:</label></td>
                    <td><label id="lblFecha"></label></td>
                </tr>
                
            </table>
            <br />
            <table style="border:1px">
                <tr><td style="max-width:50px;" ><div contenteditable>I'm editable</div></td><td><div contenteditable>I'm also editable</div></td></tr>
                <tr><td>I'm not editable</td></tr>
            <table>
        </div>
        <br/> 

            <div id="tbValorizacion">
            </div>
        <div id="tbProgramacion">
            
        </div>
    </div>

    <p>----------------------------------------------------------------------------------------------------------------</p>
    <table id="tbDinamica1" style="width:50%">
        <tr>
            <th>ITEM</th>
            <th>DESCRIPCION</th>
            <th>UND</th>
            <th>METRADO</th>
            <th>P. UNITARIO</th>
            <th>MONTO PARCIAL</th>
        </tr>
    </table>


</asp:Content>
