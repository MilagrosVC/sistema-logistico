﻿$(document).ready(function () {
    //alert("hola");
    //ListarPresupuestos();
    ListarPresupuesto2();
    //Prueba();
});

function Prueba() {

    var texto = $("#ContentPlaceHolder1_hdf_Proyecto_Id").val();
    //var json = JSON.stringify({ "ProyectoId": texto });

    $.ajax({
        type: "POST",
        url: "../../Logistica/Compras/SolicitudCompraPresupuesto.aspx/GetJewellerAssets",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ jewellerId: texto }),
        dataType: "json",
        success: 'AjaxSucceeded',
        error: 'AjaxFailed'
    });
}





function ListarPresupuesto2() {

    $("#jQGridDemo").jqGrid({
        datatype: 'json',
        colNames: ['Codigo', 'Proyecto_Id', 'Nombre Proyecto', 'Estado Presupuesto Id', 'Estado', 'Version', 'Descripcion', 'Vigente', 'Estado', ''],
        colModel: [
           { name: 'Presupuesto_Id', index: 'Presupuesto_Id', width: 75, align: 'center' },
           { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 75, align: 'center' },
           { name: 'Pry_Nombre_Tx', index: 'Pry_Nombre_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Estado_Id', index: 'Pre_Estado_Id', width: 150, align: 'center', sortable: false, editable: true, hidden: true },
           { name: 'Estado_Tx', index: 'Estado_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Version_Nu', index: 'Pre_Version_Nu', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Descripcion_Tx', index: 'Pre_Descripcion_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Vigente_Tx', index: 'Vigente_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Presupuesto', index: 'Presupuesto', width: 30, align: 'center', formatter: 'actionFormatterPresupuesto', search: false }
        ],
        height: 'auto',
        pager: "#jqGridPager",
        sortname: 'Presupuesto_Id',
        sortorder: 'asc',
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        emptyrecords: "No hay Registros.",
        caption: 'Lista Presupuestos'
    });
    $.extend($.fn.fmatter, {
        actionFormatterPresupuesto: function (cellvalue, options, rowObject) {
            return "<img title=\"Click ir al presupuesto\" onclick=\"IrPresupuesto(this)\" style=\"cursor:pointer\" src='/Scripts/JQuery-ui-1.11.4/images/Window-Enter-icon.png' />";
        }
    });



    var texto = $("#ContentPlaceHolder1_hdf_Proyecto_Id").val();

    var params = new Object();
    params.IdProyecto = texto;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompraPresupuesto.aspx/ListarPresupuestos',
        data: JSON.stringify(params),
        dataType: "json",
        async: false,
        success: function (data, textStatus) {

            if (textStatus == "success") {

                //$("#tbDetailsOrder").clearGridData();

                var grid = $("#jQGridDemo")[0];
                grid.addJSONData(jQuery.parseJSON(data.d));

            }

        },
        error: function (request, status, error) {
            alert(jQuery.parseJSON(request.responseText).Message);
        }
    });
}



function ListarPresupuesto() {

    var texto = $("#ContentPlaceHolder1_hdf_Proyecto_Id").val();

    //var json = JSON.stringify({ "ProyectoId": texto });

    jQuery("#jQGridDemo").jqGrid({
        url: '../../Logistica/Compras/SolicitudCompraPresupuesto.aspx/ListarPresupuestos',
        dataType: "json",
        mtype: "POST",        
        //data: JSON.stringify({ IdProyecto: texto }),
        serializeGridData: function (postData) {
            postData.IdProyecto = texto
            return JSON.stringify(postData);
        },
        //contentType: "application/json; charset=utf-8",
        
        ajaxGridOptions: { contentType: "application/json; charset=utf-8" },
        loadonce: true,
        colNames: ['Codigo', 'Proyecto_Id', 'Nombre Proyecto', 'Estado Presupuesto', 'Version', 'Descripcion', 'Estado',''],
        colModel: [
           { name: 'Presupuesto_Id', index: 'Presupuesto_Id', width: 75, align: 'center' },
           { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 75, align: 'center' },
           { name: 'Pry_Nombre_Tx', index: 'Pry_Nombre_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Estado_Id', index: 'Pre_Estado_Id', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Version_Nu', index: 'Pre_Version_Nu', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Descripcion_Tx', index: 'Pre_Descripcion_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Presupuesto', index: 'Presupuesto', width: 30, align: 'center', formatter: 'actionFormatterPresupuesto', search: false }
        ],
        pager: "#jqGridPager",
        sortname: 'Presupuesto_Id',
        sortorder: 'asc',
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "Presupuesto_Id"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Lista Presupuestos',
        loadComplete: function (result) {
            //alert("ASDASD");
        },
        success: function (data, textStatus) {

            if (textStatus == "success") {

                //$("#tbDetailsOrder").clearGridData();

                var grid = $("#jQGridDemo")[0];
                grid.addJSONData(jQuery.parseJSON(data.d));

            }

        },
        error: function (request, status, error) {
            alert(jQuery.parseJSON(request.responseText).Message);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterPresupuesto: function (cellvalue, options, rowObject) {
            return "<img title=\"Click ir al presupuesto\" onclick=\"IrPresupuesto(this)\" style=\"cursor:pointer\" src='/Scripts/JQuery-ui-1.11.4/images/Window-Enter-icon.png' />";
        }
    });

}

/*

jQuery(document).ready(function () {
    jQuery("#jQGridDemo").jqGrid({
        url: '../../Logistica/Compras/SolicitudCompraPresupuesto.aspx/ListarPresupuestos',
        datatype: 'Json',
        mtype: 'POST',
        colNames: ['Id', 'Codigo', 'Producto', 'Cantidad', 'Importe'],
        colModel: [
           { name: 'Presupuesto_Id', index: 'Presupuesto_Id', width: 75, align: 'center' },
           { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 75, align: 'center' },
           { name: 'Pry_Nombre_Tx', index: 'Pry_Nombre_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Estado_Id', index: 'Pre_Estado_Id', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Version_Nu', index: 'Pre_Version_Nu', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pre_Descripcion_Tx', index: 'Pre_Descripcion_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 150, align: 'center', sortable: false, editable: true }],
        pager: jQuery('#jqGridPager'),
        rowNum: 20,
        rowList: [10, 20, 50, 100],
        sortname: 'Presupuesto_Id',
        sortorder: 'asc',
        viewrecords: true,
        imgpath: '/content/sunny/images',
        caption: 'Productos',
        height: 'auto',
        width: 900,
        autowidth: true,

    });
});*/



function IrPresupuesto(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#jQGridDemo").getRowData(RowId);

    var idproyecto = rowData.Presupuesto_Id;
    var idpresupuesto = rowData.Presupuesto_Id;
    window.location.href = "../Compras/frmPresupuestoCompra.?Proyecto_Id=" + idproyecto;
}