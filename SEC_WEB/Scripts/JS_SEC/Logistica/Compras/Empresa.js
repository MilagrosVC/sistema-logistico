﻿$(document).ready(function () {
    alert("hola");
    ListarProveedor();
});


function ListarProveedor() {
    //var proveedor = {};
    jQuery("#jQGridDemo").jqGrid({
        url: '../../Logistica/Compras/frmPresupuestoCompra.aspx/ListarProveedor',
        datatype: "json",
        mtype: "POST",
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Codigo', 'Razon Social', 'Persona Contacto', 'Responsable', 'Nro Doc'],
        colModel: [
           { name: 'Empresa_Id', index: 'Empresa_Id', width: 75, align: 'center', key: true },
           { name: 'Emp_RazonSocial_Tx', index: 'Emp_RazonSocial_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Emp_Personacontacto_Tx', index: 'Emp_Personacontacto_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Emp_Nombre_Responsable_Tx', index: 'Emp_Nombre_Responsable_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Emp_DocIdent_Responsable_Nu', index: 'Emp_DocIdent_Responsable_Nu', width: 150, align: 'center', sortable: false, editable: true }
        ],
        pager: "#jqGridPager",
        sortname: 'Empresa_Id',
        sortorder: 'asc',
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "Empresa_Id"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Lista Empresas',
        loadComplete: function (result) {
            //alert("ASDASD");
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
}