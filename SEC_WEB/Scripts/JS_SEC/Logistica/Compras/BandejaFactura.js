﻿$(document).ready(function () {
    IniciarControles();
});

function IniciarControles(){

    $("#txtCodFactura").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 45 || e.which > 45)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });

    $('#txtHasta').attr('disabled', 'disabled');
    $('#txtDesde').change(function () {
        var month = ($("#txtDesde").datepicker('getDate').getMonth() + 1);
        $("#txtHasta").datepicker({
            showOn: "button",
            minDate: '-1m',
            buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
            buttonImageOnly: true,
            buttonText: "Seleccione la fecha",
            dateFormat: "dd/mm/yy",
            defaultDate: "+1w",
            onClose: function (selectedDate) {
                $("#txtDesde").datepicker("option", "maxDate", selectedDate)
            }
        });
        $("#txtHasta").prop('disabled', false);
    });
    ListarFacturas();

    $('#btnCrear').click(function () {
        window.location = '../../Logistica/Compras/Factura.aspx';
    });

    $('#btnBuscar').click(function () {
        ListarFacturas();
    });

    $('#btnLimpiar').click(function () {
        LimpiarFiltros();
    });

    $("#btnImprimirCom").click(function () {
        $("#divImprimirCom").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: BASE_URL + "/Scripts/Styles_SEC/SEC_Factura.css"
        });
    });

    $("#btnCerrarVerCom").click(function () {
        $("#dvVerComprobante").dialog('close');
    });

    $("#btnExport").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;
        var a = document.createElement('a');

        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divImprimirCom');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'Comprobante_' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
}

function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                    }
            }

    return ret;
}

function ListarFacturas() {

    var ObjComprobanteCa = {
        Comprobante_Co: $('#txtCodFactura').val(),
        //Requerimiento_Co: $('#txtNumeroRq').val(),
        FechaDesde: Controlar_Valores("txtDesde", "datetime"),
        FechaHasta: Controlar_Valores("txtHasta", "datetime")
    };

    $("#divFacturas").html('<table id="gvFacturas"></table><div id="pieFacturas"></div>');
    $("#gvFacturas").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/BandejaFactura.aspx/ListarFacturas',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjComprobanteCa:" + JSON.stringify(ObjComprobanteCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', '', 'Número', '', 'Orden_Compra_Id', 'Proveedor', 'Comprobante_Pago_Id', 'N° Guia', 'Estado', 'Fecha Creacion', 'Fecha Emision', 'Total', 'Moneda'],
        colModel: [
           { name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
           { name: 'Generar', index: 'Generar', width: 30, align: 'center', formatter: 'actionFormatterGenerar', search: false },
           { name: 'Comprobante_Co', index: 'Comprobante_Co', width: 75, align: 'right' },
           { name: 'Comprobante_Id', index: 'Comprobante_Id', width: 75, align: 'center', hidden: true },
           { name: 'Orden_Compra_Id', index: 'Orden_Compra_Id', width: 75, align: 'center', hidden: true },
           { name: 'Prv_RazonSocial_Tx', index: 'Prv_RazonSocial_Tx', width: 250, align: 'center' },
           { name: 'Comprobante_Pago_Id', index: 'Comprobante_Pago_Id', width: 100, align: 'center', hidden: true },
           { name: 'Com_Guia_Remision_Nu', index: 'Com_Guia_Remision_Nu', width: 75, align: 'right' },
           { name: 'Est_Nombre_Tx', index: 'Est_Nombre_Tx', width: 100, align: 'center' },
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' }, hidden:true },
           { name: 'Com_Emision_Fe', index: 'Com_Emision_Fe', width: 90, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Com_Total_Nu', index: 'Com_Total_Nu', width: 90, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 100, align: 'center' }           
        ],
        pager: "#pieFacturas",
        viewrecords: true,
        autowidth: true,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Facturas',
        loadComplete: function (result) {

        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
            return "<img title=\"Visualizar Factura\" onclick=\"VisualizarFactura(" + rowObject.Comprobante_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        },
        actionFormatterGenerar: function (cellvalue, options, rowObject) {
            return "<img title=\"Generar Excel\" onclick=\"GenerarExcel(" + rowObject.Comprobante_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/ExpExcel01_16.png\" />";
        }
    });
}

function VisualizarFactura(idComprobante) {
    var igv = "", subtotal = "";
    var total = "";
    comprobante = {
        Comprobante_Id: idComprobante
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaFactura.aspx/GenerarVistaComprobanteCa',
        dataType: "json",
        data: "{ObjComprobanteCa:" + JSON.stringify(comprobante) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblCom-CodComprobante").html("N°: " + data.d.Comprobante_Co);
                var FechaEmision = ConvertFechaFormatoNormal(data.d.Com_Emision_Fe);
                $("#lblCom-Fecha").html(FechaEmision);
                $("#lblCom-RazonSocial").html(data.d.Prv_RazonSocial_Tx);
                $("#lblCom-Direccion").html(data.d.Prv_Direccion_tx);                    
                $("#lblCom-RUC").html(data.d.Prv_DocIdent_nu);
                $("#lblCom-Guia").html(data.d.Com_Guia_Remision_Nu);
                igv = data.d.Com_IGV_Nu;
                subtotal = data.d.Com_SubTotal_Nu;
                total = data.d.Com_Total_Nu;


                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaFactura.aspx/GenerarVistaComprobanteDe',
                    dataType: "json",
                    data: "{ObjComprobanteDe:" + JSON.stringify(comprobante) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].ComDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].ComDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].ComDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbCom-Detalle").append(fila);
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";
                                $("#tbCom-Detalle").append(fila);
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].ComDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].ComDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en la Orden de Compra");
                        }
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de la Orden de Compra");
            }
        }
    });

    $("#dvVerComprobante").dialog({
        autoOpen: true,
        show: "blind",
        width: 830,
        resizable: false,
        modal: true,
        title: "FACTURA",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        },
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbCom-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:50px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:300px;border: 1px solid #000;'>ARTICULO</th><th style='width:150px;border: 1px solid #000;'>PRECIO UND</th><th style='width:150px;border: 1px solid #000;'>TOTAL</th></tr>");
            $("#tbCom-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");
        }
    });
}

function GenerarExcel(idComprobante) {

    var igv = "", subtotal = "";
    var total = "";
    comprobante = {
        Comprobante_Id: idComprobante
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaFactura.aspx/GenerarVistaComprobanteCa',
        dataType: "json",
        data: "{ObjComprobanteCa:" + JSON.stringify(comprobante) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblCom-CodComprobante").html("N°: " + data.d.Comprobante_Co);
                var FechaEmision = ConvertFechaFormatoNormal(data.d.Com_Emision_Fe);
                $("#lblCom-Fecha").html(FechaEmision);
                $("#lblCom-RazonSocial").html(data.d.Prv_RazonSocial_Tx);
                $("#lblCom-Direccion").html(data.d.Prv_Direccion_tx);
                $("#lblCom-RUC").html(data.d.Prv_DocIdent_nu);
                $("#lblCom-Guia").html(data.d.Com_Guia_Remision_Nu);
                igv = data.d.Com_IGV_Nu;
                subtotal = data.d.Com_SubTotal_Nu;
                total = data.d.Com_Total_Nu;

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaFactura.aspx/GenerarVistaComprobanteDe',
                    dataType: "json",
                    data: "{ObjComprobanteDe:" + JSON.stringify(comprobante) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].ComDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].ComDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].ComDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbCom-Detalle").append(fila);
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";
                                $("#tbCom-Detalle").append(fila);
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].ComDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].ComDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].ComDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en la Orden de Compra");
                        }
                        var dt = new Date();
                        var day = dt.getDate();
                        var month = dt.getMonth() + 1;
                        var year = dt.getFullYear();
                        var hour = dt.getHours();
                        var mins = dt.getMinutes();
                        var segs = dt.getSeconds();

                        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;

                        var a = document.createElement('a');

                        var data_type = 'data:application/vnd.ms-excel';
                        var table_div = document.getElementById('divImprimirCom');
                        var table_html = table_div.outerHTML.replace(/ /g, '%20');
                        a.href = data_type + ', ' + table_html;
                        //Definiendo nombre del archivo
                        a.download = 'Comprobante_' + dateNom + '.xls';

                        a.click();
                        e.preventDefault();
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de la Orden de Compra");
            }
        }
    });    
}

function LimpiarFiltros() {

    $("#txtDesde").datepicker("destroy");
    $('#txtDesde').val('');
    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "yy/mm/dd",
        defaultDate: "+1w",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });
    $('#txtHasta').attr('disabled', 'disabled');
    $("#txtHasta").datepicker("destroy");
    $('#txtHasta').val('');
    $('#txtCodFactura').val('');
    $('#txtCodFactura').focus();
    ListarFacturas();
}



