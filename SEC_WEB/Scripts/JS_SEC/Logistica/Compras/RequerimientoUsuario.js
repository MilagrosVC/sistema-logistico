﻿frstChrg_BandejaRQ = true;
//Formato de deciamles JQGrid
//formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 }
//Formato de fecha
//formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } 

$(document).ready(function () {
    IniciarControles();
});

function IniciarControles() {

    
    $("#btnGenerar").click(function () {
        window.location = BASE_URL + '/Logistica/Compras/Requerimiento.aspx';
    });

    $("#btnLimpiar").click(function () {
        LimpiarFiltros();
        ListarRequerimientosPorUsuario('asc');
    });


    $("#btnBuscar").click(function () {
        ListarRequerimientosPorUsuario('asc');
    });

    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });

    $('#txtHasta').attr('disabled', 'disabled');
    $('#txtDesde').change(function () {
        var month = ($("#txtDesde").datepicker('getDate').getMonth() + 1);
        $("#txtHasta").datepicker({
            showOn: "button",
            minDate: '-1m',
            buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
            buttonImageOnly: true,
            buttonText: "Seleccione la fecha",
            dateFormat: "dd/mm/yy",
            defaultDate: "+1w",
            onClose: function (selectedDate) {
                $("#txtDesde").datepicker("option", "maxDate", selectedDate)
            }
        });
        $("#txtHasta").prop('disabled', false);
    });

    ListarRequerimientosPorUsuario('asc');

    $("#btnImprimirRQ").click(function () {
        $("#divImprimirRQ").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: "../../SEC/Scripts/Styles_SEC/SEC_BandRQ_ImprRQ.css"
        });
    });

    $("#btnCerrarVerRQ").click(function () {
        $("#dvVerRequerimiento").dialog('close');
    });

    $("#btnCerrarExcel").click(function () {
        $("#divExceldialog").dialog('close');
    });

    $("#btnExport").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;

        var a = document.createElement('a');

        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divExcel');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'Requerimiento_' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
}

function ListarRequerimientosPorUsuario(sort) {

    var RqsAprb = 0;
    var RqsDesprb = 0;
    var RqsNuevos = 0;
    var totalRqs;

    var estado = ($('#ddlEstadoRQ').val() != null) ? $('#ddlEstadoRQ').val() : 0;

    var ObjRequerimientoCa = {
        Solicitante_Id: $('#ContentPlaceHolder1_hdf_Usuario').val(),
        Requerimiento_Co: $('#txtNumeroRq').val(),
        Req_Estado_Id: estado,
        FechaDesde: Controlar_Valores("txtDesde", "datetime"),
        FechaHasta: Controlar_Valores("txtHasta", "datetime")
    };

    $("#divRequerimientoUsuario").html('<table id="gvRequerimientoUsuario"></table><div id="pieRequerimientoUsuario"></div>');
    $("#gvRequerimientoUsuario").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/RequerimientoUsuario.aspx/ListarRequerimiento',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjRequerimientoCa:" + JSON.stringify(ObjRequerimientoCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['','', '','Codigo','', 'Partida_Id', 'Especialidad', 'Solicitante', 'Centro_Costo_Id', 'Fecha Creacion', 'Fecha Entrega', 'Req_Estado_Id', 'Estado', 'Req_Tipo_Id', 'Tipo', 'Proyecto_Id', 'Turno_Id', 'Turno'],
        colModel: [
           { name: 'Editar', index: 'Editar', width: 30, align: 'center', formatter: 'actionFormatterEditar', search: false },
           { name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
           { name: 'Generar', index: 'Generar', width: 30, align: 'center', formatter: 'actionFormatterGenerar', search: false },
           { name: 'Requerimiento_Co', index: 'Requerimiento_Co', width: 75, align: 'center' },
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 75, align: 'center', hidden: true, key:true },
           { name: 'Partida_Id', index: 'Partida_Id', width: 75, align: 'center', hidden: true },
           { name: 'Prt_Nombre_Tx', index: 'Prt_Nombre_Tx', width: 150, align: 'center' },
           { name: 'Solicitante_Id', index: 'Solicitante_Id', width: 150, align: 'center' },
           { name: 'Centro_Costo_Id', index: 'Centro_Costo_Id', width: 150, align: 'center', hidden: true },           
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Req_Entrega_Fe', index: 'Req_Entrega_Fe', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Req_Estado_Id', index: 'Req_Estado_Id', width: 150, align: 'center', hidden: true },
           { name: 'Est_Nombre_Tx', index: 'Est_Nombre_Tx', width: 150, align: 'center' },
           { name: 'Req_Tipo_Id', index: 'Req_Tipo_Id', width: 150, align: 'center', hidden: true },
           { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 150, align: 'center' },
           { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 150, hidden: true },
           { name: 'Turno_Id', index: 'Turno_Id', width: 150, align: 'center', hidden: true },
           { name: 'Tur_Nombre_Tx', index: 'Tur_Nombre_Tx', width: 150, align: 'center' }           
        ],
        pager: "#pieRequerimientoUsuario",
        viewrecords: true,
        autowidth: true,
        sortname: 'Requerimiento_Co',
        sortorder: sort,
        sortable: true,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { totalRqs = obj.d.length; return totalRqs; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Requerimientos',
        loadComplete: function (data) {
            var ids = $('#gvRequerimientoUsuario').getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var rowId = ids[i];
                var rowData = $('#gvRequerimientoUsuario').jqGrid('getRowData', rowId);
                if (rowData.Req_Estado_Id == 10) {
                    $('#' + (rowId)).find('td').css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });
                }
            }
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            if (rowObject.Est_Nombre_Tx != 'PARCIAL') {
                return "<i title=\"Sin Acciones\" class='fa fa-ban'></i>"; //onclick=\"MensajeBan(this)\" style=\"cursor:pointer;color:#222;\"
            }
            return "<i title=\"Editar Requerimiento\" onclick=\"EditarRequerimiento(" + rowObject.Requerimiento_Id + ")\" style=\"cursor:pointer;color:#222;\" class='fa fa-pencil-square-o'></i>";
        },
        actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
            return "<img title=\"Visualizar Requerimiento\" onclick=\"VisualizarRequerimiento(" + rowObject.Requerimiento_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        },
        actionFormatterGenerar: function (cellvalue, options, rowObject) {
            return "<img title=\"Generar Excel\" onclick=\"GenerarExcel(" + rowObject.Requerimiento_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/ExpExcel01_16.png\" />";
        }
    });    
}

function LimpiarFiltros() {

    $('#txtNumeroRq').val('');
    $('#ddlEstadoRQ').val(0);
    $("#txtDesde").datepicker("destroy");
    $('#txtDesde').val('');
    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy", 
        defaultDate: "+1w",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });
    $('#txtHasta').attr('disabled', 'disabled');
    $("#txtHasta").datepicker("destroy");
    $('#txtHasta').val('');
}

function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                    }
            }
    return ret;
}

function EditarRequerimiento(idRequerimiento) {

    window.location.href = '../../Logistica/Compras/Requerimiento.aspx?Requerimiento_Id=' + idRequerimiento;
}

function VisualizarRequerimiento(idRequerimiento) {

    requerimiento = {
        Requerimiento_Id: idRequerimiento
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaRequerimiento.aspx/GenerarVistaRequerimiento',
        dataType: "json",
        data: "{objRequerimientoCa:" + JSON.stringify(requerimiento) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblRq-CodRequerimiento").html("N°: " + data.d.Requerimiento_Co);
                $("#lblRq-Solicitante").html(data.d.Req_Solicitante);
                var FechaCreacion = ConvertFechaFormatoNormal(data.d.Aud_Creacion_Fe);
                $("#lblRq-FechaCreacion").html(FechaCreacion);
                var FechaEntrega = ConvertFechaFormatoNormal(data.d.Req_Entrega_Fe);
                $("#lblRq-FechaEntrega").html(FechaEntrega);
                $("#lblRq-NomProyecto").html(data.d.Req_Proyecto_Tx);
                $("#lblRq-Direccion").html(data.d.Req_ProyDireccion_Tx);
                $("#lblRq-Partida").html(data.d.Req_Partida_Tx);
                $("#lblRq-Turno").html(data.d.Req_Turno_Tx);
                $("#lstRq-Subpartidas").html(data.d.Lst_Partidas_Rq);

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaRequerimiento.aspx/ListarItemsxRequerimiento',
                    dataType: "json",
                    data: "{objRequerimientoDe:" + JSON.stringify(requerimiento) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].Itm_Co + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbRq-Detalle").append(fila);
                                }
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    var fila = "<tr><td>&nbsp</td><td></td><td></td><td></td><td></td><td></td></tr>";
                                    $("#tbRq-Detalle").append(fila);
                                }
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].Itm_Co + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbRq-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en el requerimiento");
                        }
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de Requerimiento");
            }
        }
    });

    $("#dvVerRequerimiento").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "REQUERIMIENTO",
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        },
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbRq-Detalle").html("<tr><th style='width:50px'>ITEM</th><th style='width:300px'>DESCRIPCION</th><th style='width:70px'>CANTIDAD</th><th style='width:60px'>UND</th><th style='width:80px'>Codigo</th><th style='width:196px'>OBSERVACION</th></tr>");
            $("#tbRq-Detalle").append("<tr><td>&nbsp</td><td></td><td></td><td></td><td></td><td></td></tr>");
        }
    });
}

function GenerarExcel(idRequerimiento) {

    requerimiento = {
        Requerimiento_Id: idRequerimiento
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaRequerimiento.aspx/GenerarVistaRequerimiento',
        dataType: "json",
        data: "{objRequerimientoCa:" + JSON.stringify(requerimiento) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblExcelRq-CodRequerimiento").html("N°: " + data.d.Requerimiento_Co);
                $("#lblExcelRq-Solicitante").html(data.d.Req_Solicitante);
                var FechaCreacion = ConvertFechaFormatoNormal(data.d.Aud_Creacion_Fe);
                $("#lblExcelRq-FechaCreacion").html(FechaCreacion);
                var FechaEntrega = ConvertFechaFormatoNormal(data.d.Req_Entrega_Fe);
                $("#lblExcelRq-FechaEntrega").html(FechaEntrega);

                $("#lblExcelRq-NomProyecto").html(data.d.Req_Proyecto_Tx);
                $("#lblExcelRq-Direccion").html(data.d.Req_ProyDireccion_Tx);
                $("#lblExcelRq-Partida").html(data.d.Req_Partida_Tx);
                $("#lblExcelRq-Turno").html(data.d.Req_Turno_Tx);
                $("#lstExcelRq-Subpartidas").html(data.d.Lst_Partidas_Rq);

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaRequerimiento.aspx/ListarItemsxRequerimiento',
                    dataType: "json",
                    data: "{objRequerimientoDe:" + JSON.stringify(requerimiento) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;border: 1px solid #000;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Itm_Co + "</td>";
                                    fila = fila + "<td style='text-align:left;border: 1px solid #000;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbExcelRq-Detalle").append(fila);
                                }
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    var fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbExcelRq-Detalle").append(fila);
                                }
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].Itm_Co + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbExcelRq-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en el requerimiento");
                        }
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de Requerimiento");
            }
        }
    });

    $("#divExceldialog").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "EXCEL REQUERIMIENTO",
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbExcelRq-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:300px;border: 1px solid #000;'>DESCRIPCION</th><th style='width:70px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:80px;border: 1px solid #000;'>Codigo</th><th style='width:196px;border: 1px solid #000;'>OBSERVACION</th></tr>");
            $("#tbExcelRq-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");
        }
    });
}