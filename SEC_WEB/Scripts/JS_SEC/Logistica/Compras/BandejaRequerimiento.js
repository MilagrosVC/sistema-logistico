﻿function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        //alert('Fecha ' + Fecha);
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                        //$('#txtDesde').val(fechadesdeformato);
                        //alert('fechatext ' + $('#txtDesde').val());*/
                        //ret = elemento;
                        //alert('ret ' + ret);
                    }
            }

    return ret;
}
//----------------------------------------------------------

frstChrg_BandejaRQ = true;
$(document).ready(function () {
    InicializarControles();
    GenerarBandeja();
    $("#btnBuscar").click(function () {
        GenerarBandeja();
    });
})



function InicializarControles()
{

    $("#btnLimpiar").click(function () {
        $("#txtCodRequerimiento").val("");
        $("#txtSolicitante").val("");
        $("#txtFiltroDesde").val("");
        $("#txtFiltroHasta").val("");

    });
    $("#txtFiltroDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',//'/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        //defaultDate: "+1w",
        onClose: function (selectedDate) {
        }
    });
    $("#txtFiltroHasta").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',//'/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        //defaultDate: "+1w",
        onClose: function (selectedDate) {
        }
    });
    $("#btnImprimirRQ").click(function () {
        $("#divImprimirRQ").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: "../../Scripts/Styles_SEC/SEC_BandRQ_ImprRQ.css"
        });
    });
    $("#btnCerrarVerRQ").click(function () {
        $("#dvVerRequerimiento").dialog('close');
    });

}

function GenerarBandeja() {
    var RqsAprb = 0;
    var RqsDesprb = 0;
    var RqsNuevos = 0;
    var totalRqs;

    //alert($("#txtFiltroDesde").val() + " / " + $("#txtFiltroHasta").val());

    var objRequerimientoCa = {
        Proyecto_Id: $("#ContentPlaceHolder1_hdProyecto").val(),
        Req_Solicitante: $("#txtSolicitante").val(),
        Requerimiento_Co: $("#txtCodRequerimiento").val(),
        FechaDesde: Controlar_Valores('txtFiltroDesde', 'datetime'),
        FechaHasta: Controlar_Valores('txtFiltroHasta', 'datetime')
    };

    $("#SEC_GrdBandRQ").html('<table id="gvGrdBandRQ"></table><div id="pieGrdBandRQ"></div>');
    $("#gvGrdBandRQ").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/BandejaRequerimiento.aspx/CargarBandejaRQ',
        datatype: "json",
        mtype: "POST",
        postData: "{objRequerimientoCa:" + JSON.stringify(objRequerimientoCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Cod. Requerimiento', 'Solicitante', 'Tipo', '', 'Estado', 'Num. Items', 'F. Requerimiento', 'F. Entrega', '', '', '', ''],
        colModel: [
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', hidden: true },
           { name: 'Requerimiento_Co', index: 'Requerimiento_Co', width: 100, align: 'center' },
           { name: 'Req_Solicitante', index: 'Solicitante', width: 200, align: 'center' },
           { name: 'Req_Tipo_Tx', index: 'Req_Tipo_Tx', width: 200, align: 'center' },
           { name: 'Req_Estado_Id', index: 'Req_Estado_Id', hidden: true },
           { name: 'Req_Estado_Tx', index: 'Req_Estado_Tx', width: 200, align: 'center' },
           { name: 'Cant_Items', index: 'Cant_Items', width: 60, align: 'center' },
           { name: 'Aud_Creacion_Fe', index: 'F. Requerimiento', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Req_Entrega_Fe', index: 'F. Entrega', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },

           { name: 'Ver', index: 'Ver', width: 35, align: 'center', formatter: 'actionFormatterVerRQ', search: false },
           { name: 'GenerarSolicitud', index: 'GenerarSolicitud', width: 35, align: 'center', formatter: 'actionFormatterFrmSolCompra', search: false },
           { name: 'Aprobar', index: 'Aprobar', width: 35, align: 'center', formatter: 'actionFormatterAprobar', search: false },
           { name: 'Desaprobar', index: 'Desaprobar', width: 35, align: 'center', formatter: 'actionFormatterDesaprobar', search: false },
        ],
        pager: "#pieGrdBandRQ",
        viewrecords: true,
        autowidth: true,
        height: 245,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { totalRqs = obj.d.length; return totalRqs; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Requerimientos',
        loadComplete: function (data) {
            var ids = $("#gvGrdBandRQ").getDataIDs();
            for (var i = 0; i < ids.length ; i++) {
                var rowId = ids[i];
                var rowdata = $("#gvGrdBandRQ").jqGrid("getRowData", rowId);
                if (rowdata.Req_Estado_Id == 8) {
                    $("#" + rowId).find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                }
                else if (rowdata.Req_Estado_Id == 10) {
                    $("#" + rowId).find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });
                    
                }
            }

            //console.log(data.d);
            var allRecords = $("#gvGrdBandRQ").getGridParam('records');

            if (allRecords > 0 && frstChrg_BandejaRQ === true) {
                for (var i = 0; i < allRecords; i++) {
                    if (data.d[i].Req_Estado_Id === 8) {
                        RqsNuevos = RqsNuevos + 1;
                    } else if (data.d[i].Req_Estado_Id === 9) {
                        RqsAprb = RqsAprb + 1;
                    } else if (data.d[i].Req_Estado_Id === 10) {
                        RqsDesprb = RqsDesprb + 1;
                    }
                }
                $("#RQsTotal").html(allRecords);
                $("#RQsAprobados").html(RqsAprb);
                $("#RQsDesaprobados").html(RqsDesprb);
                $("#RQsNuevos").html(RqsNuevos);
                frstChrg_BandejaRQ = false;
                //console.log("allRecords: " + allRecords + " / RqsNuevos: " + RqsNuevos + " / RqsAprb: " + RqsAprb + " / RqsDesprb: " + RqsDesprb);
            }
            
            /*
            if (allRecords > 0) {
                
                //----------------------------------------------------//
                if ((rowNum * page) > allRecords) {
                    for (var i = 0; i < rowNum - ((rowNum * page) - allRecords) ; i++) {
                        if ($("#gvGrdBandRQ").getRowData()[i].Req_Estado_Id == 8) {
                            $("#" + (i + (rowNum * (page - 1)) + 1), "#gvGrdBandRQ").find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                        }
                        else if ($("#gvGrdBandRQ").getRowData()[i].Req_Estado_Id == 10) {
                            $("#" + (i + (rowNum * (page - 1)) + 1), "#gvGrdBandRQ").find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });
                        }
                    }
                }else{
                    for (var i = 0; i < rowNum; i++) {
                        if ($("#gvGrdBandRQ").getRowData()[i].Req_Estado_Id == 8) {
                            $("#" + (i + (rowNum * (page - 1)) + 1), "#gvGrdBandRQ").find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                        }
                        else if ($("#gvGrdBandRQ").getRowData()[i].Req_Estado_Id == 10) {
                            $("#" + (i + (rowNum * (page - 1)) + 1), "#gvGrdBandRQ").find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });
                        }
                    } 
                }
            }*/
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    }).navGrid('#pieGrdBandRQ', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterVerRQ: function (cellvalue, options, rowObject) {
            return "<img title=\"Visualizar Requerimiento\" onclick=\"VisualizarRequerimiento(" + rowObject.Requerimiento_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        },
        actionFormatterFrmSolCompra: function (cellvalue, options, rowObject) {
            return "<img title=\"Generar Solicitud\" onclick=\"CargarSolicitudesCompra(" + rowObject.Requerimiento_Id + "," + rowObject.Req_Estado_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/GenSolicitud.png\" />";
        },
        actionFormatterAprobar: function (cellvalue, options, rowObject) {
            return "<img title=\"Aprobar Requerimiento\" onclick=\"EvaluarRq(" + rowObject.Requerimiento_Id + "," + rowObject.Req_Estado_Id + ", 9)\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Verif_16.png\" />";
        },
        actionFormatterDesaprobar: function (cellvalue, options, rowObject) {
            return "<img title=\"Rechazar Requerimiento\" onclick=\"EvaluarRq(" + rowObject.Requerimiento_Id + "," + rowObject.Req_Estado_Id + ", 10)\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Denied_16.png\" />";
        }
    });

}

function VisualizarRequerimiento(idRequerimiento) {
    
    requerimiento = {
        Requerimiento_Id: idRequerimiento
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaRequerimiento.aspx/GenerarVistaRequerimiento',
        dataType: "json",
        data: "{objRequerimientoCa:" + JSON.stringify(requerimiento) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblRq-CodRequerimiento").html("N°: " + data.d.Requerimiento_Co);
                $("#lblRq-Solicitante").html(data.d.Req_Solicitante);
                var FechaCreacion = ConvertFechaFormatoNormal(data.d.Aud_Creacion_Fe);
                $("#lblRq-FechaCreacion").html(FechaCreacion);
                var FechaEntrega = ConvertFechaFormatoNormal(data.d.Req_Entrega_Fe);
                $("#lblRq-FechaEntrega").html(FechaEntrega);

                $("#lblRq-NomProyecto").html(data.d.Req_Proyecto_Tx);
                $("#lblRq-Direccion").html(data.d.Req_ProyDireccion_Tx);
                $("#lblRq-Partida").html(data.d.Req_Partida_Tx);
                $("#lblRq-Turno").html(data.d.Req_Turno_Tx);
                $("#lstRq-Subpartidas").html(data.d.Lst_Partidas_Rq);

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaRequerimiento.aspx/ListarItemsxRequerimiento',
                    dataType: "json",
                    data: "{objRequerimientoDe:" + JSON.stringify(requerimiento) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbRq-Detalle").append(fila);
                                }
                                for (var i = (data.d.length - 1) ; i < 20; i++)
                                {
                                    var fila = "<tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>";
                                    $("#tbRq-Detalle").append(fila);
                                }
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].RDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:left;'>&nbsp" + data.d[i].RDe_Observacion_Tx + "</td></tr>";

                                    $("#tbRq-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Mensaje","No existen items en el requerimiento");
                        }
                    }
                });
            }
            else {
                Mensaje("Mensaje", "No se puede generar vista de Requerimiento");
            }
        }
    });

    /*$("#btnImprimirRQ").click(function () {
        $("#divImprimirRQ").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: "../../Scripts/Styles_SEC/SEC_BandRQ_ImprRQ.css"
        });
    });*/

    $("#dvVerRequerimiento").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "Requerimiento",
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbRq-Detalle").html("<tr><th style='width:50px'>ITEM</th><th style='width:300px'>DESCRIPCION</th><th style='width:70px'>CANTIDAD</th><th style='width:60px'>UND</th><th style='width:196px'>OBSERVACION</th></tr>");
            $("#tbRq-Detalle").append("<tr><td>&nbsp</td><td></td><td></td><td></td><td></td></tr>");
        }
    });

}

function EvaluarRq(idRequerimiento, estActual, resEval) {
    //alert("idRequerimiento: " + idRequerimiento + ", estActual: " + estActual + ", resEval: " + resEval);
    
        if (estActual == 10 && resEval == 10) {
            Mensaje("Mensaje", "El requerimiento ya fue rechazado!!!");
        }
        else if (estActual == 9 && resEval == 9) {
            Mensaje("Mensaje", "El requerimiento ya fue aprobado!!!");
        }
        else {
            var accion = 'aprobar';
            if (resEval == 9) {accion = 'aprobar';}else {accion = 'desaprobar';}
            ConfirmacionMensaje('Confirmacion', '¿Esta seguro que desea '+ accion +' el requerimiento?', function () {
                var requerimiento = {
                    Requerimiento_Id: idRequerimiento,
                    Req_Estado_Id: resEval
                }

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaRequerimiento.aspx/CambiarEstadoRequerimiento',
                    dataType: "json",
                    data: "{objRequerimientoCa:" + JSON.stringify(requerimiento) + "}",
                    success: function (data) {
                        if (resEval == 9) {
                            Mensaje("Mensaje", "El requerimiento fue aprobado!");
                        }
                        else {
                            Mensaje("Mensaje", "El requerimiento fue desaprobado!");
                        }
                        GenerarBandeja();
                    }
                });
            });
        }
    
}

function CargarSolicitudesCompra(idRequerimiento, estActual) {
    if (estActual == 8) { Mensaje("Mensaje", "Pendiente de evaluacion"); }
    else if (estActual == 10) { Mensaje("Mensaje", "El requerimiento fue desaprobado."); }
    else {
        //alert("Ir a pantalla de Solicitudes. IdRequerimiento: " + idRequerimiento);
        var url = "../../Logistica/Compras/SolicitudCompra.aspx?Requerimiento_Id=" + parseInt(idRequerimiento) + "&Proyecto_Id=" + parseInt($("#ContentPlaceHolder1_hdProyecto").val());
        //var res = encodeURI(url);
        window.location.href = url;
        //alert("Ir a pantalla de Solicitudes. IdRequerimiento: " + idRequerimiento);
    }
        
}