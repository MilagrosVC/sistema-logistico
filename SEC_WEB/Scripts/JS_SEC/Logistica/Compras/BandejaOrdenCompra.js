﻿var RequiereAnticipo = 67;
var Aprobada = 68;
var Denegada = 71;
var Cerrada = 15;

frstChrg_BandejaOC = true;
$(document).ready(function () {

    IniciarControles();
    
});


function IniciarControles() {

    $("#btnCerrarVerOC").click(function () {
        $("#dvVerOrdenCompra").dialog('close');
    });

    $('#btnCrear').click(function () {
        window.location = '../../Logistica/Compras/OrdenCompra.aspx';
    });

    $("#btnExport").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;

        var a = document.createElement('a');

        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divImprimirOC');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'OrdenCompra_' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
    
    $("#btnImprimirOC").click(function () {
        $("#divImprimirOC").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: BASE_URL + "/Scripts/Styles_SEC/SEC_Orden_Compra.css"
        });
    });

    $("#btnBuscar").click(function () {
        GenerarBandeja();
    });

    $("#btnLimpiar").click(function () {
        LimpiarFiltros();
    });

    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });

    $('#txtHasta').attr('disabled', 'disabled');
    $('#txtDesde').change(function () {
        var month = ($("#txtDesde").datepicker('getDate').getMonth() + 1);
        $("#txtHasta").datepicker({
            showOn: "button",
            minDate: '-1m',
            buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
            buttonImageOnly: true,
            buttonText: "Seleccione la fecha",
            dateFormat: "dd/mm/yy",
            defaultDate: "+1w",
            onClose: function (selectedDate) {
                $("#txtDesde").datepicker("option", "maxDate", selectedDate)
            }
        });
        $("#txtHasta").prop('disabled', false);
    });

    GenerarBandeja();
}

function GenerarBandeja() {
    var OOCRequiereAnticipos = 0;
    var OCCCerradas = 0;
    var OCCAprobadas = 0;
    var OCCDenegadas = 0;
    var totalOCC;


    var ObjOrdenCompraCa = {       
            Orden_Compra_Co: Controlar_Valores("txtCodOrdenCompra","string"),
            OCC_Estado_Id: Controlar_Valores("ddlEstadoBanOC","int"),
            OCC_Tipo_Id: Controlar_Valores("ddlTipoBanOC","int"),
            Proveedor_Id: Controlar_Valores("ddlProveedorBanOC", "int"),
            FechaDesde: Controlar_Valores("txtDesde", "datetime"),
            FechaHasta: Controlar_Valores("txtHasta", "datetime")
            };

    $("#divOrdenCompra").html('<table id="gvOrdenCompra"></table><div id="pieOrdenCompra"></div>');
    $("#gvOrdenCompra").jqGrid({
            regional: 'es',
            url: '../../Logistica/Compras/BandejaOrdenCompra.aspx/ListarOrdenCompra',
            datatype: "json",
            mtype: "POST",
            postData: "{ObjOrdenCompraCa:" + JSON.stringify(ObjOrdenCompraCa) + "}",
            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,           
            colNames: ['','', 'Codigo', 'N° Orden Compra', 'N° Proforma', 'Proveedor Id', 'Razon Social', 'Gestionador', 'Cot_Tipo_Id', 'Tipo', 'Cot_Estado_Id', 'Estado', 'Fecha Emision', 'Moneda_Id', 'Moneda', 'Cotizacion_Id', 'penalidad', 'Sub Total', 'IGV', 'Total'],
            colModel: [
               { name: 'Detalle', index: 'Detalle', width: 30, align: 'center', formatter: 'actionFormatterDetalle', search: false },
               { name: 'Factura', index: 'Factura', width: 30, align: 'center', formatter: 'actionFormatterFactura', search: false },
               { name: 'Orden_Compra_Id', index: 'Orden_Compra_Id', width: 75, align: 'center', hidden: true },
               { name: 'Orden_Compra_Co', index: 'Orden_Compra_Co', width: 100, align: 'center' },
               { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu', width: 100, align: 'center' },
               { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 150, align: 'center', hidden: true },
               { name: 'Prv_RazonSocial_Tx', index: 'Prv_RazonSocial_Tx', width: 250, align: 'center' },
               { name: 'OCC_Gestionador_Tx', index: 'OCC_Gestionador_Tx', width: 150, align: 'center', hidden: true },
               { name: 'OCC_Tipo_Id', index: 'OCC_Tipo_Id', width: 150, align: 'center', hidden: true },
               { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 150, align: 'center' },//, hidden: true 
               { name: 'OCC_Estado_Id', index: 'OCC_Estado_Id', width: 150, align: 'center', hidden: true },
               { name: 'Est_Nombre_Tx', index: 'Est_Nombre_Tx', width: 150, align: 'center' },
               { name: 'OCC_Emision_Fe', index: 'OCC_Emision_Fe', width: 120, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
               { name: 'Moneda_Id', index: 'Moneda_Id', width: 150, align: 'center', hidden: true },
               { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 150, align: 'center' },
               { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 150, align: 'center', hidden: true }, //, hidden: true 
               { name: 'OCC_Penalidad_Fl', index: 'OCC_Penalidad_Fl', width: 150, align: 'center', hidden: true },
               { name: 'OCC_SubTotal_Nu', index: 'OCC_SubTotal_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
               { name: 'OCC_IGV_Nu', index: 'OCC_IGV_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
               { name: 'OCC_Total_Nu', index: 'OCC_Total_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } }
            ],
            pager: "#pieOrdenCompra",
            viewrecords: true,
            autowidth: true,
            height: 'auto',
            rowNum: 10,
            rowList: [10, 20, 30, 100],
            gridview: true,
            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            emptyrecords: "No hay Registros.",
            caption: 'Bandeja de Ordenes de Compra',
            loadComplete: function (data) {
                var ids = $("#gvOrdenCompra").getDataIDs();
                for (var i = 0; i < ids.length ; i++) {
                    var rowId = ids[i];
                    var rowdata = $("#gvOrdenCompra").jqGrid("getRowData", rowId);
                    if (rowdata.OCC_Estado_Id == Aprobada) {
                        $("#" + rowId).find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                    }
                    else if (rowdata.OCC_Estado_Id == Denegada) {
                        $("#" + rowId).find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });
                    }
                }
                //console.log(data.d);
                var allRecords = $("#gvOrdenCompra").getGridParam('records');

                if (allRecords > 0 && frstChrg_BandejaOC === true) {
                    for (var i = 0; i < allRecords; i++) {
                        if (data.d[i].OCC_Estado_Id === RequiereAnticipo) {
                            OOCRequiereAnticipos = OOCRequiereAnticipos + 1;
                        } else if (data.d[i].OCC_Estado_Id === Aprobada) {
                            OCCAprobadas = OCCAprobadas + 1;
                        } else if (data.d[i].OCC_Estado_Id === Denegada) {
                            OCCDenegadas = OCCDenegadas + 1;
                        } else if (data.d[i].OCC_Estado_Id === Cerrada) {
                            OCCCerradas = OCCCerradas + 1;
                        }
                    }
                    $("#OCTotal").html(allRecords);
                    $("#OCReqAnticipo").html(OOCRequiereAnticipos);
                    $("#OCAprobados").html(OCCAprobadas);
                    $("#OCDenegada").html(OCCDenegadas);
                    $("#OCCerrada").html(OCCCerradas);
                    frstChrg_BandejaOC = false;
                    //console.log("allRecords: " + allRecords + " / RqsNuevos: " + RqsNuevos + " / RqsAprb: " + RqsAprb + " / RqsDesprb: " + RqsDesprb);
                }
            },

            loadError: function (xhr) {
                alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            }
        });
        $.extend($.fn.fmatter, {
            actionFormatterDetalle: function (cellvalue, options, rowObject) {
                return "<img title=\"Visualizar Detalle\" onclick=\"CargarFormularioDetalle(" + rowObject.Orden_Compra_Id+','+ rowObject.Cotizacion_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
            }
        });
        $.extend($.fn.fmatter, {
            actionFormatterFactura: function (cellvalue, options, rowObject) {
                if (rowObject.OCC_Estado_Id == Aprobada) {                    
                    return "<i title=\"Realizar Factura\" onclick=\"CargarFormularioFactura(this)\" style=\"cursor:pointer\" class='fa fa-external-link'></i>"
                }
                else
                {
                    return '<i title=\"No se permite ejecutar accion\" style=\"cursor:pointer\" class="fa fa-minus-square-o"></i>';
                    }
            }
        });        
}

function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                    }
            }
    return ret;
}

function LimpiarFiltros() {

    $('#ddlProveedorBanOC').val(0);
    $("#ComboBox_ddlProveedorBanOC").val($("#ddlProveedorBanOC option:selected").text());
    $('#ddlEstadoBanOC').val(0);
    $("#ComboBox_ddlEstadoBanOC").val($("#ddlEstadoBanOC option:selected").text());
    $('#ddlTipoBanOC').val(0);
    $("#ComboBox_ddlTipoBanOC").val($("#ddlTipoBanOC option:selected").text());
    
    $("#txtDesde").datepicker("destroy");
    $('#txtDesde').val('');
    $("#txtDesde").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "yy/mm/dd",
        defaultDate: "+1w",
        onClose: function (selectedDate) {
            $("#txtHasta").datepicker("option", "minDate", selectedDate);
            $('#txtHasta').datepicker("setDate", $('#txtDesde').val());
        }
    });
    $('#txtHasta').attr('disabled', 'disabled');
    $("#txtHasta").datepicker("destroy");
    $('#txtHasta').val('');
    $('#txtCodOrdenCompra').val('');
    $('#txtCodOrdenCompra').focus();
    GenerarBandeja();
}

function CargarFormularioDetalle(idOrdenCompra, Cotizacion_Id) {

    var igv = "", subtotal = "";
    var total = "";
    ordencompra = {
        Orden_Compra_Id: idOrdenCompra
    };

    cotizacion = {
        Cotizacion_Id: Cotizacion_Id
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaOrdenCompra.aspx/GenerarVistaOrdenCCA',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ordencompra) + "}",
        success: function (data) {
            if (data.d != null) {

                $("#lblOC-CodOrdenCompra").html("N°: " + data.d.Orden_Compra_Co);
                var FechaEmision = ConvertFechaFormatoNormal(data.d.OCC_Emision_Fe);
                $("#lblOC-Fecha").html(FechaEmision);
                $("#lblOC-RazonSocial").html(data.d.Prv_RazonSocial_Tx);
                $("#lblOC-Domicilio").html(data.d.Prv_Direccion_tx);
                $("#lblOC-Ciudad").html(data.d.Distrito_Tx);
                $("#lblOC-Telefono").html(data.d.Prv_Telefono1_nu);
                igv = data.d.OCC_IGV_Nu;
                subtotal = data.d.OCC_SubTotal_Nu;
                total = data.d.OCC_Total_Nu;


                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaOrdenCompra.aspx/GenerarVistaOrdenCDetalle',
                    dataType: "json",
                    data: "{ObjOrdenCompraDe:" + JSON.stringify(ordencompra) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].OCDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].OCDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbOC-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbOC-Detalle").append(fila);
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";
                                $("#tbOC-Detalle").append(fila);
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].OCDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbOC-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en la Orden de Compra");
                        }
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de la Orden de Compra");
            }
        }
    });

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaOrdenCompra.aspx/ListarEgreso',
        dataType: "json",
        data: "{ObjEgreso:" + JSON.stringify(cotizacion) + "}",
        success: function (data) {
            var iMax = 0;
            if (data.d.length > 0) {
                if (data.d.length < 20) {
                    for (var i = 0; i < data.d.length; i++) {
                        var fila = "<tr>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Moneda_Monto_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].Ege_NumeroOperacion.toString() + "</td>";
                        fila = fila + "<td colspan='2' style='text-align:center;border: 1px solid #000;'>" + data.d[i].TMP_Nombre_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Par_Nombre_Tx.toString() + "</td></tr>";
                        $("#tbOC-Anticipo").append(fila);
                    }
                }
                else {
                    for (var i = 0; i < data.d.length; i++) {
                        var fila = "<tr>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Moneda_Monto_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].Ege_NumeroOperacion.toString() + "</td>";
                        fila = fila + "<td colspan='2' style='text-align:center;border: 1px solid #000;'>" + data.d[i].TMP_Nombre_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Par_Nombre_Tx.toString() + "</td></tr>";

                        $("#tbOC-Anticipo").append(fila);
                    }
                }
            }
            else {
                Mensaje("Aviso", "No existen items de Anticipo");
            }
        }
    });


    $("#dvVerOrdenCompra").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "ORDEN DE COMPRA",
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbOC-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:50px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:300px;border: 1px solid #000;'>ARTICULO</th><th style='width:150px;border: 1px solid #000;'>PRECIO UND</th><th style='width:150px;border: 1px solid #000;'>TOTAL</th></tr>");
            $("#tbOC-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");
            $("#tbOC-Anticipo").html("<caption style='font-size:12px;font-weight:bold;color:#000;'>ANTICIPOS</caption>");
            $("#tbOC-Anticipo").append("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:120px;border: 1px solid #000;'>MONTO</th><th style='width:100px;border: 1px solid #000;'>N° OPERACIÓN</th><th colspan='2' style='width:300px;border: 1px solid #000;'>MEDIO PAGO</th><th style='width:100px;border: 1px solid #000;'>TIPO ANTICIPO</th></tr>");
            $("#tbOC-Anticipo").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td colspan='2' style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");

        }
    });
}

function CargarFormularioFactura(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvOrdenCompra").getRowData(RowId);

    window.location = BASE_URL + '/Logistica/Compras/Factura.aspx?Orden_Compra_Co=' + rowData.Orden_Compra_Co;
}