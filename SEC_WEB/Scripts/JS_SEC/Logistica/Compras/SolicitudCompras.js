﻿/// <reference path="../../../../Logistica/Compras/SolicitudCompra.aspx" />
//-- Function Eliminar en Posicion Array JS --//
Array.prototype.remove = function (x) {
    var i;
    for (i in this) {
        if (this[i].toString() == x.toString()) {
            this.splice(i, 1)
        }
    }
}
//--------------------------------------------//

//------------ Variables globales ------------//
var ItemsSeleccionados = [];
var ItemsSolicitados = 0;
var ProveedorxSolicitud = [];
//--------------------------------------------//

$(document).ready(function () {
    var idRequerimiento = $("#ContentPlaceHolder1_hdRequerimiento").val();
    var idProyecto = $("#ContentPlaceHolder1_hdProyecto").val();
    
    InicializarControles(idRequerimiento, idProyecto);

    //$("div").hide();
    //$("table").hide();
    //$("table").each(function (index, item) {
    //    var varTABLA = $(this);
    //    alert('item : ' + index + varTABLA.attr("id"));

    //});
   
});

function InicializarControles(idRequerimiento, idProyecto) {

    $("#hdNumSolicitud").val(0);

    $("#btnRegresar").click(function () {
        window.location.href = "../../Logistica/Compras/BandejaRequerimiento.aspx";
    });

    MostrarSolicitudes(idRequerimiento);

    CargarDatosProyecto(idProyecto);

    CargarDatosRQCa(idRequerimiento);
    
    $("#btnNuevo").click(function () {
        //------------------ Prueba de grilla Local _ Proveedor ------------------//
        ProveedorxSolicitud = [];
        MostrarGrillaLocalProv(ProveedorxSolicitud);
        $("#hdNumSolicitud").val(0);
        var total = 0;
        for (var i = 1; i <= $("#gvGrdItemsRQ").getGridParam('records') ; i++) {
            if ($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
                total = total + 1;
            }
        }
        if (total === $("#gvGrdItemsRQ").getGridParam('records')) {
            Mensaje("Mensaje","Todos los items del requerimiento fueron registrados en solicitudes.");
        }
        else {
            $("#dlg-NuevaSol").dialog({
                autoOpen: true,
                show: "blind",
                width: 860,
                height: 'auto',
                modal: true,
                title: "Nueva Solicitud",
                buttons: {
                    "Registrar": function () {
                        
                        //alert('"Registrar": function () { ItemsSeleccionados: ' + ItemsSeleccionados);

                        if (ItemsSeleccionados.length === 0 && total < $("#gvGrdItemsRQ").getGridParam('records')) {
                            Mensaje("Mensaje","Debe seleccionar por lo menos un item para crear una Solicitud.");
                        }else if ($("#gvGrdProvsSolRQ").getGridParam('records') === 0) {
                            Mensaje("Mensaje","Debe seleccionar por lo menos un proveedor para crear la Solicitud.");
                        }
                        else {
                            ConfirmacionMensaje("Confirmacion", '¿Desea crear la Solicitud?', function () {
                                CrearSolicitud(idRequerimiento, ItemsSeleccionados);
                                MostrarSolicitudes(idRequerimiento);
                                $("#gvGrdItemsRQ tr").addClass("ui-state-disabled ui-jqgrid-disablePointerEvents");

                                $("#gvGrdItemsRQ td").addClass("ui-state-disabled");

                                //---------- Bloquear el contenido de la grilla ------------//
                                var allRecords = $("#gvGrdItemsRQ").getGridParam('records');
                                var rowNum = $("#gvGrdItemsRQ").getGridParam('rowNum');
                                var page = $("#gvGrdItemsRQ").getGridParam('page');
                                var rowArray = $("#gvGrdItemsRQ").jqGrid('getDataIDs');
                                var filas;
                                if ((rowNum * page) > allRecords) {
                                    for (var i = (rowNum * (page - 1)) + 1 ; i <= allRecords ; i++) {
                                        if ($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx != 'SOLICITADO') {
                                            $("#gvGrdItemsRQ tr").addClass("ui-state-disabled ui-jqgrid-disablePointerEvents");
                                            $("#gvGrdItemsRQ td").addClass("ui-state-disabled");
                                            $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                                        }
                                        else {
                                            $("#gvGrdItemsRQ tr").addClass("ui-state-disabled ui-jqgrid-disablePointerEvents");
                                            $("#gvGrdItemsRQ td").addClass("ui-state-disabled");
                                            $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                                        }
                                    }
                                }
                                else {
                                    for (var i = (rowNum * (page - 1)) + 1 ; i <= (rowNum * page) ; i++) {
                                        if ($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
                                            $("#gvGrdItemsRQ tr").addClass("ui-state-disabled ui-jqgrid-disablePointerEvents");
                                            $("#gvGrdItemsRQ td").addClass("ui-state-disabled");
                                            $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                                        }
                                        else {
                                            $("#gvGrdItemsRQ tr").addClass("ui-state-disabled ui-jqgrid-disablePointerEvents");
                                            $("#gvGrdItemsRQ td").addClass("ui-state-disabled");
                                            $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                                        }
                                    }
                                }

                                //Oculta el boton Registrar para luego mostrar el boton Finalizar
                                /*$('.ui-dialog-buttonpane button:contains("Registrar")').button().hide();
                                $('.ui-dialog-buttonpane button:contains("Finalizar")').button().show();*/

                                /*$("#SEC_ProvsSolRQ-Proveedor").show();*/

                                $("#dlg-NuevaSol").dialog("close");
                                CargarDatosRQCa(idRequerimiento);
                                MostrarSolicitudes(idRequerimiento);
                            });
                        }
                    },
                    /*"Finalizar": function () {
                        if ($("#gvGrdProvsSolRQ").getGridParam('records') === 0) {
                            alert("Debe seleccionar al menos un proveedor.");
                        }
                        else {
                            $(this).dialog("close");
                            CargarDatosRQCa(idRequerimiento);
                            MostrarSolicitudes(idRequerimiento);
                        }
                    },*/
                    "Cancelar": function () {
                        $(this).dialog("close");
                    }
                },
                open: function (event, ui) {
                    $('.ui-dialog-buttonpane').find('button:contains("Registrar")').addClass('boton');
                    $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
                    $(".ui-dialog-titlebar-close", ui.dialog).hide();
                }
            });

            //Luego de cargar el Dialog oculta el boton Finalizar
            /*$('.ui-dialog-buttonpane button:contains("Finalizar")').button().hide();*/
        }
    });
    
    $('#ContenidoMsj').editable({ inlineMode: false, language: 'es', maxCharacters: 140, height: 200, buttons: ['bold', 'italic', 'underline', 'fontSize', 'align', 'insertHorizontalRule', 'undo', 'redo', 'html'] })
    $("#DeMsj").prop('readonly', true).html($("#ContentPlaceHolder1_hdMailUsu").val());
    /*$("#btnAgregarProv").click(function (e) {
        e.preventDefault();
        if ($("#ddlProveedores").val() === 0)
        {
            alert("Elija por lo menos un proveedor");
        } else {
            //alert($("#hdNumSolicitud").val());
            $("#ddlProveedores").focus();
            InsertarProveedorSolicitud($("#hdNumSolicitud").val());
        }
        return false;
    });*/

    //------------------ Prueba de grilla Local _ Proveedor ------------------//
    $("#btnAgregarProv").click(function (e) {
        /*e.preventDefault();*/
        if ($("#ddlProveedores").val() === 0) {
            Mensaje("Mensaje","Elija por lo menos un proveedor.");
            $("#ddlProveedores").focus();
        } else {
            //alert($("#hdNumSolicitud").val());
            if (InsertarProveedorSolicitud_local($("#ddlProveedores").val()) === 0) {
                Mensaje("Mensaje", "El proveedor ya fue agregado.");
            };
            $("#ddlProveedores").focus();
        }
        /*return false;*/
    });
    $("#EditSol-btnAgregarProv").click(function (e) {
        ConfirmacionMensaje('Confirmación', '¿Desea agregar el proveedor a la solicitud?', function () {
            InsertarProveedorSolicitud($("#hdNumSolicitud").val());
        }, function () { });
    });
    $("#btnExport").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "" + month + "" + year + "_" + hour + "" + mins + segs;
        
        var a = document.createElement('a');
        
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divExcel');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'Solicitud' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
}

function CargarDatosProyecto(idProyecto){
    var proyecto = {
        Empresa_Id: 1,
        Proyecto_Id: idProyecto
    }
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/DatosProyecto',
        dataType: "json",
        data: "{objProyectoBE:" + JSON.stringify(proyecto) + "}",
        success: function (data) {
            $("#lblProyecto").html(data.d.Pry_Nombre_Tx);
            $("#lblSolExcel-NomProyecto").html(data.d.Pry_Nombre_Tx);
            $("#lblSolExcel-Direccion").html(data.d.Pry_Direccion_Tx);
        }
    });
}

function CargarDatosRQCa(idRequerimiento) {
    var requerimiento = {
        Requerimiento_Id: idRequerimiento
    }
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarRequerimientoCa',
        dataType: "json",
        data: "{objRequerimientoCaBE:" + JSON.stringify(requerimiento) + "}",
        success: function (data) {
            $("#lblRequerimiento").html(data.d.Requerimiento_Co);
            var FechaRQ = ConvertFechaFormatoNormal(data.d.Aud_Creacion_Fe);
            $("#lblFRequerimiento").html(FechaRQ);
            //alert(data.d.Req_Solicitante);
            $("#lblSolicitante").html(data.d.Req_Solicitante);
            $("#lblNumItems").html(data.d.Cant_Items);
            var FechaEntregaRQ = ConvertFechaFormatoNormal(data.d.Req_Entrega_Fe);
            $("#lblSolExcel-FechaEntrega").html(FechaRQ);
            //$("#lblFRequerimiento").val(data.d.Aud_Creacion_Fe.toLocaleDateString());
            MostrarItemsRequerimiento(data.d.Requerimiento_Id);
        }
    });
    
}

function MostrarItemsRequerimiento(idRequerimiento) {
    //alert('MostrarItemsRequerimiento');
    
    myidselect = [];
    var objRequerimientoDe = {
        Requerimiento_Id: idRequerimiento,
    };

    $("#SEC_ItemsRQ").html('<table id="gvGrdItemsRQ"></table><div id="pieGrdItemsRQ"></div>');
    $("#gvGrdItemsRQ").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarItemsxRequerimiento',
        datatype: "json",
        mtype: "POST",
        postData: "{objRequerimientoDe:" + JSON.stringify(objRequerimientoDe) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: [/*'',*/ '', 'Descripcion', 'Cantidad', '', 'U.M.', 'Estado', 'Observacion', 'Id Requerimiento', '', 'Cod. Item'],
        colModel: [
           /*{ name: 'vote', width: 40, align: 'center', formatoptions: { disabled: false }, editable: true, edittype: 'checkbox', formatter: 'checkbox' },*/
           { name: 'Requerimiento_Detalle_Id', index: 'Requerimiento_Detalle_Id', hidden: true },
           { name: 'RDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 350, align: 'center' },
           { name: 'RDe_Cantidad_Nu', index: 'RDe_Cantidad_Nu', width: 80, align: 'center' },
           { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 80, align: 'center' },
           { name: 'RDe_Estado_Tx', index: 'RDe_Estado_Tx', width: 120, align: 'center' },
           { name: 'RDe_Observacion_Tx', index: 'RDe_Observacion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 80, align: 'center', hidden: true },
           { name: 'Item_Id', index: 'Item_Id', hidden: true },
           { name: 'Itm_Co', index: 'Itm_Co', width: 100, align: 'center' },
           /*{ name: 'Aud_Creacion_Fe', index: 'F. Requerimiento', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Req_Entrega_Fe', index: 'F. Entrega', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },*/

           /*{ name: 'Ver', index: 'Ver', width: 45, align: 'center', formatter: 'actionFormatterVerRQ', search: false },
           { name: 'GenerarSolicitud', index: 'GenerarSolicitud', width: 45, align: 'center', formatter: 'actionFormatterFrmSolCompra', search: false },
           { name: 'Aprobar', index: 'Aprobar', width: 45, align: 'center', formatter: 'actionFormatterAprobar', search: false },
           { name: 'Desaprobar', index: 'Desaprobar', width: 45, align: 'center', formatter: 'actionFormatterDesaprobar', search: false },*/
        ],
        pager: "#pieGrdItemsRQ",
        viewrecords: true,
        autowidth: true,
        height: 130,
        rowNum: 5,
        rowList: [5, 10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Items',
        multiselect: true,
        onSelectAll: function(){
        },
        onSelectRow: function (id, isSelected) {

            //alert('onSelectRow: function (id, isSelected) { id:' + id + '  isSelected: ' + isSelected);
            //var myGrid = $("#gvGrdItemsRQ");
            var existe = 0;
            ItemsSeleccionados = [];
            //------- Verifica si el item ya fue seleccionado -------//
            if (myidselect.length > 0)
                for (var i = 0; i < myidselect.length; i++) {
                    
                    if (id === myidselect[i]) {
                        existe = 1;
                    }
                }
            //-------------------------------------------------------//

            //--- Inserta los items no seleccionados: existe = 0  ---//
            if (existe === 0 && myidselect.length > 0)
            {
                myidselect.push(id);
            }
            else if (myidselect.length === 0)
            {
                myidselect.push(id);
            }
            //-------------------------------------------------------//

            // --- Para eliminar el Item seleccionado --- //
            if (myidselect.length > 0) {
                for (var i = myidselect.length; i--;) {
                    //alert("id: " + id + " / myidselect[i]: " + myidselect[i] + " / isSelected: " + isSelected);
                    if (myidselect[i] === id && isSelected == false) {
                        //alert("Se elimina id: " + id);
                        myidselect.splice(i, 1);
                    }
                }
            }
            //ItemsSeleccionados = myidselect;
            // --- Obtiene los Id's de requerimiento para el llenado de Items  SEC_SOLICITUD_DE --- //
            for (var i = 0; i < myidselect.length; i++) {
                

                ItemsSeleccionados.push($("#gvGrdItemsRQ").getLocalRow(myidselect[i]).Requerimiento_Detalle_Id);

                //alert('ItemsSeleccionados:' + ItemsSeleccionados);

            }
            
        },
        loadComplete: function (data) {
            //ItemsSolicitados = 0;
            
            // --------- //
            var myGrid = $("#gvGrdItemsRQ");
            $("#cb_" + myGrid[0].id).hide();
            // --------- //

            //-------- Seccion de carga de Items seleccionados --------//
            /*var idsSeleccionados = "";
            for (var i = 0; i < myidselect.length; i++) {
                idsSeleccionados = idsSeleccionados + myidselect[i].toString() + " ";
            }*/
            
            var allRecords = $("#gvGrdItemsRQ").getGridParam('records');
            var rowNum = $("#gvGrdItemsRQ").getGridParam('rowNum');
            var page = $("#gvGrdItemsRQ").getGridParam('page');
            var rowArray = $("#gvGrdItemsRQ").jqGrid('getDataIDs');
            var filas;

            if ((rowNum * page) > allRecords) {
                //filas = rowNum - ((rowNum * page) - allRecords);
                for (var i = (rowNum * (page - 1)) + 1 ; i <= allRecords ; i++) {
                    for (var j = 1; j <= myidselect.length ; j++)
                    {
                        if (i == myidselect[j - 1]) {
                            //alert("Fila: " + i + " / Esta se chequea!");
                            $("#gvGrdItemsRQ").setSelection(i, true);
                        }
                    }
                    if($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO')
                    {
                        $("#" + (i), "#gvGrdItemsRQ").find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                        $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                    }
                }
            }
            else {
                //filas = rowNum;
                for (var i = (rowNum * (page - 1)) + 1 ; i <= (rowNum * page) ; i++) {
                    for (var j = 1; j <= myidselect.length ; j++)
                    {
                        if (i == myidselect[j-1]) {
                            //alert("Fila: " + i + " / Esta se chequea!");
                            $("#gvGrdItemsRQ").setSelection(i, true);
                        }
                    }
                    if ($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
                        $("#" + (i), "#gvGrdItemsRQ").find("td").css({
                            'background-color': '#E3F6CE', 'font-weight': 'bold', 'color': '#000'
                        });
                        $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
                    }
                }
            }
            //---------------------------------------------------------//

            // --- Seccion para pintar datos de grilla, los Items ya solicitados --- //
            for (var i = 0; i < myidselect.length; i++) {
                $("#gvGrdItemsRQ").getLocalRow(myidselect[i]).Requerimiento_Detalle_Id;
            }
        },
        rowattr: function (item) {
            if (item.RDe_Estado_Tx === 'SOLICITADO') {
                
                return {"class": "ui-state-disabled ui-jqgrid-disablePointerEvents"};
            }
        },
        beforeSelectRow: function (rowid, e) {
            if ($(e.target).closest("tr.jqgrow").hasClass("ui-state-disabled")) {
                return false;   // not allow select the row
            }
            return true;    // allow select the row
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    }).navGrid('#pieGrdItemsRQ', { edit: false, add: false, del: false, search: false, refresh: false }).trigger("reloadGrid");
    
}

function CrearSolicitud(idRequerimiento, ItemsxSol) {
    //alert('function CrearSolicitud(idRequerimiento, ItemsxSol) {');

    var solicitud = {
        Requerimiento_Id: idRequerimiento,
        Aud_UsuarioCreacion_Id: $("#ContentPlaceHolder1_hdUsuario").val(),
        Sol_Observacion_Tx: $("#txtArSolObs").val(),
        ItemsxSol: ItemsxSol.toString(),
        Lst_Proveedor: ProveedorxSolicitud.toString(),
    }

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/CrearSolicitudCayDe',
        dataType: "json",
        data: "{ObjSolicitudCa:" + JSON.stringify(solicitud) + "}",
        success: function (data) {
            Mensaje('Mensaje', 'La solicitud fue creada con éxito.');
            $("#hdNumSolicitud").val(data.d);
            //CargarLstProv($("#hdNumSolicitud").val());
            //alert("La solicitud fue creada, seleccione los proveedores a tomar en cuenta!!!");
        },
        error: function () {
            Mensaje("Mensaje","Error al registrar la solicitud!!!");
        }
    });
}

function MostrarSolicitudes(idRequerimiento)
{
    var ObjSolicitudCa = {
        Requerimiento_Id: idRequerimiento,
    };

    $("#SEC_Solicitudes").html('<table id="gvGrdSolicitudes"></table><div id="pieGrdSolicitudes"></div>');
    $("#gvGrdSolicitudes").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarSolicitudes',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjSolicitudCa:" + JSON.stringify(ObjSolicitudCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: [/*'',*/ 'Id Solic.', '', 'Solicitante', 'Cantidad Items', '', 'Estado', 'F. Creacion','Num. Requerimiento','','','',''],
        colModel: [
           { name: 'Solicitud_Id', index: 'Solicitud_Id', width: 50, align: 'center' },
           { name: 'Solicitante_Id', index: 'Solicitante_Id', hidden: true },
           { name: 'Sol_Solicitante_Tx', index: 'Sol_Solicitante_Tx', width: 100, align: 'center', onmouseover: function () {  } },
           { name: 'Sol_Cant_Items', index: 'Sol_Cant_Items', width: 80, align: 'center' },
           { name: 'Sol_Estado_Id', index: 'Sol_Estado_Id', hidden: true },
           { name: 'Sol_Estado_Tx', index: 'Sol_Estado_Tx', width: 120, align: 'center' },
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 150, align: 'center', formatter: 'date', formatoptions: { srcformat: 'ISO8601Long', newformat: 'd-m-Y  H:i  A' } },
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 80, align: 'center' },

           { name: 'Editar', index: 'Ver', width: 30, align: 'center', formatter: 'actionFormatterEditarSol', search: false },
           { name: 'Enviar', index: 'Enviar', width: 30, align: 'center', formatter: 'actionFormatterEnviarSol', search: false, hidden: true },
           { name: 'Exportar', index: 'Exportar', width: 30, align: 'center', formatter: 'actionFormatterExportarExcel', search: false },
           { name: 'AdmCotizaciones', index: 'AdmCotizaciones', width: 30, align: 'center', formatter: 'actionFormatterAdmCotizaciones', search: false }
           /*{ name: 'Aprobar', index: 'Aprobar', width: 45, align: 'center', formatter: 'actionFormatterAprobar', search: false },
           { name: 'Desaprobar', index: 'Desaprobar', width: 45, align: 'center', formatter: 'actionFormatterDesaprobar', search: false },*/
        ],
        pager: "#pieGrdSolicitudes",
        viewrecords: true,
        autowidth: true,
        height: 235,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Solicitudes',
        loadComplete: function (data) {

        },
        loadError: function () {
        },
        subGrid: true,
        subGridRowExpanded: function(subgrid_id, row_id) {
            // we pass two parameters
            // subgrid_id is a id of the div tag created within a table
            // the row_id is the id of the row
            // If we want to pass additional parameters to the url we can use
            // the method getRowData(row_id) - which returns associative array in type name-value
            // here we can easy construct the following
            var idSolicitud = $("#gvGrdSolicitudes").getLocalRow(row_id).Solicitud_Id;
            ObjSolicitudDe = {
                Solicitud_id : idSolicitud
            }
            var subgrid_table_id;
            subgrid_table_id = subgrid_id+"_t";
            jQuery("#"+subgrid_id).html("<label style='margin:2px 10px;'>Items de la Solicitud:</label><table id='"+subgrid_table_id+"' class='scroll'></table>");
            jQuery("#"+subgrid_table_id).jqGrid({
                url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarSolicitudDetalle',
                datatype: "json",
                mtype: "POST",
                postData: "{ObjSolicitudDe:" + JSON.stringify(ObjSolicitudDe) + "}",
                ajaxGridOptions: { contentType: "application/json" },
                loadonce: true,
                colNames: ['', '', 'Descripcion', 'Cantidad', '', 'U.M.', 'Estado', 'Cod. Item'],
                colModel: [
                    { name: 'Solicitud_Detalle_Id', index: 'Requerimiento_Detalle_Id', hidden: true },
                    { name: 'Item_Id', index: 'Item_Id', hidden: true },
                    { name: 'SDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 350, align: 'center' },
                    { name: 'SDe_Cantidad_Nu', index: 'SDe_Cantidad_Nu', width: 80, align: 'center' },
                    { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
                    { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 80, align: 'center' },
                    { name: 'SDe_Estado_Tx', index: 'RDe_Estado_Tx', width: 120, align: 'center' },
                    { name: 'Itm_Co', index: 'Itm_Co', width: 100, align: 'center' }
                ],
                gridview: true,
                height: '100%',
                //rowNum: 10,
                jsonReader: {
                    page: function (obj) { return 1; },
                    total: function (obj) { return 1; },
                    records: function (obj) { return obj.d.length; },
                    root: function (obj) { return obj.d; },
                    repeatitems: false,
                    id: "0"
                },
                sortname: 'Solicitud_Detalle_Id',
                sortorder: "desc"
            });

        }
        /*subGridUrl: '../../Logistica/Compras/SolicitudCompra.aspx/ListarItemsxRequerimiento',
        subGridNames: ['', 'Descripcion', 'Cantidad', '', 'U.M.', 'Estado', 'Observacion', 'Id Requerimiento', '', 'Cod. Item'],
        subGridModel: [
            //{
                //name: ['No', 'Item', 'Qty', 'Unit', 'Line Total'],
                //width: [55, 200, 80, 80, 80],
                //align: ['left', 'left', 'right', 'right', 'right'],
                //params: ['invdate']
                { name: 'Requerimiento_Detalle_Id', index: 'Requerimiento_Detalle_Id', hidden: true },
                { name: 'RDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 350, align: 'center' },
                { name: 'RDe_Cantidad_Nu', index: 'RDe_Cantidad_Nu', width: 80, align: 'center' },
                { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
                { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 80, align: 'center' },
                { name: 'RDe_Estado_Tx', index: 'RDe_Estado_Tx', width: 120, align: 'center' },
                { name: 'RDe_Observacion_Tx', index: 'RDe_Observacion_Tx', width: 300, align: 'center', hidden: true },
                { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 80, align: 'center', hidden: true },
                { name: 'Item_Id', index: 'Item_Id', hidden: true },
                { name: 'Itm_Co', index: 'Itm_Co', width: 100, align: 'center' }
            //}
        ]*/
    }).navGrid('#pieGrdSolicitudes', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterEditarSol: function (cellvalue, options, rowObject) {
            return "<i title=\"Editar Solicitud\" onclick=\"EditarSolicitud(" + rowObject.Solicitud_Id + ", " + rowObject.Requerimiento_Id + ")\" style=\"cursor:pointer;color:#222;\" class='fa fa-pencil-square-o fa-2x'></i>"
            //return "<img title=\"Editar Solicitud\" onclick=\"EditarSolicitud(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        },
        actionFormatterEnviarSol: function (cellvalue, options, rowObject) {
            return "<img title=\"Enviar Solicitud\" onclick=\"EnviarSolicitud(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/EnviarMensaje02_24.png\" />";
        },
        actionFormatterExportarExcel: function (cellvalue, options, rowObject) {
            return "<img title=\"Exportar a Excel\" onclick=\"ExportarExcel(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/ExpExcel01_16.png\" />";
        },
        actionFormatterAdmCotizaciones: function (cellvalue, options, rowObject) {
            return "<img title=\"Ir a Cotizaciones\" onclick=\"IrACotizaciones(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/IrFormulario01_15.png\" />";
        }
    });
        
}
function GenerarExcelSolicitud() {
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var segs = dt.getSeconds();

    var dateNom = day + "" + month + "" + year + "_" + hour + "" + mins + segs;

    var a = document.createElement('a');

    var data_type = 'data:application/vnd.ms-excel';
    var table_div = document.getElementById('divExcel');
    var table_html = table_div.outerHTML.replace(/ /g, '%20');
    a.href = data_type + ', ' + table_html;
    //Definiendo nombre del archivo
    a.download = 'Solicitud' + dateNom + '.xls';
    //window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#divExcel').html()));

    a.click();
}
function ExportarExcel(Solicitud_Id){
    ObjSolicitudCa = {
        Solicitud_id: Solicitud_Id
    }
    $.ajax({
        type: "POST",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarSolicitudes',
        data: "{ObjSolicitudCa:" + JSON.stringify(ObjSolicitudCa) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#lblSolExcel-Solicitante").html(data.d[0].Sol_Solicitante_Tx);
            var FechaCreacionSol = ConvertFechaFormatoNormal(data.d[0].Aud_Creacion_Fe);
            $("#lblSolExcel-FechaCreacion").html(FechaCreacionSol);
            $("#lblSolExcel-Observacion").html(data.d[0].Sol_Observacion_Tx);
            //alert(data.d[0].Solicitud_Id);
            ObjSolicitudDe = {
                Solicitud_id: data.d[0].Solicitud_Id
            }
            $.ajax({
                type: "POST",
                url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarSolicitudDetalle',
                data: "{ObjSolicitudDe:" + JSON.stringify(ObjSolicitudDe) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data.d);
                    
                    for (var i = 0; i < data.d.length; i++) {
                        var fila = "<tr><td></td>";
                        fila = fila + "<td style='text-align:center;font-size:13px;border:1px groove';color:#6E6D6D>" + (i + 1) + "</td>";
                        fila = fila + "<td style='text-align:left;font-size:13px;border:1px groove';color:#6E6D6D>&nbsp" + data.d[i].RDe_Descripcion_Tx + "</td>";
                        fila = fila + "<td style='text-align:center;font-size:13px;border:1px groove';color:#6E6D6D>" + data.d[i].SDe_Cantidad_Nu.toString() + "</td>";
                        fila = fila + "<td style='text-align:center;font-size:13px;border:1px groove';color:#6E6D6D>" + data.d[i].UMe_Simbolo_Tx + "</td></tr>  ";
                        //fila = fila + "<td style='text-align:left;font-size:11px'>&nbsp" + data.d[i].SDe_Observacion_Tx + "</td></tr>";

                        $("#tbExcelRq-Detalle").append(fila);
                    }
                    GenerarExcelSolicitud();
                    //window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#divExcel').html()));
                },
                error: function (data) {
                    alert('Error! ...');
                }
            });
        },
        error: function (data) {
            alert('Error! ...');
        }
    });



    
}
//----------------------- ENVIO DE MAIL ---------------------------
function EnviarSolicitud(Solicitud_Id) {
    $("#ContenidoMsj").val("<br><hr><p><strong><em>Dinetech S.A.C.</em></strong></p>");
    $("#dlg-EnviarSol").dialog({
        autoOpen: true,
        show: "blind",
        width: 490,
        height: 600,
        modal: true,
        title: "Enviar Solicitud",
        buttons: {
            "Enviar": function () {
                EnviaMail();
                $(this).dialog("close");
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Enviar")').addClass('boton');
            $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
}

function EnviaMail() {
    var sDe = $("#ContentPlaceHolder1_hdMailUsu").val();
        
    var sPara = $("#ParaMsj").val();
    var sCCPara = $("#CCMsj").val();
    var sAsunto = $("#AsuntoMsj").val();
    var sContenido = $("#ContenidoMsj").val();
    //var datos = '{archivo: "' + res[1] + '", correo: "' + correo + '", contenido: "' + contenido + '"}';
    //var datos = '{sDe: "' + sDe + '", sPara: "' + sPara + '", sCCPara: "' + sCCPara + '", sAsunto: "' + sAsunto + '", sContenido: "' + sContenido + '"}';
    //alert(datos);
    $.ajax({
        type: "POST",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/EnviarMailSolicitud',
        data: datos,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            /*if (data.d == '0') {
                alert('Error al enviar correo. 1');
                return;
            }*/
            //alert(data.d);
            if (data.d === 1) {
                Mensaje("Mensaje",'Se envió el correo.');
            }
            else {
                Mensaje("Mensaje",'Error!... No se envió el correo.');
            }
                
        },
        error: function (data) {
            Mensaje("Mensaje",'Error interno.');
        }
    });
    
}
//-----------------------------------------------------------------

function IrACotizaciones(Solicitud_Id) {
    var url = "../../Logistica/Compras/CotizacionRequerimiento.aspx?Proyecto_Id=" + parseInt($("#ContentPlaceHolder1_hdProyecto").val()) + "&Requerimiento_Id=" + parseInt($("#ContentPlaceHolder1_hdRequerimiento").val()) + "&Solicitud_Id=" + parseInt(Solicitud_Id);
    window.location.href = url;
}

function InsertarProveedorSolicitud(idSolicitud) {
    //alert(idSolicitud);
    //alert("Items seleccionados: " + ItemsxSol.toString());
    //alert($("#EditSol-ddlProveedores").val())
    var ObjSolicitudPrv = {
        Solicitud_Id: idSolicitud,
        Aud_UsuarioCreacion_Id: $("#ContentPlaceHolder1_hdUsuario").val(),
        Proveedor_Id: $("#EditSol-ddlProveedores").val()
    }
    
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/InsertarProveedorSolicitud',
        dataType: "json",
        data: "{ObjSolicitudPrv:" + JSON.stringify(ObjSolicitudPrv) + "}",
        success: function (data) {
            CargarLstEditProv(idSolicitud);
        },
        error: function () {
            alert("Error!!!");
        }
    });
}

function EliminarProveedor(idSolPrv, idSolicitud) {
    ConfirmacionMensaje("Confirmacion", "¿Desea eliminar el proveedor de la Solicitud?", function () {
        ObjSolicitudPrv = {
            Solicitud_Prov_Id: idSolPrv,
            Aud_UsuarioEliminacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
        }

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Compras/SolicitudCompra.aspx/EliminarProveedorSolicitud',
            dataType: "json",
            data: "{ObjSolicitudPrv:" + JSON.stringify(ObjSolicitudPrv) + "}",
            success: function (data) {
                if (data.d = 1) {
                    CargarLstEditProv(idSolicitud)
                    Mensaje("Mensaje", "El proveedor fue eliminado.");
                }
                else {
                    Mensaje("Mensaje", "Error al eliminar proveedor.");
                }
            },
            error: function () {
                alert("Error!!!");
            }
        });
    }, function () {
        
    });
}


// -----------             Funciones para la Edicion de una Solicitud            ----------- //
function CargarDatosSolicitud(idSolicitud) {
    ObjSolicitudCa = {
        Solicitud_id: idSolicitud
    }
    $.ajax({
        type: "POST",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarSolicitudes',
        data: "{ObjSolicitudCa:" + JSON.stringify(ObjSolicitudCa) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#EditSol-Obs").val(data.d[0].Sol_Observacion_Tx);
        },
        error: function (data) {
            alert('Error! ...');
        }
    });
}

function EditarSolicitud(idSolicitud, idRequerimiento) {
    $("#hdNumSolicitud").val(idSolicitud);
    CargarDatosSolicitud(idSolicitud);
    CargarLstEditItems(idSolicitud, idRequerimiento);
    CargarLstEditProv(idSolicitud);
    $("#dlg-EditSol").dialog({
        autoOpen: true,
        show: "blind",
        width: 860,
        height: 'auto',
        modal: true,
        title: "Editar Solicitud",
        buttons: {
            "Aceptar": function () {
                //CargarDatosRQCa(idRequerimiento);
                //MostrarSolicitudes(idRequerimiento);
                ConfirmacionMensaje("Confirmacion", "¿Desea finalizar la edición de la Solicitud?", function () {
                    CargarDatosRQCa(idRequerimiento);
                    MostrarSolicitudes(idRequerimiento);
                    $("#dlg-EditSol").dialog("close");
                }, function () {

                });
            }
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
}
function EditarItem(idSolicitud, idSolDetalle, idRqDetalle) {
    var ObjSolicitudDe = {
        Solicitud_Id: idSolicitud,
        Solicitud_Detalle_Id: idSolDetalle,
        Requerimiento_Detalle_Id: idRqDetalle,
        Aud_UsuarioCreacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
    }
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/SolicitudCompra.aspx/EditarSolicitudDetalle',
        dataType: "json",
        data: "{ObjSolicitudDe:" + JSON.stringify(ObjSolicitudDe) + "}",
        success: function (data) {
            $('#gvEditSol-Items').setGridParam({ page: 1, datatype: "json" }).trigger('reloadGrid');
        },
        error: function (xhr) {
            //alert("Error!!!");
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
}
function CargarLstEditItems(idSolicitud, idRequerimiento) {
    myidselect = [];
    var objRequerimientoDe = {
        Requerimiento_Id: idRequerimiento,
    };


    $("#EditSol-Items").html('<table id="gvEditSol-Items"></table><div id="pieGrdEditSol-Items"></div>');
    $("#gvEditSol-Items").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarItemsxRequerimiento',
        datatype: "json",
        mtype: "POST",
        postData: "{objRequerimientoDe:" + JSON.stringify(objRequerimientoDe) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: [/*'',*/ '', 'Descripcion', 'N° Solicitud', '', 'Cantidad', '', 'U.M.', 'Estado', 'Observacion', 'Id Requerimiento', '', 'Cod. Item'],
        colModel: [
           /*{ name: 'vote', width: 40, align: 'center', formatoptions: { disabled: false }, editable: true, edittype: 'checkbox', formatter: 'checkbox' },*/
           
           { name: 'Requerimiento_Detalle_Id', index: 'Requerimiento_Detalle_Id', hidden: true },
           { name: 'RDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 350, align: 'center' },
           { name: 'Solicitud_Id', index: 'Solicitud_Id', width: 80, align: 'center' },
           { name: 'Solicitud_Detalle_Id', index: 'Solicitud_Detalle_Id', hidden: true },
           { name: 'RDe_Cantidad_Nu', index: 'RDe_Cantidad_Nu', width: 60, align: 'center' },
           { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 60, align: 'center' },
           { name: 'RDe_Estado_Tx', index: 'RDe_Estado_Tx', width: 120, align: 'center' },
           { name: 'RDe_Observacion_Tx', index: 'RDe_Observacion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 80, align: 'center', hidden: true },
           { name: 'Item_Id', index: 'Item_Id', hidden: true },
           { name: 'Itm_Co', index: 'Itm_Co', width: 100, align: 'center' },
        ],
        pager: "#pieGrdEditSol-Items",
        viewrecords: true,
        autowidth: true,
        height: 130,
        rowNum: 5,
        rowList: [5, 10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Items',
        multiselect: true,
        onSelectAll: function () {
        },
        onSelectRow: function (id, isSelected) {
        },
        loadComplete: function (data) {
            var myGrid = $("#gvEditSol-Items");
            $("#cb_" + myGrid[0].id).hide();

            var allRecords = $("#gvEditSol-Items").getGridParam('records');
            var rowNum = $("#gvEditSol-Items").getGridParam('rowNum');
            var page = $("#gvEditSol-Items").getGridParam('page');
            var rowArray = $("#gvEditSol-Items").jqGrid('getDataIDs');
            var filas;
            if ((rowNum * page) > allRecords) {
                for (var i = (rowNum * (page - 1)) + 1 ; i <= allRecords ; i++) {
                    if ($("#gvEditSol-Items").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO')
                        if ($("#gvEditSol-Items").getLocalRow(i).Solicitud_Id === idSolicitud) {
                            $("#jqg_gvEditSol-Items_" + i.toString()).prop('checked', true);
                        }
                        else{
                            $("#jqg_gvEditSol-Items_"+ i.toString()).prop('checked', true);
                            $("#jqg_gvEditSol-Items_" + i.toString()).attr("disabled", true);
                        }
                    }
                }
            else {
                for (var i = (rowNum * (page - 1)) + 1 ; i <= (rowNum * page) ; i++) {
                    if ($("#gvEditSol-Items").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO')
                        if ($("#gvEditSol-Items").getLocalRow(i).Solicitud_Id === idSolicitud) {
                            $("#jqg_gvEditSol-Items_" + i.toString()).prop('checked', true);
                        }
                        else{
                            $("#jqg_gvEditSol-Items_"+ i.toString()).prop('checked', true);
                            $("#jqg_gvEditSol-Items_" + i.toString()).attr("disabled", true);
                        }
                }
            }
        },
        rowattr: function (item, currentObj, rowId) {
            if (item.RDe_Estado_Tx === 'SOLICITADO' && item.Solicitud_Id != idSolicitud) {
                return { "class": "ui-state-disabled ui-jqgrid-disablePointerEvents" };
            }
            
        },
        beforeSelectRow: function (rowid, e) {
            var idSol = idSolicitud;
            var idSolDet = $("#gvEditSol-Items").getLocalRow(rowid).Solicitud_Detalle_Id;
            var idRqDet = $("#gvEditSol-Items").getLocalRow(rowid).Requerimiento_Detalle_Id;
            var estado = $("#jqg_gvEditSol-Items_" + rowid).prop('checked');
            //alert("idSol " + idSol + " / idSolDet " + idSolDet + " / idRqDet " + idRqDet);
            /*if (idSol >= 0 && idSolDet >= 0) {*/
            //alert($("#jqg_gvEditSol-Items_" + rowid).prop('checked'));
            if (estado) {
                ConfirmacionMensaje("Confirmacion", "¿Desea agregar el item a la Solicitud?", function () {
                    EditarItem(idSol, idSolDet, idRqDet);
                }, function () {
                    $("#jqg_gvEditSol-Items_" + rowid).prop('checked', false);
                });
            } else {
                ConfirmacionMensaje("Confirmacion", "¿Desea eliminar el item de la Solicitud?", function () {
                    EditarItem(idSol, idSolDet, idRqDet);
                }, function () {
                    $("#jqg_gvEditSol-Items_" + rowid).prop('checked', true);
                });
            }
                
            
            
            
            /*if ($(e.target).closest("tr.jqgrow").hasClass("ui-state-disabled")) {
                return false;   // not allow select the row
            }
            return true;    // allow select the row*/
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    }).navGrid('#pieGrdEditSol-Items', { edit: false, add: false, del: false, search: false, refresh: false })
    
}

function CargarLstEditProv(Solicitud_Id) {
    ObjSolicitudPrv = {
        Solicitud_Id: Solicitud_Id
    }

    $("#EditSol-ProvsSolRQ").html('<table id="gvGrdEditProvsSolRQ"></table><div id="pieGrdEditProvsSolRQ"></div>');
    $("#gvGrdEditProvsSolRQ").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarProveedorSolicitud',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjSolicitudPrv:" + JSON.stringify(ObjSolicitudPrv) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', '', '', 'Nombre', '', 'Tipo Persona', 'Direccion', 'Telefono', ''],
        colModel: [
           { name: 'Solicitud_Prov_Id', index: 'Solicitud_Prov_Id', hidden: true },
           { name: 'Solicitud_Id', index: 'Solicitud_Id', hidden: true },
           { name: 'Proveedor_Id', index: 'Proveedor_Id', hidden: true },
           { name: 'Prv_Tx', index: 'Prv_Tx', width: 350, align: 'center' },
           { name: 'Tipopersona_Id', index: 'TipoPersona_Tx', hidden: true },
           { name: 'TipoPersona_Tx', index: 'Tipopersona_Id', width: 80, align: 'center' },
           { name: 'Prv_Direccion_tx', index: 'Prv_Direccion_tx', width: 80, align: 'center' },
           { name: 'Prv_Telefono1_nu', index: 'Prv_Telefono1_nu', width: 120, align: 'center' },
           { name: 'Eliminar', index: 'Ver', width: 30, align: 'center', formatter: 'actionFormatterEliminar', search: false },
        ],
        pager: "#pieGrdEditProvsSolRQ",
        viewrecords: true,
        autowidth: true,
        height: 130,
        rowNum: 5,
        rowList: [5, 10, 15, 30],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Proveedores',
        //multiselect: true,
        loadComplete: function (data) { },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    }).navGrid('#pieGrdEditProvsSolRQ', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterEliminar: function (cellvalue, options, rowObject) {
            return "<img title=\"Eliminar Proveedor\" onclick=\"EliminarProveedor(" + rowObject.Solicitud_Prov_Id + ", " + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Denied_16.png\" />";
        }
    });

}

// ----------------------------------------------------------------------------------------- //


// ----------- Funciones para la asignacion de Proveedores a una Nueva Solicitud ----------- //

function MostrarGrillaLocalProv(lstProveedores) {
    
    if (lstProveedores.length === 0) {
        var objProveedor = {Lst_Proveedor: '0',};
    }
    else {
        var objProveedor = { Lst_Proveedor: lstProveedores.toString() };
    }
    $("#SEC_ProvsSolRQ").html('<table id="gvGrdProvsSolRQ"></table><div id="pieGrdProvsSolRQ"></div>');
    $("#gvGrdProvsSolRQ").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarProveedoresSeleccionados',
        datatype: "json",
        mtype: "POST",
        postData: "{objProveedor:" + JSON.stringify(objProveedor) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: [ '', 'Nombre', '', 'Tipo Persona', 'Direccion', 'Telefono', ''],
        colModel: [
           { name: 'Prv_Id', index: 'Prv_Id', hidden: true },
           { name: 'Prv_Tx', index: 'Prv_Tx', width: 350, align: 'center' },
           { name: 'Tipopersona_Id', index: 'TipoPersona_Tx', hidden: true },
           { name: 'TipoPersona_Tx', index: 'Tipopersona_Id', width: 80, align: 'center' },
           { name: 'Prv_Direccion_tx', index: 'Prv_Direccion_tx', width: 80, align: 'center' },
           { name: 'Prv_Telefono1_nu', index: 'Prv_Telefono1_nu', width: 120, align: 'center' },
           { name: 'Eliminar', index: 'Ver', width: 30, align: 'center', formatter: 'actionFormatterEliminar', search: false },
        ],
        pager: "#pieGrdProvsSolRQ",
        viewrecords: true,
        autowidth: true,
        height: 130,
        rowNum: 5,
        rowList: [5, 10, 15, 30],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Proveedores',
        //multiselect: true,
        loadComplete: function (data) { },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    }).navGrid('#pieGrdProvsSolRQ', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterEliminar: function (cellvalue, options, rowObject) {
            return "<img title=\"Eliminar Proveedor\" onclick=\"EliminarProveedorLocal(" + rowObject.Prv_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Denied_16.png\" />";
        }
    });
    for (var i = 0; i <= lstProveedores.length; i++)
        $("#SEC_ProvsSolRQ").jqGrid('addRowData', i + 1, lstProveedores[i]);
}

function InsertarProveedorSolicitud_local(IdProveedor) {
    for (var i = 0; i < ProveedorxSolicitud.length; i++)
    {
        if (IdProveedor == ProveedorxSolicitud[i])
        {
            return 0;
        }    
    }
    ProveedorxSolicitud.push(IdProveedor);
    MostrarGrillaLocalProv(ProveedorxSolicitud);
    return 1;
}

function EliminarProveedorLocal(IdProveedor) {
    ProveedorxSolicitud.remove(IdProveedor);
    MostrarGrillaLocalProv(ProveedorxSolicitud);
}

// ----------------------------------------------------------------------------------------- //


// ------------------------------------------- OTROS --------------------------------------- //
/*function ListarProyectos() {
    //var proveedor = {};
    jQuery("#jQGridDemo").jqGrid({
        url: '../../Logistica/Compras/SolicitudCompra.aspx/ListarProyectos',
        datatype: "json",
        mtype: "POST",
        serializeGridData: function (postData) {
            return JSON.stringify(postData);
        },
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Codigo', 'EmpresaId', 'Razon Social', 'Nombre', 'Descripcion', 'Fecha Desde', 'Fecha Hasta', 'Tipo Moneda', 'Monto',''],
        colModel: [
           { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 75, align: 'center', key: true },
           { name: 'Empresa_Id', index: 'Empresa_Id', width: 75, align: 'center', key: true },
           { name: 'Emp_RazonSocial_Tx', index: 'Emp_RazonSocial_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pry_Nombre_Tx', index: 'Pry_Nombre_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pry_Descripcion_Tx', index: 'Pry_Descripcion_Tx', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pry_Desde_Fe', index: 'Pry_Desde_Fe', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pry_Hasta_Fe', index: 'Pry_Hasta_Fe', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Moneda_Id', index: 'Moneda_Id', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Pry_Monto_Nu', index: 'Pry_Monto_Nu', width: 150, align: 'center', sortable: false, editable: true },
           { name: 'Presupuesto', index: 'Presupuesto', width: 30, align: 'center', formatter: 'actionFormatterPresupuesto', search: false }
        ],
        pager: "#jqGridPager",
        sortname: 'Proyecto_Id',
        sortorder: 'asc',
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "Proyecto_Id"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Lista Proyectos',
        loadComplete: function (result) {
            //alert("ASDASD");
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        },
    });
    $.extend($.fn.fmatter, {
        actionFormatterPresupuesto: function (cellvalue, options, rowObject) {
            return "<img title=\"Click ir al presupuesto\" onclick=\"IrPresupuesto(this)\" style=\"cursor:pointer\" src='/Scripts/JQuery-ui-1.11.4/images/Window-Enter-icon.png' />";
        }
    });
    
}


function IrPresupuesto(obj) {
    //$("#dialog-form").dialog('option', 'title', 'Editar Area');
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#jQGridDemo").getRowData(RowId);

    var id = rowData.Proyecto_Id;
    //$("#hfCodigoArea").val(rowData.AreaId);
    //$("#txtNombreRegistro").val(rowData.Nombre);
    //$("#txtDescripcionRegistro").val(rowData.Descripcion);
    //$("#dialog-form").dialog("open");
    window.location.href = "../Compras/SolicitudCompraPresupuesto.aspx?Proyecto_Id=" + id;
}*/
