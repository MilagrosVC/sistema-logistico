﻿$(document).ready(function () {        
    if ($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val() != '') {
        ObtenerDatosRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
        ListarItemsRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
    } else {
        /* -------------------------------- FECHA ACTUAL ------------------------------------------------*/
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
        var currentDate = fullDate.getDate() + "-" + twoDigitMonth + "-" + fullDate.getFullYear();
        $('#lblFechaRequerimiento').text(currentDate);
    }
    InicializarControles();
});

function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                    }
            }
    return ret;
}

function SeleccionarItem(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvGrillaItems").getRowData(RowId);

    $("#ContentPlaceHolder1_hdf_Item_Id").val(rowData.Item_Id);
    $('#lblItemDescripcion').text(rowData.Descripcion);
    $('#lblItemCodigo').text(rowData.Codigo);
    $("#Contenedor_Items").hide();
    $("#td_Codigo").show();
    $("#txtCodigo").val(rowData.Codigo);
    AbrirPopUpItemRegistro();
    $("#dialog_Items").dialog('close');
}

function CrearRequerimientoCabecera(registro) {

    var obj = {};
    if (registro == 'nuevo') {
        obj.Req_Ingreso = 'nuevo';
    } else {
        obj.Req_Ingreso = 'otro';
    }
    obj.Solicitante_Id = $("#ContentPlaceHolder1_hdf_Usuario").val();
    obj.Centro_Costo_Id = 1;
    obj.Req_Entrega_Fe = Controlar_Valores("txtFechaEntrega", "datetime");
    //obj.Req_Estado_Id = 8;
    obj.Req_Tipo_Id = $("#ddlTipoServicioRQ").val();
    if ($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val() == '') {
        obj.Requerimiento_Id = 0;
    } else {
        obj.Requerimiento_Id = $('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val();
    }
    obj.Item_Id = $("#ContentPlaceHolder1_hdf_Item_Id").val();
    obj.RDe_Cantidad_Nu = $("#txtCantidad").val();
    obj.RDe_Descripcion_Tx = $("#txtDescripcion").val();
    obj.RDe_Observacion_Tx = $("#txtObservacion").val();
    obj.Unidad_Medida_Id = $("#ddlUndMedidaRQ").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdf_Usuario").val();
    obj.Proyecto_Id = $("#ContentPlaceHolder1_hdf_Proyecto_Id").val();
    obj.Turno_Id = $('#ddlTurno').val();
    obj.Partida_Id = $('#ddlPartida').val();
    obj.IMa_Id = $("#ContentPlaceHolder1_hdf_ItemMa_Id").val();

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Requerimiento.aspx/CrearRequerimientoCabecera',
        dataType: "json",
        data: "{ObjRequerimientoCa:" + JSON.stringify(obj) + "}",
        success: function (data) {
            // alert(data.d);
            if (data.d != null) {
                if ($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val() == '') {
                    //SE REGISTRA CABECERA Y EL 1ER DETALLE
                    $('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val(data.d);
                    $('#ddlPartida').attr("disabled", "disabled");
                    ListarItemsRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
                }
                else {
                    //SE REGISTRAN SOLO LOS DETALLES
                    ListarItemsRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
                }
                $("#dialogRequerimiento").dialog("close");
                $("#dialog_Items").dialog("close");
                $('#ddlTurno').attr("disabled", "disabled");
                $('#ddlTipoServicioRQ').attr("disabled", "disabled");
                $("#txtFechaEntrega").datepicker("option", "disabled", true);
            }
        }
    });
}

function ConfirmacionEliminarItem(obj) {
    ConfirmacionMensaje("Confirmación", "¿Está seguro que Eliminar el registro seleccionado?", function () {
        EliminarItem(obj);
    });
}

function EliminarItem(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvReqDetalle").getRowData(RowId);

    var ObjRequerimientoDe = {
        Requerimiento_Detalle_Id: rowData.Requerimiento_Detalle_Id,
        Aud_UsuarioEliminacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val()
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Requerimiento.aspx/EliminarRqDetalle',
        dataType: "json",
        data: "{ObjRequerimientoDe:" + JSON.stringify(ObjRequerimientoDe) + "}",
        success: function (data) {
            // alert(data.d);
            if (data.d != null) {

            }
        }
    });
    ListarItemsRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
}

function LimpiarCombo(Control) {
    $("#" + Control)[0].options.length = 0;
    $("<option value='0'>SELECCIONE</option>").appendTo("#" + Control);
    $("#ComboBox_" + Control).val('0');
    $("#" + Control).change();
}

function CrearPopPupRequerimiento() {    

    $("#dialogRequerimiento").dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        height: 'auto',
        width: 'auto',
        modal: true,
        show: {
            effect: "blind",
            duration: 1000
        },
        buttons: {
            "Aceptar": function () {
                if ($("#txtCantidad").val() == '') {
                    Mensaje("Aviso", "Ingrese la Cantidad");
                    return;
                }
                if ($("#txtDescripcion").val() == '') {
                    Mensaje("Aviso", "Ingrese la Descripcion del Item");
                    return;
                }
                if ($("#ddlUndMedida").val() == 0) {
                    Mensaje("Aviso", "Seleccione la UND");
                    return;
                }

                if ($('#lblDescripcionItemMa').text() == '' || $('#lblFamilia').text() == '' || $('#lblMarca').text() == '') {
                    Mensaje("Aviso", "Debe Seleccionar un Item");
                    return;
                }
                if ($("#ContentPlaceHolder1_hdf_Requerimiento_Detalle_Id").val() != '') {
                    ConfirmacionMensaje("Confirmación", "¿Desea actualizar el item?", function () { ActualizarRequerimientoDe() });
                }
                else if ($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val() == '') {
                    ConfirmacionMensaje("Confirmación", "¿Desea registrar un nuevo requerimiento?", function () { CrearRequerimientoCabecera('nuevo') });
                } else {
                    ConfirmacionMensaje("Confirmación", "¿Desea agregar este item al requerimiento?", function () { CrearRequerimientoCabecera('') });
                }

            },
            "Cancelar": function () {
                $(this).dialog("close");
                $("#Contenedor_Items").hide();
            }
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
        }
    });
}

function ObtenerDatosRequerimiento(idRequerimiento) {
    var ObjRequerimientoCa = {
        Requerimiento_Id: idRequerimiento
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Requerimiento.aspx/ObtenerDatosRequerimiento',
        dataType: "json",
        data: "{ObjRequerimientoCa:" + JSON.stringify(ObjRequerimientoCa) + "}",
        success: function (data) {
            if (data.d != null) {

                var FechaEntrega = ConvertFechaFormatoNormal(data.d.Req_Entrega_Fe);
                var fecha = FechaEntrega.split("-");
                var fechaeditar = fecha[0] + '/' + fecha[1] + "/" + fecha[2];
                $('#txtFechaEntrega').val(fechaeditar);
                $('#lblFechaRequerimiento').text(ConvertFechaFormatoNormal(data.d.Aud_Creacion_Fe));
                $('#ddlTurno').val(data.d.Turno_Id);
                $('#ddlTipoServicioRQ').val(data.d.Req_Tipo_Id);
                $('#ddlPartida').val(data.d.Partida_Id);
                $('#ddlPartida').attr("disabled", "disabled");
                CargarComboSubPartida(data.d.Partida_Id);
                ListarItemsRequerimiento(idRequerimiento);                
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de Requerimiento");
            }
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function ListarItemsRequerimiento(codRequerimiento) {
    var ObjRequerimientoDe = {
        Requerimiento_Id: codRequerimiento
    };

    $("#divReqDetalle").html('<table id="gvReqDetalle"></table><div id="pieReqDetalle"></div>');
    $("#gvReqDetalle").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarRequerimientoDetalle',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjRequerimientoDe:" + JSON.stringify(ObjRequerimientoDe) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', '', 'Item', 'Codigo Item', 'Nombre Item', '', 'Padre Id', 'Descripcion Prov', 'Descripcion', 'Cantidad', 'Unidad_Medida_Id', 'UND', 'Observacion', 'Item_Id', 'IMa_Id'],
        colModel: [
           { name: 'Editar', index: 'Editar', width: 30, align: 'center', formatter: 'actionFormatterEditar', search: false },
           { name: 'Eliminar', index: 'Eliminar', width: 30, align: 'center', formatter: 'actionFormatterEliminar', search: false },
           { name: 'NumItem', index: 'NumItem', width: 55, align: 'center' },
           { name: 'Itm_Co', index: 'Itm_Co', width: 100, align: 'center' },
           { name: 'Itm_Nombre_Tx', index: 'Itm_Nombre_Tx', width: 250, align: 'center' },
           { name: 'Requerimiento_Detalle_Id', index: 'Requerimiento_Detalle_Id', hidden: true },
           { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 200, align: 'center', hidden: true },
           { name: 'RDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 300, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center' },
           { name: 'RDe_Cantidad_Nu', index: 'RDe_Cantidad_Nu', width: 80, align: 'center' },
           { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', width: 100, align: 'center', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 80, align: 'center' },
           { name: 'RDe_Observacion_Tx', index: 'RDe_Observacion_Tx', width: 400, align: 'center', hidden: true },
           { name: 'Item_Id', index: 'Item_Id', width: 200, align: 'center', hidden: true },
           { name: 'IMa_Id', index: 'IMa_Id', width: 200, align: 'center', hidden: true }  
        ],
        height: 'auto',
        pager: "#pieReqDetalle",
        viewrecords: true,
        autowidth: true,
        //width: 1335,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'DETALLE DE REQUERIMIENTO',
        loadComplete: function (result) {
            $("#GrillaItemsReq").slideDown("slow", function () {
            });
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            return "<i title=\"Editar Item\" onclick=\"EditarItem(this)\" style=\"cursor:pointer;color:#222;\" class='fa fa-pencil-square-o fa-2x'></i>";
        },
        actionFormatterEliminar: function (cellvalue, options, rowObject) {
            return "<img title=\"Eliminar Item\" onclick=\"ConfirmacionEliminarItem(this)\" style=\"cursor:pointer\" src='../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Denied_16.png' />";
        }
    });
}

function EditarItem(obj) {
    $("#dialogRequerimiento").dialog('option', 'title', 'EDITAR ITEM');
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvReqDetalle").getRowData(RowId);
    
    $("#txtCantidad").val(rowData.RDe_Cantidad_Nu);
    $("#txtDescripcion").val(rowData.RDe_Descripcion_Tx);
    $("#txtObservacion").val(rowData.RDe_Observacion_Tx);
    $("#ddlUndMedidaRQ").val(rowData.Unidad_Medida_Id);
    $("#ComboBox_ddlUndMedidaRQ").val($("#ddlUndMedidaRQ option:selected").text());
    $('#lblItemDescripcion').text(rowData.Itm_Nombre_Tx);
    $('#lblItemCodigo').text(rowData.Itm_Co);
    $("#ContentPlaceHolder1_hdf_Item_Id").val(rowData.Item_Id);
    $("#ContentPlaceHolder1_hdf_Requerimiento_Detalle_Id").val(rowData.Requerimiento_Detalle_Id);
    $("#ContentPlaceHolder1_hdf_ItemMa_Id").val(rowData.IMa_Id);
    $("#td_Codigo").hide();
    $("#dialogRequerimiento").dialog('open');
}

function ActualizarRequerimientoCa(EstadoRq) {
    var ObjRequerimientoCa = {
        Requerimiento_Id: $('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val(),
        Solicitante_Id: $('#ContentPlaceHolder1_hdf_Usuario').val(),
        Req_Estado_Id: EstadoRq,
        Req_Tipo_Id: $('#ddlTipoServicioRQ').val(),
        Req_Entrega_Fe: Controlar_Valores("txtFechaEntrega", "datetime"),
        Aud_UsuarioActualizacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val(),
        Turno_Id: $('#ddlTurno').val()        
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Requerimiento.aspx/ActualizarRequerimientoCa',
        dataType: "json",
        data: "{ObjRequerimientoCa:" + JSON.stringify(ObjRequerimientoCa) + "}",
        success: function (data) {
            if (data.d != null) {
                if (data.d == 1) {
                    if (EstadoRq == 1) {
                        Mensaje("Aviso", "El requerimiento fue solicitado para ser atentido", function () { window.location.href = '../../Logistica/Compras/RequerimientoUsuario.aspx' });
                    } else {
                        Mensaje("Aviso", "El requerimiento se mantiene en estado parcial hasta su cierre", function () { window.location.href = '../../Logistica/Compras/RequerimientoUsuario.aspx' });
                    }
                }
            }
        }
    });
}

function ActualizarRequerimientoDe() {

    var ObjRequerimientoDe = {
        Requerimiento_Detalle_Id: $("#ContentPlaceHolder1_hdf_Requerimiento_Detalle_Id").val(),
        Item_Id: $("#ContentPlaceHolder1_hdf_Item_Id").val(),
        RDe_Cantidad_Nu: $("#txtCantidad").val(),
        RDe_Descripcion_Tx: $("#txtDescripcion").val(),
        RDe_Observacion_Tx: $("#txtObservacion").val(),
        Unidad_Medida_Id: $("#ddlUndMedidaRQ").val(),
        IMa_Id: $("#ContentPlaceHolder1_hdf_ItemMa_Id").val(),
        Aud_UsuarioActualizacion_Id: $("#ContentPlaceHolder1_hdf_Usuario").val()
        };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Requerimiento.aspx/ActualizarRequerimientoDe',
        dataType: "json",
        data: "{ObjRequerimientoDe:" + JSON.stringify(ObjRequerimientoDe) + "}",
        success: function (data) {
            if (data.d != null) {
                if (data.d == 1) {                    
                    Mensaje("Aviso", "El registro fue actualizado con exito");
                    $("#dialogRequerimiento").dialog("close");
                    $("#ContentPlaceHolder1_hdf_Requerimiento_Detalle_Id").val('');
                    ListarItemsRequerimiento($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val());
                }
            }
        }
    });

}

function EstadoParcial() {
    setTimeout(function () {
        window.location.href = '../../Logistica/Compras/RequerimientoUsuario.aspx';
    }, 1500);
}

function InicializarControles() {

    /*-------------------------   LLENADO DE HIDDENS   ---------------------------------------*/
    //$('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val('');
    /*----------------------------------------------------------------------------------------*/

    /*-------------------------   ESTABLECER FECHA ACTUAL   ----------------------------------*/
    
    /*-----------------------------------------------------------------------------------------*/
    $("#tbIngreso").append("<div id='GrillaItemsReq' style='max-width:90%;padding-left:5px;padding-bottom:1%;'><br /><div id='divReqDetalle'></div><br /></div>");

    $("#td_Codigo").append("<label style='padding-right:16%;'>Codigo :</label><input type='text' name='txtCodigo' class='inputs' id='txtCodigo' style='height:30px;width:150px;display:inline-block;' /><img id='ImgAgregar' src='../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/agregar.png' alt='Agregar' title='Agrega Item' />");

    $("#tdSubPartida").append("<i id='ImgMostarItems' title='Mostrar Items' class='fa fa-eye' style='cursor:pointer;padding-left:10px;' ></i>");
    
    $("#txtFechaEntrega").datepicker({
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        onClose: function (selectedDate) {
        }
    });

    CrearPopPupRequerimiento();
    CargarComboProyecto1($('#ContentPlaceHolder1_hdf_Proyecto_Id').val());
    CargarComboPartida($('#ContentPlaceHolder1_hdf_Presupuesto_Id').val());
    CargarComboTurno();
    CargarComboFamilia("#ddlFamilia");
    CargarComboMarca("#ddlMarca");

    $("#td_Codigo").hide();
    $("#td_ItemsMa").hide();
    $("#Contenedor_Items").hide();
    $("#ImgMostarItems").hide();
    $("#GrillaItemsReq").hide();
    $('#ddlSubPartida').attr("disabled", "disabled");
    $('#txtCodigo').attr("disabled", "disabled");
    $('#txtFechaEntrega').attr("disabled", "disabled");    

    $('#ddlPartida').change(function () {
        if ($('#ddlPartida').val() == 0 || $('#ddlPartida').val() == '' || $('#ddlPartida').val() == null) {
            $("#ImgMostarItems").hide();
            $("#td_Codigo").hide();
            LimpiarCombo('ddlSubPartida');
            $('#ddlSubPartida').attr("disabled", "disabled");
        } else {
            CargarComboSubPartida($('#ddlPartida').val());
        }
    });

    $('#ddlSubPartida').change(function () {
        if ($('#ddlSubPartida').val() == 0 || $('#ddlSubPartida').val() == '' || $('#ddlSubPartida').val() == null) {
            $("#ImgMostarItems").hide();
            $("#td_Codigo").hide();
            $("#Contenedor_Items").hide();
        } else {
            $("#td_Codigo").hide();
            ListarItemsSubItems();
        }
    });

    $('#ddlFamilia').change(function () {
        if ($('#ddlFamilia').val() == 0 || $('#ddlFamilia').val() == '' || $('#ddlFamilia').val() == null) {
            $('#ddlMarca').prop("disabled", true);
            $("#ddlMarca").val(0);
        } else {            
            $('#ddlMarca').prop("disabled",false);
        }
    });    

    $("#GrillaItemsReq").append("<input class='boton' type='button' id='btnFinalizar' name='btnFinalizar' value='Cerrar Rq' />");
    $("#GrillaItemsReq").append("<input class='boton' type='button' id='btnParcial' name='btnParcial' value='Guardar Rq' />");

    $("#btnFinalizar").click(function () {
        ConfirmacionMensaje("Confirmación", "¿Desea cerrar el registro del requerimiento?", function () { ActualizarRequerimientoCa(1) });
    });

    $("#btnParcial").click(function () {
        ConfirmacionMensaje("Confirmación", "¿Desea guardar el registro del requerimiento en un estado parcial?", function () { ActualizarRequerimientoCa(2) });
    });

    $("#ImgAgregar").click(function () {
        AbrirPopUpItemRegistro();
    });

    $("#btnMostrarItemsMa").click(function () {
        ListarItems_Ma();
        CrearPopPupItemsMa();
    });
        
    $("#btnLimpiarItemMA").click(function () {
        LimpiarItems_Ma();
    });
        
    $("#btnFiltrar").click(function () {
        ListarItems_Ma();
    });

    $("#ImgOcultar").click(function () {
        $("#Contenedor_Items").slideUp("slow", function () {
        });
    });

    $("#ImgMostarItems").click(function () {
        if ($('#ddlSubPartida').val() == 0 || $('#ddlSubPartida').val() == '' || $('#ddlSubPartida').val() == null) {
            Mensaje("Aviso", "Seleccione una Partida");
            return;
        }        
        ListarItemsSubItems();
    });

    $("#btnVolver").click(function () {
        if ($('#ContentPlaceHolder1_hdf_RequerimientoCabecera_Id').val() != '') {
            Mensaje("Aviso", "Ya tiene un registro debera cerrar el requerimiento o dejarlo en estado parcial");
        } else {
            window.location = BASE_URL + '/Logistica/Compras/RequerimientoUsuario.aspx';
        }        
    });    
}

function CargarComboProyecto1(codEmpresa) {
    //alert('function CargarComboProyecto(codEmpresa) { desde  requerimiento.aspx');
    var ObjProyecto = {
        Empresa_Id : codEmpresa
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarProyectoCombo',
        data: "{ObjProyecto:" + JSON.stringify(ObjProyecto) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#lblProyecto').empty();
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    $('#lblProyecto').text(value.Pry_Nombre_Tx);
                    $('#lblDireccion').text(value.Pry_Direccion_Tx);
                });
            }            
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboPartida(codPresupuesto) {

    var ObjPartida = {
        Presupuesto_Id: codPresupuesto
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarPartidaCombo',
        data: "{ObjPartida:" + JSON.stringify(ObjPartida) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlPartida').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Prt_Nombre_Tx + "' value='" + value.Partida_Id + "'>" + value.Prt_Nombre_Tx + "</option>");
                });
            }
            $('#ddlPartida').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboSubPartida(codPartida) {

    var ObjSubPartida = {
        Partida_Id: codPartida
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarSubPartidaCombo',
        data: "{ObjSubPartida:" + JSON.stringify(ObjSubPartida) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlSubPartida').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.SPa_Nombre_Tx + "' value='" + value.Sub_Partida_Id + "'>" + value.SPa_Nombre_Tx + "</option>");
                });
            }
            $('#ddlSubPartida').attr("disabled", false);
            $('#ddlSubPartida').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboItem(codSubPartida) {

    var ObjItem = {
        Sub_Partida_Id: codSubPartida
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarItemCombo',
        data: "{ObjItem:" + JSON.stringify(ObjItem) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlItem').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Itm_Nombre_Tx + "' value='" + value.Item_Id + "'>" + value.Itm_Nombre_Tx + "</option>");
                });
            }
            $('#ddlItem').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboTurno() {

    var ObjTurno = {
        //Turno_Id: codTurno
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarTurnoCombo',
        data: "{ObjTurno:" + JSON.stringify(ObjTurno) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlTurno').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Tur_Nombre_Tx + "' value='" + value.Turno_Id + "'>" + value.Tur_Nombre_Tx + "</option>");
                });
            }
            $('#ddlTurno').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function ListarItemsSubItems() {
    var objItem = {
        Sub_Partida_Id: $('#ddlSubPartida').val()
    };
    
    $("#GrillaItems").html('<table id="gvGrillaItems"></table><div id="pieGrillaItems"></div>');
    $("#gvGrillaItems").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarItemsSubItems',
        datatype: "json",
        mtype: "POST",
        postData: "{objItem:" + JSON.stringify(objItem) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Codigo', 'Descripcion', 'UND', 'Metrado', 'Precio', 'Parcial', 'Item_Id'],
        colModel: [
           { name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
           { name: 'Codigo', index: 'Codigo', width: 150, align: 'center' },
           { name: 'Descripcion', index: 'Descripcion', width: 400, align: 'center' },
           { name: 'UND', index: 'UND', width: 150, align: 'center', hidden: true },
           { name: 'Metrado', index: 'Metrado', width: 150, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Precio', index: 'Precio', width: 150, editable:true, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Parcial', index: 'Parcial', width: 150, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Item_Id', index: 'Item_Id', width: 150, align: 'center', hidden: true }           
        ],
        pager: "#pieGrillaItems",
        autowidth: true,
        viewrecords: true,
        cellEdit: true,
        //width: 'auto',
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        //caption: 'Bandeja de Items',
        loadComplete: function (result) {
            if ($("#txtFechaEntrega").val() == '') {
                Mensaje("Aviso", "Ingrese la Fecha de Entrega");
                $('#ddlSubPartida').val(0);
                return;
            }
            else if ($('#ddlTurno').val() == 0) {
                Mensaje("Aviso", "Seleccione el turno");
                $('#ddlSubPartida').val(0);
                return;
            } else {
                CrearPopPupItems();
                $("#ImgMostarItems").show();
            }
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
            return "<i title=\"Seleccionar Item\" onclick=\"SeleccionarItem(this)\" style=\"cursor:pointer\" class='fa fa-plus-square'></i>"
        }
    });
}

function CrearPopPupItems() {
    $("#dialog_Items").dialog({
        autoOpen: false,
        resizable: false,
        draggable: true,
        height: 'auto',
        width: 'auto',
        modal: true,
        show: {
            effect: "fade",
            duration: 1000
        },
        buttons: {
            /*"Aceptar": function () {
                },
            "Cancelar": function () {
                $(this).dialog("close");
            }*/
        },
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
    $("#dialog_Items").dialog('open');
}

function AbrirPopUpItemRegistro() {
    $("#txtCantidad").val('');
    $("#txtDescripcion").val('');
    $("#txtObservacion").val('');
    $("#ddlUndMedidaRQ").val(0);
    $('#lblDescripcionItemMa').text('');
    $('#lblFamilia').text('');
    $('#lblMarca').text('');
    $("#td_ItemsMa").hide();
    $("#dialogRequerimiento").dialog('option', 'title', 'REGISTRAR ITEM');
    $("#dialogRequerimiento").dialog('open');
}

function CargarComboFamilia(control) {

    var ObjFamilia = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarFamiliaCombo',
        data: "{ObjFamilia:" + JSON.stringify(ObjFamilia) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Fam_Nombre_Tx + "' value='" + value.Familia_Id + "'>" + value.Fam_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboMarca(control) {

    var ObjMarca = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarMarcaCombo',
        data: "{ObjMarca:" + JSON.stringify(ObjMarca) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Mar_Nombre_Tx + "' value='" + value.Marca_Id + "'>" + value.Mar_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function ListarItems_Ma() {
    var ObjItem_Ma = {
        Familia_Id: $('#ddlFamilia').val(),
        Marca_Id: $('#ddlMarca').val(),
        Aud_Estado_Fl: true
    };

    $("#divItemsMa").html('<table id="gvItemsMa"></table><div id="pieItemsMa"></div>');
    $("#gvItemsMa").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/Requerimiento.aspx/ListarItemsMa',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjItem_Ma:" + JSON.stringify(ObjItem_Ma) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Codigo', 'Descripcion', 'Unidad Medida', 'Metrado', 'Familia', 'Marca', 'IMa_Id','Estado'],
        colModel: [
           { name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
           { name: 'IMa_Co', index: 'IMa_Co', width: 120, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center' },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 120, align: 'center' },
           { name: 'IMa_Metrado_Nu', index: 'IMa_Metrado_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Fam_Nombre_Tx', index: 'Fam_Nombre_Tx', width: 120, align: 'center' },
           { name: 'Mar_Nombre_Tx', index: 'Mar_Nombre_Tx', width: 150, align: 'center',  },
           { name: 'IMa_Id', index: 'IMa_Id', width: 150, align: 'center', hidden: true },
        { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },
           
        ],
        pager: "#pieItemsMa",
        //autowidth: true,
        viewrecords: true,
        width: 'auto',
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Items',
        loadComplete: function (result) {
        },

        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
            return "<i title=\"Seleccionar Item\" onclick=\"SeleccionarItemMa(this)\" style=\"cursor:pointer\" class='fa fa-plus-square'></i>"
        }
    });
}

function CrearPopPupItemsMa() {
    $("#dialog_ItemsMa").dialog({
        autoOpen: false,
        resizable: false,
        draggable: true,
        height: 'auto',
        width: 'auto',
        modal: true,
        show: {
            effect: "fade",
            duration: 1000
        },
        buttons: {
            /*"Aceptar": function () {
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }*/
        },
        open: function (event, ui) {
        }
    });
    $("#dialog_ItemsMa").dialog('open');
}

function SeleccionarItemMa(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvItemsMa").getRowData(RowId);

    $("#ContentPlaceHolder1_hdf_ItemMa_Id").val(rowData.IMa_Id);
    $('#lblDescripcionItemMa').text(rowData.IMa_Descripcion_Tx);
    $('#lblFamilia').text(rowData.Fam_Nombre_Tx);
    $('#lblMarca').text(rowData.Mar_Nombre_Tx);
    $("#dialog_ItemsMa").dialog('close');
    $("#td_ItemsMa").show();
}

function LimpiarItems_Ma() {
    $('#ddlFamilia').val(0);
    $('#ddlMarca').val(0);
    ListarItems_Ma();
}
