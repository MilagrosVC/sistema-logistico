﻿/// <reference path="../../../../Logistica/Compras/BandejaCotizacion.aspx" />
/// <reference path="../../../../Logistica/Compras/BandejaCotizacion.aspx" />
/// <reference path="../../../../Logistica/Compras/BandejaCotizacion.aspx" />

var creada = $("#ContentPlaceHolder1_hdfCreada").val();//36;
var ganadora = $("#ContentPlaceHolder1_hdfGanadora").val();//37;
var Desaprobada = $("#ContentPlaceHolder1_hdfDesaprobada").val();//38;
var OCGenerada = $("#ContentPlaceHolder1_hdfOCGenerada").val(); //39;
var Enviada_OC = $("#ContentPlaceHolder1_hdfEnviada_OC").val(); //50;
var Enviada_SA = $("#ContentPlaceHolder1_hdfEnviada_SA").val(); //51;
var DenegadaA = $("#ContentPlaceHolder1_hdfDenegadaA").val(); //63;
var Desaprobada_excede_presupuesto = $("#ContentPlaceHolder1_hdfDesaprobada_excede_presupuesto").val(); //64;

//alert('Creada : ' + creada + '    Ganadora : ' + ganadora + '   Desaprobada : ' + Desaprobada + '   OCGenerada : ' + OCGenerada + '   Enviada_OC : ' + Enviada_OC + '   Enviada_SA : ' + Enviada_SA + '   DenegadaA : ' + DenegadaA + '      Desaprobada_excede_presupuesto : ' + Desaprobada_excede_presupuesto);

frstChrg_BandejaCo = true;

$(document).ready(function () {
    InicializarControles();
    $("#btnBuscar").click(function () {
        ListarCotizacionesCa();
    });
    $("#btnLimpiar").click(function () {
        LimpiarFiltrosBusqueda();
    });
    $("#btnImprimirCo").click(function () {
        $("#divImprimirCo").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: BASE_URL + "/Scripts/Styles_SEC/SEC_BandCotizacion.css"
        });
    });


    $("#btnCerrarVerCo").click(function () {
        $("#dvVerCotizacion").dialog('close');
    });

    $("#btnExportCo").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;

        var a = document.createElement('a');

        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divImprimirCo');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'Cotizacion_' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
});

function InicializarControles() {
    ListarCotizacionesCa();
}

function ListarCotizacionesCa() {
    var CoCreadas=0;
    var CoGanadoras = 0;
    var CoDesaprobadas = 0;
    var CoGeneradas = 0;
    var CoEnviadasOC = 0;
    var CoReqAnticipos = 0;
    var CoAnticipoDenegados = 0;
    var CoExcedePre = 0;
    var CoTotales;

    var objCotizacionCa = {
        Requerimiento_Co: $("#txtCodRequerimiento").val(),
        Cot_NumProforma_Nu: $("#txtCodProforma").val(),

        Proveedor_Id: $("#ddlProveedorCo").val(),
        Cot_Estado_Id: $("#ddlEstadoCo").val()
    };

    $("#CotizacionesGrid").html('<table id="gvCotizacionesGrid"></table><div id="pieCotizacionesGrid"></div>');
    $("#gvCotizacionesGrid").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/BandejaCotizacion.aspx/ListarCotizaciones',
        datatype: "json",
        mtype: "POST",
        postData: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Detalle', 'Accion', 'Proyecto_Id', 'Requerimiento_Id', 'Cod Reque', 'Cod. Proforma', 'Cod Solic.', 'Cod Cotiz.', '', 'Nom Proveedor', 'N° Items', 'Monto', 'IGV', 'Monto total', 'Detraccion', '', 'Estado', 'F. Creacion'],
        colModel: [
             { name: 'Detalle', index: 'Detalle', width: 40, align: 'center', formatter: 'actionFormatterDetalle', search: false },
              { name: 'Accion', index: 'Accion', width: 30, align: 'center', formatter: 'actionFormatterAccion', search: false },
             { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 50, align: 'center', hidden: true },
             { name: 'Requerimiento_Id', index: 'Requerimiento_Id', width: 50, align: 'center', hidden: true },
             { name: 'Requerimiento_Co', index: 'Requerimiento_Co', width: 50, align: 'center' },
             { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu', width: 50, align: 'center' },
           { name: 'Solicitud_Id', index: 'Solicitud_Id', width: 40, align: 'center', hidden: true },
           { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 40, align: 'center' ,hidden: true },
           { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 50, align: 'center', hidden: true },
           { name: 'Cot_Proveedor_Tx', index: 'Cot_Proveedor_Tx', width: 150, align: 'center' },
           { name: 'Cot_Cant_Items', index: 'Cot_Cant_Items', width: 35, align: 'center' },
           { name: 'Cot_Monto_Nu', index: 'Cot_Monto_Nu', width: 40, align: 'center' },
           { name: 'Cot_IGV_Nu', index: 'Cot_IGV_Nu', width: 35, align: 'center' },
           { name: 'Cot_MontoTotal_Nu', index: 'Cot_MontoTotal_Nu', width: 55, align: 'center' },
           { name: 'Cot_Detraccion_Nu', index: 'Cot_Detraccion_Nu', width: 45, align: 'center' },
           { name: 'Cot_Estado_Id', index: 'Cot_Estado_Id', hidden: true },
           { name: 'Cot_Estado_Tx', index: 'Cot_Estado_Tx', width: 100, align: 'center' },
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'ISO8601Long', newformat: 'd-m-Y  H:i  A' } },

        ],

        pager: "#pieCotizacionesGrid",
        viewrecords: true,
        width: 1325,//1200,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Cotizaciones',
        loadComplete: function (data) {

            var ids = $("#gvCotizacionesGrid").getDataIDs();
            for (var i = 0; i < ids.length ; i++) {
                var rowId = ids[i];
                var rowdata = $("#gvCotizacionesGrid").jqGrid("getRowData", rowId);
                if (rowdata.Cot_Estado_Id == creada) {
                    $("#" + rowId).find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                }
                else if (rowdata.Cot_Estado_Id == DenegadaA) {
                    $("#" + rowId).find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });

                }
                else if (rowdata.Cot_Estado_Id == Desaprobada_excede_presupuesto) {
                    $("#" + rowId).find("td").css({ 'background-color': '#FFE9E9', 'font-weight': 'bold' });

                }
            }

            //console.log(data.d);
            var allRecords = $("#gvCotizacionesGrid").getGridParam('records');

            if (allRecords > 0 && frstChrg_BandejaCo === true) {
                for (var i = 0; i < allRecords; i++) {
                    //alert('Creada ID ' + creada);
                    //alert('Data CO ' + data.d[i].Cot_Estado_Id);

                    if (data.d[i].Cot_Estado_Id == creada) {
                        //alert('Creadadlll' + creada);
                        CoCreadas = CoCreadas + 1;
                    } else
                        if (data.d[i].Cot_Estado_Id == ganadora) {
                            CoGanadoras = CoGanadoras + 1;
                    } else
                        if (data.d[i].Cot_Estado_Id == Desaprobada) {
                            CoDesaprobadas = CoDesaprobadas + 1;
                    } else 
                        if (data.d[i].Cot_Estado_Id == OCGenerada) {
                            CoGeneradas = CoGeneradas + 1;
                    } else
                        if (data.d[i].Cot_Estado_Id == Enviada_OC) {
                            CoEnviadasOC = CoEnviadasOC + 1;
                   } else
                        if (data.d[i].Cot_Estado_Id == Enviada_SA) {
                            CoReqAnticipos = CoReqAnticipos + 1;
                   } else
                        if (data.d[i].Cot_Estado_Id == DenegadaA) {
                            CoAnticipoDenegados = CoAnticipoDenegados + 1;
                   } else
                        if (data.d[i].Cot_Estado_Id == Desaprobada_excede_presupuesto) {
                            CoExcedePre = CoExcedePre + 1;
                    }
                }



                $("#CoTotal").html(allRecords);
                $("#CoCreada").html(CoCreadas);
                $("#CoGanadora").html(CoGanadoras);
                $("#CoDesaprobada").html(CoDesaprobadas);
                $("#CoGenerada").html(CoGeneradas);
                $("#CoEnviadaOC").html(CoEnviadasOC);
                $("#CoReqAnticipo").html(CoReqAnticipos);
                $("#CoAnticipoDenegado").html(CoAnticipoDenegados);
                $("#CoExcedePre").html(CoExcedePre);
                frstChrg_BandejaCo = false;
                //console.log("allRecords: " + allRecords + " / RqsNuevos: " + RqsNuevos + " / RqsAprb: " + RqsAprb + " / RqsDesprb: " + RqsDesprb);
            }



        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            alert(xhr.responseText);
        }
    });

    $.extend($.fn.fmatter, {
        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            return "<img title=\"Visualizar Detalle\" onclick=\"CargarFormularioDetalle(" + rowObject.Cotizacion_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        }

    });

    $.extend($.fn.fmatter, {
        actionFormatterAccion: function (cellvalue, options, rowObject) {
            //return "<i title=\"Visualizar Detalle\" onclick=\"CargarFormularioFactura(" + rowObject.Orden_Compra_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
            if (rowObject.Cot_Estado_Id == creada) {

                return "<i title=\"Realizar Accion\" onclick=\"CargarFormularioAccion(this)\" style=\"cursor:pointer\" class='fa fa-external-link'></i>"
            }
            else {
                return '<i title=\"No se permite ejecutar accion\" style=\"cursor:pointer\" class="fa fa-minus-square-o"></i>';
            }
        }
    });
}

function LimpiarFiltrosBusqueda() {
    $("#txtCodRequerimiento").val("");
    $("#txtCodProforma").val("");
    $("#ddlProveedorCo").val(0);
    $("#ddlEstadoCo").val(0);
}



function CargarFormularioDetalle(Cotizacion_Id) {

    //alert('Id cotizacion  :  ' + Cotizacion_Id);
    var igv = "";
    var subtotal = "";
    var detraccion = "";
    var total = "";

    CodigoCotizacion = {
        Cotizacion_Id: Cotizacion_Id
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/BandejaCotizacion.aspx/GenerarVistaCotizacionCA',
        dataType: "json",
        data: "{objCotizacionCa:" + JSON.stringify(CodigoCotizacion) + "}",
        success: function (data) {
            if (data.d != null) {

                $("#lblCo-CodCotizacion").html("Proforma N°: " + data.d.Cot_NumProforma_Nu);

                var FechaEmision = ConvertFechaFormatoNormal(data.d.Cot_FechaEmision_Fe);
                $("#lblCo-Fecha").html("Fecha: " + FechaEmision);
                
                $("#lblCo-ReceptorNombre").html(data.d.Emp_RazonSocial_Tx);
                $("#lblCo-ReceptorDireccion").html(data.d.Emp_Direccion_tx);
                $("#lblCo-ReceptorRUC").html(data.d.Emp_DocIdent_RUC_nu);
                $("#lblCo-Empresa_RZ").html(data.d.Cot_Proveedor_Tx);


                $("#lblCo-EmisorMoneda").html(data.d.Mon_Nombre_Tx);
                $("#lblCo-EmisorRUC").html(data.d.Prv_DocIdent_nu);
                $("#lblCo-EmisorCondPago").html(data.d.Cot_Forma_Pago_Tx);

                $("#lblCo-EmisorVendedorNom").html(data.d.Cot_Prv_NomVendedor_Tx);
                $("#lblCo-EmisorVendedorCel").html(data.d.Cot_Prv_TelVendedor_Nu);
                $("#lblCo-EmisorVendedorMail").html(data.d.Cot_Prv_MailVendedor_Tx);
                
                igv = data.d.Cot_IGV_Nu;
                subtotal = data.d.Cot_Monto_Nu;
                total = data.d.Cot_MontoTotal_Nu;
                detraccion = data.d.Cot_Detraccion_Nu;

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/BandejaCotizacion.aspx/GenerarVistaCotizacionDetalle',
                    dataType: "json",
                    data: "{ObjCotizacionDe:" + JSON.stringify(CodigoCotizacion) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].SDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].SD_UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].SDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].CDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].CDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbCo-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbCo-Detalle").append(fila);
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";

                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Detraccion</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(detraccion).toFixed(5) + "</td></tr>";

                                $("#tbCo-Detalle").append(fila);
                            } else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].SDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].SD_UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].SDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].CDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].CDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbCo-Detalle").append(fila);
                                }
                            }
                        } else {
                            Mensaje("Aviso", "No existen items en la Cotizacion");
                        }
                    }
                });

            } else {
                Mensaje("Aviso", "No se puede generar vista de la Cotizacion");
            }
        }
    });

    $("#dvVerCotizacion").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "COTIZACION",
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbCo-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:50px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:300px;border: 1px solid #000;'>ARTICULO</th><th style='width:150px;border: 1px solid #000;'>PRECIO UND</th><th style='width:150px;border: 1px solid #000;'>TOTAL</th></tr>");
            $("#tbCo-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");
           

        }
    });
}




function CargarFormularioAccion(obj) {

    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvCotizacionesGrid").getRowData(RowId);

    //alert('Codigo Proforma  :  ' + rowData.Cot_NumProforma_Nu);
    //window.location = BASE_URL + '/Logistica/Compras/CotizacionRequerimiento.aspx?Cotizacion_Id=' + rowData.Cotizacion_Id;



    window.location = BASE_URL + '/Logistica/Compras/CotizacionRequerimiento.aspx?Cotizacion_Id=' + rowData.Cotizacion_Id +
                                                                   '&Proyecto_Id=' + rowData.Proyecto_Id +
                                                                   '&Requerimiento_Id=' + rowData.Requerimiento_Id +
                                                                   '&Solicitud_Id=' + rowData.Solicitud_Id;

}