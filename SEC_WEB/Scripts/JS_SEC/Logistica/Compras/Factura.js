﻿var Aprobada = 68;

$(document).ready(function () {
    IniciarControles();
    if ($("#ContentPlaceHolder1_hdf_Orden_Compra_Co").val() == '' || $("#ContentPlaceHolder1_hdf_Orden_Compra_Co").val() == null) {
        GenerarBandeja('');
    } else {
        GenerarBandeja($("#ContentPlaceHolder1_hdf_Orden_Compra_Co").val());
    }
});

function IniciarControles() {

    $('#divGenerarFactura').hide();
    $("#txtCodOrdenCompra").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 45 || e.which > 45)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $("#txtNumero").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 45 || e.which > 45)) {
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });

    $('#txtNumero').focusout(function () {
        if ($('#txtNumero').val() == '') {
            /*OK*/
        } else {
            ValidarFactura($('#ContentPlaceHolder1_hdf_Proveedor_Id').val(), $('#txtNumero').val());
        }
    });

    $('#txtFechaFactura').datepicker({
        dateFormat: "yy/mm/dd",
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        onSelect: function () {
        }
    });

    $("#btnRegistrar").click(function () {
        if ($('#txtNumero').val().trim() == '') {
            Mensaje("Aviso", "Debe ingresar el Número de Factura");
            return;
        }
        if ($('#txtFechaFactura').val().trim() == '') {
            Mensaje("Aviso", "Debe ingresar Fecha");
            return;
        }
        ConfirmacionMensaje("Confirmación", "¿Seguro desea registrar la factura?", function () {
            RegistrarFactura();
        });
    });

    $("#btnCancelar").click(function () {
        $("#divGenerarFactura").dialog('close');
    });

    $("#btnBuscar").click(function () {
        if ($('#txtCodOrdenCompra').val().trim() == '') {
            Mensaje("Aviso", "Debe ingresar el Número de la Orden de Compra");
            return;
        } else {
            GenerarBandeja($("#txtCodOrdenCompra").val());
        }
    });

    $("#btnLimpiar").click(function () {
        $('#txtCodOrdenCompra').val('');
        $("#divGenerarFactura").fadeOut("slow", function () {
            // Animation complete.
        });
        $("#ContentPlaceHolder1_hdf_Orden_Compra_Co").val('');
        GenerarBandeja($("#txtCodOrdenCompra").val());
    });

    $("#btnBandejaFac").click(function () {
        window.location = '../../Logistica/Compras/BandejaFactura.aspx';
    });
    $("#btnBandejaOC").click(function () {
        window.location = '../../Logistica/Compras/BandejaOrdenCompra.aspx';
    });
}

function ObtenerOrdenCompra(obj) {
    
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvOrdenCompra").getRowData(RowId);
    ordencompra = {
        Orden_Compra_Co: rowData.Orden_Compra_Co
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Factura.aspx/ObtenerOrdenCompraCa',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ordencompra) + "}",
        success: function (data) {
            if (data.d != null) {
                if (data.d.Orden_Compra_Id == 0) {
                    var resultado = "No existe la Orden de Compra N° " + "<b>" + CodOrdenCompra + "</b>";
                    Mensaje("Aviso", resultado);
                    return;
                } else {
                    $('#ContentPlaceHolder1_hdf_Orden_Compra_Id').val(data.d.Orden_Compra_Id);
                    $('#ContentPlaceHolder1_hdf_Proveedor_Id').val(data.d.Proveedor_Id);
                    $('#ContentPlaceHolder1_hdf_Moneda_Id').val(data.d.Moneda_Id);
                    $('#ContentPlaceHolder1_hdf_Com_IGV_Nu').val(data.d.OCC_IGV_Nu);
                    $('#ContentPlaceHolder1_hdf_Com_SubTotal_Nu').val(data.d.OCC_SubTotal_Nu);
                    $('#ContentPlaceHolder1_hdf_Com_Total_Nu').val(data.d.OCC_Total_Nu);
                    VisualizarOrdenCompra(data.d.Orden_Compra_Id);
                }
            }
        }
    });

    $("#divGenerarFactura").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "Registro de Factura",
        buttons: {},
    });
}

function VisualizarOrdenCompra(idOrdenCompra) {
    var igv = "", subtotal = "";
    var total = "";
    ordencompra = {
        Orden_Compra_Id: idOrdenCompra
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Factura.aspx/GenerarVistaOrdenCompraCa',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ordencompra) + "}",
        success: function (data) {
            if (data.d != null) {
                $('#txtNumero').val('');
                $('#txtFechaFactura').val('');
                $('#txtGuiaRemision').val('');
                $("#divGenerarFactura").fadeIn("slow", function () {
                    // Animation complete
                });
                $("#lblCom-CodOrdenCompra").html("N°: " + data.d.Orden_Compra_Co);                    
                $("#lblCom-RazonSocial").html(data.d.Prv_RazonSocial_Tx);
                $("#lblCom-Direccion").html(data.d.Prv_Direccion_tx);                    
                $("#lblCom-RUC").html(data.d.Prv_DocIdent_nu);
                $("#lblCom-Ciudad").html(data.d.Distrito_Tx);
                igv = data.d.OCC_IGV_Nu;
                subtotal = data.d.OCC_SubTotal_Nu;
                total = data.d.OCC_Total_Nu;


                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/Factura.aspx/GenerarVistaOrdenCompraDe',
                    dataType: "json",
                    data: "{ObjOrdenCompraDe:" + JSON.stringify(ordencompra) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].OCDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].OCDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbCom-Detalle").append(fila);
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";
                                $("#tbCom-Detalle").append(fila);
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].OCDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbCom-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            alert("No existen items en la Orden de Compra");
                        }
                    }
                });
            }
            else {
                alert("No se puede generar vista de la Orden de Compra");
            }
        }
    });
    $("#tbCom-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:50px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:300px;border: 1px solid #000;'>ARTICULO</th><th style='width:150px;border: 1px solid #000;'>PRECIO UND</th><th style='width:150px;border: 1px solid #000;'>TOTAL</th></tr>");
    $("#tbCom-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");

    }

function RegistrarFactura() {

        var ObjComprobanteCa = {
            Orden_Compra_Id: $('#ContentPlaceHolder1_hdf_Orden_Compra_Id').val(),
            Proveedor_Id: $('#ContentPlaceHolder1_hdf_Proveedor_Id').val(),
            Moneda_Id: $('#ContentPlaceHolder1_hdf_Moneda_Id').val(),
            Com_IGV_Nu: $('#ContentPlaceHolder1_hdf_Com_IGV_Nu').val(),
            Com_SubTotal_Nu: $('#ContentPlaceHolder1_hdf_Com_SubTotal_Nu').val(),
            Com_Total_Nu: $('#ContentPlaceHolder1_hdf_Com_Total_Nu').val(),
            Comprobante_Co: $('#txtNumero').val(),
            Com_Guia_Remision_Nu: $('#txtGuiaRemision').val(),
            Com_Emision_Fe: $('#txtFechaFactura').val(),
            Aud_UsuarioCreacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val()
        };

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Compras/Factura.aspx/CrearComprobanteCabecera',
            dataType: "json",
            data: "{ObjComprobanteCa:" + JSON.stringify(ObjComprobanteCa) + "}",
            success: function (data) {
                if (data.d != null) {
                    $('#divGenerarFactura').hide();
                    Mensaje("Aviso", "Se genero con éxito la factura");
                    setTimeout(function () {
                        window.location.href = BASE_URL + '/Logistica/Compras/BandejaFactura.aspx';
                    }, 1500);
                }
            }
        });
}

function ValidarFactura(CodProveedor,CodFactura) {
    comprobante = {
        Proveedor_Id: CodProveedor,
        Comprobante_Co: CodFactura
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/Factura.aspx/ExisteFactura',
        dataType: "json",
        data: "{ObjComprobanteCa:" + JSON.stringify(comprobante) + "}",
        success: function (data) {
            if (data.d != null) {
                if (data.d.Comprobante_Id == 0) {
                } else {
                    MensajeFocus("Aviso", "La Factura con el N° " + "<b>" + CodFactura + "</b>" + " ya existe", "txtNumero");
                    $('#txtNumero').val('');
                    return;
                }
            }
        }
    });
}

function GenerarBandeja(codigo) {
    
    var ObjOrdenCompraCa = {
        OCC_Estado_Id: Aprobada,
        Orden_Compra_Co: codigo//$("#txtCodOrdenCompra").val(),
    };

    $("#divOrdenCompra").html('<table id="gvOrdenCompra"></table><div id="pieOrdenCompra"></div>');
    $("#gvOrdenCompra").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/Factura.aspx/ListarOrdenCompra',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjOrdenCompraCa:" + JSON.stringify(ObjOrdenCompraCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Codigo', 'N° Orden Compra', 'N° Proforma', 'Proveedor Id', 'Razon Social', 'Gestionador', 'Cot_Tipo_Id', 'Tipo', 'Cot_Estado_Id', 'Estado', 'Fecha Emision', 'Moneda_Id', 'Moneda', 'Cotizacion_Id', 'penalidad', 'Sub Total', 'IGV', 'Total'],
        colModel: [
            { name: 'Detalle', index: 'Detalle', width: 30, align: 'center', formatter: 'actionFormatterDetalle', search: false },
           { name: 'Orden_Compra_Id', index: 'Orden_Compra_Id', width: 75, align: 'center', hidden: true },
           { name: 'Orden_Compra_Co', index: 'Orden_Compra_Co', width: 100, align: 'center' },
           { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu', width: 100, align: 'center' },
           { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 150, align: 'center', hidden: true },
           { name: 'Prv_RazonSocial_Tx', index: 'Prv_RazonSocial_Tx', width: 250, align: 'center' },
           { name: 'OCC_Gestionador_Tx', index: 'OCC_Gestionador_Tx', width: 150, align: 'center', hidden: true },
           { name: 'OCC_Tipo_Id', index: 'OCC_Tipo_Id', width: 150, align: 'center', hidden: true },
           { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 150, align: 'center' },//, hidden: true
           { name: 'OCC_Estado_Id', index: 'OCC_Estado_Id', width: 150, align: 'center', hidden: true },
           { name: 'Est_Nombre_Tx', index: 'Est_Nombre_Tx', width: 150, align: 'center' },
           { name: 'OCC_Emision_Fe', index: 'OCC_Emision_Fe', width: 120, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Moneda_Id', index: 'Moneda_Id', width: 150, align: 'center', hidden: true },
           { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 150, align: 'center' },
           { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 150, align: 'center', hidden: true }, //, hidden: true 
           { name: 'OCC_Penalidad_Fl', index: 'OCC_Penalidad_Fl', width: 150, align: 'center', hidden: true },
           { name: 'OCC_SubTotal_Nu', index: 'OCC_SubTotal_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'OCC_IGV_Nu', index: 'OCC_IGV_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'OCC_Total_Nu', index: 'OCC_Total_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } }
        ],
        pager: "#pieOrdenCompra",
        viewrecords: true,
        autowidth: true,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Ordenes de Compra',
        loadComplete: function (result) {

        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            return "<img title=\"Visualizar Detalle\" onclick=\"ObtenerOrdenCompra(this)\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
            //
        }
    });
}