﻿var Cot_Anticipo_FL = true;

$(document).ready(function () {    
    IniciarControles();
});

function IniciarControles() {

    /*---------------------------------   LLENADO DE HIDDENS   -----------------------------------------*/
    $('#ContentPlaceHolder1_hdf_OrdenCompraCabecera_Id').val('');
    $('#ContentPlaceHolder1_hdf_hdf_Cotizacion_Id').val('');
    /*--------------------------------------------------------------------------------------------------*/

    ListarCotizaciones();
    $('#txtCodigo').attr("disabled", "disabled");
    $('#txtFechaEntrega').attr("disabled", "disabled");
    $('#txtTotal').attr("disabled", "disabled");
    $('#txtIGV').attr("disabled", "disabled");
    $('#txtSubTotal').attr("disabled", "disabled");

    $("#cbPenalidad").change(function () {
        if ($(this).is(':checked') == true) {
            $("#txtPenalidad").slideDown("slow", function () {
            });
        } else {
            $("#txtPenalidad").slideUp("slow", function () {
                $("#txtPenalidad").val('');
            });
        }
    });

    $('#btnVolver').click(function () {
        window.location = '../../Logistica/Compras/BandejaOrdenCompra.aspx';
    });

    $('#ddlMonedaOC').change(function () {
        if ($('#ddlMonedaOC').val() == 1) {
            $('#txtTipoCambio').val('');
            $('#txtTipoCambio').attr("disabled", "disabled");
        } else {
            $('#txtTipoCambio').val('');
            $('#txtTipoCambio').attr("disabled", false);
        }
    });

    $('#txtTotal').focusout(function () {
        if ($('#txtTotal').val() == '') {
            $('#txtTotal').val('');
        } else {
            this.value = parseFloat(this.value).toFixed(5);
        }
    });

    $('#txtTipoCambio').focusout(function () {
        if ($('#txtTipoCambio').val() == '') {
            $('#txtTipoCambio').val('');
        } else {
            this.value = parseFloat(this.value).toFixed(5);
        }
    });

    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $("#btnLimpiar").click(function () {
        Limpiar();
    });

    $("#btnBuscar").click(function () {
        ListarCotizaciones();
    });

    $('#txtFecha').datepicker({
        dateFormat: "dd/mm/yy",
        showOn: "button",
        buttonImage: BASE_URL + '/Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        onSelect: function () {
        }
    });

    $("#btnImprimirOC").click(function () {
        $("#divImprimirOC").printThis({
            importCSS: true,
            importStyle: false,
            printContainer: true,
            loadCSS: BASE_URL + "/Scripts/Styles_SEC/SEC_Orden_Compra.css"
        });
    });

    $("#btnCerrarVerOC").click(function () {
        $("#dvVerOrdenCompra").dialog('close');
    });
    
    $("#btnExport").click(function (e) {
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var segs = dt.getSeconds();

        var dateNom = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" + segs;

        var a = document.createElement('a');

        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('divImprimirOC');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        //Definiendo nombre del archivo
        a.download = 'Orden_Compra_' + dateNom + '.xls';

        a.click();
        e.preventDefault();
    });
}

function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                    }
            }

    return ret;
}

function ListarCotizaciones() {

    var proveedor = ($('#ddlProveedorOC').val() != null) ? $('#ddlProveedorOC').val() : 0;
    var objCotizacionCa = {
        Proveedor_Id: proveedor,
        Cot_Estado_Id: 50 // ESTADO DE ENVIADO ORDEN DE COMPRA
    };

    $("#divCotizacion").html('<table id="gvCotizacion"></table><div id="pieCotizacion"></div>');
    $("#gvCotizacion").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/OrdenCompra.aspx/ListarCotizacion',
        datatype: "json",
        mtype: "POST",
        postData: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', '','Codigo', 'N° Proforma', 'Solicitud Id', 'Proveedor Id', 'Razon Social', 'Gestionador', 'Cot_Tipo_Id', 'Tipo', 'Cot_Estado_Id', 'Estado', 'Fecha Emision', 'Cot_Forma_Pago_Id', '', 'Cot_Moneda_Id', 'Moneda', 'Cot_Anticipo_FL', 'Sub Total', 'IGV', 'Total'],
        colModel: [
           { name: 'Anticipo', index: 'Anticipo', width: 30, align: 'center', formatter: 'actionFormatterAnticipo', search: false },
           { name: 'Generar', index: 'Generar', width: 30, align: 'center', formatter: 'actionFormatterGenerar', search: false },
           { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 75, align: 'center',hidden: true },
           { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu', width: 100, align: 'center' },
           { name: 'Solicitud_Id', index: 'Solicitud_Id', width: 75, align: 'center',hidden:true },
           { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Proveedor_Tx', index: 'Cot_Proveedor_Tx', width: 250, align: 'center' },
           { name: 'Cot_Prv_NomVendedor_Tx', index: 'Cot_Prv_NomVendedor_Tx', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Tipo_Id', index: 'Cot_Tipo_Id', width: 150, align: 'center', hidden: true },
           { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 150, align: 'center',hidden:true },
           { name: 'Cot_Estado_Id', index: 'Cot_Estado_Id', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Estado_Tx', index: 'Cot_Estado_Tx', width: 150, align: 'center' },
           { name: 'Cot_FechaEmision_Fe', index: 'Cot_FechaEmision_Fe', width: 120, align: 'center', formatter: 'date', formatoptions: { srcformat: 'd/m/Y', newformat: 'd/m/Y' } },
           { name: 'Cot_Forma_Pago_Id', index: 'Cot_Forma_Pago_Id', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Forma_Pago_Tx', index: 'Cot_Forma_Pago_Tx', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Moneda_Id', index: 'Cot_Moneda_Id', width: 150, align: 'center', hidden: true },
           { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Anticipo_FL', index: 'Cot_Anticipo_FL', width: 150, align: 'center', hidden: true },
           { name: 'Cot_Monto_Nu', index: 'Cot_Monto_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Cot_IGV_Nu', index: 'Cot_IGV_Nu', width: 100, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'Cot_MontoTotal_Nu', index: 'Cot_MontoTotal_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } }          
        ],
        pager: "#pieCotizacion",
        viewrecords: true,
        autowidth: true,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Cotizaciones',
        loadComplete: function (result) {

        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });    
    $.extend($.fn.fmatter, {
        actionFormatterAnticipo: function (cellvalue, options, rowObject) {
            if (rowObject.Cot_Anticipo_FL == true) {
                return "<i title=\"Tiene Anticipo\" class='fa fa-money' style=\"cursor:pointer\" ></i>"
            }
            return "<i title=\"Credito\" class='fa fa-credit-card' style=\"cursor:pointer\" ></i>"
        },
        actionFormatterGenerar: function (cellvalue, options, rowObject) {
            if (rowObject.Cot_Estado_Id == 39) {
                return "<i onclick=\"MensajeGrilla(this)\" class='fa fa-check' style=\"cursor:pointer\" ></i>"
            }
            return "<i title=\"Generar Orden de Compra\" onclick=\"GenerarOrden(this)\" class='fa fa-check' style=\"cursor:pointer\" ></i>"
        }
    });
}

function SeleccionarCotizacion(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvCotizacion").getRowData(RowId);   

    alert('una accion');
}

function GenerarOrden(obj) {
    $("#ddlTipoOrdenOC").val(0);
    $("#ddlFormaPagoOC").val(0);
    $("#ddlMonedaOC").val(0);

    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvCotizacion").getRowData(RowId);
    $('#ContentPlaceHolder1_hdf_Cotizacion_Id').val(rowData.Cotizacion_Id);
    CargarDatosProveedor(rowData.Proveedor_Id);
    ListarCotizacionDetalle(rowData.Cotizacion_Id);   
    if (rowData.Cot_Moneda_Id == 1) {
        $('#txtTipoCambio').val('');
        $('#txtTipoCambio').attr("disabled", "disabled");
    } else {
        $('#txtTipoCambio').val('');
        $('#txtTipoCambio').attr("disabled", false);
    }
    $('#lblMoneda').text(rowData.Mon_Nombre_Tx);
    $('#ContentPlaceHolder1_hdf_Moneda_Id').val(rowData.Cot_Moneda_Id);
    $('#lblFormaPago').text(rowData.Cot_Forma_Pago_Tx);
    $('#ContentPlaceHolder1_hdf_Forma_Pago_Id').val(rowData.Cot_Forma_Pago_Id);
    $('#lblIGV').text(rowData.Cot_IGV_Nu);
    $('#lblSubTotal').text(rowData.Cot_Monto_Nu);
    $('#lblTotal').text(rowData.Cot_MontoTotal_Nu);
    Cot_Anticipo_FL = rowData.Cot_Anticipo_FL;
    CrearPopPupOrdenCompra(rowData.Proveedor_Id);
}

function MensajeGrilla(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvCotizacion").getRowData(RowId);
    Mensaje("Aviso", "La Cotizacion con el N° " + rowData.Cotizacion_Id + " ya tiene una orden de compra generada");
} 

function CrearOrdenCompraCabecera(codProveedor) {
    
    var penalidad_fl = false;
    var penalidad_nu = 0;
    var estadoOC = 0;
    if ($('#cbPenalidad').is(':checked') == true) {
        penalidad_fl = true;
        penalidad_nu = $('#txtPenalidad').val();
    } else {
        penalidad_fl = false;
        penalidad_nu = 0;
    }

    var ObjOrdenCompraCa = {
        Cotizacion_Id: $('#ContentPlaceHolder1_hdf_Cotizacion_Id').val(),
        Proveedor_Id: codProveedor,
        OCC_Gestionador_Tx: $('#ContentPlaceHolder1_hdf_Usuario').val(),
        OCC_Tipo_Id: $("#ddlTipoOrdenOC").val(),
        OCC_Estado_Id: 12,//ESTADO CREADO
        //OCC_Plazo_Entrega_Id:
        OCC_Forma_Pago_Id: $('#ContentPlaceHolder1_hdf_Forma_Pago_Id').val(),
        Moneda_Id: $('#ContentPlaceHolder1_hdf_Moneda_Id').val(),   
        //OCC_Tipo_Documento_Entrega:
        //OCC_Tipo_Documento_Cancelacion:
       
        OCC_Penalidad_Fl: penalidad_fl,
        OCC_Total_Nu: $("#lblTotal").text(),
        OCC_Emision_Fe: Controlar_Valores("txtFecha","datetime"),//$("#txtFecha").val(),
        //OCC_Tipo_Cambio: $("#txtTipoCambio").val(),
        //Centro_Costo_Id:
        OCC_Penalidad_Nu: penalidad_nu,
        OCC_IGV_Nu: $("#lblIGV").text(),
        OCC_SubTotal_Nu: $("#lblSubTotal").text(),
        Aud_UsuarioCreacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val()
    };


    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/CrearOrdenCompraCabecera',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ObjOrdenCompraCa) + "}",
        success: function (data) {
            // alert(data.d);
            if (data.d != null) {
                $('#ContentPlaceHolder1_hdf_OrdenCompraCabecera_Id').val(data.d);
                CambiarEstadoCotizacionCabecera();                
                ListarCotizaciones();
                ActualizarOrdenCompra(data.d);
                $('#dialog_OrdenCompra').dialog("close");
                VisualizarOrdenCompra(data.d);
            }
            
        }

    });
}

function CrearPopPupOrdenCompra(codProveedor) {
    $("#dialog_OrdenCompra").dialog({
        autoOpen: false,
        resizable: false,
        draggable: false,
        height: 'auto',
        width: 'auto',
        modal: true,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "clip",
            duration: 1000
        },
        buttons: {
            "Aceptar": function () {
                if ($('#txtFecha').val() == '') {
                    Mensaje("Aviso","Seleccione la Fecha de la Orden de Compra");
                    return;
                }
                else if ($("#ddlTipoOrdenOC").val() == 0) {
                    Mensaje("Aviso","Seleccione el Tipo de Orden");
                    return;
                }else if ($("#ddlFormaPagoOC").val() == 0) {
                    Mensaje("Aviso","Seleccione el Forma de Pago");
                    return;
                }else if ($("#ddlMonedaOC").val() == 0) {
                    Mensaje("Aviso","Seleccione el Tipo de Moneda");
                    return;
                } else if ($('#txtTotal').val() == '') {
                    Mensaje("Aviso","Ingrese el Total de la Orden de Compra");
                    return;
                } else {
                    ConfirmacionMensaje("Confirmación", "Desea generar la orden de compra", function (){CrearOrdenCompraCabecera(codProveedor)});                     
                }    
            },
            "Cancelar": function () {
                $(this).dialog("close");
            }            
        },
        open: function (event, ui) {
            $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
            $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
        }
    });
    $("#dialog_OrdenCompra").dialog('open');
}

function CargarDatosProveedor(codProveedor) {

    var ObjProveedor = {
        Prv_Id: codProveedor
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/OrdenCompra.aspx/CargaDatosProveedor',
        data: "{ObjProveedor:" + JSON.stringify(ObjProveedor) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#lblRazonSocial').empty();
            $('#lblDomicilio').empty();
            $('#lblCuidad').empty();
            $('#lblTelefono').empty();
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    $('#lblRazonSocial').text(value.Prv_RazonSocial_Tx);
                    $('#lblDomicilio').text(value.Prv_Direccion_tx);
                    $('#lblCuidad').text(value.Distrito_Tx);
                    $('#lblTelefono').text(value.Prv_Telefono1_nu);                    
                });
            }
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function ListarCotizacionDetalle(codCotizacion) {
    var objCotizacionDe = {
        Cotizacion_Id: codCotizacion
    };

    $("#divCotizacionDe").html('<table id="gvCotizacionDe"></table><div id="pieCotizacionDe"></div>');
    $("#gvCotizacionDe").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/OrdenCompra.aspx/ListarCotizacionDetalle',
        datatype: "json",
        mtype: "POST",
        postData: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Codigo', 'Cotizacion_Id', 'Item_Id', 'Descripcion', 'Cantidad', 'Precio Uni.', 'Total'],
        colModel: [
           { name: 'Cotizacion_Detalle_Id', index: 'Cotizacion_Detalle_Id', width: 70, align: 'center' },           
           { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 75, align: 'center', hidden: true },
           { name: 'Item_Id', index: 'Item_Id', width: 75, align: 'center', hidden: true },
           { name: 'CDe_Descripcion_Tx', index: 'CDe_Descripcion_Tx', width: 450, align: 'center' },
           { name: 'CDe_Cantidad_Nu', index: 'CDe_Cantidad_Nu', width: 100, align: 'center' },
           { name: 'CDe_Precio_Unitario_Nu', index: 'CDe_Precio_Unitario_Nu', width: 100, align: 'center', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } },
           { name: 'CDe_Sub_Total_Nu', index: 'CDe_Sub_Total_Nu', width: 140, align: 'center', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 } }
        ],
        pager: "#pieCotizacionDe",
        viewrecords: true,
        width: 'auto',
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja Detalle de Cotizacion',
        loadComplete: function (result) {
            
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
}

function CambiarEstadoCotizacionCabecera() {

    var ObjCotizacionCa = {
        Cotizacion_Id: $('#ContentPlaceHolder1_hdf_Cotizacion_Id').val(),
        Cot_Estado_Id: 39,
        Aud_UsuarioActualizacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val()
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/CambiarEstadoCotizacionCabecera',
        dataType: "json",
        data: "{ObjCotizacionCa:" + JSON.stringify(ObjCotizacionCa) + "}",
        success: function (data) {
            if (data.d != null) {

            }
        }
    });
}

function ActualizarOrdenCompra(IdOrdenCompra) {

    var ObjOrdenCompraCa = {
        Orden_Compra_Id: IdOrdenCompra,
        Aud_UsuarioActualizacion_Id: $('#ContentPlaceHolder1_hdf_Usuario').val(),
        Anticipo_Fl: Cot_Anticipo_FL
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/ActualizarOrdenCompra',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ObjOrdenCompraCa) + "}",
        success: function (data) {
            if (data.d != null) {

            }
        }
    });
}

function Limpiar() {
    $('#ddlProveedorOC').val(0);
    $("#ComboBox_ddlProveedorOC").val($("#ddlProveedorOC option:selected").text());
    ListarCotizaciones();
}

function VisualizarOrdenCompra(idOrdenCompra) {
    var igv = "", subtotal = "";
    var total = "";
    ordencompra = {
        Orden_Compra_Id: idOrdenCompra
    };

    cotizacion = {
        Cotizacion_Id : $('#ContentPlaceHolder1_hdf_Cotizacion_Id').val()
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/GenerarVistaOrdenCompraCa',
        dataType: "json",
        data: "{ObjOrdenCompraCa:" + JSON.stringify(ordencompra) + "}",
        success: function (data) {
            if (data.d != null) {
                $("#lblOC-CodOrdenCompra").html("N°: " + data.d.Orden_Compra_Co);
                var FechaEmision = ConvertFechaFormatoNormal(data.d.OCC_Emision_Fe);
                $("#lblOC-Fecha").html(FechaEmision);                
                $("#lblOC-RazonSocial").html(data.d.Prv_RazonSocial_Tx);
                $("#lblOC-Domicilio").html(data.d.Prv_Direccion_tx);
                $("#lblOC-Ciudad").html(data.d.Distrito_Tx);
                $("#lblOC-Telefono").html(data.d.Prv_Telefono1_nu);
                igv = data.d.OCC_IGV_Nu;
                subtotal = data.d.OCC_SubTotal_Nu;
                total = data.d.OCC_Total_Nu;                

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Logistica/Compras/OrdenCompra.aspx/GenerarVistaOrdenCompraDe',
                    dataType: "json",
                    data: "{ObjOrdenCompraDe:" + JSON.stringify(ordencompra) + "}",
                    success: function (data) {
                        var iMax = 0;
                        if (data.d.length > 0) {
                            if (data.d.length < 20) {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + parseFloat(data.d[i].OCDe_Precio_Unitario_Nu.toString()).toFixed(5) + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + parseFloat(data.d[i].OCDe_Sub_Total_Nu.toString()).toFixed(5) + "</td></tr>";

                                    $("#tbOC-Detalle").append(fila);
                                }
                                var fila = "";
                                for (var i = (data.d.length - 1) ; i < 20; i++) {
                                    fila = "<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>";
                                    $("#tbOC-Detalle").append(fila);                                    
                                }
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Subtotal</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(subtotal).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>18% IGV</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(igv).toFixed(5) + "</td></tr>";
                                fila = fila + "<tr><td colspan='4'><td style='text-align:center;font-weight:bold;border: 1px solid #000;'>Total</td><td style='text-align:right;font-weight:bold;border: 1px solid #000;'>" + parseFloat(total).toFixed(5) + "</td></tr>";
                                $("#tbOC-Detalle").append(fila);
                            }
                            else {
                                for (var i = 0; i < data.d.length; i++) {
                                    var fila = "<tr>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Cantidad_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].UMe_Simbolo_Tx + "</td>";
                                    fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].OCDe_Descripcion_Tx + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].OCDe_Precio_Unitario_Nu.toString() + "</td>";
                                    fila = fila + "<td style='text-align:right;border: 1px solid #000;'>&nbsp" + data.d[i].OCDe_Sub_Total_Nu.toString() + "</td></tr>";

                                    $("#tbOC-Detalle").append(fila);
                                }
                            }
                        }
                        else {
                            Mensaje("Aviso", "No existen items en la Orden de Compra");
                        }
                    }
                });
            }
            else {
                Mensaje("Aviso", "No se puede generar vista de la Orden de Compra");
            }
        }
    });

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/ListarEgreso',
        dataType: "json",
        data: "{ObjEgreso:" + JSON.stringify(cotizacion) + "}",
        success: function (data) {
            var iMax = 0;
            if (data.d.length > 0) {
                if (data.d.length < 20) {
                    for (var i = 0; i < data.d.length; i++) {
                        var fila = "<tr>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Moneda_Monto_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].Ege_NumeroOperacion.toString() + "</td>";
                        fila = fila + "<td colspan='2' style='text-align:center;border: 1px solid #000;'>" + data.d[i].TMP_Nombre_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Par_Nombre_Tx.toString() + "</td></tr>";

                        $("#tbOC-Anticipo").append(fila);
                    }
                }
                else {
                    for (var i = 0; i < data.d.length; i++) {
                        var fila = "<tr>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + (i + 1).toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Moneda_Monto_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:right;border: 1px solid #000;'>" + data.d[i].Ege_NumeroOperacion.toString() + "</td>";
                        fila = fila + "<td colspan='2' style='text-align:center;border: 1px solid #000;'>" + data.d[i].TMP_Nombre_Tx.toString() + "</td>";
                        fila = fila + "<td style='text-align:center;border: 1px solid #000;'>" + data.d[i].Par_Nombre_Tx.toString() + "</td></tr>";

                        $("#tbOC-Anticipo").append(fila);
                    }
                }
            }
            else {
                Mensaje("Aviso", "No existen items de Anticipo");
            }
        }
    });

    $("#dvVerOrdenCompra").dialog({
        autoOpen: true,
        show: "blind",
        width: 835,
        resizable: false,
        modal: true,
        title: "ORDEN DE COMPRA",
        buttons: {},
        open: function (event, ui) {
            $(".ui-dialog-titlebar-close", ui.dialog).hide();
            $("#tbOC-Detalle").html("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:50px;border: 1px solid #000;'>CANTIDAD</th><th style='width:60px;border: 1px solid #000;'>UND</th><th style='width:300px;border: 1px solid #000;'>ARTICULO</th><th style='width:150px;border: 1px solid #000;'>PRECIO UND</th><th style='width:150px;border: 1px solid #000;'>TOTAL</th></tr>");
            $("#tbOC-Detalle").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");
            $("#tbOC-Anticipo").html("<caption style='font-size:12px;font-weight:bold;color:#000;'>ANTICIPOS</caption>");
            $("#tbOC-Anticipo").append("<tr><th style='width:50px;border: 1px solid #000;'>ITEM</th><th style='width:120px;border: 1px solid #000;'>MONTO</th><th style='width:100px;border: 1px solid #000;'>N° OPERACIÓN</th><th colspan='2' style='width:300px;border: 1px solid #000;'>MEDIO PAGO</th><th style='width:100px;border: 1px solid #000;'>TIPO ANTICIPO</th></tr>");
            $("#tbOC-Anticipo").append("<tr><td style='border: 1px solid #000;'>&nbsp</td><td style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td><td colspan='2' style='border: 1px solid #000;'></td><td style='border: 1px solid #000;'></td></tr>");

        }
    });
}
