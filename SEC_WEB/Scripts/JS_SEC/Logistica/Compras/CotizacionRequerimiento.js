﻿function Controlar_Valores(control, tipo_dato) {
    var elemento = $("#" + control).val();
    var ret;
    if (tipo_dato == "int") {
        if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = 0; }
        else
            if (elemento != 0 && elemento != null && elemento != "") { ret = elemento; }
    }
    else
        if (tipo_dato == "string") {
            if (elemento == 0 || elemento == null || elemento == "") { ret = elemento = ""; }
            else
                if (elemento != "" && elemento != null) { ret = elemento; }
        }
        else
            if (tipo_dato == "datetime") {
                if (elemento == 0 || elemento == "") {
                    ret = elemento = "";
                }
                else
                    if (elemento != "" && elemento != null) {
                        var Fecha = elemento.split("/");
                        //alert('Fecha ' + Fecha);
                        ret = Fecha[2] + '/' + Fecha[1] + "/" + Fecha[0];
                        //$('#txtDesde').val(fechadesdeformato);
                        //alert('fechatext ' + $('#txtDesde').val());*/
                        //ret = elemento;
                        //alert('ret ' + ret);
                    }
            }

    return ret;
}

var CotEnviada = false;
var EditCotizacion = false;
var varTotalDetalleCot = 0;
var varIGV = 0.18;
var varDetraccion = 0.05;
var varEstCotGanadora = $("#ContentPlaceHolder1_hdf_EstCotGanadora").val();
var varEstCotCreada = $("#ContentPlaceHolder1_hdf_EstCotCreada").val();
//var varEstCotDet = $("#ContentPlaceHolder1_hdf_e_EstCotDet").val();
var varEstCotDetCreadaId = $("#ContentPlaceHolder1_hdf_EstCotDetCreadaId").val();;
var varEstCotDetIngresadaId = $("#ContentPlaceHolder1_hdf_EstCotDetIngresadaId").val();;

var varEstCotOCGenerada = $("#ContentPlaceHolder1_hdf_EstCotOCGenerada").val();
var varEstCotEnviada_OC = $("#ContentPlaceHolder1_hdf_EstCotEnviada_OC").val();
var varEstCotEnviada_SA = $("#ContentPlaceHolder1_hdf_EstCotEnviada_SA").val();

$(document).ready(function () {
    var idProyecto = $("#ContentPlaceHolder1_hdProyecto").val();
    var idRequerimiento = $("#ContentPlaceHolder1_hdRequerimiento").val();
    var idSolicitud = $("#ContentPlaceHolder1_hdSolicitud").val();
    $("#NumSol").html(idSolicitud);
    
    InicializarControles(idRequerimiento, idProyecto, idSolicitud);
    //cargarEstadosCotDet();
});
function ValidarCampos_RAP() {
    var isValid = true;
    var foco = 0;

    if ($('#txtMontoTotal').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtMontoTotal').focus(); }
        $('#txtMontoTotal').addClass('ui-state-error');

    } else {
        $('#txtMontoTotal').removeClass('ui-state-error');
    }

    if ($('#txtMontoSolAnt').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtMontoSolAnt').focus(); }
        $('#txtMontoSolAnt').addClass('ui-state-error');

    } else {
        $('#txtMontoSolAnt').removeClass('ui-state-error');
    }

    if ($('#ddlTipoSolicitud').val() === '0') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#ddlTipoSolicitud').focus(); }
        $('#ddlTipoSolicitud').addClass('ui-state-error');

    } else {
        $('#ddlTipoSolicitud').removeClass('ui-state-error');
    }

    return isValid;
}
function LimpiarControles() {
    $("#ddlProveedor").val(0);
    $("#ddlTipoPago").val(0);
    $("#txtProforma").val("");
    $("#txtVendedor").val("");
    $("#txtTelefonoVend").val("");
    $("#txtMailVend").val("");
    $("#ddlMoneda").val(0);
    $("#txtMonto").val("");
    $("#txtIGV").val("");
    $("#txtTotal").val("");
    $("#txtDetraccion").val("");
    $("#txtarObs").val("");
    $("#txtFechaProf").val("");

    $("#txtDetCot-PUnit").val("");
    $("#txtDetCot-PTot").val("");
    
}
function CargarComboProveedorCotizacion(control, idSolicitud) {
    var ObjSolicitudPrv = {
        Solicitud_Id: idSolicitud,
    };
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarProveedorSolicitud',
        data: "{ObjSolicitudPrv:" + JSON.stringify(ObjSolicitudPrv) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Prv_Tx + "' value='" + value.Proveedor_Id + "'>" + value.Prv_Tx + "</option>");
                });
            }
            $(control).html(sItems);
            console.log(msg.d);
        },
        error: function () {
            alert("Error!...");
        }
    });
}
function limpiarCamposRAP() {
    $("#ddlTipoSolicitud").val(0);
    $("#txtMontoTotal").val(0);
    $("#txtMontoSolAnt").val(0);
    //$("#CrearRAP").hide();

}

function HabilitarCamposRAP() {
    $("#ddlTipoSolicitud").prop('disabled', false);
    $("#txtMontoTotal").prop('disabled', false);
    $("#txtMontoSolAnt").prop('disabled', false);
    $("#CrearRAP").show();

}


function ValidarCampos_CrearCotizacion() {
    var isValid = true;
    var foco = 0;
    
    if ($('#txtFechaProf').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtFechaProf').focus(); }
        $('#txtFechaProf').addClass('ui-state-error');

    } else {
        $('#txtFechaProf').removeClass('ui-state-error');
    }

    if ($('#ddlProveedor').val() === '0') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#ddlProveedor').focus(); }
        $('#ddlProveedor').addClass('ui-state-error');

    } else {
        $('#ddlProveedor').removeClass('ui-state-error');
    }

    if ($('#ddlTipoPago').val() == '0') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#ddlTipoPago').focus(); }
        $('#ddlTipoPago').addClass('ui-state-error');

    } else {
        $('#ddlTipoPago').removeClass('ui-state-error');
    }

    if ($('#ddlMoneda').val() == '0') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#ddlTipoPago').focus(); }
        $('#ddlMoneda').addClass('ui-state-error');

    } else {
        $('#ddlMoneda').removeClass('ui-state-error');
    }

    if ($('#txtMonto').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtMonto').focus(); }
        $('#txtMonto').addClass('ui-state-error');

    } else {
        $('#txtMonto').removeClass('ui-state-error');
    }

    if ($('#txtIGV').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtIGV').focus(); }
        $('#txtIGV').addClass('ui-state-error');

    } else {
        $('#txtIGV').removeClass('ui-state-error');
    }

    if ($('#txtTotal').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtTotal').focus(); }
        $('#txtTotal').addClass('ui-state-error');

    } else {
        $('#txtTotal').removeClass('ui-state-error');
    }

    if ($('#txtDetraccion').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtDetraccion').focus(); }
        $('#txtDetraccion').addClass('ui-state-error');

    } else {
        $('#txtDetraccion').removeClass('ui-state-error');
    }

    return isValid;
}
function ValidarCampos_EditarDetalleCotizacion() {
    var isValid = true;
    var foco = 0;

    if ($('#txtDetCot-Desc').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtDetCot-Desc').focus(); }
        $('#txtDetCot-Desc').addClass('ui-state-error');

    } else {
        $('#txtDetCot-Desc').removeClass('ui-state-error');
    }

    if ($('#txtDetCot-Cant').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtDetCot-Cant').focus(); }
        $('#txtDetCot-Cant').addClass('ui-state-error');

    } else {
        $('#txtDetCot-Cant').removeClass('ui-state-error');
    }

    if ($('#txtDetCot-PUnit').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtDetCot-PUnit').focus(); }
        $('#txtDetCot-PUnit').addClass('ui-state-error');

    } else {
        $('#txtDetCot-PUnit').removeClass('ui-state-error');
    }

    if ($('#txtDetCot-PTot').val() === '') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#txtDetCot-PTot').focus(); }
        $('#txtDetCot-PTot').addClass('ui-state-error');

    } else {
        $('#txtDetCot-PTot').removeClass('ui-state-error');
    }

    if ($('#ddlDetCot-Und').val() == '0') {
        isValid = false;
        foco = foco + 1;
        if (foco == 1) { $('#ddlDetCot-Und').focus(); }
        $('#ddlDetCot-Und').addClass('ui-state-error');

    } else {
        $('#ddlDetCot-Und').removeClass('ui-state-error');
    }

    return isValid;
}
function DeshabilitarCampos() {
    $("#ddlProveedor").prop('disabled', true);
    $("#txtProforma").prop('disabled', true);
    $("#txtFechaProf").prop('disabled', true).datepicker('option', 'disabled', true);
    //$("#txtFechaProf")
    $("#txtVendedor").prop('disabled', true);
    $("#txtTelefonoVend").prop('disabled', true);
    $("#txtMailVend").prop('disabled', true);
    $("#ddlTipoPago").prop('disabled', true);
    $("#ddlMoneda").prop('disabled', true);
    $("#txtMonto").prop('disabled', true);
    $("#txtIGV").prop('disabled', true);
    $("#txtTotal").prop('disabled', true);
    $("#txtDetraccion").prop('disabled', true);
    $("#txtarObs").prop('disabled', true);
    $("#txtDetCot-PTot").prop('disabled', true);
    $("#txtDetCot-Cant").prop('disabled', true);
    

}
function DeshabilitarCamposRAP() {
    $("#ddlTipoSolicitud").prop('disabled', true);
    $("#txtMontoTotal").prop('disabled', true);
    $("#txtMontoSolAnt").prop('disabled', true);
    $("#CrearRAP").hide();
    $("#CancelarRAP").hide();
    
}
function HabilitarCampos() {
    $("#ddlProveedor").prop('disabled', false);
    $("#txtProforma").prop('disabled', false);
    $("#txtFechaProf").prop('disabled', false).datepicker('option', 'disabled', false);
    $("#txtVendedor").prop('disabled', false);
    $("#txtTelefonoVend").prop('disabled', false);
    $("#txtMailVend").prop('disabled', false);
    $("#ddlTipoPago").prop('disabled', false);
    $("#ddlMoneda").prop('disabled', false);
    $("#txtMonto").prop('disabled', false);
    $("#txtIGV").prop('disabled', false);
    $("#txtTotal").prop('disabled', false);
    $("#txtDetraccion").prop('disabled', false);
    $("#txtarObs").prop('disabled', false);
}

function InicializarControles(idRequerimiento, idProyecto, idSolicitud) {
   // $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val('0');
//[PV
    //$("input").css("background", "red");
    $("#txtMonto").bind({
        click: function () {  },
        keyup: function () {
            $("#txtIGV").val($("#txtMonto").val() * varIGV);
            $("#txtTotal").val(parseFloat($("#txtMonto").val()) + parseFloat($("#txtIGV").val()));
            $("#txtDetraccion").val(parseFloat($("#txtTotal").val()) * varDetraccion);
            
        },
        //keypress: function () {  $("#txtIGV").val($("#txtMonto").val()*0.18) },// NO OK
        focus: function () {
            $(this).css({ 'background-color': '#FFFFEEE' });
            //$("#TRCotMontos :text").css({ 'background-color': '#FFFFEEE' }); OK 
            $("#TRCotMontos > TD :text").css({ 'background-color': '#FFFFEEE' });
        },
        blur: function () {
            $(this).css({ 'background-color': '#ffffff' });
            $("#TRCotMontos > TD :text").css({ 'background-color': '#ffffff' });
        }
        //DFD8D1 gris
    });
//PV]
    if ($("#ContentPlaceHolder1_hdf_Cotizacion_Id").val() == '' || $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val() == null) {
        $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val('0');
    } else {
        //$("#ContentPlaceHolder1_hdf_Cotizacion_Id").val();
    }

    CargarComboProveedorCotizacion('#ddlProveedor', idSolicitud);

    $("#btnRegresar").click(function () {
        //alert("RQ: " + idRequerimiento + " / Proyecto: " + idProyecto);
        window.location.href = "../../Logistica/Compras/SolicitudCompra.aspx?Requerimiento_Id=" + parseInt(idRequerimiento) + "&Proyecto_Id=" + parseInt(idProyecto);
    });
    $("#btnBandejaCotizaciones").click(function () {
        //alert("RQ: " + idRequerimiento + " / Proyecto: " + idProyecto);
        window.location.href = "../../Logistica/Compras/BandejaCotizacion.aspx";
    });


    $("#btnListarCot").click(function () {
       $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val('0');
        CargarCotizaciones($("#ContentPlaceHolder1_hdSolicitud").val());

    });




    $("#btnEditarCotCa").click(function () {
      
        EditCotizacion = true;
        HabilitarCampos();
        $("#chkbSolAnticipo").prop('disabled', false);
        $(this).hide();
        $("#dlg-NuevaCot").dialog("option", "height", 850);
        $("#btnGuardarEdicion").show();
        $("#btnCancelarEdicion").show();
    });

    $("#btnGuardarEdicion").click(function () {
        if (ValidarCampos_CrearCotizacion() === true) {
            EditCotizacion = false;
            $(this).hide();
            $("#btnCancelarEdicion").hide();
            $("#dlg-NuevaCot").dialog("option", "height", 800);
            DeshabilitarCampos();
            $("#btnEditarCotCa").show();

            ObjCotizacionCa = {
                Cotizacion_Id: $("#hdCotizacionId").val(),
                Proveedor_Id: $('#ddlProveedor').val(),
                Cot_Forma_Pago_Id: $('#ddlTipoPago').val(),
                Cot_NumProforma_Nu: $('#txtProforma').val(),
                Cot_Prv_NomVendedor_Tx: $('#txtVendedor').val(),
                Cot_Prv_TelVendedor_Nu: $('#txtTelefonoVend').val(),
                Cot_Prv_MailVendedor_Tx: $('#txtMailVend').val(),
                Cot_Moneda_Id: $('#ddlMoneda').val(),
                Cot_Monto_Nu: $('#txtMonto').val(),
                Cot_IGV_Nu: $('#txtIGV').val(),
                Cot_MontoTotal_Nu: $('#txtTotal').val(),
                Cot_Detraccion_Nu: $('#txtDetraccion').val(),
                Cot_Observacion_Tx: $('#txtarObs').val(),
                Cot_FechaEmision_Fe: $('#txtFechaProf').val(),
                Aud_UsuarioActualizacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
            }

            $.ajax({
                async: false,
                type: "POST",
                url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ActualizarCotizacionCabecera',
                data: "{ObjCotizacionCa:" + JSON.stringify(ObjCotizacionCa) + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    Mensaje('Mensaje', 'Los datos fueron guardados con éxito.');
                },
            });
        } else {
            Mensaje('Mensaje', 'Faltan completar campos obligatorios');
        }
    });

    $("#ddlEstadoCot").change(function () {
       // $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val('0');
        CargarCotizaciones($("#ContentPlaceHolder1_hdSolicitud").val());
    });

    $("#btnCancelarEdicion").click(function () {
        EditCotizacion = false;
        $(this).hide();
        $("#btnGuardarEdicion").hide();
        $("#dlg-NuevaCot").dialog("option", "height", 800);
        DeshabilitarCampos();
        $("#btnEditarCotCa").show();

        objCotizacionCa = {
            Solicitud_Id: idSolicitud,
            Cotizacion_Id: $("#hdCotizacionId").val()
        }

        $.ajax({
            async:false,
            type: "POST",
            url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizaciones',
            data: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d.length <= 0) {
                    Mensaje('Error', 'Error al mostrar cotizacion');
                } else {
                    $("#ddlProveedor").val(data.d[0].Proveedor_Id);
                    $("#ComboBox_ddlProveedor").val($("#ddlProveedor option:selected").text());
                    $("#txtProforma").val(data.d[0].Cot_NumProforma_Nu);
                    var FechaEmision = ConvertFecha(data.d[0].Cot_FechaEmision_Fe)
                    $("#txtFechaProf").val(FechaEmision);
                    $("#txtVendedor").val(data.d[0].Cot_Prv_NomVendedor_Tx);
                    $("#txtTelefonoVend").val(data.d[0].Cot_Prv_TelVendedor_Nu);
                    $("#txtMailVend").val(data.d[0].Cot_Prv_MailVendedor_Tx);
                    $("#ddlTipoPago").val(data.d[0].Cot_Forma_Pago_Id);
                    $("#ComboBox_ddlTipoPago").val($("#ddlTipoPago option:selected").text());
                    $("#ddlMoneda").val(data.d[0].Cot_Moneda_Id);
                    $("#ComboBox_ddlMoneda").val($("#ddlMoneda option:selected").text());
                    $("#txtMonto").val(data.d[0].Cot_Monto_Nu);
                    $("#txtIGV").val(data.d[0].Cot_IGV_Nu);
                    $("#txtTotal").val(data.d[0].Cot_MontoTotal_Nu);
                    $("#txtDetraccion").val(data.d[0].Cot_Detraccion_Nu);
                    $("#txtarObs").val(data.d[0].Cot_Observacion_Tx);
                }
            },
        });
        
    });

    $("#chkbSolAnticipo").change(function () {        
        
        var varCotizacionId = 0;
  
        varCotizacionId = $("#hdCotizacionId").val();
        
        var c = this.checked;
        if (c === true) {         //   alert('if (c === true) {');
            //$("#dvRAP").dialog("option", "height", 270);
            limpiarCamposRAP();
            HabilitarCamposRAP();
            $("#dvRAP").dialog(
      {
          autoOpen: true,
          show: "blind",
          resizable: false,
          draggable: true,
          height: 'auto',
          width: 500,
          modal: true,
          closeOnEscape: false,
          //dialogClass: 'hide-close',
          title: "Requerimiento de Anticipo Proveedor ",
          buttons:[ 
              {
                  id: "CrearRAP",
                  text: "Crear RAP",
                  click: function () {

                      //alert('idCotizacion' + idCotizacion);
                      if (ValidarCampos_RAP() === true) {
                          ConfirmacionMensaje("Confirmacion", '¿Confirma creación de Requerimiento de Anticipo de Proveedor (R.A.P)?', function () {
                              var ObjSolicitudAnticipo = {
                                  Cotizacion_Id: varCotizacionId,
                                  Sol_Tipo_Anticipo_Id: $("#ddlTipoSolicitud").val(),
                                  Sol_Monto_Solicitado_nu: $("#txtMontoSolAnt").val(),
                                  Aud_UsuarioCreacion_Id: $("#ContentPlaceHolder1_hdUsuario").val(),
                              }
                              $.ajax({
                                  async: false,
                                  type: "POST",
                                  url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/CrearSolicitudAnticipo',
                                  data: "{ObjSolicitudAnticipo:" + JSON.stringify(ObjSolicitudAnticipo) + "}",
                                  contentType: "application/json; charset=utf-8",
                                  dataType: "json",
                                  success: function (data) {
                                      
                                      if (data.d > 0) {
                                          
                                          $("#hdRAP").val(data.d);
                                          //alert('$("#hdRAP").val()' + $("#hdRAP").val());
                                          DeshabilitarCamposRAP();
                                          Mensaje("Mensaje", 'Requerimiento de Anticipo de Proveedor fue generado con éxito');
                                          setTimeout(function () {
                                              $("#dvRAP").dialog("close"); 
                                          }, 400);

                                      }
                                      else {
                                          Mensaje('Mensaje', 'La solicitud de anticipo no fue generada.');
                                      }
                                  },
                                  error: function (data) {
                                      alert('Error al procesar los datos ...');
                                  }
                              });

                          }
                          , function () {
                              
                              $("#chkbSolAnticipo").prop("checked", false);
                              $("#dvRAP").dialog("close");
                          });
                      }
                   },
              },

               {
                   id: "CancelarRAP",
                   text: "Cancelar RAP",
                   click: function () {
                       
                       $("#chkbSolAnticipo").prop("checked", false);
                       $("#dvRAP").dialog("close");

                   }
                   
                   },

          ],
          open: function (event, ui) {
              //$('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
              //$('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
              $(".ui-dialog-titlebar-close", ui.dialog).hide();
          }
          //close: function () {
          //    alert('close');
          //    //var c = this.checked;
          //    //if (c === true) {

          //        $("#chkbSolAnticipo").prop("checked", false);
          //    //}

          //}
        });
        }
        else {
            //alert('else {');
            //$("#dvRAP").dialog("close");
            ConfirmacionMensaje("Confirmacion", '¿Confirma eliminación de Requerimiento de Anticipo de Proveedor (R.A.P)?', function () {
                //alert('hdRAP' + $("#hdRAP").val());
                //alert('EditCotizacion' + EditCotizacion);
                //if (EditCotizacion === true) {
                //    cargarRAP();

                //}
                var ObjSolicitudAnticipo = {
                    Solicitud_Anticipo_Id: $("#hdRAP").val(),                    
                    Aud_UsuarioEliminacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
                    
                }
                $.ajax({
                    async: false,
                    type: "POST",
                    url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/EliminarRAP',
                    data: "{ObjSolicitudAnticipo:" + JSON.stringify(ObjSolicitudAnticipo) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        
                        if (data.d > 0) {
                            
                            //alert(data.d);                    
                            limpiarCamposRAP();
                            HabilitarCamposRAP();
                            Mensaje("Mensaje", 'equerimiento de Anticipo de Proveedor fue generado con éxito');

                        }
                        else {
                            Mensaje('Mensaje', 'La solicitud de anticipo no fue generada.');
                        }
                    },
                    error: function (data) {
                        alert('Error al procesar los datos ...');
                    }
                });

            },
            function () {
                
                $("#chkbSolAnticipo").prop("checked", true);
            });
        }
        
    });
    
    $('#ddlTipoSolicitud').change(function () {
        if ($("#ddlTipoSolicitud").val() === '32') {
            $("#txtMontoSolAnt").val($("#txtMontoTotal").val());
        }
        else {
            $("#txtMontoSolAnt").val("");
        }
    });

    //$("#txtDetCot-PTotCal").prop("disabled", true);

    $("#txtDetCot-PUnit").bind({
        keyup: function () {
            //keyup: function (e) {
            $("#txtDetCot-PTot").val(($(this).val()) * ($("#txtDetCot-Cant").val()));
            //$("#txtDetCot-PTotCal").val(($(this).val()) * ($("#txtDetCot-Cant").val()));

        },
        focus: function () {
            $(this).css({ 'background-color': '#FFFFEEE' });
            $("tr[id*=trMontos] :text").css({ 'background-color': '#FFFFEEE' });
        },
        blur: function () {
            $(this).css({ 'background-color': '#ffffff' });
            $("tr[id*=trMontos] :text").css({ 'background-color': '#ffffff' });
        }
    });
    $('#txtMontoSolAnt').numeric({ decimalPlaces: 5 });/**/
    $('#txtMontoTotal').numeric({ decimalPlaces: 5 });/**/
    $('#txtMonto').numeric({ decimalPlaces: 5 });
    $('#txtIGV').numeric({ decimalPlaces: 5 });
    $('#txtTotal').numeric({ decimalPlaces: 5 });
    $('#txtDetraccion').numeric({ decimalPlaces: 5 });

    $('#txtDetCot-PTot').numeric({ decimalPlaces: 5 });
    $('#txtDetCot-PTotCal').numeric({ decimalPlaces: 5 });
    $("#txtDetCot-PUnit").numeric({ decimalPlaces: 5 });

    $("#txtFechaProf").datepicker({
        showOn: "button",
        buttonImage: '../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/Calendario01_16.png',
        buttonImageOnly: true,
        buttonText: "Seleccione la fecha",
        dateFormat: "dd/mm/yy",
        //defaultDate: "+1w",
        onClose: function (selectedDate) {
        }
    });

    CargarCotizaciones(idSolicitud);

    if ($("#ddlRequerimiento").val() === 0)
    {
        $("#ddlSolicitud").attr('disable', 'true');
    }

    $("#btnIngCotizacion").click(function () {
        
        //RAP
        limpiarCamposRAP();
        $("#chkbSolAnticipo").prop('disabled', true); $("#chkbSolAnticipo").prop('checked', false);
        $("#iVisualizarRAP").hide();
        //
        /*var total = 0;
        for (var i = 1; i <= $("#gvGrdItemsRQ").getGridParam('records') ; i++) {
            if ($("#gvGrdItemsRQ").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
                total = total + 1;
            }
        }*/
        if (CotEnviada === false) {
            HabilitarCampos();
            $("#dlg-NuevaCot").dialog({
                autoOpen: true,
                show: "blind",
                width: 980,
                height: 450,
                modal: true,
                resizable: false,
                title: "Ingresar Cotizacion",
                buttons: {
                    "Crear": function () {
                        if (ValidarCampos_CrearCotizacion() === true) {
                            ConfirmacionMensaje('Confirmación', '¿Desea crear la cotizacion?', function () {
                                CrearCotizacion(idSolicitud);
                                $("#dlg-NuevaCot").dialog("option", "height", 800);
                                Mensaje('Mensaje', 'Ingrese el detalle de la cotización');

                                //Oculta el boton Registrar para luego mostrar el boton Finalizar
                                $('.ui-dialog-buttonpane button:contains("Crear")').button().hide();
                                $('.ui-dialog-buttonpane button:contains("Cancelar")').button().hide();
                                $('.ui-dialog-buttonpane button:contains("Finalizar")').button().show();
                            });
                        } else {
                            Mensaje('Mensaje', 'Faltan completar campos obligatorios');
                        }
                    },
                    "Finalizar": function () {
                        var nfaltantes = ValidarDetallesCompletos($("#hdCotizacionId").val());
                        //alert("Finalizar: " + nfaltantes);

                        if (nfaltantes === 0) {
                            
                            //[pv
                            var varValidarTotalvs = true;
                            var varValidarTotalvs = validarTotalvs();

                            if (varValidarTotalvs === false) {
                                
                                Mensaje('No coinciden Valores!', 'No coinciden el total de la cotizacion: ' + $("#txtTotal").val() + '  con la sumatoria de subtotales: ' + varTotalDetalleCot + ' editar para igualar totales ');
                                   
                                $("#dlg-NuevaCot").dialog("close");
                                $("#dvDetalleCot").hide();
                                LimpiarControles();
                                CargarCotizaciones(idSolicitud);

                                
                            }
                            else if (varValidarTotalvs === true) {
                                Mensaje('Coinciden Valores!', 'Ingreso correcto!!!');
                            //pv]
                            ConfirmacionMensaje('Confirmación', '¿Desea finalizar el registro del detalle de la cotizacion?', function () {
                                CargarCotizaciones(idSolicitud);
                                $("#dlg-NuevaCot").dialog("close");
                                $("#dvDetalleCot").hide();
                                LimpiarControles();
                            });
                            }

                        }
                        else {
                            ConfirmacionMensaje('Confirmación', '¿Desea terminar el registro del detalle de la cotizacion sin completar todos los datos?', function () {
                                CargarCotizaciones(idSolicitud);
                                $("#dlg-NuevaCot").dialog("close");
                                $("#dvDetalleCot").hide();
                                LimpiarControles();
                                //RAP
                                limpiarCamposRAP(); $("#chkbSolAnticipo").prop('disabled', false);
                                //
                            });
                        }

                        
                    },
                    "Cancelar": function () {
                        ConfirmacionMensaje('Confirmación', '¿Desea cancelar el registro de la cotización? ...', function () {
                            LimpiarControles(); $("#dlg-NuevaCot").dialog("close");
                        });
                    }
                },
                open: function (event, ui) {
                    $('.ui-dialog-buttonpane').find('button:contains("Crear")').addClass('boton');
                    $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
                    $('.ui-dialog-buttonpane').find('button:contains("Finalizar")').addClass('boton');
                    $(".ui-dialog-titlebar-close", ui.dialog).hide();
                }
            });
            //Luego de cargar el Dialog oculta el boton Finalizar
            $('.ui-dialog-buttonpane button:contains("Finalizar")').button().hide();
        }
        else {
            Mensaje("Mensaje", "No se pueden crear mas cotizaciones.");
        }
    });
    /*$('#ContenidoMsj').editable({ inlineMode: false, language: 'es', maxCharacters: 140, height: 200, buttons: ['bold', 'italic', 'underline', 'fontSize', 'align', 'insertHorizontalRule', 'undo', 'redo', 'html'] })
    $("#DeMsj").prop('readonly', true).html($("#ContentPlaceHolder1_hdMailUsu").val());*/
}

function CrearCotizacion(idSolicitud) {
    
    objCotizacionCa = {
        Solicitud_Id: idSolicitud,
        Proveedor_Id: $('#ddlProveedor').val(),
        Cot_Forma_Pago_Id: $('#ddlTipoPago').val(),
        Cot_NumProforma_Nu: $('#txtProforma').val(),
        Cot_Prv_NomVendedor_Tx: $('#txtVendedor').val(),
        Cot_Prv_TelVendedor_Nu: $('#txtTelefonoVend').val(),
        Cot_Prv_MailVendedor_Tx: $('#txtMailVend').val(),
        Cot_Moneda_Id: $('#ddlMoneda').val(),
        Cot_Monto_Nu: $('#txtMonto').val(),
        Cot_IGV_Nu: $('#txtIGV').val(),
        Cot_MontoTotal_Nu: $('#txtTotal').val(),
        Cot_Detraccion_Nu: $('#txtDetraccion').val(),
        Cot_Observacion_Tx: $('#txtarObs').val(),
        Cot_FechaEmision_Fe: Controlar_Valores('txtFechaProf','datetime'),
        Aud_UsuarioCreacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
    }
    //console.log(objCotizacionCa);
    $.ajax({
        type: "POST",
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/CrearCotizacionCayDe',
        data: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d <= 0) {
                Mensaje('Error', 'La cotizacion no pudo crearse');
            } else {
                Mensaje('Mensaje', 'La cotizacion fue creada correctamente. Ingresar el detalle de la misma.');
                DeshabilitarCampos();
                $("#dvDetalleCot").show();
                $("#hdCotizacionId").val(data.d);
                CargarDetalleCotizacion(data.d);
            }

        },
        error: function (data) {
            alert('Error al procesar los datos ...');
        }
    });
}
function CargarDetalleCotizacion(idCotizacion) {
    objCotizacionDe = {
        Cotizacion_Id: idCotizacion
    }

    $("#dvDetNuevaCotizacion").html('<table id="gvDetCotizaciones"></table><div id="pieGvDetCotizaciones"></div>');
    $("#gvDetCotizaciones").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizacionDetalle',
        datatype: "json",
        mtype: "POST",
        postData: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Desc. Item Solicitud', 'Cantidad', 'U. M.', '', '', 'Desc. Item Cotizacion', 'Cantidad', '', 'U.M.', 'Precio Unitario', 'Precio Total',''],
        colModel: [
           { name: 'Solicitud_Detalle_Id', index: 'Solicitud_Detalle_Id', width: 80, align: 'center', hidden: true },
           { name: 'SDe_Descripcion_Tx', index: 'SDe_Descripcion_Tx', width: 200, align: 'center' },
           { name: 'SDe_Cantidad_Nu', index: 'SDe_Cantidad_Nu', width: 80, align: 'center' },
           { name: 'SD_UMe_Simbolo_Tx', index: 'SD_UMe_Simbolo_Tx', width: 80, align: 'center' },
           { name: 'Editar', index: 'Editar', width: 30, align: 'center', formatter: 'actionFormatterEditarSol', search: false },
           { name: 'Cotizacion_Detalle_Id', index: 'Cotizacion_Detalle_Id', width: 80, align: 'center', hidden: true },
           { name: 'CDe_Descripcion_Tx', index: 'CDe_Descripcion_Tx', width: 200, align: 'center' },
           { name: 'CDe_Cantidad_Nu', index: 'SDe_Cantidad_Nu', width: 80, align: 'center' },
           { name: 'CDe_Und_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
           { name: 'CD_UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 80, align: 'center' },
           { name: 'CDe_Precio_Unitario_Nu', index: 'CDe_Precio_Unitario_Nu', width: 80, align: 'center' },
           { name: 'CDe_Sub_Total_Nu', index: 'CDe_Sub_Total_Nu', width: 80, align: 'center' },
           { name: 'CDe_Estado_Itm_Id', index: 'CDe_Sub_Total_Nu', width: 80, align: 'center', hidden:true },
           
           /*{ name: 'Editar', index: 'Ver', width: 30, align: 'center', formatter: 'actionFormatterEditarSol', search: false },
           { name: 'Enviar', index: 'Enviar', width: 30, align: 'center', formatter: 'actionFormatterEnviarSol', search: false, hidden: true },
           { name: 'Exportar', index: 'Exportar', width: 30, align: 'center', formatter: 'actionFormatterExportarExcel', search: false },
           { name: 'AdmCotizaciones', index: 'AdmCotizaciones', width: 30, align: 'center', formatter: 'actionFormatterAdmCotizaciones', search: false }*/
        ],
        pager: "#pieGvDetCotizaciones",
        viewrecords: true,
        autowidth: true,
        height: 235,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Items de la Cotizacion',
        loadComplete: function (data) {
        },
        loadError: function () {
        },
    }).navGrid('#pieGvDetCotizaciones', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterEditarSol: function (cellvalue, options, rowObject) {
            return "<i title=\"Editar Detalle Cotizacion\" onclick=\"EditarDetalleCotizacion(" + idCotizacion + ", " + rowObject.Cotizacion_Detalle_Id + ")\" style=\"cursor:pointer;color:#222;\" class='fa fa-pencil-square-o fa-2x'></i>"
            //return "<img title=\"Editar Solicitud\" onclick=\"EditarSolicitud(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        }
    });
}

function ValidarDetallesCompletos(IdCotizacion) {
    var nfaltantes = 0; varTotalDetalleCot = 0;
    /*var datos = $(control).jqGrid('getGridParam', 'data');*/
    objCotizacionDe = {
        Cotizacion_Id: IdCotizacion
    }
    $.ajax({
        async: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizacionDetalle',
        dataType: "json",
        data: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
        success: function (data) {
            
            for (var i = 0 ; i < data.d.length; i++) {
                if (data.d[i].CDe_Estado_Itm_Id === 1) {
                    nfaltantes++;
                }
                varTotalDetalleCot += data.d[i].CDe_Sub_Total_Nu;
            };
            //console.log(nfaltantes);
            //return nfaltantes;
        },
        error: function (xhr) {
            //alert("Error!!!");
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    //alert("ValidarDetallesCompletos: " + nfaltantes);
    //alert('varTotalDetalleCot=' + varTotalDetalleCot);
    return nfaltantes;
}

function EditarDetalleCotizacion(idCotizacion, idCotizacionDet) {
    if (CotEnviada) {
        Mensaje("Mensaje", "La cotizacion ya fue enviada, el detalle no podra ser editado.")
    } else {
        $("#txtDetCot-PUnit").val("");
        $("#txtDetCot-PTot").val("");
        //$("#txtDetCot-PTotCal").val("");
        objCotizacionDe = {
            Cotizacion_Id: idCotizacion,
            Cotizacion_Detalle_Id: idCotizacionDet
        }
        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizacionDetalle',
            dataType: "json",
            data: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
            success: function (data) {
                $("#txtDetCot-Desc").val(data.d[0].SDe_Descripcion_Tx);
                $("#txtDetCot-Cant").val(data.d[0].SDe_Cantidad_Nu);
                $("#ddlDetCot-Und").val(data.d[0].Unidad_Medida_Id);
                $("#ComboBox_ddlDetCot-Und").val($("#ddlDetCot-Und option:selected").text());
            
                $("#dlg-DetalleNuevaCot").dialog({
                    autoOpen: true,
                    show: "blind",
                    /*show: {
                        effect: "drop",
                        duration: 1000
                    },
                    hide: {
                        effect: "drop",
                        duration: 1000
                    },*/
                    width: 550,
                    height: 300,
                    modal: true,
                    title: "Ingresar Detalle Cotizacion",
                    buttons: {
                        "Aceptar": function () {
                            
                            if (ValidarCampos_EditarDetalleCotizacion() === true)
                            {
                                objCotizacionDe = {
                                    Cotizacion_Id: idCotizacion,
                                    Cotizacion_Detalle_Id: idCotizacionDet,
                                    CDe_Descripcion_Tx: $("#txtDetCot-Desc").val(),
                                    CDe_Cantidad_Nu: $("#txtDetCot-Cant").val(),
                                    CDe_Und_Medida_Id: $("#ddlDetCot-Und").val(),
                                    CDe_Precio_Unitario_Nu: $("#txtDetCot-PUnit").val(),
                                    CDe_Sub_Total_Nu: $("#txtDetCot-PTot").val(),
                                    Aud_UsuarioActualizacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
                                    , CDe_Estado_Itm_Id: $("#ContentPlaceHolder1_hdf_EstCotDetIngresadaId").val()
                                }
                                $.ajax({
                                    async: true,
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ActualizarDetalleCotizacion',
                                    dataType: "json",
                                    data: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
                                    success: function () {
                                        Mensaje('Mensaje', 'El detalle fue ingresado.', function () {
                                            $("#dlg-DetalleNuevaCot").dialog("close");
                                            CargarDetalleCotizacion(idCotizacion);
                                            //RAP

                                            var nfaltantes = ValidarDetallesCompletos($("#hdCotizacionId").val());

                                            if (nfaltantes == 0) {
                                                $("#chkbSolAnticipo").removeAttr("disabled");
                                            }
                                            //

                                        });                                
                                    },
                                    error: function (xhr) {
                                        //alert("Error!!!");
                                        alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
                                    }
                                });
                            }
                        },
                        "Cancelar": function () {
                            ConfirmacionMensaje('Confirmación', '¿Desea cancelar el registro?', function () { $("#dlg-DetalleNuevaCot").dialog("close"); });
                        }
                    },
                    open: function (event, ui) {
                        $('.ui-dialog-buttonpane').find('button:contains("Aceptar")').addClass('boton');
                        $('.ui-dialog-buttonpane').find('button:contains("Cancelar")').addClass('boton');
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    }
                
                });
            },
            error: function (xhr) {
                //alert("Error!!!");
                alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            }
        });
    }
}

function SeleccionarCotGanadora(IdSolicitud, IdCotizacion, IdEstado) {
    ObjCotizacionCa = {
        Cotizacion_Id: IdCotizacion,
        Cot_Estado_Id: IdEstado,
        Aud_UsuarioActualizacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
    }
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/SeleccionCotizacionGanadora',
        dataType: "json",
        data: "{ObjCotizacionCa:" + JSON.stringify(ObjCotizacionCa) + "}",
        success: function () {
            if (IdEstado === varEstCotGanadora){
                Mensaje('Mensaje', 'La cotizacion fue seleccionada como GANADORA');
            }
            CargarCotizaciones(IdSolicitud);
        },
        error: function (xhr) {
            //alert("Error!!!");
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
}

function CargarCotizaciones(idSolicitud){
    objCotizacionCa = {
        Solicitud_Id: idSolicitud,
        Cot_Estado_Id: $("#ddlEstadoCot").val(),
        Cotizacion_Id: $("#ContentPlaceHolder1_hdf_Cotizacion_Id").val()
    }

    $("#SEC_Cotizaciones").html('<table id="gvCotizaciones"></table><div id="pieGvCotizaciones"></div>');
    $("#gvCotizaciones").jqGrid({
        regional: 'es',
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizaciones',
        datatype: "json",
        mtype: "POST",
        postData: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['', 'Cod Reque', 'Proforma', 'Cod Solic.', 'Cod Cotiz.', '', 'Nom Proveedor', 'N° Items', 'Monto', 'IGV'
                    , 'Monto total', 'Detraccion', '', 'Estado', 'Fecha Creacion','RAP', ''/*, '', '', ''*/],
        colModel: [
           { name: 'Editar', index: 'Ver', width: 20, align: 'center', formatter: 'actionFormatterEditarSol', search: false },
           { name: 'Requerimiento_Co', index: 'Requerimiento_Co', width: 50, align: 'center' },
           { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu', width: 50, align: 'center' },
           { name: 'Solicitud_Id', index: 'Solicitud_Id', width: 40, align: 'center', hidden: true },
           { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 40, align: 'center', hidden: false },
           { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 50, align: 'center', hidden: true },
           { name: 'Cot_Proveedor_Tx', index: 'Cot_Proveedor_Tx', width: 150, align: 'center' },
           { name: 'Cot_Cant_Items', index: 'Cot_Cant_Items', width: 35, align: 'center' },
           { name: 'Cot_Monto_Nu', index: 'Cot_Monto_Nu', width: 40, align: 'center' },
           { name: 'Cot_IGV_Nu', index: 'Cot_IGV_Nu', width: 35, align: 'center' },
           { name: 'Cot_MontoTotal_Nu', index: 'Cot_MontoTotal_Nu', width: 55, align: 'center' },
           { name: 'Cot_Detraccion_Nu', index: 'Cot_Detraccion_Nu', width: 45, align: 'center' },
           { name: 'Cot_Estado_Id', index: 'Cot_Estado_Id', hidden: true },
           { name: 'Cot_Estado_Tx', index: 'Cot_Estado_Tx', width: 100, align: 'center' },
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'ISO8601Long', newformat: 'd-m-Y  H:i  A' } },
            { name: 'Cot_Anticipo_FL', index: 'Cot_Anticipo_FL', width: 25, align: 'center', formatter: 'actionFormatterVisualizarRAP' },
          { name: 'EnviarCot', index: 'Ver', width: 20, align: 'center', formatter: 'actionFormatterEnviarCot', search: false },
           /*{ name: 'Enviar', index: 'Enviar', width: 30, align: 'center', formatter: 'actionFormatterEnviarSol', search: false, hidden: true },
           { name: 'Exportar', index: 'Exportar', width: 30, align: 'center', formatter: 'actionFormatterExportarExcel', search: false },
           { name: 'AdmCotizaciones', index: 'AdmCotizaciones', width: 30, align: 'center', formatter: 'actionFormatterAdmCotizaciones', search: false }*/
        ],
        pager: "#pieGvCotizaciones",
        viewrecords: true,
        autowidth: true,
        height: 265,
        //rowNum: 10,
        //rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        multiselect: true,
        multiboxonly: true,
        /*onSelectRow: function (rowId, e, item) {
            alert(rowId);
            var gridSelRow = $('#gvCotizaciones').getGridParam('selrow');
            
        },*/
        emptyrecords: "No hay Registros.",
        caption: 'Listado de Cotizaciones',
        onCellSelect: function (rowid, index, contents, event) {
            //alert('onCellSelect: function (rowid, index, contents, event) { rowid: ' + rowid + '\n index: ' + index + 
            //   '\n contents: ' + contents + '\n event: ' + event);

            var allRecords = $("#gvCotizaciones").getGridParam('records');
            var marcado = 0;
            var filamarcada = 0;
            var nItemsFaltantes = ValidarDetallesCompletos($("#gvCotizaciones").getLocalRow(rowid).Cotizacion_Id);
            
            //alert("allRecords: " + allRecords + "\n onCellSelect: " + nItemsFaltantes);
            for (var i = 1; i <= allRecords; i++)
            {
                if (i != rowid) {
                    if ($("#jqg_gvCotizaciones_" + i ).is(":checked"))
                    {
                        marcado = 1;
                        filamarcada = i;
                    }
                }
            }
          
            var blnChecked = $("#jqg_gvCotizaciones_" + rowid).is(":checked");
            //alert(blnChecked);
            if (index === 0 && blnChecked === true && CotEnviada === false) { //Ya existe Ganadora ... Confirmar reseleccion Ganadora
                if (marcado === 1) {
                    
                    if(nItemsFaltantes === 0){
                        ConfirmacionMensaje('Confirmación', 'Ya se eligio una cotizacion como ganadora.\n¿Desea cambiar la cotizacion ganadora y establecer la presentada por ' + $("#gvCotizaciones").getLocalRow(rowid).Cot_Proveedor_Tx + '?', function () {
                            $("#jqg_gvCotizaciones_" + filamarcada).attr('checked', false);
                            SeleccionarCotGanadora(idSolicitud, $("#gvCotizaciones").getLocalRow(filamarcada).Cotizacion_Id, varEstCotCreada);
                            SeleccionarCotGanadora(idSolicitud, $("#gvCotizaciones").getLocalRow(rowid).Cotizacion_Id, varEstCotGanadora);
                        }, function () {
                            $("#jqg_gvCotizaciones_" + filamarcada).attr('checked', true);
                            $("#jqg_gvCotizaciones_" + rowid).attr('checked', false);
                        });
                    }
                    else{
                        Mensaje('Mensaje', 'Esta no puede ser elegida como GANADORA, faltan ingresar detalles a la cotizacion.');
                        $("#jqg_gvCotizaciones_" + rowid).prop('checked', false);
                    }
                    
                }
                else {
                    if(nItemsFaltantes === 0){
                        ConfirmacionMensaje('Confirmación', 'La cotizacion presentada por ' + $("#gvCotizaciones").getLocalRow(rowid).Cot_Proveedor_Tx + ' sera escogida como ganadora.¿Desea guardar los cambios?', function () {
                            SeleccionarCotGanadora(idSolicitud,$("#gvCotizaciones").getLocalRow(rowid).Cotizacion_Id, varEstCotGanadora);
                        }, function () {
                            $("#jqg_gvCotizaciones_" + rowid).attr('checked', false);
                        });
                    }
                    else{
                        Mensaje('Mensaje', 'Esta no puede ser elegida como GANADORA, faltan ingresar detalles a la cotizacion.');
                        $("#jqg_gvCotizaciones_" + rowid).prop('checked', false);
                    }
                }
            }
            else if (index === 0 && blnChecked === false && CotEnviada === false) {
                if (nItemsFaltantes === 0) {
                    ConfirmacionMensaje('Confirmación', '¿Desea quitar el estado de "GANADORA" a la cotizacion presentada por ' + $("#gvCotizaciones").getLocalRow(rowid).Cot_Proveedor_Tx + '?', function () {
                        SeleccionarCotGanadora(idSolicitud, $("#gvCotizaciones").getLocalRow(rowid).Cotizacion_Id, varEstCotCreada);
                    }, function () {
                        $("#jqg_gvCotizaciones_" + rowid).prop('checked', true);
                    });
                }
                else {
                    Mensaje('Mensaje', 'Esta no puede ser elegida como GANADORA, faltan ingresar detalles a la cotizacion.');
                    $("#jqg_gvCotizaciones_" + rowid).prop('checked', false);
                }
            }
        },
        beforeSelectRow: function (rowid, e) {

            return false;
        },
        loadComplete: function (data) {
            var myGrid = $("#gvCotizaciones");
            $("#cb_" + myGrid[0].id).hide();       
            $('#pieGvCotizaciones_center').hide();

            var allRecords = $("#gvCotizaciones").getGridParam('records');
            

           

            for (var i = 1; i <= allRecords; i++) {
                var varCot_Estado_Id = $("#gvCotizaciones").getLocalRow(i).Cot_Estado_Id;
                
                if (varCot_Estado_Id == varEstCotGanadora || varCot_Estado_Id == varEstCotOCGenerada || varCot_Estado_Id == varEstCotEnviada_OC || varCot_Estado_Id == varEstCotEnviada_SA) {
                    
                    $("#" + i, "#gvCotizaciones").find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
                    $("#jqg_gvCotizaciones_" + i).prop('checked', true);
                    if ($("#gvCotizaciones").getLocalRow(i).Cot_Estado_Id === 39 || $("#gvCotizaciones").getLocalRow(i).Cot_Estado_Id === 50 || $("#gvCotizaciones").getLocalRow(i).Cot_Estado_Id === 51)
                    {
                        CotEnviada = true;
                        BloquearCotizaciones();
                    }
                }
                else{
                    $("#" + i , "#gvCotizaciones").find("img").css({'display':'none'});
                }
            }
            
        },
        loadError: function () {
        },
        subGrid: true,
        subGridRowExpanded: function (subgrid_id, row_id) {
            var idCotizacion = $("#gvCotizaciones").getLocalRow(row_id).Cotizacion_Id;
            objCotizacionDe = {
                Cotizacion_Id: idCotizacion
            }
            var subgrid_table_id;
            subgrid_table_id = subgrid_id + "_t";
            jQuery("#" + subgrid_id).html("<label style='margin:2px 10px;'>Detalle de la Cotizacion:</label><table id='" + subgrid_table_id + "' class='scroll'></table>");
            jQuery("#" + subgrid_table_id).jqGrid({
                url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizacionDetalle',
                datatype: "json",
                mtype: "POST",
                postData: "{objCotizacionDe:" + JSON.stringify(objCotizacionDe) + "}",
                ajaxGridOptions: { contentType: "application/json" },
                loadonce: true,
                colNames: ['', '', 'Descripcion', 'Cantidad', '', 'U.M.', 'Precio Unitario', 'Precio Total',''],
                colModel: [
                    { name: 'Solicitud_Detalle_id', index: 'Requerimiento_Detalle_Id', hidden: true },
                    { name: 'Item_Id', index: 'Item_Id', hidden: true },
                    { name: 'CDe_Descripcion_Tx', index: 'RDe_Descripcion_Tx', width: 350, align: 'center' },
                    { name: 'CDe_Cantidad_Nu', index: 'SDe_Cantidad_Nu', width: 80, align: 'center' },
                    { name: 'CDe_Und_Medida_Id', index: 'Unidad_Medida_Id', hidden: true },
                    { name: 'CD_UMe_Simbolo_Tx', index: 'CD_UMe_Simbolo_Tx', width: 80, align: 'center' },
                    { name: 'CDe_Precio_Unitario_Nu', index: 'CDe_Precio_Unitario_Nu', width: 80, align: 'center' },
                    { name: 'CDe_Sub_Total_Nu', index: 'CDe_Sub_Total_Nu', width: 80, align: 'center' },
                    { name: 'CDe_Estado_Itm_Id', index: 'CDe_Sub_Total_Nu', width: 80, align: 'center', hidden:true },
                ],
                gridview: true,
                height: '100%',
                //rowNum: 10,
                jsonReader: {
                    page: function (obj) { return 1; },
                    total: function (obj) { return 1; },
                    records: function (obj) { return obj.d.length; },
                    root: function (obj) { return obj.d; },
                    repeatitems: false,
                    id: "0"
                },
                sortname: 'Cotizacion_Detalle_Id',
                sortorder: "desc",
                loadComplete: function (data) {
                    
                }
            });
        }
    }).navGrid('#pieGvCotizaciones', { edit: false, add: false, del: false, search: false, refresh: false });
    $.extend($.fn.fmatter, {
        actionFormatterEditarSol: function (cellvalue, options, rowObject) {
            return "<i title=\"Editar Cotizacion\" onclick=\"EditarCotizacion(" + rowObject.Solicitud_Id + ", " + rowObject.Cotizacion_Id + ")\" style=\"cursor:pointer;color:#222;\" class='fa fa-pencil-square-o fa-2x'></i>"
            //return "<img title=\"Editar Solicitud\" onclick=\"EditarSolicitud(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/VerDoc_16.png\" />";
        },
        actionFormatterEnviarCot: function (cellvalue, options, rowObject) {
            return "<img title=\"Enviar Cotizacion\" onclick=\"EnviarCotizacion(" + rowObject.Cotizacion_Id + "," + rowObject.Cot_MontoTotal_Nu + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/IrFormulario01_15.png\" />";
        }
        , actionFormatterVisualizarRAP: function (cellvalue, options, rowObject) {
            if (rowObject.Cot_Anticipo_FL === true) {
                return "<i title=\"Visualizar Requerimiento de Anticipo Proveedor\" onclick=\"cargarRAP(" + rowObject.Cotizacion_Id + ");visualizarRAP()\" style=\"cursor:pointer\" class='fa fa-money'></i>"
            }
            else { return "<i title=\"No tiene RAP\"  ></i>" }

        }

        /*,

        actionFormatterExportarExcel: function (cellvalue, options, rowObject) {
            return "<img title=\"Exportar a Excel\" onclick=\"ExportarExcel(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/ExpExcel01_16.png\" />";
        },
        actionFormatterAdmCotizaciones: function (cellvalue, options, rowObject) {
            return "<img title=\"Ir a Cotizaciones\" onclick=\"IrACotizaciones(" + rowObject.Solicitud_Id + ")\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/IrFormulario01_15.png\" />";
        }*/
    });

}

function EditarCotizacion(idSolicitud, idCotizacion) {
   
    //RAP
    limpiarCamposRAP();
    //$("#chkbSolAnticipo").removeAttr("disabled");
    //$("#chkbSolAnticipo").prop('disabled', false);
    //
    DeshabilitarCampos();
    DeshabilitarCamposRAP();
    $("#chkbSolAnticipo").prop('disabled', true);

    objCotizacionCa = {
        Solicitud_Id: idSolicitud,
        Cotizacion_Id: idCotizacion
    }

    $.ajax({
        async:false,
        type: "POST",
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarCotizaciones',
        data: "{objCotizacionCa:" + JSON.stringify(objCotizacionCa) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length <= 0) {
                Mensaje('Error', 'Error al mostrar cotizacion');
            } else {
                $("#ddlProveedor").val(data.d[0].Proveedor_Id);
                $("#ComboBox_ddlProveedor").val($("#ddlProveedor option:selected").text());
                $("#txtProforma").val(data.d[0].Cot_NumProforma_Nu);
                var FechaEmision = ConvertFecha(data.d[0].Cot_FechaEmision_Fe)
                $("#txtFechaProf").val(FechaEmision);
                $("#txtVendedor").val(data.d[0].Cot_Prv_NomVendedor_Tx);
                $("#txtTelefonoVend").val(data.d[0].Cot_Prv_TelVendedor_Nu);
                $("#txtMailVend").val(data.d[0].Cot_Prv_MailVendedor_Tx);
                $("#ddlTipoPago").val(data.d[0].Cot_Forma_Pago_Id);
                $("#ComboBox_ddlTipoPago").val($("#ddlTipoPago option:selected").text());
                $("#ddlMoneda").val(data.d[0].Cot_Moneda_Id);
                $("#ComboBox_ddlMoneda").val($("#ddlMoneda option:selected").text());
                $("#txtMonto").val(data.d[0].Cot_Monto_Nu);
                $("#txtIGV").val(data.d[0].Cot_IGV_Nu);
                $("#txtTotal").val(data.d[0].Cot_MontoTotal_Nu);
                $("#txtDetraccion").val(data.d[0].Cot_Detraccion_Nu);
                $("#txtarObs").val(data.d[0].Cot_Observacion_Tx);

                $("#hdCotizacionId").val(data.d[0].Cotizacion_Id);
                var varCotAnticipofl;
                varCotAnticipofl = data.d[0].Cot_Anticipo_FL;
                if (varCotAnticipofl === true) {
                    $("#chkbSolAnticipo").prop('checked', true);
                    cargarRAP(data.d[0].Cotizacion_Id);
                }
                else { $("#chkbSolAnticipo").prop('checked', false); }


                //alert('varCotAnticipofl=' + varCotAnticipofl);

                CargarDetalleCotizacion(idCotizacion);
                $("#dvDetalleCot").show();
                $("#dlg-NuevaCot").dialog({
                    autoOpen: true,
                    show: "blind",
                    width: 980,
                    height: 800,
                    modal: true,
                    resizable: false,
                    title: "Editar Cotizacion",
                    buttons: {

                        "Cerrar": function () {
                            if (EditCotizacion === false) {
                                ConfirmacionMensaje("Confirmacion", 'Desea cerrar la ventana de edicion', function () {
                                    LimpiarControles();
                                    $("#dlg-NuevaCot").dialog('close');
                                    $("#btnEditarCotCa").hide();
                                    $("#dvDetalleCot").hide();
                                    EditCotizacion = false;
                                });
                            }
                            else {
                                MensajeFocus('Mensaje', 'Debe guardar los cambios antes de cerrar la ventana de edicion.', 'btnGuardarEdicion');
                            }
                        }
                    },
                    open: function (event, ui) {
                        if (CotEnviada) {
                            $("#btnEditarCotCa").hide();
                        } else {
                            $("#btnEditarCotCa").show();
                            //("Mensaje", "La cotizacion ya no podra editarse. Se visualizaran los datos.");
                        }
                        $('.ui-dialog-buttonpane').find('button:contains("Cerrar")').addClass('boton');
                        $(".ui-dialog-titlebar-close", ui.dialog).hide();
                    }
                });
            }

        },
        error: function (data) {
            alert('Error al procesar los datos ...');
        }
    });
}

function BloquearCotizaciones() {
    var allRecords = $("#gvCotizaciones").getGridParam('records');
    for (var i = 1; i <= allRecords; i++) {
        $("#jqg_gvCotizaciones_" + i).prop("disabled", true);
    }
}

function CambiarEstadoCotizacionCabecera(idCotizacion,idEstado) {

    var ObjCotizacionCa = {
        Solicitud_Id:$("#ContentPlaceHolder1_hdSolicitud").val(),
        Cotizacion_Id: idCotizacion,
        Cot_Estado_Id: idEstado,
        Aud_UsuarioActualizacion_Id: $("#ContentPlaceHolder1_hdUsuario").val()
    };

    $.ajax({
        async: false,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Compras/OrdenCompra.aspx/CambiarEstadoCotizacionCabecera',
        dataType: "json",
        data: "{ObjCotizacionCa:" + JSON.stringify(ObjCotizacionCa) + "}",
        success: function (data) {
            // alert(data.d);
            if (data.d != null) {

            }
        }
    });
}

function EnviarCotizacion(idCotizacion, CotMontoTotal) {
    if (CotEnviada) {
        Mensaje("Mensaje", "La cotizacion ya fue enviada, no se puede enviar por segunda vez.");
    }else {
        $("#ddlTipoSolicitud").val(0);
        $("#txtMontoSolAnt").val("");
        $("#txtMontoTotal").prop('disabled', true);
        $("#txtMontoTotal").val(CotMontoTotal);
        $("#dvEnviarCotizacion").dialog({
            autoOpen: true,
            show: "blind",
            width: 360,
            //height: 200,
            modal: true,
            resizable: false,
            title: "Enviar Cotizacion",
            buttons: {
                "Enviar Cotizacion Ganadora": function () {
                    Mensaje("Mensaje", "La cotizacion fue enviada.");
                    CambiarEstadoCotizacionCabecera(idCotizacion, 50);
                    CotEnviada = true;
                    CargarCotizaciones($("#ContentPlaceHolder1_hdSolicitud").val());
                    //BloquearCotizacionesPerdedoras();
                    $("#dvEnviarCotizacion").dialog('close');
                    $(".trSolAnticipo").hide();
                    $("#chkbSolAnticipo").prop('checked', false);
                },

                "Cerrar": function () {
                    ConfirmacionMensaje("Confirmacion", '¿Desea salir sin enviar la cotizacion?', function () {
                        $("#dvEnviarCotizacion").dialog('close');
                        $(".trSolAnticipo").hide();
                        $("#chkbSolAnticipo").prop('checked', false);
                    });
                }
            },
            open: function (event, ui) {

                $('.ui-dialog-buttonpane button:contains("Enviar Solicitud de Anticipo")').button().hide();
                $('.ui-dialog-buttonpane').find('button:contains("Enviar Cotizacion Ganadora")').addClass('boton');
                $('.ui-dialog-buttonpane').find('button:contains("Enviar Cotizacion Ganadora")').css('width','200px');
                $('.ui-dialog-buttonpane').find('button:contains("Enviar Solicitud de Anticipo")').addClass('boton');
                $('.ui-dialog-buttonpane').find('button:contains("Enviar Solicitud de Anticipo")').css('width', '230px');

                $('.ui-dialog-buttonpane').find('button:contains("Cerrar")').addClass('boton');
                $(".ui-dialog-titlebar-close", ui.dialog).hide();
            }
        });
    }
    
}
function cargarRAP(idCotizacion) {
    
    var ObjSolicitudAnticipo = {
        Cotizacion_Id: idCotizacion
        //Solicitud_Anticipo_Id: $("#hdRAP").val(),                                 
    }
    $.ajax({
        async: false,
        type: "POST",
        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/cargarRAP',
        data: "{ObjSolicitudAnticipo:" + JSON.stringify(ObjSolicitudAnticipo) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            if (data.d != null) {
                //alert('if (data.d != null) { \n data.d.Solicitud_Anticipo_Id=' + data.d.Solicitud_Anticipo_Id);
                //alert('data.d.Sol_Monto_Solicitado_nu=' + data.d.Sol_Monto_Solicitado_nu);
                var varSolicitudId = data.d.Solicitud_Anticipo_Id;
                $("#hdRAP").val(varSolicitudId);
                $("#ddlTipoSolicitud").val(data.d.Sol_Tipo_Anticipo_Id);
                $("#txtMontoTotal").val(0);
                $("#txtMontoSolAnt").val(data.d.Sol_Monto_Solicitado_nu);
            }

            else {
               
                Mensaje('Mensaje', 'Error en lectura de Requerimiento de Anticipo de Proveedor');
            }
        },
        error: function (data) {
            alert('Error al procesar los datos ...');
        }
    })
    //$("#iVisualizarRAP").display(true);
    $("#iVisualizarRAP").show();
    
}

function visualizarRAP() {
       
    $("#dvRAP").dialog(
{
    autoOpen: true,
    show: "blind",
    resizable: false,
    draggable: true,
    height: 'auto',
    width: 500,
    modal: true,
    closeOnEscape: true,
    //dialogClass: 'hide-close',
    title: "Requerimiento de Anticipo Proveedor "
});
    DeshabilitarCamposRAP();
}

function validarTotalvs() {
   
    var varTotalOK = true;
    if (varTotalDetalleCot < $("#txtMonto").val()) {
       
        Mensaje('Aviso', 'El valor total del detalle es menor que el monto total de la cotización');
        varTotalOK = false;
    }
    else if (varTotalDetalleCot > $("#txtMonto").val()) {
        
        Mensaje('Aviso', 'El valor total del detalle es mayor que el monto total de la cotización');
        varTotalOK = false;

    }
    else {
       
        //Mensaje('Aviso', 'else {');

        varTotalOK = true;
    }
    return varTotalOK;
}

//function cargarEstadosCotDet()
//{
//    alert('function cargarEstadosCotDet()');
//    $.ajax({

//        async: true,
//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: '../../Logistica/Compras/CotizacionRequerimiento.aspx/ListarEstadoxCA',
//        //data: "{ObjUsuario:" + JSON.stringify(ObjUsuario) + "}",
//        data: "",
//        success: function (data) {
//            alert('success: function (data) {');
//            if (data.d != null) {
//            }
//        }
//        ,
//        error: function (data) {
//            alert('Error al procesar los datos ...cargarEstadosCotDet');
//        }
//    });
//}

