﻿$(document).ready(function () {

    //comentaruo
    InicializarControles();

    //$("#btnBuscar").click(function () {
    //    ListarAlmacenes();
    //});

    //$("#btnCargarItems").click(function () {
    //   // ListarItems_Ma();
        
    //});

    $("#btnSalirDetalle").click(function () {
        $("#ItemMa-Reg").dialog('close');
    });

    $("#btnCargarItems").click(function () {
        //CargarBandejaItemsLibres($("#ContentPlaceHolder1_hdfProyecto").val());
        //$("#dialog_ItemsMa").dialog("open");
        CargarItemMa_Generales();
        CargarListadeItemMaDisponibles($("#ContentPlaceHolder1_hdfProyecto").val());
    });
    $("#btnBuscar_Disponibles").click(function () {
        //ListarItems_Ma();
        //CargarItemMa_Generales();
       
        CargarListadeItemMaDisponibles($("#ContentPlaceHolder1_hdfProyecto").val());

    });
    $("#btnBuscarItemMaAsignados").click(function () {
        //ListarItems_Ma();
        //CargarItemMa_Generales();

        CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val());

    });
    $("#btnLimpiarBusqueda").click(function () {
        LimpiarBusquedaItemMaAsignados();

    });
    $("#btnLimpiar_Seleccion").click(function () {
        //LimpiarBusquedaItemMaAsignados();
        LimpiarBusquedaItemMaDisponibles();
    });

    $("#btnSalir_Seleccion").click(function () {
        //LimpiarBusquedaItemMaAsignados();
        $("#dialog_ItemsMa").dialog('close');
        LimpiarBusquedaItemMaDisponibles();
    });

    /*----Botones para Asignar o Desasignar ItemMA seleccionados a Proyecto------*/

    $('#btnAgregarSeleccionados').click(function () {
        if (ItemsSeleccionados.length == 0)
        {
            // alert('tiene que seleccionar al menos un item');
            Mensaje("Mensaje", "Tiene que seleccionar al menos un item");
        }
        else {
            var seleccionados = ItemsSeleccionados;
            AsignarDesignarItemMA(seleccionados, true);
        }
    });
    $("#btnAgregarTodos").click(function () {
        AsignarDesignarItemMATODOS(true);
    });
    $("#btnQuitarSeleccionados").click(function () {
        if (ItemsSeleccionados.length == 0) {
            // alert('tiene que seleccionar al menos un item');
            Mensaje("Mensaje", "Tiene que seleccionar al menos un item");
        }
        else {
        var seleccionados = ItemsSeleccionados;
        AsignarDesignarItemMA(seleccionados, false);
    }
    });
    $("#btnQuitarTodos").click(function () {
        AsignarDesignarItemMATODOS(false);
    });




});

//------------ Variables globales ------------//
var ItemsSeleccionados = [];
var ItemsSeleccionadosQuitar = [];
var ItemsSolicitados = 0;
var NumerodeStockMinimo;
var IdItemdeStock;
var IdProyectoItem;
///////////////////////////////////////////////
var rowsJqGrid=[];
var numerofilaseleccionadaJqGrid;
var rowJqGrid;

var filagrillatodaspaginas;
var filagrillasolopagpresente;
var columnagrilla;

//--------------------------------------------//

function InicializarControles() {

    CapturarDatosdeProyecto();
    CargarComboFamilia("#ddlFamiliaID");
    CargarComboMarca("#ddlMarcaID");

    CargarComboFamilia("#ddlFamilia_Seleccion");
    CargarComboMarca("#ddlMarca_Seleccion");

    CargarComboFamilia("#ddlFamilia_Detalle");
    CargarComboMarca("#ddlMarca_Detalle");

    CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val());
}

function CapturarDatosdeProyecto(PROY) {
    var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();
    var ObjProyecto = {
        Proyecto_Id: PROY
    }

    $.ajax({
        async: false,
        type: "POST",
        url: '../../Logistica/Almacen/StockMinimo.aspx/CargarDatosProyecto',
        data: "{ObjProyecto:" + JSON.stringify(ObjProyecto) + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d != null) {
                var ProyectoID = data.d.Proyecto_Id;
                //alert('Id del Proyecto - CAPTURAR DATOS PROYECTO' + IDProyecto);

                document.getElementById('LBRUC').innerHTML = data.d.Emp_DocIdent_RUC_nu;
                document.getElementById('LBEmpresa').innerHTML = data.d.Emp_RazonSocial_Tx;
                document.getElementById('LBProyecto').innerHTML = data.d.Pry_Nombre_Tx;
              
            }

            else {
                alert('else {');
                Mensaje('Mensaje', 'Error en lectura de los datos del proyecto');
            }
        },
        error: function (data) {
            alert('Error al procesar los datos ...');
        }
    })


}


function CargarComboFamilia(control) {

    var ObjFamilia = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/ItemMa.aspx/ListarFamiliaCombo',
        data: "{ObjFamilia:" + JSON.stringify(ObjFamilia) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Fam_Nombre_Tx + "' value='" + value.Familia_Id + "'>" + value.Fam_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboMarca(control) {

    var ObjMarca = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/ItemMa.aspx/ListarMarcaCombo',
        data: "{ObjMarca:" + JSON.stringify(ObjMarca) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Mar_Nombre_Tx + "' value='" + value.Marca_Id + "'>" + value.Mar_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarItemMa_Generales() {
    $("#dialog_ItemsMa").dialog({
        autoOpen: false,
        width: 1250,//'auto',//900,
        height: 500,//'auto',//300,
        title: "Seleccionar Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false,
        //closeOnEscape: false,
        //    show: {
        //        effect: "fade",
        //        duration: 1000
        //    },
            open: function (event, ui) {
            }
        });
        $("#dialog_ItemsMa").dialog('open');
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    //});
    return false;
}



function ListarItems_Ma() {
    var myidselect = [];
    var myselectedrows;

    var ObjItem_Ma = {
        IMa_Nombre_Tx: $('#txtNombreItemMa_Busq').val(),
        Familia_Id: $('#ddlFamilia_Busq').val(),
        Marca_Id: $('#ddlMarca_Busq').val(),
        Aud_Estado_Fl:true
    };

    $("#divItemsMa").html('<table id="gvItemsMa"></table><div id="pieItemsMa"></div>');
    $("#gvItemsMa").jqGrid({
        regional: 'es',
        url: '../../Logistica/Almacen/StockMinimo.aspx/ListarItemsMa',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjItem_Ma:" + JSON.stringify(ObjItem_Ma) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Detalle', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        //colNames: ['Detalle', 'Stock Min', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        colModel: [
           //{ name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
            { name: 'Detalle', index: 'Detalle', width: 50, align: 'center', formatter: 'actionFormatterDetalle', search: false },
         
            //{ name: 'Seleccion', index: 'Seleccion', width: 60, align: 'center', formatter: 'actionFormatterSeleccionarItem', search: false },
            //{ name: 'Stock_Minimo', index: 'Stock_Minimo', width: 80, align: 'center', editable: true, hidden: false },
           { name: 'IMa_Co', index: 'IMa_Co', width: 120, align: 'center' },
           { name: 'IMa_Nombre_Tx', index: 'IMa_Nombre_Tx', width: 300, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 120, align: 'center' },
           { name: 'IMa_Metrado_Nu', index: 'IMa_Metrado_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 }, hidden: true },
           { name: 'IMa_Metrado_tx', index: 'IMa_Metrado_tx', width: 120, align: 'center' },
           { name: 'Fam_Nombre_Tx', index: 'Fam_Nombre_Tx', width: 120, align: 'center' },
           { name: 'Mar_Nombre_Tx', index: 'Mar_Nombre_Tx', width: 150, align: 'center', },
           { name: 'IMa_Id', index: 'IMa_Id', width: 150, align: 'center', hidden: true },
            { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', width: 150, align: 'center', hidden: true },
            { name: 'Familia_Id', index: 'Familia_Id', width: 150, align: 'center', hidden: true },
            { name: 'Marca_Id', index: 'Marca_Id', width: 150, align: 'center', hidden: true },
            { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },
            

        ],
        pager: "#pieItemsMa",
        //autowidth: true,
        viewrecords: true,
        width: 1100,
        height: 'auto',
        sortorder: 'asc', //Aumentado por sac
        cellEdit: true,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        loadui: "block", //Aumentado por sac
        sortname: 'IMa_Id',
        sortable: true,  //Aumentado por sac
        paging: true, //Aumentado por sac
        shrinkToFit: false,//Aumentado por sac
        multiselect: true,//Aumentado por sac
        prmNames: { nd: null, search: null },//Aumentado por sac

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },

        /// Aumentado por SAC *---- onSelectRow
        onSelectRow: function (id, isSelected) {
            if (myidselect.length != 0) {
                if (isSelected == false) {
                    for (var i = 0; i < myidselect.length; i++) {
                        if (id == myidselect[i]) {
                            //alert("Arreglo actual: " + myidselect);
                            //alert("Deberia eliminar: " + myidselect[i]);
                            myidselect.splice(i, 1);
                            //alert("Arreglo despues de eliminar: " + myidselect);
                        }
                    }
                }
            } else {
                //alert("Arreglo vacio");
            }

        },

        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Items',
        loadComplete: function (data) {

            var rows = jQuery("#gvItemsMa").jqGrid('getRowData');
            //var row = rows[0];
            //alert(row['UsuarioSubId']);
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                var nombreId = row['IMa_Id'];
                for (var j = 0; j < myidselect.length; j++) {
                    //alert(nombreId + " es igual a " + myidselect[j]);
                    if (myidselect[j] == nombreId) {
                        //alert("Marca: " + myidselect[j]);
                        $("#gvItemsMa").setSelection(nombreId, true);
                    }
                }
            }

        },
        onPaging: function () {
            var myselect = $("#gvItemsMa").jqGrid('getGridParam', 'selarrrow');
            for (var i = 0; i < myselect.length; i++) {
                var rep = 0;
                if (myidselect.length > 0) {
                    for (var j = 0; j < myidselect.length; j++) {
                        if (myselect[i] == myidselect[j]) {
                            rep++;
                        }
                    }
                    if (rep == 0) { myidselect.push(myselect[i]); }
                    else { rep = 0; }
                }
                else {
                    myidselect.push(myselect[i]);
                }
            }
        },



        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });

    




    $.extend($.fn.fmatter, {
        

        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }


        },

        //actionFormatterSeleccionarItem: function (cellvalue, options, rowObject) {

        //    //alert('Seleccion');
        //    //return '<i title=\"Click para el regitro\" style=\"cursor:pointer\" class="fa fa-check-square-o"></i>';  //class="fa fa-check-square
        //    if (rowObject.Aud_Estado_Fl == true) {
        //        return '<i title=\"Click para seleccionar el item\" onclick=\"SeleccionarItem(this)\" style=\"cursor:pointer\" class="fa fa-check-square-o" ></i>';
        //        //     alert('a');
        //    }
        //    else
        //        //class="fa fa-trash-o
        //        if (rowObject.Aud_Estado_Fl == false) {
        //            return '<i title=\"No se puede seleccionar el item\" style=\"cursor:pointer\" class="fa fa-check-square-o"></i>';
        //            //     alert('b');
        //        }

        //}





        //actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
        //    //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
        //    return '<i title=\"Click para Seleccionar\" style=\"cursor:pointer\" class="fa fa-hand-pointer-o"></i>';
        //  }

    });




    return myidselect;



}



function MostrarCamposDeshabilitados() {
    $("#txtCodigo").attr('disabled', true);

    //$('#txtCodigo').attr("disabled", "disabled");
    $("#txtNombre").attr('disabled', true);
    $("#txtDescripcion").attr('disabled', true);
    $("#txtMetrado").attr('disabled', true);
    $("#ddlUnidadMedida_Detalle").prop('disabled', true);
    $("#ddlFamilia_Detalle").prop('disabled', true);
    $("#ddlMarca_Detalle").prop('disabled', true);

}

function MostrarCamposHabilitados() {
    $("#txtCodigo").attr('disabled', false);
    $("#txtNombre").attr('disabled', false);
    $("#txtDescripcion").attr('disabled', false);
    $("#txtMetrado").attr('disabled', false);
    $("#ddlUnidadMedida").attr('disabled', false);
    $("#ddlFamilia").attr('disabled', false);
    $("#ddlMarca").attr('disabled', false);
}

/* Nuevas Funciones - */
//SAC
//$("#btnAsignacionUsuarios").click(function () {
//    CargarBandejaUsuariosLibres($("#ddlUsuarioJefeCon option:selected").val().valueList(), $("#ddlPerfilCon option:selected").val());
//    $("#frmAsignarUsuarios").dialog("open");
//});
//SEC - 2 VECES se llama a la funcion con fines de ejemplo - Luego se eliminará


var idProyecto = $("#ContentPlaceHolder1_hdfProyecto").val();



function CargarBandejaItemsLibres(codigoProyecto) {
    //ListarUsuariosLibres(codigoPerfil);
    alert('Cargar Bandeja Usuarios Libres');
    var MiArray = ListarUsuariosLibres(codigoProyecto);;
    $("#dialog_ItemsMa").dialog({

        autoOpen: false,
        resizable: false,
        draggable: true,
        height: '500',
        width: '750',
        modal: true,
        buttons: {
            Aceptar: function () {
                //alert(sList.length);
                var idx = 0;
                var map = new Object();
                //$("#divItemsMa").html('<table id="gvItemsMa"></table><div id="pieItemsMa"></div>');
                //$("#gvItemsMa").jqGrid({
                var grid = $("#gvItemsMa");
                //1. Obtenemos los indices sleccionados
                $('input[class$=cbox]', grid).each(function () {
                    var sThisVal = (this.checked ? "1" : "0");

                    if (sThisVal == true) {

                        map[idx] = 1;
                    }

                    idx++;

                });
                var sList = MiArray;
                //alert("Este es el arreglo: " + MiArray);
                idx = 0;
                $("tr", grid).each(function () {

                    $this = $(this)
                    var value = $this.find("td:nth-child(2)").html();

                    if (value.length > 0) {

                        if (map[idx] != null) {
                            if (sList.length != 0) {
                                var existe = false;
                                for (var i = 0; i < MiArray.length; i++) {
                                    if (value == MiArray[i]) { existe = true; }
                                }
                                if (!existe) {
                                    sList.push(value);
                                }
                            }
                            else {
                                sList.push(value);
                            }
                        }

                        idx++;
                    }
                    return;
                });
                if (sList.length != 0) {
                    var asignacionusuario =
                    {
                        Ids: sList.toString(),
                        UsuarioCreacionId: $("#ContentPlaceHolder1_hdUsuario").val(),
                        Proyecto: codigoProyecto
                    };

                    $.ajax({
                        type: 'GET',
                        url: BASE_URL + '/Service/Mantenimiento.svc/CrearAsignacionItem',
                        data: { asignacionusuario: JSON.stringify(asignacionusuario) },
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            Mensaje("Mensaje", "El usuario se asignó correctamente")
                            GenerarBandeja('asc');
                        },
                        error: function (msg) {
                            Mensaje("Aviso", "Ocurrió un error en la asignación");
                        }

                    });
                }
                $(this).dialog("close");
                //3. Cargar la lista
            },
            Cancelar: function () {
                $(this).dialog("close");
            }
        },
        show: {
            effect: "scale",
            duration: 500
        },
        hide: {
            effect: "scale",
            duration: 500
        },

        close: function () {
        }
    });
}




function CargarListadeItemMaDisponibles(id) {
    var myidselect = [];
    var myselectedrows;

    var proyecto = {
        Proyecto_Id: id,
        Aud_Estado_Fl: true
        ,IMa_Co: $('#txtCodigo_Seleccion').val()
        ,IMa_Nombre_Tx: $('#txtNombre_Seleccion').val()
        , Familia_Id: $('#ddlFamilia_Seleccion').val()
        , Marca_Id: $('#ddlMarca_Seleccion').val()
    };

    $("#divItemsMaDisponibles").html('<table id="gvItemsMaDisponibles"></table><div id="pieItemsMaDisponibles"></div>');
    $("#gvItemsMaDisponibles").jqGrid({
        regional: 'es',
        url: '../../Logistica/Almacen/StockMinimo.aspx/ListarItemMaDisponible',
        datatype: "json",
        mtype: "POST",
        postData: "{ProyectoItemMa:" + JSON.stringify(proyecto) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Detalle', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        //colNames: ['Detalle', 'Stock Min', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        colModel: [
           //{ name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
            { name: 'Detalle', index: 'Detalle', width: 50, align: 'center', formatter: 'actionFormatterDetalle', search: false },

            //{ name: 'Seleccion', index: 'Seleccion', width: 60, align: 'center', formatter: 'actionFormatterSeleccionarItem', search: false },
            //{ name: 'Stock_Minimo', index: 'Stock_Minimo', width: 80, align: 'center', editable: true, hidden: false },
           { name: 'IMa_Co', index: 'IMa_Co', width: 120, align: 'center' },
           { name: 'IMa_Nombre_Tx', index: 'IMa_Nombre_Tx', width: 300, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 120, align: 'center' },
           { name: 'IMa_Metrado_Nu', index: 'IMa_Metrado_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 }, hidden: true },
           { name: 'IMa_Metrado_tx', index: 'IMa_Metrado_tx', width: 120, align: 'center' },
           { name: 'Fam_Nombre_Tx', index: 'Fam_Nombre_Tx', width: 120, align: 'center' },
           { name: 'Mar_Nombre_Tx', index: 'Mar_Nombre_Tx', width: 150, align: 'center', },
           { name: 'IMa_Id', index: 'IMa_Id', width: 150, align: 'center', hidden: false },
            { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', width: 150, align: 'center', hidden: true },
            { name: 'Familia_Id', index: 'Familia_Id', width: 150, align: 'center', hidden: true },
            { name: 'Marca_Id', index: 'Marca_Id', width: 150, align: 'center', hidden: true },
            { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },


        ],
        pager: "#pieItemsMaDisponibles",
        //autowidth: true,
        viewrecords: true,
        width: 1215,
        height: 'auto',
        //sortorder: 'asc', //Aumentado por sac
        //cellEdit: true,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        loadui: "block", //Aumentado por sac
       // sortname: 'IMa_Id',
        sortable: true,  //Aumentado por sac
        paging: true, //Aumentado por sac
        //shrinkToFit: false,//Aumentado por sac
        multiselect: true,//Aumentado por sac
        prmNames: { nd: null, search: null },//Aumentado por sac

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },

        /// Aumentado por SAC *---- onSelectRow
        onSelectRow: function (id, isSelected) {
          //  alert('onSelectRow: function (id, isSelected) { id:' + id + '  isSelected: ' + isSelected);
            var existe = 0;
            ItemsSeleccionados = [];

            if (myidselect.length > 0)
            //{
                //if (isSelected == false) { //No contemplado
                    for (var i = 0; i < myidselect.length; i++) {
                        if (id === myidselect[i]) {
                            //alert("Arreglo actual: " + myidselect);
                            //alert("Deberia eliminar: " + myidselect[i]);
                           myidselect.splice(i, 1);
                            existe = 1; //sec
                            //alert("Arreglo despues de eliminar: " + myidselect);
                        }
                    }
              //  } //No contemplado
          //  } else { //No contemplado //alert("Arreglo vacio");
            //}  //No contemplado

            //--- Inserta los items no seleccionados: existe = 0  ---//
            if (existe === 0 && myidselect.length > 0) {
                myidselect.push(id);
            }
            else if (myidselect.length === 0) {
                myidselect.push(id);
            }
            // --- Para eliminar el Item seleccionado --- //
            if (myidselect.length > 0) {
                for (var i = myidselect.length; i--;) {
                    //alert("id: " + id + " / myidselect[i]: " + myidselect[i] + " / isSelected: " + isSelected);
                    if (myidselect[i] === id && isSelected == false) {
                        //alert("Se elimina id: " + id);
                        myidselect.splice(i, 1);
                    }
                }
            }
            for (var i = 0; i < myidselect.length; i++) {
                ItemsSeleccionados.push($("#gvItemsMaDisponibles").getLocalRow(myidselect[i]).IMa_Id);
              //  alert('ItemsSeleccionados:' + ItemsSeleccionados);
            }

        },


        onSelectAll: function (aRowids, status) {
           // alert('OnselectAll');
            ItemsSeleccionados = [];

            if (status) {
                var rows = jQuery("#gvItemsMaDisponibles").jqGrid('getRowData');
                var cbs = $("tr.jqgrow > td > input.cbox:disabled", $("#gvItemsMaDisponibles")[0]);
                cbs.removeAttr("checked");
                //alert('ABC');
                //modify the selarrrow parameter
                $("#gvItemsMaDisponibles")[0].p.selarrrow = $("#gvItemsMaDisponibles").find("tr.jqgrow:has(td > input.cbox:checked)")
                .map(function () { return this.id; }) // convert to set of ids
                .get(); // convert to instance of Array
                //alert('$("#gvItemsMaDisponibles")[0].p.selarrrow+ ESTO ES EL ARREGLO :  ' + $("#gvItemsMaDisponibles")[0].p.selarrrow);

               // alert('$("#gvItemsMaDisponibles").length rows ' + rows.length);



                for (var i = 0; i < rows.length; i++) {
                    ItemsSeleccionados.push(rows[i].IMa_Id);
                    //alert('ItemsSeleccionados - MODIFICADO: SELECCIONADOS : ' + ItemsSeleccionados);
                }


            }
        },



        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Items Disponibles',
        loadComplete: function (data) {
            //   alert('LoadComplete');
            var existe = 0;
            ItemsSeleccionados = [];

            var rows = jQuery("#gvItemsMaDisponibles").jqGrid('getRowData');
            //var row = rows[0];
            //alert(row['UsuarioSubId']);
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                var nombreId = row['IMa_Id'];
            
                for (var j = 0; j < myidselect.length; j++) {
                   // alert(nombreId + " es igual a " + myidselect[j]);
                    if (myidselect[j] == nombreId) {
                        //alert("Marca: " + myidselect[j]);
                        $("#gvItemsMaDisponibles").setSelection(nombreId, true);
                     //  alert('Todos' + $("#gvItemsMaDisponibles").setSelection(nombreId, true));
                    }
                }
            }
           

            for (var i = 0; i < myidselect.length; i++) {
                ItemsSeleccionados.push($("#gvItemsMaDisponibles").getLocalRow(myidselect[i]).IMa_Id);
                 //  alert('ItemsSeleccionados:' + ItemsSeleccionados);
            }

            ////////////////////////////////////////////////////////////////////////////////

            ////var myGrid = $("#gvItemsMaDisponibles");
            ////$("#cb_" + myGrid[0].id).hide();

            ////var allRecords = $("#gvItemsMaDisponibles").getGridParam('records');
            ////var rowNum = $("#gvItemsMaDisponibles").getGridParam('rowNum');
            ////var page = $("#gvItemsMaDisponibles").getGridParam('page');
            ////var rowArray = $("#gvItemsMaDisponibles").jqGrid('getDataIDs');
            ////var filas;


            ////if ((rowNum * page) > allRecords) {
            ////    //filas = rowNum - ((rowNum * page) - allRecords);
            ////    for (var i = (rowNum * (page - 1)) + 1 ; i <= allRecords ; i++) {
            ////        for (var j = 1; j <= myidselect.length ; j++) {
            ////            if (i == myidselect[j - 1]) {
            ////                //alert("Fila: " + i + " / Esta se chequea!");
            ////                $("#gvItemsMaDisponibles").setSelection(i, true);
            ////            }
            ////        }
            ////        //if ($("#gvItemsMaDisponibles").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
            ////        //    $("#" + (i), "#gvItemsMaDisponibles").find("td").css({ 'background-color': '#E3F6CE', 'font-weight': 'bold' });
            ////        //    $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
            ////        //}
            ////    }

            ////}
            ////else {
            ////    //filas = rowNum;
            ////    for (var i = (rowNum * (page - 1)) + 1 ; i <= (rowNum * page) ; i++) {
            ////        for (var j = 1; j <= myidselect.length ; j++) {
            ////            if (i == myidselect[j - 1]) {
            ////                //alert("Fila: " + i + " / Esta se chequea!");
            ////                $("#gvItemsMaDisponibles").setSelection(i, true);
            ////            }
            ////        }
            ////        //if ($("#gvItemsMaDisponibles").getLocalRow(i).RDe_Estado_Tx === 'SOLICITADO') {
            ////        //    $("#" + (i), "#gvGrdItemsRQ").find("td").css({
            ////        //        'background-color': '#E3F6CE', 'font-weight': 'bold', 'color': '#000'
            ////        //    });
            ////        //    $("#jqg_gvGrdItemsRQ_" + i.toString()).attr("disabled", true);
            ////        //}
            ////    }
            ////}
        },
        onPaging: function () {
            var myselect = $("#gvItemsMaDisponibles").jqGrid('getGridParam', 'selarrrow');
            for (var i = 0; i < myselect.length; i++) {
                var rep = 0;
                if (myidselect.length > 0) {
                    for (var j = 0; j < myidselect.length; j++) {
                        if (myselect[i] == myidselect[j]) {
                            rep++;
                        }
                    }
                    if (rep == 0) { myidselect.push(myselect[i]); }
                    else { rep = 0; }
                }
                else {
                    myidselect.push(myselect[i]);
                }
            }
        },



        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });






    $.extend($.fn.fmatter, {


        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle_Disponibles(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }


        },

        //actionFormatterSeleccionarItem: function (cellvalue, options, rowObject) {

        //    //alert('Seleccion');
        //    //return '<i title=\"Click para el regitro\" style=\"cursor:pointer\" class="fa fa-check-square-o"></i>';  //class="fa fa-check-square
        //    if (rowObject.Aud_Estado_Fl == true) {
        //        return '<i title=\"Click para seleccionar el item\" onclick=\"SeleccionarItem(this)\" style=\"cursor:pointer\" class="fa fa-check-square-o" ></i>';
        //        //     alert('a');
        //    }
        //    else
        //        //class="fa fa-trash-o
        //        if (rowObject.Aud_Estado_Fl == false) {
        //            return '<i title=\"No se puede seleccionar el item\" style=\"cursor:pointer\" class="fa fa-check-square-o"></i>';
        //            //     alert('b');
        //        }

        //}





        //actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
        //    //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
        //    return '<i title=\"Click para Seleccionar\" style=\"cursor:pointer\" class="fa fa-hand-pointer-o"></i>';
        //  }

    });




    return myidselect;



}


function CargarListadeItemMaAsignados(id) {
    var myidselect = [];
    var myselectedrows;
    var lastIndex;
    var lastsel2; //Ejemplo de Grid
    var proyecto = {
        Proyecto_Id: id,
        Aud_Estado_Fl: true,
        ///
        IMa_Co: $('#ICodigo').val(),
        IMa_Nombre_Tx: $('#INombre').val(),
        Familia_Id: $('#ddlFamiliaID').val(),
        Marca_Id: $('#ddlMarcaID').val(),
    };

    $("#divItemsMaAsignados").html('<table id="gvItemsMaAsignados"></table><div id="pieItemsMaAsignados"></div>');
    $("#gvItemsMaAsignados").jqGrid({
        regional: 'es',
        url: '../../Logistica/Almacen/StockMinimo.aspx/ListarItemMaAsignado',
        datatype: "json",
        mtype: "POST",
        postData: "{ProyectoItemMa:" + JSON.stringify(proyecto) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Detalle', 'Stock', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Ite_Pro_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        //colNames: ['Detalle', 'Stock Min', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        colModel: [
           //{ name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
            { name: 'Detalle', index: 'Detalle', width: 50, align: 'center', formatter: 'actionFormatterDetalle', search: false },

            //{ name: 'Seleccion', index: 'Seleccion', width: 60, align: 'center', formatter: 'actionFormatterSeleccionarItem', search: false },
            {
                //name: 'Ite_Pro_Stock', index: 'Ite_Pro_Stock', width: 80, align: 'center', editable: true, hidden: false,
                name: 'Ite_Pro_Stock_Min', index: 'Ite_Pro_Stock_Min', width: 80, align: 'center', editable: true, hidden: false,
                //editoptions: {}
                //editoptions: { onchange: 'grabarEdicion();',  }
                editoptions: { onkeyup: 'grabarEdicion(this)' }
                //editoptions: { onchange: 'grabarEdicion();', onkeyup: 'grabarEdicion()' }
                //editoptions: { edittype: 'select', dataUrl: 'StockMinimo.aspx', onkeyup: 'grabarEdicion();' }
                //editoptions: { dataEvents: [{ type: 'keyup', fn: function (e) { grabarEdicion2(); } }] }
            },
           { name: 'IMa_Co', index: 'IMa_Co', width: 120, align: 'center' },
           { name: 'IMa_Nombre_Tx', index: 'IMa_Nombre_Tx', width: 300, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 120, align: 'center' },
           { name: 'IMa_Metrado_Nu', index: 'IMa_Metrado_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 }, hidden: true },
           { name: 'IMa_Metrado_tx', index: 'IMa_Metrado_tx', width: 120, align: 'center' },
           { name: 'Fam_Nombre_Tx', index: 'Fam_Nombre_Tx', width: 120, align: 'center' },
           { name: 'Mar_Nombre_Tx', index: 'Mar_Nombre_Tx', width: 150, align: 'center', },
           { name: 'IMa_Id', index: 'IMa_Id', width: 150, align: 'center', hidden: false },
           { name: 'Ite_Pro_Id', index: 'Ite_Pro_Id', width: 150, align: 'center', hidden: false },
            { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', width: 150, align: 'center', hidden: true },
            { name: 'Familia_Id', index: 'Familia_Id', width: 150, align: 'center', hidden: true },
            { name: 'Marca_Id', index: 'Marca_Id', width: 150, align: 'center', hidden: true },
            { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },


        ],
        pager: "#pieItemsMaAsignados",
        //autowidth: true,
        viewrecords: true,
        width: 1290,
        height: 'auto',
        //  sortorder: 'asc', //Aumentado por sac
        // cellEdit: true,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        loadui: "block", //Aumentado por sac
        //  sortname: 'IMa_Id',
        sortable: true,  //Aumentado por sac
        paging: true, //Aumentado por sac
        //   shrinkToFit: false,//Aumentado por sac
        multiselect: true,//Aumentado por sac
        prmNames: { nd: null, search: null },//Aumentado por sac
        editurl: "",
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },

        /// Aumentado por SAC *---- onSelectRow
        //onSelectRow: function (id, isSelected) {
            
        //    alert('onSelectRow: \nid:' + id + '  \nisSelected: ' + isSelected + '  \ne: ' + jQuery("#gvItemsMaAsignados").jqGrid('getRowData'));
          
        //    var existe = 0;
        //    ItemsSeleccionados = [];
        //    if (myidselect.length > 0)
        //        //{
        //        //if (isSelected == false) { //No contemplado
        //        for (var i = 0; i < myidselect.length; i++) {
        //            if (id == myidselect[i]) {
        //                //alert("Arreglo actual: " + myidselect);
        //                //alert("Deberia eliminar: " + myidselect[i]);
        //                myidselect.splice(i, 1);
        //                //alert("Arreglo despues de eliminar: " + myidselect);
        //                existe = 1;
        //            }
        //        }
        //    // } //No contemplado
        //    //} else {  //No contemplado

        //    //alert("Arreglo vacio");
        //    //}

        //    //--- Inserta los items no seleccionados: existe = 0  ---//
        //    if (existe === 0 && myidselect.length > 0) {
        //        myidselect.push(id);
        //    }
        //    else if (myidselect.length === 0) {
        //        myidselect.push(id);
        //    }
        //    // --- Para eliminar el Item seleccionado --- //
        //    if (myidselect.length > 0) {
        //        for (var i = myidselect.length; i--;) {
        //            //alert("id: " + id + " / myidselect[i]: " + myidselect[i] + " / isSelected: " + isSelected);
        //            if (myidselect[i] === id && isSelected == false) {
        //                //alert("Se elimina id: " + id);
        //                myidselect.splice(i, 1);
        //            }
        //        }
        //    }
        //    //ItemsSeleccionados = myidselect;
        //    // --- Obtiene los Id's de requerimiento para el llenado de Items  SEC_SOLICITUD_DE --- //
        //    for (var i = 0; i < myidselect.length; i++) {
        //        ItemsSeleccionados.push($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
        //        //  alert('ItemsSeleccionados:osr' + ItemsSeleccionados);
        //    }

        //},
        //onUnselectAll: function (aRowids, status) { alert('onunselectall');},
        //onCheckAll


        //onCellSelect: function (rowid, index, contents, event) {





        onCellSelect: function (rowid, iCol, cellcontent, e) {
            //alert('onCellSelect: \nrowid: ' + rowid + '\n iCol: ' + iCol +
            //       '\n cellcontent: ' + cellcontent + '\n e: ' + e);

            var blnChecked = $("#jqg_gvItemsMaAsignados_" + rowid).is(":checked");
            //alert('blnChecked: ' + blnChecked);
            if (iCol == 0) {
                var existe = 0;
                ItemsSeleccionados = [];

                if (myidselect.length > 0) 
                    for (var i = 0; i < myidselect.length; i++) {
                        if (rowid == myidselect[i]) {
                         //   alert("Arreglo actual: " + myidselect);
                         //   alert("Deberia eliminar: " + myidselect[i]);

                            myidselect.splice(i, 1);
                            existe = 1;
                        //    alert("Arreglo despues de eliminar: " + myidselect);
                            $('jqg_gvItemsMaAsignados_' + myidselect[i].rowid).prop('checked', true); //Aumentado 211015
                            
                        }
                    }
                
                //--- Inserta los items no seleccionados: existe = 0  ---//
                if (existe === 0 && myidselect.length > 0) {
                    myidselect.push(rowid);
                }
                else if (myidselect.length === 0) {
                    myidselect.push(rowid);
                }
                //// --- Para eliminar el Item seleccionado --- //
                   
                //if (myidselect.length > 0) {
                //            for (var i = myidselect.length; i--;) {
                //                //alert("id: " + id + " / myidselect[i]: " + myidselect[i] + " / isSelected: " + isSelected);
                //                if (myidselect[i] === rowid && blnChecked === false) {
                //                    alert("Se elimina id: " + rowid);
                //                    myidselect.splice(i, 1);
                //                }
                //            }
                //        }

                // --- Obtiene los Id's de requerimiento para el llenado de Items  SEC_SOLICITUD_DE --- //
                for (var i = 0; i < myidselect.length; i++) {
                    ItemsSeleccionados.push($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
                   // $('jqg_gvItemsMaAsignados_' + $("#gvItemsMaAsignados").getLocalRow(myidselect[i]).rowid).prop('checked', true); //Aumentado 211015
                    //alert($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
                    //   alert('ItemsSeleccionados:osr' + ItemsSeleccionados);
                   // alert($("#gvItemsMaAsignados").myidselect[i].rowid);
                }
            } //else if (iCol == 0 && blnChecked === false) {
            //    $("#jqg_gvItemsMaAsignados_" + rowid).prop('checked', false);

            //}
            else {
                //var filamarcada = 0;
                //alert('No se escogio item' + rowid);
                blnChecked === false;
                //alert('jqg_gvItemsMaAsignados_' + rowid);
                $('jqg_gvItemsMaAsignados_' + rowid).prop('checked', false);
               // $("#jqg_gvItemsMaAsignados_" + rowid).attr('checked', false); 
                //                $("#jqg_gvItemsMaAsignados_" + rowid).removeAttr('checked');

               // $("#jqg_gvItemsMaAsignados_" + rowid).filter(':checkbox').prop('checked', false);
                //$("#jqg_gvItemsMaAsignados_" + rowid).prop('checked', blnChecked);
            }
        },

        onSelectAll: function (aRowids, status) {
            //  alert('OnselectAll');
            ItemsSeleccionados = [];
           
            if (status) {
                // uncheck "protected" rows
                var rows = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');

                var cbs = $("tr.jqgrow > td > input.cbox:disabled", $("#gvItemsMaAsignados")[0]);
                cbs.removeAttr("checked");
                //  alert('ABC');
                //modify the selarrrow parameter
                $("#gvItemsMaAsignados")[0].p.selarrrow = $("#gvItemsMaAsignados").find("tr.jqgrow:has(td > input.cbox:checked)")
                .map(function () { return this.id; }) // convert to set of ids
                .get(); // convert to instance of Array
                // alert('$("#gvItemsMaAsignados")[0].p.selarrrow+ ESTO ES EL ARREGLO :  ' + $("#gvItemsMaAsignados")[0].p.selarrrow);

                // alert('$("#gvItemsMaAsignados")[0].p.selarrrow+ ESTO ES EL ARREGLO - ID ITEMMA :  ' + $("#gvItemsMaAsignados")[0].p.selarrrow(myidselect[i]).IMa_Id);
                //var row = rows[i];
                //alert('ROW' + row);
                //var nombreId = row['IMa_Id'];
                //alert('ID - son 10 -' + nombreId);

                //ESTO ES LA POSICION
                //ItemsSeleccionados.push($("#gvItemsMaAsignados")[0].p.selarrrow);
                //alert('ItemsSeleccionados' + ItemsSeleccionados);


                ///
                //  alert('$("#gvItemsMaAsignados").length rows ' + rows.length);
               


                for (var i = 0; i < rows.length; i++) {
                    ItemsSeleccionados.push(rows[i].IMa_Id);
                    // alert('ItemsSeleccionados - MODIFICADO: SELECCIONADOS : ' + ItemsSeleccionados);
                }


            }
            //var existe = 0;
            //ItemsSeleccionados = [];
            //var rows = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');
            ////var row = rows[0];
            ////alert(row['UsuarioSubId']);

            ////OSR
            ////if (myidselect.length > 0)
            ////    //{
            ////    //if (isSelected == false) { //No contemplado
            ////    for (var i = 0; i < myidselect.length; i++) {
            ////        if (id == myidselect[i]) {
            ////            //alert("Arreglo actual: " + myidselect);
            ////            //alert("Deberia eliminar: " + myidselect[i]);
            ////            myidselect.splice(i, 1);
            ////            //alert("Arreglo despues de eliminar: " + myidselect);
            ////            existe = 1;
            ////        }
            ////    }

            ////LC
            //for (var i = 0; i < rows.length; i++) {
            //    var row = rows[i];
            //    var nombreId = row['IMa_Id'];
            //    alert('ID - son 10 -' + nombreId);
            //    if (myidselect.length =! 0)

            //        if (aRowids == myidselect[i]) {
            //                    alert("Arreglo actual: " + myidselect);
            //                    //alert("Deberia eliminar: " + myidselect[i]);
            //                    myidselect.splice(i, 1);
            //                    //alert("Arreglo despues de eliminar: " + myidselect);
            //                    existe = 1;
            //                }

            //    //for (var j = 0; j < myidselect.length; j++) {
            //    //  //  alert(nombreId + " es igual a " + myidselect[j]);
            //    //    //alert('B');
            //    //    if (myidselect[j] == nombreId) {
            //    //        //alert("Marca: " + myidselect[j]);
            //    //        $("#gvItemsMaAsignados").setSelection(nombreId, true);
            //    //    }
            //    //}
                
            //}
            //for (var i = 0; i < myidselect.length; i++) {
            //    ItemsSeleccionados.push($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
            //    alert('ItemsSeleccionados todos row - ONSELECT ALL:' + ItemsSeleccionados);
            //}
            ////for (var i = 0; i < myidselect.length; i++) {
            ////    ItemsSeleccionados.push($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
            ////    alert('ItemsSeleccionados todos row - ONSELECT ALL:' + ItemsSeleccionados);
            ////}

        },

        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Items Asignados',
        loadComplete: function (data) {
            var existe = 0;
            ItemsSeleccionados = [];

            
            var rows = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');
          //  var rowid = jQuery("#gvItemsMaAsignados").jqGrid('getRowid');

            for (var j = 0; j < myidselect.length; j++) {
                $("#gvItemsMaAsignados").setSelection(myidselect[j], true);
            }

            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                //alert('ROW : '+row+'\nFila0 : '+row[0]);

               // $('jqg_gvItemsMaAsignados_' + row).prop('checked', true);

                var nombreId = row['IMa_Id'];
                //for (var j = 0; j < myidselect.length; j++) {
                //    //alert(nombreId + " es igual a " + myidselect[j]);
                //    if (myidselect[j] == nombreId) {
                //        //alert("Marca: " + myidselect[j]);
                //        $("#gvItemsMaAsignados").setSelection(nombreId, true);
                //        // $('jqg_gvItemsMaAsignados_' + myidselect[i].rowid).prop('checked', true); //Aumentado 211015
                //      //  alert(myidselect[j].rowid);

                //    }
                //}
            }
            for (var i = 0; i < myidselect.length; i++) {
                //Mantiene los Items Seleccionados de la pagina anterior - pero no pinta el check correspondiente de cada uno

                ItemsSeleccionados.push($("#gvItemsMaAsignados").getLocalRow(myidselect[i]).IMa_Id);
                 // alert('ItemsSeleccionados onselect row:' + ItemsSeleccionados);
            }
        },
        onPaging: function () {
            var myselect = $("#gvItemsMaAsignados").jqGrid('getGridParam', 'selarrrow');
            for (var i = 0; i < myselect.length; i++) {
                var rep = 0;
                if (myidselect.length > 0) {
                    for (var j = 0; j < myidselect.length; j++) {
                        if (myselect[i] == myidselect[j]) {
                            rep++;
                        }
                    }
                    if (rep == 0) { myidselect.push(myselect[i]); }
                    else { rep = 0; }
                }
                else {
                    myidselect.push(myselect[i]);
                }
            }
        },

        //ondblClickRow: function (rowid, iRow, iCol) 
        //{
        //    alert('Doble click');

        //    var rows = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');

        //    //var cbs = $("tr.jqgrow > td > input.cbox:disabled", $("#gvItemsMaAsignados")[0]);

        //    jQuery("#gvItemsMaAsignados");
        //editCell(Ite_Pro_Stock, true);
        //  //  cellEdit: true,
        //},

        //ondblClickRow: function (obj)
        //{
        //    alert('Doble clickclick');
        //    //var row_id = $ ("#gvItemsMaAsignados").getGridParam('selrow');
        //    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
        //    var rowData = $("#gvItemsMaAsignados").getRowData(RowId);
        //    var dato = rowData.Ite_Pro_Stock;

        //    alert('Dato: '+dato);
        //    editCell(dato, true);
        //    //jQuery ('#gvItemsMaAsignados').editRow(row_id,true);
        //   // jQuery('#gvItemsMaAsignados').editRow(rowData.Ite_Pro_Stock, true);
        //},
        cellsubmit: 'clientArray',//'remote',//'clientArray',//'remote',//'clientArray',
        //ondblClickRow: function (id, row, col)
        //{
        //    alert('Doble clickclick');
        //    //alert('lastIndex  B' + col);
        //    //alert('rowid B' + row);
        //    //alert('id B' + id);
        //    var $this = $(this);
        //    $this.jqGrid('setGridParam', { cellEdit: true });
        //    $this.jqGrid('editCell', iRow, col, true);
        //    $this.jqGrid('setGridParam', { cellEdit: false });

        //},
        //caption: "Input Types",
        ondblClickRow: function (rowid, iRow, iCol) {
            var $this = $(this);
           // var itemmodificadoarreglo=[];
            $this.jqGrid('setGridParam', { cellEdit: true });
            $this.jqGrid('editCell', iRow, iCol, true);
            $this.jqGrid('setGridParam', { cellEdit: false });
            
            //Alertas de ejemplo
           // alert('Numero de fila en la grilla de todas las paginas: ' + rowid); 
           // alert('Numero de fila en la pagina actual - sin contar las anteriores: ' + iRow);
            //alert('Numero de columna: ' + iCol);
            
            filagrillatodaspaginas = rowid;
            filagrillasolopagpresente = iRow;
            columnagrilla = iCol;

            var rows = $this.jqGrid('getRowData');
            var numerofilaseleccionada = iRow - 1;
            var row = rows[numerofilaseleccionada];
           // alert('ROW' + row);
            var nombreId = row['IMa_Id'];
            var PKProyectoItem = row['Ite_Pro_Id'];

            rowsJqGrid = rows;
            numerofilaseleccionadaJqGrid = numerofilaseleccionada;
            rowJqGrid = row;

            IdItemdeStock = nombreId;
            IdProyectoItem = PKProyectoItem;
         //   alert('ID del seleccionado-' + nombreId); //ID DEL ITEM QUE SE DIO DOBLE CLICK
           // nombreId.focus();

            //if (rowid && rowid !== lastsel2) {
            //    $this.jqGrid('restoreRow', lastsel2);
            //    //$this.jqGrid('editCell', iRow, iCol, true);
            //    $this.jqGrid('setGridParam', { cellEdit: true });
            //    $this.jqGrid('editCell', iRow, iCol, true);
            //    //$this.jqGrid('setGridParam', { cellEdit: true });
            //    lastsel2 = rowid;
            //    alert('LASTSEL2' + lastsel2);
            //}
            
            //if (rowid && rowid !== lastsel2) {
            //    $this.jqGrid('restoreCell', iRow, iCol, true);
            //    $this.jqGrid('editCell', iRow, iCol, true);
            //    lastsel2 = rowid;
            //    alert('LASTSEL2' + lastsel2);
            //}

            //if (rowid && rowid !== lastsel2) {
            //    $this.jqGrid('restoreCell', lastsel2, true);
            //    $this.jqGrid('editCell', iRow, iCol, true);
            //    lastsel2 = rowid;
            //    alert('AAENTERRAA' + lastsel2);
            //}

            //AUMENTADO LUNES 12 OCTUBRE
            ////if (rowid && rowid !== lastsel2) {

            ////    alert('ENTER');
            //   // $this.jqGrid('setGridParam', { cellEdit: true });
            //    $this.jqGrid('editCell', iRow, iCol, true);
            //    //$this.jqGrid('setGridParam', { cellEdit: false });
            //    $this.jqGrid('restoreCell', iRow, iCol, true);
            //    //('restoreRow', lastsel2);
            //    //('editRow', id, true);
            ////   lastsel2 = id;

            ////}
            // if (rowid && rowid !== lastsel2) {
            //    $this.jqGrid('restoreRow', lastsel2);
            //     $this.jqGrid('editRow', rowid, true);
            //   // $this.jqGrid('editCell', iRow, iCol, true);
            //    lastsel2 = rowid;
            //    alert('ENTER');
            //}

            

        },
        //onBeginEdit:function(rowIndex){
        //    var editors = $('#gvItemsMaAsignados').datagrid('getEditors', rowIndex);
        //    var n1 = $(editors[0].target);
        //    var n2 = $(editors[1].target);
        //    var n3 = $(editors[2].target);
        //    n1.add(n2).numberbox({
        //        onChange:function(){
        //            var cost = n1.numberbox('getValue')*n2.numberbox('getValue');
        //            n3.numberbox('setValue',cost);
        //        }
        //    })
        //},
        // afterSaveCell: function (rowid, name, val, iRow, iCol) 
        afterSaveCell: function (rowid, cellname, value, iRow, iCol)
          {
            //alert('AFTER SAVE CELL');
            
            var $this = $(this);
            $this.jqGrid('setGridParam', { datatype: 'local', loadonce: true }).trigger('reloadGrid');           
           // alert('antes de guardar' + $this.jqGrid('setGridParam', { datatype: 'local', loadonce: true }).trigger('reloadGrid'));
           // alert('VALOR + :' + (iRow + '_' + cellname).select());
            var rows = $this.jqGrid('getRowData');
            var numerofilaseleccionada = iRow - 1;
            var row = rows[numerofilaseleccionada];
            // alert('ROW' + row);
            //  var nombreId = row['Ite_Pro_Stock'];
            var nombreId = row['Ite_Pro_Stock_Min'];
           // alert('ID del seleccionado STOCK-' + nombreId);
            //jQuery("#list11").jqGrid('setGridParam',{datatype:'local',loadonce:true}).trigger('reloadGrid');
        },

        afterEditCell: function (rowid, cellName, cellValue, iRow, iCol) {
            // alert('  afterEditCell cellDOM' + cellDOM);
            //var $this = $(this);
            //$this.jqGrid('setGridParam', { cellEdit: true });
            //$this.jqGrid('editCell', iRow, iCol, true);


            var cellDOM = this.rows[iRow].cells[iCol], oldKeydown,

        //$cellInput = $('input, select, textarea', cellDOM),
        $cellInput = $('input', cellDOM),
        events = $cellInput.data('events'),
        $this = $(this);
            //alert('cellDOM' + cellDOM);
            //alert('$cellInput' + $cellInput);
            if (events && events.keydown && events.keydown.length) {
                oldKeydown = events.keydown[0].handler;
                $cellInput.unbind('keydown', oldKeydown);
                $cellInput.bind('keydown', function (e) {
                    $this.jqGrid('setGridParam', { cellEdit: true });
                    oldKeydown.call(this, e);
                   // $this.jqGrid('setGridParam', { cellEdit: false });
                }).bind('focusout', function (e) {
                    $this.jqGrid('setGridParam', { cellEdit: true });
                    $this.jqGrid('restoreCell', iRow, iCol, true);
                   // $this.jqGrid('setGridParam', { cellEdit: false });
                    $(cellDOM).removeClass("ui-state-highlight");
                });
            }


        },
        beforeSelectRow: function (rowid, e) {

            return false;
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });






    $.extend($.fn.fmatter, {


        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle_Asignados(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
        },
    });
    return myidselect;
}

function LimpiarBusquedaItemMaAsignados() {

    $('#ICodigo').val("");
    $('#INombre').val("");
    $('#ddlFamiliaID').val("0");
    $('#ddlMarcaID').val("0");
}

/* Visualizar Detalle */
function CargarFormularioDetalle_Asignados(ProyectoItemMa) {
    MostrarCamposDeshabilitados();
    $('#btnSalirDetalle').show();
    var RowId = parseInt($(ProyectoItemMa).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMaAsignados').getRowData(RowId);

    //SE OBTIENE LOS VALORES 
    $("#hdnIdItemMa").val(rowData.IMa_Id);
    $("#txtCodigo").val(rowData.IMa_Co);
    $("#txtNombre").val(rowData.IMa_Nombre_Tx);
    $("#txtDescripcion").val(rowData.IMa_Descripcion_Tx);
    //$("#txtMetrado").val(rowData.IMa_Metrado_Nu);
    $("#txtMetrado").val(rowData.IMa_Metrado_tx);
    $("#ddlUnidadMedida_Detalle").val(rowData.Unidad_Medida_Id);
    $("#ddlFamilia_Detalle").val(rowData.Familia_Id);
    $("#ddlMarca_Detalle").val(rowData.Marca_Id);
    //alert(rowData.Marca_Id);
    $("#ItemMa-Reg").dialog({
        width: 600,
        height: 300,
        title: "Detalle de Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}



/* Visualizar Detalle */
function CargarFormularioDetalle_Disponibles(ProyectoItemMa) {
    MostrarCamposDeshabilitados();
    $('#btnSalirDetalle').show();
    var RowId = parseInt($(ProyectoItemMa).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMaDisponibles').getRowData(RowId);

    //SE OBTIENE LOS VALORES 
    $("#hdnIdItemMa").val(rowData.IMa_Id);
    $("#txtCodigo").val(rowData.IMa_Co);
    $("#txtNombre").val(rowData.IMa_Nombre_Tx);
    $("#txtDescripcion").val(rowData.IMa_Descripcion_Tx);
    //$("#txtMetrado").val(rowData.IMa_Metrado_Nu);
    $("#txtMetrado").val(rowData.IMa_Metrado_tx);
    $("#ddlUnidadMedida_Detalle").val(rowData.Unidad_Medida_Id);
    $("#ddlFamilia_Detalle").val(rowData.Familia_Id);
    $("#ddlMarca_Detalle").val(rowData.Marca_Id);
    //alert(rowData.Marca_Id);
    $("#ItemMa-Reg").dialog({
        width: 600,
        height: 300,
        title: "Detalle de Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}

function LimpiarBusquedaItemMaDisponibles() {
    $('#txtCodigo_Seleccion').val("");
    $('#txtNombre_Seleccion').val("");
    $('#ddlFamilia_Seleccion').val("0");
    $('#ddlMarca_Seleccion').val("0");
}

///*****  ASIGNAR DESIGNAR ITEMMA *****///


function AsignarDesignarItemMA(seleccion, estado) {
  //  alert("AsignarDesignarRubros: " + seleccion);
    var datos = '{';
    datos = datos + 'lstItemMa:"' + seleccion + '",';
    datos = datos + 'Proyecto_Id:"' + $("#ContentPlaceHolder1_hdfProyecto").val() + '",';
    datos = datos + 'usuario:"' + $("#ContentPlaceHolder1_hdUsuario").val() + '",';
    datos = datos + 'estado:"' + estado + '"';
    datos = datos + '}';

  //  alert("Datos a AJAX: \n" + datos);
    $.ajax({
        type: 'POST',
        // url: '../../Logistica/Maestros/Proveedores.aspx/ModificarRubroaProveedor',
        url: '../../Logistica/Almacen/StockMinimo.aspx/AsignarDesignarItemMa',
        data: datos,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (msg) {

            CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val());
            CargarListadeItemMaDisponibles($("#ContentPlaceHolder1_hdfProyecto").val());

        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            //   Mensaje("Aviso", "Ocurrio un error en la asignacion de Rubro");
        }
        /*error: function (msg) {
            alert("ERROR ASIGNAR-DESIGNAR RUBROS");
            alert(msg);
        }*/
    });

    CargarListadeItemMaAsignados(); //////////VERIFICAR
    CargarListadeItemMaDisponibles();//////////VERIFICAR

}



///*****  ASIGNAR DESIGNAR ITEMMA TODOS *****///
function AsignarDesignarItemMATODOS(estado) {

    var datos = '{';
   // datos = datos + 'lstItemMa:"' + seleccion + '",';
    datos = datos + 'Proyecto_Id:"' + $("#ContentPlaceHolder1_hdfProyecto").val() + '",';
    datos = datos + 'usuario:"' + $("#ContentPlaceHolder1_hdUsuario").val() + '",';
    datos = datos + 'estado:"' + estado + '"';
    datos = datos + '}';

    //alert('DATOS ' + datos);

    $.ajax({
        type: 'POST',
        // url: '../../Logistica/Maestros/Proveedores.aspx/ModificarRubroaProveedor',
        url: '../../Logistica/Almacen/StockMinimo.aspx/AsignarDesignarItemMaTodos',
        data: datos,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (msg) {

            CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val());
            CargarListadeItemMaDisponibles($("#ContentPlaceHolder1_hdfProyecto").val());

        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            //   Mensaje("Aviso", "Ocurrio un error en la asignacion de Rubro");
        }
        /*error: function (msg) {
            alert("ERROR ASIGNAR-DESIGNAR RUBROS");
            alert(msg);
        }*/
    });
    //$.ajax({
    //    async: true,
    //    type: "POST",
    //    contentType: "application/json; charset=utf-8",
    //    url: '../../Contabilidad/Maestros/Cuentas.aspx/VerificarCodigoCuenta',
    //    dataType: "json",
    //    data: "{ObjCuenta:" + JSON.stringify(ObjCuenta) + "}",


    //});

}


function grabarEdicion(digito) {
    //alert('grabarEdicion MENSAJE');
    //alert('grabarEdicion MENSAJE value:' + digito.value.toString());
    var vaDigito = digito;
    //alert(vaDigito);
    //alert('grabarEdicion id ' + vaDigito.id.toString());
    //digito.blur();
    //alert('grabarEdicion rowid ' + vaDigito.rowid.toString());
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == '13') {
        //alert('Se ha presionado Enter!');
      //  alert('grabarEdicion MENSAJE value:' + digito.value.toString());
        var Numeroingresado = digito.value.toString();
        // alert('NUMERO INGRESADO    :    ' + Numeroingresado);  //NUMERO INGRESADO
        NumerodeStockMinimo= Numeroingresado;
        digito.blur();
        capturarnumeros(); //de prueba
        //alert('grabarEdicion id ' + vaDigito.id.toString());
    }
//    $("divItemsMaAsignados").dblclick(function () {
//    alert("The paragraph was double-clicked.");
//});
   // alert('grabarEdicion' + digito);
   // var fila = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');
   // alert('fila' + fila);
   ////var rows = jQuery("#gvItemsMaAsignados").jqGrid('getRowData');
   ////var Grid = jQuery("#gvItemsMaAsignados");
   // //var cellDOM = jQuery("#gvItemsMaAsignados").rows[iRow].cells[iCol], oldKeydown;
   // //var $cellInput = $('input, select, textarea', cellDOM);
   // //var events = $cellInput.data('events');
   // //var $this = $(this);

   // //alert('events' + events);
   // //var a = document.getElementById("Ite_Pro_Stock").value;

   // alert(document.getElementById("Ite_Pro_Stock"));

   // //var cellDOM = jQuery("#gvItemsMaAsignados").rows[iRow].cells[iCol], oldKeydown;
   // alert('AAAA' + a);
   //     //Ite_Pro_Stock
}
function capturarnumeros() {
  //  alert('ITEM  : ' + IdItemdeStock + '     -------    NumerodeStockMinimo : ' + NumerodeStockMinimo + '     -------    ITEMPROYECTO : ' + IdProyectoItem);

    //var item = IdItemdeStock;
    //var stock = NumerodeStockMinimo; 
    //if (NumerodeStockMinimo == null) {
    if (isNaN(NumerodeStockMinimo)) {
        Mensaje("Mensaje", "Por favor ingrese correctamente el número");
        //NumerodeStockMinimo = "";
    } else
        if (NumerodeStockMinimo == 0) {
            Mensaje("Mensaje", "Por favor ingrese correctamente el número");
          //  NumerodeStockMinimo = "";
        } else {
        var obj = {};
        obj.Ite_Pro_Stock_Min = NumerodeStockMinimo;
        obj.Ite_Pro_Id = IdProyectoItem;
        obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
        // obj.
        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Almacen/StockMinimo.aspx/ModificarProyectoItem',
            dataType: "json",
            data: "{objProyectoItemMa:" + JSON.stringify(obj) + "}",
            success: function (msg) {
               // Mensaje("Mensaje", "El stock fue ingresado");
                //$("#gvItemsMaAsignados").jqGrid('setGridParam', { cellEdit: true });
                //$("#gvItemsMaAsignados").jqGrid('editCell', iRow, iCol, true);



                SetearValoresEnviados();
                //$("#gvItemsMaAsignados").trigger("reloadGrid");
                //reload(rowid, result);
                //reload(rowsJqGrid, NumerodeStockMinimo);

                // $("#gvItemsMaAsignados").trigger("reloadGrid");
                // jQuery('#gvItemsMaAsignados').jqGrid('editRow', id, true, reload());

                //CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val());
                //$("#gvItemsMaAsignados").trigger("reloadGrid");

                //$("#gvItemsMaAsignados").jqGrid('addRowData', i + 1, mydata[i]);
                //$("#gvItemsMaAsignados").trigger("reloadGrid");


                //$("#gvItemsMaAsignados").trigger(CargarListadeItemMaAsignados($("#ContentPlaceHolder1_hdfProyecto").val()));
            },

            error: function (xhr, status, error) {
                alert(xhr.responseText);
                Mensaje("Aviso", "Ocurrio un error en el ingreso del stock");
            }

        });
    }
}
function grabarEdicion2() {
    alert('grabarEdicion2');
}

function SetearValoresEnviados() {
   

    var item = IdProyectoItem;
    var stock = NumerodeStockMinimo;
    rowJqGrid['IMa_Id'].html(stock);

   // $("#gvItemsMaAsignados").trigger("reloadGrid");

    //alert(' ItemProyecto: ' + item + 
    //    '\n NumerodeStockMinimo : ' + stock +
    //   // ' --------- /n rowSJqGrid :' + rowsJqGrid + 
    //    '\n NumeroFilaSeleccionadaJqGrid  :' + numerofilaseleccionadaJqGrid + 
    //   // ' --------- /n rowJqGrid  :' + rowJqGrid +
    //    '\n FilaGrillaTodasPaginas  :' + filagrillatodaspaginas +
    //    '\n FilaGrillaSoloPagPresente  :' + filagrillasolopagpresente +
    //    '\n ColumnaGrilla  :' + columnagrilla);


    


}

//function reload(rowid, result) { $("#gvItemsMaAsignados").trigger("reloadGrid"); }
//jQuery("#gvItemsMaAsignados").