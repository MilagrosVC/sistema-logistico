﻿//var ProyectoID = 1;
//var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();
//var PA = $("#ContentPlaceHolder1_hdfDenegadaA").val(); //63;
//alert(PROY);
//var Interno = $("#ContentPlaceHolder1_hdfInterno").val();
//var InternoAireLibre = $("#ContentPlaceHolder1_hdfInternoAireLibre").val();
//var Externo = $("#ContentPlaceHolder1_hdfExterno").val();
//var Arrendado = $("#ContentPlaceHolder1_hdfArrendado").val();
//alert('I : ' + Interno + '  ----  ' + 'IAL : ' + InternoAireLibre + '  ----  ' + 'E : ' + Externo + '  ----  ' + 'A : ' + Arrendado);
var Interno = $("#ContentPlaceHolder1_hdfInterno").val();
var InternoAireLibre = $("#ContentPlaceHolder1_hdfInternoAireLibre").val();
var Externo = $("#ContentPlaceHolder1_hdfExterno").val();
var Arrendado = $("#ContentPlaceHolder1_hdfArrendado").val();
var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();
//var Arrendado = 0;
   
//alert('I : ' + Interno + '  ----  ' + 'IAL : ' + InternoAireLibre + '  ----  ' + 'E : ' + Externo + '  ----  ' + 'A : ' + Arrendado);




$(document).ready(function () {


    InicializarControles();

    $("#btnBuscar").click(function () {
        ListarAlmacenes();
    });

    $("#btnLimpiar").click(function () {
        LimpiarBusqueda();
    });

    $("#btnNuevoAlmacen").click(function () {
        LimpiarDatosRegistro();
        NuevoAlmacen();
    });

    $("#btnCancelarRegistro").click(function () {
        $("#Almacen-Reg").dialog('close');
    });

    $("#btnCancelarModificar").click(function () {
        $("#Almacen-Reg").dialog('close');
    });

    $("#btnSalirDetalle").click(function () {
        $("#Almacen-Reg").dialog('close');
    });

    $("#btnLimpiarRegistro").click(function () {
        LimpiarDatosRegistro();
    });

    $("#btnRegistrar").click(function () {
        // CrearAlmacen();
        if ($("#txtNombre").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");
            $("#txtNombre").focus();
        }


       else if ($("#ddlUsuarioResponsable").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Responsable");
            $("#ddlUsuarioResponsable").focus();
        }

        else if ($("#ddlTipoAlmacen").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Tipo de Almacen");
            $("#ddlTipoAlmacen").focus();
        }


        else if ($("#txtDireccion").val().trim() == "") {
            Mensaje("Aviso", "Tiene que ingresar la direccion");
            $("#txtDireccion").focus();
        }

      else if ($("#ddlDepartamentoA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Departamento");
            $("#ddlDepartamentoA").focus();
        }
        else if ($("#ddlProvinciaA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar la Provincia");
            $("#ddlProvinciaA").focus();
        }
        else if ($("#ddlDistritoA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Distrito");
            $("#ddlDistritoA").focus();
        } else {
             CrearAlmacen();
           // alert('CREAR');
        }

    });
    //var Arrendado = $("#ContentPlaceHolder1_hdfArrendado").val();

    $("#btnModificar").click(function () {
        if ($("#txtNombre").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");
            $("#txtNombre").focus();
        }


        else if ($("#ddlUsuarioResponsable").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Responsable");
            $("#ddlUsuarioResponsable").focus();
        }

        else if ($("#ddlTipoAlmacen").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Tipo de Almacen");
            $("#ddlTipoAlmacen").focus();
        }


        else if ($("#txtDireccion").val().trim() == "") {
            Mensaje("Aviso", "Tiene que ingresar la direccion");
            $("#txtDireccion").focus();
        }

        else if ($("#ddlDepartamentoA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Departamento");
            $("#ddlDepartamentoA").focus();
        }
        else if ($("#ddlProvinciaA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar la Provincia");
            $("#ddlProvinciaA").focus();
        }
        else if ($("#ddlDistritoA").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Distrito");
            $("#ddlDistritoA").focus();
        } else {
            EditarAlmacen();
            // alert('CREAR');
        }
    });
});


function InicializarControles() {
    //alert('function InicializarControles() {');
    ListarAlmacenes();
    LimpiarDatosRegistro();
    //$("#ddlDepartamentoA").attr('disabled', false);
    //$("#ddlProvinciaA").attr('disabled', false);
    //$("#ddlDistritoA").attr('disabled', false);
    //$("#txtDireccion").attr('readonly', false);
    
    
    /*--- Combos Ubigeo ---*/
    CargarComboDepartamento();

    $('#ddlDepartamentoA').change(function () {
        CargarComboProvincia($(this).val());
    });
    $('#ddlProvinciaA').change(function () {
        CargarComboDistrito($(this).val());
    });
    $('#ddlDistritoA').change(function () {
        ObtenerCodigoUbigeo();
    });
    ///////////////////

    //if ($("#ddlTipoAlmacen").SelectedValue === Interno) {
    //    alert('AAAA');
    //}


    var Interno = $("#ContentPlaceHolder1_hdfInterno").val();
    var InternoAireLibre = $("#ContentPlaceHolder1_hdfInternoAireLibre").val();
    var Externo = $("#ContentPlaceHolder1_hdfExterno").val();
    var Arrendado = $("#ContentPlaceHolder1_hdfArrendado").val();

  //  alert('I : ' + Interno + '  ----  ' + 'IAL : ' + InternoAireLibre + '  ----  ' + 'E : ' + Externo + '  ----  ' + 'A : ' + Arrendado);
   
    ///////////////////////
    $('#ddlTipoAlmacen').change(function () {
       // alert('$(#ddlTipoAlmacen).change(function () {');
       // alert('parseInt(jQuery(this).val()'+ parseInt(jQuery(this).val()));
        var selectedValue = parseInt(jQuery(this).val());

        if (parseInt(jQuery(this).val()) == 0) {
            //CapturarUbigeoyDireccion();
            IngresarUbigeoyDireccion();
        }

        if (parseInt(jQuery(this).val()) == Interno)
        {
            //CapturarUbigeoyDireccion();
            CapturarUbigeoyDireccion(PROY);
        }
        if (parseInt(jQuery(this).val()) == InternoAireLibre) {
            //CapturarUbigeoyDireccion();
            CapturarUbigeoyDireccion(PROY);
        }
        if (parseInt(jQuery(this).val()) == Externo) {
            IngresarUbigeoyDireccion();
        }
        if (parseInt(jQuery(this).val()) == Arrendado) {
            IngresarUbigeoyDireccion();
        }
        //switch (selectedValue) {

        //    case integer(Interno):
        //        CapturarUbigeoyDireccion();
        //        break;
        //    case InternoAireLibre:
        //        CapturarUbigeoyDireccion();
        //        break;
        //    case Externo:
        //        IngresarUbigeoyDireccion();
        //        break;
        //    case Arrendado:
        //        IngresarUbigeoyDireccion();
        //        break;
        //        //etc... 
        //    //default:
        //    //  //  alert("catch default");
        //    //    break;
        //}
    });

    //function CapturarUbigeoyDireccion() {
    function CapturarUbigeoyDireccion(PROY) {
        // alert("Capturar Dpto, Provincia , Distrito y Direccion del Proyecto");
        var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();
        $("#ddlDepartamentoA").attr('disabled', true);
        $("#ddlProvinciaA").attr('disabled', true);
        $("#ddlDistritoA").attr('disabled', true);
        $("#txtDireccion").attr('disabled', true);
        var ObjProyecto = {
            Proyecto_Id: PROY                               
        }
        $.ajax({
            async: false,
            type: "POST",
            url: '../../Logistica/Maestros/Almacen.aspx/CargarDatosProyecto',
            data: "{ObjProyecto:" + JSON.stringify(ObjProyecto) + "}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    var ProyectoID = data.d.Proyecto_Id;
                    $("#txtDireccion").val(data.d.Pry_Direccion_Tx);


                    $("#ddlDepartamentoA").val(data.d.Departamento_Co);
                    CargarComboProvincia(data.d.Departamento_Co);
                    $("#ddlProvinciaA").val(data.d.Provincia_co);
                    CargarComboDistrito(data.d.Provincia_co);
                    $("#ddlDistritoA").val(data.d.Distrito_co);
                    ObtenerCodigoUbigeo();
             
                    //alert(ProyectoID);
                }

                else {
                    alert('else {');
                    Mensaje('Mensaje', 'Error en lectura de los datos del proyecto');
                }
            },
            error: function (data) {
                alert('Error al procesar los datos ...');
            }
        })


    }

    function IngresarUbigeoyDireccion() {
        //alert("Tiene que seleccionar Dpto, Provincia, Distrito y Direccion del Proyecto");
        $("#ddlDepartamentoA").attr('disabled', false);
        $("#ddlProvinciaA").attr('disabled', false);
        $("#ddlDistritoA").attr('disabled', false);
        $("#txtDireccion").attr('disabled', false);
        $("#txtDireccion").val("");
        $("#ddlDepartamentoA").val("0");
        $("#ddlProvinciaA").val("0");
        $("#ddlDistritoA").val("0");
    }

}


function ListarAlmacenes() {


    var objAlmacen = {
        Alm_Nombre_Tx: $("#txtbusqNombre").val(),
        Alm_Tipo_Id: $("#ddlTipoAlmacenBusq").val()
    };
    $("#AlmacenGrid").html('<table id="gvAlmacenGrid"></table><div id="pieAlmacenGrid"></div>');

    $("#gvAlmacenGrid").jqGrid({
        regional: 'es',
        url: '../../Logistica/Maestros/Almacen.aspx/ListarAlmacen',
        datatype: "json",
        mtype: "POST",
        postData: "{objAlmacen:" + JSON.stringify(objAlmacen) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        //colNames: ['Proyecto_Id', 'Alm_Tipo_Id', 'Cod Reque', 'Cod. Proforma', 'Cod Solic.', 'Cod Cotiz.', '', 'Nom Proveedor', 'N° Items', 'Monto', 'IGV', 'Monto total', 'Detraccion', '', 'Estado', 'F. Creacion'],
        colNames: ['Detalle', 'Editar', 'Eliminar', 'Configuracion', 'Nombre', 'Alm_Tipo_Id', 'Tipo', 'Abreviatura', 'Telefono', 'Proyecto_Id', 'Proyecto',
            'Empresa', 'Direccion', 'Responsable', 'Ubigeo_Id', 'Alm_Descripcion_Tx', 'Almacen_Id', 'Aud_Estado_Fl', 'Dpto', 'Provincia', 'Distrito'],
        colModel: [
             { name: 'Detalle', index: 'Detalle', width: 15, align: 'center', formatter: 'actionFormatterDetalle', search: false },
             { name: 'Editar', index: 'Editar', width: 15, align: 'center', formatter: 'actionFormatterEditar', search: false },
           { name: 'Eliminar', index: 'Eliminar', width: 15, align: 'center', formatter: 'actionFormatterEliminar', search: false },
           { name: 'ConfiguracionStock', index: 'ConfiguracionStock', width: 15, align: 'center', formatter: 'actionFormatterConfiguracionStock', search: false, hidden: true },

             //{ name: 'Detalle', index: 'Detalle', width: 40, align: 'center', formatter: 'actionFormatterDetalle', search: false },
             // { name: 'Accion', index: 'Accion', width: 30, align: 'center', formatter: 'actionFormatterAccion', search: false },
             { name: 'Alm_Nombre_Tx', index: 'Alm_Nombre_Tx', width: 50, align: 'center', hidden: false },
             { name: 'Alm_Tipo_Id', index: 'Alm_Tipo_Id', width: 50, align: 'center', hidden: true },

             { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 50, align: 'center' },
             { name: 'Alm_Abreviado_Tx', index: 'Alm_Abreviado_Tx', width: 50, align: 'center' },
             { name: 'Alm_Telefono_Tx', index: 'Alm_Telefono_Tx', width: 40, align: 'center' },

             { name: 'Proyecto_Id', index: 'Proyecto_Id', width: 50, align: 'center', hidden: true },
             { name: 'Pry_Nombre_Tx', index: 'Pry_Nombre_Tx', width: 50, align: 'center' },
             { name: 'Emp_RazonSocial_Tx', index: 'Emp_RazonSocial_Tx', width: 40, align: 'center', hidden: true },

             { name: 'Alm_Direccion_Tx', index: 'Alm_Direccion_Tx', width: 40, align: 'center' },
             { name: 'Alm_Responsable_Tx', index: 'Alm_Responsable_Tx', width: 40, align: 'center' },
             { name: 'Ubigeo_Id', index: 'Ubigeo_Id', width: 40, align: 'center', hidden: true },

             { name: 'Alm_Descripcion_Tx', index: 'Alm_Descripcion_Tx', width: 200, align: 'center', hidden: true },
             { name: 'Almacen_Id', index: 'Almacen_Id', width: 40, align: 'center', hidden: true },
             { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true},
                { name: 'Departamento_Co', index: 'Departamento_Co', width: 200, align: 'center', hidden: true },
                { name: 'Provincia_co', index: 'Provincia_co', width: 200, align: 'center', hidden: true },
                { name: 'Distrito_co', index: 'Distrito_co', width: 200, align: 'center', hidden: true }
                 ],

        pager: "#pieAlmacenGrid",
        viewrecords: true,
        width: 1325,//1200,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Almacenes',
        loadComplete: function (data) {},
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            alert(xhr.responseText);
        }
    });



    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            if (rowObject.Aud_Estado_Fl == 1) {
                return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
            }
            else
            { return '<i title=\"No se puede editar el regitro\"  style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>'; }
            //return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
        },

         actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }


         },
         actionFormatterConfiguracionStock: function (cellvalue, options, rowObject) {
             //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

             if (rowObject.Aud_Estado_Fl == 1) {

                 return '<i title=\"Click para Configurar Stock\" onclick=\"CargarFormularioConfiguracionStock(this)\" style=\"cursor:pointer\" class="fa fa-share-square-o"></i>';
             }
             else {
                 return '<i title=\"No se permite Configurar Stock\" style=\"cursor:pointer\" class="fa fa-share-square-o"></i>';
             }


         },



         actionFormatterEliminar: function (cellvalue, options, rowObject) {


             //return '<i title=\"Click para eliminar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-times"></i>';  //class="fa fa-check-square
             if (rowObject.Aud_Estado_Fl == false) {
                 return '<i title=\"Click para hablitar el regitro\" onclick=\"ConfirmacionHabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-trash-o" ></i>';
                 //     alert('a');
             }
             else
                 //class="fa fa-trash-o
                 if (rowObject.Aud_Estado_Fl == true ) 
                 {
                     return '<i title=\"Click para deshablitar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-check-square"></i>';
                     //     alert('b');
                 }

         }

        
});

}
function LimpiarBusqueda() {
    $("#txtbusqNombre").val("");
}

/*----------- Crear Almacen ------------*/

function NuevoAlmacen() {
    MostrarCamposHabilitados();
    var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();

    $('#ddlProyecto').val(PROY);
    $('#ddlProyecto').attr('disabled', true);

    $('#btnRegistrar').show();
    $('#btnModificar').hide();
    $('#btnCancelarRegistro').show();
    $('#btnCancelarModificar').hide();
    $('#btnSalirDetalle').hide();
    //$("#ddlDepartamento").val(Departamento_Co);
    //CargarComboProvincia(Departamento_Co);
    //$("#ddlProvincia").val(Provincia_co);
    //CargarComboDistrito(Provincia_co);
    //$("#ddlDistrito").val(Distrito_co);
    //ObtenerCodigoUbigeo();


    $("#Almacen-Reg").dialog({
        width: 900,
        height: 300,
        title: "Registrar Almacen",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}


//UBIGEO

function CargarComboDepartamento() {
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Almacen.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Distrito_co: 0, Provincia_co: 0 }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlDepartamentoA').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Departamento_Co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlDepartamentoA').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboProvincia(Departamento_Co) {
    $('#ddlProvinciaA').html("<option value='0' selected='selected'>SELECCIONE</option>");
    $('#ddlDistritoA').html("<option value='0' selected='selected'>SELECCIONE</option>");
    if (Departamento_Co == 0) {
        $('#ddlDepartamentoA').val('');
        return;
    }
    $('#ddlDepartamentoA').val(Departamento_Co);
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Almacen.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Departamento_Co: Departamento_Co }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlProvinciaA').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Provincia_co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlProvinciaA').html(sItems);
        },
        error: function (data) {
            //$('#ddlProvincia').combobox('error');
        }
    });
}

function CargarComboDistrito(Provincia_co) {
    if (Provincia_co == 0) {
        $('ddlProvinciaA').val('');
        return;
    }
    $('ddlProvinciaA').val(Provincia_co);
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Almacen.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Departamento_Co: $('#ddlDepartamentoA').val(), Provincia_co: Provincia_co }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlDistritoA').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Distrito_co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlDistritoA').html(sItems);
        },
        error: function (data) {
            //$('#ddlDistrito').combobox('error');
        }
    });
}



/*----------- Obtener Codigo Ubigeo------------*/
function ObtenerCodigoUbigeo() {
    var CodDpto = $('#ddlDepartamentoA').val();
    var CodProv = $('#ddlProvinciaA').val();
    var CodDist = $('#ddlDistritoA').val();

    var Ubigeo = {
        Departamento_Co: CodDpto,
        Provincia_co: CodProv,
        Distrito_co: CodDist
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Almacen.aspx/ObtenerCodigoUbigeo',
        data: "{ubigeo:'" + JSON.stringify(Ubigeo) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            //alert("Codigo Ubigeo: " + data.d);
            $('#hdfCodigoUbigeo').val(data.d);
            //alert("Valor: " + $('#hdfCodigoUbigeo').val());

        },
        error: function () {
            alert("Error!...");
        }
    });

}


/*--Acciones por Tipo--*/
function LimpiarDatosRegistro() {
    $('#ddlProyecto').attr('disabled', true);
    $("#txtNombre").val("");
    $("#txtAbreviatura").val("");
    $("#ddlUsuarioResponsable").val("0");

    $("#txtTelefono").val("");
    $("#ddlTipoAlmacen").val("0");
    $("#txtDescripcion").val("");

    $("#txtDireccion").val("");

    $("#ddlDepartamentoA").val("0");
    $("#ddlProvinciaA").val("0");
    $("#ddlDistritoA").val("0");

    $("#ddlDepartamentoA").attr('disabled', false);
    $("#ddlProvinciaA").attr('disabled', false);
    $("#ddlDistritoA").attr('disabled', false);
    $("#txtDireccion").attr('readonly', false);
}


//Registrar Almacen

function CrearAlmacen() {
    var obj = {};
    //alert(' $("#hdfCodigoUbigeo").val()' + $("#hdfCodigoUbigeo").val());

    obj.Alm_Nombre_Tx = $("#txtNombre").val();
    obj.Alm_Tipo_Id = $("#ddlTipoAlmacen").val();
    obj.Alm_Abreviado_Tx = $("#txtAbreviatura").val();
    obj.Alm_Descripcion_Tx = $("#txtDescripcion").val();
    obj.Alm_Telefono_Tx = $("#txtTelefono").val();
    obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();

    obj.Proyecto_Id = $("#ddlProyecto").val();
    obj.Alm_Direccion_Tx = $("#txtDireccion").val();
    obj.Alm_Responsable_Tx = $("#ddlUsuarioResponsable").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Almacen.aspx/CrearAlmacen',
        dataType: "json",
        data: "{objAlmacen:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            //alert('success: function (msg) {');
            //alert(msg.d); 
            //$('#hdnIdProveedor').val(msg.d);
            Mensaje("Mensaje", "Se registro correctamente el Almacen");

            //AsignarRubros();
            //CargarListadeRubrosDisponibles(msg.d);
            //CargarListadeRubrosAsignados(msg.d);
            //alert("Valor: " + $('#hdnIdProveedor').val());
            ListarAlmacenes();
            $("#Almacen-Reg").dialog('close');

        },
        error: function (xhr, status, error) {

            alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
        }
    });


}



//Habilitar - Deshabilitar
function ConfirmacionDeshabilitarRegistro(objAlmacen) {
    ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Deshabilitar el registro seleccionado?", function () {
        CargarFormularioEliminar(objAlmacen);
    });
}

function ConfirmacionHabilitarRegistro(objAlmacen) {
    ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Habilitar el registro seleccionado?", function () {
        CargarFormularioHabilitar(objAlmacen);
    });
}


//Cargar Formulario Habilitar - Eliminar
function CargarFormularioEliminar(objAlmacen) {
    // alert("¿Está seguro que desea Eliminar el registro seleccionado?")
    //function () {
    //DeshabilitarRegistro(obj);
    //  alert('Eliminar');
    //});
    var RowId = parseInt($(objAlmacen).parent().parent().attr("id"), 10);
    var rowData = $('#gvAlmacenGrid').getRowData(RowId);
    var obj = {};
    //obj.Prv_Id = rowData.Prv_Estado_Id;
    obj.Almacen_Id = rowData.Almacen_Id;
    obj.Aud_Estado_Fl = false;
    obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    // alert(obj.Prv_Id + " / " + obj.Aud_UsuarioEliminacion_Id);
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Almacen.aspx/EliminarAlmacen',
        dataType: "json",
        data: "{objAlmacen:" + JSON.stringify(obj) + "}",
        success: function (msg) {

            Mensaje("Mensaje", "El registro se elimino correctamente");
            ListarAlmacenes();
        },
        error: function (xhr, status, error) {
            // alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrió un error en la eliminacion");
        }



    });
}

function CargarFormularioHabilitar(objAlmacen) {

    //var objPerfilBE = {
    //    Per_id: $("#hdnId").val(),
    //    Aud_UsuarioEliminacion_Id: varUsuario,
    //    Aud_Estado_Fl: true
    //}
    var RowId = parseInt($(objAlmacen).parent().parent().attr("id"), 10);
    var rowData = $('#gvAlmacenGrid').getRowData(RowId);
    var obj = {};
    //obj.Prv_Id = rowData.Prv_Estado_Id;
    obj.Almacen_Id = rowData.Almacen_Id;
    obj.Aud_Estado_Fl = true;
    obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Almacen.aspx/EliminarAlmacen',
        dataType: "json",
        data: "{objAlmacen:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "El registro se habilito correctamente");
            ListarAlmacenes();
        },
        error: function (xhr, status, error) {
            // alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrió un error en la habilitacion");
        }
    });
}



//Cargar Formulario Editar
function CargarFormularioEditar(objAlmacen) {
    MostrarCamposHabilitados();
    var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();
    var Interno = $("#ContentPlaceHolder1_hdfInterno").val();
    var InternoAireLibre = $("#ContentPlaceHolder1_hdfInternoAireLibre").val();
    var Externo = $("#ContentPlaceHolder1_hdfExterno").val();
    var Arrendado = $("#ContentPlaceHolder1_hdfArrendado").val();
    $('#btnRegistrar').hide();
    $('#btnModificar').show();

    $('#btnCancelarRegistro').hide();
    $('#btnCancelarModificar').show();
    $('#btnSalirDetalle').hide();

    var RowId = parseInt($(objAlmacen).parent().parent().attr("id"), 10);
    var rowData = $('#gvAlmacenGrid').getRowData(RowId);
    //SE OBTIENE LOS VALORES 
    $("#hdnIdAlmacen").val(rowData.Almacen_Id); 
    $("#ddlProyecto").val(rowData.Proyecto_Id);
    $("#txtNombre").val(rowData.Alm_Nombre_Tx);
    $("#txtAbreviatura").val(rowData.Alm_Abreviado_Tx);
    $("#ddlUsuarioResponsable").val(rowData.Alm_Responsable_Tx);
    $("#txtTelefono").val(rowData.Alm_Telefono_Tx);
    $("#ddlTipoAlmacen").val(rowData.Alm_Tipo_Id);
    $("#txtDescripcion").val(rowData.Alm_Descripcion_Tx);
    $("#txtDireccion").val(rowData.Alm_Direccion_Tx);
    $("#ddlDepartamentoA").val(rowData.Departamento_Co);
    CargarComboProvincia(rowData.Departamento_Co);
    $("#ddlProvinciaA").val(rowData.Provincia_co);
    CargarComboDistrito(rowData.Provincia_co);
    $("#ddlDistritoA").val(rowData.Distrito_co);
    ObtenerCodigoUbigeo();
    if (rowData.Alm_Tipo_Id == Interno) {
        $("#ddlDepartamentoA").attr('disabled', true);
        $("#ddlProvinciaA").attr('disabled', true);
        $("#ddlDistritoA").attr('disabled', true);
        $("#txtDireccion").attr('disabled', true);
    }
    else if (rowData.Alm_Tipo_Id == InternoAireLibre) {
        $("#ddlDepartamentoA").attr('disabled', true);
        $("#ddlProvinciaA").attr('disabled', true);
        $("#ddlDistritoA").attr('disabled', true);
        $("#txtDireccion").attr('disabled', true);
    }
    else if (rowData.Alm_Tipo_Id == Externo) {
        $("#ddlDepartamentoA").attr('disabled', false);
        $("#ddlProvinciaA").attr('disabled', false);
        $("#ddlDistritoA").attr('disabled', false);
        $("#txtDireccion").attr('disabled', false);
    }
    else if (rowData.Alm_Tipo_Id == Arrendado) {
        $("#ddlDepartamentoA").attr('disabled', false);
        $("#ddlProvinciaA").attr('disabled', false);
        $("#ddlDistritoA").attr('disabled', false);
        $("#txtDireccion").attr('disabled', false);
    }
    $("#Almacen-Reg").dialog({
        //width: 1000, height: 340,
        width: 900,
        height: 300,
        title: "Editar Almacen", modal: true, resizable: false,
        dialogClass: 'hide-close',
        draggable: false,// down:100,// bgiframe: true,
        //position: myPos,
        // scrollTop:onkeydown,
        closeOnEscape: false,
    });
}

//Editar Proveedor Natural
function EditarAlmacen() {
    var obj = {};
    obj.Almacen_Id = $("#hdnIdAlmacen").val();
    obj.Alm_Nombre_Tx = $("#txtNombre").val();
    obj.Alm_Tipo_Id = $("#ddlTipoAlmacen").val();
    obj.Alm_Abreviado_Tx = $("#txtAbreviatura").val();
    obj.Alm_Descripcion_Tx = $("#txtDescripcion").val();
    obj.Alm_Telefono_Tx = $("#txtTelefono").val();
    obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();
    obj.Proyecto_Id = $("#ddlProyecto").val();
    obj.Alm_Direccion_Tx = $("#txtDireccion").val();
    obj.Alm_Responsable_Tx = $("#ddlUsuarioResponsable").val();
    obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    /////////////////////////////////////////
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Almacen.aspx/ModificarAlmacen',
        dataType: "json",
        data: "{objAlmacen:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "Los datos del Almacen fueron Modificados");
            ListarAlmacenes();
            $("#Almacen-Reg").dialog('close');
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrio un error en la Modificacion del Almacen");
        }
    });

}

//Visualizar Detalle

/* Visualizar Detalle */
function CargarFormularioDetalle(objAlmacen) {
    MostrarCamposDeshabilitados();
    $('#btnRegistrar').hide();
    $('#btnModificar').hide();
    $('#btnCancelarRegistro').hide();
    $('#btnCancelarModificar').hide(); 
    $('#btnSalirDetalle').show();
    $('#btnLimpiarRegistro').hide();
    var RowId = parseInt($(objAlmacen).parent().parent().attr("id"), 10);
    var rowData = $('#gvAlmacenGrid').getRowData(RowId);

    //SE OBTIENE LOS VALORES 
    $("#hdnIdAlmacen").val(rowData.Almacen_Id);
    $("#ddlProyecto").val(rowData.Proyecto_Id);
    $("#txtNombre").val(rowData.Alm_Nombre_Tx);
    $("#txtAbreviatura").val(rowData.Alm_Abreviado_Tx);
    $("#ddlUsuarioResponsable").val(rowData.Alm_Responsable_Tx);
    $("#txtTelefono").val(rowData.Alm_Telefono_Tx);
    $("#ddlTipoAlmacen").val(rowData.Alm_Tipo_Id);
    $("#txtDescripcion").val(rowData.Alm_Descripcion_Tx);
    $("#txtDireccion").val(rowData.Alm_Direccion_Tx);
    $("#ddlDepartamentoA").val(rowData.Departamento_Co);
    CargarComboProvincia(rowData.Departamento_Co);
    $("#ddlProvinciaA").val(rowData.Provincia_co);
    CargarComboDistrito(rowData.Provincia_co);
    $("#ddlDistritoA").val(rowData.Distrito_co);
    ObtenerCodigoUbigeo();
    if (rowData.Alm_Tipo_Id == Interno) {
        $("#ddlDepartamentoA").attr('disabled', true);
        $("#ddlProvinciaA").attr('disabled', true);
        $("#ddlDistritoA").attr('disabled', true);
        $("#txtDireccion").attr('readonly', true);
    }
    else if (rowData.Alm_Tipo_Id == InternoAireLibre) {
        $("#ddlDepartamentoA").attr('disabled', true);
        $("#ddlProvinciaA").attr('disabled', true);
        $("#ddlDistritoA").attr('disabled', true);
        $("#txtDireccion").attr('readonly', true);
    }
    else if (rowData.Alm_Tipo_Id == Externo) {
        $("#ddlDepartamentoA").attr('disabled', false);
        $("#ddlProvinciaA").attr('disabled', false);
        $("#ddlDistritoA").attr('disabled', false);
        $("#txtDireccion").attr('readonly', false);
    }
    else if (rowData.Alm_Tipo_Id == Arrendado) {
        $("#ddlDepartamentoA").attr('disabled', false);
        $("#ddlProvinciaA").attr('disabled', false);
        $("#ddlDistritoA").attr('disabled', false);
        $("#txtDireccion").attr('readonly', false);
    }
    $("#Almacen-Reg").dialog({
        //width: 1000, height: 340,
        width: 900,
        height: 300,
        title: "Detalle de Almacen", modal: true, resizable: false,
        dialogClass: 'hide-close',
        draggable: false,// down:100,// bgiframe: true,
        //position: myPos,
        // scrollTop:onkeydown,
        closeOnEscape: false,
    });
}

function MostrarCamposDeshabilitados() {
    $("#ddlProyecto").attr('disabled', true);
    $("#txtNombre").attr('disabled', true);
    $("#txtAbreviatura").attr('disabled', true);
    $("#ddlUsuarioResponsable").attr('disabled', true);
    $("#txtTelefono").attr('disabled', true);
    $("#ddlTipoAlmacen").attr('disabled', true);
    $("#txtDescripcion").attr('disabled', true);
    $("#ddlDepartamentoA").attr('disabled', true);
    $("#ddlProvinciaA").attr('disabled', true);
    $("#ddlDistritoA").attr('disabled', true);
    $("#txtDireccion").attr('disabled', true);
}

function MostrarCamposHabilitados() {
    $("#ddlProyecto").attr('disabled', false);
    $("#txtNombre").attr('disabled', false);
    $("#txtAbreviatura").attr('disabled', false);
    $("#ddlUsuarioResponsable").attr('disabled', false);
    $("#txtTelefono").attr('disabled', false);
    $("#ddlTipoAlmacen").attr('disabled', false);
    $("#txtDescripcion").attr('disabled', false);
    $("#ddlDepartamentoA").attr('disabled', false);
    $("#ddlProvinciaA").attr('disabled', false);
    $("#ddlDistritoA").attr('disabled', false);
    $("#txtDireccion").attr('disabled', false);
}

//Configuracion del stock minimo

function CargarFormularioConfiguracionStock(objAlmacen) {
    var RowId = parseInt($(objAlmacen).parent().parent().attr("id"), 10);
    var rowData = $("#gvAlmacenGrid").getRowData(RowId);
    //  alert('Dato' + rowData.PlanCuenta_Id);
    // alert('Dato Fecha : ' + rowData.PC_FechaCreacion + 'Dato Hora : ' + rowData.Aud_Creacion_Fe);
    //window.location = BASE_URL + '/Contabilidad/Maestros/Cuentas.aspx?PlanCuenta_Id=' + rowData.PlanCuenta_Id;

    //window.location = BASE_URL + '/Contabilidad/Maestros/Cuentas.aspx?PlanCuenta_Id=' + rowData.PlanCuenta_Id +
    //                                                                '&Pla_Nombre_Tx=' + rowData.Pla_Nombre_Tx +
    //                                                                '&Emp_RazonSocial_Tx=' + rowData.Emp_RazonSocial_Tx +
    //                                                                '&Emp_DocIdent_RUC_nu=' + rowData.Emp_DocIdent_RUC_nu +
    //                                                                '&PC_FechaCreacion=' + rowData.PC_FechaCreacion +
    //                                                                '&Aud_Creacion_Fe=' + rowData.Aud_Creacion_Fe;

    //Se dirige a cargar los hidden de la otra pagina
    //window.location = BASE_URL + '/Logistica/Maestros/Cuentas.aspx?PlanCuenta_Id=' + rowData.PlanCuenta_Id +
    //                                                                '&Pla_Nombre_Tx=' + rowData.Pla_Nombre_Tx +
    //                                                                '&Emp_RazonSocial_Tx=' + rowData.Emp_RazonSocial_Tx +
    //                                                                '&Emp_DocIdent_RUC_nu=' + rowData.Emp_DocIdent_RUC_nu +
    //                                                                '&PC_FechaCreacion=' + rowData.PC_FechaCreacion +
    //                                                                '&Aud_Creacion_Fe=' + rowData.Aud_Creacion_Fe;

}
