﻿/// <reference path="../../../../../Logistica/Maestros/Proveedores.aspx" />
/// <reference path="../../../../../Logistica/Maestros/Proveedores.aspx" />
/// <reference path="../../../../../Logistica/Maestros/Proveedores.aspx" />
//var P_Juridico = 17;
//var P_Natural = 16;
var P_Juridico = $("#ContentPlaceHolder1_hdfPersonaNatural").val();//36;
var P_Natural = $("#ContentPlaceHolder1_hdfPersonaJuridica").val();//37;
$(document).ready(function () {

    $('#tdIFNaciemiento').html('<input id="txtFechaNacimiento" type="date" class="inputs" style="width:158px" name="txtFechaNacimiento" max="' + MayoresdeEdad() + '" value="" required="required"/>');




    $("#dvAsignarRubros").hide();

    InicializarControles();
    EspacioBlancoInicioCadena('txtApePaterno');
    EspacioBlancoInicioCadena('txtApeMaterno');
    EspacioBlancoInicioCadena('txtNombres');
    EspacioBlancoInicioCadena('txtApePaterno');
    EspacioBlancoInicioCadena('txtApeMaterno');
    EspacioBlancoInicioCadena('txtNombres');
    sinEspaciosEnBlanco('txtNumeroRUC');
    sinEspaciosEnBlanco('txtNumeroDocumento');
    sinEspaciosEnBlanco('txtemail');
    //  longitudmayor('txtApePaterno', 8);
    //limitarInput( $("txtApePaterno"));
  //  limitarInput( $("txtApePaterno"), 8);
  //  $('txtApePaterno').prop('maxlength', '12');

    //limitarInput
   // limitarCajaTexto('10',  $("#txtApePaterno").val());

    // valruc($("#txtNumeroRUC").val())

    $("#btnAVRegistroPN").click(function () {
        NuevoProveedorNatural();
    });
    $("#btnAVRegistroPJ").click(function () {
        NuevoProveedorJuridico();
    });
    $("#btnAgregarRubro").click(function () {
        SeleccionarRubro();
    });
    $("#btnRegistrarProveedorNatural").click(function () {

        
        //Valida_Datos_Registro_PersonaNatural();
        if ($("#txtApePaterno").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Apellido Paterno");
            $("#txtApePaterno").focus();
        }
        else if ($("#txtApeMaterno").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Apellido Materno");
            $("#txtApeMaterno").focus();
        }
        else if ($("#txtNombres").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");

            $("#txtNombres").focus();
        }
        else if ($("#ddlTipoDocumento").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Tipo de Documento");
            $("#ddlTipoDocumento").focus();
        }
        else if ($("#txtNumeroDocumento").val() == "") {
            Mensaje("Aviso", "Tiene que ingresar el Numero de Documento");
            $("#txtNumeroDocumento").focus();


        }

            //Validacion segun tipo de documento
        else if ($("#ddlTipoDocumento  option:selected").text() == 'DNI' && $("#txtNumeroDocumento").val().length != 8) {
                $("#txtNumeroDocumento").prop('maxlength', '8');
            //longitudmayor("#txtNumeroDocumento", 8);
            //$("#txtNumeroDocumento").value().maxlength == 8;
            Mensaje("Aviso", "Tiene que ingresar numero de DNI valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'CARNE DE EXTRANJERIA' && $("#txtNumeroDocumento").val().length != 12) {
            $("#txtNumeroDocumento").prop('maxlength', '12');
            Mensaje("Aviso", "Tiene que ingresar numero de Carnet de Extrangeria valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'PASAPORTE' && $("#txtNumeroDocumento").val().length != 12) {
            $("#txtNumeroDocumento").prop('maxlength', '12');
            Mensaje("Aviso", "Tiene que ingresar numero de Pasaporte valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'RUC' && $("#txtNumeroDocumento").val().length != 11) {
            $("#txtNumeroDocumento").prop('maxlength', '11');
            Mensaje("Aviso", "El RUC ingresado tiene menos digitos que el formato valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'RUC' && $("#txtNumeroDocumento").val().length == 11) {
            var isValido = valruc($("#txtNumeroDocumento").val());
            if (isValido == false) {
                return Mensaje("Aviso", "El RUC ingresado no es Valido");
            }
            else {
                if ($("#ddlDepartamento").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Departamento");
                    $("#ddlDepartamento").focus();
                }
                else if ($("#ddlProvincia").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar la Provincia");
                    $("#ddlProvincia").focus();
                }
                else if ($("#ddlDistrito").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Distrito");
                    $("#ddlDistrito").focus();
                } else {
                    CrearProveedorNatural();
                }

            }
        }
            //else if ($("#txtFechaNacimiento").val() == "") {
            //    alert("Tiene que ingresar Fecha de Nacimiento");
            //    $("#txtFechaNacimiento").focus();
            //}
        else if ($("#ddlDepartamento").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Departamento");
            $("#ddlDepartamento").focus();
        }
        else if ($("#ddlProvincia").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar la Provincia");
            $("#ddlProvincia").focus();
        }
        else if ($("#ddlDistrito").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Distrito");
            $("#ddlDistrito").focus();
        }
        else { CrearProveedorNatural(); }
    });

    $("#btnRegistrarProveedorJuridico").click(function () {
        //Valida_Datos_Registro_PersonaJuridica(); 
        //var input = document.getElementById("txtNumeroRUC");
        //input.maxLength() = "11";

        //document.getElementById('txtNumeroRUC').maxLength = 11;
        //pwdField.setAttribute("maxlength", 128);
        //$("#txtNumeroRUC").setAttribute("maxlength", 11);
        //$("#txtNumeroRUC").maxlength(11);
        //$("#txtNumeroRUC").attr('maxlength', '6');
        if ($("#txtNumeroRUC").val().trim() == "") {
            $("#txtNumeroRUC").prop('maxlength', '11');
            Mensaje("Aviso", "Tiene que escribir el Numero de RUC");
            $("#txtNumeroRUC").focus();
        }
        else if ($("#txtNumeroRUC").val().length != 11) {
            Mensaje("Aviso", "El RUC ingresado tiene menos digitos que el formato valido");
            $("#txtNumeroRUC").focus();
        }

        else if ($("#txtNumeroRUC").val().length == 11) {
            var isValido = valruc($("#txtNumeroRUC").val());
            //alert('L :' + isValido);
            if (isValido == false) { return Mensaje("Aviso", "El RUC ingresado no es Valido"); }

            else {
                if ($("#txtRazonSocial").val().trim() == "") {
                    Mensaje("Aviso", "Tiene que escribir la Razon Social");
                    $("#txtRazonSocial").focus();
                }
                    //else if ($("#txtPersonaContacto").val() == "") {
                    //    alert("Tiene que escribir el Nombre de la Persona Contacto");
                    //    $("#txtPersonaContacto").focus();
                    //}
                else if ($("#ddlDepartamento").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Departamento");
                    $("#ddlDepartamento").focus();
                }
                else if ($("#ddlProvincia").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar la Provincia");
                    $("#ddlProvincia").focus();
                }
                else if ($("#ddlDistrito").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Distrito");
                    $("#ddlDistrito").focus();
                }
                else { CrearProveedorJuridico(); }
            }
        }


        // else if ($("#txtRazonSocial").val().trim() == "") {
        //    Mensaje("Aviso", "Tiene que escribir la Razon Social");
        //    $("#txtRazonSocial").focus();
        //}
        ////else if ($("#txtPersonaContacto").val() == "") {
        ////    alert("Tiene que escribir el Nombre de la Persona Contacto");
        ////    $("#txtPersonaContacto").focus();
        ////}
        //else if ($("#ddlDepartamento").val() == 0) {
        //    Mensaje("Aviso", "Tiene que seleccionar el Departamento");
        //    $("#ddlDepartamento").focus();
        //}
        //else if ($("#ddlProvincia").val() == 0) {
        //    Mensaje("Aviso", "Tiene que seleccionar la Provincia");
        //    $("#ddlProvincia").focus();
        //}
        //else if ($("#ddlDistrito").val() == 0) {
        //    Mensaje("Aviso", "Tiene que seleccionar el Distrito");
        //    $("#ddlDistrito").focus();
        //}

        //else{
        //    CrearProveedorJuridico();
        //}

    });

    $("#btnRealizarBusqueda").click(function () {

        ListarProveedor();
    });

    $("#btnCancelarProveedorNatural").click(function () {
        CerrarVentanaRegistroProveedor();
    });
    $("#btnCancelarProveedorJuridico").click(function () {
        CerrarVentanaRegistroProveedor();
    });


    $("#btnModificaraProveedorNatural").click(function () {

        if ($("#txtApePaterno").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Apellido Paterno");
            $("#txtApePaterno").focus();
        }
        else if ($("#txtApeMaterno").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Apellido Materno");
            $("#txtApeMaterno").focus();
        }
        else if ($("#txtNombres").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");

            $("#txtNombres").focus();
        }
        else if ($("#ddlTipoDocumento").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Tipo de Documento");
            $("#ddlTipoDocumento").focus();
        }
        else if ($("#txtNumeroDocumento").val() == "") {
            Mensaje("Aviso", "Tiene que ingresar el Numero de Documento");
            $("#txtNumeroDocumento").focus();


        }

            //Validacion segun tipo de documento
        else if ($("#ddlTipoDocumento  option:selected").text() == 'DNI' && $("#txtNumeroDocumento").val().length != 8) {
            $("#txtNumeroDocumento").prop('maxlength', '8');
            Mensaje("Aviso", "Tiene que ingresar numero de DNI valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'CARNE DE EXTRANJERIA' && $("#txtNumeroDocumento").val().length != 12) {
            $("#txtNumeroDocumento").prop('maxlength', '12');
            Mensaje("Aviso", "Tiene que ingresar numero de Carnet de Extrangeria valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'PASAPORTE' && $("#txtNumeroDocumento").val().length != 12) {
            $("#txtNumeroDocumento").prop('maxlength', '12');
            Mensaje("Aviso", "Tiene que ingresar numero de Pasaporte valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'RUC' && $("#txtNumeroDocumento").val().length != 11) {
            $("#txtNumeroDocumento").prop('maxlength', '11');
            Mensaje("Aviso", "El RUC ingresado tiene menos digitos que el formato valido");
            $("#txtNumeroDocumento").focus();
        }
        else if ($("#ddlTipoDocumento option:selected").text() == 'RUC' && $("#txtNumeroDocumento").val().length == 11) {
            var isValido = valruc($("#txtNumeroDocumento").val());
            if (isValido == false) {
                return Mensaje("Aviso", "El RUC ingresado no es Valido");
            }
            else {
                if ($("#ddlDepartamento").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Departamento");
                    $("#ddlDepartamento").focus();
                }
                else if ($("#ddlProvincia").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar la Provincia");
                    $("#ddlProvincia").focus();
                }
                else if ($("#ddlDistrito").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Distrito");
                    $("#ddlDistrito").focus();
                } else {
                    EditarProveedorNatural();
                }

            }
        }
            //else if ($("#txtFechaNacimiento").val() == "") {
            //    alert("Tiene que ingresar Fecha de Nacimiento");
            //    $("#txtFechaNacimiento").focus();
            //}
        else if ($("#ddlDepartamento").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Departamento");
            $("#ddlDepartamento").focus();
        }
        else if ($("#ddlProvincia").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar la Provincia");
            $("#ddlProvincia").focus();
        }
        else if ($("#ddlDistrito").val() == 0) {
            Mensaje("Aviso", "Tiene que seleccionar el Distrito");
            $("#ddlDistrito").focus();
        }
        else { EditarProveedorNatural(); }


       // EditarProveedorNatural();
    });
    $("#btnCancelarModificarProveedorNatural").click(function () {
        CerrarVentanaRegistroProveedor();
    });
    $("#btnModificaraProveedorJuridico").click(function () {

        if ($("#txtNumeroRUC").val().trim() == "") {
            $("#txtNumeroRUC").prop('maxlength', '11');
            Mensaje("Aviso", "Tiene que escribir el Numero de RUC");
            $("#txtNumeroRUC").focus();
        }
        else if ($("#txtNumeroRUC").val().length != 11) {
            Mensaje("Aviso", "El RUC ingresado tiene menos digitos que el formato valido");
            $("#txtNumeroRUC").focus();
        }

        else if ($("#txtNumeroRUC").val().length == 11) {
            var isValido = valruc($("#txtNumeroRUC").val());
            //alert('L :' + isValido);
            if (isValido == false) { return Mensaje("Aviso", "El RUC ingresado no es Valido"); }

            else {
                if ($("#txtRazonSocial").val().trim() == "") {
                    Mensaje("Aviso", "Tiene que escribir la Razon Social");
                    $("#txtRazonSocial").focus();
                }
                    //else if ($("#txtPersonaContacto").val() == "") {
                    //    alert("Tiene que escribir el Nombre de la Persona Contacto");
                    //    $("#txtPersonaContacto").focus();
                    //}
                else if ($("#ddlDepartamento").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Departamento");
                    $("#ddlDepartamento").focus();
                }
                else if ($("#ddlProvincia").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar la Provincia");
                    $("#ddlProvincia").focus();
                }
                else if ($("#ddlDistrito").val() == 0) {
                    Mensaje("Aviso", "Tiene que seleccionar el Distrito");
                    $("#ddlDistrito").focus();
                }
                else { EditarProveedorJuridico(); }
            }
        }




       // EditarProveedorJuridico();
    });
    $("#btnCancelarModificarProveedorJuridico").click(function () {
        CerrarVentanaRegistroProveedor();
    });
    $("#btnSalirdelRegistro").click(function () {
        CerrarVentanaRegistroProveedor();
    });

    $("#btnLimpiarBusqueda").click(function () {
        LimpiarFiltrosBusqueda();
        ListarProveedor();
    });
    /* --- Botones de Informacion Detalle */
    $("#btnSalirDetalle").click(function () {
        CerrarVentanaRegistroProveedor();
    });

    $("#btnDetalleRubros").click(function () {
        VisualizarRubroProveedor();
      
    });
    
    
    /*----Botones para Asignar o Desasignar Rubros seleccionados a Proveedor------*/

    $('#btnAgregarSeleccionados').click(function () {
        var seleccionados = $("#ddlRubro").val().join(',');
        AsignarDesignarRubros(seleccionados, true);
    });
    $("#btnAgregarTodos").click(function () {
        $("#ddlRubro option").attr("selected", "selected");
        var seleccionados = $("#ddlRubro").val().join(',');
        AsignarDesignarRubros(seleccionados, true);
    });
    $("#btnQuitarSeleccionados").click(function () {
        var seleccionados = $("#ddlRubroProveedor").val().join(',');
        AsignarDesignarRubros(seleccionados, false);

    });
    $("#btnQuitarTodos").click(function () {
        $("#ddlRubroProveedor option").attr("selected", "selected");
        var seleccionados = $("#ddlRubroProveedor").val().join(',');
        AsignarDesignarRubros(seleccionados, false);
    });

    /*----Botones para Salir de Registro y guardar los cambios------*/

    $("#btnGuardarRubrosdeProveedor").click(function () {
        Mensaje("Mensaje", "Los Rubros se asignaron correctamente para el Proveedor");
        CerrarVentanaSeleccionRubros();
        CerrarVentanaRegistroProveedor();
        
    });
    $("#btnSalirVentanaRubrosProveedor").click(function () {
        CerrarVentanaSeleccionRubros();
    });
    
});



function InicializarControles() {
    ListarProveedor();
    LimpiarData();
    /*--- Combos Parametro ---*/
    CargarComboParametro("#ddlTipoDocumento", 2);
    CargarComboParametro("#ddlProvedorClasificacion", 5);


    //CargarComboParametroAZ();

    /*--- Cargar en Multiple Rubros ---*/
    //CargarListadeRubro();

    //CargarListadeRubrosDisponibles();

    //CargarListadeRubrosAsignados();

    /*--- Combos Ubigeo ---*/
    CargarComboDepartamento();

    $('#ddlDepartamento').change(function () {
        CargarComboProvincia($(this).val());
    });
    $('#ddlProvincia').change(function () {
        CargarComboDistrito($(this).val());
    });
    $('#ddlDistrito').change(function () {
        ObtenerCodigoUbigeo();
    });


    /*------------Controles de Registro de Proveedor-------------*/
    $("#chkProveedorHomologadoFl").click(function () {
        if ($("#chkProveedorHomologadoFl").is(':checked')) {
            $("#txtPrvEstadoHomologadoID").attr('disabled', false);
            $("#txtProvHomologadoraID").attr('disabled', false);
        }

        else {
            $("#txtPrvEstadoHomologadoID").attr('disabled', true);
            $("#txtProvHomologadoraID").attr('disabled', true);
            $("#txtPrvEstadoHomologadoID").val("");
            $("#txtProvHomologadoraID").val("");
        }
    });
    /*-----------------------------------------------------------*/
    $('#btnRealizarBusqueda').hide();
    $('#btnLimpiarBusqueda').hide();
    $('#TbFiltroBusq').hide();
    $('#tdLApellidoPaterno').hide();
    $('#tdIApellidoPaterno').hide();
    $('#tdLApellidoMaterno').hide();
    $('#tdIApellidoMaterno').hide();
    $('#tdLRazonSocial').hide();
    $('#tdIRazonSocial').hide();
    $('#tdLPersonaContacto').hide();
    $('#tdIPersonaContacto').hide();
    $('#tdLNroDocumento').hide();
    $('#tdINroDocumento').hide();
    $('#TdEsp1').hide();
    $('#TdEsp2').hide();
    $('#TdEsp3').hide();

    $('#TrBtnBusqueda').hide();
    /*------------Controles de Consulta de Proveedor-------------*/
    $("#RdbtnNatural").click(function () {
        $('#btnRealizarBusqueda').show();
        $('#btnLimpiarBusqueda').show();
        $('#TbFiltroBusq').show();
        $('#TdEsp1').show();
        $('#TdEsp2').show();
        $('#TdEsp3').hide();
        $('#TdEsp4').hide();
        $('#tdLApellidoPaterno').show();
        $('#tdIApellidoPaterno').show();
        $('#tdLApellidoMaterno').show();
        $('#tdIApellidoMaterno').show();


        $('#tdLRazonSocial').hide();
        $('#tdIRazonSocial').hide();
        $('#tdLPersonaContacto').hide();
        $('#tdIPersonaContacto').hide();
        $('#tdLNroDocumento').show();
        $('#tdINroDocumento').show();
        $('#TrBtnBusqueda').show();



        LimpiarFiltrosBusqueda();
        ListarProveedor();
    });

    $("#RdbtnJuridico").click(function () {
        $('#btnRealizarBusqueda').show();
        $('#btnLimpiarBusqueda').show();
        $('#TbFiltroBusq').show();
        $('#TdEsp1').hide();
        $('#TdEsp2').hide();
        $('#TdEsp3').show();
        $('#TdEsp4').show();
        $('#tdLApellidoPaterno').hide();
        $('#tdIApellidoPaterno').hide();
        $('#tdLApellidoMaterno').hide();
        $('#tdIApellidoMaterno').hide();
        $('#tdLRazonSocial').show();
        $('#tdIRazonSocial').show();
        $('#tdLPersonaContacto').show();
        $('#tdIPersonaContacto').show();
        $('#tdLNroDocumento').show();
        $('#tdINroDocumento').show();
        $('#TrBtnBusqueda').show();
        LimpiarFiltrosBusqueda();
        ListarProveedor();
    });

    //    if ($("#RdbtnNatural").is(':checked')) {
    //        alert('NATURAL');
    //    }
    //    else if ($("#RdbtnJuridico").is(':checked')){
    //        alert('JURIDICO');   
    //    }

    /*-----------------------------------------------------------*/
}

function LimpiarFiltrosBusqueda() {
    $("#txtbusqApellidoPaterno").val('');
    $("#txtbusqApellidoMaterno").val('');
    $("#txtbusqRazSocial").val('');
    $("#txtbusqPersContacto").val('');
    $("#txtbusqNroDocumento").val('');

}


function CargarComboDepartamento() {
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Distrito_co: 0, Provincia_co: 0 }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlDepartamento').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Departamento_Co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlDepartamento').html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboProvincia(Departamento_Co) {
    $('#ddlProvincia').html("<option value='0' selected='selected'>SELECCIONE</option>");
    $('#ddlDistrito').html("<option value='0' selected='selected'>SELECCIONE</option>");
    if (Departamento_Co == 0) {
        $('#ddlDepartamento').val('');
        return;
    }
    $('#ddlDepartamento').val(Departamento_Co);
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Departamento_Co: Departamento_Co }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlProvincia').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Provincia_co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlProvincia').html(sItems);
        },
        error: function (data) {
            //$('#ddlProvincia').combobox('error');
        }
    });
}

function CargarComboDistrito(Provincia_co) {
    if (Provincia_co == 0) {
        $('ddlProvincia').val('');
        return;
    }
    $('ddlProvincia').val(Provincia_co);
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarUbigeoCombo',
        data: "{ubigeo:'" + JSON.stringify({ Departamento_Co: $('#ddlDepartamento').val(), Provincia_co: Provincia_co }) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlDistrito').empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Nombre + "' value='" + value.Distrito_co + "'>" + value.Nombre + "</option>");
                });
            }
            $('#ddlDistrito').html(sItems);
        },
        error: function (data) {
            //$('#ddlDistrito').combobox('error');
        }
    });
}

//function CargarComboParametroAZ() {

//    $.ajax({
//        type: 'POST',
//        url: '../../Logistica/Maestros/Proveedores.aspx/ListarParametroCombo',
//      //data: "{parametro:'"+JSON.stringify(parametro)+"'}",
//        //data: "{ parametro:'"+JSON.stringify(parametro) +"'}",
//        contentType: 'application/json; charset=utf-8',
//        dataType: "json",
//        async: false,
//        success: function (msg) {
//            $('#ddlTipoDocumento').empty();
//            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
//            if (msg.d != null) {
//                $.each(msg.d, function (index, value) {
//                    sItems += ("<option title='" + value.Par_Nombre_Tx + "' value='" + value.Par_Id + "'>" + value.Par_Nombre_Tx + "</option>");
//                });
//            }
//            $('#ddlTipoDocumento').html(sItems);
//        },
//        error: function () {
//            alert("Error!...");
//        }
//    });
//    }


function CargarComboParametro(control, padreID) {

    var ObjetoParametroBE = {
        Par_Padre_Id: padreID,
        Aud_Estado_Fl: true
    };

    $.ajax({
        type: "POST",
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarParametrosCombos',
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        data: "{ObjetoParametroBE:" + JSON.stringify(ObjetoParametroBE) + "}",

        async: false,

        success: function (msg) {
            // alert("Entro");
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Par_Nombre_Tx + "' value='" + value.Par_Id + "'>" + value.Par_Nombre_Tx + "</option>");
                });
            }
            //$('#ddlTipoDocumento').html(sItems);
            $(control).append(sItems);
        },

        error: function () {
            alert("Error JAVASCRIPT.");
        }
    });


}



function LimpiarData() {
    $("#txtRubroID").val("");
    $("#txtPrvEstadoHomologadoID").val("");
    $("#txtProvHomologadoraID").val("");
    // $("#txtEstadoID").val("");
    $("#txtApePaterno").val("");
    $("#txtApeMaterno").val("");
    $("#txtNombres").val("");
    $("#txtRazonSocial").val("");
    $("#txtPersonaContacto").val("");
    $("#txtTipoDocumentoID").val("");
    $("#txtNumeroDocumento").val("");
    $("#txtFechaNacimiento").val("");
    $("#txtDireccion").val("");
    $("#txtUbigeoID").val("");
    $("#txttelefono1").val("");
    $("#txttelefono2").val("");
    $("#txtemail").val("");
    $("#ddlProvedorClasificacion").val("0");
    $("#txtNumeroRUC").val("");
    $("#ddlTipoDocumento").val("0");
    $("#ddlDepartamento").val("0");
    $("#ddlProvincia").val("0");
    $("#ddlDistrito").val("0");
    $("#chkProveedorHomologadoFl").attr("checked", false);
    $("#txtPrvEstadoHomologadoID").attr('disabled', true);
    $("#txtProvHomologadoraID").attr('disabled', true);

}

/*----------- Obtener Codigo Ubigeo------------*/
function ObtenerCodigoUbigeo() {
    var CodDpto = $('#ddlDepartamento').val();
    var CodProv = $('#ddlProvincia').val();
    var CodDist = $('#ddlDistrito').val();

    var Ubigeo = {
        Departamento_Co: CodDpto,
        Provincia_co: CodProv,
        Distrito_co: CodDist
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ObtenerCodigoUbigeo',
        data: "{ubigeo:'" + JSON.stringify(Ubigeo) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            //alert("Codigo Ubigeo: " + data.d);
            $('#hdfCodigoUbigeo').val(data.d);
            //alert("Valor: " + $('#hdfCodigoUbigeo').val());

        },
        error: function () {
            alert("Error!...");
        }
    });

}


/*----------- Crear Proveedor ------------*/

function NuevoProveedorJuridico() {
    $("#dvAsignarRubros").hide();
    //$("#txtNumeroRUC").attr('disabled', false);
    $("#btnRegistrarProveedorJuridico").show();
    $("#btnCancelarProveedorJuridico").show();
    $("#btnRegistrarProveedorNatural").hide();
    $("#btnCancelarProveedorNatural").hide();
    MostrarCamposHabilitados();
    LimpiarData();
    //$('#trRazonSocial').show();
    //$('#trPContacto').show();
    $('#tdDatosPrincipalesJuridico').show();
    //$('#trAPaterno').hide();
    //$('#trAMaterno').hide();
    //$('#trNombres').hide();
    $('#trbtnProveedorJuridico').show();
    $('#trbtnProveedorNatural').hide();
    $('#trbtnModificarProveedorNatural').hide();
    $('#trbtnModificarProveedorJuridico').hide();
    $('#trbtnDetalleProveedor').hide(); //ESPACIO DE BOTON PARA DETALLE 
    
    $('#trDatosPrincipalesNatural').hide();
    //$('#trFNacimiento').hide();
    $('#tdLTipoDocumento').hide();
    $('#dlSTipoDocumento').hide();
    $('#tdLtxtNumeroDoc').hide();
    $('#tdItxtNumeroDoc').hide();
    $('#tdLFNacimineto').hide();
    $('#tdIFNaciemiento').hide();


    $("#Proveedor-Reg").dialog({
        width: 1000,
        height: 400,
        title: "Registrar Proveedor Juridico",
        modal: true
        , resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}
function NuevoProveedorNatural() {
    $("#dvAsignarRubros").hide();
    
    MostrarCamposHabilitados();
    $("#btnRegistrarProveedorJuridico").hide();
    $("#btnCancelarProveedorJuridico").hide();
    $("#btnRegistrarProveedorNatural").show();
    $("#btnCancelarProveedorNatural").show();

    LimpiarData();
    //$('#trRazonSocial').hide();
    //$('#trPContacto').hide();
    $('#tdDatosPrincipalesJuridico').hide();
    $('#trbtnProveedorJuridico').hide();
    $('#trbtnModificarProveedorNatural').hide();
    $('#trbtnModificarProveedorJuridico').hide();
    $('#trbtnDetalleProveedor').hide(); //ESPACIO DE BOTON PARA DETALLE 
    $('#trbtnProveedorNatural').show();
    $('#trDatosPrincipalesNatural').show();
    //$('#trAPaterno').show();
    //$('#trAMaterno').show();
    //$('#trNombres').show();
    //$('#trFNacimiento').show();
    $('#tdLTipoDocumento').show();
    $('#dlSTipoDocumento').show();
    $('#tdLtxtNumeroDoc').show();
    $('#tdItxtNumeroDoc').show();
    $('#tdLFNacimineto').show();
    $('#tdIFNaciemiento').show();


    //alert("Natural");

    $("#Proveedor-Reg").dialog({
        width: 1000,
        height: 460,
        title: "Registrar Proveedor Natural",
        //buttons: {
        //    Aceptar: function () {
        //        CrearProveedorNatural();
        //        AsignarRubros();
        //        //ListarProveedor();
        //        //$(this).dialog('close');
        //    },
        //    Cancelar: function () {
        //        $(this).dialog('close');
        //    }
        //},
        modal: true
        , resizable: false,
        dialogClass: 'hide-close',
        draggable: false, closeOnEscape: false
    });
    return false;
}
function CrearProveedorJuridico() {

    var obj = {};
    //obj.Rubro_Id = $("#txtRubroID").val();
    //obj.Tipopersona_Id = $("#txtTipoPersonaID").val();//Set a 17(Juridica)
    obj.Tipopersona_Id = P_Juridico;
    //obj.Prv_Homologado_Fl = $("#chkProveedorHomologadoFl").attr('checked');
    if ($("#chkProveedorHomologadoFl").is(':checked')) {
        obj.Prv_Homologado_Fl = true;
        //obj.Prv_Estado_Homologado_Id = $("#txtPrvEstadoHomologadoID").val();
        //obj.Prv_Homologadora_Id = $("#txtProvHomologadoraID").val();

        if ($("#txtPrvEstadoHomologadoID").val() != 0) {
            obj.Prv_Estado_Homologado_Id = $("#txtPrvEstadoHomologadoID").val();
        }
        if ($("#txtProvHomologadoraID").val() != 0) {
            obj.Prv_Homologadora_Id = $("#txtProvHomologadoraID").val();
        }

    } else { obj.Prv_Homologado_Fl = false; }
    
    //  obj.Prv_Estado_Id = $("#txtEstadoID").val();
    obj.Prv_RazonSocial_Tx = $("#txtRazonSocial").val();
    obj.Prv_Personacontacto_Tx = $("#txtPersonaContacto").val();
    //obj.TipDocIdent_Id = $("#ddlTipoDocumento").val();//Set a 20(RUC) ->Ruc para persona Juridica
    obj.TipDocIdent_Id = 20;
    obj.Prv_DocIdent_nu = $("#txtNumeroRUC").val();
    obj.Prv_Direccion_tx = $("#txtDireccion").val();
    obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();
    obj.Prv_Telefono1_nu = $("#txttelefono1").val();
    obj.Prv_Telefono2_nu = $("#txttelefono2").val();
    obj.Prv_Email_tx = $("#txtemail").val();
    obj.Prv_Cla_Id = $("#ddlProvedorClasificacion").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    //alert('obj.Tipopersona_Id' + obj.Tipopersona_Id + '\n obj.Prv_Homologado_Fl ' + obj.Prv_Homologado_Fl);
    //alert("Clasific_Id: " + $("#ddlProvedorClasificacion").val() +
    //    " / Estado_Id: " + $("#txtEstadoID").val() + " / UbigeoId: " + $("#hdfCodigoUbigeo").val())

    //obj = JSON.stringify(obj)
    //var pageUrl = '../../Logistica/Maestros/Proveedores.aspx/CrearProveedor';

    //return;
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Proveedores.aspx/CrearProveedor',
        dataType: "json",
        data: "{proveedor:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            //alert('success: function (msg) {');
            //alert(msg.d); 
            $('#hdnIdProveedor').val(msg.d);
            Mensaje("Mensaje", "Se registro correctamente el Proveedor Juridico," + '</br>' + "Proceda a ingresar los rubros correspondientes");

            AsignarRubros();
            CargarListadeRubrosDisponibles(msg.d);
            CargarListadeRubrosAsignados(msg.d);
            //alert("Valor: " + $('#hdnIdProveedor').val());

        },
        error: function (xhr, status, error) {

            alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
        }
    });

}
function CrearProveedorNatural() {

    var obj = {};
    //obj.Rubro_Id = $("#txtRubroID").val();
    //obj.Tipopersona_Id = $("#txtTipoPersonaID").val();
    obj.Tipopersona_Id = P_Natural;//Set a 16(Natural)
  //  obj.Prv_Homologado_Fl = $("#chkProveedorHomologadoFl").attr('checked');
    //  alert($("#chkProveedorHomologadoFl").attr('checked'));
    if ($("#chkProveedorHomologadoFl").is(':checked')) {
        //Set a 1(true)
        obj.Prv_Homologado_Fl = true;

        if ($("#txtPrvEstadoHomologadoID").val() != 0) {
            obj.Prv_Estado_Homologado_Id = $("#txtPrvEstadoHomologadoID").val();
        }
        if ($("#txtProvHomologadoraID").val() != 0) {
            obj.Prv_Homologadora_Id = $("#txtProvHomologadoraID").val();
        }


    } else { obj.Prv_Homologado_Fl = false; }
    //    obj.Prv_Estado_Id = $("#txtEstadoID").val();
    obj.Prv_APaterno_tx = $("#txtApePaterno").val();
    obj.Prv_AMaterno_tx = $("#txtApeMaterno").val();
    obj.Prv_Nombres_tx = $("#txtNombres").val();
    obj.TipDocIdent_Id = $("#ddlTipoDocumento").val();
    obj.Prv_DocIdent_nu = $("#txtNumeroDocumento").val();
    obj.Prv_Nacimiento_Fe = $("#txtFechaNacimiento").val();
    obj.Prv_Direccion_tx = $("#txtDireccion").val();
    obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();
    obj.Prv_Telefono1_nu = $("#txttelefono1").val();
    obj.Prv_Telefono2_nu = $("#txttelefono2").val();
    obj.Prv_Email_tx = $("#txtemail").val();
    //obj.Prv_Cla_Id = $("#txtProvClaID").val();
    obj.Prv_Cla_Id = $("#ddlProvedorClasificacion").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    //obj = JSON.stringify(obj)
    //var pageUrl = '../../Logistica/Maestros/Proveedores.aspx/CrearProveedor';

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/Proveedores.aspx/CrearProveedor',
        dataType: "json",
        data: "{proveedor:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            //alert(msg.d); 
            $('#hdnIdProveedor').val(msg.d);
            Mensaje("Mensaje", "Se registro correctamente el Proveedor Natural," + '</br>' + "Proceda a ingresar los rubros correspondientes");

            AsignarRubros();
            CargarListadeRubrosDisponibles(msg.d);

            CargarListadeRubrosAsignados(msg.d);
            //alert("Valor Crear_Natural: " + $('#hdnIdProveedor').val());
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });

}

function CerrarVentanaRegistroProveedor() {

    $("#Proveedor-Reg").dialog('close');
    ListarProveedor();

}

function CerrarVentanaSeleccionRubros() {
    $("#ProveedorRubro").dialog('close');
}

/*----------- Mostrar Campos Habilitados ------------*/
function MostrarCamposHabilitados() {
    $("#trbtnProveedorNatural").show();
    $("#trbtnProveedorJuridico").show();
    // $('#trbtnModificarProveedor').show();
    $('#trbtnModificarProveedorNatural').show();
    $('#trbtnModificarProveedorJuridico').show();
    $('#trbtnDetalleProveedor').show(); //ESPACIO DE BOTON PARA DETALLE 
    $("#txtNombres").attr('disabled', false);
    $("#txtApePaterno").attr('disabled', false);
    $("#txtApeMaterno").attr('disabled', false);
    $("#ddlTipoDocumento").attr('disabled', false);
    $("#txtRazonSocial").attr('disabled', false);
    $("#txtNumeroRUC").attr('disabled', false);
    $("#txtPersonaContacto").attr('disabled', false);
    $("#txtNumeroDocumento").attr('disabled', false);
    $("#txtFechaNacimiento").attr('disabled', false);
    $("#ddlDepartamento").attr('disabled', false);
    $("#ddlProvincia").attr('disabled', false);
    $("#ddlDistrito").attr('disabled', false);
    $("#txtDireccion").attr('disabled', false);
    $("#txtemail").attr('disabled', false);
    $("#txttelefono1").attr('disabled', false);
    $("#txttelefono2").attr('disabled', false);
    $("#ddlProvedorClasificacion").attr('disabled', false);
    $("#chkProveedorHomologadoFl").attr('disabled', false);
    $("#txtPrvEstadoHomologadoID").attr('disabled', false);
    $("#txtProvHomologadoraID").attr('disabled', false);
}

/*----------- Mostrar Campos Deshabilitados ------------*/
function MostrarCamposDeshabilitados() {
    $("#txtNombres").attr('disabled', true);
    $("#txtApePaterno").attr('disabled', true);
    $("#txtApeMaterno").attr('disabled', true);
    $("#ddlTipoDocumento").attr('disabled', true);
    $("#txtRazonSocial").attr('disabled', true);
    $("#txtNumeroRUC").attr('disabled', true);
    $("#txtPersonaContacto").attr('disabled', true);
    $("#txtNumeroDocumento").attr('disabled', true);
    $("#txtFechaNacimiento").attr('disabled', true);
    $("#ddlDepartamento").attr('disabled', true);
    $("#ddlProvincia").attr('disabled', true);
    $("#ddlDistrito").attr('disabled', true);
    $("#txtDireccion").attr('disabled', true);
    $("#txtemail").attr('disabled', true);
    $("#txttelefono1").attr('disabled', true);
    $("#txttelefono2").attr('disabled', true);
    $("#ddlProvedorClasificacion").attr('disabled', true);
    $("#chkProveedorHomologadoFl").attr('disabled', true);
    $("#txtPrvEstadoHomologadoID").attr('disabled', true);
    $("#txtProvHomologadoraID").attr('disabled', true);

}

/*----------- Asignar Rubros Proveedor ------------*/
function AsignarRubros() {
    $("#trbtnProveedorNatural").hide();
    $("#trbtnProveedorJuridico").hide();
    //$('#trbtnModificarProveedor').hide();
    $('#trbtnModificarProveedorNatural').hide();
    $('#trbtnModificarProveedorJuridico').hide();
    $('#trbtnDetalleProveedor').hide(); //ESPACIO DE BOTON PARA DETALLE 
    $("#txtAsigRubro").focus();
    MostrarCamposDeshabilitados();
    $("#dvAsignarRubros").show();
    $("#Proveedor-Reg").scrollTop(440);
}

/*----------- Listar Proveedor ----------*/
function ListarProveedor() {
    /*var objProveedor = {};
    objProveedor.Prv_APaterno_tx = $("#txtbusqApellidoPaterno").val();
    objProveedor.Prv_AMaterno_tx = $("#txtbusqApellidoMaterno").val();
    objProveedor.Prv_DocIdent_nu = $("#txtbusqNroDocumento").val();
    objProveedor.Prv_RazonSocial_Tx = $("#txtbusqRazSocial").val();
    objProveedor.Prv_Personacontacto_Tx = $("#txtbusqPersContacto").val();*/

    var objProveedor = {
        Prv_APaterno_tx: $("#txtbusqApellidoPaterno").val(),
        Prv_AMaterno_tx: $("#txtbusqApellidoMaterno").val(),
        Prv_DocIdent_nu: $("#txtbusqNroDocumento").val(),
        Prv_RazonSocial_Tx: $("#txtbusqRazSocial").val(),
        Prv_Personacontacto_Tx: $("#txtbusqPersContacto").val()
    };
    //alert("ENTRA!");
    //alert(objProveedor[0].Prv_AMaterno_tx);

    $("#ProveedorGrid").html('<table id="gvProveedorGrid"></table><div id="pieProveedorGrid"></div>');
    $("#gvProveedorGrid").jqGrid({
        regional: 'es',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarProveedores',
        datatype: "json",
        mtype: "POST",
        postData: "{objProveedor:" + JSON.stringify(objProveedor) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,

      //  colNames: ['Editar', 'Hab/Des', 'Rubro', 'Detalle','Tipo Persona', 'Proveedor', 'Direccion', 'Email', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
       // colNames: ['', '', '', '', 'Tipo Persona', 'Proveedor', 'Direccion', 'Email', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colNames: ['', '', '', '', 'Tipo Persona', 'Num Documento','Proveedor', 'Direccion', 'Email', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
        colModel: [

           { name: 'Editar', index: 'Editar', width: 25, align: 'center', formatter: 'actionFormatterEditar', search: false },
           { name: 'Eliminar', index: 'Eliminar', width: 25, align: 'center', formatter: 'actionFormatterEliminar', search: false },

           { name: 'Rubro', index: 'Rubro', width: 25, align: 'center', formatter: 'actionFormatterRubro', search: false },
           //
           { name: 'Detalle', index: 'Detalle', width: 25, align: 'center', formatter: 'actionFormatterDetalle', search: false },
           //

           { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 100, align: 'center' },
           { name: 'Prv_DocIdent_nu', index: 'Prv_DocIdent_nu', width: 200, align: 'center', hidden: true },
           { name: 'NombreProveedor_Tx', index: 'NombreProveedor_Tx', width: 200, align: 'center' },
           { name: 'Prv_Direccion_tx', index: 'Direccion', width: 300, align: 'left' },
           { name: 'Prv_Email_tx', index: 'Email', width: 200, align: 'center' },
           ///
            { name: 'Prv_RazonSocial_Tx', index: 'Razon Social', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Nombres_tx', index: 'Nombre', width: 200, align: 'center', hidden: true },
           { name: 'Prv_APaterno_tx', index: 'ApePaterno', width: 200, align: 'center', hidden: true },
           { name: 'Prv_AMaterno_tx', index: 'ApeMaterno', width: 200, align: 'center', hidden: true },

     //      { name: 'Prv_DocIdent_nu', index: 'Prv_DocIdent_nu', width: 200, align: 'center', hidden: true },
           { name: 'Tipopersona_Id', index: 'Tipopersona_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Personacontacto_Tx', index: 'Prv_Personacontacto_Tx', width: 200, align: 'center', hidden: true },
           { name: 'TipDocIdent_Id', index: 'TipDocIdent_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Nacimiento_Fe', index: 'Prv_Nacimiento_Fe', width: 200, align: 'center', hidden: true },
           { name: 'Departamento_Co', index: 'Departamento_Co', width: 200, align: 'center', hidden: true },
           { name: 'Provincia_co', index: 'Provincia_co', width: 200, align: 'center', hidden: true },
           { name: 'Distrito_co', index: 'Distrito_co', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Telefono1_nu', index: 'Prv_Telefono1_nu', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Telefono2_nu', index: 'Prv_Telefono2_nu', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Cla_Id', index: 'Prv_Cla_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Homologado_Fl', index: 'Prv_Homologado_Fl', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Estado_Homologado_Id', index: 'Prv_Estado_Homologado_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Homologadora_Id', index: 'Prv_Homologadora_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Estado_Id', index: 'Prv_Estado_Id', width: 200, align: 'center', hidden: true },
           { name: 'Prv_Id', index: 'Prv_Id', width: 200, align: 'center', hidden: true },
           //Estado Aud_Estado_Fl
           { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },
        ],
        pager: "#pieProveedorGrid",
        viewrecords: true,
        //width: 1200,
        width: 1325,
        height: 230,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },

        

        emptyrecords: "No hay Registros.",
        caption: 'Mantenimiento de Proveedor',
        loadComplete: function (result) {
//
            //var rowData = $("#gvProveedorGrid").getDataIDs();
            //if (result.rows.length > 0) {
            //    for (var i = 0; i < result.rows.length; i++) {
            //        if (result.rows[i].Aud_Estado_Fl != 1) {
            //            var checkbox = $("#jqg_gvProveedorGrid_" + result.rows[i].Prv_Id); //update this with your own grid name
            //            checkbox.css("visibility", "hidden");
            //            checkbox.attr("disabled", true);
            //        }
            //    }
            //}



//
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });

  

    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            if (rowObject.Aud_Estado_Fl == 1) {
                return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
            }
            else
            { return '<i title=\"No se puede editar el regitro\"  style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>'; }
            //return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
        },
        //actionFormatterEliminar: function (cellvalue, options, rowObject) {
        //    return '<i title=\"Click para eliminar el regitro\" onclick=\"CargarFormularioEliminar(this)\" style=\"cursor:pointer\" class="fa fa-times"></i>';
        //}
        actionFormatterEliminar: function (cellvalue, options, rowObject) {


            //return '<i title=\"Click para eliminar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-times"></i>';  //class="fa fa-check-square
            if (rowObject.Aud_Estado_Fl == false) {
                return '<i title=\"Click para hablitar el regitro\" onclick=\"ConfirmacionHabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-trash-o" ></i>';
           //     alert('a');
            }
            else
                //class="fa fa-trash-o
                if (rowObject.Aud_Estado_Fl == true) {
                    return '<i title=\"Click para deshablitar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-check-square"></i>';
               //     alert('b');
                }

        },
        //fa fa-plus-square-o
        actionFormatterRubro: function (cellvalue, options, rowObject) {

            //return '<i title=\"Click para agregar Rubro\" onclick=\"CargarFormularioRubro(this)\" style=\"cursor:pointer\" class="fa fa-plus"></i>';
            if (rowObject.Aud_Estado_Fl == 1) {
                return '<i title=\"Click para agregar Rubro\" onclick=\"CargarFormularioRubro(this)\" style=\"cursor:pointer\" class="fa fa-plus"></i>';
            }
            else {
                return '<i title=\"No se permite agregar Rubro\" style=\"cursor:pointer\" class="fa fa-plus"></i>';
            }
        }
        //actionFormatterDetalle // class="fa fa-eye" / class="fa fa-info"/ class="fa fa-info-circle"
        , actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }


        }
    });

    //jQuery("#ProveedorGrid").jqGrid('setGroupHeaders', {
    //    useColSpanStyle: false,
    //    groupHeaders: [
    //      { startColumnName: 'Par_Nombre_Tx', numberOfColumns: 2, titleText: '<em>Acciones</em>' },
    //      { startColumnName: 'Email', numberOfColumns: 2, titleText: 'Ac' }
    //    ]
    //});

    //$("#jqGrid").jqGrid({
    //    url: '../../Logistica/Maestros/Proveedores.aspx/ListarProveedores',
    //    datatype: "json",
    //    mtype: "POST",
    //    //serializeGridData: function (postData) {    return JSON.stringify(postData);   },
    //    postData: "{objProveedor:" + JSON.stringify(objProveedor) + "}",
    //    ajaxGridOptions: { contentType: "application/json" },
    //    ajaxOptions: { async: true },
    //    jsonReader: { repeatitems: false },
    //    loadui: "block",
    //    loadonce: true,
    //    colNames: ['Editar','Razon Social','Nombre', 'ApePaterno', 'ApeMaterno', 'Direccion', 'Email','','','','','','','','','','','','','','','',''],
    //    colModel: [

    //       { name: 'Editar', index: 'Editar', width: 55, align: 'center', formatter: 'actionFormatterEditar', search: false },
    //      // { name: 'Eliminar', index: 'Eliminar', width: 30, align: 'center', formatter: 'actionFormatterHabilitar', search: false },

    //       { name: 'Prv_RazonSocial_Tx', index: 'Razon Social', width: 200, align: 'center' },
    //       { name: 'Prv_Nombres_tx', index: 'Nombre', width: 200, align: 'center'},
    //       { name: 'Prv_APaterno_tx', index: 'ApePaterno', width: 200, align: 'center' },
    //       { name: 'Prv_AMaterno_tx', index: 'ApeMaterno', width: 200, align: 'center' },
    //       { name: 'Prv_Direccion_tx', index: 'Direccion', width: 300, align: 'center' },
    //       { name: 'Prv_Email_tx', index: 'Email', width: 200, align: 'center' },
    //       { name: 'Prv_DocIdent_nu', index: 'Prv_DocIdent_nu', width: 200, align: 'center', hidden: true },
    //       { name: 'Tipopersona_Id', index: 'Tipopersona_Id', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Personacontacto_Tx', index: 'Prv_Personacontacto_Tx', width: 200, align: 'center', hidden: true },
    //       { name: 'TipDocIdent_Id', index: 'TipDocIdent_Id', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Nacimiento_Fe', index: 'Prv_Nacimiento_Fe', width: 200, align: 'center', hidden: true },
    //       { name: 'Departamento_Co', index: 'Departamento_Co', width: 200, align: 'center', hidden: true },
    //       { name: 'Provincia_co', index: 'Provincia_co', width: 200, align: 'center', hidden: true },
    //       { name: 'Distrito_co', index: 'Distrito_co', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Telefono1_nu', index: 'Prv_Telefono1_nu', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Telefono2_nu', index: 'Prv_Telefono2_nu', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Cla_Id', index: 'Prv_Cla_Id', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Homologado_Fl', index: 'Prv_Homologado_Fl', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Estado_Homologado_Id', index: 'Prv_Estado_Homologado_Id', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Homologadora_Id', index: 'Prv_Homologadora_Id', width: 200, align: 'center', hidden: true },
    //       { name: 'Prv_Estado_Id', index: 'Prv_Estado_Id', width: 200, align: 'center', hidden: true },

    //        { name: 'Prv_Id', index: 'Prv_Id', width: 200, align: 'center', hidden: true },
    //       //{ name: 'EstadoStr', index: 'EstadoStr' }


    //    ],
    //    pager: "#jqGridPager",
    //    viewrecords: true,
    //    width: 1200,
    //    //width: 1250,
    //    height: 230,
    //    rowNum: 10,
    //    rowList: [10, 20, 30, 100],
    //    gridview: true,
    //    jsonReader: {
    //        page: function (obj) { return 1; },
    //        total: function (obj) { return 1; },
    //        records: function (obj) { return obj.d.length; },
    //        root: function (obj) { return obj.d; },
    //        repeatitems: false,
    //        id: "0"
    //    },
    //    emptyrecords: "No hay Registros.",
    //    caption: 'Mantenimiento de Proveedor',
    //    loadComplete: function (result) {

    //    },
    //    loadError: function (xhr) {
    //        alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
    //    }
    ////}).navGrid('#jqGridPager', { edit: true, add: false, del: true, search: false });
    //}).navGrid('#jqGridPager', { edit: false, add: false, del: false, search: false, refresh: false});
    //$.extend($.fn.fmatter, {
    //    actionFormatterEditar: function (cellvalue, options, rowObject) {
    //        return "<img title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" src=\"../Images/General/iconos/edit.gif\" />";
    //    },
    //    //actionFormatterHabilitar: function (cellvalue, options, rowObject) {
    //    //    if (rowObject.EstadoStr == 'Deshabilitado') {
    //    //        return "<img title='Click para habilitar el registro' onclick='ConfirmacionHabilitarRegistro(this)' style='cursor:pointer' src=\"../Images/General/iconos/habilitar.png\" />";
    //    //    }
    //    //    else
    //    //        if (rowObject.EstadoStr == 'Habilitado') {
    //    //            return "<img title='Click para deshabilitar el registro' onclick='ConfirmacionDeshabilitarRegistro(this)' style='cursor:pointer' src=\"../Images/General/iconos/delete.png\" />";
    //    //        }
    //    //}
    //});

    //jQuery("#ProveedorGrid").jqGrid('setGroupHeaders', {
    //    useColSpanStyle: false,
    //    groupHeaders: [
    //      { startColumnName: 'Par_Nombre_Tx', numberOfColumns: 2, titleText: '<em>Acciones</em>' },
    //      { startColumnName: 'Email', numberOfColumns: 2, titleText: 'Ac' }
    //    ]
    //});

}

/*----Abrir ventana para Agregar Rubros al Proveedor--------*/

function SeleccionarRubro() {
    $('#tdbtnAsigRubro').show(); //BOTONES PARA ASIGNAR RUBRO
    $('#trGuardarRubrosdeProveedor').show();
    $('#trSalirVentanaRubrosProveedor').hide();
    HabilitarBotonesAsignacionRubro();
    $("#ProveedorRubro").dialog({
        width: 725,
        height: 480,
        closeOnEscape: false,
        resizable: false,
        draggable: false,
        dialogClass: 'hide-close',
        title: "Seleccione los Rubros del Proveedor",
        modal: true
    });
    return false;
}

/*----Listar Rubros DISPONIBLES para Proveedor--------*/
function CargarListadeRubro() {
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarRubroProveedor',
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlRubro').empty();
            var sItems = "";

            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Rub_Nombre_Tx + "' value='" + value.Rubro_Id + "'>" + value.Rub_Nombre_Tx + "</option>");
                }
                                );
            }
            $('#ddlRubro').html(sItems);
        },
        error: function () {
            alert("Error! JS");
        }
    });
}

/*----Listar Rubros DISPONIBLES para Proveedor--------*/
function CargarListadeRubrosDisponibles(id) {
    //alert("Valor_ListadoRubros: " + id);
    //var IdProveedor = $('#hdnIdProveedor').val();
    var proveedor = { Prv_Id: id };
    $.ajax({
        type: 'POST',
        //url: '../../Logistica/Maestros/Proveedores.aspx/ListarRubroProveedrorDisponible?IdProveedor=' + IdProveedor,
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarRubroProveedorDisponible',
        //data: "{IdProveedor:'" + JSON.stringify(proveedor) + "'}",
        data: "{ProveedorRubro:" + JSON.stringify(proveedor) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlRubro').empty();
            var sItems = "";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Rub_Nombre_Tx + "' value='" + value.Rubro_Id + "'>" + value.Rub_Nombre_Tx + "</option>");
                });
            } $('#ddlRubro').html(sItems);
        },
        error: function () {
            alert("Error! LISTA RUBRO PROVEEDOR DISPONIBLE ");
        }
    });
}

/*----Listar Rubros ASIGNADOS a Proveedor--------*/
function CargarListadeRubrosAsignados(id) {
    //Capturar el Codigo del Proveedor Registrado
    //var IdProveedor = $('#hdnIdProveedor').val();
    var proveedorID = { Prv_Id: id };
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ListarRubroAsignadoProveedor',
        data: "{ProveedorRubro:" + JSON.stringify(proveedorID) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $('#ddlRubroProveedor').empty();
            var sItems = "";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Rub_Nombre_Tx + "' value='" + value.Rubro_Id + "'>" + value.Rub_Nombre_Tx + "</option>");
                });
            }
            $('#ddlRubroProveedor').html(sItems);
        },
        error: function () {
            alert("Error! Rubro Asignado")
        }

    })


}

/*----Asignar o Designar Rubros a Proveedor--------*/
function AsignarDesignarRubros(seleccion, estado) {
    //alert("AsignarDesignarRubros: " + seleccion);
    var datos = '{';
    datos = datos + 'lstRubro:"' + seleccion + '",';
    datos = datos + 'Prov_Id:"' + $("#hdnIdProveedor").val() + '",';
    datos = datos + 'usuario:"' + $("#ContentPlaceHolder1_hdUsuario").val() + '",';
    datos = datos + 'estado:"' + estado + '"';
    datos = datos + '}';

    //alert("Datos a AJAX: \n" + datos);
    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/Proveedores.aspx/ModificarRubroaProveedor',
        data: datos,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: true,
        success: function (msg) {

            CargarListadeRubrosAsignados($("#hdnIdProveedor").val());
            CargarListadeRubrosDisponibles($("#hdnIdProveedor").val());

        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            //   Mensaje("Aviso", "Ocurrio un error en la asignacion de Rubro");
        }
        /*error: function (msg) {
            alert("ERROR ASIGNAR-DESIGNAR RUBROS");
            alert(msg);
        }*/
    });

    CargarListadeRubrosDisponibles();
    CargarListadeRubrosAsignados();

}



/*----Editar a Proveedor--------*/

function CargarFormularioEditar(objProveedor) {
    //  ListarProveedor();
    $("#dvAsignarRubros").hide();
    //$("#txtNumeroRUC").attr('disabled',true);
    
    // $("#dvAsignarRubros").show();
    MostrarCamposHabilitados();
    var RowId = parseInt($(objProveedor).parent().parent().attr("id"), 10);
    var rowData = $('#gvProveedorGrid').getRowData(RowId);
    //var rowData = $("#jqGrid").getRowData(RowId);ProveedorGrid ProveedorGrid
    /*------------Controles Edicion de Proveedor-------------*/
    //TIPO  16 = Natural
    if ((rowData.Par_Nombre_Tx) == 'NATURAL') {
        $('#trbtnModificarProveedorNatural').show();
        $('#trbtnModificarProveedorJuridico').hide();
        $('#trbtnDetalleProveedor').hide(); //ESPACIO DE BOTON PARA DETALLE 
        $('#trbtnProveedorJuridico').hide();
        $('#trbtnProveedorNatural').hide();
        $('#tdDatosPrincipalesJuridico').hide();
        $('#trDatosPrincipalesNatural').show();
        $('#tdLTipoDocumento').show();
        $('#dlSTipoDocumento').show();
        $('#tdLtxtNumeroDoc').show();
        $('#tdItxtNumeroDoc').show();
        $('#tdLFNacimineto').show();
        $('#tdIFNaciemiento').show();
        $("#btnRegistrarProveedorJuridico").hide();
        $("#btnCancelarProveedorJuridico").hide();
        /////////////////////////////////
        $("#btnRegistrarProveedorNatural").hide();
        $("#btnCancelarProveedorNatural").hide();

    }                                              //TIPO 17= Juridico
    else if ((rowData.Par_Nombre_Tx) == 'JURIDICA') {
        $('#trbtnModificarProveedorNatural').hide();
        $('#trbtnModificarProveedorJuridico').show();
        $('#trbtnDetalleProveedor').hide(); //ESPACIO DE BOTON PARA DETALLE 
        $('#trbtnProveedorJuridico').hide();
        $('#trbtnProveedorNatural').hide();
        $('#tdDatosPrincipalesJuridico').show();
        $('#trDatosPrincipalesNatural').hide();
        $('#tdLTipoDocumento').hide();
        $('#dlSTipoDocumento').hide();
        $('#tdLtxtNumeroDoc').hide();
        $('#tdItxtNumeroDoc').hide();
        $('#tdLFNacimineto').hide();
        $('#tdIFNaciemiento').hide();
        $("#btnRegistrarProveedorJuridico").hide();
        $("#btnCancelarProveedorJuridico").hide();
        $("#btnRegistrarProveedorNatural").hide();
        $("#btnCancelarProveedorNatural").hide();
    }
    //var myPos = [$(window).width() / 2, 50];
    //var currentTop = $(document).scrollTop();
    //var d=   $('<div>', {id: 'Proveedor-Reg', css:{top: currentTop + 150}}).prependTo('body'),
    //        d.hide().appendTo('body').fadeIn();
    $("#Proveedor-Reg").dialog({
        //width: 1000, height: 440, title: "Editar Proveedor", modal: true, resizable: false,


        width: 1000, height: 430, title: "Editar Proveedor", modal: true, resizable: false,
        dialogClass: 'hide-close',
        draggable: false,// down:100,// bgiframe: true,
        //position: myPos,
        // scrollTop:onkeydown,
        closeOnEscape: false,

    });

    //  $("#Proveedor-Reg").scrollTop(400)

    //SE OBTIENE LOS VALORES 
    $("#hdnIdProveedor").val(rowData.Prv_Id); //El CAMPO QUE SE USO PARA ID DE PROVEEDOR  EN ASIGNACION DE RUBROS
    $("#txtEstadoID").val(rowData.Prv_Estado_Id);
    $("#txtApePaterno").val(rowData.Prv_APaterno_tx);
    $("#txtApeMaterno").val(rowData.Prv_AMaterno_tx);
    $("#txtNombres").val(rowData.Prv_Nombres_tx);
    $("#txtRazonSocial").val(rowData.Prv_RazonSocial_Tx);
    $("#txtPersonaContacto").val(rowData.Prv_Personacontacto_Tx);
    $("#txtTipoDocumentoID").val(rowData.TipDocIdent_Id);
    $("#txtNumeroDocumento").val(rowData.Prv_DocIdent_nu);
    //var FechaVal = ConvertFecha(rowData.Prv_Nacimiento_Fe);//alert(FechaVal);
    //$("#txtFechaNacimiento").val(FechaVal);
    $("#txtFechaNacimiento").val(ConvertFecha(rowData.Prv_Nacimiento_Fe));


    //$('#tdIFNaciemiento').html('<input id="txtFechaNacimiento" type="date" class="form-control" name="txtFechaNacimiento" max="' + MayoresdeEdad() + '" value="" required="required"/>').val(rowData.Prv_DocIdent_nu);


    $("#txtDireccion").val(rowData.Prv_Direccion_tx);
    $("#txtUbigeoID").val(rowData.Ubigeo_Id);
    $("#txttelefono1").val(rowData.Prv_Telefono1_nu);
    $("#txttelefono2").val(rowData.Prv_Telefono2_nu);
    $("#txtemail").val(rowData.Prv_Email_tx);

    //if ((rowData.Prv_Cla_Id) == NULL) {
    //    $("#ddlProvedorClasificacion").val("0");
    //}
    //else {
    $("#ddlProvedorClasificacion").val(rowData.Prv_Cla_Id);
    //}

    $("#txtNumeroRUC").val(rowData.Prv_DocIdent_nu);
    $("#ddlTipoDocumento").val(rowData.TipDocIdent_Id);
    /*--- Combos Ubigeo ---*/

    //CargarComboDepartamento();
    //if (rowData.Departamento_Co == NULL) {
    //   //alert('VALORES VACIOS');
    //    $("#ddlDepartamento").val("0");
    //    $("#ddlProvincia").val("0");
    //    $("#ddlDistrito").val("0");

    //} else {
    $("#ddlDepartamento").val(rowData.Departamento_Co);
    CargarComboProvincia(rowData.Departamento_Co);
    $("#ddlProvincia").val(rowData.Provincia_co);
    CargarComboDistrito(rowData.Provincia_co);
    $("#ddlDistrito").val(rowData.Distrito_co);
    ObtenerCodigoUbigeo();
    //}

    //   alert('rowData.Prv_Homologado_Fl' + rowData.Prv_Homologado_Fl);


    if (rowData.Prv_Homologado_Fl == "false") {
        $("#chkProveedorHomologadoFl").prop("checked", false);
        $("#txtPrvEstadoHomologadoID").attr('disabled', true);
        $("#txtProvHomologadoraID").attr('disabled', true);
        $("#txtPrvEstadoHomologadoID").val("");
        $("#txtProvHomologadoraID").val("");
    }
    if ((rowData.Prv_Homologado_Fl) == "true") {
        $("#chkProveedorHomologadoFl").prop("checked", true);
        $("#txtPrvEstadoHomologadoID").attr('disabled', false);
        $("#txtProvHomologadoraID").attr('disabled', false);
        $("#txtPrvEstadoHomologadoID").val(rowData.Prv_Estado_Homologado_Id);
        $("#txtProvHomologadoraID").val(rowData.Prv_Homologadora_Id);
    }

    //$("#Proveedor-Reg").dialog({ width: 1000, height: 500, title: "Editar Proveedor", modal: true });


}

    function EditarProveedorJuridico() {
        var obj = {};
        // alert('Persona Natural');
        //  alert("Clasific_Id: " + $("#ddlProvedorClasificacion").val() + " / Estado_Id: " + $("#txtEstadoID").val() + " / UbigeoId: " + $("#hdfCodigoUbigeo").val())
        obj.Tipopersona_Id = P_Juridico;//Set a 17(Juridico)
        if ($("#chkProveedorHomologadoFl").is(':checked')) {
            obj.Prv_Homologado_Fl = true;

            if ($("#txtPrvEstadoHomologadoID").val() != 0) {
                obj.Prv_Estado_Homologado_Id = $("#txtPrvEstadoHomologadoID").val();
            }
            if ($("#txtProvHomologadoraID").val() != 0) {
                obj.Prv_Homologadora_Id = $("#txtProvHomologadoraID").val();
            }
        } else { obj.Prv_Homologado_Fl = false; }
        obj.Prv_Id = $("#hdnIdProveedor").val();
        obj.Prv_Estado_Id = $("#txtEstadoID").val();
        obj.Prv_RazonSocial_Tx = $("#txtRazonSocial").val();
        obj.Prv_Personacontacto_Tx = $("#txtPersonaContacto").val();
        obj.TipDocIdent_Id = 20;//Set a 20(RUC) ->Ruc para persona Juridica
        obj.Prv_DocIdent_nu = $("#txtNumeroRUC").val();
        obj.Prv_Direccion_tx = $("#txtDireccion").val();
        obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();
        obj.Prv_Telefono1_nu = $("#txttelefono1").val();
        obj.Prv_Telefono2_nu = $("#txttelefono2").val();
        obj.Prv_Email_tx = $("#txtemail").val();
        obj.Prv_Cla_Id = $("#ddlProvedorClasificacion").val();
        obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
        //  alert('obj.Tipopersona_Id' + obj.Tipopersona_Id);
        //alert(obj);
        //alert(obj[0]);
        //return;
        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Maestros/Proveedores.aspx/ModificarProveedor',
            dataType: "json",
            data: "{ObjProveedor:" + JSON.stringify(obj) + "}",
            success: function (msg) {
                //   alert('$.ajax({ \n success: function (msg) {');
                //  alert('entro ajax juridico');
                //alert(msg.d); 
                var idProveedor = $("#hdnIdProveedor").val();


                //$("#dialog-message").dialog({
                //    title: "Mensaje",
                //    modal: true, resizable: false, draggable: false, maxWidth: 500, minWidth: 400, dialogClass: 'hide-close'
                //    , buttons: {
                //        Aceptar: function () {
                //         try { vfunction(); } catch (e) { }
                //         $(this).dialog("close");}
                //                }
                //                            });
                //$("#divMensaje").html("Los datos del proveedor fueron modificados,"+'</br>'+"Proceda a ingresar los rubros correspondientes");
                Mensaje("Mensaje", "Los datos del Proveedor Juridico fueron modificados," + '</br>' + "Proceda a ingresar los rubros correspondientes");

                // $('#hdnIdProveedor').val(msg.d);
                //alert("#hdnIdProveedor");

                //    alert("Codigo del proveedor Juridico: " + idProveedor);
                AsignarRubros();
                CargarListadeRubrosDisponibles(idProveedor);
                CargarListadeRubrosAsignados(idProveedor);
                //alert("Valor: " + $('#hdnIdProveedor').val());

            },
            error: function (xhr, status, error) {
                // alert('error: function (xhr, status, error) {');
                alert(xhr.responseText);
                Mensaje("Aviso", "Ocurrio un error en la Modificacion del Proveedor");
            }
        });
    }

    function EditarProveedorNatural() {

        var obj = {};

        // alert('Persona Natural');
        obj.Tipopersona_Id = P_Natural;//Set a 16(Natural)
        // obj.Prv_Homologado_Fl = $("#chkProveedorHomologadoFl").attr('checked');
        if ($("#chkProveedorHomologadoFl").is(':checked')) {
            obj.Prv_Homologado_Fl = true;//Set a 1(true)

            if ($("#txtPrvEstadoHomologadoID").val() != 0) {
                obj.Prv_Estado_Homologado_Id = $("#txtPrvEstadoHomologadoID").val();
            }
            if ($("#txtProvHomologadoraID").val() != 0) {
                obj.Prv_Homologadora_Id = $("#txtProvHomologadoraID").val();
            }
        } else { obj.Prv_Homologado_Fl = false; }
        obj.Prv_Id = $("#hdnIdProveedor").val();
        obj.Prv_Estado_Id = $("#txtEstadoID").val();
        obj.Prv_APaterno_tx = $("#txtApePaterno").val();
        obj.Prv_AMaterno_tx = $("#txtApeMaterno").val();
        obj.Prv_Nombres_tx = $("#txtNombres").val();
        obj.TipDocIdent_Id = $("#ddlTipoDocumento").val();
        obj.Prv_DocIdent_nu = $("#txtNumeroDocumento").val();
        obj.Prv_Nacimiento_Fe = $("#txtFechaNacimiento").val();
        obj.Prv_Direccion_tx = $("#txtDireccion").val();
        obj.Ubigeo_Id = $("#hdfCodigoUbigeo").val();
        obj.Prv_Telefono1_nu = $("#txttelefono1").val();
        obj.Prv_Telefono2_nu = $("#txttelefono2").val();
        obj.Prv_Email_tx = $("#txtemail").val();
        obj.Prv_Cla_Id = $("#ddlProvedorClasificacion").val();
        obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

        //alert("Clasific_Id: " + $("#ddlProvedorClasificacion").val() + " / Estado_Id: " + $("#txtEstadoID").val() + " / UbigeoId: " + $("#hdfCodigoUbigeo").val())


        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Maestros/Proveedores.aspx/ModificarProveedor',
            dataType: "json",
            data: "{ObjProveedor:" + JSON.stringify(obj) + "}",
            success: function (msg) {
                //   alert('entro ajax natural');
                //alert(msg.d); 
                var idProveedor = $('#hdnIdProveedor').val();
                //  alert('Codigo Proveedor Natural' + idProveedor);
                // $('#hdnIdProveedor').val(msg.d);
                //alert("#hdnIdProveedor");
                Mensaje("Mensaje", "Los datos del Proveedor Natural fueron modificados," + '</br>' + "Proceda a ingresar los rubros correspondientes");

                AsignarRubros();
                CargarListadeRubrosDisponibles(idProveedor);
                CargarListadeRubrosAsignados(idProveedor);





            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                Mensaje("Aviso", "Ocurrio un error en la Modificacion del Proveedor");
            }
        });
    }

    /* Editar Rubro a Proveedor (Solo Rubro)*/
    function CargarFormularioRubro(objProveedor) {
        var RowId = parseInt($(objProveedor).parent().parent().attr("id"), 10);
        var rowData = $('#gvProveedorGrid').getRowData(RowId);

        //// $("#ProveedorRubro").dialog("Open");
        $('#tdbtnAsigRubro').show(); //BOTONES PARA ASIGNAR RUBRO
        $('#trGuardarRubrosdeProveedor').show();
        $('#trSalirVentanaRubrosProveedor').hide();

        HabilitarBotonesAsignacionRubro();
        $("#ProveedorRubro").dialog({
            width: 725,
            height: 480,
            closeOnEscape: false,
            resizable: false,
            draggable: false,
            dialogClass: 'hide-close',
            title: "Seleccione los Rubros del Proveedor",
            modal: true
        });

        $("#hdnIdProveedor").val(rowData.Prv_Id); //El CAMPO QUE SE USO PARA ID DE PROVEEDOR  EN ASIGNACION DE RUBROS
        var idProveedor = $('#hdnIdProveedor').val();

        CargarListadeRubrosDisponibles(idProveedor);
        CargarListadeRubrosAsignados(idProveedor);
    }


    /* Visualizar Detalle */
    function CargarFormularioDetalle(objProveedor) {
        $("#dvAsignarRubros").hide();
        //MostrarCamposHabilitados();
        MostrarCamposDeshabilitados();

        var RowId = parseInt($(objProveedor).parent().parent().attr("id"), 10);
        var rowData = $('#gvProveedorGrid').getRowData(RowId);
        //
        /*------------Controles Edicion de Proveedor-------------*/
        //TIPO  16 = Natural
        if ((rowData.Par_Nombre_Tx) == 'NATURAL') {
            //$('#trbtnModificarProveedorNatural').show(); 
            $('#trbtnModificarProveedorNatural').hide(); ///////////////////////// BOTONES DE EDICION
            $('#trbtnModificarProveedorJuridico').hide();

            $('#trbtnDetalleProveedor').show(); //ESPACIO DE BOTON PARA DETALLE 
            $('#trbtnProveedorJuridico').hide();
            $('#trbtnProveedorNatural').hide();
            $('#tdDatosPrincipalesJuridico').hide();
            $('#trDatosPrincipalesNatural').show();  
            $('#tdLTipoDocumento').show();
            $('#dlSTipoDocumento').show();
            $('#tdLtxtNumeroDoc').show();
            $('#tdItxtNumeroDoc').show();
            $('#tdLFNacimineto').show();
            $('#tdIFNaciemiento').show();
            $("#btnRegistrarProveedorJuridico").hide();
            $("#btnCancelarProveedorJuridico").hide();
            /////////////////////////////////
            $("#btnRegistrarProveedorNatural").hide();
            $("#btnCancelarProveedorNatural").hide();

        }                                              //TIPO 17= Juridico
        else if ((rowData.Par_Nombre_Tx) == 'JURIDICA') {
            $('#trbtnModificarProveedorNatural').hide();
            //$('#trbtnModificarProveedorJuridico').show(); 
            $('#trbtnModificarProveedorJuridico').hide(); ///////////////////////// BOTONES DE EDICION
            $('#trbtnDetalleProveedor').show(); //ESPACIO DE BOTON PARA DETALLE 
            $('#trbtnProveedorJuridico').hide();
            $('#trbtnProveedorNatural').hide();
            $('#tdDatosPrincipalesJuridico').show(); 
            $('#trDatosPrincipalesNatural').hide();
            $('#tdLTipoDocumento').hide();
            $('#dlSTipoDocumento').hide();
            $('#tdLtxtNumeroDoc').hide();
            $('#tdItxtNumeroDoc').hide();
            $('#tdLFNacimineto').hide();
            $('#tdIFNaciemiento').hide();
            $("#btnRegistrarProveedorJuridico").hide();
            $("#btnCancelarProveedorJuridico").hide();
            $("#btnRegistrarProveedorNatural").hide();
            $("#btnCancelarProveedorNatural").hide();
        }
      
        $("#Proveedor-Reg").dialog({
            width: 1000, height: 430, title: "Detalle de Proveedor", modal: true, resizable: false,
            dialogClass: 'hide-close',
            draggable: false,
            closeOnEscape: false,
        });
        //SE OBTIENE LOS VALORES 
        $("#hdnIdProveedor").val(rowData.Prv_Id); //El CAMPO QUE SE USO PARA ID DE PROVEEDOR  EN ASIGNACION DE RUBROS
        $("#txtEstadoID").val(rowData.Prv_Estado_Id);
        $("#txtApePaterno").val(rowData.Prv_APaterno_tx);
        $("#txtApeMaterno").val(rowData.Prv_AMaterno_tx);
        $("#txtNombres").val(rowData.Prv_Nombres_tx);
        $("#txtRazonSocial").val(rowData.Prv_RazonSocial_Tx);
        $("#txtPersonaContacto").val(rowData.Prv_Personacontacto_Tx);
        $("#txtTipoDocumentoID").val(rowData.TipDocIdent_Id);
        $("#txtNumeroDocumento").val(rowData.Prv_DocIdent_nu);
        $("#txtFechaNacimiento").val(ConvertFecha(rowData.Prv_Nacimiento_Fe));
        $("#txtDireccion").val(rowData.Prv_Direccion_tx);
        $("#txtUbigeoID").val(rowData.Ubigeo_Id);
        $("#txttelefono1").val(rowData.Prv_Telefono1_nu);
        $("#txttelefono2").val(rowData.Prv_Telefono2_nu);
        $("#txtemail").val(rowData.Prv_Email_tx);
        $("#ddlProvedorClasificacion").val(rowData.Prv_Cla_Id);
        $("#txtNumeroRUC").val(rowData.Prv_DocIdent_nu);
        $("#ddlTipoDocumento").val(rowData.TipDocIdent_Id);
        $("#ddlDepartamento").val(rowData.Departamento_Co);
        CargarComboProvincia(rowData.Departamento_Co);
        $("#ddlProvincia").val(rowData.Provincia_co);
        CargarComboDistrito(rowData.Provincia_co);
        $("#ddlDistrito").val(rowData.Distrito_co);
        ObtenerCodigoUbigeo();
        if (rowData.Prv_Homologado_Fl == "false") {
            $("#chkProveedorHomologadoFl").prop("checked", false);
            $("#txtPrvEstadoHomologadoID").attr('disabled', true);
            $("#txtProvHomologadoraID").attr('disabled', true);
            $("#txtPrvEstadoHomologadoID").val("");
            $("#txtProvHomologadoraID").val("");
        }
        if ((rowData.Prv_Homologado_Fl) == "true") {
            $("#chkProveedorHomologadoFl").prop("checked", true);
            $("#txtPrvEstadoHomologadoID").attr('disabled', false);
            $("#txtProvHomologadoraID").attr('disabled', false);
            $("#txtPrvEstadoHomologadoID").val(rowData.Prv_Estado_Homologado_Id);
            $("#txtProvHomologadoraID").val(rowData.Prv_Homologadora_Id);
        }

    }


    function VisualizarRubroProveedor(objProveedor) {
        var idProveedor = $('#hdnIdProveedor').val();
        // alert('Id' + idProveedor);
        CargarListadeRubrosDisponibles(idProveedor);
        CargarListadeRubrosAsignados(idProveedor);
        $('#tdbtnAsigRubro').show();  //$('#tdbtnAsigRubro').hide(); PARA  OCULTAR LOS BOTONES 
        //   HabilitarBotonesAsignacionRubro();
        $('#trGuardarRubrosdeProveedor').hide();
        $('#trSalirVentanaRubrosProveedor').show();

        DeshabilitarBotonesAsignacionRubro();
         $("#ProveedorRubro").dialog({
            width: 725,
            height: 480,
            closeOnEscape: false,
            resizable: false,
            draggable: false,
            dialogClass: 'hide-close',
            title: "Rubros del Proveedor",
            modal: true
        });
        return false;


    }

    function DeshabilitarBotonesAsignacionRubro() {

        $("#btnAgregarSeleccionados").attr('disabled', true);
        $("#btnAgregarTodos").attr('disabled', true);
        $("#btnQuitarTodos").attr('disabled', true);
        $("#btnQuitarSeleccionados").attr('disabled', true);
    }
    function HabilitarBotonesAsignacionRubro() {

        $("#btnAgregarSeleccionados").attr('disabled', false);
        $("#btnAgregarTodos").attr('disabled', false);
        $("#btnQuitarTodos").attr('disabled', false);
        $("#btnQuitarSeleccionados").attr('disabled', false);
    }
    /*----Buscar a Proveedor--------*/
    function BuscarProveedor() {
        var obj = {};

        obj.Prv_APaterno_tx = $("#txtbusqApellidoPaterno").val();
        obj.Prv_AMaterno_tx = $("#txtbusqApellidoMaterno").val();
        obj.Prv_DocIdent_nu = $("#txtbusqNroDocumento").val()
        obj.Prv_RazonSocial_Tx = $("#txtbusqRazSocial").val();
        obj.Prv_Personacontacto_Tx = $("#txtbusqPersContacto").val();


    }



    function CargarFormularioEliminar(objProveedor) {
        // alert("¿Está seguro que desea Eliminar el registro seleccionado?")
        //function () {
        //DeshabilitarRegistro(obj);
        //  alert('Eliminar');
        //});
        var RowId = parseInt($(objProveedor).parent().parent().attr("id"), 10);
        var rowData = $('#gvProveedorGrid').getRowData(RowId);
        var obj = {};
        //obj.Prv_Id = rowData.Prv_Estado_Id;
        obj.Prv_Id = rowData.Prv_Id;
        obj.Aud_Estado_Fl = false;
        obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

        // alert(obj.Prv_Id + " / " + obj.Aud_UsuarioEliminacion_Id);
        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Maestros/Proveedores.aspx/EliminarProveedor',
            dataType: "json",
            data: "{objProveedor:" + JSON.stringify(obj) + "}",
            success: function (msg) {

                Mensaje("Mensaje", "El registro se elimino correctamente");
                ListarProveedor();
            },
            error: function (xhr, status, error) {
                // alert('error: function (xhr, status, error) {');
                alert(xhr.responseText);
                Mensaje("Aviso", "Ocurrió un error en la eliminacion");
            }



        });
    }


    function ConfirmacionDeshabilitarRegistro(objProveedor) {
        ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Deshabilitar el registro seleccionado?", function () {
            CargarFormularioEliminar(objProveedor);
        });
    }


    function CargarFormularioHabilitar(objProveedor) {

        //var objPerfilBE = {
        //    Per_id: $("#hdnId").val(),
        //    Aud_UsuarioEliminacion_Id: varUsuario,
        //    Aud_Estado_Fl: true
        //}
        var RowId = parseInt($(objProveedor).parent().parent().attr("id"), 10);
        var rowData = $('#gvProveedorGrid').getRowData(RowId);
        var obj = {};
        //obj.Prv_Id = rowData.Prv_Estado_Id;
        obj.Prv_Id = rowData.Prv_Id;
        obj.Aud_Estado_Fl = true;
        obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Logistica/Maestros/Proveedores.aspx/EliminarProveedor',
            dataType: "json",
            data: "{objProveedor:" + JSON.stringify(obj) + "}",
            success: function (msg) {
                Mensaje("Mensaje", "El registro se habilito correctamente");
                ListarProveedor();
            },
            error: function (xhr, status, error) {
                // alert('error: function (xhr, status, error) {');
                alert(xhr.responseText);
                Mensaje("Aviso", "Ocurrió un error en la habilitacion");
            }
        });
    }

    function ConfirmacionHabilitarRegistro(objProveedor) {
        ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Habilitar el registro seleccionado?", function () {
            CargarFormularioHabilitar(objProveedor);
        });
    }








    function longitudmayor(campo, len) {
        return (campo != null) ? (campo.length > len) : false;
    }

    function limitarInput(obj) {
        obj.value = obj.value.substring(0, 10);
    }

    function limitarCajaTexto(max, id) {
        if (max < document.getElementById(id).value.length)
            document.getElementById(id).value = document.getElementById(id).value.substr(0, max);
    }

    function trim(cadena) {
        cadena2 = "";
        len = cadena.length;
        for (var i = 0; i <= len ; i++) if (cadena.charAt(i) != " "
      ) { cadena2 += cadena.charAt(i); }
        return cadena2;
    }
    function esnumero(campo) { return (!(isNaN(campo))); }

    //Funcion de Modulo 11
    function valruc(valor) {
        //valor = trim(valor)
        if (esnumero(valor)) {
            if (valor.length == 8) {
                suma = 0
                for (i = 0; i < valor.length - 1; i++) {
                    digito = valor.charAt(i) - '0';
                    if (i == 0) suma += (digito * 2)
                    else suma += (digito * (valor.length - i))
                }
                resto = suma % 11;
                if (resto == 1) resto = 11;
                if (resto + (valor.charAt(valor.length - 1) - '0') == 11) {
                    return true
                }
            } else if (valor.length == 11) {
                suma = 0
                x = 6
                for (i = 0; i < valor.length - 1; i++) {
                    if (i == 4) x = 8
                    digito = valor.charAt(i) - '0';
                    x--
                    if (i == 0) suma += (digito * x)
                    else suma += (digito * x)
                }
                resto = suma % 11;
                resto = 11 - resto

                if (resto >= 10) resto = resto - 10;
                if (resto == valor.charAt(valor.length - 1) - '0') {
                    return true
                }
            }
        }
        return false
        //   return Mensaje("Aviso", "El RUC ingresado no es Valido");

    }



