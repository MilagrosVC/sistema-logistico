﻿
$(document).ready(function () {
    InicializarControles();
    sinEspaciosEnBlanco('txtMetrado')
    $("#btnBuscar").click(function () {
        ListarItems_Ma();
    });
    $("#btnLimpiar").click(function () {
        LimpiarFiltrosBusqueda();
    });

    $("#btnNuevoItemMA").click(function () {
        LimpiarDatosRegistro();
        NuevoItemMa();
    });
    $("#btnCancelarRegistro").click(function () {
        $("#ItemMa-Reg").dialog('close');
    });
    $("#btnCancelarModificar").click(function () {
        $("#ItemMa-Reg").dialog('close');
    });

    $("#btnLimpiarRegistro").click(function () {
        LimpiarDatosRegistro();
   
    });
    $("#btnRegistrar").click(function () {
        if ($("#txtCodigo").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Codigo");
            $("#txtCodigo").focus();
        } else if ($("#txtNombre").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");
            $("#txtNombre").focus();
        }
        else if ($("#txtDescripcion").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir la descripcion");
            $("#txtDescripcion").focus();
        } else { CrearItemMa(); }

        //CrearItemMa();
    });

    $("#btnModificar").click(function () {

        if ($("#txtCodigo").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Codigo");
            $("#txtCodigo").focus();
        } else if ($("#txtNombre").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir el Nombre");
            $("#txtNombre").focus();
        }
        else if ($("#txtDescripcion").val().trim() == "") {
            Mensaje("Aviso", "Tiene que escribir la descripcion");
            $("#txtDescripcion").focus();
        } else { EditarItemMa(); }
        //EditarItemMa();
    });
    $("#btnSalirDetalle").click(function () {
        $("#ItemMa-Reg").dialog('close');
    });
});



function InicializarControles() {

    CargarComboFamilia("#ddlFamilia_Busq");
    CargarComboMarca("#ddlMarca_Busq");

    CargarComboFamilia("#ddlFamilia");
    CargarComboMarca("#ddlMarca");

    ListarItems_Ma();
}

function CargarComboFamilia(control) {

    var ObjFamilia = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/ItemMa.aspx/ListarFamiliaCombo',
        data: "{ObjFamilia:" + JSON.stringify(ObjFamilia) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Fam_Nombre_Tx + "' value='" + value.Familia_Id + "'>" + value.Fam_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboMarca(control) {

    var ObjMarca = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: '../../Logistica/Maestros/ItemMa.aspx/ListarMarcaCombo',
        data: "{ObjMarca:" + JSON.stringify(ObjMarca) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Mar_Nombre_Tx + "' value='" + value.Marca_Id + "'>" + value.Mar_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function ListarItems_Ma() {
    var ObjItem_Ma = {
        IMa_Nombre_Tx: $('#txtNombreItemMa_Busq').val(),
        Familia_Id: $('#ddlFamilia_Busq').val(),
        Marca_Id: $('#ddlMarca_Busq').val()
       // Aud_Estado_Fl:
        //Mantenimiento_Item: 1
    };

    $("#divItemsMa").html('<table id="gvItemsMa"></table><div id="pieItemsMa"></div>');
    $("#gvItemsMa").jqGrid({
        regional: 'es',
        url: '../../Logistica/Maestros/ItemMa.aspx/ListarItemsMa',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjItem_Ma:" + JSON.stringify(ObjItem_Ma) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,
        colNames: ['Detalle', 'Editar', 'Eliminar', 'Codigo', 'Nombre', 'Descripcion', 'Unidad Medida', 'IMa_Metrado_Nu', 'Metrado', 'Familia', 'Marca', 'IMa_Id', 'Unidad_Medida_Id', 'Familia_Id', 'Marca_Id', 'Aud_Estado_Fl'],
        colModel: [
           //{ name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
            { name: 'Detalle', index: 'Detalle', width: 40, align: 'center', formatter: 'actionFormatterDetalle', search: false },
             { name: 'Editar', index: 'Editar', width: 40, align: 'center', formatter: 'actionFormatterEditar', search: false },
           { name: 'Eliminar', index: 'Eliminar', width: 40, align: 'center', formatter: 'actionFormatterEliminar', search: false },


           { name: 'IMa_Co', index: 'IMa_Co', width: 120, align: 'center' },
           { name: 'IMa_Nombre_Tx', index: 'IMa_Nombre_Tx', width: 300, align: 'center' },
           { name: 'IMa_Descripcion_Tx', index: 'IMa_Descripcion_Tx', width: 300, align: 'center', hidden: true },
           { name: 'UMe_Simbolo_Tx', index: 'UMe_Simbolo_Tx', width: 120, align: 'center' },
           { name: 'IMa_Metrado_Nu', index: 'IMa_Metrado_Nu', width: 120, align: 'right', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 5 }, hidden: true },
           { name: 'IMa_Metrado_tx', index: 'IMa_Metrado_tx', width: 120, align: 'center' },
           { name: 'Fam_Nombre_Tx', index: 'Fam_Nombre_Tx', width: 120, align: 'center' },
           { name: 'Mar_Nombre_Tx', index: 'Mar_Nombre_Tx', width: 150, align: 'center', },
           { name: 'IMa_Id', index: 'IMa_Id', width: 150, align: 'center', hidden: true },
            { name: 'Unidad_Medida_Id', index: 'Unidad_Medida_Id', width: 150, align: 'center', hidden: true },
            { name: 'Familia_Id', index: 'Familia_Id', width: 150, align: 'center', hidden: true },
            { name: 'Marca_Id', index: 'Marca_Id', width: 150, align: 'center', hidden: true },
            { name: 'Aud_Estado_Fl', index: 'Aud_Estado_Fl', width: 200, align: 'center', hidden: true },

        ],
        pager: "#pieItemsMa",
        //autowidth: true,
        viewrecords: true,
        width: 1325,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,
        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Bandeja de Items',
        loadComplete: function (result) {
        },

        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });







    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            if (rowObject.Aud_Estado_Fl == 1) {
                return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
            }
            else { return '<i title=\"No se puede editar el regitro\"  style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>'; }
            //return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
        },

        actionFormatterDetalle: function (cellvalue, options, rowObject) {
            //return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';

            if (rowObject.Aud_Estado_Fl == 1) {

                return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }
            else {
                return '<i title=\"No se permite Visualizar Detalle\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
            }


        },
        actionFormatterEliminar: function (cellvalue, options, rowObject) {


            //return '<i title=\"Click para eliminar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-times"></i>';  //class="fa fa-check-square
            if (rowObject.Aud_Estado_Fl == false) {
                return '<i title=\"Click para hablitar el regitro\" onclick=\"ConfirmacionHabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-trash-o" ></i>';
                //     alert('a');
            }
            else
                //class="fa fa-trash-o
                if (rowObject.Aud_Estado_Fl == true) {
                    return '<i title=\"Click para deshablitar el regitro\" onclick=\"ConfirmacionDeshabilitarRegistro(this)\" style=\"cursor:pointer\" class="fa fa-check-square"></i>';
                    //     alert('b');
                }

        }


    });







}

function LimpiarFiltrosBusqueda() {

 $('#txtNombreItemMa_Busq').val(""),
 $('#ddlFamilia_Busq').val("0"),
 $('#ddlMarca_Busq').val("0")

}

function LimpiarDatosRegistro() {
    $("#txtCodigo").val("");
    $("#txtNombre").val("");

    $("#txtDescripcion").val("");

    $("#txtMetrado").val("");
    $("#ddlUnidadMedida").val("0");
        
    $("#ddlFamilia").val("0");
    $("#ddlMarca").val("0");
    
}

//Crear Item MA
function NuevoItemMa() {
    //var PROY = $("#ContentPlaceHolder1_hdfProyecto").val();

    //$('#ddlProyecto').val(PROY);
    //$('#ddlProyecto').attr('disabled', true);
    MostrarCamposHabilitados();
    $('#btnRegistrar').show();
    $('#btnModificar').hide();
    $('#btnCancelarRegistro').show();
    $('#btnCancelarModificar').hide();
    $('#btnSalirDetalle').hide();

    $("#ItemMa-Reg").dialog({
        width: 600,
        height: 300,
        title: "Registrar Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}

function CrearItemMa() {
    var obj = {};
    //alert(' $("#hdfCodigoUbigeo").val()' + $("#hdfCodigoUbigeo").val());

    obj.IMa_Nombre_Tx = $("#txtNombre").val();
    obj.IMa_Descripcion_Tx = $("#txtDescripcion").val();
    obj.IMa_Metrado_Nu = $("#txtMetrado").val();
    obj.Unidad_Medida_Id = $("#ddlUnidadMedida").val();
    obj.Familia_Id = $("#ddlFamilia").val();
    obj.Marca_Id = $("#ddlMarca").val();
    obj.IMa_Co = $("#txtCodigo").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/ItemMa.aspx/CrearItemMA',
        dataType: "json",
        data: "{ObjItem_Ma:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "Se registro correctamente el Item");
            ListarItems_Ma();
            $("#ItemMa-Reg").dialog('close');

        },
        error: function (xhr, status, error) {
            alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
        }
    });


}

//Habilitar - Deshabilitar
function ConfirmacionDeshabilitarRegistro(ObjItem_Ma) {
    ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Deshabilitar el registro seleccionado?", function () {
        CargarFormularioEliminar(ObjItem_Ma);
    });
}

function ConfirmacionHabilitarRegistro(ObjItem_Ma) {
    ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Habilitar el registro seleccionado?", function () {
        CargarFormularioHabilitar(ObjItem_Ma);
    });
}
//Cargar Formulario Habilitar - Eliminar
function CargarFormularioEliminar(ObjItem_Ma) {
    // alert("¿Está seguro que desea Eliminar el registro seleccionado?")
    //function () {
    //DeshabilitarRegistro(obj);
    //  alert('Eliminar');
    //});
    var RowId = parseInt($(ObjItem_Ma).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMa').getRowData(RowId);
    var obj = {};
    //obj.Prv_Id = rowData.Prv_Estado_Id;
    obj.IMa_Id = rowData.IMa_Id;
    obj.Aud_Estado_Fl = false;
    obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    // alert(obj.Prv_Id + " / " + obj.Aud_UsuarioEliminacion_Id);
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/ItemMa.aspx/EliminarItemMA',
        dataType: "json",
        data: "{ObjItem_Ma:" + JSON.stringify(obj) + "}",
        success: function (msg) {

            Mensaje("Mensaje", "El registro se elimino correctamente");
            ListarItems_Ma();
        },
        error: function (xhr, status, error) {
            // alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrió un error en la eliminacion");
        }



    });
}

function CargarFormularioHabilitar(ObjItem_Ma) {

    //var objPerfilBE = {
    //    Per_id: $("#hdnId").val(),
    //    Aud_UsuarioEliminacion_Id: varUsuario,
    //    Aud_Estado_Fl: true
    //}
    var RowId = parseInt($(ObjItem_Ma).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMa').getRowData(RowId);
    var obj = {};
    //obj.Prv_Id = rowData.Prv_Estado_Id;
    obj.IMa_Id = rowData.IMa_Id;
    obj.Aud_Estado_Fl = true;
    obj.Aud_UsuarioEliminacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/ItemMa.aspx/EliminarItemMA',
        dataType: "json",
        data: "{ObjItem_Ma:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "El registro se habilito correctamente");
            ListarItems_Ma();
        },
        error: function (xhr, status, error) {
            // alert('error: function (xhr, status, error) {');
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrió un error en la habilitacion");
        }
    });
}

//Cargar Formulario Editar
function CargarFormularioEditar(ObjItem_Ma) {
    MostrarCamposHabilitados();
    $('#btnRegistrar').hide();
    $('#btnModificar').show();

    $('#btnCancelarRegistro').hide();
    $('#btnCancelarModificar').show();
    $('#btnSalirDetalle').hide();

    var RowId = parseInt($(ObjItem_Ma).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMa').getRowData(RowId);

    //SE OBTIENE LOS VALORES 
    $("#hdnIdItemMa").val(rowData.IMa_Id);
    $("#txtCodigo").val(rowData.IMa_Co);
    $("#txtNombre").val(rowData.IMa_Nombre_Tx);
    $("#txtDescripcion").val(rowData.IMa_Descripcion_Tx);
    //$("#txtMetrado").val(rowData.IMa_Metrado_Nu);
    $("#txtMetrado").val(rowData.IMa_Metrado_tx);
    $("#ddlUnidadMedida").val(rowData.Unidad_Medida_Id);
    $("#ddlFamilia").val(rowData.Familia_Id);
    $("#ddlMarca").val(rowData.Marca_Id);
   
    

    $("#ItemMa-Reg").dialog({
        width: 600,
        height: 300,
        title: "Editar Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}

function EditarItemMa() {
    var obj = {};
    obj.IMa_Id = $("#hdnIdItemMa").val();

    obj.IMa_Nombre_Tx = $("#txtNombre").val();
    obj.IMa_Descripcion_Tx = $("#txtDescripcion").val();
    obj.IMa_Metrado_Nu = $("#txtMetrado").val();
    obj.Unidad_Medida_Id = $("#ddlUnidadMedida").val();
    obj.Familia_Id = $("#ddlFamilia").val();
    obj.Marca_Id = $("#ddlMarca").val();
    obj.IMa_Co = $("#txtCodigo").val();
    obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();


    /////////////////////////////////////////
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Logistica/Maestros/ItemMa.aspx/ModificarItemMA',
        dataType: "json",
        data: "{ObjItem_Ma:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "Los datos del Item fueron Modificados");
            ListarItems_Ma();
            $("#ItemMa-Reg").dialog('close');
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrio un error en la Modificacion del Almacen");
        }
    });

}


/* Visualizar Detalle */
function CargarFormularioDetalle(ObjItem_Ma) {
    MostrarCamposDeshabilitados();
    $('#btnRegistrar').hide();
    $('#btnModificar').hide();
    $('#btnCancelarRegistro').hide();
    $('#btnCancelarModificar').hide();
    $('#btnSalirDetalle').show();
    $('#btnLimpiarRegistro').hide();
    var RowId = parseInt($(ObjItem_Ma).parent().parent().attr("id"), 10);
    var rowData = $('#gvItemsMa').getRowData(RowId);

    //SE OBTIENE LOS VALORES 
    $("#hdnIdItemMa").val(rowData.IMa_Id);
    $("#txtCodigo").val(rowData.IMa_Co);
    $("#txtNombre").val(rowData.IMa_Nombre_Tx);
    $("#txtDescripcion").val(rowData.IMa_Descripcion_Tx);
    //$("#txtMetrado").val(rowData.IMa_Metrado_Nu);
    $("#txtMetrado").val(rowData.IMa_Metrado_tx);
    $("#ddlUnidadMedida").val(rowData.Unidad_Medida_Id);
    $("#ddlFamilia").val(rowData.Familia_Id);
    $("#ddlMarca").val(rowData.Marca_Id);
   
    $("#ItemMa-Reg").dialog({
        width: 600,
        height: 300,
        title: "Detalle de Item",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
        //  ,hide: "scale"
        //   ,position: absolute
        //, show: "fold",
    });
    return false;
}

function MostrarCamposDeshabilitados() {
    $("#txtCodigo").attr('disabled', true);

    //$('#txtCodigo').attr("disabled", "disabled");
    $("#txtNombre").attr('disabled', true);
    $("#txtDescripcion").attr('disabled', true);
    $("#txtMetrado").attr('disabled', true);
    $("#ddlUnidadMedida").prop('disabled', true);
    $("#ddlFamilia").prop('disabled', true);
    $("#ddlMarca").prop('disabled', true);
    
}

function MostrarCamposHabilitados() {
    $("#txtCodigo").attr('disabled', false);
    $("#txtNombre").attr('disabled', false);
    $("#txtDescripcion").attr('disabled', false);
    $("#txtMetrado").attr('disabled', false);
    $("#ddlUnidadMedida").attr('disabled', false);
    $("#ddlFamilia").attr('disabled', false);
    $("#ddlMarca").attr('disabled', false);
}

