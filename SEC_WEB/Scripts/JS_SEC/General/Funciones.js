﻿/// <reference path="../../../Contabilidad/Maestros/PlanDeCuenta.aspx" />
$(document).ready(function () {

    //------------------------- REQUERIMIENTO -----------------------------------------------//
    CargarComboParametro('#ddlTipoServicioRQ', 7, '../../Logistica/Compras/Requerimiento.aspx');
    CargarComboUnidadMedida('#ddlUndMedidaRQ', '../../Logistica/Compras/Requerimiento.aspx');


    //-------------------------- BANDE DE REQUERIMIENTO -------------------------------------//
    CargarComboEstado('#ddlEstadoRQ', 3, '../../Logistica/Compras/RequerimientoUsuario.aspx');

    //------------------------- BANDEJA ORDEN COMPRA ------------------------------------------//
    CargarComboParametro('#ddlTipoBanOC', 7, '../../Logistica/Compras/BandejaOrdenCompra.aspx');
    CargarComboEstado('#ddlEstadoBanOC', 4, '../../Logistica/Compras/BandejaOrdenCompra.aspx');
    CargarComboProveedor('#ddlProveedorBanOC', '../../Logistica/Compras/BandejaOrdenCompra.aspx');
    //------------------------  ORDEN DE COMPRA ---------------------------------------------//

    CargarComboParametro('#ddlTipoOrdenOC', 7, '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboTipoMedioPago('#ddlFormaPagoOC', '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboMoneda('#ddlMonedaOC', '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboParametro('#ddlTipoOrdenInfoOC', 7, '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboTipoMedioPago('#ddlFormaPagoInfoOC', '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboMoneda('#ddlMonedaInfoOC', '../../Logistica/Compras/OrdenCompra.aspx');
    CargarComboProveedor('#ddlProveedorOC', '../../Logistica/Compras/OrdenCompra.aspx');
    //---------------------------------------------------------------------------------------//

    //---------------- SOLICITUD DE ANTICIPO --------------//
    //Datos Para busqueda
    CargarComboParametro('#ddlTipoAnticipo', 13, '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboTipoMedioPago('#ddlFormaPago', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboMoneda('#ddlMoneda', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    //----Datos Solicitados
    CargarComboMoneda('#ddlMonedaId', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboParametro('#ddlTipoAnticipoId', 13, '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboTipoMedioPago('#ddlTipoPagoId', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboBanco('#dllBancoOrigenId', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboBanco('#dllBancoDestinoId', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    //----Buscar Egresos
    CargarComboMoneda('#ddlMonedaEgreso', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboParametro('#ddlTipoAnticipoEgreso', 13, '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboTipoMedioPago('#ddlFormaPagoEgreso', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboBanco('#ddlBancoOrigenEgreso', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');
    CargarComboBanco('#ddlBancoDestinoEgreso', '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx');

    //----SolicitudCompra
    CargarComboProveedor('#ddlProveedores', '../../Logistica/Compras/SolicitudCompra.aspx');
    CargarComboProveedor('#ddlProveedores_2', '../../Logistica/Compras/SolicitudCompra.aspx');
    CargarComboProveedor('#EditSol-ddlProveedores', '../../Logistica/Compras/SolicitudCompra.aspx');

    //----Cotizaciones
    //CargarComboProveedor('#ddlProveedor', '../../Logistica/Compras/CotizacionRequerimiento.aspx');
    CargarComboEstado('#ddlEstadoCot', 11, '../../Logistica/Compras/CotizacionRequerimiento.aspx');
    CargarComboParametro('#ddlTipoSolicitud', 13, '../../Logistica/Compras/CotizacionRequerimiento.aspx');
    CargarComboTipoMedioPago('#ddlTipoPago', '../../Logistica/Compras/CotizacionRequerimiento.aspx');
    CargarComboMoneda('#ddlMoneda', '../../Logistica/Compras/CotizacionRequerimiento.aspx');
    CargarComboUnidadMedida('#ddlDetCot-Und', '../../Logistica/Compras/CotizacionRequerimiento.aspx');

    //------------------------- PLAN DE CUENTA ---------------------------------------------//
    CargarComboEmpresa('#ddlRazSocialEmpresaID', '../../Contabilidad/Maestros/PlanDeCuenta.aspx');
    //---------------------CUENTAS DE PLAN DE CUENTA------------------
    CargarComboBanco('#ddlEntidadFinancieraID', '../../Contabilidad/Maestros/Cuentas.aspx');
    CargarComboMoneda('#ddlTipoMonedaCuenta', '../../Contabilidad/Maestros/Cuentas.aspx');
    //--------------------BAND COTIZACIONES---------------//
    CargarComboProveedor('#ddlProveedorCo', '../../Logistica/Compras/BandejaCotizacion.aspx');
    CargarComboEstado('#ddlEstadoCo', 11, '../../Logistica/Compras/BandejaCotizacion.aspx');

    //-------------------ALMACEN -----------------------------//

    CargarComboParametro('#ddlTipoAlmacen', 14, '../../Logistica/Maestros/Almacen.aspx');
    CargarComboProyecto('#ddlProyecto', '../../Logistica/Maestros/Almacen.aspx');
    CargarComboUsuario('#ddlUsuarioResponsable', 8, '../../Logistica/Maestros/Almacen.aspx');
    CargarComboParametro('#ddlTipoAlmacenBusq', 14, '../../Logistica/Maestros/Almacen.aspx');

    //------ITEM MA--------//
    CargarComboUnidadMedida('#ddlUnidadMedida', '../../Logistica/Maestros/ItemMa.aspx');
    // StockMinimo
    CargarComboUnidadMedida('#ddlUnidadMedida_Detalle', '../../Logistica/Almacen/StockMinimo.aspx');
});

function CargarComboParametro(control, padreId, urlf) {

    urlf = urlf + '/ListarComboParametro';

    var ObjParametro = {
        Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{ObjParametro:" + JSON.stringify(ObjParametro) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Par_Nombre_Tx + "' value='" + value.Par_Id + "'>" + value.Par_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboEstado(control, padreId, urlf) {
    urlf = urlf + '/ListarComboEstado';

    var ObjEstadoDe = {
        Estado_CA_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{ObjEstadoDe:" + JSON.stringify(ObjEstadoDe) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Est_Nombre_Tx + "' value='" + value.Estado_Id + "'>" + value.Est_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboMoneda(control, urlf) {

    urlf = urlf + '/ListarComboMoneda';

    var ObjMoneda = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{ObjMoneda:" + JSON.stringify(ObjMoneda) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Mon_Nombre_Tx + "' value='" + value.Moneda_Id + "'>" + value.Mon_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboTipoMedioPago(control, urlf) {
    urlf = urlf + '/ListarComboTipoMedioPago';
    var ObjTipoMedioPago = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{ObjTipoMedioPago:" + JSON.stringify(ObjTipoMedioPago) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,

        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.TMP_Nombre_Tx + "' value='" + value.Tipo_Medio_Pago_Id + "'>" + value.TMP_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });


}

function CargarComboBanco(control, urlf) {
    urlf = urlf + '/ListarComboBanco';
    var objBanco = {
        //Par_Padre_Id: padreId
    };
    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{objBanco:" + JSON.stringify(objBanco) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Ban_Nombre_Tx + "' value='" + value.Banco_Id + "'>" + value.Ban_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}

function CargarComboProveedor(control, urlf)
{
    urlf = urlf + '/ListarComboProveedor';
    var objProveedor = { };
    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{objProveedor:" + JSON.stringify(objProveedor) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.NombreProveedor_Tx + "' value='" + value.Prv_Id + "'>" + value.NombreProveedor_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}


function CargarComboEmpresa(control, urlf) {
    urlf = urlf + '/ListarComboEmpresa';
    var ObjEmpresa = {};
    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{ObjEmpresa:" + JSON.stringify(ObjEmpresa) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Emp_RazonSocial_Tx + "' value='" + value.Empresa_Id + "'>" + value.Emp_RazonSocial_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}


function CargarComboUnidadMedida(control, urlf) {

    var ObjUndMedida = {};

    $.ajax({
        type: 'POST',
        url: urlf + '/ListarUnidadMedidaCombo',
        data: "{ObjUndMedida:" + JSON.stringify(ObjUndMedida) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.UMe_Simbolo_Tx + "' value='" + value.Unidad_Medida_Id + "'>" + value.UMe_Simbolo_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}



//17 Septiembre 2015

function CargarComboProyecto(control, urlf) {
    //alert('function CargarComboProyecto(control, urlf) { desde funciones control: ' + control + 'urlf: ' + urlf);
    urlf = urlf + '/ListarProyectoCombo';

    var objProyecto = {
        //Par_Padre_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{objProyecto:" + JSON.stringify(objProyecto) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Pry_Nombre_Tx + "' value='" + value.Proyecto_Id + "'>" + value.Pry_Nombre_Tx + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}





///////////////////////////////////////////////

function CargarComboUsuario(control, padreId, urlf) {

    urlf = urlf + '/ListarComboUsuario';

    var objUsuarioBE = {
        Per_Id: padreId
    };

    $.ajax({
        type: 'POST',
        url: urlf,
        data: "{objUsuarioBE:" + JSON.stringify(objUsuarioBE) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            $(control).empty();
            var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
            if (msg.d != null) {
                $.each(msg.d, function (index, value) {
                    sItems += ("<option title='" + value.Usu_Id + "' value='" + value.Usu_Id + "'>" + value.Usu_Id + "</option>");
                });
            }
            $(control).html(sItems);
        },
        error: function () {
            alert("Error!...");
        }
    });
}