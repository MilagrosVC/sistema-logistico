﻿$(document).ready(function () {
    IniciarControles();
});

function IniciarControles() {

    $("#btnLogin").click(function () {
        if ($('#txtUsuario').val() == '' || $('#txtUsuario').val() == null) {
            Mensaje("Aviso", "Ingrese el nombre de Usuario");
            return;
        }
        else if ($('#txtPassword').val() == '' || $('#txtPassword').val() == null) {
            Mensaje("Aviso", "Ingrese el password");
            return;
        } else {
            ValidarUsuarioLista();
        }
    });

    $("#btnLimpiar").click(function () {
        $('#txtUsuario').val('');
        $('#txtPassword').val('');
        $("#txtUsuario").focus();
    });

    $('#txtUsuario').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($('#txtUsuario').val() == '' || $('#txtUsuario').val() == null) {
                Mensaje("Aviso", "Ingrese el nombre de Usuario");
                return;
            }
            else if ($('#txtPassword').val() == '' || $('#txtPassword').val() == null) {
                Mensaje("Aviso", "Ingrese el password");
                return;
            } else {
                ValidarUsuarioLista();
            }
        }
    });

    $('#txtPassword').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            if ($('#txtUsuario').val() == '' || $('#txtUsuario').val() == null) {
                Mensaje("Aviso", "Ingrese el nombre de Usuario");
                return;
            }
            else if ($('#txtPassword').val() == '' || $('#txtPassword').val() == null) {
                Mensaje("Aviso", "Ingrese el password");
                return;
            } else {
                ValidarUsuarioLista();
            }
        }
    });
}

function ValidarUsuarioLista() {
    var ObjUsuario = {
        Usu_Id: $('#txtUsuario').val(),
        Usu_Password_Tx: $('#txtPassword').val()
    };

    $.ajax({
        type: 'POST',
        url: '../Acceso/Login.aspx/ValidarUsuarioLista',
        data: "{ObjUsuario:" + JSON.stringify(ObjUsuario) + "}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (msg) {
            if (msg.d == '' || msg == null) {
                MensajeLogin("Aviso", "Usuario y/o Password Incorrecto", 0);
            } else {

                generarMenu();

                $.each(msg.d, function (index, value) {
                    var Mensaje = "Bienvenido al sistema " + value.NombreCompleto;
                    MensajeLogin("Usuario Identificado", Mensaje, 1);
                    setTimeout(function () {
                        window.location.href = "../Home/Form/Home.aspx";
                    }, 1800);
                });
            }            
        },
        error: function () {
            alert("Error!...");
        }
    });
}

/*
function ValidarUsuario() {
    var ObjUsuario = {
        Usu_Id: $('#txtUsuario').val(),
        Usu_Password_Tx: $('#txtPassword').val()
    };

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../Acceso/Login.aspx/ValidarUsuario',
        data: "{ObjUsuario:" + JSON.stringify(ObjUsuario) + "}",
        success: function (data) {
            if (data.d != null) {
                if (data.d == 1) {                    
                    var Mensaje = "Bienvenido al sistema " + 
                    MensajeLogin("Usuario Identificado", "Bienvenido al sistema",1);
                    setTimeout(function () {
                        window.location.href = "../Home/Home.aspx";
                    }, 2000);

                } else {
                    MensajeLogin("Aviso", "Usuario y/o Password Incorrecto",0);
                }
            }
        }
    });
}*/

function generarMenu() {
    //alert('function generarMenu() {');
    var ObjUsuario = {
        Usu_Id: $('#txtUsuario').val(),
        Usu_Password_Tx: $('#txtPassword').val()
    };

    $.ajax({

        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../Acceso/Login.aspx/generarMenu',
        data: "{ObjUsuario:" + JSON.stringify(ObjUsuario) + "}",
        success: function (data) {
            if (data.d != null) {
            }
        }
    });
}