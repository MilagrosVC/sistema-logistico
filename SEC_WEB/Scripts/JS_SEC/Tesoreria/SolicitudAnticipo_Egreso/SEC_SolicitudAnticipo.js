﻿var visible = true;

$(document).ready(function () {

    InicializarControles();
    //ListarSolicitudesAnticipo();
    $("#btnLimpiarBusqueda").click(function () {
        LimpiarFiltrosBuqueda();
    });

    $("#btnLimpiarCampos").click(function () {
        LimpiarDatosAtencion();
    });

    $("#btnLimpiarPantallaGerente").click(function () {
        /*PRUEBA GIT*/
        LimpiarDatosAprobacionGerente();
    });

    $("#btnGrabarAprobacion").click(function () {
        //if ($("#EstadoAprobada").is(':checked') == true) { alert('valor' + $("#EstadoAprobada").is(':checked')); }
        //alert('valor1 ' + $("#EstadoAprobada").is(':checked'));
        //alert('valor2 ' + $("#EstadoDenegada").is(':checked'));
        //return;     
        if ($("#EstadoAprobada").is(':checked')==false && $("#EstadoDenegada").is(':checked')==false) {
            Mensaje("Mensaje", "Se tiene que seleccionar al una opción");
            return;
        } else {
            RegistrarAprobacion();
            CerrarFormularioAtenciondeSolicitud();
        }
        
    });


    $("#btnSalirPantallaAtencion").click(function () {

        LimpiarDatosAtencion();
        LimpiarDatosAprobacionGerente();
        CerrarFormularioAtenciondeSolicitud();
    });

    $("#btnSalirPantallaGerente").click(function () {

        LimpiarDatosAtencion();
        LimpiarDatosAprobacionGerente();
        CerrarFormularioAtenciondeSolicitud();
    });
    //Graba los datos de Pago Solicitado
    $("#btnGrabarPagoSolicitado").click(function () {
        CrearEgreso();
        LimpiarDatosAtencion();
        CerrarFormularioAtenciondeSolicitud();

    });

    $("#btnRealizarBusqueda").click(function () {

        ListarSolicitudesAnticipo();
    });

    $("#btnVerEgresos").click(function () {
        LimpiarDataBusquedaEgreso();
        CargarFormularioEgresos();
        ListarSolicitudesAtendidas_Egreso();
    });

    $("#btnLimpiarBusquedaEgreso").click(function () {
        LimpiarDataBusquedaEgreso();
    });

    $("#btnRealizarBusquedaEgreso").click(function () {

        ListarSolicitudesAtendidas_Egreso();
    });
    $("#btnSalirPantallaEgreso").click(function () {

        CerrarListaEgreso();
    });

    $("#btnModificarEgreso").click(function () {
        EditarEgreso();
        CerrarFormularioAtenciondeSolicitud();
    });
});


function InicializarControles() {
    ListarSolicitudesAnticipo();
    LimpiarFiltrosBuqueda();
    LimpiarDatosAtencion();
    InhabilitarCamposdeSolicitud();
    LimpiarDataBusquedaEgreso();

   // alert('PERFIL ID ' + $("#ContentPlaceHolder1_hdIdPerfil").val());
    
    //NUEVOS CAMBIO 01 SEPTIEMBRE - 11:17 am

    ValidarAccionesporUsuario();
}

//funcion para validar opciones segun usuario
function ValidarAccionesporUsuario() {
    var GG = 5;
    var TS = 4;

    if ($("#ContentPlaceHolder1_hdIdPerfil").val() == GG) {
  //      alert('USUARIO GG');
        $('#DVDatosAtencionTesoreria').hide();
        $('#DVDatosAtencionGerente').show();
        visible = true;


    }
    else if ($("#ContentPlaceHolder1_hdIdPerfil").val() == TS) {
     //   alert('USUARIO TS');
        $('#DVDatosAtencionTesoreria').show();
        $('#DVDatosAtencionGerente').hide();
        visible = false;
    }
    else { alert('No tiene permiso para ingresar a esta bandeja'); }

}

function ListarSolicitudesAnticipo() {
    var objSolicitudAnticipo = {
        //Cotizacion_Id: 0,//$("#txtNumeroCotizacion").val(),
        //Moneda_Id: 0,
        //FormaPago_Id: 0,
        //Sol_Tipo_Anticipo_Id: 0
        Moneda_Id: $("#ddlMoneda").val(),
        FormaPago_Id: $("#ddlFormaPago").val(),
        Sol_Tipo_Anticipo_Id: $("#ddlTipoAnticipo").val(),
        //NUEVO PARAMETRO 01 SEPTIEMBRE
        Per_Id: $("#ContentPlaceHolder1_hdIdPerfil").val()
    };
    //alert("Mensaje Grilla");

    $("#SolicitudAnticipoGrid").html('<table id="gvSolicitudAnticipoGrid"></table><div id="pieSolicitudAnticipoGrid"></div>');
    $("#gvSolicitudAnticipoGrid").jqGrid({
        regional: 'es',
        url: '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx/ListarSolicitudAnticipo',
        datatype: "json",
        mtype: "POST",
        postData: "{objSolicitudAnticipo:" + JSON.stringify(objSolicitudAnticipo) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,

        colNames: ['Atender', 'Cod Solicitud', 'Cotizacion','N° Proforma', 'Tipo Proveedor', 'Proveedor', 'Monto Solicitado', 'Moneda', 'Monto', 'Medio Pago', 'Tipo Anticipo', '', '', '', '', '', '', ''],
        // colNames: ['Atender', 'Cod Solicitud', 'Numero Cotizacion', 'Moneda', 'Monto', 'Medio Pago', 'Tipo Anticipo', '', '', '', '', ''],
        colModel: [
          { name: 'Atender', index: 'Atender', width: 40, align: 'center', formatter: 'actionFormatterAtender', search: false },
          { name: 'Solicitud_Anticipo_Id', index: 'Solicitud_Anticipo_Id', width: 60, align: 'center' },

          { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 60, align: 'center', hidden: true },
          { name: 'Cot_NumProforma_Nu', index: 'Cot_NumProforma_Nu ', width: 60, align: 'center'},

          { name: 'TipoPersona_TXx', index: 'TipoPersona_TXx', width: 60, align: 'center' },
          { name: 'NombreProveedor_Tx', index: 'NombreProveedor_Tx', width: 130, align: 'center' },
          { name: 'Moneda_Monto_Tx', index: 'Moneda_Monto_Tx', width: 66, align: 'right' },

          { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 80, align: 'center', hidden: true },
          { name: 'Sol_Monto_Solicitado_nu', index: 'Sol_Monto_Solicitado_nu', width: 60, align: 'center', hidden: true },
          { name: 'TMP_Nombre_Tx', index: 'TMP_Nombre_Tx', width: 200, align: 'center' },
          { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 70, align: 'center' },

        //HIDDEN
          { name: 'Moneda_Id', index: 'Moneda_Id', width: 200, align: 'center', hidden: true },
          { name: 'FormaPago_Id', index: 'FormaPago_Id', width: 200, align: 'center', hidden: true },
          { name: 'Sol_Tipo_Anticipo_Id', index: 'Sol_Tipo_Anticipo_Id', width: 200, align: 'center', hidden: true },
          { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 200, align: 'center', hidden: true },
          { name: 'Solicitud_Anticipo_Id', index: 'Solicitud_Anticipo_Id', width: 200, align: 'center', hidden: true },
          { name: 'Estado_Solicitud_Id', index: 'Estado_Solicitud_Id', width: 200, align: 'center', hidden: true },
          { name: 'TipoPersona_Id', index: 'TipoPersona_Id', width: 200, align: 'center', hidden: true },


        ],
        pager: "#pieSolicitudAnticipoGrid",
        viewrecords: true,
        width: 1300,//1200,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Solicitudes Pendientes',
        loadComplete: function (result) {
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            //alert(xhr.responseText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterAtender: function (cellvalue, options, rowObject) {
            return "<img title=\"Realizar Atencion\" onclick=\"CargarFormularioAtender(this)\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/GenSolicitud.png\" />";

        }

    });
}

function LimpiarFiltrosBuqueda() {
    $("#ddlMoneda").val("0");
    $("#ddlFormaPago").val("0");
    $("#ddlTipoAnticipo").val("0");
}
function LimpiarDatosAtencion() {
    //Campos Solicitados
    $("#txtNumeroCheque").val("");
    $("#txtNumeroOperacion").val("");
    $("#txtTipoCambio").val("");
    $("#dllBancoOrigenId").val("0");
    $("#dllBancoDestinoId").val("0");
    $("#txtNumeroCuentaOrigen").val("");
    $("#txtNumeroCuentaDestino").val("");
}
//01 Septiembre
function LimpiarDatosAprobacionGerente() {
    $("#txtObservacion").val("");
    $('#EstadoAprobada').attr('readonly', false);
    $('#EstadoDenegada').attr('readonly', false);
    $('#EstadoAprobada').prop('checked', false);
    $('#EstadoDenegada').prop('checked', false);
}



function InhabilitarCamposdeSolicitud() {
    $("#txtCodigoSolicitud").attr('readonly', true);
    $("#txtProveedor").attr('readonly', true);

    $("#txtTipoPersona").attr('readonly', true);
    $("#txtCotizacionId").attr('readonly', true);
    $("#ddlMonedaId").attr('disabled', true);
    $("#txtMontoSolicitado").attr('readonly', true);
    $("#ddlTipoAnticipoId").attr('disabled', true);
    //Estado
    $("#ddlEstadoSolicitudId").attr('disabled', true);
    //Tipo de Pago
    $("#ddlTipoPagoId").attr('disabled', true);


}
function CargarFormularioAtender(objSolicitudAnticipo) {
    //alert('function CargarFormularioAtender(objSolicitudAnticipo) { ');
    $('#btnModificarEgreso').hide();
    $('#btnGrabarPagoSolicitado').show();
    LimpiarDatosAtencion();
    LimpiarDatosAprobacionGerente();
    var RowId = parseInt($(objSolicitudAnticipo).parent().parent().attr("id"), 10);
    var rowData = $('#gvSolicitudAnticipoGrid').getRowData(RowId);

    var ObjMedioPago = {
        NumCheque: 0,
        NumOpe: 0,
        //   TipoCambio: 0,
        BancoOrigen: 0,
        BancoDestino: 0,
        NumCuentaOri: 0,
        NumCuentaDes: 0
    };

    var ObjTipoCambio = { TipoCambio: 0 };

    if ((rowData.TMP_Nombre_Tx) == 'DEPÓSITO EN CUENTA') { //Deposito en Cuenta
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'GIRO') { //Giro
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'TRANSFERENCIA DE FONDOS') { //Transferencia Fondos
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'ORDEN DE PAGO') { //Orden Pago
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }

    else if ((rowData.TMP_Nombre_Tx) == 'CHEQUE') { //Cheque con clausula
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 0;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }

    else if ((rowData.FormaPago_Id) == '16') { //Remesa
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'CREDIPAGO') { //CREDIPAGO
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else {
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);

    }
    if ((rowData.Moneda_Id) == '1') {
        ObjTipoCambio.TipoCambio = 0;
        HabilitarTipoCambio(ObjTipoCambio);
    } else {
        ObjTipoCambio.TipoCambio = 1;
        HabilitarTipoCambio(ObjTipoCambio);
    }

    //  alert('Proveedor a Atender con codigo : ' + rowData.Solicitud_Anticipo_Id);
    $("#txtCodigoSolicitud").val(rowData.Solicitud_Anticipo_Id);
    //NombreProveedor

    $("#txtProveedor").val(rowData.NombreProveedor_Tx);
    $("#txtTipoPersona").val(rowData.TipoPersona_TXx);
    $("#txtCotizacionId").val(rowData.Cotizacion_Id);
    $("#ddlMonedaId").val(rowData.Moneda_Id);
    //$("#txtMontoSolicitado").val(rowData.Sol_Monto_Solicitado_nu); //
    $("#txtMontoSolicitado").val(rowData.Moneda_Monto_Tx);
    //Forma de Pago
    $("#ddlTipoAnticipoId").val(rowData.Sol_Tipo_Anticipo_Id);
    //Tipo de Pago
    $("#ddlTipoPagoId").val(rowData.FormaPago_Id);

    $("#DvAtencionSolicitudAnticipo").dialog({
        width: 1105,
        height: 'auto', //330
        title: "Atencion de Solicitud",
        modal: true
        , resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
    });
    return false;

}



function CerrarFormularioAtenciondeSolicitud() {
    $("#DvAtencionSolicitudAnticipo").dialog('close');
    LimpiarDatosAtencion();
    //ListarSolicitudesAnticipo();
}

function CrearEgreso() {
    var obj = {};
    obj.Solicitud_Anticipo_Id = $("#txtCodigoSolicitud").val();
    obj.Ege_NumeroCheque = $("#txtNumeroCheque").val();
    obj.Ege_NumeroOperacion = $("#txtNumeroOperacion").val();
    obj.Ege_TipoCambio = $("#txtTipoCambio").val();
    if ($("#dllBancoOrigenId").val() != 0) {
        obj.BancoOrigen_Id = $("#dllBancoOrigenId").val();
    }
    if ($("#dllBancoDestinoId").val() != 0) {
        obj.BancoDestino_Id = $("#dllBancoDestinoId").val();
    }

    obj.Ege_CuentaOrigen = $("#txtNumeroCuentaOrigen").val();
    obj.Ege_CuentaDestino = $("#txtNumeroCuentaDestino").val();
    obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx/CrearEgreso',
        dataType: "json",
        data: "{ObjEgreso:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            $('#hdnIdEgreso').val(msg.d);
            Mensaje("Mensaje", "Se registro correctamente el Pago de Anticipo");
            ListarSolicitudesAnticipo();

        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}

function CargarFormularioEgresos() {

    $("#DVVisualizarEgreso").dialog({
        width: 1335,
        height: 510,
        title: "Solicitudes Antendidas",
        modal: true,
        resizable: false,
        dialogClass: 'hide-close',
        closeOnEscape: false,
        draggable: false

        //,position: 'center'
    });

    return false;

}

function LimpiarDataBusquedaEgreso() {
    $("#ddlMonedaEgreso").val("0");
    $("#ddlFormaPagoEgreso").val("0");
    $("#ddlTipoAnticipoEgreso").val("0");
    $("#ddlBancoOrigenEgreso").val("0");
    $("#ddlBancoDestinoEgreso").val("0");
}

function ListarSolicitudesAtendidas_Egreso() {

    var ObjEgreso = {

        Moneda_Id: $("#ddlMonedaEgreso").val(),
        FormaPago_Id: $("#ddlFormaPagoEgreso").val(),
        Sol_Tipo_Anticipo_Id: $("#ddlTipoAnticipoEgreso").val(),
        BancoOrigen_Id: $("#ddlBancoOrigenEgreso").val(),
        BancoDestino_Id: $("#ddlBancoDestinoEgreso").val()
    };
    $("#EgresoGrid").html('<table id="gvEgresoGrid"></table><div id="pieEgresoGrid"></div>');
    $("#gvEgresoGrid").jqGrid({

        regional: 'es',
        url: '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx/ListarEgreso',
        datatype: "json",
        mtype: "POST",
        postData: "{ObjEgreso:" + JSON.stringify(ObjEgreso) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,

        //colNames: ['Solicitud_Anticipo_Id', 'Cotizacion_Id', 'Mon_Nombre_Tx', 'Sol_Monto_Solicitado_nu', 'TMP_Nombre_Tx', 'Par_Nombre_Tx',
        //    'Ban_Nombre_Tx', 'Ban_Nombre_Tx_Destino', 'Ege_NumeroCheque', 'Ege_NumeroOperacion', 'Ege_TipoCambio', 'Ege_CuentaOrigen', 'Ege_CuentaDestino', '', '', '', '', '', ''],

        //colNames: ['N° Solicitud', 'N° Cot.', 'Tipo Persona', 'Proveedor', 'Moneda', 'Monto', 'Medio Pago', 'Tipo Anticipo',
        //  'Banco Origen', 'Banco Destino', 'Num Cheque', 'Num Operacion', 'Tipo Cambio', 'Cuenta Origen', 'Cuenta Destino', 'Fecha y Hora', '', '', '', '', '', '', '', ''],

        //colNames: ['N° Solicitud', 'N° Cot.', 'Tipo Persona', 'Proveedor', 'Monto', 'Medio Pago', 'Tipo Anticipo',
        //    'Banco Origen', 'Banco Destino', 'Num Cheque', 'Num Operacion', 'Tipo Cambio', 'Cuenta Origen', 'Cuenta Destino',  'Fecha y Hora','', '', '','','', '', '', '', '', ''],

        colNames: ['', 'N° Solicitud', 'N° Cot.', 'Tipo Persona', 'Proveedor', 'Monto', 'Medio Pago', 'Tipo Anticipo',
            'Banco Origen', 'Banco Destino', 'Num Cheque', 'Num Operacion', 'Tipo Cambio', 'Cuenta Origen', 'Cuenta Destino', 'Fecha y Hora', '', '', '', '', '', '', '', '', '', ''],

        colModel: [
          //{ name: 'Atender', index: 'Atender', width: 55, align: 'center', formatter: 'actionFormatterAtender', search: false },
          { name: 'Editar', index: 'Editar', width: 40, align: 'center', formatter: 'actionFormatterEditarEgreso', search: false, hidden: visible },
          { name: 'Solicitud_Anticipo_Id', index: 'Solicitud Anticipo', width: 180, align: 'center' },
          { name: 'Cotizacion_Id', index: 'Cotizacion_Id', width: 100, align: 'center' },
        //  { name: 'Mon_Nombre_Tx', index: 'Mon_Nombre_Tx', width: 200, align: 'center' },
        { name: 'TipoPersona_TXx', index: 'TipoPersona_TXx', width: 200, align: 'center', hidden: true },
         { name: 'NombreProveedor_Tx', index: 'NombreProveedor_Tx', width: 360, align: 'center' },
         { name: 'Moneda_Monto_Tx', index: 'Moneda_Monto_Tx', width: 250, align: 'right' },
         //{ name: 'Mon_Simbolo_Tx', index: 'Mon_Simbolo_Tx', width: 120, align: 'center' },
         // { name: 'Sol_Monto_Solicitado_nu', index: 'Sol_Monto_Solicitado_nu', width: 200, align: 'center' },
          { name: 'TMP_Nombre_Tx', index: 'TMP_Nombre_Tx', width: 200, align: 'center' },
          { name: 'Par_Nombre_Tx', index: 'Par_Nombre_Tx', width: 200, align: 'center' },
          { name: 'Ban_Nombre_Tx', index: 'Ban_Nombre_Tx', width: 260, align: 'center' },
          { name: 'Ban_Nombre_Tx_Destino', index: 'Ban_Nombre_Tx_Destino', width: 260, align: 'center' },
          { name: 'Ege_NumeroCheque', index: 'Ege_NumeroCheque', width: 200, align: 'center' },
          { name: 'Ege_NumeroOperacion', index: 'Ege_NumeroOperacion', width: 200, align: 'center' },
          { name: 'Ege_TipoCambio', index: 'Ege_TipoCambio', width: 200, align: 'center' },
          { name: 'Ege_CuentaOrigen', index: 'Ege_CuentaOrigen', width: 200, align: 'center' },
          { name: 'Ege_CuentaDestino', index: 'Ege_CuentaDestino', width: 200, align: 'center' },

          //FECHA Y HORA
         // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', datefmt: "m/d/Y h:i AmPm" },
         // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y h:i:s', newformat: 'Y-m-d h:i:s' }, sorttype: "date", datefmt: "m/d/Y h:i:s" },
         // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y h:i:s', newformat: 'Y-m-d h:i:s' }, sorttype: "date", datefmt: "m/d/Y h:i:s" },
         // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y h:i:s', newformat: 'd-m-Y h:i:s' }, sorttype: "date", datefmt: "m/d/Y h:i:s" },
         // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y H:i:s A', newformat: 'd-m-Y H:i:s A' }, sorttype: "date", datefmt: "m/d/Y H:i:s " },
         { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 300, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y H:i:s', newformat: 'd-m-Y H:i:s' }, sorttype: "date", datefmt: "m/d/Y H:i:s " },

        //HIDDEN
          { name: 'Moneda_Id', index: 'Moneda_Id', width: 200, align: 'center', hidden: true },
          { name: 'FormaPago_Id', index: 'FormaPago_Id', width: 200, align: 'center', hidden: true },
          { name: 'Sol_Tipo_Anticipo_Id', index: 'Sol_Tipo_Anticipo_Id', width: 200, align: 'center', hidden: true },
          { name: 'BancoOrigen_Id', index: 'BancoOrigen_Id', width: 200, align: 'center', hidden: true },
          { name: 'BancoDestino_Id', index: 'BancoDestino_Id', width: 200, align: 'center', hidden: true },
          { name: 'Egreso_Id', index: 'Egreso_Id', width: 200, align: 'center', hidden: true },
          { name: 'Proveedor_Id', index: 'Proveedor_Id', width: 200, align: 'center', hidden: true },
          { name: 'TipoPersona_Id', index: 'TipoPersona_Id', width: 200, align: 'center', hidden: true },
           { name: 'Mon_Simbolo_Tx', index: 'Mon_Simbolo_Tx', width: 120, align: 'center', hidden: true },
          { name: 'Sol_Monto_Solicitado_nu', index: 'Sol_Monto_Solicitado_nu', width: 200, align: 'center', hidden: true },
        ],
        pager: "#pieEgresoGrid",
        viewrecords: true,
        width: 1310,
        height: 230,
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Atenciones Registradas',
        loadComplete: function (result) {
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
            //alert(xhr.responseText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterEditarEgreso: function (cellvalue, options, rowObject) { //
            //return "<img title=\"Editar\" style=\"cursor:pointer\" src=\"../../Scripts/JQuery-ui-1.11.4/images/Icons_SEC/GenSolicitud.png\" />";
            return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditarEgreso(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
        }

    });
    //alert(ConvertFecha(Aud_Creacion_Fe));
}
function CerrarListaEgreso() {

    $("#DVVisualizarEgreso").dialog('close');
    ListarSolicitudesAnticipo();
}

/////EDITAR
function CargarFormularioEditarEgreso(ObjEgreso) {

    $('#btnModificarEgreso').show();
    $('#btnGrabarPagoSolicitado').hide();
    LimpiarDatosAtencion();

    var RowId = parseInt($(ObjEgreso).parent().parent().attr("id"), 10);
    var rowData = $('#gvEgresoGrid').getRowData(RowId);
    ///////////////////////////

    var ObjMedioPago = {
        NumCheque: 0,
        NumOpe: 0,
        //   TipoCambio: 0,
        BancoOrigen: 0,
        BancoDestino: 0,
        NumCuentaOri: 0,
        NumCuentaDes: 0
    };

    var ObjTipoCambio = { TipoCambio: 0 };
    if ((rowData.TMP_Nombre_Tx) == 'DEPÓSITO EN CUENTA') { //Deposito en Cuenta
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'GIRO') { //Giro
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'TRANSFERENCIA DE FONDOS') { //Transferencia Fondos
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'ORDEN DE PAGO') { //Orden Pago
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 0;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }

    else if ((rowData.TMP_Nombre_Tx) == 'CHEQUE') { //Cheque con clausula
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 0;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }

    else if ((rowData.FormaPago_Id) == '16') { //Remesa
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else if ((rowData.TMP_Nombre_Tx) == 'CREDIPAGO') { //CREDIPAGO
        ObjMedioPago.NumCheque = 0;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 0;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 0;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);
    }
    else {
        ObjMedioPago.NumCheque = 1;
        ObjMedioPago.NumOpe = 1;
        //   ObjMedioPago.TipoCambio = 1;
        ObjMedioPago.BancoOrigen = 1;
        ObjMedioPago.BancoDestino = 1;
        ObjMedioPago.NumCuentaOri = 1;
        ObjMedioPago.NumCuentaDes = 1;

        HabilitarPorMedioPago(ObjMedioPago);

    }
    if ((rowData.Moneda_Id) == '1') {
        ObjTipoCambio.TipoCambio = 0;
        HabilitarTipoCambio(ObjTipoCambio);
    } else {
        ObjTipoCambio.TipoCambio = 1;
        HabilitarTipoCambio(ObjTipoCambio);
    }

    //  alert('Proveedor a Atender con codigo : ' + rowData.Solicitud_Anticipo_Id);



    $("#hdnIdEgreso").val(rowData.Egreso_Id);
    $("#txtCodigoSolicitud").val(rowData.Solicitud_Anticipo_Id);
    //NombreProveedor

    $("#txtProveedor").val(rowData.NombreProveedor_Tx);
    $("#txtTipoPersona").val(rowData.TipoPersona_TXx);
    $("#txtCotizacionId").val(rowData.Cotizacion_Id);
    $("#ddlMonedaId").val(rowData.Moneda_Id);
    //$("#txtMontoSolicitado").val(rowData.Sol_Monto_Solicitado_nu); //
    $("#txtMontoSolicitado").val(rowData.Moneda_Monto_Tx);
    //Forma de Pago
    $("#ddlTipoAnticipoId").val(rowData.Sol_Tipo_Anticipo_Id);
    //Tipo de Pago
    $("#ddlTipoPagoId").val(rowData.FormaPago_Id);

    ///DATOS ADICIONALES
    $("#txtNumeroCheque").val(rowData.Ege_NumeroCheque);
    $("#txtNumeroOperacion").val(rowData.Ege_NumeroOperacion);
    $("#txtTipoCambio").val(rowData.Ege_TipoCambio);
    //alert(rowData.BancoOrigen_Id);
    if (rowData.BancoOrigen_Id > "0") {
        $("#dllBancoOrigenId").val(rowData.BancoOrigen_Id);
    } else { $("#dllBancoOrigenId").val("0"); }

    if (rowData.BancoOrigen_Id > "0") {
        $("#dllBancoDestinoId").val(rowData.BancoDestino_Id);
    } else { $("#dllBancoDestinoId").val("0"); }


    $("#txtNumeroCuentaOrigen").val(rowData.Ege_CuentaOrigen);
    $("#txtNumeroCuentaDestino").val(rowData.Ege_CuentaDestino);


    //SE ABRE DIALOG
    $("#DvAtencionSolicitudAnticipo").dialog({
        width: 1105,
        height: 'auto', //330
        title: "Edicion de Solicitud",
        modal: true
        , resizable: false,
        dialogClass: 'hide-close',
        draggable: false,
        closeOnEscape: false
    });
    return false;

    //SE OBTIENE LOS DATOS

}
function EditarEgreso() {
    var obj = {};
    obj.Egreso_Id = $("#hdnIdEgreso").val();
    obj.Solicitud_Anticipo_Id = $("#txtCodigoSolicitud").val();
    obj.Ege_NumeroCheque = $("#txtNumeroCheque").val();
    obj.Ege_NumeroOperacion = $("#txtNumeroOperacion").val();
    obj.Ege_TipoCambio = $("#txtTipoCambio").val();
    if ($("#dllBancoOrigenId").val() != 0) {
        obj.BancoOrigen_Id = $("#dllBancoOrigenId").val();
    }
    if ($("#dllBancoDestinoId").val() != 0) {
        obj.BancoDestino_Id = $("#dllBancoDestinoId").val();
    }

    obj.Ege_CuentaOrigen = $("#txtNumeroCuentaOrigen").val();
    obj.Ege_CuentaDestino = $("#txtNumeroCuentaDestino").val();
    obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx/ModificarEgreso',
        dataType: "json",
        data: "{ObjEgreso:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "Los datos fueron modificados correctamente");
            ListarSolicitudesAtendidas_Egreso();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
            Mensaje("Aviso", "Ocurrio un error en la Modificacion");
        }

    });
}

function HabilitarPorMedioPago(ObjMedioPago) {
    //alert('function HabilitarPorMedioPago(ObjMedioPago) {');

    //alert('ObjMedioPago.NumCheque=' + ObjMedioPago.NumCheque);
    //alert('NumCheque' + NumCheque);
    if (ObjMedioPago.NumCheque == 1)
    { $("#txtNumeroCheque").attr('readonly', false); }
    else
    { $("#txtNumeroCheque").attr('readonly', true); }
    //NumeroOperacion
    if (ObjMedioPago.NumOpe == 1)
    { $("#txtNumeroOperacion").attr('readonly', false); }
    else
    { $("#txtNumeroOperacion").attr('readonly', true); }
    //TipoCambio
    //if (ObjMedioPago.TipoCambio == 1)
    //{ $("#txtTipoCambio").attr('readonly', false); }
    //else
    //{ $("#txtTipoCambio").attr('readonly', true); }
    //BancoOrigen
    if (ObjMedioPago.BancoOrigen == 1)
    { $("#dllBancoOrigenId").attr('disabled', false); }
    else
    { $("#dllBancoOrigenId").attr('disabled', true); }
    //BancoDestino
    if (ObjMedioPago.BancoDestino == 1)
    { $("#dllBancoDestinoId").attr('disabled', false); }
    else
    { $("#dllBancoDestinoId").attr('disabled', true); }
    //CuentaOrigen
    if (ObjMedioPago.NumCuentaOri == 1)
    { $("#txtNumeroCuentaOrigen").attr('readonly', false); }
    else
    { $("#txtNumeroCuentaOrigen").attr('readonly', true); }
    //CuentaDestino
    if (ObjMedioPago.NumCuentaDes == 1)
    { $("#txtNumeroCuentaDestino").attr('readonly', false); }
    else
    { $("#txtNumeroCuentaDestino").attr('readonly', true); }
}

function HabilitarTipoCambio(ObjTipoCambio) {

    if (ObjTipoCambio.TipoCambio == 1)
    { $("#txtTipoCambio").attr('readonly', false); }
    else
    { $("#txtTipoCambio").attr('readonly', true); }

}
//Funcion para grabar aprobacion - 01 Septiembre 
//Similar a editar solicitud anticipo porque se modifica el estado segun el flat seleccionado
function RegistrarAprobacion() {
  
    var Aprobada= 1; //donde 1 es aprobado //= 53; //A
    var Denegada= 2; //donde 2 es desprobado //= 54; //D 

    var obj = {};
    obj.Solicitud_Anticipo_Id = $("#txtCodigoSolicitud").val();

    ////ESTADOS
    if ($("#EstadoAprobada").is(':checked')) {
        obj.Estado_Solicitud_Id = Aprobada;  //Aprobado
    }
    else
        if ($("#EstadoDenegada").is(':checked')) {
            obj.Estado_Solicitud_Id = Denegada; //Desaprobado
        }  
        else { obj.Estado_Solicitud_Id = Denegada; }

    ///TERMINO ESTADOS

    obj.Sol_Observacion_tx = $("#txtObservacion").val();
    obj.Aud_UsuarioActualizacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
    $.ajax({
        async: true,
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../Tesoreria/Solicitudes_Anticipo_Egreso/SolicitudAnticipo_Egreso.aspx/ModificarEstadoAnticipo',
        dataType: "json",
        data: "{objSolicitudAnticipo:" + JSON.stringify(obj) + "}",
        success: function (msg) {
            Mensaje("Mensaje", "Se realizo correctamente la atencion del anticipo");
            ListarSolicitudesAnticipo();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}