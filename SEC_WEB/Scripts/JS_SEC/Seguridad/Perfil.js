﻿
$(document).ready(function () {
    //alert('$(document).ready(function () {');
    //$('#btnBuscarPerfil').hide();
    GenerarBandeja();
    //$("#UserSesion").val($("#hdf_Usuario_Smo").val());
    //$('#btnBuscarPerfil').live('click', GenerarBandeja);
    CargarFormulario();
    $("#btnCrearPerfil").click(function () {
        //alert('$("#btnCrearPerfil").click(function () {');
        $("#dialog-form").dialog("open");
        $("#Aceptar").show();
        $("#Editar").hide(); $("#Deshabilitar").hide(); $("#Habilitar").hide();
        $("#txtNombre").attr('disabled', false);
        $("#txtAreaDescripcion").attr('disabled', false);
        //alert('despues de $("#dialog-form").dialog("open");');
    });
    
});

function GenerarBandeja() {

    var objPerfilBE = {

    };
    //alert('function GenerarBandeja() {');
    $("#pnlPerfil").html('<table id="gvPerfil"></table><div id="piePaginaPerfil"></div>');
    $("#gvPerfil").jqGrid({
        regional: 'es',
        url: '../Seguridad/Perfil.aspx/ListarPerfil',
        datatype: "json",
        mtype: "POST",
        postData: "{objPerfilBE:" + JSON.stringify(objPerfilBE) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,

        		
        colNames: ['', '', '', 'Id', 'Nombre', 'Descripcion'],
        colModel: [
            //{ name: 'Permisos', index: 'Permisos', width: 30, align: 'center', formatter: 'actionFormatterPermisos', search: false },
            { name: 'Editar', index: 'Editar', width: 30, align: 'center', formatter: 'actionFormatterEditar', search: false },
            { name: 'Eliminar', index: 'Eliminar', width: 30, align: 'center', formatter: 'actionFormatterHabilitar', search: false },
            { name: 'Visualizar', index: 'Visualizar', width: 30, align: 'center', formatter: 'actionFormatterVisualizar', search: false },
            { name: 'Per_Id', index: 'Id', hidden: false, key: true, align: 'center', width: 30 },
            { name: 'Per_Nombre_Tx', index: 'Descripcion', width: 150 },
            { name: 'Per_Descripcion_Tx', index: 'PadreId', width: 350 }
        ],
        pager: "#pieProveedorGrid",
        viewrecords: true,
        width: 1200,
        height: 230,
        rowNum: 20,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },

        emptyrecords: "No hay Registros.",
        caption: 'Mantenimiento de Perfiles',
        loadComplete: function (result) { 
        },
        loadError: function (xhr) {
            //alert('loadError: function (xhr) {');
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText);
        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterEditar: function (cellvalue, options, rowObject) {
            return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioEditar(this)\" style=\"cursor:pointer\" class="fa fa-pencil-square-o"></i>';
        }
        ,actionFormatterHabilitar: function (cellvalue, options, rowObject) {
            if (rowObject.Aud_Estado_Fl == false) {
                return '<i title=\"Click para hablitar el regitro\" onclick=\"CargarFormularioHablitar(this)\" style=\"cursor:pointer\" class="fa fa-trash-o"></i>';
            }
            else
                if (rowObject.Aud_Estado_Fl == true) {
                    return '<i title=\"Click para deshablitar el regitro\" onclick=\"CargarFormularioDeshablitar(this)\" style=\"cursor:pointer\" class="fa fa-check-square"></i>';
                }
        }
        , actionFormatterVisualizar: function (cellvalue, options, rowObject) {
            return '<i title=\"Click para editar el regitro\" onclick=\"CargarFormularioVisualizar(this)\" style=\"cursor:pointer\" class="fa fa-eye"></i>';
            //fa fa-tripadvisor
        }
    });
}

function CargarFormulario() {
    //alert('function CargarFormulario() {');
    var varUsuario = 'System';
    $("#dialog-form").dialog(
        {
        autoOpen: false,
        resizable: false,
        draggable: true,
        height: 'auto',
        width: 500,
        modal: true,
        buttons:[ 
            {
                id: "Aceptar",
                text: "Grabar Registro",
                //disabled: true,
                
                click: function () {
                    
                    //alert('$("#dialog-form").dialog({\n\t Aceptar: function () {');
                    var objPerfilBE = {
                        Per_Nombre_Tx: $("#txtNombre").val(),
                        Per_Descripcion_Tx: $("#txtAreaDescripcion").val(),
                        Aud_UsuarioCreacion_Id: varUsuario
                    }

                    $.ajax({
                        async: true,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '../Seguridad/Perfil.aspx/CrearPerfil',
                        dataType: "json",
                        data: "{objPerfilBE:" + JSON.stringify(objPerfilBE) + "}",
                        success: function (msg) {
                            Mensaje('Aviso', 'Se realizó el registro del nuevo perfil');
                            GenerarBandeja()
                            $("#dialog-form").dialog("close");
                        },
                        error: function (xhr, status, error) {
                            alert(xhr.responseText);
                        }
                    });
                }
            },
        { 
            id: "Editar",
            text: "Editar Registro",
            click: function () {

                var objPerfilBE = {
                    Per_id: $("#hdnId").val(),
                    Per_Nombre_Tx: $("#txtNombre").val(),
                    Per_Descripcion_Tx: $("#txtAreaDescripcion").val(),
                    Aud_UsuarioActualizacion_Id: varUsuario
                                        }
                    $.ajax({
                        async: true,
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '../Seguridad/Perfil.aspx/ActualizarPerfil',
                        dataType: "json",
                        data: "{objPerfilBE:" + JSON.stringify(objPerfilBE) + "}",
                        success: function (msg) {
                            Mensaje('Aviso', 'Se realizó la actualización del registro');
                            GenerarBandeja()
                            $("#dialog-form").dialog("close");
                        },
                        error: function (xhr, status, error) {
                            alert(xhr.responseText);
                        }
                    });
            }
        },
          {
              id: "Deshabilitar",
              text: "Deshabilitar Perfil",
              click: function () {

                  var objPerfilBE = {
                      Per_id: $("#hdnId").val(),
                      Aud_UsuarioEliminacion_Id: varUsuario,
                      Aud_Estado_Fl:false
                  }
                  $.ajax({
                      async: true,
                      type: "POST",
                      contentType: "application/json; charset=utf-8",
                      url: '../Seguridad/Perfil.aspx/EliminarPerfil',
                      dataType: "json",
                      data: "{objPerfilBE:" + JSON.stringify(objPerfilBE) + "}",
                      success: function (msg) {
                          Mensaje('Aviso','Se realizó la deshabilitación del registro');
                          GenerarBandeja()
                          $("#dialog-form").dialog("close");
                      },
                      error: function (xhr, status, error) {
                          alert(xhr.responseText);
                      }
                  });
              }
          },
               {
                   id: "Habilitar",
                   text: "Habilitar Perfil",
                   click: function () {

                       var objPerfilBE = {
                           Per_id: $("#hdnId").val(),
                           Aud_UsuarioEliminacion_Id: varUsuario,
                           Aud_Estado_Fl: true
                       }
                       $.ajax({
                           async: true,
                           type: "POST",
                           contentType: "application/json; charset=utf-8",
                           url: '../Seguridad/Perfil.aspx/EliminarPerfil',
                           dataType: "json",
                           data: "{objPerfilBE:" + JSON.stringify(objPerfilBE) + "}",
                           success: function (msg) {
                               Mensaje('Aviso','Se realizó la habilitación del registro');
                               GenerarBandeja()
                               $("#dialog-form").dialog("close");
                           },
                           error: function (xhr, status, error) {
                               alert(xhr.responseText);
                           }
                       });
                   }
               },

          {       id: "Cancelar",
                  text: "Cancelar",
                  click: function () {
                  $("#dialog-form").dialog("close");

              }
          }
        ],
        
        close: function () {
            limpiar();
        }
    });
}

function limpiar(){
    $("#txtNombre").val("");
    $("#txtAreaDescripcion").val("");

}

function CargarFormularioEditar(obj) {
    //alert('function CargarFormularioEditar(obj) {');

    var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPerfil").getRowData(rowId);
    //$("#hdnId").val(rowData.Id);
    
    $("#hdnId").val(rowData.Per_Id );
    $("#txtNombre").val(rowData.Per_Nombre_Tx);
    $("#txtAreaDescripcion").val(rowData.Per_Descripcion_Tx);
    $("#txtNombre").attr('disabled', false);
    $("#txtAreaDescripcion").attr('disabled', false);
    $("#dialog-form").dialog("open");
    //$("#Aceptar").attr('disabled', true);
    $("#Aceptar").hide(); $("#Deshabilitar").hide(); $("#Habilitar").hide();
    $("#Editar").show();
}

function CargarFormularioDeshablitar(obj) {
    //alert('function CargarFormularioDesHablitar() {');
    var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPerfil").getRowData(rowId);
    $("#hdnId").val(rowData.Per_Id);
    $("#txtNombre").val(rowData.Per_Nombre_Tx);
    $("#txtAreaDescripcion").val(rowData.Per_Descripcion_Tx);
    $("#txtNombre").attr('disabled', true);
    $("#txtAreaDescripcion").attr('disabled', true);

    $("#dialog-form").dialog("open");
    $("#Aceptar").hide();
    $("#Editar").hide();
    $("#Habilitar").hide();
    $("#Deshabilitar").show();
}

function CargarFormularioHablitar(obj) {
    //alert('function CargarFormularioHablitar(obj) {');
    var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPerfil").getRowData(rowId);
    $("#hdnId").val(rowData.Per_Id);
    $("#txtNombre").val(rowData.Per_Nombre_Tx);
    $("#txtAreaDescripcion").val(rowData.Per_Descripcion_Tx);
    $("#txtNombre").attr('disabled', true);
    $("#txtAreaDescripcion").attr('disabled', true);
    $("#dialog-form").dialog("open");
    $("#Aceptar").hide();
    $("#Editar").hide();
    $("#Deshabilitar").hide();
    $("#Habilitar").show();

}
function CargarFormularioVisualizar(obj) {
    var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPerfil").getRowData(rowId);
    $("#hdnId").val(rowData.Per_Id);
    $("#txtNombre").val(rowData.Per_Nombre_Tx);
    $("#txtAreaDescripcion").val(rowData.Per_Descripcion_Tx);
    $("#txtNombre").attr('disabled', true);
    $("#txtAreaDescripcion").attr('disabled', true);
    $("#dialog-form").dialog("open");
    $("#Aceptar").hide();
    $("#Editar").hide();
    $("#Deshabilitar").hide();
    $("#Habilitar").hide();

}

function AgregarPermisos(obj) {


    var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPerfil").getRowData(rowId);
    $('#hdnIdPerfil').val(rowId);

    CargarListados();

    $("#pnlAccesos").dialog('open');

}


    function CargarListados() {

        var idperfil = $('#hdnIdPerfil').val();

        $.ajax({
            url: BASE_URL + '/Service/Seguridad.svc/ListarPerfilNavegacionDisponible?idPerfil=' + idperfil,
            cache: false,
            success: function (data) {
                var sItems = "";
                $('#ddlAccesos').empty();
                if (data != null) {
                    $.each(data, function (index, value) {
                        sItems += ("<option title='" + value.Descripcion + "' value='" + value.Id + "'>" + value.Descripcion + "</option>");
                    });
                    $('#ddlAccesos').append(sItems);
                }
            },
            error: function (data) {
                alert('Error');
            }
        });

        $.ajax({
            url: BASE_URL + '/Service/Seguridad.svc/ListarPerfilNavegacionAsignados?idPerfil=' + idperfil,
            cache: false,
            success: function (data) {
                var sItems = "";
                $('#ddlAccesosAsignados').empty();
                if (data != null) {
                    $.each(data, function (index, value) {
                        sItems += ("<option title='" + value.Descripcion + "' value='" + value.Id + "'>" + value.Descripcion + "</option>");
                    });
                    $('#ddlAccesosAsignados').append(sItems);
                }
            },
            error: function (data) {
                alert('Error');
            }
        });

    }

    
    function EliminarRegistro(obj) {
        var rowId = parseInt($(obj).parent().parent().attr("id"), 10);
        var rowData = $("#gvPerfil").getRowData(rowId);

        var perfil = {};
        perfil.Id = rowData.Id;

        $.ajax({
            type: 'GET',
            url: BASE_URL + '/Service/Seguridad.svc/EliminarPerfil',
            data: { perfil: JSON.stringify(perfil) },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#dialog-form").dialog("close");
                Mensaje("Mensaje", "El registro se eliminó correctamente");
                GenerarBandeja();
            },
            error: function (msg) {
                Mensaje("Aviso", "Ocurrió un error en la eliminación");
            }
        });
    }

    function ConfirmacionEliminarRegistro(obj) {
        ConfirmacionMensaje("Confirmación", "¿Está seguro que desea Eliminar el registro seleccionado?", function () {
            EliminarRegistro(obj);
        });
    }

    $('#btnDesignarTodos').live('click', function () {

        $("#ddlAccesosAsignados option").attr("selected", "selected");

        var seleccionados = $('#ddlAccesosAsignados').val().join(',');

        EditarAcceso(seleccionados, false);
    });

    $('#btnDesignarSeleccionados').live('click', function () {

        var seleccionados = $('#ddlAccesosAsignados').val().join(',');

        EditarAcceso(seleccionados, false);

    });

    $('#btnAsignarTodos').live('click', function () {

        $("#ddlAccesos option").attr("selected", "selected");

        var seleccionados = $('#ddlAccesos').val().join(',');

        EditarAcceso(seleccionados, true);

    });

    $('#btnAsignarSeleccionados').live('click', function () {

        var seleccionados = $('#ddlAccesos').val().join(',');

        EditarAcceso(seleccionados, true);

    });


    function EditarAcceso(seleccion, estado) {
        $.ajax({
            type: 'GET',
            url: BASE_URL + '/Service/Seguridad.svc/ModificarPerfilDetalle',
            data: {
                lstNavegacion: seleccion, perfil: parseInt($('#hdnIdPerfil').val(), 10),
                usuario: $("#hdf_Usuario_Smo").val(), estado: estado
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                $("#pnlAccesos").dialog("close");
                Mensaje("Mensaje", "La transaccion se ejecuto correctamente");
                GenerarBandeja();
            },
            error: function (msg) {
                Mensaje("Aviso", "Ocurrió un error en la transaccion");
            }
        });

        CargarListados();
    }