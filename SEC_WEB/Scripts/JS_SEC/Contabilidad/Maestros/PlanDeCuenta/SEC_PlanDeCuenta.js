﻿/// <reference path="../../../../../Contabilidad/Maestros/PlanDeCuenta.aspx" />
/// <reference path="../../../../../Contabilidad/Maestros/PlanDeCuenta.aspx" />
$(document).ready(function () {
    InicializarControles();
    $("#btnBuscarPlanCuenta").click(function () {
        ListarPlanDeCuenta();
    });
    $("#btnLimpiarFiltrosBusq").click(function () {
        LimpiarFiltrosBusq();

    });
    

});

function InicializarControles() {
    ListarPlanDeCuenta();
    // LimpiarDataBusquedaPlanDeCuenta();
    //LimpiarFiltrosBusq();
}

function LimpiarFiltrosBusq() {
    $("#ddlRazSocialEmpresaID").val(0);

    $("#txtPla_Nombre_Tx").val("");
}

function ListarPlanDeCuenta() {
    var objPlanDeCuenta = {
       // PlanCuenta_Id:null,
        Pla_Nombre_Tx: $("#txtPla_Nombre_Tx").val(),
       //  Empresa_Id: 0
         Empresa_Id: $("#ddlRazSocialEmpresaID").val()

    };
 // alert('Empresaid  :  ' + $("#ddlRazSocialEmpresaID").val());
   // alert('Listar Plan Cuenta');

    $("#PlanCuentaGrid").html('<table id="gvPlanCuentaGrid"></table><div id="piePlanCuentaGrid"></div>');
    $("#gvPlanCuentaGrid").jqGrid({
        regional: 'es',
        url: '../../Contabilidad/Maestros/PlanDeCuenta.aspx/ListarPlanDeCuenta',
        datatype: "json",
        mtype: "POST",
        postData: "{objPlanDeCuenta:" + JSON.stringify(objPlanDeCuenta) + "}",
        ajaxGridOptions: { contentType: "application/json" },
        loadonce: true,

        colNames: ['Ir a Cuentas','N° Plan Cuenta', 'Nombre Plan Cuenta', 'Empresa', 'EmpresaID', 'RUC', 'Fecha Creacion','Hora Creacion'],
        colModel: [
           { name: 'Cuentas', index: 'Cuentas', width: 55, align: 'center', formatter: 'actionFormatterCuentas', search: false },
           { name: 'PlanCuenta_Id', index: 'PlanCuenta_Id', width: 100, align: 'center' },
           { name: 'Pla_Nombre_Tx', index: 'Pla_Nombre_Tx', width: 100, align: 'center' },
           { name: 'Emp_RazonSocial_Tx', index: 'Emp_RazonSocial_Tx', width: 100, align: 'center' }, // 1
           { name: 'Empresa_Id', index: 'Empresa_Id', width: 100, align: 'center', hidden: true },
           { name: 'Emp_DocIdent_RUC_nu', index: 'Emp_DocIdent_RUC_nu', width: 100, align: 'center' }, //2
          // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center', formatter: 'date', datefmt: "m/d/Y h:i AmPm" },
          { name: 'PC_FechaCreacion', index: 'PC_FechaCreacion', width: 100, align: 'center', formatter: 'date', datefmt: "m/d/Y h:i AmPm" },
          // { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center' }, //3
           { name: 'Aud_Creacion_Fe', index: 'Aud_Creacion_Fe', width: 100, align: 'center', formatter: 'date', formatoptions: { srcformat: 'm/d/Y H:i:s A', newformat: 'H:i:s A' }, sorttype: "date", datefmt: "m/d/Y H:i:s " },
           //{ name: 'PC_FechaCreacion', index: 'PC_FechaCreacion', width: 100, align: 'center' }, //3
           //{ name: 'PC_HoraCreacion', index: 'PC_HoraCreacion', width: 100, align: 'center' }, //4
        ],
        pager: "#piePlanCuentaGrid",
        viewrecords: true,
        width: 1300,
        height: 'auto',
        rowNum: 10,
        rowList: [10, 20, 30, 100],
        gridview: true,

        jsonReader: {
            page: function (obj) { return 1; },
            total: function (obj) { return 1; },
            records: function (obj) { return obj.d.length; },
            root: function (obj) { return obj.d; },
            repeatitems: false,
            id: "0"
        },
        emptyrecords: "No hay Registros.",
        caption: 'Plan Cuenta',
        loadComplete: function (result) {
        },
        loadError: function (xhr) {
            alert("The Status code:" + xhr.status + " Message:" + xhr.statusText+"A:" + xhr.result);
            //alert(xhr.responseText);

        }
    });
    $.extend($.fn.fmatter, {
        actionFormatterCuentas: function (cellvalue, options, rowObject) {
            return '<i title=\"Ir a Cuentas\" style=\"cursor:pointer\" class="fa fa-book" onclick=\"CargarFormularioDetalle(this)\"></i>';
            //  return '<i title=\"Click para Visualizar Detalle\" onclick=\"CargarFormularioDetalle(this)\" style=\"cursor:pointer\" class="fa fa-info-circle"></i>';
     }

    });
}

function CargarFormularioDetalle(obj) {
    var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
    var rowData = $("#gvPlanCuentaGrid").getRowData(RowId);
    //  alert('Dato' + rowData.PlanCuenta_Id);
   // alert('Dato Fecha : ' + rowData.PC_FechaCreacion + 'Dato Hora : ' + rowData.Aud_Creacion_Fe);
    //window.location = BASE_URL + '/Contabilidad/Maestros/Cuentas.aspx?PlanCuenta_Id=' + rowData.PlanCuenta_Id;
    window.location = BASE_URL + '/Contabilidad/Maestros/Cuentas.aspx?PlanCuenta_Id=' + rowData.PlanCuenta_Id+
                                                                    '&Pla_Nombre_Tx='+rowData.Pla_Nombre_Tx+
                                                                    '&Emp_RazonSocial_Tx=' + rowData.Emp_RazonSocial_Tx +
                                                                    '&Emp_DocIdent_RUC_nu=' + rowData.Emp_DocIdent_RUC_nu+
                                                                    '&PC_FechaCreacion=' + rowData.PC_FechaCreacion +
                                                                    '&Aud_Creacion_Fe=' + rowData.Aud_Creacion_Fe;
}
