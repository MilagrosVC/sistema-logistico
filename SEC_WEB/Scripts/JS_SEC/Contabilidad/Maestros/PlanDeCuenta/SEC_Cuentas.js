﻿
$(document).ready(function () {
    InicializarControles();
    //$('#LBEmpresa').val(RazonSocial);
    //$("#ContentPlaceHolder1_hdf_PlanCuentaID").val();
    //alert($("#ContentPlaceHolder1_hdf_PlanCuentaID").val());
    $("#btnCrearTransferenciaDestino").attr('disabled', false);
    $("#btnBuscarCuentas").click(function () {
        ListarCuenta();
    });
    $("#btnBuscarCuentaTransf").click(function () {
        ListarCuentaSeleccion();
    });
    $("#btnLimpiarBusqueda").click(function () {
        LimpiarDataBusqueda();
        LimpiarDatosNuevaCuentaPorDefecto();
        //Mensaje("Aviso", "No existe Numero de Cuenta en seleccion");
    });
    $("#btnLimpiarCamposNuevaCuenta").click(function () {
        LimpiarDatosNuevaCuentaIngreso();
        //Mensaje("Aviso", "No existe Numero de Cuenta en seleccion");
    });
    $("#btnLimpiarCamposDestino").click(function () {
        LimpiarDatosNuevoDestinoIngreso();
        //Mensaje("Aviso", "No existe Numero de Cuenta en seleccion");
    });
    $("#btnCrearCuentas").click(function () {

        //  alert('a;');
        //  SeleccionarPadre(obj);
        //BOTONES
   
        $('#btnGrabarCuenta').show();
        $('#btnLimpiarCamposDestino').hide();
        $("#btnLimpiarCamposNuevaCuenta").show();


        $('#trCrearDestino').hide();
        $('#trCrearSubCuenta').show();
        $('#txtNombreCuenta').attr('readonly', false);
        $('#txtTA').attr('readonly', false);

        
        //FLAT
        $('#flatAsociado').attr('disabled', false);
        $('#flatSINAsociar').attr('disabled', false);

        // FLATT NINGUNO DESACTIVADO
        $('#flatTipoNinguno').attr('disabled', false);


        $('#btnGrabarCuentaTransferencia').hide();
        if ($("#txtCodCuentaPadreID").val() == '') {
            Mensaje("Aviso", "Tiene que seleccionar la Cuenta a la que Pertenecerá la nueva Sub-Cuenta");
            return;
        } else {
            NuevaCuenta();

            //CerrarFormularioNuevaCuenta();
        }
    });


    
    $("#btnCrearCuentaDestinoMens").click(function () {
        //
        $('#btnGrabarCuenta').hide();
        $('#btnLimpiarCamposDestino').show();
        $("#btnLimpiarCamposNuevaCuenta").hide();

        $('#trCrearDestino').show();
        $('#trCrearSubCuenta').hide();
        $('#txtNombreCuenta').attr('readonly', true);
        $('#txtTA').attr('readonly', true);
        //FLAT
        $('#flatAsociado').attr('disabled', true);
        $('#flatSINAsociar').attr('disabled', true);

        // FLATT NINGUNO DESACTIVADO
        $('#flatTipoNinguno').attr('disabled', true);
        $('#btnGrabarCuentaTransferencia').show();
        //FALTA CAPTURAR DATOS
        NuevaCuentaDestino();
        //CERRAR VENTANA
        CerrarMensajedeRegistroCuenta();
    });

    $("#btnCrearTransferenciaDestino").click(function () {
        //  alert('a;');
        //  SeleccionarPadre(obj);
        //BOTONEs
        

        $('#btnGrabarCuenta').hide();
        $('#btnLimpiarCamposDestino').show();
        $("#btnLimpiarCamposNuevaCuenta").hide();

        $('#trCrearDestino').show();
        $('#trCrearSubCuenta').hide();
        $('#txtNombreCuenta').attr('readonly', true);
        $('#txtTA').attr('readonly', true);
        //FLAT
        $('#flatAsociado').attr('disabled', true);
        $('#flatSINAsociar').attr('disabled', true);

        // FLATT NINGUNO DESACTIVADO
        $('#flatTipoNinguno').attr('disabled', true);

        $('#btnGrabarCuentaTransferencia').show();
        if (($("#txtCodCuentaPadreID").val() == '')) {
            Mensaje("Aviso", "Tiene que seleccionar la Cuenta - Transferencia - Destino");
            return;
        }
        else if(($("#txtNombreCuenta").val() == '')){
            Mensaje("Aviso", "Seleccione nuevamente la Cuenta - Transferencia - Destino");
            return;
        }
         else { NuevaCuentaDestino(); }
    });
    
    //////////////////////////////////// MENSAJE DDE REGISTRO CUENT

    $("#btnSalirMensaje").click(function () {
        CerrarMensajedeRegistroCuenta();
    });
    
    $("#btnAceptarMensaje").click(function () {
        CerrarMensajedeRegistroCuenta();
    });
    /////////////////////////////////////////////////////////////////
    $("#btnSalir").click(function () {
        //LimpiarDataBusqueda();
        //CerrarFormularioNuevaCuenta();
        //  $("#DVCrearCuenta").dialog('close');
        CerrarFormularioNuevaCuenta();
        //LimpiarDatosNuevaCuentaPorDefecto();
    });

    $("#btnSeleccionarTransfiere").click(function () {
        //LimpiarDataBusqueda();
        SeleccionarCuentaTranfiere();
         ListarCuentaSeleccion();
        //ListarCuenta();
    });
    $("#btnSalirSeleccionCuentaTransf").click(function () {
        CerrarFormularioSeleccionCuentaTrans();//  SalirSeleccionCuentaTrans();
        LimpiarFiltrosSeleccionCuenta();
        $("#txtTransfiere").val("");
    });
    $("#btnSeleccionCuentaTransf").click(function () {
        CerrarFormularioSeleccionCuentaTrans();//  SalirSeleccionCuentaTrans();
        //LimpiarFiltrosSeleccionCuenta();
    });
    $("#btnLimpiarFiltrosCuentaTransf").click(function () {
        //LimpiarDataBusqueda();
        LimpiarFiltrosSeleccionCuenta();
        $("#txtTransfiere").val("");
    });
    
    
    $("#btnGrabarCuenta").click(function () {
        //LimpiarDataBusqueda();
        // ValidarPorcentajeDebeHaber();
       // CerrarFormularioNuevaCuenta();
        CrearCuentaNueva();


    });
    $("#btnGrabarCuentaTransferencia").click(function () {
        //LimpiarDataBusqueda();
        // ValidarPorcentajeDebeHaber();
        //CerrarFormularioNuevaCuenta();
        CrearCuentaDestino();
    });
    
});

function InicializarControles() {
    CargarDataPlanDeCuenta();
    AsignarDataPlanCuenta();
    CargarCombo();
    ListarCuenta();
    DeshabilitarCodigoCuentaPadre();
    DeshabilitarCuentaTransf();


    /*------------Controles de Registro de Cuenta Nueva-------------*/

    $('#txtPorcentajeDebe').attr('readonly', true);
    $('#txtPorcentajeHaber').attr('readonly', true);
    $('#btnSeleccionarTransfiere').attr("disabled", true);

    $("#flatTipoNinguno").click(function () {
        var checked_status = this.checked;
      
        if (checked_status == false) {            //$('#txt').show();
            $('#txtPorcentajeDebe').attr('readonly', false);
            $('#txtPorcentajeHaber').attr('readonly', false);
            $('#btnSeleccionarTransfiere').attr("disabled", false);
        }
        else {            //$('#txt').hide();
            $('#txtPorcentajeDebe').attr('readonly', true);
            $('#txtPorcentajeHaber').attr('readonly', true);
            $('#txtPorcentajeDebe').val("");
            $('#txtPorcentajeHaber').val("");
            $('#btnSeleccionarTransfiere').attr("disabled", true);
        }
         //alert(checked_status);
    })
    $("#flatTipoDebe").click(function () {
        var checked_status = this.checked;

        if (checked_status == false) {            //$('#txt').show();
            $('#txtPorcentajeDebe').attr('readonly', true);
            $('#txtPorcentajeHaber').attr('readonly', true);
            $('#btnSeleccionarTransfiere').attr("disabled", true);
        }
        else {            //$('#txt').hide();
            $('#txtPorcentajeDebe').attr('readonly', false);
            $('#txtPorcentajeHaber').attr('readonly', false);
            $('#btnSeleccionarTransfiere').attr("disabled", false);

        }
        //alert(checked_status);
    })
    $("#flatTipoHaber").click(function () {
        var checked_status = this.checked;
        if (checked_status == false) {            //$('#txt').show();
            $('#txtPorcentajeDebe').attr('readonly', true);
            $('#txtPorcentajeHaber').attr('readonly', true);
            $('#btnSeleccionarTransfiere').attr("disabled", true);
        }
        else {            //$('#txt').hide();
            $('#txtPorcentajeDebe').attr('readonly', false);
            $('#txtPorcentajeHaber').attr('readonly', false);
            $('#btnSeleccionarTransfiere').attr("disabled", false);
        }
        //alert(checked_status);
    })


   
    //ActivarCasillas();
    /*-----------------------------------------------------------*/
}

//Cargando datos Pre-Establecidos de la Cuenta
    function CargarDataPlanDeCuenta() {
        PlanCuentaID = $("#ContentPlaceHolder1_hdf_PlanCuentaID").val();
        NombrePlanCuenta = $("#ContentPlaceHolder1_hdf_PlanCuentaNombre").val();
        RazonSocial = $("#ContentPlaceHolder1_hdf_Emp_RazonSocial").val();
        RSRUC = $("#ContentPlaceHolder1_hdf_Emp_RUC").val();
        FechaCreacionPC = $("#ContentPlaceHolder1_hdf_FechaCreacionPC").val();
        HoraCreacionPC = $("#ContentPlaceHolder1_hdf_HoraCreacionPC").val();
    }
    function AsignarDataPlanCuenta() {

        document.getElementById('PlanCuentaNombre').innerHTML = NombrePlanCuenta;
        document.getElementById('LBRUC').innerHTML = RSRUC;
        document.getElementById('LBEmpresa').innerHTML = RazonSocial;
        document.getElementById('LBFechaCreacion').innerHTML = FechaCreacionPC;
        document.getElementById('LBHoraCreacion').innerHTML = HoraCreacionPC;

    }

//Filtros de Busqueda de Cuentas - SubCuentas
    function LimpiarDataBusqueda() {
        $("#ICueCodigo").val("");
        $("#ICueNombre").val("");
        $("#ddlElementoID").val(0);
    }

    //Cargando Combos
    function CargarCombo() {
            //Combo de Nueva Cuenta
        CargarComboElemento('#ddlElementoID', '../../Contabilidad/Maestros/Cuentas.aspx');
            //Combo de Cuenta Transferencia
        CargarComboElemento('#ddlElementoIDTransf', '../../Contabilidad/Maestros/Cuentas.aspx');
        //Combo de Nueva Cuenta
        CargarTTipoCambio('#ddlTTipoCambio', '../../Contabilidad/Maestros/Cuentas.aspx');
    }
    function CargarComboElemento(control, urlf) {
        urlf = urlf + '/ListarComboElemento';
        var ObjElemento = {};
        $.ajax({
            type: 'POST',
            url: urlf,
            data: "{ObjElemento:" + JSON.stringify(ObjElemento) + "}",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: false,
            success: function (msg) {
                $(control).empty();
                var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
                if (msg.d != null) {
                    $.each(msg.d, function (index, value) {
                        sItems += ("<option title='" + value.Elemento_Tx + "' value='" + value.Elemento_Id + "'>" + value.Elemento_Tx + "</option>");
                    });
                }
                $(control).html(sItems);
            },
            error: function () {
                alert("Error!...");
            }
        });
    }


    function CargarTTipoCambio(control, urlf) {
        urlf = urlf + '/ListarTTipoCambio';
        var ObjTTipoCambio = {};
        $.ajax({
            type: 'POST',
            url: urlf,
            data: "{ObjTTipoCambio:" + JSON.stringify(ObjTTipoCambio) + "}",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            async: false,
            success: function (msg) {
                $(control).empty();
                var sItems = "<option value='0' selected='selected'>SELECCIONE</option>";
                if (msg.d != null) {
                    $.each(msg.d, function (index, value) {
                        sItems += ("<option title='" + value.Ttc_Nombre_Tx + "' value='" + value.TTipoCambio_Id + "'>" + value.Ttc_Nombre_Tx + "</option>");
                    });
                }
                $(control).html(sItems);
            },
            error: function () {
                alert("Error!...");
            }
        });
    }
//Input con propiedades Establecidas
    function DeshabilitarCodigoCuentaPadre() {
        $("#txtCodCuentaPadreID").attr('readonly', true);

    }
    function DeshabilitarCuentaTransf() {
        $("#txtCueCodigoTransf").attr('readonly', true);
        $("#txtTransfiere").attr('readonly', true);
    }


//Listar Cuentas
    //Cuentas - Sub Cuentas para Padre de Nueva Cuenta
    function ListarCuenta() {
        var objCuenta = {
            PlanCuenta_Id: $("#ContentPlaceHolder1_hdf_PlanCuentaID").val(),
            Cue_Codigo: $("#ICueCodigo").val(),
            Cue_Nombre_tx: $("#ICueNombre").val(),
            Elemento_Id: $("#ddlElementoID").val(),
            
        };

        $("#CuentaGrid").html('<table id="gvCuentaGrid"></table><div id="pieCuentaGrid"></div>');
        $("#gvCuentaGrid").jqGrid({
            regional: 'es',
            url: '../../Contabilidad/Maestros/Cuentas.aspx/ListarCuenta',
            datatype: "json",
            mtype: "POST",
            postData: "{objCuenta:" + JSON.stringify(objCuenta) + "}",
            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,
            colNames: ['Cuenta_Id', 'Codigo', 'Nombre', 'Cue_Cc_FL', 'Centro Costo', 'TA', 'Transfiere', 'Cuenta Transfiere', 'Cue_TipoDestino_FL', 'Tipo Destino', 'DECIMALHABER', '% Debe', 'DECIMALHABER', '% Haber', 'Cue_Padre_Id', 'Elemento', 'Ele_Nombre_Tx', 'Tipo Cuenta', 'Categoria', 'Pla_Nombre_Tx', 'Elemento_Id', 'TipoCuenta_Id', 'CatalogoCuenta_Id', 'PlanCuenta_Id','','','','','',''],

            colModel: [
               //{ name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },

               { name: 'Cuenta_Id', index: 'Cuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'Cue_Codigo', index: 'Cue_Codigo', width: 35, align: 'center', align: 'left'},
               { name: 'Cue_Nombre_tx', index: 'Cue_Nombre_tx', width: 100, align: 'left' },

               { name: 'Cue_Cc_Fl', index: 'Cue_Cc_Fl', width: 40, align: 'center', hidden: true },
               { name: 'Cue_Cc_TX', index: 'Cue_Cc_TX', width: 40, align: 'center' },

               { name: 'Cue_Ta_Tx', index: 'Cue_Ta_Tx', width: 30, align: 'center', hidden: true },
               { name: 'Cue_Transfiere_Tx', index: 'Cue_Transfiere_Tx', width: 50, align: 'center', hidden: true  },
               { name: 'Cuenta_Transfiere_Nombre_Tx', index: 'Cuenta_Transfiere_Nombre_Tx', width: 100, align: 'left' },
               
               { name: 'Cue_TipoDestino_FL', index: 'Cue_TipoDestino_FL', width: 50, align: 'center', hidden: true },
               { name: 'Cue_TipoDestino_TX', index: 'Cue_TipoDestino_TX', width: 50, align: 'center' },

               { name: 'Cue_PorcenDebe_nu', index: 'Cue_PorcenDebe_nu', width: 30, align: 'center', hidden: true },
               { name: 'Cue_PorcenDebe_nu_entero', index: 'Cue_PorcenDebe_nu_entero', width: 30, align: 'center' },

               { name: 'Cue_PorcenHaber_nu', index: 'Cue_PorcenHaber_nu', width: 30, align: 'center', hidden: true },
               { name: 'Cue_PorcenHaber_nu_entero', index: 'Cue_PorcenHaber_nu_entero', width: 30, align: 'center' },


               { name: 'Cue_Padre_Id', index: 'Cue_Padre_Id', width: 40, align: 'center', hidden: true },
               //
               { name: 'Ele_Co', index: 'Ele_Co', width: 30, align: 'center' },
               { name: 'Ele_Nombre_Tx', index: 'Ele_Nombre_Tx', width: 100, align: 'center', hidden: true },
               { name: 'TCu_Nombre_Tx', index: 'TCu_Nombre_Tx', width: 50, align: 'center' },
               { name: 'Cat_Nombre_Tx', index: 'Cat_Nombre_Tx', width: 150, align: 'center' },
               { name: 'Pla_Nombre_Tx', index: 'Pla_Nombre_Tx', width: 100, align: 'center', hidden: true },

               { name: 'Elemento_Id', index: 'Elemento_Id', width: 100, align: 'center', hidden: true },
               { name: 'TipoCuenta_Id', index: 'TipoCuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'CatalogoCuenta_Id', index: 'CatalogoCuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'PlanCuenta_Id', index: 'PlanCuenta_Id', width: 100, align: 'center', hidden: true },
          //NUEVOS CAMPOS
                { name: 'Cue_CancTranf_FL', index: 'Cue_CancTranf_FL', width: 100, align: 'center', hidden: true },
                { name: 'Cue_CuentaOficial_FL', index: 'Cue_CuentaOficial_FL', width: 100, align: 'center', hidden: true },
                { name: 'Cue_NumeroCuenta', index: 'Cue_NumeroCuenta', width: 100, align: 'center', hidden: true },
           
                { name: 'Banco_Id', index: 'Banco_Id', width: 100, align: 'center', hidden: true },
                { name: 'Moneda_Id', index: 'Moneda_Id', width: 100, align: 'center', hidden: true },
                { name: 'TTipoCambio_Id', index: 'TTipoCambio_Id', width: 100, align: 'center', hidden: true }

            ],
            pager: "#pieCuentaGrid",
            viewrecords: true,
            width: 1300,
            height: 'auto', //height: 275,
            //height: 230,
            rowNum: 10,
            rowList: [10, 20, 30, 100],
            gridview: true,

            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            emptyrecords: "No hay Registros.",
            caption: 'Cuentas',
            loadComplete: function (result) {
            },
            beforeSelectRow: function (rowid, e) {
                
                var CodCuenta=$("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo;

                var ObjCuenta = {
                    //CUE_CODIGO: CodCuenta
                    Cue_Codigo: CodCuenta
                }

                $.ajax({
                    async: true,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../Contabilidad/Maestros/Cuentas.aspx/VerificarCodigoCuenta',
                    dataType: "json",
                    data: "{ObjCuenta:" + JSON.stringify(ObjCuenta) + "}",
                    success: function (data) {
                        //alert('mensaje : ' + data.d); //ALMACENAR EN UNA VARIABLE Y LUEGO HACER EL IF PARA EL BOTON 
                        $('#hdfCantidadNumeroCuenta').val(data.d); // HACER EL IF PARA EL BOTON

                        // alert('cantidadregistros' + $('#hdfCantidadNumeroCuenta').val());


                        var CantidadRegistrosNumero = $('#hdfCantidadNumeroCuenta').val();
                        if (CantidadRegistrosNumero == 1) {
                           // alert('CANTIDAD Uno: ' + CantidadRegistrosNumero);
                            $("#btnCrearTransferenciaDestino").attr('disabled', false);

                            //$("#btnCrearTransferenciaDestino").mouseenter(function (evento) {
                            ////    alert('mensaje pasa por encima');
                            //});
                   
                        } else
                            if (CantidadRegistrosNumero == 2) {
                             //   alert('CANTIDAD DOS: ' + CantidadRegistrosNumero);
                                $("#btnCrearTransferenciaDestino").attr('disabled', true);
                              //  $("#btnCrearTransferenciaDestino").mouseenter(function (evento) {
                              ////      alert('mensaje pasa por 2');
                              //  });
                            }
                            else {
                              //  alert('CANTIDAD OTRA' + CantidadRegistrosNumero);
                                $("#btnCrearTransferenciaDestino").attr('disabled', true);
                            }

                    },
                    error: function (xhr, status, error) {
                        alert('error: function (xhr, status, error) {');
                      
                        alert(xhr.responseText);
                    }

                })


                



                $("#txtCodCuentaPadreID").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo);
                //Cargar Datos de Nueva Cuenta 
                    //Datos Mostrados en el Label
                $("#txtCuentaCodigoPadre").text($("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo); //Label en pantalla Nueva Subcuenta
                $("#txtNombreCuentaPadre").text($("#gvCuentaGrid").getLocalRow(rowid).Cue_Nombre_tx); //Label en pantalla Nueva Subcuenta
                $("#txtElementoCodigoPadre").text($("#gvCuentaGrid").getLocalRow(rowid).Ele_Co); //Label en pantalla Nueva Subcuenta
                $("#txtElementoNombrePadre").text($("#gvCuentaGrid").getLocalRow(rowid).Ele_Nombre_Tx); //Label en pantalla Nueva Subcuenta
                    //Datos en el Hidden
                $("#hdfPlanCuenta_Id").val($("#gvCuentaGrid").getLocalRow(rowid).PlanCuenta_Id); // Hidden en pantalla nueva sub-cuenta
                $("#hdfIDCuentaPadre").val($("#gvCuentaGrid").getLocalRow(rowid).Cuenta_Id);// Hidden en pantalla nueva sub-cuenta
                $("#hdfIDElementoPadre").val($("#gvCuentaGrid").getLocalRow(rowid).Elemento_Id);// Hidden en pantalla nueva sub-cuenta

                //Cargar Datos de Nueva Cuenta Destino
                $("#txtCuentaCodigoCuenta").text($("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo);
                $("#hdfCodigoCuenta").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo);
                
                $("#txtNombreCuenta").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_Nombre_tx);
                $("#txtTA").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_Ta_Tx);
               

                //Llenar porcentaje DEBE  a HABER
                $("#txtPorcentajeDebe").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_PorcenHaber_nu_entero);
                  //alert('ROW'+($("#gvCuentaGrid").getLocalRow(rowid).Cue_PorcenHaber_nu_entero));
                  //alert('INPUT' + $("#txtPorcentajeDebe").val());
                //Llenar porcentaje HABER a DEBE
                $("#txtPorcentajeHaber").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_PorcenDebe_nu_entero);

                //alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_Codigo);
                //alert('txtx' + $("#txtCuentaCodigoCuenta").txt());

                //$("#txtTA").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                //alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                
                if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl === true) 
                {
                    // alert('1 :' + ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl));
                    $("#flatAsociado").prop('checked', true);
                    $("#flatSINAsociar").prop('checked', false);
                } else if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl === false) {
                    //alert('0 :' + $("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                    $("#flatAsociado").prop('checked', false);
                    $("#flatSINAsociar").prop('checked', true);
                }
                else {
                    alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                }
                /**/
                if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl === true) {
                    // alert('1 :' + ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl));
                    $("#flatAsociado").prop('checked', true);
                    $("#flatSINAsociar").prop('checked', false);
                } else if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl === false) {
                    //alert('0 :' + $("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                    $("#flatAsociado").prop('checked', false);
                    $("#flatSINAsociar").prop('checked', true);
                }
                else {
                    alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                }
                //////
                
                $("#txtNumCuenta").val($("#gvCuentaGrid").getLocalRow(rowid).Cue_NumeroCuenta);
                $("#ddlEntidadFinancieraID").val($("#gvCuentaGrid").getLocalRow(rowid).Banco_Id);
                $("#ddlTipoMonedaCuenta").val($("#gvCuentaGrid").getLocalRow(rowid).Moneda_Id);
                $("#ddlTTipoCambio").val($("#gvCuentaGrid").getLocalRow(rowid).TTipoCambio_Id);

                ///////////////////////////////////////////////////////////////////////////////
               // alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_CancTranf_FL);
                if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_CancTranf_FL === true) {
                    $("#flatCancelarTransfSI").prop('checked', true);
                    $("#flatCancelarTransfNO").prop('checked', false);
                } else if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_CancTranf_FL === false) {
                    $("#flatCancelarTransfSI").prop('checked', false);
                    $("#flatCancelarTransfNO").prop('checked', true);
                }
                else {
                  //  alert($("#gvCuentaGrid").getLocalRow(rowid).Cue_CancTranf_FL);
                    $("#flatCancelarTransfSI").prop('checked', false);
                    $("#flatCancelarTransfNO").prop('checked', false);
                }
                ///////////////////////////////////////////////////////////////////////////////
                
                if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_CuentaOficial_FL === true) {
                    $("#flatCuentaOficialSI").prop('checked', true);
                    $("#flatCuentaOficialNO").prop('checked', false);
                } else if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_CuentaOficial_FL === false) {
                    $("#flatCuentaOficialSI").prop('checked', false);
                    $("#flatCuentaOficialNO").prop('checked', true);
                }
                else {
                   
                    $("#flatCuentaOficialSI").prop('checked', false);
                    $("#flatCuentaOficialNO").prop('checked', false);
                }
                ///////////////////////////////////////////////////////////////////////////////

                if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_TipoDestino_FL === true) {
                    //Si es que el anterior registro es Tipo Debe entonces el nuevo destino será haber
                    $("#flatTipoHaber").prop('checked', true);
                    $("#flatTipoDebe").prop('checked', false);
                    $("#flatTipoNinguno").prop('checked', false);
                    //Desabilitamos las opciones
                    $("#flatTipoDebe").attr('disabled', true);
                    $("#flatTipoNinguno").attr('disabled', true);
                    $("#btnCrearTransferenciaDestino").attr('disabled', false);
                }
                else if ($("#gvCuentaGrid").getLocalRow(rowid).Cue_TipoDestino_FL === false) {
                    //Si es que el anterior registro es Tipo Haber entonces el nuevo destino será Haber
                    $("#flatTipoDebe").prop('checked', true);
                    $("#flatTipoHaber").prop('checked', false);
                    $("#flatTipoNinguno").prop('checked', false);
                    //Desabilitamos las opciones
                    $("#flatTipoHaber").attr('disabled', true);
                    $("#flatTipoNinguno").attr('disabled', true);
                    $("#btnCrearTransferenciaDestino").attr('disabled', false);
                }
                else {

                    $("#btnCrearTransferenciaDestino").attr('disabled', true);
                    
                }

                //SE TIENE QUE HACER CONSULTA A LA BASE DE DATOS CONSULTANDO SI LA CUENTA YA TIENE HABER Y DEBE PARA DE ESA MANERA DESAHABILITAR EL BOTON
                //SE TIENE QUE EMITIR MENSAJE ADVIRTIENDO QUE NO SE PUEDE CAMBIAR DE DESTINO CUANDO HAGO CLICK O PASO EL MOUSE POR EL BOTON DESTINO DEBE HABER NULL

                //if ($("#flatTipoDebe").is(':checked')) {
                //    alert("La cuenta anterior esta grabada con DEBE");
                //} else if ($("#flatTipoNinguno").is(':checked')) {
                //    alert("No se permite esta opcion");
                //}

                
            },
            loadError: function (xhr) {
                alert("The Status code: " + xhr.status + "   Message :" + xhr.statusText + "  A :" + xhr.result);
                console.log(xhr.responseText);

            }
        });
        //$.extend($.fn.fmatter, {
        //    actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
        //        //  fa fa-plus-square
        //        return "<i title=\"Seleccionar Cuenta Padre\" onclick=\"SeleccionarPadre(this)\" style=\"cursor:pointer\" class='fa fa-check-square-o'></i>"
        //    }
        //});
    }
    //Cuentas - Sub Cuentas para Transferencia de Nueva Cuenta
    function ListarCuentaSeleccion() {
        var objCuenta = {
            PlanCuenta_Id: $("#ContentPlaceHolder1_hdf_PlanCuentaID").val(),
            Cue_Codigo: $("#ICueCodigoTransf").val(),
            Cue_Nombre_tx: $("#ICueNombreTransf").val(),
            Elemento_Id: $("#ddlElementoIDTransf").val(),
            //Cue_Padre_Id: $("#ICuePadre").val()
        };

        $("#SeleccionCuentaGrid").html('<table id="gvSeleccionCuentaGrid"></table><div id="pieSeleccionCuentaGrid"></div>');
        $("#gvSeleccionCuentaGrid").jqGrid({
            regional: 'es',
            url: '../../Contabilidad/Maestros/Cuentas.aspx/ListarCuenta',
            datatype: "json",
            mtype: "POST",
            postData: "{objCuenta:" + JSON.stringify(objCuenta) + "}",
            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,
            // colNames: ['', 'Cuenta_Id', 'Codigo', 'Nombre', 'Centro Costo', 'TA', 'Transfiere', 'Cuenta Transfiere', 'Tipo Destino', '% Debe', '% Haber', 'Cue_Padre_Id', 'Elemento', 'Ele_Nombre_Tx', 'Tipo Cuenta', 'Categoria', 'Pla_Nombre_Tx', 'Elemento_Id', 'TipoCuenta_Id', 'CatalogoCuenta_Id', 'PlanCuenta_Id'],
           // colNames: ['Cuenta_Id', 'Codigo', 'Nombre', 'Cue_Cc_FL', 'Centro Costo', 'TA', 'Transfiere', 'Cuenta Transfiere', 'Cue_TipoDestino_FL', 'Tipo Destino', '% Debe', 'DEBE', '% Haber', 'HABER', 'Cue_Padre_Id', 'Elemento', 'Ele_Nombre_Tx', 'Tipo Cuenta', 'Categoria', 'Pla_Nombre_Tx', 'Elemento_Id', 'TipoCuenta_Id', 'CatalogoCuenta_Id', 'PlanCuenta_Id'],


            //colNames: ['Cuenta_Id', 'Codigo', 'Nombre', 'Cue_Cc_FL', 'Centro Costo', 'TA', 'Transfiere', 'Cuenta Transfiere', 'Cue_TipoDestino_FL', 'Tipo Destino', '% Debe', '% Haber', 'Cue_Padre_Id', 'Elemento', 'Ele_Nombre_Tx', 'Tipo Cuenta', 'Categoria', 'Pla_Nombre_Tx', 'Elemento_Id', 'TipoCuenta_Id', 'CatalogoCuenta_Id', 'PlanCuenta_Id'],

            colNames: ['Cuenta_Id', 'Codigo', 'Nombre', 'Cue_Cc_FL', 'Centro Costo', 'TA', 'Transfiere', 'Cuenta Transfiere', 'Cue_TipoDestino_FL', 'Tipo Destino', 'DECIMALHABER', '% Debe', 'DECIMALHABER', '% Haber', 'Cue_Padre_Id', 'Elemento', 'Ele_Nombre_Tx', 'Tipo Cuenta', 'Categoria', 'Pla_Nombre_Tx', 'Elemento_Id', 'TipoCuenta_Id', 'CatalogoCuenta_Id', 'PlanCuenta_Id'],


            colModel: [
            //   { name: 'Seleccionar', index: 'Seleccionar', width: 30, align: 'center', formatter: 'actionFormatterSeleccionar', search: false },
               { name: 'Cuenta_Id', index: 'Cuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'Cue_Codigo', index: 'Cue_Codigo', width: 35, align: 'center', align: 'left' },
               { name: 'Cue_Nombre_tx', index: 'Cue_Nombre_tx', width: 100, align: 'left' },
               //{ name: 'Cue_Cc_Fl', index: 'Cue_Cc_Fl', width: 40, align: 'center' },
               { name: 'Cue_Cc_Fl', index: 'Cue_Cc_Fl', width: 40, align: 'center', hidden: true },
               { name: 'Cue_Cc_TX', index: 'Cue_Cc_TX', width: 40, align: 'center' },

               { name: 'Cue_Ta_Tx', index: 'Cue_Ta_Tx', width: 30, align: 'center', hidden: true },
               { name: 'Cue_Transfiere_Tx', index: 'Cue_Transfiere_Tx', width: 50, align: 'center', hidden: true },
               { name: 'Cuenta_Transfiere_Nombre_Tx', index: 'Cuenta_Transfiere_Nombre_Tx', width: 100, align: 'left' },

               //{ name: 'Cue_TipoDestino_FL', index: 'Cue_TipoDestino_FL', width: 50, align: 'center' },
               { name: 'Cue_TipoDestino_FL', index: 'Cue_TipoDestino_FL', width: 50, align: 'center', hidden: true },
               { name: 'Cue_TipoDestino_TX', index: 'Cue_TipoDestino_TX', width: 50, align: 'center' },

               { name: 'Cue_PorcenDebe_nu', index: 'Cue_PorcenDebe_nu', width: 30, align: 'center', hidden: true },
               { name: 'Cue_PorcenDebe_nu_entero', index: 'Cue_PorcenDebe_nu_entero', width: 30, align: 'center' },
               { name: 'Cue_PorcenHaber_nu', index: 'Cue_PorcenHaber_nu', width: 30, align: 'center', hidden: true },
               { name: 'Cue_PorcenHaber_nu_entero', index: 'Cue_PorcenHaber_nu_entero', width: 30, align: 'center' },
               //{ name: 'Cue_PorcenDebe_nu', index: 'Cue_PorcenDebe_nu', width: 30, align: 'center' },
               //{ name: 'Cue_PorcenDebe_nu_entero', index: 'Cue_PorcenDebe_nu_entero', width: 30, align: 'center' },
               //{ name: 'Cue_PorcenHaber_nu', index: 'Cue_PorcenHaber_nu', width: 30, align: 'center' },
               //{ name: 'Cue_PorcenHaber_nu_entero', index: 'Cue_PorcenHaber_nu_entero', width: 30, align: 'center' },

               { name: 'Cue_Padre_Id', index: 'Cue_Padre_Id', width: 40, align: 'center', hidden: true },
               //
               { name: 'Ele_Co', index: 'Ele_Co', width: 30, align: 'center', hidden: true }, ///ELEMENTO CODIGO - TAMANO DE GRID CAMBIA 1300
               { name: 'Ele_Nombre_Tx', index: 'Ele_Nombre_Tx', width: 100, align: 'center', hidden: true },
               { name: 'TCu_Nombre_Tx', index: 'TCu_Nombre_Tx', width: 50, align: 'center', hidden: true },/// TIPO CUENTA NOMBRE
               { name: 'Cat_Nombre_Tx', index: 'Cat_Nombre_Tx', width: 150, align: 'center', hidden: true },///CATEGORIA NOMBRE
               { name: 'Pla_Nombre_Tx', index: 'Pla_Nombre_Tx', width: 100, align: 'center', hidden: true },

               { name: 'Elemento_Id', index: 'Elemento_Id', width: 100, align: 'center', hidden: true },
               { name: 'TipoCuenta_Id', index: 'TipoCuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'CatalogoCuenta_Id', index: 'CatalogoCuenta_Id', width: 100, align: 'center', hidden: true },
               { name: 'PlanCuenta_Id', index: 'PlanCuenta_Id', width: 100, align: 'center', hidden: true },

            ],
            pager: "#pieSeleccionCuentaGrid",
            viewrecords: true,
            width: 1020, ///1300
            height:'auto',// 230,//275
            //height: 230,
            rowNum: 10,
            rowList: [10, 20, 30, 100],
            gridview: true,

            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                records: function (obj) { return obj.d.length; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            emptyrecords: "No hay Registros.",
            caption: 'Cuentas',
            loadComplete: function (result) {
            },
            beforeSelectRow: function (rowid, e) {
                $("#txtCueCodigoTransf").val($("#gvSeleccionCuentaGrid").getLocalRow(rowid).Cue_Codigo);
                $("#txtTransfiere").val($("#gvSeleccionCuentaGrid").getLocalRow(rowid).Cue_Codigo); //Llenar Datos
              
            },

            loadError: function (xhr) {
                alert("The Status code: " + xhr.status + "   Message :" + xhr.statusText + "  A :" + xhr.result);
                console.log(xhr.responseText);

            }
        });
        //$.extend($.fn.fmatter, {
        //    actionFormatterSeleccionar: function (cellvalue, options, rowObject) {
        //        //  fa fa-plus-square
        //        return "<i title=\"Seleccionar Cuenta Transfiere\" onclick=\"SeleccionCuentaTransfiere(this)\" style=\"cursor:pointer\" class='fa fa-check-square-o'></i>"
        //    }
        //});
    }

//Crear Cuenta Nueva
    //Cuenta - Nueva SubCuenta
    function NuevaCuenta() {
        

        LimpiarDatosNuevaCuentaIngreso();

        $("#DVCrearCuenta").dialog({
            width: 870, // 800
            height: 460, //330
            title: "Nueva Cuenta",
            modal: true
            , resizable: false,
            dialogClass: 'hide-close',
            draggable: false,
            closeOnEscape: false
        });
        return false;

    }
    function CrearCuentaNueva() {
        
        var obj = {};
        obj.Cue_Nombre_tx = $("#txtNombreCuenta").val();
        //CENTRO-COSTO
        if ($("#flatAsociado").is(':checked')) {
            obj.Cue_Cc_Fl = true;
        }
        else
            if ($("#flatAsociado").is(':checked'))
            { obj.Cue_Cc_Fl = false; }

            else { obj.Cue_Cc_Fl = false; }

        obj.Cue_Ta_Tx = $("#txtTA").val();

        //TIPO-DESTINO
        if ($("#flatTipoDebe").is(':checked')) {
            obj.Cue_TipoDestino_FL = true;
            obj.Cue_Transfiere_Tx = $("#txtTransfiere").val();
            //Convertir a Decimal 
            EnteroDebe = $("#txtPorcentajeDebe").val();
            EnteroHaber = $("#txtPorcentajeHaber").val();
            PorcentajeDebe = EnteroDebe / 100;
            PorcentajeHaber = EnteroHaber / 100;
            //Almacenar Porcentajes en obj
            obj.Cue_PorcenDebe_nu = PorcentajeDebe //Decimal
            obj.Cue_PorcenHaber_nu = PorcentajeHaber //Decimal

        }
        else
            if ($("#flatTipoHaber").is(':checked')) {
                obj.Cue_TipoDestino_FL = false;
                obj.Cue_Transfiere_Tx = $("#txtTransfiere").val();
                //Convertir a Decimal 
                EnteroDebe = $("#txtPorcentajeDebe").val();
                EnteroHaber = $("#txtPorcentajeHaber").val();
                PorcentajeDebe = EnteroDebe / 100;
                PorcentajeHaber = EnteroHaber / 100;
                //Almacenar Porcentajes en obj
                obj.Cue_PorcenDebe_nu = PorcentajeDebe //Decimal
                obj.Cue_PorcenHaber_nu = PorcentajeHaber //Decimal
            }
            else
                if ($("#flatTipoNinguno").is(':checked')) {
                    obj.Cue_TipoDestino_FL = null;
                }
                else {
                    obj.Cue_TipoDestino_FL = null;
                }

        //obj.Cue_Transfiere_Tx = $("#txtTransfiere").val();
        ////Convertir a Decimal 
        //EnteroDebe = $("#txtPorcentajeDebe").val();
        //EnteroHaber = $("#txtPorcentajeHaber").val();
        //PorcentajeDebe = EnteroDebe / 100;
        //PorcentajeHaber = EnteroHaber / 100;
        ////Almacenar Porcentajes en obj
        //obj.Cue_PorcenDebe_nu = PorcentajeDebe //Decimal
        //obj.Cue_PorcenHaber_nu = PorcentajeHaber //Decimal

        //LECTURA DE LOS PARAMETROS ADICIONALES 17 AGOSTO
        if ($("#flatCancelarTransfSI").is(':checked')) {
            obj.Cue_CancTranf_FL = true;
        }
        else if ($("#flatCancelarTransfNO").is(':checked')) {
            obj.Cue_CancTranf_FL = false;
        } else {
            obj.Cue_CancTranf_FL = false;
        }

        if ($("#flatCuentaOficialSI").is(':checked')) {
            obj.Cue_CuentaOficial_FL = true;
        }
        else if ($("#flatCuentaOficialNO").is(':checked')) {
            obj.Cue_CuentaOficial_FL = false;
        } else {
            obj.Cue_CuentaOficial_FL = false;
        }

        obj.Cue_NumeroCuenta = $("#txtNumCuenta").val();

        if ($("#ddlEntidadFinancieraID").val() != 0) {
            obj.Banco_Id = $("#ddlEntidadFinancieraID").val();
        }
        if ($("#ddlTipoMonedaCuenta").val() != 0) {
            obj.Moneda_Id = $("#ddlTipoMonedaCuenta").val();
        }
        if ($("#ddlTTipoCambio").val() != 0) {
        obj.TTipoCambio_Id = $("#ddlTTipoCambio").val();
        }

        ///DATOS PREDETERMINADOS

        obj.Cue_Padre_Id = $("#hdfIDCuentaPadre").val();
        obj.Elemento_Id = $("#hdfIDElementoPadre").val();
        obj.PlanCuenta_Id = $("#hdfPlanCuenta_Id").val();
        obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();
        //private int _Cuenta_Id;
        //private String _Cue_Codigo;

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Contabilidad/Maestros/Cuentas.aspx/CrearCuenta',
            dataType: "json",
            data: "{ObjCuenta:" + JSON.stringify(obj) + "}",
            success: function (data) {
              // Mensaje("Mensaje", "Se registro Correctamente la Cuenta " + data.d.Cue_Codigo);

                $("#CodigoCuentaCreado").text(data.d.Cue_Codigo);
                //alert(data.d.Cue_TipoDestino_FL)
                ListarCuenta();
                //ListarCuentaSeleccion();
                //ListarCuenta();
                
                ListarCuentaSeleccion();
                //alert(data.d.Cue_Codigo);
                CerrarFormularioNuevaCuenta();

                //Almacenar en variables los datos predefinidos para la cuenta destino RECUPERAR LOS DATOS VER BLOC DE NOTAS
                DCue_Codigo = data.d.Cue_Codigo;
                DCue_Nombre_tx = data.d.Cue_Nombre_tx;
                DCue_Cc_Fl = data.d.Cue_Cc_Fl;
                DCue_Ta_Tx = data.d.Cue_Ta_Tx;
                DCue_Transfiere_Tx = data.d.Cue_Transfiere_Tx;
                DCue_TipoDestino_FL = data.d.Cue_TipoDestino_FL;
                DCue_PorcenDebe_nu = data.d.Cue_PorcenDebe_nu;
                DCue_PorcenHaber_nu = data.d.Cue_PorcenHaber_nu;
                DCue_Padre_Id = data.d.Cue_Padre_Id;
                DElemento_Id = data.d.Elemento_Id;
                DPlanCuenta_Id = data.d.PlanCuenta_Id;
                //--------------------- DATOS ADICIONALES AGOSTO-------
                DCue_CancTranf_FL = data.d.Cue_CancTranf_FL;
                DCue_CuentaOficial_FL = data.d.Cue_CuentaOficial_FL;
                DCue_NumeroCuenta = data.d.Cue_NumeroCuenta;
                DBanco_Id = data.d.Banco_Id;
                DMoneda_Id = data.d.Moneda_Id;
                DTTipoCambio_Id = data.d.TTipoCambio_Id;
                ///AUDITORIA
                DAud_UsuarioCreacion_Id = data.d.Aud_UsuarioCreacion_Id;
                DAud_Creacion_Fe = data.d.Aud_Creacion_Fe;
                DAud_Estado_Fl = data.d.Aud_Estado_Fl;
                //--------------------------------
                //CARGA DE DATOS 
                $("#txtCuentaCodigoCuenta").text(DCue_Codigo);
                $("#hdfCodigoCuenta").val(DCue_Codigo);
                $("#txtNombreCuenta").val(DCue_Nombre_tx);
                $("#txtTA").val(DCue_Ta_Tx);

                //Llenar porcentaje DEBE  a HABER
                $("#txtPorcentajeDebe").val(DCue_PorcenHaber_nu*100);
                //Llenar porcentaje HABER a DEBE
                $("#txtPorcentajeHaber").val(DCue_PorcenDebe_nu*100);

                ///----
                if (DCue_Cc_Fl === true) {
                    // alert('1 :' + ($("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl));
                    $("#flatAsociado").prop('checked', true);
                    $("#flatSINAsociar").prop('checked', false);
                } else if (DCue_Cc_Fl === false) {
                    //alert('0 :' + $("#gvCuentaGrid").getLocalRow(rowid).Cue_Cc_Fl);
                    $("#flatAsociado").prop('checked', false);
                    $("#flatSINAsociar").prop('checked', true);
                }
                else {
                    alert(DCue_Cc_Fl);
                }
                //////

                $("#txtNumCuenta").val(DCue_NumeroCuenta);
                $("#ddlEntidadFinancieraID").val(DBanco_Id);
                $("#ddlTipoMonedaCuenta").val(DMoneda_Id);
                $("#ddlTTipoCambio").val(DTTipoCambio_Id);
                //

                if (DCue_CuentaOficial_FL === true) {
                    $("#flatCuentaOficialSI").prop('checked', true);
                    $("#flatCuentaOficialNO").prop('checked', false);
                } else if (DCue_CuentaOficial_FL === false) {
                    $("#flatCuentaOficialSI").prop('checked', false);
                    $("#flatCuentaOficialNO").prop('checked', true);
                }
                else {
                    $("#flatCuentaOficialSI").prop('checked', false);
                    $("#flatCuentaOficialNO").prop('checked', false);
                }

                ///
                if (DCue_CancTranf_FL === true) {
                    $("#flatCancelarTransfSI").prop('checked', true);
                    $("#flatCancelarTransfNO").prop('checked', false);
                } else if (DCue_CancTranf_FL === false) {
                    $("#flatCancelarTransfSI").prop('checked', false);
                    $("#flatCancelarTransfNO").prop('checked', true);
                }
                else {
                    $("#flatCancelarTransfSI").prop('checked', false);
                    $("#flatCancelarTransfNO").prop('checked', false);
                }

                ///
                $("#hdfIDElementoPadre").val(DElemento_Id);
                $("#hdfPlanCuenta_Id").val(DPlanCuenta_Id);
                $("#hdfIDCuentaPadre").val(DCue_Padre_Id);


                //Condicionales para mostrar mensaje de acuerdo a ello se mostrara opcion para crear cuenta destino
                if (data.d.Cue_TipoDestino_FL == true) {
                   
                    // Mensaje("Mensaje", "MENSAJE TRUE" + data.d.Cue_Codigo);
                    MensajedeRegistro();

                    //Si es que el anterior registro es Tipo Debe entonces el nuevo destino será haber
                    $("#flatTipoHaber").prop('checked', true);
                    $("#flatTipoDebe").prop('checked', false);
                    $("#flatTipoNinguno").prop('checked', false);
                    //Desabilitamos las opciones
                    $("#flatTipoDebe").attr('disabled', true);
                    $("#flatTipoNinguno").attr('disabled', true);
                    $("#btnCrearTransferenciaDestino").attr('disabled', false);
                }
                else if (data.d.Cue_TipoDestino_FL == false) {
                   
                    //Mensaje("Mensaje", "MENSAJE FALSE" + data.d.Cue_Codigo);
                    MensajedeRegistro();
                    //Si es que el anterior registro es Tipo Haber entonces el nuevo destino será Haber
                    $("#flatTipoDebe").prop('checked', true);
                    $("#flatTipoHaber").prop('checked', false);
                    $("#flatTipoNinguno").prop('checked', false);
                    //Desabilitamos las opciones
                    $("#flatTipoHaber").attr('disabled', true);
                    $("#flatTipoNinguno").attr('disabled', true);
                    $("#btnCrearTransferenciaDestino").attr('disabled', false);
                }
                else {
                    Mensaje("Mensaje", "Se registro Correctamente la Cuenta : " + data.d.Cue_Codigo);
                    //MensajedeRegistro();
                    $("#btnCrearTransferenciaDestino").attr('disabled', true);
                     }
                
            },
            error: function (xhr, status, error) {
                alert('error: function (xhr, status, error) {');
                Mensaje("Mensaje", "Ocurrio un error en la creacion de la cuenta");
                alert(xhr.responseText);
            }
        })
    }

    function CerrarFormularioNuevaCuenta() {
        
        $("#DVCrearCuenta").dialog('close');
        LimpiarDatosNuevaCuentaPorDefecto();
        //LimpiarDatosNuevaCuentaPorDefecto();
        // Mensaje("Aviso", "No existe Numero de Cuenta en seleccion");
    }


    // Cuenta Transferencia
    function SeleccionarCuentaTranfiere() {
        $("#DVSeleccionarCuentaTrasf").dialog({
            width: 1100,//1320
            height: 535, //330 //580
            title: "Seleccion de Cuenta - Transfiere",
            modal: true
           , resizable: false,
            dialogClass: 'hide-close',
            draggable: false,
            closeOnEscape: false
        });
        return false;

    }
    function CerrarFormularioSeleccionCuentaTrans() {
        $("#DVSeleccionarCuentaTrasf").dialog('close');
    }

//Funcion para Seleccionar Cuentas ----> Actualmente no se usan//
//-----------------------------------------------------------------//
    function SeleccionarPadre(obj) {
        var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
        var rowData = $("#gvCuentaGrid").getRowData(RowId);
        //alert('dato' + rowData.Cue_Codigo);
        //Almacenar en input
        $("#txtCodCuentaPadreID").val(rowData.Cue_Codigo); //Input en la pantalla principal

        $("#txtCuentaCodigoPadre").text(rowData.Cue_Codigo); //Label en pantalla Nueva Subcuenta
        $("#txtNombreCuentaPadre").text(rowData.Cue_Nombre_tx); //Label en pantalla Nueva Subcuenta
        $("#txtElementoCodigoPadre").text(rowData.Ele_Co); //Label en pantalla Nueva Subcuenta
        $("#txtElementoNombrePadre").text(rowData.Ele_Nombre_Tx); //Label en pantalla Nueva Subcuenta
    


        $("#hdfPlanCuenta_Id").val(rowData.PlanCuenta_Id); // Hidden en pantalla nueva sub-cuenta
      //  $("#hdfIDCuentaPadre").val(rowData.Cue_Padre_Id);// Hidden en pantalla nueva sub-cuenta
        $("#hdfIDCuentaPadre").val(rowData.Cuenta_Id);// Hidden en pantalla nueva sub-cuenta
        $("#hdfIDElementoPadre").val(rowData.Elemento_Id);// Hidden en pantalla nueva sub-cuenta

      
        //if (rowData.Cue_TipoDestino_FL === 'true') {
        //    alert('1 :' + rowData.Cue_TipoDestino_FL);
        //} else if (rowData.Cue_TipoDestino_FL === 'false') {
        //    alert('0 :' + rowData.Cue_TipoDestino_FL);
        //}

        //else {
        //    alert('NULL PADRE ' + rowData.Cue_TipoDestino_FL);
        //}
     


    }
    function SeleccionCuentaTransfiere(obj) {
        var RowId = parseInt($(obj).parent().parent().attr("id"), 10);
        var rowData = $("#gvSeleccionCuentaGrid").getRowData(RowId);
        $("#txtCueCodigoTransf").val(rowData.Cue_Codigo);
        $("#txtTransfiere").val(rowData.Cue_Codigo);
    
    }
//-------------------------------------------------------------------//


    //Limpiar Data
    function LimpiarDatosNuevaCuentaPorDefecto() {
      //  $("#txtCodCuentaPadreID").val("");

        $("#txtCuentaCodigoPadre").val("");
        $("#txtNombreCuentaPadre").val("");
        $("#txtElementoCodigoPadre").val("");

        $("#hdfPlanCuenta_Id").val("");
        $("#hdfIDCuentaPadre").val("");
        $("#hdfIDElementoPadre").val("");
    }
    function LimpiarDatosNuevaCuentaIngreso() {
        $("#txtNombreCuenta").val("");
        $("#flatAsociado").prop('checked', false);
        $("#flatSINAsociar").prop('checked', false);
        $("#txtTA").val("");
        $("#txtTransfiere").val("");
        $("#txtPorcentajeDebe").val("");
        $("#txtPorcentajeHaber").val("");

        $("#flatTipoDebe").prop('checked', false);
        $("#flatTipoHaber").prop('checked', false);
        $("#flatTipoNinguno").prop('checked', false);

        $("#flatTipoDebe").attr('disabled', false);
        $("#flatTipoHaber").attr('disabled', false);
        $("#flatTipoNinguno").attr('disabled', false);

        $('#txtPorcentajeDebe').attr('readonly', true);
        $('#txtPorcentajeHaber').attr('readonly', true);
        $('#txtPorcentajeDebe').val("");
        $('#txtPorcentajeHaber').val("");
        $('#btnSeleccionarTransfiere').attr("disabled", true);
        /*---Nuevos Datos ----*/

        $("#flatCancelarTransfSI").attr('disabled', false);
        $("#flatCancelarTransfNO").attr('disabled', false);
        $("#flatCancelarTransfSI").prop('checked', false);
        $("#flatCancelarTransfNO").prop('checked', false);
        $("#flatCuentaOficialSI").attr('disabled', false);
        $("#flatCuentaOficialNO").attr('disabled', false);
        $("#flatCuentaOficialSI").prop('checked', false);
        $("#flatCuentaOficialNO").prop('checked', false);

      //  $("#flatCuentaOficialSI").prop('checked', false);
      ////$("#flatCuentaOficialNO").prop('checked', false);
      ////$("#flatCancelarTransfSI").prop('checked', false);
      //  $("#flatCancelarTransfNO").prop('checked', false);

        $("#txtNumCuenta").val("");
        $("#ddlEntidadFinancieraID").val("0");
        $("#ddlTipoMonedaCuenta").val("0");
        $("#ddlTTipoCambio").val("0");
        $("#txtNumCuenta").attr('readonly', false);
        $("#ddlEntidadFinancieraID").attr('disabled', false);
        $("#ddlTipoMonedaCuenta").attr('disabled', false);
        $("#ddlTTipoCambio").attr('disabled', false);


    }
    function LimpiarFiltrosSeleccionCuenta() {
        $("#ICueCodigoTransf").val("");
        $("#ICueNombreTransf").val("");
        $("#ddlElementoIDTransf").val(0);
        $("#txtCueCodigoTransf").val("");
    }

//Crear Cuenta Nueva - Destino
    function NuevaCuentaDestino() { //NuevaCuentaTranferencia

        $('#txtPorcentajeDebe').attr('readonly', false);
        $('#txtPorcentajeHaber').attr('readonly', false);
        //$('#txtPorcentajeDebe').val("");
        //$('#txtPorcentajeHaber').val("");
        $('#btnSeleccionarTransfiere').attr("disabled", false);
        $('#txtTransfiere').val("");
        //$('#btnSeleccionarTransfiere').attr("disabled", true);
        $("#flatCancelarTransfSI").attr('disabled', true);
        $("#flatCancelarTransfNO").attr('disabled', true);
        $("#flatCuentaOficialSI").attr('disabled', true);
        $("#flatCuentaOficialNO").attr('disabled', true);
        $("#txtNumCuenta").attr('readonly', true);
        //$("#flatCuentaOficialSI").prop('checked', false);
        //$("#flatCuentaOficialNO").prop('checked', false);
        //$("#flatCancelarTransfSI").prop('checked', false);
        //$("#flatCancelarTransfNO").prop('checked', false);

        $("#ddlEntidadFinancieraID").attr('disabled', true);
        $("#ddlTipoMonedaCuenta").attr('disabled', true);
        $("#ddlTTipoCambio").attr('disabled', true);

        $("#DVCrearCuenta").dialog({
            width: 870,//760
            height: 460, //360
            title: "Nueva Cuenta - Transferencia",
            modal: true
           , resizable: false,
            dialogClass: 'hide-close',
            draggable: false,
            closeOnEscape: false
        });
        return false;

    }
    function CrearCuentaDestino() {
        var obj = {};
        //DATOS DEFINIDOS DE LA CUENTA 
        
        obj.Cue_Nombre_tx = $("#txtNombreCuenta").val();
            //CENTRO-COSTO
        if ($("#flatAsociado").is(':checked')) {
            obj.Cue_Cc_Fl = true;
        }
        else
            if ($("#flatAsociado").is(':checked'))
            { obj.Cue_Cc_Fl = false; }

            else { obj.Cue_Cc_Fl = false; }

        obj.Cue_Ta_Tx = $("#txtTA").val();

            //TIPO-DESTINO
        if ($("#flatTipoDebe").is(':checked')) {
            obj.Cue_TipoDestino_FL = true;
            obj.Cue_Transfiere_Tx = $("#txtTransfiere").val();
            //Convertir a Decimal 
            EnteroDebe = $("#txtPorcentajeDebe").val();
            EnteroHaber = $("#txtPorcentajeHaber").val();
            PorcentajeDebe = EnteroDebe / 100;
            PorcentajeHaber = EnteroHaber / 100;
            //Almacenar Porcentajes en obj
            obj.Cue_PorcenDebe_nu = PorcentajeDebe //Decimal
            obj.Cue_PorcenHaber_nu = PorcentajeHaber //Decimal

        }
        else
            if ($("#flatTipoHaber").is(':checked')) {
                obj.Cue_TipoDestino_FL = false;
                obj.Cue_Transfiere_Tx = $("#txtTransfiere").val();
                //Convertir a Decimal 
                EnteroDebe = $("#txtPorcentajeDebe").val();
                EnteroHaber = $("#txtPorcentajeHaber").val();
                PorcentajeDebe = EnteroDebe / 100;
                PorcentajeHaber = EnteroHaber / 100;
                //Almacenar Porcentajes en obj
                obj.Cue_PorcenDebe_nu = PorcentajeDebe //Decimal
                obj.Cue_PorcenHaber_nu = PorcentajeHaber //Decimal
            }
            else
                if ($("#flatTipoNinguno").is(':checked')) {
                    obj.Cue_TipoDestino_FL = null;
                }
                else {
                    obj.Cue_TipoDestino_FL = null;
                }

        //CODIGO
        //obj.Cue_Codigo = $("#txtCuentaCodigoCuenta").val();
        obj.Cue_Codigo = $("#hdfCodigoCuenta").val();

        //LECTURA DE LOS PARAMETROS ADICIONALES 18 AGOSTO
        if ($("#flatCancelarTransfSI").is(':checked')) {
            obj.Cue_CancTranf_FL = true;
        }
        else if ($("#flatCancelarTransfNO").is(':checked')) {
            obj.Cue_CancTranf_FL = false;
        } else {
            obj.Cue_CancTranf_FL = false;
        }

        if ($("#flatCuentaOficialSI").is(':checked')) {
            obj.Cue_CuentaOficial_FL = true;
        }
        else if ($("#flatCuentaOficialNO").is(':checked')) {
            obj.Cue_CuentaOficial_FL = false;
        } else {
            obj.Cue_CuentaOficial_FL = false;
        }

        obj.Cue_NumeroCuenta = $("#txtNumCuenta").val();

        if ($("#ddlEntidadFinancieraID").val() != 0) {
            obj.Banco_Id = $("#ddlEntidadFinancieraID").val();
        }
        if ($("#ddlTipoMonedaCuenta").val() != 0) {
            obj.Moneda_Id = $("#ddlTipoMonedaCuenta").val();
        }
        if ($("#ddlTTipoCambio").val() != 0) {
            obj.TTipoCambio_Id = $("#ddlTTipoCambio").val();
        }

        ///DATOS PREDETERMINADOS
        obj.Cue_Padre_Id = $("#hdfIDCuentaPadre").val();
        obj.Elemento_Id = $("#hdfIDElementoPadre").val();
        obj.PlanCuenta_Id = $("#hdfPlanCuenta_Id").val();
        obj.Aud_UsuarioCreacion_Id = $("#ContentPlaceHolder1_hdUsuario").val();

        $.ajax({
            async: true,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../Contabilidad/Maestros/Cuentas.aspx/CrearCuentaTransfiere',
            dataType: "json",
            data: "{ObjCuenta:" + JSON.stringify(obj) + "}",
            success: function (msg) {
                Mensaje("Mensaje", "Se registro Correctamente la Cuenta Destino");

                ListarCuenta();
                ListarCuentaSeleccion();
                //ListarCuenta();
                //ListarCuentaSeleccion();
                CerrarFormularioNuevoDestino();
                LimpiarDatosNuevaCuentaIngreso();

            },
            error: function (xhr, status, error) {
                alert('error: function (xhr, status, error) {');
                Mensaje("Mensaje", "Ocurrio un error en la creacion de la cuenta - destino");
                alert(xhr.responseText);
            }
        })

    }

    //Limpiar Data
    function LimpiarDatosNuevoDestinoIngreso() {
     
        //$("#flatAsociado").prop('checked', false);
        //$("#flatSINAsociar").prop('checked', false);
        //$("#txtTA").val("");
        $("#txtTransfiere").val("");
        $("#txtPorcentajeDebe").val("");
        $("#txtPorcentajeHaber").val("");
        //$("#flatTipoDebe").prop('checked', false);
        //$("#flatTipoHaber").prop('checked', false);
        //$("#flatTipoNinguno").prop('checked', false);

        $('#txtPorcentajeDebe').attr('readonly', false);
        $('#txtPorcentajeHaber').attr('readonly', false);
        $('#txtPorcentajeDebe').val("");
        $('#txtPorcentajeHaber').val("");
        $('#btnSeleccionarTransfiere').attr("disabled", false);
        //DATOS NUEVOS
        //$("#flatCancelarTransfSI").attr('disabled', true);
        //$("#flatCancelarTransfNO").attr('disabled', true);
        //$("#flatCuentaOficialSI").attr('disabled', true);
        //$("#flatCuentaOficialNO").attr('disabled', true);
        //$("#flatCancelarTransfSI").prop('checked', false);
        //$("#flatCancelarTransfNO").prop('checked', false);
       
        ////$("#flatCuentaOficialSI").prop('checked', false);
        ////$("#flatCuentaOficialNO").prop('checked', false);
        $("#txtNumCuenta").val("");
        $("#ddlEntidadFinancieraID").val("0");
        $("#ddlTipoMonedaCuenta").val("0");
        $("#ddlTTipoCambio").val("0");
    }

    function CerrarFormularioNuevoDestino() {
        $("#DVCrearCuenta").dialog('close');
        LimpiarDatosNuevoDestinoIngreso();
    }

    function ValidarPorcentajeDebeHaber() {
        PorceDebe = $("#txtPorcentajeDebe").val();
        PorceHaber = $("#txtPorcentajeHaber").val();
            //if (PorceDebe > 1) {  Mensaje('Mensaje','El Porcentaje Debe no debe exceder al 100%') }
            //else if (PorceHaber > 1) { Mensaje('Mensaje', 'El Porcentaje Haber no debe exceder al 100%')}
        DebeDecimal=PorceDebe/100;
        HaberDecimal = PorceHaber / 100;
        if (DebeDecimal > 1) {
            Mensaje('Mensaje','El Porcentaje Debe no debe exceder al 100%')
        }
        else if (HaberDecimal > 1) {
            Mensaje('Mensaje', 'El Porcentaje Haber no debe exceder al 100%')
        }
        if ((DebeDecimal + HaberDecimal) < 1) {
            Mensaje('Mensaje', 'El Porcentaje entre debe y haber debe sumar a 100%')
        }
        if ((DebeDecimal + HaberDecimal) > 1) {
            Mensaje('Mensaje', 'El Porcentaje entre debe y haber no debe superar el 100%')
        }

    }

 
///////////////////////////////// MENSAJE PARA CREAR CUENTA DESTINO ////////////////////////////

    function MensajedeRegistro() {
        $("#DVMensajeRegistroCuenta").dialog({
            //width: 400, // 800
            //height: 300, //330
            maxWidth: 500,
            minWidth: 400,
            title: "Mensaje",
            modal: true
            , resizable: false,
            dialogClass: 'hide-close',
            draggable: false,
            closeOnEscape: false
        });
        return false;
    }

    function CerrarMensajedeRegistroCuenta() {
        $("#DVMensajeRegistroCuenta").dialog('close');
    }