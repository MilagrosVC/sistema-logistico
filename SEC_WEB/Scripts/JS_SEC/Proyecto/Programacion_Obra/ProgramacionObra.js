﻿$.ui.plugin.add('resizable', 'alsoResizeReverse', {

    start: function () {
        var that = $(this).data('ui-resizable'),
        o = that.options,
        _store = function (exp) {
            $(exp).each(function () {
                var el = $(this);
                el.data('ui-resizable-alsoresize-reverse', {
                    width: parseInt(el.width(), 10), height: parseInt(el.height(), 10),
                    left: parseInt(el.css('left'), 10), top: parseInt(el.css('top'), 10)
                });
            });
        };

        if (typeof (o.alsoResizeReverse) === 'object' && !o.alsoResizeReverse.parentNode) {
            if (o.alsoResizeReverse.length) { o.alsoResize = o.alsoResizeReverse[0]; _store(o.alsoResizeReverse); }
            else { $.each(o.alsoResizeReverse, function (exp) { _store(exp); }); }
        } else {
            _store(o.alsoResizeReverse);
        }
    },

    resize: function (event, ui) {
        var that = $(this).data('ui-resizable'),
        o = that.options,
        os = that.originalSize,
        op = that.originalPosition,
        delta = {
            height: (that.size.height - os.height) || 0, width: (that.size.width - os.width) || 0,
            top: (that.position.top - op.top) || 0, left: (that.position.left - op.left) || 0
        },

        _alsoResizeReverse = function (exp, c) {
            $(exp).each(function () {
                var el = $(this), start = $(this).data('ui-resizable-alsoresize-reverse'), style = {},
                css = c && c.length ? c : el.parents(ui.originalElement[0]).length ? ['width', 'height'] : ['width', 'height', 'top', 'left'];

                $.each(css, function (i, prop) {
                    var sum = (start[prop] || 0) - (delta[prop] || 0); // subtracting instead of adding
                    if (sum && sum >= 0) {
                        style[prop] = sum || null;
                    }
                });

                el.css(style);
            });
        };

        if (typeof (o.alsoResizeReverse) === 'object' && !o.alsoResizeReverse.nodeType) {
            $.each(o.alsoResizeReverse, function (exp, c) { _alsoResizeReverse(exp, c); });
        } else {
            _alsoResizeReverse(o.alsoResizeReverse);
        }
    },

    stop: function (event, ui) {
        $(this).removeData("resizable-alsoresize-reverse");
    }
});

$(document).ready(function () {
    /*$("#divisor").resizable({
        alsoResizeReverse: "#tbProgramacion",
        handles: 'e, w'
    });*/
    GenerarTabla();
    CargarDatosValorizacion();
    //CargarValorizacion();
    CargarProgramacion();
    CargarValorizacion();
});



function CargarDatosValorizacion()
{
    var partida = { Partida_Id: 1 };
    //alert(partida.Partida_Id);
    $.ajax({
        type: 'POST',
        url: '../../Proyecto/Programacion_Obra/ProgramacionObra.aspx/ObtenerValorizacionPartida',
        data: "{partida:'" + JSON.stringify(partida) + "'}",
        contentType: 'application/json; charset=utf-8',
        dataType: "json",
        async: false,
        success: function (data) {
            if (data.d == '') {
                alert("Error al obtener Valorizacion de la partida");
                return;
            }
            else {
                //alert(data.d[0].NomCliente);
                $('#lblObra').html(data.d[0].Val_Obra_Tx);
                $('#lblCliente').html(data.d[0].NomCliente);
                $('#lblEjecuta').html(data.d[0].NomProveedor);
                $('#lblPlazo').html(data.d[0].NomPartida);
                var FechaVal = new Date(parseInt(data.d[0].Val_Fe.substr(6)));
                $('#lblFecha').html(FechaVal.toLocaleDateString());

                //alert(data.d[0].NomCliente);
            }
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}

function CargarDesdeASP(id)
{
    var listaDatos = [];

    var parametro = '{idValorizacionCa: "' + id + '"}';

    $.ajax({
        async: false,
        type: "POST",
        data: parametro,
        url: '../../Proyecto/Programacion_Obra/ProgramacionObra.aspx/ListarItemsValorizacion',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            /*for (var i = 0; i < msg.d.length; i++) {
                listaDatos.push([msg.d[i].Itm_Co, msg.d[i].VDe_Descripcion_Tx, msg.d[i].UMe_Simbolo_Tx,
                                 msg.d[i].VDe_Metrado_Nu, msg.d[i].Itm_Precio_Unitario_Nu, msg.d[i].VDe_Monto_Nu]);
            }*/
            listaDatos = msg.d;
        },
        error: function (msg) {
            listaDatos = msg;
        }
    });
    return listaDatos;
}

function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
    Handsontable.renderers.TextRenderer.apply(this, arguments);
    td.style.fontWeight = 'bold';
    td.style.color = 'green';
    td.style.background = '#CEC';
}
function CargarValorizacion()
{
    var datos = CargarDesdeASP(1);
    var container = document.getElementById("tbValorizacion");

    var hot = new Handsontable(container, {
        data: datos,
        height: 400,
        className: "htCenter",
        colHeaders: ["ITEM", "DESCRIPCION", "UND", "METRADO", "P. UNITARIO", "MONTO PARCIAL"],
        columns: [
            { data: 'Itm_Co', className: "htLeft", readOnly: true },
            { data: 'VDe_Descripcion_Tx', className: "htLeft", readOnly: true},
            { data: 'UMe_Simbolo_Tx', readOnly: true},
            { data: 'VDe_Metrado_Nu', className: "htRight", readOnly: true },
            { data: 'Itm_Precio_Unitario_Nu', className: "htRight", readOnly: true },
            { data: 'VDe_Monto_Nu', className: "htRight", readOnly: true }],
        colWidths: [70, 400, 40, 70, 75, 95],
        //width:100%
        //stretchH: 'all',    
        manualColumnResize: true,
        /*cells: function (rowIndex, columnIndex, property) {
            var cellProperties = {};
            var cellValue = $table.handsontable('getDate')[rowIndex][columnIndex];
            if (cellValue == 'someValue') {
                cellProperties.readOnly = false;
            }
            return cellProperties;
        }*/
        
    });
    hot.updateSettings({
        cells: function (row, col, prop) {
            var cellProperties = {};
            var dato = hot.getData()[row][prop];
            var row_id = -1;
            if (col == 0 && dato.length === 2) {
                //cellProperties.readOnly = false;
                //cellProperties.renderer = firstRowRenderer;
                
                cellProperties.renderer = firstRowRenderer;
                row_id = row;
                //$table.handsontable('setDate')[3][col] = '';
                //td.style.fontWeight = 'bold';
                //td.style.color = 'green';
                //td.style.background = '#CEC';
            }
            /*if (row_id != -1) {
                //alert("Cuenta");
                cellProperties.renderer = firstRowRenderer; // uses function directly
            }*/
            return cellProperties;
        }
    })
}
function CargarProgramacion() {
    var data = [['Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1'],
                ['Datos Prog A2', 'Datos Prog B2', 'Datos Prog C2', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1'],
                ['Datos Prog A3', 'Datos Prog B3', 'Datos Prog C3', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1'],
                ['Datos Prog A4', 'Datos Prog B4', 'Datos Prog C4', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1'],
                ['Datos Prog A5', 'Datos Prog B5', 'Datos Prog C5', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1', 'Datos Prog A1', 'Datos Prog B1', 'Datos Prog C1'], ];
    var container = document.getElementById("tbProgramacion");
    var hot = new Handsontable(container, {
        //data: data,
        height: 400,
        colHeaders: ["Programacion A", "Programacion B", "Programacion C", "Programacion A", "Programacion B", "Programacion C", "Programacion A", "Programacion B", "Programacion C"]
    });
}
/*---------------------------------------------------------------------------------------------------*/
function GenerarTabla()
{
    var lstDatos = CargarDesdeASP(1);
    //alert(lstDatos);
    var cantidad = lstDatos.length;
    var rows = "<tr>";

    for (var i = 0; i < msg.d.length; i++) {
        rows = rows + "<td>" + lstDatos[i].Itm_Co + "</td>";
        rows = rows + "<td>" + lstDatos[i].VDe_Descripcion_Tx + "</td>";
        rows = rows + "<td>" + lstDatos[i].UMe_Simbolo_Tx + "</td>";
        rows = rows + "<td>" + lstDatos[i].VDe_Metrado_Nu + "</td>";
        rows = rows + "<td>" + lstDatos[i].Itm_Precio_Unitario_Nu + "</td>";
        rows = rows + "<td>" + lstDatos[i].VDe_Monto_Nu + "</td>";
    }
    
    rows = rows + "</tr>";

    //alert(rows);
}


