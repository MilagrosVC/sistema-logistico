﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SEC_Utilitarios;

namespace SEC_WEB.Home
{
    public partial class HOME : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.hdf_Usuario.Value = HttpContext.Current.Session[Constantes.SessionVariables.Usuario].ToString();
        }
    }
}