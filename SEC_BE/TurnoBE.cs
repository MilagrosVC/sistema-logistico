﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class TurnoBE :AuditoriaBE
    {
        private int _Turno_Id;        
        private String _Tur_Nombre_Tx;        
        private String _Tur_Descripcion_Tx;        
        private TimeSpan _Tur_Hora_Ini_Ti;        
        private TimeSpan _Tur_Hora_Fin_Ti;        
        private int _Tur_Horas_Nu;

        public int Turno_Id
        {
            get { return _Turno_Id; }
            set { _Turno_Id = value; }
        }

        public String Tur_Nombre_Tx
        {
            get { return _Tur_Nombre_Tx; }
            set { _Tur_Nombre_Tx = value; }
        }

        public String Tur_Descripcion_Tx
        {
            get { return _Tur_Descripcion_Tx; }
            set { _Tur_Descripcion_Tx = value; }
        }

        public TimeSpan Tur_Hora_Ini_Ti
        {
            get { return _Tur_Hora_Ini_Ti; }
            set { _Tur_Hora_Ini_Ti = value; }
        }

        public TimeSpan Tur_Hora_Fin_Ti
        {
            get { return _Tur_Hora_Fin_Ti; }
            set { _Tur_Hora_Fin_Ti = value; }
        }

        public int Tur_Horas_Nu
        {
            get { return _Tur_Horas_Nu; }
            set { _Tur_Horas_Nu = value; }
        }
    }
}
