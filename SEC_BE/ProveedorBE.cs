﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ProveedorBE : AuditoriaBE
    {
        private int _Prv_Id;
        private int _Rubro_Id;
        private int _Tipopersona_Id;
        private bool _Prv_Homologado_Fl;
        private int _Prv_Estado_Homologado_Id;
        private int _Prv_Homologadora_Id;
        private int? _Prv_Estado_Id;
        private String _Prv_APaterno_tx;
        private String _Prv_AMaterno_tx;
        private String _Prv_Nombres_tx;
        private String _Prv_RazonSocial_Tx;
        private String _Prv_Personacontacto_Tx;
        private int _TipDocIdent_Id;
        private String _Prv_DocIdent_nu;
        private DateTime? _Prv_Nacimiento_Fe;
        private String _Prv_Direccion_tx;
        private int _Ubigeo_Id;
        private String _Prv_Telefono1_nu;
        private String _Prv_Telefono2_nu;
        private String _Prv_Email_tx;
        private int? _Prv_Cla_Id;

        //-----------------Otros-------------------//
        private int _Departamento_Co;
        private int _Provincia_co;
        private int _Distrito_co;
        private String _NombreProveedor_Tx;
        private String _Distrito_Tx;        

        private String _Par_Nombre_Tx;

        private String _Lst_Proveedor;
        private String _Prv_Tx;
        private String _TipoPersona_Tx;

        public String TipoPersona_Tx
        {
            get { return _TipoPersona_Tx; }
            set { _TipoPersona_Tx = value; }
        }

        public String Prv_Tx
        {
            get { return _Prv_Tx; }
            set { _Prv_Tx = value; }
        }

        public String Lst_Proveedor {
            get { return _Lst_Proveedor; }
            set { _Lst_Proveedor = value; }
        }

        public String Par_Nombre_Tx {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }

        public int Departamento_Co
        {
            get { return _Departamento_Co; }
            set { _Departamento_Co = value; }
        }
        public int Provincia_co
        {
            get { return _Provincia_co; }
            set { _Provincia_co = value; }
        }
        public int Distrito_co
        {
            get { return _Distrito_co; }
            set { _Distrito_co = value; }
        }
        
        public String NombreProveedor_Tx {
            get { return _NombreProveedor_Tx; }
            set { _NombreProveedor_Tx = value; }
        }
        //----------------------------------------//

        
        public int Prv_Id
        {
            get { return _Prv_Id; }
            set { _Prv_Id = value; }
        }

		public int Rubro_Id
		{
            get { return _Rubro_Id; }
            set { _Rubro_Id = value; }
        }

		public int Tipopersona_Id
		{
            get { return _Tipopersona_Id; }
            set { _Tipopersona_Id = value; }
        }

		public bool Prv_Homologado_Fl
		{
            get { return _Prv_Homologado_Fl; }
            set { _Prv_Homologado_Fl = value; }
        }

		public int Prv_Estado_Homologado_Id
		{
            get { return _Prv_Estado_Homologado_Id; }
            set { _Prv_Estado_Homologado_Id = value; }
        }

		public int Prv_Homologadora_Id
		{
            get { return _Prv_Homologadora_Id; }
            set { _Prv_Homologadora_Id = value; }
        }

		public int? Prv_Estado_Id
		{
            get { return _Prv_Estado_Id; }
            set { _Prv_Estado_Id = value; }
        }

		public String Prv_APaterno_tx
		{
            get { return _Prv_APaterno_tx; }
            set { _Prv_APaterno_tx = value; }
        }

		public String Prv_AMaterno_tx
		{
            get { return _Prv_AMaterno_tx; }
            set { _Prv_AMaterno_tx = value; }
        }

		public String Prv_Nombres_tx
		{
            get { return _Prv_Nombres_tx; }
            set { _Prv_Nombres_tx = value; }
        }

		public String Prv_RazonSocial_Tx
		{
            get { return _Prv_RazonSocial_Tx; }
            set { _Prv_RazonSocial_Tx = value; }
        }

		public String Prv_Personacontacto_Tx
		{
            get { return _Prv_Personacontacto_Tx; }
            set { _Prv_Personacontacto_Tx = value; }
        }

		public int TipDocIdent_Id
		{
            get { return _TipDocIdent_Id; }
            set { _TipDocIdent_Id = value; }
        }

		public String Prv_DocIdent_nu
		{
            get { return _Prv_DocIdent_nu; }
            set { _Prv_DocIdent_nu = value; }
        }

		public DateTime? Prv_Nacimiento_Fe
		{
            get { return _Prv_Nacimiento_Fe; }
            set { _Prv_Nacimiento_Fe = value; }
        }

		public String Prv_Direccion_tx
		{
            get { return _Prv_Direccion_tx; }
            set { _Prv_Direccion_tx = value; }
        }

		public int Ubigeo_Id
		{
            get { return _Ubigeo_Id; }
            set { _Ubigeo_Id = value; }
        }

		public String Prv_Telefono1_nu
		{
            get { return _Prv_Telefono1_nu; }
            set { _Prv_Telefono1_nu = value; }
        }

		public String Prv_Telefono2_nu
		{
            get { return _Prv_Telefono2_nu; }
            set { _Prv_Telefono2_nu = value; }
        }

		public String Prv_Email_tx
		{
            get { return _Prv_Email_tx; }
            set { _Prv_Email_tx = value; }
        }

		public int? Prv_Cla_Id
		{
            get { return _Prv_Cla_Id; }
            set { _Prv_Cla_Id = value; }
        }

        public String Distrito_Tx
        {
            get { return _Distrito_Tx; }
            set { _Distrito_Tx = value; }
        }
    }
}
