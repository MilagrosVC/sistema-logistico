﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class SolicitudProveedorBE : AuditoriaBE
    {
        private int _Solicitud_Id; 
        private int _Solicitud_Prov_Id;
        private int _Proveedor_Id;
        private String _Prv_Tx;
        private int _TipoPersona_Id;
        private String _TipoPersona_Tx;
        private String _Prv_Direccion_Tx;
        private String _Prv_Telefono1_nu;

        public int Solicitud_Id
        {
            get { return _Solicitud_Id; }
            set { _Solicitud_Id = value; }
        }
        public int Solicitud_Prov_Id
        {
            get { return _Solicitud_Prov_Id; }
            set { _Solicitud_Prov_Id = value; }
        }
        public int TipoPersona_Id
        {
            get { return _TipoPersona_Id; }
            set { _TipoPersona_Id = value; }
        }
        public String TipoPersona_Tx
        {
            get { return _TipoPersona_Tx; }
            set { _TipoPersona_Tx = value; }
        }
        public int Proveedor_Id
        {
            get { return _Proveedor_Id; }
            set { _Proveedor_Id = value; }
        }
        public String Prv_Tx
        {
            get { return _Prv_Tx; }
            set { _Prv_Tx = value; }
        }
        public String Prv_Direccion_Tx
        {
            get { return _Prv_Direccion_Tx; }
            set { _Prv_Direccion_Tx = value; }
        }
        public String Prv_Telefono1_nu
        {
            get { return _Prv_Telefono1_nu; }
            set { _Prv_Telefono1_nu = value; }
        }
    }
}
