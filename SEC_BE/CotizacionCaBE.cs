﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class CotizacionCaBE : AuditoriaBE
    {
        private int _Requerimiento_Id;
        private int _Cotizacion_Id;
        private int _Solicitud_Id;
        private int? _Proveedor_Id;
        private String _Cot_Gestionador_Tx;
        private int _Cot_Tipo_Id;
        private int? _Cot_Estado_Id;
        private int _Cot_Plazo_Entrega_Id;
        private int _Cot_Forma_Pago_Id;
        private decimal _CDe_Total_Nu;
        private String _Cot_NumProforma_Nu;
        private String _Cot_Prv_NomVendedor_Tx;
        private String _Cot_Prv_TelVendedor_Nu;
        private String _Cot_Prv_MailVendedor_Tx;
        private decimal _Cot_Monto_Nu;
        private decimal _Cot_IGV_Nu;
        private decimal _Cot_MontoTotal_Nu;
        private decimal? _Cot_Detraccion_Nu;
        private String _Cot_Observacion_Tx;
        private DateTime _Cot_FechaEmision_Fe;
        private int _Cot_Moneda_Id;
        private Boolean _Cot_Anticipo_FL;

        
        /* Atributos adicionales */
        private String _Requerimiento_Co;
        private String _Cot_Proveedor_Tx;
        private int _Cot_Cant_Items;
        private String _Cot_Tipo_Tx;
        private String _Mon_Simbolo_Tx;
        private String _Cot_Forma_Pago_Tx;
        private String _Cot_Estado_Tx;
        private String _Mon_Nombre_Tx;


        private String _Emp_Direccion_tx;
        private String _Emp_RazonSocial_Tx;
        private String _Emp_DocIdent_RUC_nu;
        private String _Prv_DocIdent_nu;

        public String Prv_DocIdent_nu
        {
            get { return _Prv_DocIdent_nu; }
            set { _Prv_DocIdent_nu = value; }
        }

        public String Emp_Direccion_tx
        {
            get { return _Emp_Direccion_tx; }
            set { _Emp_Direccion_tx = value; }
        }
        public String Emp_RazonSocial_Tx
        {
            get { return _Emp_RazonSocial_Tx; }
            set { _Emp_RazonSocial_Tx = value; }
        }
        public String Emp_DocIdent_RUC_nu
        {
            get { return _Emp_DocIdent_RUC_nu; }
            set { _Emp_DocIdent_RUC_nu = value; }
        }
        public String Requerimiento_Co
        {
            get { return _Requerimiento_Co; }
            set { _Requerimiento_Co = value; }
        }
        public String Cot_Proveedor_Tx
        {
            get { return _Cot_Proveedor_Tx; }
            set { _Cot_Proveedor_Tx = value; }
        }
        public int Cot_Cant_Items
        {
            get { return _Cot_Cant_Items; }
            set { _Cot_Cant_Items = value; }
        }
        public String Cot_Tipo_Tx
        {
            get { return _Cot_Tipo_Tx; }
            set { _Cot_Tipo_Tx = value; }
        }
        public String Mon_Simbolo_Tx
        {
            get { return _Mon_Simbolo_Tx; }
            set { _Mon_Simbolo_Tx = value; }
        }
        public String Cot_Forma_Pago_Tx
        {
            get { return _Cot_Forma_Pago_Tx; }
            set { _Cot_Forma_Pago_Tx = value; }
        }
        public String Cot_Estado_Tx
        {
            get { return _Cot_Estado_Tx; }
            set { _Cot_Estado_Tx = value; }
        }

        public Boolean Cot_Anticipo_FL
        {
            get { return _Cot_Anticipo_FL; }
            set { _Cot_Anticipo_FL = value; }
        }
        /* --------------------- */
        public int Requerimiento_Id
        {
            get { return _Requerimiento_Id; }
            set { _Requerimiento_Id = value; }
        }
        public int Cotizacion_Id 
        { 
            get { return _Cotizacion_Id; } 
            set { _Cotizacion_Id = value; } 
        }
        public int Solicitud_Id 
        { 
            get { return _Solicitud_Id; } 
            set { _Solicitud_Id = value; } 
        }
        public int? Proveedor_Id 
        { 
            get { return _Proveedor_Id; } 
            set { _Proveedor_Id = value; } 
        }
        public String Cot_Gestionador_Tx 
        { 
            get { return _Cot_Gestionador_Tx; } 
            set { _Cot_Gestionador_Tx = value; } 
        }
        public int Cot_Tipo_Id 
        { 
            get { return _Cot_Tipo_Id; } 
            set { _Cot_Tipo_Id = value; } 
        }
        public int? Cot_Estado_Id { 
            get { return _Cot_Estado_Id; } 
            set { _Cot_Estado_Id = value; } 
        }
        public int Cot_Plazo_Entrega_Id 
        { 
            get { return _Cot_Plazo_Entrega_Id; } 
            set { _Cot_Plazo_Entrega_Id = value; } 
        }
        public int Cot_Forma_Pago_Id 
        { 
            get { return _Cot_Forma_Pago_Id; } 
            set { _Cot_Forma_Pago_Id = value; } 
        }
        public decimal CDe_Total_Nu 
        { 
            get { return _CDe_Total_Nu; } 
            set { _CDe_Total_Nu = value; } 
        }
        public String Cot_NumProforma_Nu 
        { 
            get { return _Cot_NumProforma_Nu; } 
            set { _Cot_NumProforma_Nu = value; } 
        }
        public String Cot_Prv_NomVendedor_Tx 
        { 
            get { return _Cot_Prv_NomVendedor_Tx; } 
            set { _Cot_Prv_NomVendedor_Tx = value; } 
        }
        public String Cot_Prv_TelVendedor_Nu 
        { 
            get { return _Cot_Prv_TelVendedor_Nu; } 
            set { _Cot_Prv_TelVendedor_Nu = value; } 
        }
        public String Cot_Prv_MailVendedor_Tx 
        { 
            get { return _Cot_Prv_MailVendedor_Tx; } 
            set { _Cot_Prv_MailVendedor_Tx = value; } 
        }
        public decimal Cot_Monto_Nu 
        { 
            get { return _Cot_Monto_Nu; } 
            set { _Cot_Monto_Nu = value; } 
        }
        public decimal Cot_IGV_Nu
        { 
            get { return _Cot_IGV_Nu; } 
            set { _Cot_IGV_Nu = value; } 
        }
        public decimal Cot_MontoTotal_Nu 
        { 
            get { return _Cot_MontoTotal_Nu; } 
            set { _Cot_MontoTotal_Nu = value; } 
        }
        public decimal? Cot_Detraccion_Nu 
        { 
            get { return _Cot_Detraccion_Nu; } 
            set { _Cot_Detraccion_Nu = value; } 
        }
        public String Cot_Observacion_Tx 
        { 
            get { return _Cot_Observacion_Tx; } 
            set { _Cot_Observacion_Tx = value; } 
        }
        public DateTime Cot_FechaEmision_Fe 
        { 
            get { return _Cot_FechaEmision_Fe; } 
            set { _Cot_FechaEmision_Fe = value; } 
        }
        public int Cot_Moneda_Id 
        { 
            get { return _Cot_Moneda_Id; } 
            set { _Cot_Moneda_Id = value; } 
        }

        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }
    }
}
