﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class MonedaBE : AuditoriaBE
    {
        private int _Moneda_Id;        
        private String _Mon_Nombre_Tx;        
        private String _Mon_Simbolo_Tx;        
        private String _Mon_Descripcion_Tx;        

        public int Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }

        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }

        public String Mon_Simbolo_Tx
        {
            get { return _Mon_Simbolo_Tx; }
            set { _Mon_Simbolo_Tx = value; }
        }

        public String Mon_Descripcion_Tx
        {
            get { return _Mon_Descripcion_Tx; }
            set { _Mon_Descripcion_Tx = value; }
        }
    }
}
