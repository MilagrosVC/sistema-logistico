﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class EgresoBE :AuditoriaBE
    {
        private int _Egreso_Id;
        private int _Solicitud_Anticipo_Id;
        private String _Ege_NumeroCheque;
        private String _Ege_NumeroOperacion;
        private decimal? _Ege_TipoCambio;
        private int? _BancoOrigen_Id;
        private int? _BancoDestino_Id;
        private String _Ege_CuentaOrigen;
        private String _Ege_CuentaDestino;


        public int Egreso_Id 
        { 
            get { return _Egreso_Id; } 
            set { _Egreso_Id = value; } 
        }
        public int Solicitud_Anticipo_Id 
        { 
            get { return _Solicitud_Anticipo_Id; } 
            set { _Solicitud_Anticipo_Id = value; } 
        }
        public String Ege_NumeroCheque 
        { 
            get { return _Ege_NumeroCheque; } 
            set { _Ege_NumeroCheque = value; } 
        }
        public String Ege_NumeroOperacion 
        { 
            get { return _Ege_NumeroOperacion; } 
            set { _Ege_NumeroOperacion = value; } 
        }
        public decimal? Ege_TipoCambio 
        { get { return _Ege_TipoCambio; } 
            set { _Ege_TipoCambio = value; } 
        }
        public int? BancoOrigen_Id 
        { 
            get { return _BancoOrigen_Id; } 
            set { _BancoOrigen_Id = value; } 
        }
        public int? BancoDestino_Id 
        { 
            get { return _BancoDestino_Id; } 
            set { _BancoDestino_Id = value; } 
        }
        public String Ege_CuentaOrigen 
        { 
            get { return _Ege_CuentaOrigen; } 
            set { _Ege_CuentaOrigen = value; } 
        }
        public String Ege_CuentaDestino 
        { 
            get { return _Ege_CuentaDestino; } 
            set { _Ege_CuentaDestino = value; } 
        }


        ///PARAMETROS DE OTRAS TABLAS
        ///
        private int _Cotizacion_Id;
        private String _Mon_Nombre_Tx;
        private String _Mon_Simbolo_Tx;
        private decimal _Sol_Monto_Solicitado_nu;
        private String _TMP_Nombre_Tx;
        private String _Par_Nombre_Tx;
        private String _Ban_Nombre_Tx; //BANCO
        private String _Ban_Nombre_Tx_Destino;
        private int _Moneda_Id;
        private int _FormaPago_Id;
        private int _Sol_Tipo_Anticipo_Id;
        

        /// Proveedor
        private int _Proveedor_Id;
        private String _NombreProveedor_Tx;
        //Tipo Persona
        private int _TipoPersona_Id;
        private String _TipoPersona_TXx;
        //Moneda Monto
        private String _Moneda_Monto_Tx;
        public int Cotizacion_Id
        {
            get { return _Cotizacion_Id; }
            set { _Cotizacion_Id = value; }
        }
        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }
        public String Mon_Simbolo_Tx
        {
            get { return _Mon_Simbolo_Tx; }
            set { _Mon_Simbolo_Tx = value; }
        }
        
        public decimal Sol_Monto_Solicitado_nu
        {
            get { return _Sol_Monto_Solicitado_nu; }
            set { _Sol_Monto_Solicitado_nu = value; }
        }
        public String TMP_Nombre_Tx
        {
            get { return _TMP_Nombre_Tx; }
            set { _TMP_Nombre_Tx = value; }
        }
        public String Par_Nombre_Tx
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }
        public String Ban_Nombre_Tx
        {
            get { return _Ban_Nombre_Tx; }
            set { _Ban_Nombre_Tx = value; }
        }
        public String Ban_Nombre_Tx_Destino
        {
            get { return _Ban_Nombre_Tx_Destino; }
            set { _Ban_Nombre_Tx_Destino = value; }
        }
        public int Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }
        public int FormaPago_Id
        {
            get { return _FormaPago_Id; }
            set { _FormaPago_Id = value; }
        }
        public int Sol_Tipo_Anticipo_Id
        {
            get { return _Sol_Tipo_Anticipo_Id; }
            set { _Sol_Tipo_Anticipo_Id = value; }
        }
        public int Proveedor_Id
        {
            get { return _Proveedor_Id; }
            set { _Proveedor_Id = value; }
        }
        
        public String NombreProveedor_Tx
        {
            get { return _NombreProveedor_Tx; }
            set { _NombreProveedor_Tx = value; }
        }



        public int TipoPersona_Id
        {
            get { return _TipoPersona_Id; }
            set { _TipoPersona_Id = value; }
        }
        public String TipoPersona_TXx
        {
            get { return _TipoPersona_TXx; }
            set { _TipoPersona_TXx = value; }
        }

        public String Moneda_Monto_Tx
        {
            get { return _Moneda_Monto_Tx; }
            set { _Moneda_Monto_Tx = value; }
        }
        
    }
}
