﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class CotizacionDeBE:AuditoriaBE
    {
        private int _Cotizacion_Detalle_Id;        
        private int _Cotizacion_Id;        
        private int _Item_Id;        
        private String _CDe_Descripcion_Tx;        
        private int _CDe_Cantidad_Nu;        
        private decimal _CDe_Precio_Unitario_Nu;        
        private decimal _CDe_Sub_Total_Nu;
        private int _CDe_Und_Medida_Id;
        private int _Solicitud_Detalle_Id;
        private int _CDe_Estado_Itm_Id;

        private String _CD_UMe_Simbolo_Tx;
        private String _SDe_Descripcion_Tx;
        private int _SDe_Cantidad_Nu;
        private int _Unidad_Medida_Id;
        private String _SD_UMe_Simbolo_Tx;

        public int CDe_Estado_Itm_Id
        {
            get { return _CDe_Estado_Itm_Id; }
            set { _CDe_Estado_Itm_Id = value; }
        }
        public String CD_UMe_Simbolo_Tx
        {
            get { return _CD_UMe_Simbolo_Tx; }
            set { _CD_UMe_Simbolo_Tx = value; }
        }
        public String SDe_Descripcion_Tx
        {
            get { return _SDe_Descripcion_Tx; }
            set { _SDe_Descripcion_Tx = value; }
        }
        public int SDe_Cantidad_Nu
        {
            get { return _SDe_Cantidad_Nu; }
            set { _SDe_Cantidad_Nu = value; }
        }
        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }
        public String SD_UMe_Simbolo_Tx
        {
            get { return _SD_UMe_Simbolo_Tx; }
            set { _SD_UMe_Simbolo_Tx = value; }
        }

        public int Cotizacion_Detalle_Id
        {
            get { return _Cotizacion_Detalle_Id; }
            set { _Cotizacion_Detalle_Id = value; }
        }

        public int Cotizacion_Id
        {
            get { return _Cotizacion_Id; }
            set { _Cotizacion_Id = value; }
        }

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public String CDe_Descripcion_Tx
        {
            get { return _CDe_Descripcion_Tx; }
            set { _CDe_Descripcion_Tx = value; }
        }

        public int CDe_Cantidad_Nu
        {
            get { return _CDe_Cantidad_Nu; }
            set { _CDe_Cantidad_Nu = value; }
        }

        public decimal CDe_Precio_Unitario_Nu
        {
            get { return _CDe_Precio_Unitario_Nu; }
            set { _CDe_Precio_Unitario_Nu = value; }
        }

        public decimal CDe_Sub_Total_Nu
        {
            get { return _CDe_Sub_Total_Nu; }
            set { _CDe_Sub_Total_Nu = value; }
        }

        public int CDe_Und_Medida_Id 
        { 
            get { return _CDe_Und_Medida_Id; } 
            set { _CDe_Und_Medida_Id = value; } 
        }

        public int Solicitud_Detalle_Id 
        { 
            get { return _Solicitud_Detalle_Id; } 
            set { _Solicitud_Detalle_Id = value; } 
        }
    }
}
