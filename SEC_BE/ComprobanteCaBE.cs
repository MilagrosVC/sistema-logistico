﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ComprobanteCaBE : AuditoriaBE
    {
        private int _Comprobante_Id;        
        private int _Orden_Compra_Id;
        private int _Proveedor_Id;        
        private int _Comprobante_Pago_Id;        
        private int _Com_Estado_Id;        
        private int _Moneda_Id;
        private decimal _Com_IGV_Nu;        
        private decimal _Com_SubTotal_Nu;        
        private decimal _Com_Total_Nu;        
        private DateTime _Com_Emision_Fe;        
        private String _Comprobante_Co;
        private String _Com_Guia_Remision_Nu;
        private String _Prv_RazonSocial_Tx;
        private String _Est_Nombre_Tx;
        private String _Mon_Nombre_Tx;
        private DateTime? _FechaDesde;
        private DateTime? _FechaHasta;
        private String _Prv_Direccion_tx;
        private String _Prv_DocIdent_nu;        

        public int Comprobante_Id
        {
            get { return _Comprobante_Id; }
            set { _Comprobante_Id = value; }
        }

        public int Orden_Compra_Id
        {
            get { return _Orden_Compra_Id; }
            set { _Orden_Compra_Id = value; }
        }

        public int Comprobante_Pago_Id
        {
            get { return _Comprobante_Pago_Id; }
            set { _Comprobante_Pago_Id = value; }
        }

        public int Proveedor_Id
        {
            get { return _Proveedor_Id; }
            set { _Proveedor_Id = value; }
        }

        public int Com_Estado_Id
        {
            get { return _Com_Estado_Id; }
            set { _Com_Estado_Id = value; }
        }

        public int Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }

        public decimal Com_IGV_Nu
        {
            get { return _Com_IGV_Nu; }
            set { _Com_IGV_Nu = value; }
        }

        public decimal Com_SubTotal_Nu
        {
            get { return _Com_SubTotal_Nu; }
            set { _Com_SubTotal_Nu = value; }
        }

        public decimal Com_Total_Nu
        {
            get { return _Com_Total_Nu; }
            set { _Com_Total_Nu = value; }
        }

        public DateTime Com_Emision_Fe
        {
            get { return _Com_Emision_Fe; }
            set { _Com_Emision_Fe = value; }
        }

        public String Comprobante_Co
        {
            get { return _Comprobante_Co; }
            set { _Comprobante_Co = value; }
        }

        public String Com_Guia_Remision_Nu
        {
            get { return _Com_Guia_Remision_Nu; }
            set { _Com_Guia_Remision_Nu = value; }
        }

        public String Prv_RazonSocial_Tx
        {
            get { return _Prv_RazonSocial_Tx; }
            set { _Prv_RazonSocial_Tx = value; }
        }

        public String Est_Nombre_Tx
        {
            get { return _Est_Nombre_Tx; }
            set { _Est_Nombre_Tx = value; }
        }

        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }

        public DateTime? FechaDesde
        {
            get { return _FechaDesde; }
            set { _FechaDesde = value; }
        }

        public DateTime? FechaHasta
        {
            get { return _FechaHasta; }
            set { _FechaHasta = value; }
        }

        public String Prv_Direccion_tx
        {
            get { return _Prv_Direccion_tx; }
            set { _Prv_Direccion_tx = value; }
        }

        public String Prv_DocIdent_nu
        {
            get { return _Prv_DocIdent_nu; }
            set { _Prv_DocIdent_nu = value; }
        }
    }
}
