﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class PresupuestoBE : AuditoriaBE
    {
        private int _Presupuesto_Id;        
        private int _Proyecto_Id;
        private String _Pry_Nombre_Tx;        
        private int _Pre_Estado_Id;
        private String _Estado_Tx;        
        private int _Pre_Version_Nu;
        private String _Pre_Descripcion_Tx;
        private String _Vigente_Tx;        


        public int Presupuesto_Id
        {
            get { return _Presupuesto_Id; }
            set { _Presupuesto_Id = value; }
        }

        public int Proyecto_Id
        {
            get { return _Proyecto_Id; }
            set { _Proyecto_Id = value; }
        }

        public String Pry_Nombre_Tx
        {
            get { return _Pry_Nombre_Tx; }
            set { _Pry_Nombre_Tx = value; }
        }

        public int Pre_Estado_Id
        {
            get { return _Pre_Estado_Id; }
            set { _Pre_Estado_Id = value; }
        }

        public String Estado_Tx
        {
            get { return _Estado_Tx; }
            set { _Estado_Tx = value; }
        }

        public int Pre_Version_Nu
        {
            get { return _Pre_Version_Nu; }
            set { _Pre_Version_Nu = value; }
        }

        public String Pre_Descripcion_Tx
        {
            get { return _Pre_Descripcion_Tx; }
            set { _Pre_Descripcion_Tx = value; }
        }

        public String Vigente_Tx
        {
            get { return _Vigente_Tx; }
            set { _Vigente_Tx = value; }
        }
    }
}
