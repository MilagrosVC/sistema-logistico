﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ValorizacionDeBE: AuditoriaBE
    {
        private int _Valorizacion_De_Id;
        private int _Valorizacion_Id;
        private int _Item_Id;
        private String _VDe_Descripcion_Tx;
        private int _Unidad_Medida_Id;
        private decimal? _Itm_Precio_Unitario_Nu;
        private decimal _Itm_Parcial_Nu;
        private decimal _Itm_Metrado_Nu;
        private decimal? _VDe_Metrado_Nu;
        private decimal? _VDe_Monto_Nu;
        private decimal _VDe_Porcentaje_Nu;

        //-------------
        private String _Itm_Co;
        private String _UMe_Simbolo_Tx;
 
        public int Valorizacion_De_Id
        {
            get { return _Valorizacion_De_Id; }
            set { _Valorizacion_De_Id = value; }
        }
        public int Valorizacion_Id
        {
            get { return _Valorizacion_Id; }
            set { _Valorizacion_Id = value; }
        }
        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }
        public String VDe_Descripcion_Tx
        {
            get { return _VDe_Descripcion_Tx; }
            set { _VDe_Descripcion_Tx = value; }
        }
        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }
        public decimal? Itm_Precio_Unitario_Nu
        {
            get { return _Itm_Precio_Unitario_Nu; }
            set { _Itm_Precio_Unitario_Nu = value; }
        }
        public decimal Itm_Parcial_Nu
        {
            get { return _Itm_Parcial_Nu; }
            set { _Itm_Parcial_Nu = value; }
        }
        public decimal Itm_Metrado_Nu
        {
            get { return _Itm_Metrado_Nu; }
            set { _Itm_Metrado_Nu = value; }
        }
        public decimal? VDe_Metrado_Nu
        {
            get { return _VDe_Metrado_Nu; }
            set { _VDe_Metrado_Nu = value; }
        }
        public decimal? VDe_Monto_Nu
        {
            get { return _VDe_Monto_Nu; }
            set { _VDe_Monto_Nu = value; }
        }
        public decimal VDe_Porcentaje_Nu
        {
            get { return _VDe_Porcentaje_Nu; }
            set { _VDe_Porcentaje_Nu = value; }
        }

        //-----------------
        public String Itm_Co
        {
            get { return _Itm_Co; }
            set { _Itm_Co = value; }
        }
        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }
    }
}
