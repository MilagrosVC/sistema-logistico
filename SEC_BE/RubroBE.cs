﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class RubroBE:AuditoriaBE
    {
        private int _Rubro_Id;
        private String _Rub_Nombre_Tx;
        private String _Rub_Descripcion_Tx;

        public int Rubro_Id 
        {
            get { return _Rubro_Id; } 
            set { _Rubro_Id = value; } 
         }
        public String Rub_Nombre_Tx 
        { 
            get { return _Rub_Nombre_Tx; } 
            set { _Rub_Nombre_Tx = value; } 
        }
        public String Rub_Descripcion_Tx 
        { 
            get { return _Rub_Descripcion_Tx; } 
            set { _Rub_Descripcion_Tx = value; } 
        }
    }
}
