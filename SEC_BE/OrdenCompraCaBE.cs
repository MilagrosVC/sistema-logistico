﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class OrdenCompraCaBE : AuditoriaBE
    {
        private int _Orden_Compra_Id;        
        private int _Cotizacion_Id;        
        private int _Proveedor_Id;        
        private String _OCC_Gestionador_Tx;        
        private int _OCC_Tipo_Id;        
        private int _OCC_Estado_Id;        
        private int _OCC_Plazo_Entrega_Id;        
        private int _OCC_Forma_Pago_Id;        
        private int _Moneda_Id;        
        private int _OCC_Tipo_Documento_Entrega;        
        private int _OCC_Tipo_Documento_Cancelacion;        
        private Boolean _OCC_Penalidad_Fl;        
        private decimal _OCC_Total_Nu;        
        private DateTime _OCC_Emision_Fe;        
        private decimal _OCC_Tipo_Cambio;        
        private int _Centro_Costo_Id;
        private String _Numero;
        private String _Fecha_Fe;
        private decimal _OCC_Penalidad_Nu;
        private decimal _OCC_IGV_Nu;
        private decimal _OCC_SubTotal_Nu;
        private String _Prv_RazonSocial_Tx;
        private String _Prv_Direccion_tx;
        private String _Orden_Compra_Co;
        private String _Prv_Telefono1_nu;
        private String _Distrito_Tx;        
        private String _Prv_DocIdent_nu;
        private Boolean _Anticipo_Fl;
        private String _Cot_NumProforma_Nu;
        private String _Est_Nombre_Tx;
        private String _Mon_Nombre_Tx;
        private DateTime? _FechaDesde;
        private DateTime? _FechaHasta;
        private String _Par_Nombre_Tx;
        public String Par_Nombre_Tx
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }
        public int Orden_Compra_Id
        {
            get { return _Orden_Compra_Id; }
            set { _Orden_Compra_Id = value; }
        }
        public int Cotizacion_Id
        {
            get { return _Cotizacion_Id; }
            set { _Cotizacion_Id = value; }
        }

        public int Proveedor_Id
        {
            get { return _Proveedor_Id; }
            set { _Proveedor_Id = value; }
        }

        public String OCC_Gestionador_Tx
        {
            get { return _OCC_Gestionador_Tx; }
            set { _OCC_Gestionador_Tx = value; }
        }

        public int OCC_Tipo_Id
        {
            get { return _OCC_Tipo_Id; }
            set { _OCC_Tipo_Id = value; }
        }

        public int OCC_Estado_Id
        {
            get { return _OCC_Estado_Id; }
            set { _OCC_Estado_Id = value; }
        }

        public int OCC_Plazo_Entrega_Id
        {
            get { return _OCC_Plazo_Entrega_Id; }
            set { _OCC_Plazo_Entrega_Id = value; }
        }

        public int OCC_Forma_Pago_Id
        {
            get { return _OCC_Forma_Pago_Id; }
            set { _OCC_Forma_Pago_Id = value; }
        }

        public int Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }

        public int OCC_Tipo_Documento_Entrega
        {
            get { return _OCC_Tipo_Documento_Entrega; }
            set { _OCC_Tipo_Documento_Entrega = value; }
        }

        public int OCC_Tipo_Documento_Cancelacion
        {
            get { return _OCC_Tipo_Documento_Cancelacion; }
            set { _OCC_Tipo_Documento_Cancelacion = value; }
        }

        public Boolean OCC_Penalidad_Fl
        {
            get { return _OCC_Penalidad_Fl; }
            set { _OCC_Penalidad_Fl = value; }
        }

        public DateTime OCC_Emision_Fe
        {
            get { return _OCC_Emision_Fe; }
            set { _OCC_Emision_Fe = value; }
        }

        public decimal OCC_Total_Nu
        {
            get { return _OCC_Total_Nu; }
            set { _OCC_Total_Nu = value; }
        }

        public decimal OCC_Tipo_Cambio
        {
            get { return _OCC_Tipo_Cambio; }
            set { _OCC_Tipo_Cambio = value; }
        }

        public int Centro_Costo_Id
        {
            get { return _Centro_Costo_Id; }
            set { _Centro_Costo_Id = value; }
        }

        public String Numero
        {
            get { return _Numero; }
            set { _Numero = value; }
        }

        public String Fecha_Fe
        {
            get { return _Fecha_Fe; }
            set { _Fecha_Fe = value; }
        }

        public decimal OCC_Penalidad_Nu
        {
            get { return _OCC_Penalidad_Nu; }
            set { _OCC_Penalidad_Nu = value; }
        }

        public decimal OCC_IGV_Nu
        {
            get { return _OCC_IGV_Nu; }
            set { _OCC_IGV_Nu = value; }
        }

        public decimal OCC_SubTotal_Nu
        {
            get { return _OCC_SubTotal_Nu; }
            set { _OCC_SubTotal_Nu = value; }
        }

        public String Prv_RazonSocial_Tx
        {
            get { return _Prv_RazonSocial_Tx; }
            set { _Prv_RazonSocial_Tx = value; }
        }

        public String Prv_Direccion_tx
        {
            get { return _Prv_Direccion_tx; }
            set { _Prv_Direccion_tx = value; }
        }

        public String Orden_Compra_Co
        {
            get { return _Orden_Compra_Co; }
            set { _Orden_Compra_Co = value; }
        }

        public String Prv_Telefono1_nu
        {
            get { return _Prv_Telefono1_nu; }
            set { _Prv_Telefono1_nu = value; }
        }

        public String Distrito_Tx
        {
            get { return _Distrito_Tx; }
            set { _Distrito_Tx = value; }
        }

        public String Prv_DocIdent_nu
        {
            get { return _Prv_DocIdent_nu; }
            set { _Prv_DocIdent_nu = value; }
        }

        public Boolean Anticipo_Fl
        {
            get { return _Anticipo_Fl; }
            set { _Anticipo_Fl = value; }
        }

        public String Cot_NumProforma_Nu
        {
            get { return _Cot_NumProforma_Nu; }
            set { _Cot_NumProforma_Nu = value; }
        }

        public String Est_Nombre_Tx
        {
            get { return _Est_Nombre_Tx; }
            set { _Est_Nombre_Tx = value; }
        }

        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }

        public DateTime? FechaDesde
        {
            get { return _FechaDesde; }
            set { _FechaDesde = value; }
        }

        public DateTime? FechaHasta
        {
            get { return _FechaHasta; }
            set { _FechaHasta = value; }
        }
    }
}
