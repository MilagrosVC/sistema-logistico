﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class SolicitudDeBE : AuditoriaBE
    {
        private int? _Solicitud_Detalle_Id;        
        private int _Solicitud_Id;        
        private int _Item_Id;        
        private int _SDe_Cantidad_Nu;        
        private String _SDe_Descripcion_Tx;        
        private int _Unidad_Medida_Id;
        private int _Requerimiento_Detalle_Id;

        /* Visualizar detalles de solicitud */
        private String _RDe_Descripcion_Tx;
        private String _UMe_Simbolo_Tx;
        private int _RDe_Estado_Id;
        private String _SDe_Estado_Tx;
        private String _Itm_Co;
        
        public String RDe_Descripcion_Tx {
            get { return _RDe_Descripcion_Tx; }
            set { _RDe_Descripcion_Tx = value; }
        }
        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }
        public int RDe_Estado_Id
        {
            get { return _RDe_Estado_Id; }
            set { _RDe_Estado_Id = value; }
        }
        public String SDe_Estado_Tx
        {
            get { return _SDe_Estado_Tx; }
            set { _SDe_Estado_Tx = value; }
        }
        public String Itm_Co
        {
            get { return _Itm_Co; }
            set { _Itm_Co = value; }
        }
        /*------------------------------------*/

        public int? Solicitud_Detalle_Id
        {
            get { return _Solicitud_Detalle_Id; }
            set { _Solicitud_Detalle_Id = value; }
        }

        public int Solicitud_Id
        {
            get { return _Solicitud_Id; }
            set { _Solicitud_Id = value; }
        }

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public int SDe_Cantidad_Nu
        {
            get { return _SDe_Cantidad_Nu; }
            set { _SDe_Cantidad_Nu = value; }
        }

        public String SDe_Descripcion_Tx
        {
            get { return _SDe_Descripcion_Tx; }
            set { _SDe_Descripcion_Tx = value; }
        }

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }
        public int Requerimiento_Detalle_Id
        {
            get { return _Requerimiento_Detalle_Id; }
            set { _Requerimiento_Detalle_Id = value; }
        }
    }
}
