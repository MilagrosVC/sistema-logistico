﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class UndMedidaBE
    {
        private int _Unidad_Medida_Id;        
        private String _UMe_Nombre_Tx;        
        private String _UMe_Simbolo_Tx;        
        private String _UMe_Descripcion_Tx;       

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }

        public String UMe_Nombre_Tx
        {
            get { return _UMe_Nombre_Tx; }
            set { _UMe_Nombre_Tx = value; }
        }

        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }

        public String UMe_Descripcion_Tx
        {
            get { return _UMe_Descripcion_Tx; }
            set { _UMe_Descripcion_Tx = value; }
        }
    }
}
