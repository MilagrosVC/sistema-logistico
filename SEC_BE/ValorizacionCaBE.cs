﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ValorizacionCaBE: AuditoriaBE
    {
        private int _Valorizacion_Id;
        private int _Partida_Id;
        private String _Val_Obra_Tx;
        private int _Cliente_Id;
        private DateTime _Val_Fe;
        private int _Ejecuta_Id;
        private int _Periodicidad_Id;
        private int _Val_Periodos_Nu;
        private int _Val_Periodo_Id;
        private String _Val_Descripcion_Tx;

        //----------------//
        private String _NomPartida;
        private String _NomCliente;
        private String _NomProveedor;


        public int Valorizacion_Id { get { return _Valorizacion_Id; } set { _Valorizacion_Id = value; } }
        public int Partida_Id { get { return _Partida_Id; } set { _Partida_Id = value; } }
        public String Val_Obra_Tx { get { return _Val_Obra_Tx; } set { _Val_Obra_Tx = value; } }
        public int Cliente_Id { get { return _Cliente_Id; } set { _Cliente_Id = value; } }
        public DateTime Val_Fe { get { return _Val_Fe; } set { _Val_Fe = value; } }
        public int Ejecuta_Id { get { return _Ejecuta_Id; } set { _Ejecuta_Id = value; } }
        public int Periodicidad_Id { get { return _Periodicidad_Id; } set { _Periodicidad_Id = value; } }
        public int Val_Periodos_Nu { get { return _Val_Periodos_Nu; } set { _Val_Periodos_Nu = value; } }
        public int Val_Periodo_Id { get { return _Val_Periodo_Id; } set { _Val_Periodo_Id = value; } }
        public String Val_Descripcion_Tx { get { return _Val_Descripcion_Tx; } set { _Val_Descripcion_Tx = value; } }

        //----------------//
        public String NomPartida { get { return _NomPartida; } set { _NomPartida = value; } }
        public String NomCliente { get { return _NomCliente; } set { _NomCliente = value; } }
        public String NomProveedor { get { return _NomProveedor; } set { _NomProveedor = value; } }
    }
}
