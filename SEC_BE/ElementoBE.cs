﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ElementoBE : AuditoriaBE
    {
        private int _Elemento_Id;
        private int _Ele_Co;
        private String _Ele_Nombre_Tx;
        private int _TipoCuenta_Id;
        private String _Elemento_Tx;
        public int Elemento_Id 
        { 
            get { return _Elemento_Id; } 
            set { _Elemento_Id = value; } 
        }
        public int Ele_Co 
        { 
            get { return _Ele_Co; } 
            set { _Ele_Co = value; } 
        }
        public String Ele_Nombre_Tx { 
            get { return _Ele_Nombre_Tx; } 
            set { _Ele_Nombre_Tx = value; } 
        }
        public int TipoCuenta_Id { 
            get { return _TipoCuenta_Id; } 
            set { _TipoCuenta_Id = value; } 
        }

        public String Elemento_Tx
        {
            get { return _Elemento_Tx; }
            set { _Elemento_Tx = value; }
        }
    }
}
