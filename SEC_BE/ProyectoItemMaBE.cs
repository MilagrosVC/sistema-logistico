﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
   public class ProyectoItemMaBE : AuditoriaBE
    {
        private int _Ite_Pro_Id;
        private int _Proyecto_Id;
        private int _IMa_Id;
        public int Ite_Pro_Id 
        { 
            get { return _Ite_Pro_Id; } 
            set { _Ite_Pro_Id = value; } 
        }
        public int Proyecto_Id 
        { 
            get { return _Proyecto_Id; } 
            set { _Proyecto_Id = value; } 
        }
        public int IMa_Id 
        { 
            get { return _IMa_Id; } 
            set { _IMa_Id = value; } 
        }
       //Nuevos Campos que se usaran en la grilla
       private String _IMa_Nombre_Tx;
        private String _IMa_Descripcion_Tx;
        private int _Rubro_Item_Id;
        private decimal? _IMa_Metrado_Nu;
        private int _IMa_Padre_Id;
        private int _Unidad_Medida_Id;
        private int? _Familia_Id;
        private int? _Marca_Id;
        private String _IMa_Co;
        private String _UMe_Simbolo_Tx;
        private String _Fam_Nombre_Tx;
        private String _Mar_Nombre_Tx;
        private String _IMa_Metrado_tx;
        private int? _Ite_Pro_Stock;

        public int? Ite_Pro_Stock
        {
            get { return _Ite_Pro_Stock; }
            set { _Ite_Pro_Stock = value; }
        }


        private int? _Ite_Pro_Stock_Min;

        public int? Ite_Pro_Stock_Min
        {
            get { return _Ite_Pro_Stock_Min; }
            set { _Ite_Pro_Stock_Min = value; }
        }
        public String IMa_Nombre_Tx
        {
            get { return _IMa_Nombre_Tx; }
            set { _IMa_Nombre_Tx = value; }
        }

        public String IMa_Descripcion_Tx
        {
            get { return _IMa_Descripcion_Tx; }
            set { _IMa_Descripcion_Tx = value; }
        }

        public int Rubro_Item_Id
        {
            get { return _Rubro_Item_Id; }
            set { _Rubro_Item_Id = value; }
        }

        public decimal? IMa_Metrado_Nu
        {
            get { return _IMa_Metrado_Nu; }
            set { _IMa_Metrado_Nu = value; }
        }

        public int IMa_Padre_Id
        {
            get { return _IMa_Padre_Id; }
            set { _IMa_Padre_Id = value; }
        }

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }

        public int? Familia_Id
        {
            get { return _Familia_Id; }
            set { _Familia_Id = value; }
        }

        public int? Marca_Id
        {
            get { return _Marca_Id; }
            set { _Marca_Id = value; }
        }

        public String IMa_Co
        {
            get { return _IMa_Co; }
            set { _IMa_Co = value; }
        }

        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }

        public String Fam_Nombre_Tx
        {
            get { return _Fam_Nombre_Tx; }
            set { _Fam_Nombre_Tx = value; }
        }

        public String Mar_Nombre_Tx
        {
            get { return _Mar_Nombre_Tx; }
            set { _Mar_Nombre_Tx = value; }
        }


        public String IMa_Metrado_tx
        {
            get { return _IMa_Metrado_tx; }
            set { _IMa_Metrado_tx = value; }
        }
    }
}
