﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class SolicitudAnticipoBE : AuditoriaBE

    {
        private int _Solicitud_Anticipo_Id;
        private int _Cotizacion_Id;
        private int _Moneda_Id;
        private decimal _Sol_Monto_Solicitado_nu;
        private int _FormaPago_Id;
        private int _Sol_Tipo_Anticipo_Id;
        private int _Estado_Solicitud_Id;
        //Nuevo 01 Septiembre
        private int _Per_Id;
        private String _Sol_Observacion_tx;

        private String _Cot_NumProforma_Nu;

        public String Cot_NumProforma_Nu
        {
            get { return _Cot_NumProforma_Nu; }
            set { _Cot_NumProforma_Nu = value; }
        }

        public String Sol_Observacion_tx {
            get { return _Sol_Observacion_tx; }
            set { _Sol_Observacion_tx = value; }
        }

        public int Per_Id
        {
            get { return _Per_Id; }
            set { _Per_Id = value; }
        }
        
       //-

        public int Solicitud_Anticipo_Id 
        { 
            get { return _Solicitud_Anticipo_Id; } 
            set { _Solicitud_Anticipo_Id = value; } 
        }
        public int Cotizacion_Id 
        { 
            get { return _Cotizacion_Id; } 
            set { _Cotizacion_Id = value; } 
        }
        public int Moneda_Id 
        { 
            get { return _Moneda_Id; } 
            set { _Moneda_Id = value; } 
        }
        public decimal Sol_Monto_Solicitado_nu 
        { 
            get { return _Sol_Monto_Solicitado_nu; } 
            set { _Sol_Monto_Solicitado_nu = value; } 
        }
        public int FormaPago_Id 
        { 
            get { return _FormaPago_Id; } 
            set { _FormaPago_Id = value; } 
        }
        public int Sol_Tipo_Anticipo_Id 
        { 
            get { return _Sol_Tipo_Anticipo_Id; } 
            set { _Sol_Tipo_Anticipo_Id = value; } 
        }
        public int Estado_Solicitud_Id 
        { 
            get { return _Estado_Solicitud_Id; } 
            set { _Estado_Solicitud_Id = value; } 
        }

        //Parametros de diferentes tablas
        private String _Mon_Nombre_Tx; //Moneda
        private String _TMP_Nombre_Tx; //Tipo Pago
        private String _Par_Nombre_Tx; //Tipo Anticipo
        private String _NombreProveedor_Tx;
        /// <summary>
        /// TIPO PROVEEDOR
        /// </summary>
        private int _TipoPersona_Id;
        private String _TipoPersona_TXx;
        //Moneda Monto
        private String _Moneda_Monto_Tx;
        public String Mon_Nombre_Tx
        {
            get { return _Mon_Nombre_Tx; }
            set { _Mon_Nombre_Tx = value; }
        }
        public String TMP_Nombre_Tx
        {
            get { return _TMP_Nombre_Tx; }
            set { _TMP_Nombre_Tx = value; }
        }
        public String Par_Nombre_Tx
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }
        public String NombreProveedor_Tx
        {
            get { return _NombreProveedor_Tx; }
            set { _NombreProveedor_Tx = value; }
        }

        ////NombreNatural
        //private String _NombreNatural;
        //public String NombreNatural
        //{
        //    get { return _NombreNatural; }
        //    set { _NombreNatural = value; }
        //} TipoPersona_Id;
        

         public int TipoPersona_Id
        {
            get { return _TipoPersona_Id; }
            set { _TipoPersona_Id = value; }
        }
         public String TipoPersona_TXx
        {
            get { return _TipoPersona_TXx; }
            set { _TipoPersona_TXx = value; }
        }
         public String Moneda_Monto_Tx
         {
             get { return _Moneda_Monto_Tx; }
             set { _Moneda_Monto_Tx = value; }
         }
    }
}
