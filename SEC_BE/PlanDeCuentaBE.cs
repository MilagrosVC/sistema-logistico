﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class PlanDeCuentaBE : AuditoriaBE
    {
        private int _PlanCuenta_Id;
        private String _Pla_Nombre_Tx;
        private String _Pla_Descripcion_Tx;
        private int _Empresa_Id;

        public int PlanCuenta_Id 
        { 
            get { return _PlanCuenta_Id; } set 
            { _PlanCuenta_Id = value; } 
        }
        public String Pla_Nombre_Tx 
        { 
            get { return _Pla_Nombre_Tx; } 
            set { _Pla_Nombre_Tx = value; } 
        }
        public String Pla_Descripcion_Tx 
        { 
            get { return _Pla_Descripcion_Tx; } 
            set { _Pla_Descripcion_Tx = value; } 
        }
        public int Empresa_Id 
        { 
            get { return _Empresa_Id; } 
            set { _Empresa_Id = value; } 
        }

        /* Datos Adicionales para la seleccion de Plan de Cuenta*/
       private String _Emp_RazonSocial_Tx;
       private String _Emp_DocIdent_RUC_nu;
       //private String _PC_FechaCreacion;
       private String _PC_HoraCreacion;
       private DateTime _PC_FechaCreacion;
       //private DateTime _PC_HoraCreacion;


       public String Emp_RazonSocial_Tx
       {
           get { return _Emp_RazonSocial_Tx; }
           set { _Emp_RazonSocial_Tx = value; }
       }
       public String Emp_DocIdent_RUC_nu
       {
           get { return _Emp_DocIdent_RUC_nu; }
           set { _Emp_DocIdent_RUC_nu = value; }
       }
       //public String PC_FechaCreacion
       //{
       //    get { return _PC_FechaCreacion; }
       //    set { _PC_FechaCreacion = value; }
       //}
       
       public DateTime PC_FechaCreacion
       {
           get { return _PC_FechaCreacion; }
           set { _PC_FechaCreacion = value; }
       }
       public String PC_HoraCreacion
       {
           get { return _PC_HoraCreacion; }
           set { _PC_HoraCreacion = value; }
       }
       //public DateTime PC_HoraCreacion
       //{
       //    get { return _PC_HoraCreacion; }
       //    set { _PC_HoraCreacion = value; }
       //}
    }
}
