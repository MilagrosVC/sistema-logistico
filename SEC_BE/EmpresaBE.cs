﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class EmpresaBE : AuditoriaBE
    {
        private int _Empresa_Id;
        private String _Emp_RazonSocial_Tx;
        private String _Emp_Personacontacto_Tx;

        public String Emp_Personacontacto_Tx
        {
            get { return _Emp_Personacontacto_Tx; }
            set { _Emp_Personacontacto_Tx = value; }
        }

        public int Empresa_Id
        {
            get { return _Empresa_Id; }
            set { _Empresa_Id = value; }
        }

        public String Emp_RazonSocial_Tx
        {
            get { return _Emp_RazonSocial_Tx; }
            set { _Emp_RazonSocial_Tx = value; }
        }
    }
}
