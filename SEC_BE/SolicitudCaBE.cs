﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class SolicitudCaBE : AuditoriaBE
    {
        private int _Solicitud_Id;        
        private String _Solicitante_Id;
        private int _Centro_Costo_Id;        
        private DateTime? _Sol_Entrega_Fe;        
        private int _Sol_Estado_Id;        
        private int _Sol_Tipo_Id;       
        /*private int _SDe_Cantidad_Nu;        
        private String _SDe_Descripcion_Tx;        
        private int _Unidad_Medida_Id;        
        private String _Sol_Ingreso;*/
        private int _Requerimiento_Id;
        private String _Sol_Observacion_Tx;
        
        //-----------//
        private String _ItemsxSol;
        private String _Lst_Proveedor;

        private String _Sol_Solicitante_Tx;
        private int _Sol_Cant_Items;
        private String _Sol_Estado_Tx;

        public String Lst_Proveedor
        {
            get { return _Lst_Proveedor; }
            set { _Lst_Proveedor = value; }
        }
        public String ItemsxSol
        {
            get { return _ItemsxSol; }
            set { _ItemsxSol = value; }
        }
        public String Sol_Solicitante_Tx
        {
            get { return _Sol_Solicitante_Tx; }
            set { _Sol_Solicitante_Tx = value; }
        }
        public int Sol_Cant_Items
        {
            get { return _Sol_Cant_Items; }
            set { _Sol_Cant_Items = value; }
        }
        public String Sol_Estado_Tx
        {
            get { return _Sol_Estado_Tx; }
            set { _Sol_Estado_Tx = value; }
        }


        //-----------//

        public int Solicitud_Id
        {
            get { return _Solicitud_Id; }
            set { _Solicitud_Id = value; }
        }

        public String Solicitante_Id
        {
            get { return _Solicitante_Id; }
            set { _Solicitante_Id = value; }
        }

        public int Centro_Costo_Id
        {
            get { return _Centro_Costo_Id; }
            set { _Centro_Costo_Id = value; }
        }

        public DateTime? Sol_Entrega_Fe
        {
            get { return _Sol_Entrega_Fe; }
            set { _Sol_Entrega_Fe = value; }
        }

        public int Sol_Estado_Id
        {
            get { return _Sol_Estado_Id; }
            set { _Sol_Estado_Id = value; }
        }

        public int Sol_Tipo_Id
        {
            get { return _Sol_Tipo_Id; }
            set { _Sol_Tipo_Id = value; }
        }

        /*public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public int SDe_Cantidad_Nu
        {
            get { return _SDe_Cantidad_Nu; }
            set { _SDe_Cantidad_Nu = value; }
        }

        public String SDe_Descripcion_Tx
        {
            get { return _SDe_Descripcion_Tx; }
            set { _SDe_Descripcion_Tx = value; }
        }

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }

        public String Sol_Ingreso
        {
            get { return _Sol_Ingreso; }
            set { _Sol_Ingreso = value; }
        }*/
        public int Requerimiento_Id
        {
            get { return _Requerimiento_Id; }
            set { _Requerimiento_Id = value; }
        }

        public String Sol_Observacion_Tx
        {
            get { return _Sol_Observacion_Tx; }
            set { _Sol_Observacion_Tx = value; }
        }
    }
}
