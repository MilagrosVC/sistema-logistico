﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class FamiliaBE
    {
        private int _Familia_Id;
        private String _Fam_Nombre_Tx;
        private String _Fam_Descripcion_Tx;        

        public int Familia_Id
        {
            get { return _Familia_Id; }
            set { _Familia_Id = value; }
        }

        public String Fam_Nombre_Tx
        {
            get { return _Fam_Nombre_Tx; }
            set { _Fam_Nombre_Tx = value; }
        }

        public String Fam_Descripcion_Tx
        {
            get { return _Fam_Descripcion_Tx; }
            set { _Fam_Descripcion_Tx = value; }
        }
    }
}
