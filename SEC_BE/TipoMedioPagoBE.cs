﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class TipoMedioPagoBE:AuditoriaBE
    {
        private int _Tipo_Medio_Pago_Id;
        private String _TMP_Nombre_Tx;
        private String _TMP_Co;

        public int Tipo_Medio_Pago_Id 
        { 
            get { return _Tipo_Medio_Pago_Id; } 
            set { _Tipo_Medio_Pago_Id = value; } 
        }
        public String TMP_Nombre_Tx 
        { 
            get { return _TMP_Nombre_Tx; } 
            set { _TMP_Nombre_Tx = value; } 
        }
        public String TMP_Co 
        { 
            get { return _TMP_Co; } 
            set { _TMP_Co = value; } 
        }

    }
}
