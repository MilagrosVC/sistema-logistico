﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ProyectoBE : AuditoriaBE
    {
        private int _Proyecto_Id;        
        private int _Empresa_Id;
        private String _Emp_RazonSocial_Tx;        
        private String _Pry_Nombre_Tx;
        private String _Pry_Descripcion_Tx;
        private String _Pry_Direccion_Tx;        
        private String _Pry_Desde_Fe;
        private String _Pry_Hasta_Fe;        
        private int _Moneda_Id;        
        private decimal _Pry_Monto_Nu;
        private String _Emp_DocIdent_RUC_nu;


        public String Emp_DocIdent_RUC_nu
        {
            get { return _Emp_DocIdent_RUC_nu; }
            set { _Emp_DocIdent_RUC_nu = value; }
        }

        public int Proyecto_Id
        {
            get { return _Proyecto_Id; }
            set { _Proyecto_Id = value; }
        }

        public int Empresa_Id
        {
            get { return _Empresa_Id; }
            set { _Empresa_Id = value; }
        }

        public String Emp_RazonSocial_Tx
        {
            get { return _Emp_RazonSocial_Tx; }
            set { _Emp_RazonSocial_Tx = value; }
        }

        public String Pry_Nombre_Tx
        {
            get { return _Pry_Nombre_Tx; }
            set { _Pry_Nombre_Tx = value; }
        }

        public String Pry_Descripcion_Tx
        {
            get { return _Pry_Descripcion_Tx; }
            set { _Pry_Descripcion_Tx = value; }
        }

        public String Pry_Desde_Fe
        {
            get { return _Pry_Desde_Fe; }
            set { _Pry_Desde_Fe = value; }
        }

        public String Pry_Hasta_Fe
        {
            get { return _Pry_Hasta_Fe; }
            set { _Pry_Hasta_Fe = value; }
        }

        public int Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }

        public decimal Pry_Monto_Nu
        {
            get { return _Pry_Monto_Nu; }
            set { _Pry_Monto_Nu = value; }
        }

        public String Pry_Direccion_Tx
        {
            get { return _Pry_Direccion_Tx; }
            set { _Pry_Direccion_Tx = value; }
        }



        //-----------------Otros-------------------//
        private int _Departamento_Co;
        private int _Provincia_co;
        private int _Distrito_co;
        private String _NombreProveedor_Tx;
        private String _Distrito_Tx;





        public int Departamento_Co
        {
            get { return _Departamento_Co; }
            set { _Departamento_Co = value; }
        }
        public int Provincia_co
        {
            get { return _Provincia_co; }
            set { _Provincia_co = value; }
        }
        public int Distrito_co
        {
            get { return _Distrito_co; }
            set { _Distrito_co = value; }
        }

        public String NombreProveedor_Tx
        {
            get { return _NombreProveedor_Tx; }
            set { _NombreProveedor_Tx = value; }
        }

        public String Distrito_Tx
        {
            get { return _Distrito_Tx; }
            set { _Distrito_Tx = value; }
        }
    }
}
