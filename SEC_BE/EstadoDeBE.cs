﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class EstadoDeBE: AuditoriaBE
    {
        private int _Estado_Id;
        private int _Estado_CA_Id;
        private int _Est_Id;
        private String _Est_Nombre_Tx;

        public int Estado_Id { 
            get { return _Estado_Id; } 
            set { _Estado_Id = value; } 
        }
        public int Estado_CA_Id { 
            get { return _Estado_CA_Id; } 
            set { _Estado_CA_Id = value; } 
        }
        public int Est_Id { 
            get { return _Est_Id; } 
            set { _Est_Id = value; } 
        }
        public String Est_Nombre_Tx { 
            get { return _Est_Nombre_Tx; } 
            set { _Est_Nombre_Tx = value; } 
        }
    }
}
