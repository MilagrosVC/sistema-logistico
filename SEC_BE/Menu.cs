﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class MenuBE:PerfilDetalleBE
    {
        private bool _Hijo_Fl;
        private bool _Cargado_Fl;
        private int _N_Hijos;

        public bool Hijo_Fl
        { 
            get { return _Hijo_Fl; }
            set { _Hijo_Fl = value; }
        }
        public bool Cargado_Fl
        {
            get { return _Cargado_Fl; }
            set { _Cargado_Fl = value; }
        }

        public int N_Hijos
        {
            get { return _N_Hijos; }
            set { _N_Hijos = value; }
        }
    }
}
