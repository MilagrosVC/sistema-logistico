﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ItemBE : AuditoriaBE
    {
        private int _Item_Id;
        private String _Itm_Co;
        private int _Sub_Partida_Id;
        private String _Itm_Nombre_Tx;        
        private String _Codigo;
        private String _Descripcion;        
        private int _Und;        
        private decimal _Metrado;
        private decimal _Precio;        
        private decimal _Parcial;        

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public String Itm_Co
        {
            get { return _Itm_Co; }
            set { _Itm_Co = value; }
        }

        public int Sub_Partida_Id
        {
            get { return _Sub_Partida_Id; }
            set { _Sub_Partida_Id = value; }
        }

        public String Itm_Nombre_Tx
        {
            get { return _Itm_Nombre_Tx; }
            set { _Itm_Nombre_Tx = value; }
        }

        public String Codigo
        {
            get { return _Codigo; }
            set { _Codigo = value; }
        }

        public String Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }

        public int Und
        {
            get { return _Und; }
            set { _Und = value; }
        }

        public decimal Metrado
        {
            get { return _Metrado; }
            set { _Metrado = value; }
        }

        public decimal Precio
        {
            get { return _Precio; }
            set { _Precio = value; }
        }

        public decimal Parcial
        {
            get { return _Parcial; }
            set { _Parcial = value; }
        }

    }
}
