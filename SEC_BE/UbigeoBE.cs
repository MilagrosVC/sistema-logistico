﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class UbigeoBE : AuditoriaBE
    {
        private int? _Ubigeo_Id;
        private String _Ubi_Nombre_Tx;
        private int? _Departamento_Co;
        private int? _Provincia_co;
        private int? _Distrito_co;
        private String _Ubigeo_Co;
        
        //-- Alias manejado en SP [SEC_PRC_S_UBIGEO] --// 
        private String _Nombre;
        public String Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        //---------------------------------------------//

        public int? Ubigeo_Id
        {
            get { return _Ubigeo_Id; }
            set { _Ubigeo_Id = value; }
        }

        public String Ubi_Nombre_Tx
        {
            get { return _Ubi_Nombre_Tx; }
            set { _Ubi_Nombre_Tx = value; }
        }

        public int? Departamento_Co
        {
            get { return _Departamento_Co; }
            set { _Departamento_Co = value; }
        }
        public int? Provincia_co
        {
            get { return _Provincia_co; }
            set { _Provincia_co = value; }
        }
        public int? Distrito_co
        {
            get { return _Distrito_co; }
            set { _Distrito_co = value; }
        }
        public String Ubigeo_Co
        {
            get { return _Ubigeo_Co; }
            set { _Ubigeo_Co = value; }
        }
    }
}