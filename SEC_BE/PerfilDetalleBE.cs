﻿using System;
//using System.Collections.Generic;

namespace SEC_BE
{
    public class PerfilDetalleBE : AuditoriaBE
    {

        private int _PDt_Id;
        private int _Per_Id;
        private int _Nav_Navegacion_Id;
        private bool _PDt_Nav_Default_Fl;
        private int _Nav_Nivel_Nu;

        public int Nav_Nivel_Nu
        {
            get { return _Nav_Nivel_Nu; }
            set { _Nav_Nivel_Nu = value; }
        }
        public int PDt_Id
        {
            get { return _PDt_Id; }
            set { _PDt_Id = value; }
        }
        public int Per_Id
        {
            get { return _Per_Id; }
            set { _Per_Id = value; }
        }
        public int Nav_Navegacion_Id
        {
            get { return _Nav_Navegacion_Id; }
            set { _Nav_Navegacion_Id = value; }
        }

        public bool PDt_Nav_Default_Fl
        {
            get { return _PDt_Nav_Default_Fl; }
            set { _PDt_Nav_Default_Fl = value; }
        }

        private string _Nav_Navegacion_Tx;
        private int _Nav_Padre_Id;
        private string _Nav_Link;
        public string Nav_Navegacion_Tx
        {
            get { return _Nav_Navegacion_Tx; }
            set { _Nav_Navegacion_Tx = value; }
        }
        public int Nav_Padre_Id
        {
            get { return _Nav_Padre_Id; }
            set { _Nav_Padre_Id = value; }
        }
        public string Nav_Link
        {
            get { return _Nav_Link; }
            set { _Nav_Link = value; }
        }
    }
}
