﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    class NavegacionBE
    {

        private int _Nav_Id;
        private string _Nav_Navegacion_Tx;
        private int _Nav_Padre_Id;
        private string _Nav_Link;
        private string _Nav_RutaImagen_Tx;
        private int _Nav_Orden_Nu;
        private bool _Nav_Habilitado_Fl;



        public int Nav_Id
        {
            get { return _Nav_Id; }
            set { _Nav_Id = value; }
        }
        public string Nav_Navegacion_Tx
        {
            get { return _Nav_Navegacion_Tx; }
            set { _Nav_Navegacion_Tx = value; }
        }
        public int Nav_Padre_Id
        {
            get { return _Nav_Padre_Id; }
            set { _Nav_Padre_Id = value; }
        }
        public string Nav_Link
        {
            get { return _Nav_Link; }
            set { _Nav_Link = value; }
        }
        public string Nav_RutaImagen_Tx
        {
            get { return _Nav_RutaImagen_Tx; }
            set { _Nav_RutaImagen_Tx = value; }
        }
        public int Nav_Orden_Nu
        {
            get { return _Nav_Orden_Nu; }
            set { _Nav_Orden_Nu = value; }
        }
        public bool Nav_Habilitado_Fl
        {
            get { return _Nav_Habilitado_Fl; }
            set { _Nav_Habilitado_Fl = value; }
        }

    }
}
