﻿using System;
using System.Collections.Generic;


namespace SEC_BE
{
    public class PerfilBE : AuditoriaBE
    {
        private int _Per_Id;

        private string _Per_Nombre_Tx;

        private string _Per_Descripcion_Tx;

        private int _Per_Jefe_Id;




        public int Per_Id
        {

            get { return _Per_Id; }

            set { _Per_Id = value; }

        }

        public string Per_Nombre_Tx
        {

            get { return _Per_Nombre_Tx; }

            set { _Per_Nombre_Tx = value; }

        }

        public string Per_Descripcion_Tx
        {

            get { return _Per_Descripcion_Tx; }

            set { _Per_Descripcion_Tx = value; }

        }

        public int Per_Jefe_Id
        {

            get { return _Per_Jefe_Id; }

            set { _Per_Jefe_Id = value; }

        }

      

 

    }
}
