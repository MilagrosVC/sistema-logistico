﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ParametroBE: AuditoriaBE
    {
        private int _Par_Id;
        private int _Par_Padre_Id;
        private String _Par_Nombre_Tx;
        private String _Par_Descripcion_Tx;
        private int _Par_Valor_Nu;

        public int Par_Id
        {
            get { return _Par_Id; }
            set { _Par_Id = value; }
        }

        public int Par_Padre_Id 
        {
            get { return _Par_Padre_Id; }
            set { _Par_Padre_Id = value; }
        }

        public String Par_Nombre_Tx 
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }

        }

        public String Par_Descripcion_Tx
        {
            get { return _Par_Descripcion_Tx; }
            set { _Par_Descripcion_Tx = value; }
        }

        public int Par_Valor_Nu
        {
            get { return _Par_Valor_Nu; }
            set { _Par_Valor_Nu = value; }
        }
    }
}
