﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class UsuarioBE:AuditoriaBE 
    {
       private int _Usu_Id_Codigo;
        private string _Usu_Id;
        private string _Usu_Password_Tx;
        private string _Usu_APaterno_tx;
        private string _Usu_AMaterno_tx;
        private string _Usu_Nombres_tx;
        private string _Usu_Email_tx;
        private int _Usu_TipDocIdent_Id;
        private string _Usu_DocIdent_nu;
        private string _Usu_Telefono_nu;
        private string _Usu_Direccion_tx;
        private string _Usu_HoraLabDes_nu;
        private string _Usu_HoraLabHas_nu;
        private int _Area_Id;
        private int _Existe;
        private string _NombreCompleto;
        private int _Per_Id;

        public int Per_Id
        {
            get { return _Per_Id; }
            set { _Per_Id = value; }
        }
        public int Usu_Id_Codigo
        {
            get { return _Usu_Id_Codigo; }
            set { _Usu_Id_Codigo = value; }
        }
        public string Usu_Id
        {
            get { return _Usu_Id; }
            set { _Usu_Id = value; }
        }
        public string Usu_Password_Tx
        {
            get { return _Usu_Password_Tx; }
            set { _Usu_Password_Tx = value; }
        }
        public string Usu_APaterno_tx
        {
            get { return _Usu_APaterno_tx; }
            set { _Usu_APaterno_tx = value; }
        }
        public string Usu_AMaterno_tx
        {
            get { return _Usu_AMaterno_tx; }
            set { _Usu_AMaterno_tx = value; }
        }
        public string Usu_Nombres_tx
        {
            get { return _Usu_Nombres_tx; }
            set { _Usu_Nombres_tx = value; }
        }
        public string Usu_Email_tx
        {
            get { return _Usu_Email_tx; }
            set { _Usu_Email_tx = value; }
        }
        public int Usu_TipDocIdent_Id
        {
            get { return _Usu_TipDocIdent_Id; }
            set { _Usu_TipDocIdent_Id = value; }
        }
        public string Usu_DocIdent_nu
        {
            get { return _Usu_DocIdent_nu; }
            set { _Usu_DocIdent_nu = value; }
        }
        public string Usu_Telefono_nu
        {
            get { return _Usu_Telefono_nu; }
            set { _Usu_Telefono_nu = value; }
        }
        public string Usu_Direccion_tx
        {
            get { return _Usu_Direccion_tx; }
            set { _Usu_Direccion_tx = value; }
        }
        public string Usu_HoraLabDes_nu
        {
            get { return _Usu_HoraLabDes_nu; }
            set { _Usu_HoraLabDes_nu = value; }
        }
        public string Usu_HoraLabHas_nu
        {
            get { return _Usu_HoraLabHas_nu; }
            set { _Usu_HoraLabHas_nu = value; }
        }
        public int Area_Id
        {
            get { return _Area_Id; }
            set { _Area_Id = value; }
        }

        public int Existe
        {
            get { return _Existe; }
            set { _Existe = value; }
        }

        public string NombreCompleto
        {
            get { return _NombreCompleto; }
            set { _NombreCompleto = value; }
        }

    }
}
