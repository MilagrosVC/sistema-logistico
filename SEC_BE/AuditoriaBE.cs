﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class AuditoriaBE
    {
        private String _Aud_UsuarioCreacion_Id;
        private DateTime _Aud_Creacion_Fe;
        private String _Aud_UsuarioActualizacion_Id;
        private DateTime _Aud_Actualizacion_Fe;
        private DateTime _Aud_Eliminacion_Fe;
        private String _Aud_UsuarioEliminacion_Id;
        private bool? _Aud_Estado_Fl;


        public String Aud_UsuarioCreacion_Id
        {
            get { return _Aud_UsuarioCreacion_Id; }
            set { _Aud_UsuarioCreacion_Id = value; }
        }
        public DateTime Aud_Creacion_Fe
        {
            get { return _Aud_Creacion_Fe; }
            set { _Aud_Creacion_Fe = value; }
        }
        public String Aud_UsuarioActualizacion_Id
        {
            get { return _Aud_UsuarioActualizacion_Id; }
            set { _Aud_UsuarioActualizacion_Id = value; }
        }
        public DateTime Aud_Actualizacion_Fe
        {
            get { return _Aud_Actualizacion_Fe; }
            set { _Aud_Actualizacion_Fe = value; }
        }
        public DateTime Aud_Eliminacion_Fe
        {
            get { return _Aud_Eliminacion_Fe; }
            set { _Aud_Eliminacion_Fe = value; }
        }
        public String Aud_UsuarioEliminacion_Id
        {
            get { return _Aud_UsuarioEliminacion_Id; }
            set { _Aud_UsuarioEliminacion_Id = value; }
        }
        public bool? Aud_Estado_Fl
        {
            get { return _Aud_Estado_Fl; }
            set { _Aud_Estado_Fl = value; }
        }
    }
}
