﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ComprobanteDeBE
    {
        private int _Comprobante_Id;        
        private int _ComDe_Cantidad_Nu;        
        private String _UMe_Simbolo_Tx;        
        private String _ComDe_Descripcion_Tx;        
        private decimal _ComDe_Precio_Unitario_Nu;        
        private decimal _ComDe_Sub_Total_Nu;        

        public int Comprobante_Id
        {
            get { return _Comprobante_Id; }
            set { _Comprobante_Id = value; }
        }

        public int ComDe_Cantidad_Nu
        {
            get { return _ComDe_Cantidad_Nu; }
            set { _ComDe_Cantidad_Nu = value; }
        }

        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }

        public String ComDe_Descripcion_Tx
        {
            get { return _ComDe_Descripcion_Tx; }
            set { _ComDe_Descripcion_Tx = value; }
        }

        public decimal ComDe_Precio_Unitario_Nu
        {
            get { return _ComDe_Precio_Unitario_Nu; }
            set { _ComDe_Precio_Unitario_Nu = value; }
        }

        public decimal ComDe_Sub_Total_Nu
        {
            get { return _ComDe_Sub_Total_Nu; }
            set { _ComDe_Sub_Total_Nu = value; }
        }

    }
}
