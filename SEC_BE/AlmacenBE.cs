﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class AlmacenBE : AuditoriaBE
    {
        private int _Almacen_Id;
        private String _Alm_Nombre_Tx;
        private int _Alm_Tipo_Id;
        private String _Alm_Abreviado_Tx;
        private String _Alm_Descripcion_Tx;
        private String _Alm_Telefono_Tx;
        private int? _Ubigeo_Id;
        private int _Proyecto_Id;
        private String _Alm_Direccion_Tx;
        private String _Alm_Responsable_Tx;

     
        public int Almacen_Id 
        { 
            get { return _Almacen_Id; } 
            set { _Almacen_Id = value; } 
        }
        public String Alm_Nombre_Tx 
        { 
            get { return _Alm_Nombre_Tx; } 
            set { _Alm_Nombre_Tx = value; } 
        }
        public int Alm_Tipo_Id 
        { 
            get { return _Alm_Tipo_Id; } 
            set { _Alm_Tipo_Id = value; } 
        }
        public String Alm_Abreviado_Tx 
        { 
            get { return _Alm_Abreviado_Tx; } 
            set { _Alm_Abreviado_Tx = value; } 
        }
        public String Alm_Descripcion_Tx 
        { 
            get { return _Alm_Descripcion_Tx; } 
            set { _Alm_Descripcion_Tx = value; } 
        }
        public String Alm_Telefono_Tx 
        { 
            get { return _Alm_Telefono_Tx; } 
            set { _Alm_Telefono_Tx = value; } 
        }
        public int? Ubigeo_Id 
        { 
            get { return _Ubigeo_Id; } 
            set { _Ubigeo_Id = value; } 
        }

        public int Proyecto_Id 
        {
            get { return _Proyecto_Id; } 
            set { _Proyecto_Id = value; } 
        }
        public String Alm_Direccion_Tx 
        { 
            get { return _Alm_Direccion_Tx; } 
            set { _Alm_Direccion_Tx = value; } 
        }
        public String Alm_Responsable_Tx 
        { 
            get { return _Alm_Responsable_Tx; } 
            set { _Alm_Responsable_Tx = value; }
        }

        //Nuevos 16 Septiembre
       
        private String _Par_Nombre_Tx;
        private String _Pry_Nombre_Tx;
        private String _Emp_RazonSocial_Tx;


        public String Par_Nombre_Tx
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }

        public String Pry_Nombre_Tx
        {
            get { return _Pry_Nombre_Tx; }
            set { _Pry_Nombre_Tx = value; }
        }

        public String Emp_RazonSocial_Tx
        {
            get { return _Emp_RazonSocial_Tx; }
            set { _Emp_RazonSocial_Tx = value; }
        }

        //NUEVOS 17 SEPTIEMBRE

        private int _Departamento_Co;
        private int _Provincia_co;
        private int _Distrito_co;
        private String _Distrito_Tx;


        public int Departamento_Co
        {
            get { return _Departamento_Co; }
            set { _Departamento_Co = value; }
        }
        public int Provincia_co
        {
            get { return _Provincia_co; }
            set { _Provincia_co = value; }
        }
        public int Distrito_co
        {
            get { return _Distrito_co; }
            set { _Distrito_co = value; }
        }

        public String Distrito_Tx
        {
            get { return _Distrito_Tx; }
            set { _Distrito_Tx = value; }
        }
    }
}
