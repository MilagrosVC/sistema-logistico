﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class SubPartidaBE : AuditoriaBE
    {
        private int _Sub_Partida_Id;        
        private String _SPa_Nombre_Tx;        
        private String _SPa_Co;        
        private String _SPa_Descripcion_Tx;        
        private int _Partida_Id;       


        public int Sub_Partida_Id
        {
            get { return _Sub_Partida_Id; }
            set { _Sub_Partida_Id = value; }
        }

        public String SPa_Nombre_Tx
        {
            get { return _SPa_Nombre_Tx; }
            set { _SPa_Nombre_Tx = value; }
        }

        public String SPa_Co
        {
            get { return _SPa_Co; }
            set { _SPa_Co = value; }
        }

        public String SPa_Descripcion_Tx
        {
            get { return _SPa_Descripcion_Tx; }
            set { _SPa_Descripcion_Tx = value; }
        }
        public int Partida_Id
        {
            get { return _Partida_Id; }
            set { _Partida_Id = value; }
        }

    }
}
