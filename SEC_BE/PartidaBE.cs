﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class PartidaBE: AuditoriaBE
    {
        private int _Partida_Id;
        private String _Prt_Nombre_Tx;
        private String _Prt_Descripcion_Tx;
        private int _Presupuesto_Id;

        public int Partida_Id { get { return _Partida_Id; } set { _Partida_Id = value; } }
        public String Prt_Nombre_Tx { get { return _Prt_Nombre_Tx; } set { _Prt_Nombre_Tx = value; } }
        public String Prt_Descripcion_Tx { get { return _Prt_Descripcion_Tx; } set { _Prt_Descripcion_Tx = value; } }
        public int Presupuesto_Id { get { return _Presupuesto_Id; } set { _Presupuesto_Id = value; } }
    }
}
