﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
   public  class TTipoCambioBE : AuditoriaBE
    {
        int _TTipoCambio_Id;
        String _Ttc_Nombre_Tx;
        String _Ttc_Descripcion;

        public int TTipoCambio_Id
        {
            get { return _TTipoCambio_Id; }
            set { _TTipoCambio_Id = value; }
        }
        public String Ttc_Nombre_Tx
        {
            get { return _Ttc_Nombre_Tx; }
            set { _Ttc_Nombre_Tx = value; }
        }
        public String Ttc_Descripcion
        {
            get { return _Ttc_Descripcion; }
            set { _Ttc_Descripcion = value; }
        }
   
   }
}
