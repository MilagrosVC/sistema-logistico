﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class RequerimientoCaBE : AuditoriaBE
    {
        private int _Requerimiento_Id;        
        private String _Solicitante_Id;        
        private int _Centro_Costo_Id;        
        private DateTime _Req_Entrega_Fe;        
        private int _Req_Estado_Id;        
        private int _Req_Tipo_Id;        
        private int _Proyecto_Id;        
        private int _Item_Id;        
        private int _RDe_Cantidad_Nu;        
        private String _RDe_Descripcion_Tx;        
        private String _RDe_Observacion_Tx;        
        private int _Unidad_Medida_Id;
        private int _Turno_Id;        
        private String _Req_Ingreso;
        private String _Requerimiento_Co;
        private int _Partida_Id;
        private String _Est_Nombre_Tx;
        private String _Par_Nombre_Tx;
        private String _Tur_Nombre_Tx;
        private String _Prt_Nombre_Tx;
        private int _IMa_Id;
        private String _IMa_Descripcion_Tx;        


        #region  ------------------------Bandeja_RQ-------------------------
        private String _Req_Solicitante;
        private String _Req_Estado_Tx;
        private String _Req_Tipo_Tx;
        private DateTime? _FechaDesde;
        private DateTime? _FechaHasta;
        private int _Cant_Items;
        private String _Req_Turno_Tx;
        private String _Req_Proyecto_Tx;
        private String _Req_ProyDireccion_Tx;
        private String _Req_Partida_Tx;
        private String _Lst_Partidas_Rq;

        public int Cant_Items
        {
            get { return _Cant_Items; }
            set { _Cant_Items = value; }
        }
        public String Req_Solicitante
        {
            get { return _Req_Solicitante; }
            set { _Req_Solicitante = value; }
        }
        public String Req_Estado_Tx
        {
            get { return _Req_Estado_Tx; }
            set { _Req_Estado_Tx = value; }
        }
        public String Req_Tipo_Tx
        {
            get { return _Req_Tipo_Tx; }
            set { _Req_Tipo_Tx = value; }
        }
        public DateTime? FechaDesde
        {
            get { return _FechaDesde; }
            set { _FechaDesde = value; }
        }
        public DateTime? FechaHasta
        {
            get { return _FechaHasta; }
            set { _FechaHasta = value; }
        }
        public String Req_Turno_Tx
        {
            get { return _Req_Turno_Tx; }
            set { _Req_Turno_Tx = value; }
        }
        public String Req_Proyecto_Tx
        {
            get { return _Req_Proyecto_Tx; }
            set { _Req_Proyecto_Tx = value; }
        }
        public String Req_ProyDireccion_Tx
        {
            get { return _Req_ProyDireccion_Tx; }
            set { _Req_ProyDireccion_Tx = value; }
        }
        public String Req_Partida_Tx 
        {
            get { return _Req_Partida_Tx; }
            set { _Req_Partida_Tx = value; }
        }
        public String Lst_Partidas_Rq 
        {
            get { return _Lst_Partidas_Rq; }
            set { _Lst_Partidas_Rq = value; }
        }
    #endregion ---------------------------------------------------------


        public int Requerimiento_Id
        {
            get { return _Requerimiento_Id; }
            set { _Requerimiento_Id = value; }
        }

        public String Solicitante_Id
        {
            get { return _Solicitante_Id; }
            set { _Solicitante_Id = value; }
        }

        public int Centro_Costo_Id
        {
            get { return _Centro_Costo_Id; }
            set { _Centro_Costo_Id = value; }
        }

        public DateTime Req_Entrega_Fe
        {
            get { return _Req_Entrega_Fe; }
            set { _Req_Entrega_Fe = value; }
        }

        public int Req_Estado_Id
        {
            get { return _Req_Estado_Id; }
            set { _Req_Estado_Id = value; }
        }

        public int Req_Tipo_Id
        {
            get { return _Req_Tipo_Id; }
            set { _Req_Tipo_Id = value; }
        }

        public int Proyecto_Id
        {
            get { return _Proyecto_Id; }
            set { _Proyecto_Id = value; }
        }

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public int RDe_Cantidad_Nu
        {
            get { return _RDe_Cantidad_Nu; }
            set { _RDe_Cantidad_Nu = value; }
        }

        public String RDe_Descripcion_Tx
        {
            get { return _RDe_Descripcion_Tx; }
            set { _RDe_Descripcion_Tx = value; }
        }

        public String RDe_Observacion_Tx
        {
            get { return _RDe_Observacion_Tx; }
            set { _RDe_Observacion_Tx = value; }
        }

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }

        public int Turno_Id
        {
            get { return _Turno_Id; }
            set { _Turno_Id = value; }
        }

        public String Req_Ingreso
        {
            get { return _Req_Ingreso; }
            set { _Req_Ingreso = value; }
        }

        public String Requerimiento_Co 
        {
            get { return _Requerimiento_Co; }
            set { _Requerimiento_Co = value; }
        }

        public int Partida_Id
        {
            get { return _Partida_Id; }
            set { _Partida_Id = value; }
        }

        public String Est_Nombre_Tx
        {
            get { return _Est_Nombre_Tx; }
            set { _Est_Nombre_Tx = value; }
        }

        public String Par_Nombre_Tx
        {
            get { return _Par_Nombre_Tx; }
            set { _Par_Nombre_Tx = value; }
        }

        public String Tur_Nombre_Tx
        {
            get { return _Tur_Nombre_Tx; }
            set { _Tur_Nombre_Tx = value; }
        }

        public String Prt_Nombre_Tx
        {
            get { return _Prt_Nombre_Tx; }
            set { _Prt_Nombre_Tx = value; }
        }

        public int IMa_Id
        {
            get { return _IMa_Id; }
            set { _IMa_Id = value; }
        }

        public String IMa_Descripcion_Tx
        {
            get { return _IMa_Descripcion_Tx; }
            set { _IMa_Descripcion_Tx = value; }
        } 
    }
}
