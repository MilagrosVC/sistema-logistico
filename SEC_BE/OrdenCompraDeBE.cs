﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class OrdenCompraDeBE
    {
        private int _Orden_Compra_Detalle_Id;        
        private int _Orden_Compra_Id;        
        private int _Item_Id;        
        private int _OCDe_Cantidad_Nu;        
        private decimal _OCDe_Precio_Unitario_Nu;        
        private decimal _OCDe_Sub_Total_Nu;        
        private String _OCDe_Descripcion_Tx;
        private String _UMe_Simbolo_Tx;        

        public int Orden_Compra_Detalle_Id
        {
            get { return _Orden_Compra_Detalle_Id; }
            set { _Orden_Compra_Detalle_Id = value; }
        }

        public int Orden_Compra_Id
        {
            get { return _Orden_Compra_Id; }
            set { _Orden_Compra_Id = value; }
        }

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public int OCDe_Cantidad_Nu
        {
            get { return _OCDe_Cantidad_Nu; }
            set { _OCDe_Cantidad_Nu = value; }
        }

        public decimal OCDe_Precio_Unitario_Nu
        {
            get { return _OCDe_Precio_Unitario_Nu; }
            set { _OCDe_Precio_Unitario_Nu = value; }
        }

        public decimal OCDe_Sub_Total_Nu
        {
            get { return _OCDe_Sub_Total_Nu; }
            set { _OCDe_Sub_Total_Nu = value; }
        }

        public String OCDe_Descripcion_Tx
        {
            get { return _OCDe_Descripcion_Tx; }
            set { _OCDe_Descripcion_Tx = value; }
        }

        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }
    }
}
