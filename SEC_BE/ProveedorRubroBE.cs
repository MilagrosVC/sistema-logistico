﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class ProveedorRubroBE: AuditoriaBE
    {
        private int _PRu_Id;
        private int _Prv_Id;
        private int _Rubro_Id;
        //*-Para asignar el nombre del Rubro-*//
        private String _Rub_Nombre_Tx;
        //*-Para asignar el nombre del proveedor-*//
        //*--No es necesario-*//

        public int PRu_Id 
        { 
            get { return _PRu_Id; } 
            set { _PRu_Id = value; } 
        }
        public int Prv_Id 
        { 
            get { return _Prv_Id; } 
            set { _Prv_Id = value; } 
        }
        public int Rubro_Id 
        {
            get { return _Rubro_Id; } 
            set { _Rubro_Id = value; } 
        }

        public String Rub_Nombre_Tx
        {
            get { return _Rub_Nombre_Tx; }
            set { _Rub_Nombre_Tx = value; }
        }
    }
}
