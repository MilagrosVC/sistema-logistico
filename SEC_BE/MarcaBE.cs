﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class MarcaBE
    {
        private int _Marca_Id;
        private String _Mar_Nombre_Tx;
        private String _Mar_Descripcion_Tx;        

        public int Marca_Id
        {
            get { return _Marca_Id; }
            set { _Marca_Id = value; }
        }

        public String Mar_Nombre_Tx
        {
            get { return _Mar_Nombre_Tx; }
            set { _Mar_Nombre_Tx = value; }
        }

        public String Mar_Descripcion_Tx
        {
            get { return _Mar_Descripcion_Tx; }
            set { _Mar_Descripcion_Tx = value; }
        }
    }
}
