﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class BancoBE:AuditoriaBE
    {
        private int _Banco_Id;
        private String _Ban_Nombre_Tx;
        private String _Ban_Descripcion_Tx;

        public int Banco_Id 
        { 
            get { return _Banco_Id; } 
            set { _Banco_Id = value; } 
        }
        public String Ban_Nombre_Tx 
        { 
            get { return _Ban_Nombre_Tx; } 
            set { _Ban_Nombre_Tx = value; } 
        }
        public String Ban_Descripcion_Tx 
        { 
            get { return _Ban_Descripcion_Tx; } 
            set { _Ban_Descripcion_Tx = value; } 
        }
    }
}
