﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class CuentaBE : AuditoriaBE
    {
        private int _Cuenta_Id;
        private String _Cue_Codigo;
        private String _Cue_Nombre_tx;
        private String _Cue_Cc_TX;
        private bool _Cue_Cc_Fl;
        private String _Cue_Ta_Tx;
        private String _Cue_Transfiere_Tx;
        private bool? _Cue_TipoDestino_FL;
        private String _Cue_TipoDestino_TX;

        private Decimal? _Cue_PorcenDebe_nu;
        private Decimal? _Cue_PorcenHaber_nu;

        private Decimal? _Cue_PorcenDebe_nu_entero;
        private Decimal? _Cue_PorcenHaber_nu_entero;

        private int _Cue_Padre_Id;
        private int _Elemento_Id;
        private int _PlanCuenta_Id;

        /*------------NUEVOS CAMPOS - 17 Agosto--------*/
        private int? _Banco_Id;
        private int? _Moneda_Id;
        private int? _TTipoCambio_Id;
        private String _Cue_NumeroCuenta;
        private bool _Cue_CancTranf_FL;
        private bool _Cue_CuentaOficial_FL;
        /*-----------------------------------------------*/

        public int Cuenta_Id 
        {
            get { return _Cuenta_Id; } 
            set { _Cuenta_Id = value; } 
        }
        public String Cue_Codigo 
        { 
            get { return _Cue_Codigo; } 
            set { _Cue_Codigo = value; } 
        }
        public String Cue_Nombre_tx 
        { 
            get { return _Cue_Nombre_tx; } 
            set { _Cue_Nombre_tx = value; } 
        }
        public String Cue_Cc_TX
        {
            get { return _Cue_Cc_TX; }
            set { _Cue_Cc_TX = value; }
        }
        public bool Cue_Cc_Fl
        {
            get { return _Cue_Cc_Fl; }
            set { _Cue_Cc_Fl = value; }
        }
        public String Cue_Ta_Tx 
        { 
            get { return _Cue_Ta_Tx; } 
            set { _Cue_Ta_Tx = value; } 
        }
        public String Cue_Transfiere_Tx 
        { 
            get { return _Cue_Transfiere_Tx; } 
            set { _Cue_Transfiere_Tx = value; } 
        }
        public bool? Cue_TipoDestino_FL 
        { 
            get { return _Cue_TipoDestino_FL; } 
            set { _Cue_TipoDestino_FL = value; } 
        }
        public String Cue_TipoDestino_TX
        {
            get { return _Cue_TipoDestino_TX; }
            set { _Cue_TipoDestino_TX = value; }
        }

        public Decimal? Cue_PorcenDebe_nu 
        { 
            get { return _Cue_PorcenDebe_nu; } 
            set { _Cue_PorcenDebe_nu = value; } 
        }
        public Decimal? Cue_PorcenHaber_nu 
        { 
            get { return _Cue_PorcenHaber_nu; } 
            set { _Cue_PorcenHaber_nu = value; } 
        }
        //public String Cue_PorcenDebe_nu
        //{
        //    get { return _Cue_PorcenDebe_nu; }
        //    set { _Cue_PorcenDebe_nu = value; }
        //}
        //public String Cue_PorcenHaber_nu
        //{
        //    get { return _Cue_PorcenHaber_nu; }
        //    set { _Cue_PorcenHaber_nu = value; }
        //}
        public int Cue_Padre_Id 
        { 
            get { return _Cue_Padre_Id; } 
            set { _Cue_Padre_Id = value; } 
        }
        public int Elemento_Id 
        { 
            get { return _Elemento_Id; } 
            set { _Elemento_Id = value; } 
        }
        public int PlanCuenta_Id 
        { 
            get { return _PlanCuenta_Id; } 
            set { _PlanCuenta_Id = value; } 
        }

        /*----Metodos de Acceso de Nuevos Campos - 17 Agosto-----*/
        public int? Banco_Id
        {
            get { return _Banco_Id; }
            set { _Banco_Id = value; }
        }
        public int? Moneda_Id
        {
            get { return _Moneda_Id; }
            set { _Moneda_Id = value; }
        }
        public int? TTipoCambio_Id
        {
            get { return _TTipoCambio_Id; }
            set { _TTipoCambio_Id = value; }
        }
        public String Cue_NumeroCuenta
        {
            get { return _Cue_NumeroCuenta; }
            set { _Cue_NumeroCuenta = value; }
        }
        public bool Cue_CancTranf_FL
        {
            get { return _Cue_CancTranf_FL; }
            set { _Cue_CancTranf_FL = value; }
        }
        public bool Cue_CuentaOficial_FL
        {
            get { return _Cue_CuentaOficial_FL; }
            set { _Cue_CuentaOficial_FL = value; }
        }
        /*----------------------------------------------------------*/

        int _Ele_Co;
        String _Ele_Nombre_Tx;
        String _TCu_Nombre_Tx;
        String _Cat_Nombre_Tx;
        String _Pla_Nombre_Tx;
        int _TipoCuenta_Id;
        int _CatalogoCuenta_Id;
        String _Cuenta_Transfiere_Nombre_Tx;
        public int Ele_Co
        {
            get { return _Ele_Co; }
            set { _Ele_Co = value; }
        }

        public String Ele_Nombre_Tx
        {
            get { return _Ele_Nombre_Tx; }
            set { _Ele_Nombre_Tx = value; }
        }
        public String TCu_Nombre_Tx
        {
            get { return _TCu_Nombre_Tx; }
            set { _TCu_Nombre_Tx = value; }
        }
        public String Cat_Nombre_Tx
        {
            get { return _Cat_Nombre_Tx; }
            set { _Cat_Nombre_Tx = value; }
        }
        public String Pla_Nombre_Tx
        {
            get { return _Pla_Nombre_Tx; }
            set { _Pla_Nombre_Tx = value; }
        }
        public int TipoCuenta_Id
        {
            get { return _TipoCuenta_Id; }
            set { _TipoCuenta_Id = value; }
        }
        public int CatalogoCuenta_Id 
        {
            get { return _CatalogoCuenta_Id; }
            set { _CatalogoCuenta_Id = value; } 
        }

        public String Cuenta_Transfiere_Nombre_Tx
        {
            get { return _Cuenta_Transfiere_Nombre_Tx; }
            set { _Cuenta_Transfiere_Nombre_Tx = value; }
        }

        public Decimal? Cue_PorcenDebe_nu_entero
        {
            get { return _Cue_PorcenDebe_nu_entero; }
            set { _Cue_PorcenDebe_nu_entero = value; }
        }
        public Decimal? Cue_PorcenHaber_nu_entero
        {
            get { return _Cue_PorcenHaber_nu_entero; }
            set { _Cue_PorcenHaber_nu_entero = value; }
        }

    }
}
