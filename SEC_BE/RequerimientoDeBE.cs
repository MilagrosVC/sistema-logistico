﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEC_BE
{
    public class RequerimientoDeBE : AuditoriaBE
    {
        private int _Requerimiento_Detalle_Id;        
        private int _Requerimiento_Id;        
        private int _Item_Id;
        private int _RDe_Cantidad_Nu;
        private String _RDe_Descripcion_Tx;
        private int _Unidad_Medida_Id;
        private String _RDe_Observacion_Tx;
        private int _RDe_Estado_Id;

        /*-----Atributos adicionales-----*/
        private int? _Solicitud_Id;
        private int? _Solicitud_Detalle_Id;
        private String _UMe_Simbolo_Tx;
        private String _Itm_Co;
        private String _RDe_Estado_Tx;
        private Int64 _NumItem;
        private String _Itm_Nombre_Tx;
        private int _IMa_Id;
        private String _IMa_Descripcion_Tx;        

        public String Itm_Nombre_Tx
        {
            get { return _Itm_Nombre_Tx; }
            set { _Itm_Nombre_Tx = value; }
        }

        public int? Solicitud_Id
        {
            get { return _Solicitud_Id; }
            set { _Solicitud_Id = value; }
        }
        public int? Solicitud_Detalle_Id
        {
            get { return _Solicitud_Detalle_Id; }
            set { _Solicitud_Detalle_Id = value; }
        }
        public String UMe_Simbolo_Tx
        {
            get { return _UMe_Simbolo_Tx; }
            set { _UMe_Simbolo_Tx = value; }
        }
        public String Itm_Co
        {
            get { return _Itm_Co; }
            set { _Itm_Co = value; }
        }
        public String RDe_Estado_Tx
        {
            get { return _RDe_Estado_Tx; }
            set { _RDe_Estado_Tx = value; }
        }
        /*-------------------------------*/
        public int Requerimiento_Detalle_Id
        {
            get { return _Requerimiento_Detalle_Id; }
            set { _Requerimiento_Detalle_Id = value; }
        }

        public int Requerimiento_Id
        {
            get { return _Requerimiento_Id; }
            set { _Requerimiento_Id = value; }
        }

        public int Item_Id
        {
            get { return _Item_Id; }
            set { _Item_Id = value; }
        }

        public int RDe_Cantidad_Nu
        {
            get { return _RDe_Cantidad_Nu; }
            set { _RDe_Cantidad_Nu = value; }
        }

        public String RDe_Descripcion_Tx
        {
            get { return _RDe_Descripcion_Tx; }
            set { _RDe_Descripcion_Tx = value; }
        }

        public int Unidad_Medida_Id
        {
            get { return _Unidad_Medida_Id; }
            set { _Unidad_Medida_Id = value; }
        }

        public String RDe_Observacion_Tx
        {
            get { return _RDe_Observacion_Tx; }
            set { _RDe_Observacion_Tx = value; }
        }
        public int RDe_Estado_Id
        {
            get { return _RDe_Estado_Id; }
            set { _RDe_Estado_Id = value; }
        }

        public Int64 NumItem
        {
            get { return _NumItem; }
            set { _NumItem = value; }
        }

        public int IMa_Id
        {
            get { return _IMa_Id; }
            set { _IMa_Id = value; }
        }

        public String IMa_Descripcion_Tx
        {
            get { return _IMa_Descripcion_Tx; }
            set { _IMa_Descripcion_Tx = value; }
        }
    }
}
